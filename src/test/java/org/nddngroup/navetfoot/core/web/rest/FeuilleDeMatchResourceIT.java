package org.nddngroup.navetfoot.core.web.rest;

import org.nddngroup.navetfoot.core.NavetfootCore;
import org.nddngroup.navetfoot.core.domain.FeuilleDeMatch;
import org.nddngroup.navetfoot.core.repository.FeuilleDeMatchRepository;
import org.nddngroup.navetfoot.core.repository.search.FeuilleDeMatchSearchRepository;
import org.nddngroup.navetfoot.core.service.FeuilleDeMatchService;
import org.nddngroup.navetfoot.core.service.dto.FeuilleDeMatchDTO;
import org.nddngroup.navetfoot.core.service.mapper.FeuilleDeMatchMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.nddngroup.navetfoot.core.repository.search.FeuilleDeMatchSearchRepositoryMockConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static org.nddngroup.navetfoot.core.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link FeuilleDeMatchResource} REST controller.
 */
@SpringBootTest(classes = NavetfootCore.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class FeuilleDeMatchResourceIT {

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_HASHCODE = "AAAAAAAAAA";
    private static final String UPDATED_HASHCODE = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Boolean DEFAULT_DELETED = false;
    private static final Boolean UPDATED_DELETED = true;

    @Autowired
    private FeuilleDeMatchRepository feuilleDeMatchRepository;

    @Autowired
    private FeuilleDeMatchMapper feuilleDeMatchMapper;

    @Autowired
    private FeuilleDeMatchService feuilleDeMatchService;

    /**
     * This repository is mocked in the org.nddngroup.navetfoot.core.repository.search test package.
     *
     * @see FeuilleDeMatchSearchRepositoryMockConfiguration
     */
    @Autowired
    private FeuilleDeMatchSearchRepository mockFeuilleDeMatchSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restFeuilleDeMatchMockMvc;

    private FeuilleDeMatch feuilleDeMatch;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FeuilleDeMatch createEntity(EntityManager em) {
        FeuilleDeMatch feuilleDeMatch = new FeuilleDeMatch()
            .code(DEFAULT_CODE)
            .hashcode(DEFAULT_HASHCODE)
            .updatedAt(DEFAULT_UPDATED_AT)
            .createdAt(DEFAULT_CREATED_AT)
            .deleted(DEFAULT_DELETED);
        return feuilleDeMatch;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FeuilleDeMatch createUpdatedEntity(EntityManager em) {
        FeuilleDeMatch feuilleDeMatch = new FeuilleDeMatch()
            .code(UPDATED_CODE)
            .hashcode(UPDATED_HASHCODE)
            .updatedAt(UPDATED_UPDATED_AT)
            .createdAt(UPDATED_CREATED_AT)
            .deleted(UPDATED_DELETED);
        return feuilleDeMatch;
    }

    @BeforeEach
    public void initTest() {
        feuilleDeMatch = createEntity(em);
    }

    @Test
    @Transactional
    public void createFeuilleDeMatch() throws Exception {
        int databaseSizeBeforeCreate = feuilleDeMatchRepository.findAll().size();
        // Create the FeuilleDeMatch
        FeuilleDeMatchDTO feuilleDeMatchDTO = feuilleDeMatchMapper.toDto(feuilleDeMatch);
        restFeuilleDeMatchMockMvc.perform(post("/api/feuille-de-matches")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(feuilleDeMatchDTO)))
            .andExpect(status().isCreated());

        // Validate the FeuilleDeMatch in the database
        List<FeuilleDeMatch> feuilleDeMatchList = feuilleDeMatchRepository.findAll();
        assertThat(feuilleDeMatchList).hasSize(databaseSizeBeforeCreate + 1);
        FeuilleDeMatch testFeuilleDeMatch = feuilleDeMatchList.get(feuilleDeMatchList.size() - 1);
        assertThat(testFeuilleDeMatch.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testFeuilleDeMatch.getHashcode()).isEqualTo(DEFAULT_HASHCODE);
        assertThat(testFeuilleDeMatch.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testFeuilleDeMatch.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testFeuilleDeMatch.isDeleted()).isEqualTo(DEFAULT_DELETED);

        // Validate the FeuilleDeMatch in Elasticsearch
        verify(mockFeuilleDeMatchSearchRepository, times(1)).save(testFeuilleDeMatch);
    }

    @Test
    @Transactional
    public void createFeuilleDeMatchWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = feuilleDeMatchRepository.findAll().size();

        // Create the FeuilleDeMatch with an existing ID
        feuilleDeMatch.setId(1L);
        FeuilleDeMatchDTO feuilleDeMatchDTO = feuilleDeMatchMapper.toDto(feuilleDeMatch);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFeuilleDeMatchMockMvc.perform(post("/api/feuille-de-matches")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(feuilleDeMatchDTO)))
            .andExpect(status().isBadRequest());

        // Validate the FeuilleDeMatch in the database
        List<FeuilleDeMatch> feuilleDeMatchList = feuilleDeMatchRepository.findAll();
        assertThat(feuilleDeMatchList).hasSize(databaseSizeBeforeCreate);

        // Validate the FeuilleDeMatch in Elasticsearch
        verify(mockFeuilleDeMatchSearchRepository, times(0)).save(feuilleDeMatch);
    }


    @Test
    @Transactional
    public void getAllFeuilleDeMatches() throws Exception {
        // Initialize the database
        feuilleDeMatchRepository.saveAndFlush(feuilleDeMatch);

        // Get all the feuilleDeMatchList
        restFeuilleDeMatchMockMvc.perform(get("/api/feuille-de-matches?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(feuilleDeMatch.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].hashcode").value(hasItem(DEFAULT_HASHCODE)))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())));
    }

    @Test
    @Transactional
    public void getFeuilleDeMatch() throws Exception {
        // Initialize the database
        feuilleDeMatchRepository.saveAndFlush(feuilleDeMatch);

        // Get the feuilleDeMatch
        restFeuilleDeMatchMockMvc.perform(get("/api/feuille-de-matches/{id}", feuilleDeMatch.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(feuilleDeMatch.getId().intValue()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE))
            .andExpect(jsonPath("$.hashcode").value(DEFAULT_HASHCODE))
            .andExpect(jsonPath("$.updatedAt").value(sameInstant(DEFAULT_UPDATED_AT)))
            .andExpect(jsonPath("$.createdAt").value(sameInstant(DEFAULT_CREATED_AT)))
            .andExpect(jsonPath("$.deleted").value(DEFAULT_DELETED.booleanValue()));
    }
    @Test
    @Transactional
    public void getNonExistingFeuilleDeMatch() throws Exception {
        // Get the feuilleDeMatch
        restFeuilleDeMatchMockMvc.perform(get("/api/feuille-de-matches/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFeuilleDeMatch() throws Exception {
        // Initialize the database
        feuilleDeMatchRepository.saveAndFlush(feuilleDeMatch);

        int databaseSizeBeforeUpdate = feuilleDeMatchRepository.findAll().size();

        // Update the feuilleDeMatch
        FeuilleDeMatch updatedFeuilleDeMatch = feuilleDeMatchRepository.findById(feuilleDeMatch.getId()).get();
        // Disconnect from session so that the updates on updatedFeuilleDeMatch are not directly saved in db
        em.detach(updatedFeuilleDeMatch);
        updatedFeuilleDeMatch
            .code(UPDATED_CODE)
            .hashcode(UPDATED_HASHCODE)
            .updatedAt(UPDATED_UPDATED_AT)
            .createdAt(UPDATED_CREATED_AT);
        FeuilleDeMatchDTO feuilleDeMatchDTO = feuilleDeMatchMapper.toDto(updatedFeuilleDeMatch);

        restFeuilleDeMatchMockMvc.perform(put("/api/feuille-de-matches")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(feuilleDeMatchDTO)))
            .andExpect(status().isOk());

        // Validate the FeuilleDeMatch in the database
        List<FeuilleDeMatch> feuilleDeMatchList = feuilleDeMatchRepository.findAll();
        assertThat(feuilleDeMatchList).hasSize(databaseSizeBeforeUpdate);
        FeuilleDeMatch testFeuilleDeMatch = feuilleDeMatchList.get(feuilleDeMatchList.size() - 1);
        assertThat(testFeuilleDeMatch.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testFeuilleDeMatch.getHashcode()).isEqualTo(UPDATED_HASHCODE);
        assertThat(testFeuilleDeMatch.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testFeuilleDeMatch.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);

        // Validate the FeuilleDeMatch in Elasticsearch
        verify(mockFeuilleDeMatchSearchRepository, times(1)).save(testFeuilleDeMatch);
    }

    @Test
    @Transactional
    public void updateNonExistingFeuilleDeMatch() throws Exception {
        int databaseSizeBeforeUpdate = feuilleDeMatchRepository.findAll().size();

        // Create the FeuilleDeMatch
        FeuilleDeMatchDTO feuilleDeMatchDTO = feuilleDeMatchMapper.toDto(feuilleDeMatch);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFeuilleDeMatchMockMvc.perform(put("/api/feuille-de-matches")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(feuilleDeMatchDTO)))
            .andExpect(status().isBadRequest());

        // Validate the FeuilleDeMatch in the database
        List<FeuilleDeMatch> feuilleDeMatchList = feuilleDeMatchRepository.findAll();
        assertThat(feuilleDeMatchList).hasSize(databaseSizeBeforeUpdate);

        // Validate the FeuilleDeMatch in Elasticsearch
        verify(mockFeuilleDeMatchSearchRepository, times(0)).save(feuilleDeMatch);
    }

    @Test
    @Transactional
    public void deleteFeuilleDeMatch() throws Exception {
        // Initialize the database
        feuilleDeMatchRepository.saveAndFlush(feuilleDeMatch);

        int databaseSizeBeforeDelete = feuilleDeMatchRepository.findAll().size();

        // Delete the feuilleDeMatch
        restFeuilleDeMatchMockMvc.perform(delete("/api/feuille-de-matches/{id}", feuilleDeMatch.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<FeuilleDeMatch> feuilleDeMatchList = feuilleDeMatchRepository.findAll();
        assertThat(feuilleDeMatchList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the FeuilleDeMatch in Elasticsearch
        verify(mockFeuilleDeMatchSearchRepository, times(1)).deleteById(feuilleDeMatch.getId());
    }

    @Test
    @Transactional
    public void searchFeuilleDeMatch() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        feuilleDeMatchRepository.saveAndFlush(feuilleDeMatch);
        when(mockFeuilleDeMatchSearchRepository.search(queryStringQuery("id:" + feuilleDeMatch.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(feuilleDeMatch), PageRequest.of(0, 1), 1));

        // Search the feuilleDeMatch
        restFeuilleDeMatchMockMvc.perform(get("/api/_search/feuille-de-matches?query=id:" + feuilleDeMatch.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(feuilleDeMatch.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].hashcode").value(hasItem(DEFAULT_HASHCODE)))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())));
    }
}
