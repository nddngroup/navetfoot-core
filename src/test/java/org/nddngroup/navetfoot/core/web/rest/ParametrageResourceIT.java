package org.nddngroup.navetfoot.core.web.rest;

import org.nddngroup.navetfoot.core.NavetfootCore;
import org.nddngroup.navetfoot.core.domain.Parametrage;
import org.nddngroup.navetfoot.core.repository.ParametrageRepository;
import org.nddngroup.navetfoot.core.repository.search.ParametrageSearchRepository;
import org.nddngroup.navetfoot.core.service.ParametrageService;
import org.nddngroup.navetfoot.core.service.dto.ParametrageDTO;
import org.nddngroup.navetfoot.core.service.mapper.ParametrageMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.nddngroup.navetfoot.core.repository.search.ParametrageSearchRepositoryMockConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static org.nddngroup.navetfoot.core.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.nddngroup.navetfoot.core.domain.enumeration.TypeParamatrage;
/**
 * Integration tests for the {@link ParametrageResource} REST controller.
 */
@SpringBootTest(classes = NavetfootCore.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class ParametrageResourceIT {

    private static final String DEFAULT_TAG = "AAAAAAAAAA";
    private static final String UPDATED_TAG = "BBBBBBBBBB";

    private static final String DEFAULT_LIBELLE = "AAAAAAAAAA";
    private static final String UPDATED_LIBELLE = "BBBBBBBBBB";

    private static final TypeParamatrage DEFAULT_TYPE = TypeParamatrage.VALEUR;
    private static final TypeParamatrage UPDATED_TYPE = TypeParamatrage.SELECT;

    private static final String DEFAULT_VALEUR = "AAAAAAAAAA";
    private static final String UPDATED_VALEUR = "BBBBBBBBBB";

    private static final String DEFAULT_FONCTION_DE_CHOIX = "AAAAAAAAAA";
    private static final String UPDATED_FONCTION_DE_CHOIX = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_REFERENCE_TAG = "AAAAAAAAAA";
    private static final String UPDATED_REFERENCE_TAG = "BBBBBBBBBB";

    private static final Long DEFAULT_REFERENCE_ID = 1L;
    private static final Long UPDATED_REFERENCE_ID = 2L;

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private ParametrageRepository parametrageRepository;

    @Autowired
    private ParametrageMapper parametrageMapper;

    @Autowired
    private ParametrageService parametrageService;

    /**
     * This repository is mocked in the org.nddngroup.navetfoot.core.repository.search test package.
     *
     * @see ParametrageSearchRepositoryMockConfiguration
     */
    @Autowired
    private ParametrageSearchRepository mockParametrageSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restParametrageMockMvc;

    private Parametrage parametrage;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Parametrage createEntity(EntityManager em) {
        Parametrage parametrage = new Parametrage()
            .tag(DEFAULT_TAG)
            .libelle(DEFAULT_LIBELLE)
            .type(DEFAULT_TYPE)
            .valeur(DEFAULT_VALEUR)
            .fonctionDeChoix(DEFAULT_FONCTION_DE_CHOIX)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT)
            .referenceTag(DEFAULT_REFERENCE_TAG)
            .referenceId(DEFAULT_REFERENCE_ID)
            .description(DEFAULT_DESCRIPTION);
        return parametrage;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Parametrage createUpdatedEntity(EntityManager em) {
        Parametrage parametrage = new Parametrage()
            .tag(UPDATED_TAG)
            .libelle(UPDATED_LIBELLE)
            .type(UPDATED_TYPE)
            .valeur(UPDATED_VALEUR)
            .fonctionDeChoix(UPDATED_FONCTION_DE_CHOIX)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .referenceTag(UPDATED_REFERENCE_TAG)
            .referenceId(UPDATED_REFERENCE_ID)
            .description(UPDATED_DESCRIPTION);
        return parametrage;
    }

    @BeforeEach
    public void initTest() {
        parametrage = createEntity(em);
    }

    @Test
    @Transactional
    public void createParametrage() throws Exception {
        int databaseSizeBeforeCreate = parametrageRepository.findAll().size();
        // Create the Parametrage
        ParametrageDTO parametrageDTO = parametrageMapper.toDto(parametrage);
        restParametrageMockMvc.perform(post("/api/parametrages")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(parametrageDTO)))
            .andExpect(status().isCreated());

        // Validate the Parametrage in the database
        List<Parametrage> parametrageList = parametrageRepository.findAll();
        assertThat(parametrageList).hasSize(databaseSizeBeforeCreate + 1);
        Parametrage testParametrage = parametrageList.get(parametrageList.size() - 1);
        assertThat(testParametrage.getTag()).isEqualTo(DEFAULT_TAG);
        assertThat(testParametrage.getLibelle()).isEqualTo(DEFAULT_LIBELLE);
        assertThat(testParametrage.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testParametrage.getValeur()).isEqualTo(DEFAULT_VALEUR);
        assertThat(testParametrage.getFonctionDeChoix()).isEqualTo(DEFAULT_FONCTION_DE_CHOIX);
        assertThat(testParametrage.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testParametrage.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testParametrage.getReferenceTag()).isEqualTo(DEFAULT_REFERENCE_TAG);
        assertThat(testParametrage.getReferenceId()).isEqualTo(DEFAULT_REFERENCE_ID);
        assertThat(testParametrage.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);

        // Validate the Parametrage in Elasticsearch
        verify(mockParametrageSearchRepository, times(1)).save(testParametrage);
    }

    @Test
    @Transactional
    public void createParametrageWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = parametrageRepository.findAll().size();

        // Create the Parametrage with an existing ID
        parametrage.setId(1L);
        ParametrageDTO parametrageDTO = parametrageMapper.toDto(parametrage);

        // An entity with an existing ID cannot be created, so this API call must fail
        restParametrageMockMvc.perform(post("/api/parametrages")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(parametrageDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Parametrage in the database
        List<Parametrage> parametrageList = parametrageRepository.findAll();
        assertThat(parametrageList).hasSize(databaseSizeBeforeCreate);

        // Validate the Parametrage in Elasticsearch
        verify(mockParametrageSearchRepository, times(0)).save(parametrage);
    }


    @Test
    @Transactional
    public void getAllParametrages() throws Exception {
        // Initialize the database
        parametrageRepository.saveAndFlush(parametrage);

        // Get all the parametrageList
        restParametrageMockMvc.perform(get("/api/parametrages?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(parametrage.getId().intValue())))
            .andExpect(jsonPath("$.[*].tag").value(hasItem(DEFAULT_TAG)))
            .andExpect(jsonPath("$.[*].libelle").value(hasItem(DEFAULT_LIBELLE)))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].valeur").value(hasItem(DEFAULT_VALEUR)))
            .andExpect(jsonPath("$.[*].fonctionDeChoix").value(hasItem(DEFAULT_FONCTION_DE_CHOIX)))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].referenceTag").value(hasItem(DEFAULT_REFERENCE_TAG)))
            .andExpect(jsonPath("$.[*].referenceId").value(hasItem(DEFAULT_REFERENCE_ID.intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)));
    }

    @Test
    @Transactional
    public void getParametrage() throws Exception {
        // Initialize the database
        parametrageRepository.saveAndFlush(parametrage);

        // Get the parametrage
        restParametrageMockMvc.perform(get("/api/parametrages/{id}", parametrage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(parametrage.getId().intValue()))
            .andExpect(jsonPath("$.tag").value(DEFAULT_TAG))
            .andExpect(jsonPath("$.libelle").value(DEFAULT_LIBELLE))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()))
            .andExpect(jsonPath("$.valeur").value(DEFAULT_VALEUR))
            .andExpect(jsonPath("$.fonctionDeChoix").value(DEFAULT_FONCTION_DE_CHOIX))
            .andExpect(jsonPath("$.createdAt").value(sameInstant(DEFAULT_CREATED_AT)))
            .andExpect(jsonPath("$.updatedAt").value(sameInstant(DEFAULT_UPDATED_AT)))
            .andExpect(jsonPath("$.referenceTag").value(DEFAULT_REFERENCE_TAG))
            .andExpect(jsonPath("$.referenceId").value(DEFAULT_REFERENCE_ID.intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION));
    }
    @Test
    @Transactional
    public void getNonExistingParametrage() throws Exception {
        // Get the parametrage
        restParametrageMockMvc.perform(get("/api/parametrages/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateParametrage() throws Exception {
        // Initialize the database
        parametrageRepository.saveAndFlush(parametrage);

        int databaseSizeBeforeUpdate = parametrageRepository.findAll().size();

        // Update the parametrage
        Parametrage updatedParametrage = parametrageRepository.findById(parametrage.getId()).get();
        // Disconnect from session so that the updates on updatedParametrage are not directly saved in db
        em.detach(updatedParametrage);
        updatedParametrage
            .tag(UPDATED_TAG)
            .libelle(UPDATED_LIBELLE)
            .type(UPDATED_TYPE)
            .valeur(UPDATED_VALEUR)
            .fonctionDeChoix(UPDATED_FONCTION_DE_CHOIX)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .referenceTag(UPDATED_REFERENCE_TAG)
            .referenceId(UPDATED_REFERENCE_ID)
            .description(UPDATED_DESCRIPTION);
        ParametrageDTO parametrageDTO = parametrageMapper.toDto(updatedParametrage);

        restParametrageMockMvc.perform(put("/api/parametrages")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(parametrageDTO)))
            .andExpect(status().isOk());

        // Validate the Parametrage in the database
        List<Parametrage> parametrageList = parametrageRepository.findAll();
        assertThat(parametrageList).hasSize(databaseSizeBeforeUpdate);
        Parametrage testParametrage = parametrageList.get(parametrageList.size() - 1);
        assertThat(testParametrage.getTag()).isEqualTo(UPDATED_TAG);
        assertThat(testParametrage.getLibelle()).isEqualTo(UPDATED_LIBELLE);
        assertThat(testParametrage.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testParametrage.getValeur()).isEqualTo(UPDATED_VALEUR);
        assertThat(testParametrage.getFonctionDeChoix()).isEqualTo(UPDATED_FONCTION_DE_CHOIX);
        assertThat(testParametrage.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testParametrage.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testParametrage.getReferenceTag()).isEqualTo(UPDATED_REFERENCE_TAG);
        assertThat(testParametrage.getReferenceId()).isEqualTo(UPDATED_REFERENCE_ID);
        assertThat(testParametrage.getDescription()).isEqualTo(UPDATED_DESCRIPTION);

        // Validate the Parametrage in Elasticsearch
        verify(mockParametrageSearchRepository, times(1)).save(testParametrage);
    }

    @Test
    @Transactional
    public void updateNonExistingParametrage() throws Exception {
        int databaseSizeBeforeUpdate = parametrageRepository.findAll().size();

        // Create the Parametrage
        ParametrageDTO parametrageDTO = parametrageMapper.toDto(parametrage);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restParametrageMockMvc.perform(put("/api/parametrages")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(parametrageDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Parametrage in the database
        List<Parametrage> parametrageList = parametrageRepository.findAll();
        assertThat(parametrageList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Parametrage in Elasticsearch
        verify(mockParametrageSearchRepository, times(0)).save(parametrage);
    }

    @Test
    @Transactional
    public void deleteParametrage() throws Exception {
        // Initialize the database
        parametrageRepository.saveAndFlush(parametrage);

        int databaseSizeBeforeDelete = parametrageRepository.findAll().size();

        // Delete the parametrage
        restParametrageMockMvc.perform(delete("/api/parametrages/{id}", parametrage.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Parametrage> parametrageList = parametrageRepository.findAll();
        assertThat(parametrageList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Parametrage in Elasticsearch
        verify(mockParametrageSearchRepository, times(1)).deleteById(parametrage.getId());
    }

    @Test
    @Transactional
    public void searchParametrage() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        parametrageRepository.saveAndFlush(parametrage);
        when(mockParametrageSearchRepository.search(queryStringQuery("id:" + parametrage.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(parametrage), PageRequest.of(0, 1), 1));

        // Search the parametrage
        restParametrageMockMvc.perform(get("/api/_search/parametrages?query=id:" + parametrage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(parametrage.getId().intValue())))
            .andExpect(jsonPath("$.[*].tag").value(hasItem(DEFAULT_TAG)))
            .andExpect(jsonPath("$.[*].libelle").value(hasItem(DEFAULT_LIBELLE)))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].valeur").value(hasItem(DEFAULT_VALEUR)))
            .andExpect(jsonPath("$.[*].fonctionDeChoix").value(hasItem(DEFAULT_FONCTION_DE_CHOIX)))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].referenceTag").value(hasItem(DEFAULT_REFERENCE_TAG)))
            .andExpect(jsonPath("$.[*].referenceId").value(hasItem(DEFAULT_REFERENCE_ID.intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)));
    }
}
