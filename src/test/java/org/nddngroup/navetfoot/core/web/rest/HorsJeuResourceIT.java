package org.nddngroup.navetfoot.core.web.rest;

import org.nddngroup.navetfoot.core.NavetfootCore;
import org.nddngroup.navetfoot.core.domain.HorsJeu;
import org.nddngroup.navetfoot.core.domain.Match;
import org.nddngroup.navetfoot.core.repository.HorsJeuRepository;
import org.nddngroup.navetfoot.core.repository.search.HorsJeuSearchRepository;
import org.nddngroup.navetfoot.core.service.HorsJeuService;
import org.nddngroup.navetfoot.core.service.dto.HorsJeuDTO;
import org.nddngroup.navetfoot.core.service.mapper.HorsJeuMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.nddngroup.navetfoot.core.repository.search.HorsJeuSearchRepositoryMockConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static org.nddngroup.navetfoot.core.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;
/**
 * Integration tests for the {@link HorsJeuResource} REST controller.
 */
@SpringBootTest(classes = NavetfootCore.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class HorsJeuResourceIT {

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_HASHCODE = "AAAAAAAAAA";
    private static final String UPDATED_HASHCODE = "BBBBBBBBBB";

    private static final Integer DEFAULT_INSTANT = 1;
    private static final Integer UPDATED_INSTANT = 2;

    private static final Boolean DEFAULT_DELETED = false;
    private static final Boolean UPDATED_DELETED = true;

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Equipe DEFAULT_EQUIPE = Equipe.LOCAL;
    private static final Equipe UPDATED_EQUIPE = Equipe.VISITEUR;

    @Autowired
    private HorsJeuRepository horsJeuRepository;

    @Autowired
    private HorsJeuMapper horsJeuMapper;

    @Autowired
    private HorsJeuService horsJeuService;

    /**
     * This repository is mocked in the org.nddngroup.navetfoot.core.repository.search test package.
     *
     * @see HorsJeuSearchRepositoryMockConfiguration
     */
    @Autowired
    private HorsJeuSearchRepository mockHorsJeuSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restHorsJeuMockMvc;

    private HorsJeu horsJeu;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static HorsJeu createEntity(EntityManager em) {
        HorsJeu horsJeu = new HorsJeu()
            .code(DEFAULT_CODE)
            .hashcode(DEFAULT_HASHCODE)
            .instant(DEFAULT_INSTANT)
            .deleted(DEFAULT_DELETED)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT)
            .equipe(DEFAULT_EQUIPE);
        // Add required entity
        Match match;
        if (TestUtil.findAll(em, Match.class).isEmpty()) {
            match = MatchResourceIT.createEntity(em);
            em.persist(match);
            em.flush();
        } else {
            match = TestUtil.findAll(em, Match.class).get(0);
        }
        horsJeu.setMatch(match);
        return horsJeu;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static HorsJeu createUpdatedEntity(EntityManager em) {
        HorsJeu horsJeu = new HorsJeu()
            .code(UPDATED_CODE)
            .hashcode(UPDATED_HASHCODE)
            .instant(UPDATED_INSTANT)
            .deleted(UPDATED_DELETED)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .equipe(UPDATED_EQUIPE);
        // Add required entity
        Match match;
        if (TestUtil.findAll(em, Match.class).isEmpty()) {
            match = MatchResourceIT.createUpdatedEntity(em);
            em.persist(match);
            em.flush();
        } else {
            match = TestUtil.findAll(em, Match.class).get(0);
        }
        horsJeu.setMatch(match);
        return horsJeu;
    }

    @BeforeEach
    public void initTest() {
        horsJeu = createEntity(em);
    }

    @Test
    @Transactional
    public void createHorsJeu() throws Exception {
        int databaseSizeBeforeCreate = horsJeuRepository.findAll().size();
        // Create the HorsJeu
        HorsJeuDTO horsJeuDTO = horsJeuMapper.toDto(horsJeu);
        restHorsJeuMockMvc.perform(post("/api/hors-jeus")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(horsJeuDTO)))
            .andExpect(status().isCreated());

        // Validate the HorsJeu in the database
        List<HorsJeu> horsJeuList = horsJeuRepository.findAll();
        assertThat(horsJeuList).hasSize(databaseSizeBeforeCreate + 1);
        HorsJeu testHorsJeu = horsJeuList.get(horsJeuList.size() - 1);
        assertThat(testHorsJeu.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testHorsJeu.getHashcode()).isEqualTo(DEFAULT_HASHCODE);
        assertThat(testHorsJeu.getInstant()).isEqualTo(DEFAULT_INSTANT);
        assertThat(testHorsJeu.isDeleted()).isEqualTo(DEFAULT_DELETED);
        assertThat(testHorsJeu.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testHorsJeu.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testHorsJeu.getEquipe()).isEqualTo(DEFAULT_EQUIPE);

        // Validate the HorsJeu in Elasticsearch
        verify(mockHorsJeuSearchRepository, times(1)).save(testHorsJeu);
    }

    @Test
    @Transactional
    public void createHorsJeuWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = horsJeuRepository.findAll().size();

        // Create the HorsJeu with an existing ID
        horsJeu.setId(1L);
        HorsJeuDTO horsJeuDTO = horsJeuMapper.toDto(horsJeu);

        // An entity with an existing ID cannot be created, so this API call must fail
        restHorsJeuMockMvc.perform(post("/api/hors-jeus")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(horsJeuDTO)))
            .andExpect(status().isBadRequest());

        // Validate the HorsJeu in the database
        List<HorsJeu> horsJeuList = horsJeuRepository.findAll();
        assertThat(horsJeuList).hasSize(databaseSizeBeforeCreate);

        // Validate the HorsJeu in Elasticsearch
        verify(mockHorsJeuSearchRepository, times(0)).save(horsJeu);
    }


    @Test
    @Transactional
    public void checkCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = horsJeuRepository.findAll().size();
        // set the field null
        horsJeu.setCode(null);

        // Create the HorsJeu, which fails.
        HorsJeuDTO horsJeuDTO = horsJeuMapper.toDto(horsJeu);


        restHorsJeuMockMvc.perform(post("/api/hors-jeus")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(horsJeuDTO)))
            .andExpect(status().isBadRequest());

        List<HorsJeu> horsJeuList = horsJeuRepository.findAll();
        assertThat(horsJeuList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkHashcodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = horsJeuRepository.findAll().size();
        // set the field null
        horsJeu.setHashcode(null);

        // Create the HorsJeu, which fails.
        HorsJeuDTO horsJeuDTO = horsJeuMapper.toDto(horsJeu);


        restHorsJeuMockMvc.perform(post("/api/hors-jeus")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(horsJeuDTO)))
            .andExpect(status().isBadRequest());

        List<HorsJeu> horsJeuList = horsJeuRepository.findAll();
        assertThat(horsJeuList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllHorsJeus() throws Exception {
        // Initialize the database
        horsJeuRepository.saveAndFlush(horsJeu);

        // Get all the horsJeuList
        restHorsJeuMockMvc.perform(get("/api/hors-jeus?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(horsJeu.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].hashcode").value(hasItem(DEFAULT_HASHCODE)))
            .andExpect(jsonPath("$.[*].instant").value(hasItem(DEFAULT_INSTANT)))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].equipe").value(hasItem(DEFAULT_EQUIPE.toString())));
    }

    @Test
    @Transactional
    public void getHorsJeu() throws Exception {
        // Initialize the database
        horsJeuRepository.saveAndFlush(horsJeu);

        // Get the horsJeu
        restHorsJeuMockMvc.perform(get("/api/hors-jeus/{id}", horsJeu.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(horsJeu.getId().intValue()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE))
            .andExpect(jsonPath("$.hashcode").value(DEFAULT_HASHCODE))
            .andExpect(jsonPath("$.instant").value(DEFAULT_INSTANT))
            .andExpect(jsonPath("$.deleted").value(DEFAULT_DELETED.booleanValue()))
            .andExpect(jsonPath("$.createdAt").value(sameInstant(DEFAULT_CREATED_AT)))
            .andExpect(jsonPath("$.updatedAt").value(sameInstant(DEFAULT_UPDATED_AT)))
            .andExpect(jsonPath("$.equipe").value(DEFAULT_EQUIPE.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingHorsJeu() throws Exception {
        // Get the horsJeu
        restHorsJeuMockMvc.perform(get("/api/hors-jeus/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateHorsJeu() throws Exception {
        // Initialize the database
        horsJeuRepository.saveAndFlush(horsJeu);

        int databaseSizeBeforeUpdate = horsJeuRepository.findAll().size();

        // Update the horsJeu
        HorsJeu updatedHorsJeu = horsJeuRepository.findById(horsJeu.getId()).get();
        // Disconnect from session so that the updates on updatedHorsJeu are not directly saved in db
        em.detach(updatedHorsJeu);
        updatedHorsJeu
            .code(UPDATED_CODE)
            .hashcode(UPDATED_HASHCODE)
            .instant(UPDATED_INSTANT)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .equipe(UPDATED_EQUIPE);
        HorsJeuDTO horsJeuDTO = horsJeuMapper.toDto(updatedHorsJeu);

        restHorsJeuMockMvc.perform(put("/api/hors-jeus")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(horsJeuDTO)))
            .andExpect(status().isOk());

        // Validate the HorsJeu in the database
        List<HorsJeu> horsJeuList = horsJeuRepository.findAll();
        assertThat(horsJeuList).hasSize(databaseSizeBeforeUpdate);
        HorsJeu testHorsJeu = horsJeuList.get(horsJeuList.size() - 1);
        assertThat(testHorsJeu.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testHorsJeu.getHashcode()).isEqualTo(UPDATED_HASHCODE);
        assertThat(testHorsJeu.getInstant()).isEqualTo(UPDATED_INSTANT);
        assertThat(testHorsJeu.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testHorsJeu.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testHorsJeu.getEquipe()).isEqualTo(UPDATED_EQUIPE);

        // Validate the HorsJeu in Elasticsearch
        verify(mockHorsJeuSearchRepository, times(1)).save(testHorsJeu);
    }

    @Test
    @Transactional
    public void updateNonExistingHorsJeu() throws Exception {
        int databaseSizeBeforeUpdate = horsJeuRepository.findAll().size();

        // Create the HorsJeu
        HorsJeuDTO horsJeuDTO = horsJeuMapper.toDto(horsJeu);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restHorsJeuMockMvc.perform(put("/api/hors-jeus")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(horsJeuDTO)))
            .andExpect(status().isBadRequest());

        // Validate the HorsJeu in the database
        List<HorsJeu> horsJeuList = horsJeuRepository.findAll();
        assertThat(horsJeuList).hasSize(databaseSizeBeforeUpdate);

        // Validate the HorsJeu in Elasticsearch
        verify(mockHorsJeuSearchRepository, times(0)).save(horsJeu);
    }

    @Test
    @Transactional
    public void deleteHorsJeu() throws Exception {
        // Initialize the database
        horsJeuRepository.saveAndFlush(horsJeu);

        int databaseSizeBeforeDelete = horsJeuRepository.findAll().size();

        // Delete the horsJeu
        restHorsJeuMockMvc.perform(delete("/api/hors-jeus/{id}", horsJeu.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<HorsJeu> horsJeuList = horsJeuRepository.findAll();
        assertThat(horsJeuList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the HorsJeu in Elasticsearch
        verify(mockHorsJeuSearchRepository, times(1)).deleteById(horsJeu.getId());
    }

    @Test
    @Transactional
    public void searchHorsJeu() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        horsJeuRepository.saveAndFlush(horsJeu);
        when(mockHorsJeuSearchRepository.search(queryStringQuery("id:" + horsJeu.getId())))
            .thenReturn(Collections.singletonList(horsJeu));

        // Search the horsJeu
        restHorsJeuMockMvc.perform(get("/api/_search/hors-jeus?query=id:" + horsJeu.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(horsJeu.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].hashcode").value(hasItem(DEFAULT_HASHCODE)))
            .andExpect(jsonPath("$.[*].instant").value(hasItem(DEFAULT_INSTANT)))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].equipe").value(hasItem(DEFAULT_EQUIPE.toString())));
    }
}
