package org.nddngroup.navetfoot.core.web.rest;

import org.nddngroup.navetfoot.core.NavetfootCore;
import org.nddngroup.navetfoot.core.domain.Competition;
import org.nddngroup.navetfoot.core.domain.CompetitionSaison;
import org.nddngroup.navetfoot.core.domain.Saison;
import org.nddngroup.navetfoot.core.repository.CompetitionSaisonRepository;
import org.nddngroup.navetfoot.core.repository.search.CompetitionSaisonSearchRepository;
import org.nddngroup.navetfoot.core.service.CompetitionSaisonService;
import org.nddngroup.navetfoot.core.service.dto.CompetitionSaisonDTO;
import org.nddngroup.navetfoot.core.service.mapper.CompetitionSaisonMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.nddngroup.navetfoot.core.repository.search.CompetitionSaisonSearchRepositoryMockConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static org.nddngroup.navetfoot.core.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CompetitionSaisonResource} REST controller.
 */
@SpringBootTest(classes = NavetfootCore.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class CompetitionSaisonResourceIT {

    private static final String DEFAULT_HASHCODE = "AAAAAAAAAA";
    private static final String UPDATED_HASHCODE = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Boolean DEFAULT_DELETED = false;
    private static final Boolean UPDATED_DELETED = true;

    @Autowired
    private CompetitionSaisonRepository competitionSaisonRepository;

    @Autowired
    private CompetitionSaisonMapper competitionSaisonMapper;

    @Autowired
    private CompetitionSaisonService competitionSaisonService;

    /**
     * This repository is mocked in the org.nddngroup.navetfoot.core.repository.search test package.
     *
     * @see CompetitionSaisonSearchRepositoryMockConfiguration
     */
    @Autowired
    private CompetitionSaisonSearchRepository mockCompetitionSaisonSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCompetitionSaisonMockMvc;

    private CompetitionSaison competitionSaison;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CompetitionSaison createEntity(EntityManager em) {
        CompetitionSaison competitionSaison = new CompetitionSaison()
            .hashcode(DEFAULT_HASHCODE)
            .code(DEFAULT_CODE)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT)
            .deleted(DEFAULT_DELETED);

        // add required entities
        Competition competition;
        if (TestUtil.findAll(em, Competition.class).isEmpty()) {
            competition = CompetitionResourceIT.createUpdatedEntity(em);
            em.persist(competition);
            em.flush();
        } else {
            competition = TestUtil.findAll(em, Competition.class).get(0);
        }
        competitionSaison.setCompetition(competition);

        Saison saison;
        if (TestUtil.findAll(em, Saison.class).isEmpty()) {
            saison = SaisonResourceIT.createUpdatedEntity(em);
            em.persist(saison);
            em.flush();
        } else {
            saison = TestUtil.findAll(em, Saison.class).get(0);
        }
        competitionSaison.setSaison(saison);
        return competitionSaison;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CompetitionSaison createUpdatedEntity(EntityManager em) {
        CompetitionSaison competitionSaison = new CompetitionSaison()
            .hashcode(UPDATED_HASHCODE)
            .code(UPDATED_CODE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .deleted(UPDATED_DELETED);
        return competitionSaison;
    }

    @BeforeEach
    public void initTest() {
        competitionSaison = createEntity(em);
    }

    @Test
    @Transactional
    public void createCompetitionSaison() throws Exception {
        int databaseSizeBeforeCreate = competitionSaisonRepository.findAll().size();
        // Create the CompetitionSaison
        CompetitionSaisonDTO competitionSaisonDTO = competitionSaisonMapper.toDto(competitionSaison);
        restCompetitionSaisonMockMvc.perform(post("/api/competition-saisons")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(competitionSaisonDTO)))
            .andExpect(status().isCreated());

        // Validate the CompetitionSaison in the database
        List<CompetitionSaison> competitionSaisonList = competitionSaisonRepository.findAll();
        assertThat(competitionSaisonList).hasSize(databaseSizeBeforeCreate + 1);
        CompetitionSaison testCompetitionSaison = competitionSaisonList.get(competitionSaisonList.size() - 1);
        assertThat(testCompetitionSaison.getHashcode()).isEqualTo(DEFAULT_HASHCODE);
        assertThat(testCompetitionSaison.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testCompetitionSaison.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testCompetitionSaison.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testCompetitionSaison.isDeleted()).isEqualTo(DEFAULT_DELETED);

        // Validate the CompetitionSaison in Elasticsearch
        verify(mockCompetitionSaisonSearchRepository, times(1)).save(testCompetitionSaison);
    }

    @Test
    @Transactional
    public void createCompetitionSaisonWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = competitionSaisonRepository.findAll().size();

        // Create the CompetitionSaison with an existing ID
        competitionSaison.setId(1L);
        CompetitionSaisonDTO competitionSaisonDTO = competitionSaisonMapper.toDto(competitionSaison);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCompetitionSaisonMockMvc.perform(post("/api/competition-saisons")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(competitionSaisonDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CompetitionSaison in the database
        List<CompetitionSaison> competitionSaisonList = competitionSaisonRepository.findAll();
        assertThat(competitionSaisonList).hasSize(databaseSizeBeforeCreate);

        // Validate the CompetitionSaison in Elasticsearch
        verify(mockCompetitionSaisonSearchRepository, times(0)).save(competitionSaison);
    }


    @Test
    @Transactional
    public void getAllCompetitionSaisons() throws Exception {
        // Initialize the database
        competitionSaisonRepository.saveAndFlush(competitionSaison);

        // Get all the competitionSaisonList
        restCompetitionSaisonMockMvc.perform(get("/api/competition-saisons?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(competitionSaison.getId().intValue())))
            .andExpect(jsonPath("$.[*].hashcode").value(hasItem(DEFAULT_HASHCODE)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())));
    }

    @Test
    @Transactional
    public void getCompetitionSaison() throws Exception {
        // Initialize the database
        competitionSaisonRepository.saveAndFlush(competitionSaison);

        // Get the competitionSaison
        restCompetitionSaisonMockMvc.perform(get("/api/competition-saisons/{id}", competitionSaison.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(competitionSaison.getId().intValue()))
            .andExpect(jsonPath("$.hashcode").value(DEFAULT_HASHCODE))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE))
            .andExpect(jsonPath("$.createdAt").value(sameInstant(DEFAULT_CREATED_AT)))
            .andExpect(jsonPath("$.updatedAt").value(sameInstant(DEFAULT_UPDATED_AT)))
            .andExpect(jsonPath("$.deleted").value(DEFAULT_DELETED.booleanValue()));
    }
    @Test
    @Transactional
    public void getNonExistingCompetitionSaison() throws Exception {
        // Get the competitionSaison
        restCompetitionSaisonMockMvc.perform(get("/api/competition-saisons/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCompetitionSaison() throws Exception {
        // Initialize the database
        competitionSaisonRepository.saveAndFlush(competitionSaison);

        int databaseSizeBeforeUpdate = competitionSaisonRepository.findAll().size();

        // Update the competitionSaison
        CompetitionSaison updatedCompetitionSaison = competitionSaisonRepository.findById(competitionSaison.getId()).get();
        // Disconnect from session so that the updates on updatedCompetitionSaison are not directly saved in db
        em.detach(updatedCompetitionSaison);
        updatedCompetitionSaison
            .hashcode(UPDATED_HASHCODE)
            .code(UPDATED_CODE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT);
        CompetitionSaisonDTO competitionSaisonDTO = competitionSaisonMapper.toDto(updatedCompetitionSaison);

        restCompetitionSaisonMockMvc.perform(put("/api/competition-saisons")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(competitionSaisonDTO)))
            .andExpect(status().isOk());

        // Validate the CompetitionSaison in the database
        List<CompetitionSaison> competitionSaisonList = competitionSaisonRepository.findAll();
        assertThat(competitionSaisonList).hasSize(databaseSizeBeforeUpdate);
        CompetitionSaison testCompetitionSaison = competitionSaisonList.get(competitionSaisonList.size() - 1);
        assertThat(testCompetitionSaison.getHashcode()).isEqualTo(UPDATED_HASHCODE);
        assertThat(testCompetitionSaison.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testCompetitionSaison.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testCompetitionSaison.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);

        // Validate the CompetitionSaison in Elasticsearch
        verify(mockCompetitionSaisonSearchRepository, times(1)).save(testCompetitionSaison);
    }

    @Test
    @Transactional
    public void updateNonExistingCompetitionSaison() throws Exception {
        int databaseSizeBeforeUpdate = competitionSaisonRepository.findAll().size();

        // Create the CompetitionSaison
        CompetitionSaisonDTO competitionSaisonDTO = competitionSaisonMapper.toDto(competitionSaison);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCompetitionSaisonMockMvc.perform(put("/api/competition-saisons")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(competitionSaisonDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CompetitionSaison in the database
        List<CompetitionSaison> competitionSaisonList = competitionSaisonRepository.findAll();
        assertThat(competitionSaisonList).hasSize(databaseSizeBeforeUpdate);

        // Validate the CompetitionSaison in Elasticsearch
        verify(mockCompetitionSaisonSearchRepository, times(0)).save(competitionSaison);
    }

    @Test
    @Transactional
    public void deleteCompetitionSaison() throws Exception {
        // Initialize the database
        competitionSaisonRepository.saveAndFlush(competitionSaison);

        int databaseSizeBeforeDelete = competitionSaisonRepository.findAll().size();

        // Delete the competitionSaison
        restCompetitionSaisonMockMvc.perform(delete("/api/competition-saisons/{id}", competitionSaison.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CompetitionSaison> competitionSaisonList = competitionSaisonRepository.findAll();
        assertThat(competitionSaisonList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the CompetitionSaison in Elasticsearch
        verify(mockCompetitionSaisonSearchRepository, times(1)).deleteById(competitionSaison.getId());
    }

    @Test
    @Transactional
    public void searchCompetitionSaison() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        competitionSaisonRepository.saveAndFlush(competitionSaison);
        when(mockCompetitionSaisonSearchRepository.search(queryStringQuery("id:" + competitionSaison.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(competitionSaison), PageRequest.of(0, 1), 1));

        // Search the competitionSaison
        restCompetitionSaisonMockMvc.perform(get("/api/_search/competition-saisons?query=id:" + competitionSaison.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(competitionSaison.getId().intValue())))
            .andExpect(jsonPath("$.[*].hashcode").value(hasItem(DEFAULT_HASHCODE)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())));
    }
}
