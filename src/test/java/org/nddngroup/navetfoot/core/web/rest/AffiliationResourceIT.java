package org.nddngroup.navetfoot.core.web.rest;

import org.nddngroup.navetfoot.core.NavetfootCore;
import org.nddngroup.navetfoot.core.domain.Affiliation;
import org.nddngroup.navetfoot.core.repository.AffiliationRepository;
import org.nddngroup.navetfoot.core.repository.search.AffiliationSearchRepository;
import org.nddngroup.navetfoot.core.service.AffiliationService;
import org.nddngroup.navetfoot.core.service.dto.AffiliationDTO;
import org.nddngroup.navetfoot.core.service.mapper.AffiliationMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.nddngroup.navetfoot.core.repository.search.AffiliationSearchRepositoryMockConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static org.nddngroup.navetfoot.core.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AffiliationResource} REST controller.
 */
@SpringBootTest(classes = NavetfootCore.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class AffiliationResourceIT {

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_HASHCODE = "AAAAAAAAAA";
    private static final String UPDATED_HASHCODE = "BBBBBBBBBB";

    private static final String DEFAULT_STATE = "AAAAAAAAAA";
    private static final String UPDATED_STATE = "BBBBBBBBBB";

    private static final Boolean DEFAULT_DELETED = false;
    private static final Boolean UPDATED_DELETED = true;

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private AffiliationRepository affiliationRepository;

    @Autowired
    private AffiliationMapper affiliationMapper;

    @Autowired
    private AffiliationService affiliationService;

    /**
     * This repository is mocked in the org.nddngroup.navetfoot.core.repository.search test package.
     *
     * @see AffiliationSearchRepositoryMockConfiguration
     */
    @Autowired
    private AffiliationSearchRepository mockAffiliationSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAffiliationMockMvc;

    private Affiliation affiliation;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Affiliation createEntity(EntityManager em) {
        Affiliation affiliation = new Affiliation()
            .code(DEFAULT_CODE)
            .hashcode(DEFAULT_HASHCODE)
            .state(DEFAULT_STATE)
            .deleted(DEFAULT_DELETED)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT);
        return affiliation;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Affiliation createUpdatedEntity(EntityManager em) {
        Affiliation affiliation = new Affiliation()
            .code(UPDATED_CODE)
            .hashcode(UPDATED_HASHCODE)
            .state(UPDATED_STATE)
            .deleted(UPDATED_DELETED)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT);
        return affiliation;
    }

    @BeforeEach
    public void initTest() {
        affiliation = createEntity(em);
    }

    @Test
    @Transactional
    public void createAffiliation() throws Exception {
        int databaseSizeBeforeCreate = affiliationRepository.findAll().size();
        // Create the Affiliation
        AffiliationDTO affiliationDTO = affiliationMapper.toDto(affiliation);
        restAffiliationMockMvc.perform(post("/api/affiliations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(affiliationDTO)))
            .andExpect(status().isCreated());

        // Validate the Affiliation in the database
        List<Affiliation> affiliationList = affiliationRepository.findAll();
        assertThat(affiliationList).hasSize(databaseSizeBeforeCreate + 1);
        Affiliation testAffiliation = affiliationList.get(affiliationList.size() - 1);
        assertThat(testAffiliation.getHashcode()).isEqualTo(DEFAULT_HASHCODE);
        assertThat(testAffiliation.getState()).isEqualTo(DEFAULT_STATE);
        assertThat(testAffiliation.isDeleted()).isEqualTo(DEFAULT_DELETED);
        assertThat(testAffiliation.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testAffiliation.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);

        // Validate the Affiliation in Elasticsearch
        verify(mockAffiliationSearchRepository, times(1)).save(testAffiliation);
    }

    @Test
    @Transactional
    public void createAffiliationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = affiliationRepository.findAll().size();

        // Create the Affiliation with an existing ID
        affiliation.setId(1L);
        AffiliationDTO affiliationDTO = affiliationMapper.toDto(affiliation);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAffiliationMockMvc.perform(post("/api/affiliations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(affiliationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Affiliation in the database
        List<Affiliation> affiliationList = affiliationRepository.findAll();
        assertThat(affiliationList).hasSize(databaseSizeBeforeCreate);

        // Validate the Affiliation in Elasticsearch
        verify(mockAffiliationSearchRepository, times(0)).save(affiliation);
    }


    @Test
    @Transactional
    public void getAllAffiliations() throws Exception {
        // Initialize the database
        affiliationRepository.saveAndFlush(affiliation);

        // Get all the affiliationList
        restAffiliationMockMvc.perform(get("/api/affiliations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(affiliation.getId().intValue())))
            .andExpect(jsonPath("$.[*].hashcode").value(hasItem(DEFAULT_HASHCODE)))
            .andExpect(jsonPath("$.[*].state").value(hasItem(DEFAULT_STATE)))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))));
    }

    @Test
    @Transactional
    public void getAffiliation() throws Exception {
        // Initialize the database
        affiliationRepository.saveAndFlush(affiliation);

        // Get the affiliation
        restAffiliationMockMvc.perform(get("/api/affiliations/{id}", affiliation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(affiliation.getId().intValue()))
            .andExpect(jsonPath("$.hashcode").value(DEFAULT_HASHCODE))
            .andExpect(jsonPath("$.state").value(DEFAULT_STATE))
            .andExpect(jsonPath("$.deleted").value(DEFAULT_DELETED.booleanValue()))
            .andExpect(jsonPath("$.createdAt").value(sameInstant(DEFAULT_CREATED_AT)))
            .andExpect(jsonPath("$.updatedAt").value(sameInstant(DEFAULT_UPDATED_AT)));
    }
    @Test
    @Transactional
    public void getNonExistingAffiliation() throws Exception {
        // Get the affiliation
        restAffiliationMockMvc.perform(get("/api/affiliations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAffiliation() throws Exception {
        // Initialize the database
        affiliationRepository.saveAndFlush(affiliation);

        int databaseSizeBeforeUpdate = affiliationRepository.findAll().size();

        // Update the affiliation
        Affiliation updatedAffiliation = affiliationRepository.findById(affiliation.getId()).get();
        // Disconnect from session so that the updates on updatedAffiliation are not directly saved in db
        em.detach(updatedAffiliation);
        updatedAffiliation
            .code(UPDATED_CODE)
            .hashcode(UPDATED_HASHCODE)
            .state(UPDATED_STATE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT);
        AffiliationDTO affiliationDTO = affiliationMapper.toDto(updatedAffiliation);

        restAffiliationMockMvc.perform(put("/api/affiliations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(affiliationDTO)))
            .andExpect(status().isOk());

        // Validate the Affiliation in the database
        List<Affiliation> affiliationList = affiliationRepository.findAll();
        assertThat(affiliationList).hasSize(databaseSizeBeforeUpdate);
        Affiliation testAffiliation = affiliationList.get(affiliationList.size() - 1);
        assertThat(testAffiliation.getHashcode()).isEqualTo(UPDATED_HASHCODE);
        assertThat(testAffiliation.getState()).isEqualTo(UPDATED_STATE);
        assertThat(testAffiliation.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testAffiliation.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);

        // Validate the Affiliation in Elasticsearch
        verify(mockAffiliationSearchRepository, times(1)).save(testAffiliation);
    }

    @Test
    @Transactional
    public void updateNonExistingAffiliation() throws Exception {
        int databaseSizeBeforeUpdate = affiliationRepository.findAll().size();

        // Create the Affiliation
        AffiliationDTO affiliationDTO = affiliationMapper.toDto(affiliation);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAffiliationMockMvc.perform(put("/api/affiliations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(affiliationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Affiliation in the database
        List<Affiliation> affiliationList = affiliationRepository.findAll();
        assertThat(affiliationList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Affiliation in Elasticsearch
        verify(mockAffiliationSearchRepository, times(0)).save(affiliation);
    }

    @Test
    @Transactional
    public void deleteAffiliation() throws Exception {
        // Initialize the database
        affiliationRepository.saveAndFlush(affiliation);

        int databaseSizeBeforeDelete = affiliationRepository.findAll().size();

        // Delete the affiliation
        restAffiliationMockMvc.perform(delete("/api/affiliations/{id}", affiliation.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Affiliation> affiliationList = affiliationRepository.findAll();
        assertThat(affiliationList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Affiliation in Elasticsearch
        verify(mockAffiliationSearchRepository, times(1)).deleteById(affiliation.getId());
    }

    @Test
    @Transactional
    public void searchAffiliation() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        affiliationRepository.saveAndFlush(affiliation);
        when(mockAffiliationSearchRepository.search(queryStringQuery("id:" + affiliation.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(affiliation), PageRequest.of(0, 1), 1));

        // Search the affiliation
        restAffiliationMockMvc.perform(get("/api/_search/affiliations?query=id:" + affiliation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(affiliation.getId().intValue())))
            .andExpect(jsonPath("$.[*].hashcode").value(hasItem(DEFAULT_HASHCODE)))
            .andExpect(jsonPath("$.[*].state").value(hasItem(DEFAULT_STATE)))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))));
    }
}
