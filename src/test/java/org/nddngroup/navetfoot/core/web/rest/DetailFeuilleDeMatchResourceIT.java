package org.nddngroup.navetfoot.core.web.rest;

import org.nddngroup.navetfoot.core.NavetfootCore;
import org.nddngroup.navetfoot.core.domain.DetailFeuilleDeMatch;
import org.nddngroup.navetfoot.core.repository.DetailFeuilleDeMatchRepository;
import org.nddngroup.navetfoot.core.repository.search.DetailFeuilleDeMatchSearchRepository;
import org.nddngroup.navetfoot.core.service.DetailFeuilleDeMatchService;
import org.nddngroup.navetfoot.core.service.dto.DetailFeuilleDeMatchDTO;
import org.nddngroup.navetfoot.core.service.mapper.DetailFeuilleDeMatchMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.nddngroup.navetfoot.core.repository.search.DetailFeuilleDeMatchSearchRepositoryMockConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static org.nddngroup.navetfoot.core.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;
import org.nddngroup.navetfoot.core.domain.enumeration.Position;
/**
 * Integration tests for the {@link DetailFeuilleDeMatchResource} REST controller.
 */
@SpringBootTest(classes = NavetfootCore.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class DetailFeuilleDeMatchResourceIT {

    private static final Equipe DEFAULT_EQUIPE = Equipe.LOCAL;
    private static final Equipe UPDATED_EQUIPE = Equipe.VISITEUR;

    private static final Integer DEFAULT_NUMERO_JOUEUR = 1;
    private static final Integer UPDATED_NUMERO_JOUEUR = 2;

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_HASHCODE = "AAAAAAAAAA";
    private static final String UPDATED_HASHCODE = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Boolean DEFAULT_DELETED = false;
    private static final Boolean UPDATED_DELETED = true;

    private static final Position DEFAULT_POSITION = Position.TITULAIRE;
    private static final Position UPDATED_POSITION = Position.SUPPLEANT;

    @Autowired
    private DetailFeuilleDeMatchRepository detailFeuilleDeMatchRepository;

    @Autowired
    private DetailFeuilleDeMatchMapper detailFeuilleDeMatchMapper;

    @Autowired
    private DetailFeuilleDeMatchService detailFeuilleDeMatchService;

    /**
     * This repository is mocked in the org.nddngroup.navetfoot.core.repository.search test package.
     *
     * @see DetailFeuilleDeMatchSearchRepositoryMockConfiguration
     */
    @Autowired
    private DetailFeuilleDeMatchSearchRepository mockDetailFeuilleDeMatchSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDetailFeuilleDeMatchMockMvc;

    private DetailFeuilleDeMatch detailFeuilleDeMatch;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DetailFeuilleDeMatch createEntity(EntityManager em) {
        DetailFeuilleDeMatch detailFeuilleDeMatch = new DetailFeuilleDeMatch()
            .equipe(DEFAULT_EQUIPE)
            .numeroJoueur(DEFAULT_NUMERO_JOUEUR)
            .code(DEFAULT_CODE)
            .hashcode(DEFAULT_HASHCODE)
            .updatedAt(DEFAULT_UPDATED_AT)
            .createdAt(DEFAULT_CREATED_AT)
            .deleted(DEFAULT_DELETED)
            .position(DEFAULT_POSITION);
        return detailFeuilleDeMatch;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DetailFeuilleDeMatch createUpdatedEntity(EntityManager em) {
        DetailFeuilleDeMatch detailFeuilleDeMatch = new DetailFeuilleDeMatch()
            .equipe(UPDATED_EQUIPE)
            .numeroJoueur(UPDATED_NUMERO_JOUEUR)
            .code(UPDATED_CODE)
            .hashcode(UPDATED_HASHCODE)
            .updatedAt(UPDATED_UPDATED_AT)
            .createdAt(UPDATED_CREATED_AT)
            .deleted(UPDATED_DELETED)
            .position(UPDATED_POSITION);
        return detailFeuilleDeMatch;
    }

    @BeforeEach
    public void initTest() {
        detailFeuilleDeMatch = createEntity(em);
    }

    @Test
    @Transactional
    public void createDetailFeuilleDeMatch() throws Exception {
        int databaseSizeBeforeCreate = detailFeuilleDeMatchRepository.findAll().size();
        // Create the DetailFeuilleDeMatch
        DetailFeuilleDeMatchDTO detailFeuilleDeMatchDTO = detailFeuilleDeMatchMapper.toDto(detailFeuilleDeMatch);
        restDetailFeuilleDeMatchMockMvc.perform(post("/api/detail-feuille-de-matches")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(detailFeuilleDeMatchDTO)))
            .andExpect(status().isCreated());

        // Validate the DetailFeuilleDeMatch in the database
        List<DetailFeuilleDeMatch> detailFeuilleDeMatchList = detailFeuilleDeMatchRepository.findAll();
        assertThat(detailFeuilleDeMatchList).hasSize(databaseSizeBeforeCreate + 1);
        DetailFeuilleDeMatch testDetailFeuilleDeMatch = detailFeuilleDeMatchList.get(detailFeuilleDeMatchList.size() - 1);
        assertThat(testDetailFeuilleDeMatch.getEquipe()).isEqualTo(DEFAULT_EQUIPE);
        assertThat(testDetailFeuilleDeMatch.getNumeroJoueur()).isEqualTo(DEFAULT_NUMERO_JOUEUR);
        assertThat(testDetailFeuilleDeMatch.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testDetailFeuilleDeMatch.getHashcode()).isEqualTo(DEFAULT_HASHCODE);
        assertThat(testDetailFeuilleDeMatch.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testDetailFeuilleDeMatch.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testDetailFeuilleDeMatch.isDeleted()).isEqualTo(DEFAULT_DELETED);
        assertThat(testDetailFeuilleDeMatch.getPosition()).isEqualTo(DEFAULT_POSITION);

        // Validate the DetailFeuilleDeMatch in Elasticsearch
        verify(mockDetailFeuilleDeMatchSearchRepository, times(1)).save(testDetailFeuilleDeMatch);
    }

    @Test
    @Transactional
    public void createDetailFeuilleDeMatchWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = detailFeuilleDeMatchRepository.findAll().size();

        // Create the DetailFeuilleDeMatch with an existing ID
        detailFeuilleDeMatch.setId(1L);
        DetailFeuilleDeMatchDTO detailFeuilleDeMatchDTO = detailFeuilleDeMatchMapper.toDto(detailFeuilleDeMatch);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDetailFeuilleDeMatchMockMvc.perform(post("/api/detail-feuille-de-matches")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(detailFeuilleDeMatchDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DetailFeuilleDeMatch in the database
        List<DetailFeuilleDeMatch> detailFeuilleDeMatchList = detailFeuilleDeMatchRepository.findAll();
        assertThat(detailFeuilleDeMatchList).hasSize(databaseSizeBeforeCreate);

        // Validate the DetailFeuilleDeMatch in Elasticsearch
        verify(mockDetailFeuilleDeMatchSearchRepository, times(0)).save(detailFeuilleDeMatch);
    }


    @Test
    @Transactional
    public void checkPositionIsRequired() throws Exception {
        int databaseSizeBeforeTest = detailFeuilleDeMatchRepository.findAll().size();
        // set the field null
        detailFeuilleDeMatch.setPosition(null);

        // Create the DetailFeuilleDeMatch, which fails.
        DetailFeuilleDeMatchDTO detailFeuilleDeMatchDTO = detailFeuilleDeMatchMapper.toDto(detailFeuilleDeMatch);


        restDetailFeuilleDeMatchMockMvc.perform(post("/api/detail-feuille-de-matches")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(detailFeuilleDeMatchDTO)))
            .andExpect(status().isBadRequest());

        List<DetailFeuilleDeMatch> detailFeuilleDeMatchList = detailFeuilleDeMatchRepository.findAll();
        assertThat(detailFeuilleDeMatchList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDetailFeuilleDeMatches() throws Exception {
        // Initialize the database
        detailFeuilleDeMatchRepository.saveAndFlush(detailFeuilleDeMatch);

        // Get all the detailFeuilleDeMatchList
        restDetailFeuilleDeMatchMockMvc.perform(get("/api/detail-feuille-de-matches?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(detailFeuilleDeMatch.getId().intValue())))
            .andExpect(jsonPath("$.[*].equipe").value(hasItem(DEFAULT_EQUIPE.toString())))
            .andExpect(jsonPath("$.[*].numeroJoueur").value(hasItem(DEFAULT_NUMERO_JOUEUR)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].hashcode").value(hasItem(DEFAULT_HASHCODE)))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].position").value(hasItem(DEFAULT_POSITION.toString())));
    }

    @Test
    @Transactional
    public void getDetailFeuilleDeMatch() throws Exception {
        // Initialize the database
        detailFeuilleDeMatchRepository.saveAndFlush(detailFeuilleDeMatch);

        // Get the detailFeuilleDeMatch
        restDetailFeuilleDeMatchMockMvc.perform(get("/api/detail-feuille-de-matches/{id}", detailFeuilleDeMatch.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(detailFeuilleDeMatch.getId().intValue()))
            .andExpect(jsonPath("$.equipe").value(DEFAULT_EQUIPE.toString()))
            .andExpect(jsonPath("$.numeroJoueur").value(DEFAULT_NUMERO_JOUEUR))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE))
            .andExpect(jsonPath("$.hashcode").value(DEFAULT_HASHCODE))
            .andExpect(jsonPath("$.updatedAt").value(sameInstant(DEFAULT_UPDATED_AT)))
            .andExpect(jsonPath("$.createdAt").value(sameInstant(DEFAULT_CREATED_AT)))
            .andExpect(jsonPath("$.deleted").value(DEFAULT_DELETED.booleanValue()))
            .andExpect(jsonPath("$.position").value(DEFAULT_POSITION.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingDetailFeuilleDeMatch() throws Exception {
        // Get the detailFeuilleDeMatch
        restDetailFeuilleDeMatchMockMvc.perform(get("/api/detail-feuille-de-matches/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDetailFeuilleDeMatch() throws Exception {
        // Initialize the database
        detailFeuilleDeMatchRepository.saveAndFlush(detailFeuilleDeMatch);

        int databaseSizeBeforeUpdate = detailFeuilleDeMatchRepository.findAll().size();

        // Update the detailFeuilleDeMatch
        DetailFeuilleDeMatch updatedDetailFeuilleDeMatch = detailFeuilleDeMatchRepository.findById(detailFeuilleDeMatch.getId()).get();
        // Disconnect from session so that the updates on updatedDetailFeuilleDeMatch are not directly saved in db
        em.detach(updatedDetailFeuilleDeMatch);
        updatedDetailFeuilleDeMatch
            .equipe(UPDATED_EQUIPE)
            .numeroJoueur(UPDATED_NUMERO_JOUEUR)
            .code(UPDATED_CODE)
            .hashcode(UPDATED_HASHCODE)
            .updatedAt(UPDATED_UPDATED_AT)
            .createdAt(UPDATED_CREATED_AT)
            .position(UPDATED_POSITION);
        DetailFeuilleDeMatchDTO detailFeuilleDeMatchDTO = detailFeuilleDeMatchMapper.toDto(updatedDetailFeuilleDeMatch);

        restDetailFeuilleDeMatchMockMvc.perform(put("/api/detail-feuille-de-matches")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(detailFeuilleDeMatchDTO)))
            .andExpect(status().isOk());

        // Validate the DetailFeuilleDeMatch in the database
        List<DetailFeuilleDeMatch> detailFeuilleDeMatchList = detailFeuilleDeMatchRepository.findAll();
        assertThat(detailFeuilleDeMatchList).hasSize(databaseSizeBeforeUpdate);
        DetailFeuilleDeMatch testDetailFeuilleDeMatch = detailFeuilleDeMatchList.get(detailFeuilleDeMatchList.size() - 1);
        assertThat(testDetailFeuilleDeMatch.getEquipe()).isEqualTo(UPDATED_EQUIPE);
        assertThat(testDetailFeuilleDeMatch.getNumeroJoueur()).isEqualTo(UPDATED_NUMERO_JOUEUR);
        assertThat(testDetailFeuilleDeMatch.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testDetailFeuilleDeMatch.getHashcode()).isEqualTo(UPDATED_HASHCODE);
        assertThat(testDetailFeuilleDeMatch.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testDetailFeuilleDeMatch.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testDetailFeuilleDeMatch.getPosition()).isEqualTo(UPDATED_POSITION);

        // Validate the DetailFeuilleDeMatch in Elasticsearch
        verify(mockDetailFeuilleDeMatchSearchRepository, times(1)).save(testDetailFeuilleDeMatch);
    }

    @Test
    @Transactional
    public void updateNonExistingDetailFeuilleDeMatch() throws Exception {
        int databaseSizeBeforeUpdate = detailFeuilleDeMatchRepository.findAll().size();

        // Create the DetailFeuilleDeMatch
        DetailFeuilleDeMatchDTO detailFeuilleDeMatchDTO = detailFeuilleDeMatchMapper.toDto(detailFeuilleDeMatch);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDetailFeuilleDeMatchMockMvc.perform(put("/api/detail-feuille-de-matches")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(detailFeuilleDeMatchDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DetailFeuilleDeMatch in the database
        List<DetailFeuilleDeMatch> detailFeuilleDeMatchList = detailFeuilleDeMatchRepository.findAll();
        assertThat(detailFeuilleDeMatchList).hasSize(databaseSizeBeforeUpdate);

        // Validate the DetailFeuilleDeMatch in Elasticsearch
        verify(mockDetailFeuilleDeMatchSearchRepository, times(0)).save(detailFeuilleDeMatch);
    }

    @Test
    @Transactional
    public void deleteDetailFeuilleDeMatch() throws Exception {
        // Initialize the database
        detailFeuilleDeMatchRepository.saveAndFlush(detailFeuilleDeMatch);

        int databaseSizeBeforeDelete = detailFeuilleDeMatchRepository.findAll().size();

        // Delete the detailFeuilleDeMatch
        restDetailFeuilleDeMatchMockMvc.perform(delete("/api/detail-feuille-de-matches/{id}", detailFeuilleDeMatch.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DetailFeuilleDeMatch> detailFeuilleDeMatchList = detailFeuilleDeMatchRepository.findAll();
        assertThat(detailFeuilleDeMatchList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the DetailFeuilleDeMatch in Elasticsearch
        verify(mockDetailFeuilleDeMatchSearchRepository, times(1)).deleteById(detailFeuilleDeMatch.getId());
    }

    @Test
    @Transactional
    public void searchDetailFeuilleDeMatch() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        detailFeuilleDeMatchRepository.saveAndFlush(detailFeuilleDeMatch);
        when(mockDetailFeuilleDeMatchSearchRepository.search(queryStringQuery("id:" + detailFeuilleDeMatch.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(detailFeuilleDeMatch), PageRequest.of(0, 1), 1));

        // Search the detailFeuilleDeMatch
        restDetailFeuilleDeMatchMockMvc.perform(get("/api/_search/detail-feuille-de-matches?query=id:" + detailFeuilleDeMatch.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(detailFeuilleDeMatch.getId().intValue())))
            .andExpect(jsonPath("$.[*].equipe").value(hasItem(DEFAULT_EQUIPE.toString())))
            .andExpect(jsonPath("$.[*].numeroJoueur").value(hasItem(DEFAULT_NUMERO_JOUEUR)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].hashcode").value(hasItem(DEFAULT_HASHCODE)))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].position").value(hasItem(DEFAULT_POSITION.toString())));
    }
}
