package org.nddngroup.navetfoot.core.web.rest;

import org.nddngroup.navetfoot.core.NavetfootCore;
import org.nddngroup.navetfoot.core.domain.Tir;
import org.nddngroup.navetfoot.core.domain.Match;
import org.nddngroup.navetfoot.core.domain.Joueur;
import org.nddngroup.navetfoot.core.repository.TirRepository;
import org.nddngroup.navetfoot.core.repository.search.TirSearchRepository;
import org.nddngroup.navetfoot.core.service.TirService;
import org.nddngroup.navetfoot.core.service.dto.TirDTO;
import org.nddngroup.navetfoot.core.service.mapper.TirMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.nddngroup.navetfoot.core.repository.search.TirSearchRepositoryMockConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static org.nddngroup.navetfoot.core.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;
/**
 * Integration tests for the {@link TirResource} REST controller.
 */
@SpringBootTest(classes = NavetfootCore.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class TirResourceIT {

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_HASHCODE = "AAAAAAAAAA";
    private static final String UPDATED_HASHCODE = "BBBBBBBBBB";

    private static final Integer DEFAULT_INSTANT = 1;
    private static final Integer UPDATED_INSTANT = 2;

    private static final Boolean DEFAULT_DELETED = false;
    private static final Boolean UPDATED_DELETED = true;

    private static final Boolean DEFAULT_CADRE = false;
    private static final Boolean UPDATED_CADRE = true;

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Equipe DEFAULT_EQUIPE = Equipe.LOCAL;
    private static final Equipe UPDATED_EQUIPE = Equipe.VISITEUR;

    @Autowired
    private TirRepository tirRepository;

    @Autowired
    private TirMapper tirMapper;

    @Autowired
    private TirService tirService;

    /**
     * This repository is mocked in the org.nddngroup.navetfoot.core.repository.search test package.
     *
     * @see TirSearchRepositoryMockConfiguration
     */
    @Autowired
    private TirSearchRepository mockTirSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restTirMockMvc;

    private Tir tir;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Tir createEntity(EntityManager em) {
        Tir tir = new Tir()
            .code(DEFAULT_CODE)
            .hashcode(DEFAULT_HASHCODE)
            .instant(DEFAULT_INSTANT)
            .deleted(DEFAULT_DELETED)
            .cadre(DEFAULT_CADRE)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT)
            .equipe(DEFAULT_EQUIPE);
        // Add required entity
        Match match;
        if (TestUtil.findAll(em, Match.class).isEmpty()) {
            match = MatchResourceIT.createEntity(em);
            em.persist(match);
            em.flush();
        } else {
            match = TestUtil.findAll(em, Match.class).get(0);
        }
        tir.setMatch(match);
        // Add required entity
        Joueur joueur;
        if (TestUtil.findAll(em, Joueur.class).isEmpty()) {
            joueur = JoueurResourceIT.createEntity(em);
            em.persist(joueur);
            em.flush();
        } else {
            joueur = TestUtil.findAll(em, Joueur.class).get(0);
        }
        tir.setJoueur(joueur);
        return tir;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Tir createUpdatedEntity(EntityManager em) {
        Tir tir = new Tir()
            .code(UPDATED_CODE)
            .hashcode(UPDATED_HASHCODE)
            .instant(UPDATED_INSTANT)
            .deleted(UPDATED_DELETED)
            .cadre(UPDATED_CADRE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .equipe(UPDATED_EQUIPE);
        // Add required entity
        Match match;
        if (TestUtil.findAll(em, Match.class).isEmpty()) {
            match = MatchResourceIT.createUpdatedEntity(em);
            em.persist(match);
            em.flush();
        } else {
            match = TestUtil.findAll(em, Match.class).get(0);
        }
        tir.setMatch(match);
        // Add required entity
        Joueur joueur;
        if (TestUtil.findAll(em, Joueur.class).isEmpty()) {
            joueur = JoueurResourceIT.createUpdatedEntity(em);
            em.persist(joueur);
            em.flush();
        } else {
            joueur = TestUtil.findAll(em, Joueur.class).get(0);
        }
        tir.setJoueur(joueur);
        return tir;
    }

    @BeforeEach
    public void initTest() {
        tir = createEntity(em);
    }

    @Test
    @Transactional
    public void createTir() throws Exception {
        int databaseSizeBeforeCreate = tirRepository.findAll().size();
        // Create the Tir
        TirDTO tirDTO = tirMapper.toDto(tir);
        restTirMockMvc.perform(post("/api/tirs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tirDTO)))
            .andExpect(status().isCreated());

        // Validate the Tir in the database
        List<Tir> tirList = tirRepository.findAll();
        assertThat(tirList).hasSize(databaseSizeBeforeCreate + 1);
        Tir testTir = tirList.get(tirList.size() - 1);
        assertThat(testTir.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testTir.getHashcode()).isEqualTo(DEFAULT_HASHCODE);
        assertThat(testTir.getInstant()).isEqualTo(DEFAULT_INSTANT);
        assertThat(testTir.isDeleted()).isEqualTo(DEFAULT_DELETED);
        assertThat(testTir.isCadre()).isEqualTo(DEFAULT_CADRE);
        assertThat(testTir.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testTir.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testTir.getEquipe()).isEqualTo(DEFAULT_EQUIPE);

        // Validate the Tir in Elasticsearch
        verify(mockTirSearchRepository, times(1)).save(testTir);
    }

    @Test
    @Transactional
    public void createTirWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tirRepository.findAll().size();

        // Create the Tir with an existing ID
        tir.setId(1L);
        TirDTO tirDTO = tirMapper.toDto(tir);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTirMockMvc.perform(post("/api/tirs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tirDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Tir in the database
        List<Tir> tirList = tirRepository.findAll();
        assertThat(tirList).hasSize(databaseSizeBeforeCreate);

        // Validate the Tir in Elasticsearch
        verify(mockTirSearchRepository, times(0)).save(tir);
    }


    @Test
    @Transactional
    public void checkCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = tirRepository.findAll().size();
        // set the field null
        tir.setCode(null);

        // Create the Tir, which fails.
        TirDTO tirDTO = tirMapper.toDto(tir);


        restTirMockMvc.perform(post("/api/tirs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tirDTO)))
            .andExpect(status().isBadRequest());

        List<Tir> tirList = tirRepository.findAll();
        assertThat(tirList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkHashcodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = tirRepository.findAll().size();
        // set the field null
        tir.setHashcode(null);

        // Create the Tir, which fails.
        TirDTO tirDTO = tirMapper.toDto(tir);


        restTirMockMvc.perform(post("/api/tirs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tirDTO)))
            .andExpect(status().isBadRequest());

        List<Tir> tirList = tirRepository.findAll();
        assertThat(tirList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTirs() throws Exception {
        // Initialize the database
        tirRepository.saveAndFlush(tir);

        // Get all the tirList
        restTirMockMvc.perform(get("/api/tirs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tir.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].hashcode").value(hasItem(DEFAULT_HASHCODE)))
            .andExpect(jsonPath("$.[*].instant").value(hasItem(DEFAULT_INSTANT)))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].cadre").value(hasItem(DEFAULT_CADRE.booleanValue())))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].equipe").value(hasItem(DEFAULT_EQUIPE.toString())));
    }

    @Test
    @Transactional
    public void getTir() throws Exception {
        // Initialize the database
        tirRepository.saveAndFlush(tir);

        // Get the tir
        restTirMockMvc.perform(get("/api/tirs/{id}", tir.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(tir.getId().intValue()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE))
            .andExpect(jsonPath("$.hashcode").value(DEFAULT_HASHCODE))
            .andExpect(jsonPath("$.instant").value(DEFAULT_INSTANT))
            .andExpect(jsonPath("$.deleted").value(DEFAULT_DELETED.booleanValue()))
            .andExpect(jsonPath("$.cadre").value(DEFAULT_CADRE.booleanValue()))
            .andExpect(jsonPath("$.createdAt").value(sameInstant(DEFAULT_CREATED_AT)))
            .andExpect(jsonPath("$.updatedAt").value(sameInstant(DEFAULT_UPDATED_AT)))
            .andExpect(jsonPath("$.equipe").value(DEFAULT_EQUIPE.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingTir() throws Exception {
        // Get the tir
        restTirMockMvc.perform(get("/api/tirs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTir() throws Exception {
        // Initialize the database
        tirRepository.saveAndFlush(tir);

        int databaseSizeBeforeUpdate = tirRepository.findAll().size();

        // Update the tir
        Tir updatedTir = tirRepository.findById(tir.getId()).get();
        // Disconnect from session so that the updates on updatedTir are not directly saved in db
        em.detach(updatedTir);
        updatedTir
            .code(UPDATED_CODE)
            .hashcode(UPDATED_HASHCODE)
            .instant(UPDATED_INSTANT)
            .cadre(UPDATED_CADRE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .equipe(UPDATED_EQUIPE);
        TirDTO tirDTO = tirMapper.toDto(updatedTir);

        restTirMockMvc.perform(put("/api/tirs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tirDTO)))
            .andExpect(status().isOk());

        // Validate the Tir in the database
        List<Tir> tirList = tirRepository.findAll();
        assertThat(tirList).hasSize(databaseSizeBeforeUpdate);
        Tir testTir = tirList.get(tirList.size() - 1);
        assertThat(testTir.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testTir.getHashcode()).isEqualTo(UPDATED_HASHCODE);
        assertThat(testTir.getInstant()).isEqualTo(UPDATED_INSTANT);
        assertThat(testTir.isCadre()).isEqualTo(UPDATED_CADRE);
        assertThat(testTir.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testTir.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testTir.getEquipe()).isEqualTo(UPDATED_EQUIPE);

        // Validate the Tir in Elasticsearch
        verify(mockTirSearchRepository, times(1)).save(testTir);
    }

    @Test
    @Transactional
    public void updateNonExistingTir() throws Exception {
        int databaseSizeBeforeUpdate = tirRepository.findAll().size();

        // Create the Tir
        TirDTO tirDTO = tirMapper.toDto(tir);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTirMockMvc.perform(put("/api/tirs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tirDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Tir in the database
        List<Tir> tirList = tirRepository.findAll();
        assertThat(tirList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Tir in Elasticsearch
        verify(mockTirSearchRepository, times(0)).save(tir);
    }

    @Test
    @Transactional
    public void deleteTir() throws Exception {
        // Initialize the database
        tirRepository.saveAndFlush(tir);

        int databaseSizeBeforeDelete = tirRepository.findAll().size();

        // Delete the tir
        restTirMockMvc.perform(delete("/api/tirs/{id}", tir.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Tir> tirList = tirRepository.findAll();
        assertThat(tirList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Tir in Elasticsearch
        verify(mockTirSearchRepository, times(1)).deleteById(tir.getId());
    }

    @Test
    @Transactional
    public void searchTir() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        tirRepository.saveAndFlush(tir);
        when(mockTirSearchRepository.search(queryStringQuery("id:" + tir.getId())))
            .thenReturn(Collections.singletonList(tir));

        // Search the tir
        restTirMockMvc.perform(get("/api/_search/tirs?query=id:" + tir.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tir.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].hashcode").value(hasItem(DEFAULT_HASHCODE)))
            .andExpect(jsonPath("$.[*].instant").value(hasItem(DEFAULT_INSTANT)))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].cadre").value(hasItem(DEFAULT_CADRE.booleanValue())))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].equipe").value(hasItem(DEFAULT_EQUIPE.toString())));
    }
}
