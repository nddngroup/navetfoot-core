package org.nddngroup.navetfoot.core.web.rest.custom;

import org.nddngroup.navetfoot.core.NavetfootCore;
import org.nddngroup.navetfoot.core.domain.*;
import org.nddngroup.navetfoot.core.repository.*;
import org.nddngroup.navetfoot.core.domain.*;
import org.nddngroup.navetfoot.core.repository.*;
import org.nddngroup.navetfoot.core.repository.search.CompetitionSaisonSearchRepository;
import org.nddngroup.navetfoot.core.repository.search.ContratSearchRepository;
import org.nddngroup.navetfoot.core.repository.search.SaisonSearchRepository;
import org.nddngroup.navetfoot.core.service.SaisonService;
import org.nddngroup.navetfoot.core.service.mapper.SaisonMapper;
import org.nddngroup.navetfoot.core.web.rest.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.nddngroup.navetfoot.core.repository.search.SaisonSearchRepositoryMockConfiguration;
import org.nddngroup.navetfoot.core.web.rest.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.*;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Integration tests for the {@link SaisonResource} REST controller.
 */
@SpringBootTest(classes = NavetfootCore.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class CustomSaisonResourceIT {

    private static final String DEFAULT_LIBELLE = "AAAAAAAAAA";
    private static final String UPDATED_LIBELLE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATE_DEBUT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_DEBUT = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_DATE_FIN = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_FIN = LocalDate.now(ZoneId.systemDefault());

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Boolean DEFAULT_DELETED = false;
    private static final Boolean UPDATED_DELETED = true;

    private static final String DEFAULT_HASHCODE = "AAAAAAAAAA";
    private static final String UPDATED_HASHCODE = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    @Autowired
    private SaisonRepository saisonRepository;
    @Autowired
    private ContratRepository contratRepository;
    @Autowired
    private AssociationRepository associationRepository;
    @Autowired
    private JoueurRepository joueurRepository;
    @Autowired
    private CompetitionRepository competitionRepository;

    @Autowired
    private CompetitionSaisonRepository competitionSaisonRepository;

    @Autowired
    private SaisonMapper saisonMapper;

    @Autowired
    private SaisonService saisonService;

    /**
     * This repository is mocked in the org.nddngroup.navetfoot.core.repository.search test package.
     *
     * @see SaisonSearchRepositoryMockConfiguration
     */
    @Autowired
    private SaisonSearchRepository mockSaisonSearchRepository;
    @Autowired
    private CompetitionSaisonSearchRepository mockCompetitionSaisonSearchRepository;
    @Autowired
    private ContratSearchRepository mockContratSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restSaisonMockMvc;

    private Saison saison;

    /**
     * Create an entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Saison createEntity(EntityManager em) {
        Saison saison = new Saison()
            .libelle(DEFAULT_LIBELLE)
            .dateDebut(DEFAULT_DATE_DEBUT)
            .dateFin(DEFAULT_DATE_FIN)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT)
            .deleted(DEFAULT_DELETED)
            .hashcode(DEFAULT_HASHCODE)
            .code(DEFAULT_CODE);

        // Add required entity
        Organisation organisation;
        if (TestUtil.findAll(em, Organisation.class).isEmpty()) {
            organisation = OrganisationResourceIT.createEntity(em);
            em.persist(organisation);
            em.flush();
        } else {
            organisation = TestUtil.findAll(em, Organisation.class).get(0);
        }
        saison.setOrganisation(organisation);
        return saison;
    }

    /**
     * Create an updated entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Saison createUpdatedEntity(EntityManager em) {
        Saison saison = new Saison()
            .libelle(UPDATED_LIBELLE)
            .dateDebut(UPDATED_DATE_DEBUT)
            .dateFin(UPDATED_DATE_FIN)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .deleted(UPDATED_DELETED)
            .hashcode(UPDATED_HASHCODE)
            .code(UPDATED_CODE);
        // Add required entity
        Organisation organisation;
        if (TestUtil.findAll(em, Organisation.class).isEmpty()) {
            organisation = OrganisationResourceIT.createUpdatedEntity(em);
            em.persist(organisation);
            em.flush();
        } else {
            organisation = TestUtil.findAll(em, Organisation.class).get(0);
        }
        saison.setOrganisation(organisation);
        return saison;
    }

    @BeforeEach
    public void initTest() {
        saison = createEntity(em);
    }

    @Test
    @Transactional
    public void deleteSaison() throws Exception {
        // Initialize the database
        saisonRepository.saveAndFlush(saison);

        // create competiton
        Competition competition = CompetitionResourceIT.createEntity(em);
        competitionRepository.saveAndFlush(competition);

        // create competitionSaison
        CompetitionSaison competitionSaison = CompetitionSaisonResourceIT.createEntity(em);
        competitionSaison.setSaison(saison);
        competitionSaison.setCompetition(competition);
        competitionSaisonRepository.saveAndFlush(competitionSaison);

        // create association
        Association association = AssociationResourceIT.createEntity(em);
        associationRepository.saveAndFlush(association);

        //create joueur
        Joueur joueur = JoueurResourceIT.createEntity(em);
        joueurRepository.saveAndFlush(joueur);

        //create contrat
        Contrat contrat = ContratResourceIT.createEntity(em);
        contrat.setSaison(saison);
        contrat.setAssociation(association);
        contrat.setJoueur(joueur);
        contratRepository.saveAndFlush(contrat);

        int databaseSaisonSizeBeforeDelete = saisonRepository.findAll().size();
        int databaseContratSizeBeforeDelete = contratRepository.findAll().size();

        // Delete the saison
        restSaisonMockMvc.perform(delete("/api/organisations/{organisationId}/saisons/{id}", saison.getOrganisation().getId(), saison.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());

        // Validate the database contains saison with delete equals true
        List<Saison> saisonList = saisonRepository.findAllByDeletedIsFalse();
        assertThat(saisonList).hasSize(databaseSaisonSizeBeforeDelete - 1);

        // Validate the database contains competitionSaison with delete equals true
        assertThat(competitionSaisonRepository.findAllBySaisonAndDeletedIsFalse(saison)).isEmpty();

        // Validate the database contains competitionSaison with delete equals true
        List<Contrat> contratList = contratRepository.findAllBySaisonAndDeletedIsFalse(saison);
        assertThat(contratRepository.findAllBySaisonAndDeletedIsFalse(saison)).isEmpty();

        // Validate the Saison in Elasticsearch
        verify(mockSaisonSearchRepository, times(1)).deleteById(saison.getId());

        // Validate the CompetitionSaison in Elasticsearch
        verify(mockCompetitionSaisonSearchRepository, times(1)).deleteById(competitionSaison.getId());

        // Validate the Contrat in Elasticsearch
        verify(mockContratSearchRepository, times(1)).deleteById(contrat.getId());
    }
}
