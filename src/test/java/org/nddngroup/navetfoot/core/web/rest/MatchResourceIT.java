package org.nddngroup.navetfoot.core.web.rest;

import org.nddngroup.navetfoot.core.NavetfootCore;
import org.nddngroup.navetfoot.core.domain.Association;
import org.nddngroup.navetfoot.core.domain.EtapeCompetition;
import org.nddngroup.navetfoot.core.domain.Match;
import org.nddngroup.navetfoot.core.repository.MatchRepository;
import org.nddngroup.navetfoot.core.repository.search.MatchSearchRepository;
import org.nddngroup.navetfoot.core.service.MatchService;
import org.nddngroup.navetfoot.core.service.dto.MatchDTO;
import org.nddngroup.navetfoot.core.service.mapper.MatchMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.nddngroup.navetfoot.core.repository.search.MatchSearchRepositoryMockConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static org.nddngroup.navetfoot.core.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.nddngroup.navetfoot.core.domain.enumeration.EtatMatch;
/**
 * Integration tests for the {@link MatchResource} REST controller.
 */
@SpringBootTest(classes = NavetfootCore.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class MatchResourceIT {

    private static final String DEFAULT_COUP_D_ENVOI = "AAAAAAAAAA";
    private static final String UPDATED_COUP_D_ENVOI = "BBBBBBBBBB";

    private static final String DEFAULT_HASHCODE = "AAAAAAAAAA";
    private static final String UPDATED_HASHCODE = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Boolean DEFAULT_DELETED = false;
    private static final Boolean UPDATED_DELETED = true;

    private static final LocalDate DEFAULT_DATE_MATCH = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_MATCH = LocalDate.now(ZoneId.systemDefault());

    private static final EtatMatch DEFAULT_ETAT_MATCH = EtatMatch.PROGRAMME;
    private static final EtatMatch UPDATED_ETAT_MATCH = EtatMatch.EN_COURS_1;

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final Integer DEFAULT_JOURNEE = 1;
    private static final Integer UPDATED_JOURNEE = 2;

    private static final String DEFAULT_CHRONO = "AAAAAAAAAA";
    private static final String UPDATED_CHRONO = "BBBBBBBBBB";

    @Autowired
    private MatchRepository matchRepository;

    @Autowired
    private MatchMapper matchMapper;

    @Autowired
    private MatchService matchService;

    /**
     * This repository is mocked in the org.nddngroup.navetfoot.core.repository.search test package.
     *
     * @see MatchSearchRepositoryMockConfiguration
     */
    @Autowired
    private MatchSearchRepository mockMatchSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMatchMockMvc;

    private Match match;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Match createEntity(EntityManager em) {
        Match match = new Match()
            .coupDEnvoi(DEFAULT_COUP_D_ENVOI)
            .hashcode(DEFAULT_HASHCODE)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT)
            .deleted(DEFAULT_DELETED)
            .dateMatch(DEFAULT_DATE_MATCH)
            .etatMatch(DEFAULT_ETAT_MATCH)
            .code(DEFAULT_CODE)
            .journee(DEFAULT_JOURNEE)
            .chrono(DEFAULT_CHRONO);
        // Add required entities
        Association local;
        if (TestUtil.findAll(em, Association.class).isEmpty()) {
            local = AssociationResourceIT.createUpdatedEntity(em);
            em.persist(local);
            em.flush();
        } else {
            local = TestUtil.findAll(em, Association.class).get(0);
        }
        match.setLocal(local);

        Association visiteur;
        if (TestUtil.findAll(em, Association.class).isEmpty()) {
            visiteur = AssociationResourceIT.createUpdatedEntity(em);
            em.persist(visiteur);
            em.flush();
        } else {
            visiteur = TestUtil.findAll(em, Association.class).get(0);
        }
        match.setVisiteur(visiteur);

        EtapeCompetition etapeCompetition;
        if (TestUtil.findAll(em, EtapeCompetition.class).isEmpty()) {
            etapeCompetition = EtapeCompetitionResourceIT.createUpdatedEntity(em);
            em.persist(etapeCompetition);
            em.flush();
        } else {
            etapeCompetition = TestUtil.findAll(em, EtapeCompetition.class).get(0);
        }
        match.setEtapeCompetition(etapeCompetition);

        return match;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Match createUpdatedEntity(EntityManager em) {
        Match match = new Match()
            .coupDEnvoi(UPDATED_COUP_D_ENVOI)
            .hashcode(UPDATED_HASHCODE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .dateMatch(UPDATED_DATE_MATCH)
            .etatMatch(UPDATED_ETAT_MATCH)
            .code(UPDATED_CODE)
            .journee(UPDATED_JOURNEE)
            .chrono(UPDATED_CHRONO);
        return match;
    }

    @BeforeEach
    public void initTest() {
        match = createEntity(em);
    }

    @Test
    @Transactional
    public void createMatch() throws Exception {
        int databaseSizeBeforeCreate = matchRepository.findAll().size();
        // Create the Match
        MatchDTO matchDTO = matchMapper.toDto(match);
        restMatchMockMvc.perform(post("/api/matches")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(matchDTO)))
            .andExpect(status().isCreated());

        // Validate the Match in the database
        List<Match> matchList = matchRepository.findAll();
        assertThat(matchList).hasSize(databaseSizeBeforeCreate + 1);
        Match testMatch = matchList.get(matchList.size() - 1);
        assertThat(testMatch.getCoupDEnvoi()).isEqualTo(DEFAULT_COUP_D_ENVOI);
        assertThat(testMatch.getHashcode()).isEqualTo(DEFAULT_HASHCODE);
        assertThat(testMatch.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testMatch.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testMatch.getDateMatch()).isEqualTo(DEFAULT_DATE_MATCH);
        assertThat(testMatch.getEtatMatch()).isEqualTo(DEFAULT_ETAT_MATCH);
        assertThat(testMatch.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testMatch.getJournee()).isEqualTo(DEFAULT_JOURNEE);
        assertThat(testMatch.getChrono()).isEqualTo(DEFAULT_CHRONO);

        // Validate the Match in Elasticsearch
        verify(mockMatchSearchRepository, times(1)).save(testMatch);
    }

    @Test
    @Transactional
    public void createMatchWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = matchRepository.findAll().size();

        // Create the Match with an existing ID
        match.setId(1L);
        MatchDTO matchDTO = matchMapper.toDto(match);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMatchMockMvc.perform(post("/api/matches")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(matchDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Match in the database
        List<Match> matchList = matchRepository.findAll();
        assertThat(matchList).hasSize(databaseSizeBeforeCreate);

        // Validate the Match in Elasticsearch
        verify(mockMatchSearchRepository, times(0)).save(match);
    }


    @Test
    @Transactional
    public void checkCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = matchRepository.findAll().size();
        // set the field null
        match.setCode(null);

        // Create the Match, which fails.
        MatchDTO matchDTO = matchMapper.toDto(match);


        restMatchMockMvc.perform(post("/api/matches")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(matchDTO)))
            .andExpect(status().isBadRequest());

        List<Match> matchList = matchRepository.findAll();
        assertThat(matchList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllMatches() throws Exception {
        // Initialize the database
        matchRepository.saveAndFlush(match);

        // Get all the matchList
        restMatchMockMvc.perform(get("/api/matches?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(match.getId().intValue())))
            .andExpect(jsonPath("$.[*].coupDEnvoi").value(hasItem(DEFAULT_COUP_D_ENVOI)))
            .andExpect(jsonPath("$.[*].hashcode").value(hasItem(DEFAULT_HASHCODE)))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].dateMatch").value(hasItem(DEFAULT_DATE_MATCH.toString())))
            .andExpect(jsonPath("$.[*].etatMatch").value(hasItem(DEFAULT_ETAT_MATCH.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].journee").value(hasItem(DEFAULT_JOURNEE)))
            .andExpect(jsonPath("$.[*].chrono").value(hasItem(DEFAULT_CHRONO)));
    }

    @Test
    @Transactional
    public void getMatch() throws Exception {
        // Initialize the database
        matchRepository.saveAndFlush(match);

        // Get the match
        restMatchMockMvc.perform(get("/api/matches/{id}", match.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(match.getId().intValue()))
            .andExpect(jsonPath("$.coupDEnvoi").value(DEFAULT_COUP_D_ENVOI))
            .andExpect(jsonPath("$.hashcode").value(DEFAULT_HASHCODE))
            .andExpect(jsonPath("$.createdAt").value(sameInstant(DEFAULT_CREATED_AT)))
            .andExpect(jsonPath("$.updatedAt").value(sameInstant(DEFAULT_UPDATED_AT)))
            .andExpect(jsonPath("$.deleted").value(DEFAULT_DELETED.booleanValue()))
            .andExpect(jsonPath("$.dateMatch").value(DEFAULT_DATE_MATCH.toString()))
            .andExpect(jsonPath("$.etatMatch").value(DEFAULT_ETAT_MATCH.toString()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE))
            .andExpect(jsonPath("$.journee").value(DEFAULT_JOURNEE))
            .andExpect(jsonPath("$.chrono").value(DEFAULT_CHRONO));
    }
    @Test
    @Transactional
    public void getNonExistingMatch() throws Exception {
        // Get the match
        restMatchMockMvc.perform(get("/api/matches/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMatch() throws Exception {
        // Initialize the database
        matchRepository.saveAndFlush(match);

        int databaseSizeBeforeUpdate = matchRepository.findAll().size();

        // Update the match
        Match updatedMatch = matchRepository.findById(match.getId()).get();
        // Disconnect from session so that the updates on updatedMatch are not directly saved in db
        em.detach(updatedMatch);
        updatedMatch
            .coupDEnvoi(UPDATED_COUP_D_ENVOI)
            .hashcode(UPDATED_HASHCODE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .dateMatch(UPDATED_DATE_MATCH)
            .etatMatch(UPDATED_ETAT_MATCH)
            .code(UPDATED_CODE)
            .journee(UPDATED_JOURNEE)
            .chrono(UPDATED_CHRONO);
        MatchDTO matchDTO = matchMapper.toDto(updatedMatch);

        restMatchMockMvc.perform(put("/api/matches")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(matchDTO)))

            .andExpect(status().isOk());

        // Validate the Match in the database
        List<Match> matchList = matchRepository.findAll();
        assertThat(matchList).hasSize(databaseSizeBeforeUpdate);
        Match testMatch = matchList.get(matchList.size() - 1);
        assertThat(testMatch.getCoupDEnvoi()).isEqualTo(UPDATED_COUP_D_ENVOI);
        assertThat(testMatch.getHashcode()).isEqualTo(UPDATED_HASHCODE);
        assertThat(testMatch.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testMatch.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testMatch.getDateMatch()).isEqualTo(UPDATED_DATE_MATCH);
        assertThat(testMatch.getEtatMatch()).isEqualTo(UPDATED_ETAT_MATCH);
        assertThat(testMatch.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testMatch.getJournee()).isEqualTo(UPDATED_JOURNEE);
        assertThat(testMatch.getChrono()).isEqualTo(UPDATED_CHRONO);

        // Validate the Match in Elasticsearch
        verify(mockMatchSearchRepository, times(1)).save(testMatch);
    }

    @Test
    @Transactional
    public void updateNonExistingMatch() throws Exception {
        int databaseSizeBeforeUpdate = matchRepository.findAll().size();

        // Create the Match
        MatchDTO matchDTO = matchMapper.toDto(match);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMatchMockMvc.perform(put("/api/matches")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(matchDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Match in the database
        List<Match> matchList = matchRepository.findAll();
        assertThat(matchList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Match in Elasticsearch
        verify(mockMatchSearchRepository, times(0)).save(match);
    }

    @Test
    @Transactional
    public void deleteMatch() throws Exception {
        // Initialize the database
        matchRepository.saveAndFlush(match);

        int databaseSizeBeforeDelete = matchRepository.findAll().size();

        // Delete the match
        restMatchMockMvc.perform(delete("/api/matches/{id}", match.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Match> matchList = matchRepository.findAll();
        assertThat(matchList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Match in Elasticsearch
        verify(mockMatchSearchRepository, times(1)).deleteById(match.getId());
    }

    @Test
    @Transactional
    public void searchMatch() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        matchRepository.saveAndFlush(match);
        when(mockMatchSearchRepository.search(queryStringQuery("id:" + match.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(match), PageRequest.of(0, 1), 1));

        // Search the match
        restMatchMockMvc.perform(get("/api/_search/matches?query=id:" + match.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(match.getId().intValue())))
            .andExpect(jsonPath("$.[*].coupDEnvoi").value(hasItem(DEFAULT_COUP_D_ENVOI)))
            .andExpect(jsonPath("$.[*].hashcode").value(hasItem(DEFAULT_HASHCODE)))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].dateMatch").value(hasItem(DEFAULT_DATE_MATCH.toString())))
            .andExpect(jsonPath("$.[*].etatMatch").value(hasItem(DEFAULT_ETAT_MATCH.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].journee").value(hasItem(DEFAULT_JOURNEE)))
            .andExpect(jsonPath("$.[*].chrono").value(hasItem(DEFAULT_CHRONO)));
    }
}
