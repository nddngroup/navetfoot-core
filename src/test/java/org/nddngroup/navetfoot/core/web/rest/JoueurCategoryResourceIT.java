package org.nddngroup.navetfoot.core.web.rest;

import org.nddngroup.navetfoot.core.NavetfootCore;
import org.nddngroup.navetfoot.core.domain.JoueurCategory;
import org.nddngroup.navetfoot.core.repository.JoueurCategoryRepository;
import org.nddngroup.navetfoot.core.repository.search.JoueurCategorySearchRepository;
import org.nddngroup.navetfoot.core.service.JoueurCategoryService;
import org.nddngroup.navetfoot.core.service.dto.JoueurCategoryDTO;
import org.nddngroup.navetfoot.core.service.mapper.JoueurCategoryMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.nddngroup.navetfoot.core.repository.search.JoueurCategorySearchRepositoryMockConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static org.nddngroup.navetfoot.core.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link JoueurCategoryResource} REST controller.
 */
@SpringBootTest(classes = NavetfootCore.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class JoueurCategoryResourceIT {

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Boolean DEFAULT_DELETED = false;
    private static final Boolean UPDATED_DELETED = true;

    @Autowired
    private JoueurCategoryRepository joueurCategoryRepository;

    @Autowired
    private JoueurCategoryMapper joueurCategoryMapper;

    @Autowired
    private JoueurCategoryService joueurCategoryService;

    /**
     * This repository is mocked in the org.nddngroup.navetfoot.core.repository.search test package.
     *
     * @see JoueurCategorySearchRepositoryMockConfiguration
     */
    @Autowired
    private JoueurCategorySearchRepository mockJoueurCategorySearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restJoueurCategoryMockMvc;

    private JoueurCategory joueurCategory;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static JoueurCategory createEntity(EntityManager em) {
        JoueurCategory joueurCategory = new JoueurCategory()
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT)
            .deleted(DEFAULT_DELETED);
        return joueurCategory;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static JoueurCategory createUpdatedEntity(EntityManager em) {
        JoueurCategory joueurCategory = new JoueurCategory()
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .deleted(UPDATED_DELETED);
        return joueurCategory;
    }

    @BeforeEach
    public void initTest() {
        joueurCategory = createEntity(em);
    }

    @Test
    @Transactional
    public void createJoueurCategory() throws Exception {
        int databaseSizeBeforeCreate = joueurCategoryRepository.findAll().size();
        // Create the JoueurCategory
        JoueurCategoryDTO joueurCategoryDTO = joueurCategoryMapper.toDto(joueurCategory);
        restJoueurCategoryMockMvc.perform(post("/api/joueur-categories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(joueurCategoryDTO)))
            .andExpect(status().isCreated());

        // Validate the JoueurCategory in the database
        List<JoueurCategory> joueurCategoryList = joueurCategoryRepository.findAll();
        assertThat(joueurCategoryList).hasSize(databaseSizeBeforeCreate + 1);
        JoueurCategory testJoueurCategory = joueurCategoryList.get(joueurCategoryList.size() - 1);
        assertThat(testJoueurCategory.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testJoueurCategory.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testJoueurCategory.isDeleted()).isEqualTo(DEFAULT_DELETED);

        // Validate the JoueurCategory in Elasticsearch
        verify(mockJoueurCategorySearchRepository, times(1)).save(testJoueurCategory);
    }

    @Test
    @Transactional
    public void createJoueurCategoryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = joueurCategoryRepository.findAll().size();

        // Create the JoueurCategory with an existing ID
        joueurCategory.setId(1L);
        JoueurCategoryDTO joueurCategoryDTO = joueurCategoryMapper.toDto(joueurCategory);

        // An entity with an existing ID cannot be created, so this API call must fail
        restJoueurCategoryMockMvc.perform(post("/api/joueur-categories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(joueurCategoryDTO)))
            .andExpect(status().isBadRequest());

        // Validate the JoueurCategory in the database
        List<JoueurCategory> joueurCategoryList = joueurCategoryRepository.findAll();
        assertThat(joueurCategoryList).hasSize(databaseSizeBeforeCreate);

        // Validate the JoueurCategory in Elasticsearch
        verify(mockJoueurCategorySearchRepository, times(0)).save(joueurCategory);
    }


    @Test
    @Transactional
    public void getAllJoueurCategories() throws Exception {
        // Initialize the database
        joueurCategoryRepository.saveAndFlush(joueurCategory);

        // Get all the joueurCategoryList
        restJoueurCategoryMockMvc.perform(get("/api/joueur-categories?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(joueurCategory.getId().intValue())))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())));
    }

    @Test
    @Transactional
    public void getJoueurCategory() throws Exception {
        // Initialize the database
        joueurCategoryRepository.saveAndFlush(joueurCategory);

        // Get the joueurCategory
        restJoueurCategoryMockMvc.perform(get("/api/joueur-categories/{id}", joueurCategory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(joueurCategory.getId().intValue()))
            .andExpect(jsonPath("$.createdAt").value(sameInstant(DEFAULT_CREATED_AT)))
            .andExpect(jsonPath("$.updatedAt").value(sameInstant(DEFAULT_UPDATED_AT)))
            .andExpect(jsonPath("$.deleted").value(DEFAULT_DELETED.booleanValue()));
    }
    @Test
    @Transactional
    public void getNonExistingJoueurCategory() throws Exception {
        // Get the joueurCategory
        restJoueurCategoryMockMvc.perform(get("/api/joueur-categories/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateJoueurCategory() throws Exception {
        // Initialize the database
        joueurCategoryRepository.saveAndFlush(joueurCategory);

        int databaseSizeBeforeUpdate = joueurCategoryRepository.findAll().size();

        // Update the joueurCategory
        JoueurCategory updatedJoueurCategory = joueurCategoryRepository.findById(joueurCategory.getId()).get();
        // Disconnect from session so that the updates on updatedJoueurCategory are not directly saved in db
        em.detach(updatedJoueurCategory);
        updatedJoueurCategory
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT);
        JoueurCategoryDTO joueurCategoryDTO = joueurCategoryMapper.toDto(updatedJoueurCategory);

        restJoueurCategoryMockMvc.perform(put("/api/joueur-categories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(joueurCategoryDTO)))
            .andExpect(status().isOk());

        // Validate the JoueurCategory in the database
        List<JoueurCategory> joueurCategoryList = joueurCategoryRepository.findAll();
        assertThat(joueurCategoryList).hasSize(databaseSizeBeforeUpdate);
        JoueurCategory testJoueurCategory = joueurCategoryList.get(joueurCategoryList.size() - 1);
        assertThat(testJoueurCategory.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testJoueurCategory.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);

        // Validate the JoueurCategory in Elasticsearch
        verify(mockJoueurCategorySearchRepository, times(1)).save(testJoueurCategory);
    }

    @Test
    @Transactional
    public void updateNonExistingJoueurCategory() throws Exception {
        int databaseSizeBeforeUpdate = joueurCategoryRepository.findAll().size();

        // Create the JoueurCategory
        JoueurCategoryDTO joueurCategoryDTO = joueurCategoryMapper.toDto(joueurCategory);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restJoueurCategoryMockMvc.perform(put("/api/joueur-categories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(joueurCategoryDTO)))
            .andExpect(status().isBadRequest());

        // Validate the JoueurCategory in the database
        List<JoueurCategory> joueurCategoryList = joueurCategoryRepository.findAll();
        assertThat(joueurCategoryList).hasSize(databaseSizeBeforeUpdate);

        // Validate the JoueurCategory in Elasticsearch
        verify(mockJoueurCategorySearchRepository, times(0)).save(joueurCategory);
    }

    @Test
    @Transactional
    public void deleteJoueurCategory() throws Exception {
        // Initialize the database
        joueurCategoryRepository.saveAndFlush(joueurCategory);

        int databaseSizeBeforeDelete = joueurCategoryRepository.findAll().size();

        // Delete the joueurCategory
        restJoueurCategoryMockMvc.perform(delete("/api/joueur-categories/{id}", joueurCategory.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<JoueurCategory> joueurCategoryList = joueurCategoryRepository.findAll();
        assertThat(joueurCategoryList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the JoueurCategory in Elasticsearch
        verify(mockJoueurCategorySearchRepository, times(1)).deleteById(joueurCategory.getId());
    }

    @Test
    @Transactional
    public void searchJoueurCategory() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        joueurCategoryRepository.saveAndFlush(joueurCategory);
        when(mockJoueurCategorySearchRepository.search(queryStringQuery("id:" + joueurCategory.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(joueurCategory), PageRequest.of(0, 1), 1));

        // Search the joueurCategory
        restJoueurCategoryMockMvc.perform(get("/api/_search/joueur-categories?query=id:" + joueurCategory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(joueurCategory.getId().intValue())))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())));
    }
}
