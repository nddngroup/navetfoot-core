package org.nddngroup.navetfoot.core.web.rest;

import org.nddngroup.navetfoot.core.NavetfootCore;
import org.nddngroup.navetfoot.core.domain.Joueur;
import org.nddngroup.navetfoot.core.repository.JoueurRepository;
import org.nddngroup.navetfoot.core.repository.search.JoueurSearchRepository;
import org.nddngroup.navetfoot.core.service.JoueurService;
import org.nddngroup.navetfoot.core.service.dto.JoueurDTO;
import org.nddngroup.navetfoot.core.service.mapper.JoueurMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.nddngroup.navetfoot.core.repository.search.JoueurSearchRepositoryMockConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.nddngroup.navetfoot.core.web.rest.TestUtil.sameInstant;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link JoueurResource} REST controller.
 */
@SpringBootTest(classes = NavetfootCore.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class JoueurResourceIT {

    private static final String DEFAULT_PRENOM = "AAAAAAAAAA";
    private static final String UPDATED_PRENOM = "BBBBBBBBBB";

    private static final String DEFAULT_NOM = "AAAAAAAAAA";
    private static final String UPDATED_NOM = "BBBBBBBBBB";

    private static final String DEFAULT_IDENTIFIANT = "AAAAAAAAAA";
    private static final String UPDATED_IDENTIFIANT = "BBBBBBBBBB";

    private static final String DEFAULT_LIEU_DE_NAISSANCE = "AAAAAAAAAA";
    private static final String UPDATED_LIEU_DE_NAISSANCE = "BBBBBBBBBB";

    private static final String DEFAULT_DATE_DE_NAISSANCE = "AAAAAAAAAA";
    private static final String UPDATED_DATE_DE_NAISSANCE = "BBBBBBBBBB";

    private static final String DEFAULT_HASHCODE = "AAAAAAAAAA";
    private static final String UPDATED_HASHCODE = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Boolean DEFAULT_DELETED = false;
    private static final Boolean UPDATED_DELETED = true;

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_IMAGE_URL = "AAAAAAAAAA";
    private static final String UPDATED_IMAGE_URL = "BBBBBBBBBB";

    private static final Long DEFAULT_USER_ID = 1L;
    private static final Long UPDATED_USER_ID = 2L;

    @Autowired
    private JoueurRepository joueurRepository;

    @Autowired
    private JoueurMapper joueurMapper;

    @Autowired
    private JoueurService joueurService;

    /**
     * This repository is mocked in the org.nddngroup.navetfoot.core.repository.search test package.
     *
     * @see JoueurSearchRepositoryMockConfiguration
     */
    @Autowired
    private JoueurSearchRepository mockJoueurSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restJoueurMockMvc;

    private Joueur joueur;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Joueur createEntity(EntityManager em) {
        Joueur joueur = new Joueur()
            .prenom(DEFAULT_PRENOM)
            .nom(DEFAULT_NOM)
            .identifiant(DEFAULT_IDENTIFIANT)
            .lieuDeNaissance(DEFAULT_LIEU_DE_NAISSANCE)
            .dateDeNaissance(DEFAULT_DATE_DE_NAISSANCE)
            .hashcode(DEFAULT_HASHCODE)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT)
            .deleted(DEFAULT_DELETED)
            .code(DEFAULT_CODE)
            .imageUrl(DEFAULT_IMAGE_URL)
            .userId(DEFAULT_USER_ID);
        return joueur;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Joueur createUpdatedEntity(EntityManager em) {
        Joueur joueur = new Joueur()
            .prenom(UPDATED_PRENOM)
            .nom(UPDATED_NOM)
            .identifiant(UPDATED_IDENTIFIANT)
            .lieuDeNaissance(UPDATED_LIEU_DE_NAISSANCE)
            .dateDeNaissance(UPDATED_DATE_DE_NAISSANCE)
            .hashcode(UPDATED_HASHCODE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .deleted(UPDATED_DELETED)
            .code(UPDATED_CODE)
            .imageUrl(UPDATED_IMAGE_URL)
            .userId(UPDATED_USER_ID);
        return joueur;
    }

    @BeforeEach
    public void initTest() {
        joueur = createEntity(em);
    }

    @Test
    @Transactional
    public void createJoueur() throws Exception {
        int databaseSizeBeforeCreate = joueurRepository.findAll().size();
        // Create the Joueur
        JoueurDTO joueurDTO = joueurMapper.toDto(joueur);
        restJoueurMockMvc.perform(post("/api/joueurs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(joueurDTO)))
            .andExpect(status().isCreated());

        // Validate the Joueur in the database
        List<Joueur> joueurList = joueurRepository.findAll();
        assertThat(joueurList).hasSize(databaseSizeBeforeCreate + 1);
        Joueur testJoueur = joueurList.get(joueurList.size() - 1);
        assertThat(testJoueur.getPrenom()).isEqualTo(DEFAULT_PRENOM);
        assertThat(testJoueur.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testJoueur.getIdentifiant()).isEqualTo(DEFAULT_IDENTIFIANT);
        assertThat(testJoueur.getLieuDeNaissance()).isEqualTo(DEFAULT_LIEU_DE_NAISSANCE);
        assertThat(testJoueur.getDateDeNaissance()).isEqualTo(DEFAULT_DATE_DE_NAISSANCE);
        assertThat(testJoueur.getHashcode()).isEqualTo(DEFAULT_HASHCODE);
        assertThat(testJoueur.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testJoueur.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testJoueur.isDeleted()).isEqualTo(DEFAULT_DELETED);
        assertThat(testJoueur.getImageUrl()).isEqualTo(DEFAULT_IMAGE_URL);
        assertThat(testJoueur.getUserId()).isEqualTo(DEFAULT_USER_ID);

        // Validate the Joueur in Elasticsearch
        verify(mockJoueurSearchRepository, times(1)).save(testJoueur);
    }

    @Test
    @Transactional
    public void createJoueurWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = joueurRepository.findAll().size();

        // Create the Joueur with an existing ID
        joueur.setId(1L);
        JoueurDTO joueurDTO = joueurMapper.toDto(joueur);

        // An entity with an existing ID cannot be created, so this API call must fail
        restJoueurMockMvc.perform(post("/api/joueurs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(joueurDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Joueur in the database
        List<Joueur> joueurList = joueurRepository.findAll();
        assertThat(joueurList).hasSize(databaseSizeBeforeCreate);

        // Validate the Joueur in Elasticsearch
        verify(mockJoueurSearchRepository, times(0)).save(joueur);
    }


    @Test
    @Transactional
    public void checkPrenomIsRequired() throws Exception {
        int databaseSizeBeforeTest = joueurRepository.findAll().size();
        // set the field null
        joueur.setPrenom(null);

        // Create the Joueur, which fails.
        JoueurDTO joueurDTO = joueurMapper.toDto(joueur);


        restJoueurMockMvc.perform(post("/api/joueurs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(joueurDTO)))
            .andExpect(status().isBadRequest());

        List<Joueur> joueurList = joueurRepository.findAll();
        assertThat(joueurList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNomIsRequired() throws Exception {
        int databaseSizeBeforeTest = joueurRepository.findAll().size();
        // set the field null
        joueur.setNom(null);

        // Create the Joueur, which fails.
        JoueurDTO joueurDTO = joueurMapper.toDto(joueur);


        restJoueurMockMvc.perform(post("/api/joueurs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(joueurDTO)))
            .andExpect(status().isBadRequest());

        List<Joueur> joueurList = joueurRepository.findAll();
        assertThat(joueurList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkIdentifiantIsRequired() throws Exception {
        int databaseSizeBeforeTest = joueurRepository.findAll().size();
        // set the field null
        joueur.setIdentifiant(null);

        // Create the Joueur, which fails.
        JoueurDTO joueurDTO = joueurMapper.toDto(joueur);


        restJoueurMockMvc.perform(post("/api/joueurs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(joueurDTO)))
            .andExpect(status().isBadRequest());

        List<Joueur> joueurList = joueurRepository.findAll();
        assertThat(joueurList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = joueurRepository.findAll().size();
        // set the field null
        joueur.setCode(null);

        // Create the Joueur, which fails.
        JoueurDTO joueurDTO = joueurMapper.toDto(joueur);


        restJoueurMockMvc.perform(post("/api/joueurs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(joueurDTO)))
            .andExpect(status().isBadRequest());

        List<Joueur> joueurList = joueurRepository.findAll();
        assertThat(joueurList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllJoueurs() throws Exception {
        // Initialize the database
        joueurRepository.saveAndFlush(joueur);

        // Get all the joueurList
        restJoueurMockMvc.perform(get("/api/joueurs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(joueur.getId().intValue())))
            .andExpect(jsonPath("$.[*].prenom").value(hasItem(DEFAULT_PRENOM)))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)))
            .andExpect(jsonPath("$.[*].identifiant").value(hasItem(DEFAULT_IDENTIFIANT)))
            .andExpect(jsonPath("$.[*].lieuDeNaissance").value(hasItem(DEFAULT_LIEU_DE_NAISSANCE)))
            .andExpect(jsonPath("$.[*].dateDeNaissance").value(hasItem(DEFAULT_DATE_DE_NAISSANCE)))
            .andExpect(jsonPath("$.[*].hashcode").value(hasItem(DEFAULT_HASHCODE)))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].imageUrl").value(hasItem(DEFAULT_IMAGE_URL)))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID.intValue())));
    }

    @Test
    @Transactional
    public void getJoueur() throws Exception {
        // Initialize the database
        joueurRepository.saveAndFlush(joueur);

        // Get the joueur
        restJoueurMockMvc.perform(get("/api/joueurs/{id}", joueur.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(joueur.getId().intValue()))
            .andExpect(jsonPath("$.prenom").value(DEFAULT_PRENOM))
            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM))
            .andExpect(jsonPath("$.identifiant").value(DEFAULT_IDENTIFIANT))
            .andExpect(jsonPath("$.lieuDeNaissance").value(DEFAULT_LIEU_DE_NAISSANCE))
            .andExpect(jsonPath("$.dateDeNaissance").value(DEFAULT_DATE_DE_NAISSANCE))
            .andExpect(jsonPath("$.hashcode").value(DEFAULT_HASHCODE))
            .andExpect(jsonPath("$.createdAt").value(sameInstant(DEFAULT_CREATED_AT)))
            .andExpect(jsonPath("$.updatedAt").value(sameInstant(DEFAULT_UPDATED_AT)))
            .andExpect(jsonPath("$.deleted").value(DEFAULT_DELETED.booleanValue()))
            .andExpect(jsonPath("$.imageUrl").value(DEFAULT_IMAGE_URL))
            .andExpect(jsonPath("$.userId").value(DEFAULT_USER_ID.intValue()));
    }
    @Test
    @Transactional
    public void getNonExistingJoueur() throws Exception {
        // Get the joueur
        restJoueurMockMvc.perform(get("/api/joueurs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateJoueur() throws Exception {
        // Initialize the database
        joueurRepository.saveAndFlush(joueur);

        int databaseSizeBeforeUpdate = joueurRepository.findAll().size();

        // Update the joueur
        Joueur updatedJoueur = joueurRepository.findById(joueur.getId()).get();
        // Disconnect from session so that the updates on updatedJoueur are not directly saved in db
        em.detach(updatedJoueur);
        updatedJoueur
            .prenom(UPDATED_PRENOM)
            .nom(UPDATED_NOM)
            .identifiant(UPDATED_IDENTIFIANT)
            .lieuDeNaissance(UPDATED_LIEU_DE_NAISSANCE)
            .dateDeNaissance(UPDATED_DATE_DE_NAISSANCE)
            .hashcode(UPDATED_HASHCODE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .code(UPDATED_CODE)
            .imageUrl(UPDATED_IMAGE_URL)
            .userId(UPDATED_USER_ID);
        JoueurDTO joueurDTO = joueurMapper.toDto(updatedJoueur);

        restJoueurMockMvc.perform(put("/api/joueurs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(joueurDTO)))
            .andExpect(status().isOk());

        // Validate the Joueur in the database
        List<Joueur> joueurList = joueurRepository.findAll();
        assertThat(joueurList).hasSize(databaseSizeBeforeUpdate);
        Joueur testJoueur = joueurList.get(joueurList.size() - 1);
        assertThat(testJoueur.getPrenom()).isEqualTo(UPDATED_PRENOM);
        assertThat(testJoueur.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testJoueur.getIdentifiant()).isEqualTo(UPDATED_IDENTIFIANT);
        assertThat(testJoueur.getLieuDeNaissance()).isEqualTo(UPDATED_LIEU_DE_NAISSANCE);
        assertThat(testJoueur.getDateDeNaissance()).isEqualTo(UPDATED_DATE_DE_NAISSANCE);
        assertThat(testJoueur.getHashcode()).isEqualTo(UPDATED_HASHCODE);
        assertThat(testJoueur.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testJoueur.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testJoueur.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testJoueur.getImageUrl()).isEqualTo(UPDATED_IMAGE_URL);
        assertThat(testJoueur.getUserId()).isEqualTo(UPDATED_USER_ID);

        // Validate the Joueur in Elasticsearch
        verify(mockJoueurSearchRepository, times(1)).save(testJoueur);
    }

    @Test
    @Transactional
    public void updateNonExistingJoueur() throws Exception {
        int databaseSizeBeforeUpdate = joueurRepository.findAll().size();

        // Create the Joueur
        JoueurDTO joueurDTO = joueurMapper.toDto(joueur);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restJoueurMockMvc.perform(put("/api/joueurs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(joueurDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Joueur in the database
        List<Joueur> joueurList = joueurRepository.findAll();
        assertThat(joueurList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Joueur in Elasticsearch
        verify(mockJoueurSearchRepository, times(0)).save(joueur);
    }

    @Test
    @Transactional
    public void deleteJoueur() throws Exception {
        // Initialize the database
        joueurRepository.saveAndFlush(joueur);

        int databaseSizeBeforeDelete = joueurRepository.findAll().size();

        // Delete the joueur
        restJoueurMockMvc.perform(delete("/api/joueurs/{id}", joueur.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Joueur> joueurList = joueurRepository.findAll();
        assertThat(joueurList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Joueur in Elasticsearch
        verify(mockJoueurSearchRepository, times(1)).deleteById(joueur.getId());
    }

    @Test
    @Transactional
    public void searchJoueur() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        joueurRepository.saveAndFlush(joueur);
        when(mockJoueurSearchRepository.search(queryStringQuery("id:" + joueur.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(joueur), PageRequest.of(0, 1), 1));

        // Search the joueur
        restJoueurMockMvc.perform(get("/api/_search/joueurs?query=id:" + joueur.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(joueur.getId().intValue())))
            .andExpect(jsonPath("$.[*].prenom").value(hasItem(DEFAULT_PRENOM)))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)))
            .andExpect(jsonPath("$.[*].identifiant").value(hasItem(DEFAULT_IDENTIFIANT)))
            .andExpect(jsonPath("$.[*].lieuDeNaissance").value(hasItem(DEFAULT_LIEU_DE_NAISSANCE)))
            .andExpect(jsonPath("$.[*].dateDeNaissance").value(hasItem(DEFAULT_DATE_DE_NAISSANCE)))
            .andExpect(jsonPath("$.[*].hashcode").value(hasItem(DEFAULT_HASHCODE)))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].imageUrl").value(hasItem(DEFAULT_IMAGE_URL)))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID.intValue())));
    }
}
