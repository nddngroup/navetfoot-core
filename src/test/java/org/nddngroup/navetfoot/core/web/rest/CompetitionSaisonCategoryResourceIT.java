package org.nddngroup.navetfoot.core.web.rest;

import org.nddngroup.navetfoot.core.NavetfootCore;
import org.nddngroup.navetfoot.core.domain.CompetitionSaisonCategory;
import org.nddngroup.navetfoot.core.repository.CompetitionSaisonCategoryRepository;
import org.nddngroup.navetfoot.core.repository.search.CompetitionSaisonCategorySearchRepository;
import org.nddngroup.navetfoot.core.service.CompetitionSaisonCategoryService;
import org.nddngroup.navetfoot.core.service.dto.CompetitionSaisonCategoryDTO;
import org.nddngroup.navetfoot.core.service.mapper.CompetitionSaisonCategoryMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.nddngroup.navetfoot.core.repository.search.CompetitionSaisonCategorySearchRepositoryMockConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static org.nddngroup.navetfoot.core.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CompetitionSaisonCategoryResource} REST controller.
 */
@SpringBootTest(classes = NavetfootCore.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class CompetitionSaisonCategoryResourceIT {

    private static final String DEFAULT_HASHCODE = "AAAAAAAAAA";
    private static final String UPDATED_HASHCODE = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Boolean DEFAULT_DELETED = false;
    private static final Boolean UPDATED_DELETED = true;

    @Autowired
    private CompetitionSaisonCategoryRepository competitionSaisonCategoryRepository;

    @Autowired
    private CompetitionSaisonCategoryMapper competitionSaisonCategoryMapper;

    @Autowired
    private CompetitionSaisonCategoryService competitionSaisonCategoryService;

    /**
     * This repository is mocked in the org.nddngroup.navetfoot.core.repository.search test package.
     *
     * @see CompetitionSaisonCategorySearchRepositoryMockConfiguration
     */
    @Autowired
    private CompetitionSaisonCategorySearchRepository mockCompetitionSaisonCategorySearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCompetitionSaisonCategoryMockMvc;

    private CompetitionSaisonCategory competitionSaisonCategory;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CompetitionSaisonCategory createEntity(EntityManager em) {
        CompetitionSaisonCategory competitionSaisonCategory = new CompetitionSaisonCategory()
            .hashcode(DEFAULT_HASHCODE)
            .code(DEFAULT_CODE)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT)
            .deleted(DEFAULT_DELETED);
        return competitionSaisonCategory;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CompetitionSaisonCategory createUpdatedEntity(EntityManager em) {
        CompetitionSaisonCategory competitionSaisonCategory = new CompetitionSaisonCategory()
            .hashcode(UPDATED_HASHCODE)
            .code(UPDATED_CODE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .deleted(UPDATED_DELETED);
        return competitionSaisonCategory;
    }

    @BeforeEach
    public void initTest() {
        competitionSaisonCategory = createEntity(em);
    }

    @Test
    @Transactional
    public void createCompetitionSaisonCategory() throws Exception {
        int databaseSizeBeforeCreate = competitionSaisonCategoryRepository.findAll().size();
        // Create the CompetitionSaisonCategory
        CompetitionSaisonCategoryDTO competitionSaisonCategoryDTO = competitionSaisonCategoryMapper.toDto(competitionSaisonCategory);
        restCompetitionSaisonCategoryMockMvc.perform(post("/api/competition-saison-categories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(competitionSaisonCategoryDTO)))
            .andExpect(status().isCreated());

        // Validate the CompetitionSaisonCategory in the database
        List<CompetitionSaisonCategory> competitionSaisonCategoryList = competitionSaisonCategoryRepository.findAll();
        assertThat(competitionSaisonCategoryList).hasSize(databaseSizeBeforeCreate + 1);
        CompetitionSaisonCategory testCompetitionSaisonCategory = competitionSaisonCategoryList.get(competitionSaisonCategoryList.size() - 1);
        assertThat(testCompetitionSaisonCategory.getHashcode()).isEqualTo(DEFAULT_HASHCODE);
        assertThat(testCompetitionSaisonCategory.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testCompetitionSaisonCategory.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testCompetitionSaisonCategory.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testCompetitionSaisonCategory.isDeleted()).isEqualTo(DEFAULT_DELETED);

        // Validate the CompetitionSaisonCategory in Elasticsearch
        verify(mockCompetitionSaisonCategorySearchRepository, times(1)).save(testCompetitionSaisonCategory);
    }

    @Test
    @Transactional
    public void createCompetitionSaisonCategoryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = competitionSaisonCategoryRepository.findAll().size();

        // Create the CompetitionSaisonCategory with an existing ID
        competitionSaisonCategory.setId(1L);
        CompetitionSaisonCategoryDTO competitionSaisonCategoryDTO = competitionSaisonCategoryMapper.toDto(competitionSaisonCategory);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCompetitionSaisonCategoryMockMvc.perform(post("/api/competition-saison-categories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(competitionSaisonCategoryDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CompetitionSaisonCategory in the database
        List<CompetitionSaisonCategory> competitionSaisonCategoryList = competitionSaisonCategoryRepository.findAll();
        assertThat(competitionSaisonCategoryList).hasSize(databaseSizeBeforeCreate);

        // Validate the CompetitionSaisonCategory in Elasticsearch
        verify(mockCompetitionSaisonCategorySearchRepository, times(0)).save(competitionSaisonCategory);
    }


    @Test
    @Transactional
    public void getAllCompetitionSaisonCategories() throws Exception {
        // Initialize the database
        competitionSaisonCategoryRepository.saveAndFlush(competitionSaisonCategory);

        // Get all the competitionSaisonCategoryList
        restCompetitionSaisonCategoryMockMvc.perform(get("/api/competition-saison-categories?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(competitionSaisonCategory.getId().intValue())))
            .andExpect(jsonPath("$.[*].hashcode").value(hasItem(DEFAULT_HASHCODE)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())));
    }

    @Test
    @Transactional
    public void getCompetitionSaisonCategory() throws Exception {
        // Initialize the database
        competitionSaisonCategoryRepository.saveAndFlush(competitionSaisonCategory);

        // Get the competitionSaisonCategory
        restCompetitionSaisonCategoryMockMvc.perform(get("/api/competition-saison-categories/{id}", competitionSaisonCategory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(competitionSaisonCategory.getId().intValue()))
            .andExpect(jsonPath("$.hashcode").value(DEFAULT_HASHCODE))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE))
            .andExpect(jsonPath("$.createdAt").value(sameInstant(DEFAULT_CREATED_AT)))
            .andExpect(jsonPath("$.updatedAt").value(sameInstant(DEFAULT_UPDATED_AT)))
            .andExpect(jsonPath("$.deleted").value(DEFAULT_DELETED.booleanValue()));
    }
    @Test
    @Transactional
    public void getNonExistingCompetitionSaisonCategory() throws Exception {
        // Get the competitionSaisonCategory
        restCompetitionSaisonCategoryMockMvc.perform(get("/api/competition-saison-categories/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCompetitionSaisonCategory() throws Exception {
        // Initialize the database
        competitionSaisonCategoryRepository.saveAndFlush(competitionSaisonCategory);

        int databaseSizeBeforeUpdate = competitionSaisonCategoryRepository.findAll().size();

        // Update the competitionSaisonCategory
        CompetitionSaisonCategory updatedCompetitionSaisonCategory = competitionSaisonCategoryRepository.findById(competitionSaisonCategory.getId()).get();
        // Disconnect from session so that the updates on updatedCompetitionSaisonCategory are not directly saved in db
        em.detach(updatedCompetitionSaisonCategory);
        updatedCompetitionSaisonCategory
            .hashcode(UPDATED_HASHCODE)
            .code(UPDATED_CODE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT);
        CompetitionSaisonCategoryDTO competitionSaisonCategoryDTO = competitionSaisonCategoryMapper.toDto(updatedCompetitionSaisonCategory);

        restCompetitionSaisonCategoryMockMvc.perform(put("/api/competition-saison-categories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(competitionSaisonCategoryDTO)))
            .andExpect(status().isOk());

        // Validate the CompetitionSaisonCategory in the database
        List<CompetitionSaisonCategory> competitionSaisonCategoryList = competitionSaisonCategoryRepository.findAll();
        assertThat(competitionSaisonCategoryList).hasSize(databaseSizeBeforeUpdate);
        CompetitionSaisonCategory testCompetitionSaisonCategory = competitionSaisonCategoryList.get(competitionSaisonCategoryList.size() - 1);
        assertThat(testCompetitionSaisonCategory.getHashcode()).isEqualTo(UPDATED_HASHCODE);
        assertThat(testCompetitionSaisonCategory.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testCompetitionSaisonCategory.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testCompetitionSaisonCategory.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);

        // Validate the CompetitionSaisonCategory in Elasticsearch
        verify(mockCompetitionSaisonCategorySearchRepository, times(1)).save(testCompetitionSaisonCategory);
    }

    @Test
    @Transactional
    public void updateNonExistingCompetitionSaisonCategory() throws Exception {
        int databaseSizeBeforeUpdate = competitionSaisonCategoryRepository.findAll().size();

        // Create the CompetitionSaisonCategory
        CompetitionSaisonCategoryDTO competitionSaisonCategoryDTO = competitionSaisonCategoryMapper.toDto(competitionSaisonCategory);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCompetitionSaisonCategoryMockMvc.perform(put("/api/competition-saison-categories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(competitionSaisonCategoryDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CompetitionSaisonCategory in the database
        List<CompetitionSaisonCategory> competitionSaisonCategoryList = competitionSaisonCategoryRepository.findAll();
        assertThat(competitionSaisonCategoryList).hasSize(databaseSizeBeforeUpdate);

        // Validate the CompetitionSaisonCategory in Elasticsearch
        verify(mockCompetitionSaisonCategorySearchRepository, times(0)).save(competitionSaisonCategory);
    }

    @Test
    @Transactional
    public void deleteCompetitionSaisonCategory() throws Exception {
        // Initialize the database
        competitionSaisonCategoryRepository.saveAndFlush(competitionSaisonCategory);

        int databaseSizeBeforeDelete = competitionSaisonCategoryRepository.findAll().size();

        // Delete the competitionSaisonCategory
        restCompetitionSaisonCategoryMockMvc.perform(delete("/api/competition-saison-categories/{id}", competitionSaisonCategory.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CompetitionSaisonCategory> competitionSaisonCategoryList = competitionSaisonCategoryRepository.findAll();
        assertThat(competitionSaisonCategoryList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the CompetitionSaisonCategory in Elasticsearch
        verify(mockCompetitionSaisonCategorySearchRepository, times(1)).deleteById(competitionSaisonCategory.getId());
    }

    @Test
    @Transactional
    public void searchCompetitionSaisonCategory() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        competitionSaisonCategoryRepository.saveAndFlush(competitionSaisonCategory);
        when(mockCompetitionSaisonCategorySearchRepository.search(queryStringQuery("id:" + competitionSaisonCategory.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(competitionSaisonCategory), PageRequest.of(0, 1), 1));

        // Search the competitionSaisonCategory
        restCompetitionSaisonCategoryMockMvc.perform(get("/api/_search/competition-saison-categories?query=id:" + competitionSaisonCategory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(competitionSaisonCategory.getId().intValue())))
            .andExpect(jsonPath("$.[*].hashcode").value(hasItem(DEFAULT_HASHCODE)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())));
    }
}
