package org.nddngroup.navetfoot.core.web.rest.custom;

import org.nddngroup.navetfoot.core.NavetfootCore;
import org.nddngroup.navetfoot.core.domain.*;
import org.nddngroup.navetfoot.core.domain.*;
import org.nddngroup.navetfoot.core.domain.enumeration.EtatMatch;
import org.nddngroup.navetfoot.core.repository.*;
import org.nddngroup.navetfoot.core.repository.search.*;
import org.nddngroup.navetfoot.core.repository.*;
import org.nddngroup.navetfoot.core.repository.search.*;
import org.nddngroup.navetfoot.core.service.MatchService;
import org.nddngroup.navetfoot.core.service.mapper.MatchMapper;
import org.nddngroup.navetfoot.core.web.rest.*;
import org.nddngroup.navetfoot.core.web.rest.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.*;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CustomMatchResource} REST controller.
 */
@SpringBootTest(classes = NavetfootCore.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class CustomMatchResourceIT {

    private static final String DEFAULT_COUP_D_ENVOI = "AAAAAAAAAA";
    private static final String UPDATED_COUP_D_ENVOI = "BBBBBBBBBB";

    private static final String DEFAULT_HASHCODE = "AAAAAAAAAA";
    private static final String UPDATED_HASHCODE = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Boolean DEFAULT_DELETED = false;
    private static final Boolean UPDATED_DELETED = true;

    private static final LocalDate DEFAULT_DATE_MATCH = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_MATCH = LocalDate.now(ZoneId.systemDefault());

    private static final EtatMatch DEFAULT_ETAT_MATCH = EtatMatch.PROGRAMME;
    private static final EtatMatch UPDATED_ETAT_MATCH = EtatMatch.EN_COURS_1;

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final Integer DEFAULT_JOURNEE = 1;
    private static final Integer UPDATED_JOURNEE = 2;

    private static final String DEFAULT_CHRONO = "AAAAAAAAAA";
    private static final String UPDATED_CHRONO = "BBBBBBBBBB";

    @Autowired
    private MatchRepository matchRepository;
    @Autowired
    private FeuilleDeMatchRepository feuilleDeMatchRepository;
    @Autowired
    private DetailFeuilleDeMatchRepository detailFeuilleDeMatchRepository;
    @Autowired
    private CartonRepository cartonRepository;
    @Autowired
    private PenaltyRepository penaltyRepository;
    @Autowired
    private ButRepository butRepository;
    @Autowired
    private CoupFrancRepository coupFrancRepository;
    @Autowired
    private CornerRepository cornerRepository;
    @Autowired
    private RemplacementRepository remplacementRepository;
    @Autowired
    private TirRepository tirRepository;
    @Autowired
    private HorsJeuRepository horsJeuRepository;

    @Autowired
    private MatchMapper matchMapper;

    @Autowired
    private MatchService matchService;

    /**
     * This repository is mocked in the org.nddngroup.navetfoot.core.repository.search test package.
     *
     * @see MatchSearchRepositoryMockConfiguration
     */
    @Autowired
    private MatchSearchRepository mockMatchSearchRepository;
    @Autowired
    private FeuilleDeMatchSearchRepository mockFeuilleDeMatchSearchRepository;
    @Autowired
    private DetailFeuilleDeMatchSearchRepository mockDetailFeuilleDeMatchSearchRepository;
    @Autowired
    private ButSearchRepository mockButSearchRepository;
    @Autowired
    private CartonSearchRepository mockCartonSearchRepository;
    @Autowired
    private PenaltySearchRepository mockPenaltySearchRepository;
    @Autowired
    private CoupFrancSearchRepository mockCoupFrancSearchRepository;
    @Autowired
    private CornerSearchRepository mockCornerSearchRepository;
    @Autowired
    private RemplacementSearchRepository mockRemplacementSearchRepository;
    @Autowired
    private TirSearchRepository mockTirSearchRepository;
    @Autowired
    private HorsJeuSearchRepository mockHorsJeuSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMatchMockMvc;

    private Match match;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Match createEntity(EntityManager em) {
        Match match = new Match()
            .coupDEnvoi(DEFAULT_COUP_D_ENVOI)
            .hashcode(DEFAULT_HASHCODE)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT)
            .deleted(DEFAULT_DELETED)
            .dateMatch(DEFAULT_DATE_MATCH)
            .etatMatch(DEFAULT_ETAT_MATCH)
            .code(DEFAULT_CODE)
            .journee(DEFAULT_JOURNEE)
            .chrono(DEFAULT_CHRONO);

        return match;
    }

    public static Organisation createCompleteEntity(Match match, EntityManager em) {
        // add organisation
        Organisation organisation = OrganisationResourceIT.createEntity(em);
        em.persist(organisation);

        // create saison and competition
        Saison saison = SaisonResourceIT.createEntity(em);
        saison.setOrganisation(organisation);
        Competition competition = CompetitionResourceIT.createEntity(em);
        competition.setOrganisation(organisation);
        em.persist(saison);
        em.persist(competition);

        // create competitionSaison
        CompetitionSaison competitionSaison = CompetitionSaisonResourceIT.createEntity(em);
        competitionSaison.setSaison(saison);
        competitionSaison.setCompetition(competition);
        em.persist(competitionSaison);

        // create EtapeCompetition
        EtapeCompetition etapeCompetition = EtapeCompetitionResourceIT.createEntity(em);
        etapeCompetition.setCompetitionSaison(competitionSaison);
        em.persist(etapeCompetition);

        // add local and visiteur
        Association local = AssociationResourceIT.createEntity(em);
        Association visiteur = AssociationResourceIT.createEntity(em);
        em.persist(local);
        em.persist(visiteur);

        // add affiliation
        Affiliation affiliation1 = AffiliationResourceIT.createEntity(em);
        affiliation1.setAssociation(local);
        affiliation1.setOrganisation(organisation);
        em.persist(affiliation1);

        Affiliation affiliation2 = AffiliationResourceIT.createEntity(em);
        affiliation2.setAssociation(visiteur);
        affiliation2.setOrganisation(organisation);
        em.persist(affiliation2);

        em.flush();

        match.setLocal(local);
        match.setVisiteur(visiteur);
        match.setEtapeCompetition(etapeCompetition);

        return organisation;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Match createUpdatedEntity(EntityManager em) {
        Match match = new Match()
            .coupDEnvoi(UPDATED_COUP_D_ENVOI)
            .hashcode(UPDATED_HASHCODE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .deleted(UPDATED_DELETED)
            .dateMatch(UPDATED_DATE_MATCH)
            .etatMatch(UPDATED_ETAT_MATCH)
            .code(UPDATED_CODE)
            .journee(UPDATED_JOURNEE)
            .chrono(UPDATED_CHRONO);
        return match;
    }

    @BeforeEach
    public void initTest() {
        match = createEntity(em);
    }

    @Test
    @Transactional
    public void supprimerMatchEnCours() throws Exception {
        // Initialize the database
        match.setEtatMatch(EtatMatch.EN_COURS_1);
        Organisation organisation = createCompleteEntity(match, em);
        matchRepository.saveAndFlush(match);

        int databaseSizeBeforeDelete = matchRepository.findAll().size();

        // Delete the match
        restMatchMockMvc.perform(delete("/api/organisations/{organisationId}/matches/{id}", organisation.getId(), match.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.code").value(6004))
            .andExpect(jsonPath("$.message").value("Le match ne peut pas être supprimé."))
        ;

        match.setEtatMatch(EtatMatch.EN_COURS_2);
        // Delete the match
        restMatchMockMvc.perform(delete("/api/organisations/{organisationId}/matches/{id}", organisation.getId(), match.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.code").value(6004))
            .andExpect(jsonPath("$.message").value("Le match ne peut pas être supprimé."))
        ;

        match.setEtatMatch(EtatMatch.EN_COURS_3);
        // Delete the match
        restMatchMockMvc.perform(delete("/api/organisations/{organisationId}/matches/{id}", organisation.getId(), match.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.code").value(6004))
            .andExpect(jsonPath("$.message").value("Le match ne peut pas être supprimé."))
        ;

        match.setEtatMatch(EtatMatch.EN_COURS_4);
        // Delete the match
        restMatchMockMvc.perform(delete("/api/organisations/{organisationId}/matches/{id}", organisation.getId(), match.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.code").value(6004))
            .andExpect(jsonPath("$.message").value("Le match ne peut pas être supprimé."))
        ;

        match.setEtatMatch(EtatMatch.TERMINE);
        // Delete the match
        restMatchMockMvc.perform(delete("/api/organisations/{organisationId}/matches/{id}", organisation.getId(), match.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.code").value(6004))
            .andExpect(jsonPath("$.message").value("Le match ne peut pas être supprimé."))
        ;

        match.setEtatMatch(EtatMatch.MI_TEMPS_1);
        // Delete the match
        restMatchMockMvc.perform(delete("/api/organisations/{organisationId}/matches/{id}", organisation.getId(), match.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.code").value(6004))
            .andExpect(jsonPath("$.message").value("Le match ne peut pas être supprimé."))
        ;

        match.setEtatMatch(EtatMatch.MI_TEMPS_2);
        // Delete the match
        restMatchMockMvc.perform(delete("/api/organisations/{organisationId}/matches/{id}", organisation.getId(), match.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.code").value(6004))
            .andExpect(jsonPath("$.message").value("Le match ne peut pas être supprimé."))
        ;

        // Validate the database contains one less item
        List<Match> matchList = matchRepository.findAll();
        assertThat(matchList).hasSize(databaseSizeBeforeDelete);

        // Validate the Match in Elasticsearch
        verify(mockMatchSearchRepository, times(0)).deleteById(match.getId());
    }

    @Test
    @Transactional
    public void deleteMatch() throws Exception {
        // Initialize the database
        Organisation organisation = createCompleteEntity(match, em);
        matchRepository.saveAndFlush(match);

        // add feuilleDeMatch
        FeuilleDeMatch feuilleDeMatch = FeuilleDeMatchResourceIT.createEntity(em);
        feuilleDeMatch.setMatch(match);
        em.persist(feuilleDeMatch);

        // add details to feuilleDeMatch
        DetailFeuilleDeMatch detail1 = DetailFeuilleDeMatchResourceIT.createEntity(em);
        detail1.setFeuilleDeMatch(feuilleDeMatch);
        em.persist(detail1);
        DetailFeuilleDeMatch detail2 = DetailFeuilleDeMatchResourceIT.createEntity(em);
        detail2.setFeuilleDeMatch(feuilleDeMatch);
        em.persist(detail2);

        // add events
        But but = ButResourceIT.createEntity(em);
        but.setMatch(match);
        em.persist(but);
        Carton carton = CartonResourceIT.createEntity(em);
        carton.setMatch(match);
        em.persist(carton);
        Penalty penalty = PenaltyResourceIT.createEntity(em);
        penalty.setMatch(match);
        em.persist(penalty);
        CoupFranc coupFranc = CoupFrancResourceIT.createEntity(em);
        coupFranc.setMatch(match);
        em.persist(coupFranc);
        Corner corner = CornerResourceIT.createEntity(em);
        corner.setMatch(match);
        em.persist(corner);
        Remplacement remplacement = RemplacementResourceIT.createEntity(em);
        remplacement.setMatch(match);
        em.persist(remplacement);
        Tir tir = TirResourceIT.createEntity(em);
        tir.setMatch(match);
        em.persist(tir);
        HorsJeu horsJeu = HorsJeuResourceIT.createEntity(em);
        horsJeu.setMatch(match);
        em.persist(horsJeu);

        em.flush();

        int databaseSizeBeforeDelete = matchRepository.findAll().size();

        // Delete the match
        restMatchMockMvc.perform(delete("/api/organisations/{organisationId}/matches/{id}", organisation.getId(), match.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());

        // Validate the database contains one less match
        List<Match> matchList = matchRepository.findAllByDeletedIsFalse();
        assertThat(matchList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the database cleaned
        assertThat(feuilleDeMatchRepository.findOneByMatchAndDeletedIsFalse(match)).isEmpty();
        assertThat(detailFeuilleDeMatchRepository.findAllByFeuilleDeMatchAndDeletedIsFalse(feuilleDeMatch)).isEmpty();
        assertThat(butRepository.findAllByMatchAndDeletedIsFalse(match)).isEmpty();
        assertThat(cartonRepository.findAllByMatchAndDeletedIsFalse(match)).isEmpty();
        assertThat(penaltyRepository.findAllByMatchAndDeletedIsFalse(match)).isEmpty();
        assertThat(coupFrancRepository.findAllByMatchAndDeletedIsFalse(match)).isEmpty();
        assertThat(cornerRepository.findAllByMatchAndDeletedIsFalse(match)).isEmpty();
        assertThat(remplacementRepository.findAllByMatchAndDeletedIsFalse(match)).isEmpty();
        assertThat(tirRepository.findAllByMatchAndDeletedIsFalse(match)).isEmpty();
        assertThat(horsJeuRepository.findAllByMatchAndDeletedIsFalse(match)).isEmpty();

        // Validate in Elasticsearch
        verify(mockMatchSearchRepository, times(1)).deleteById(match.getId());
        verify(mockFeuilleDeMatchSearchRepository, times(1)).deleteById(feuilleDeMatch.getId());
        verify(mockDetailFeuilleDeMatchSearchRepository, times(1)).deleteById(detail1.getId());
        verify(mockDetailFeuilleDeMatchSearchRepository, times(1)).deleteById(detail2.getId());
        verify(mockButSearchRepository, times(1)).deleteById(but.getId());
        verify(mockCartonSearchRepository, times(1)).deleteById(carton.getId());
        verify(mockPenaltySearchRepository, times(1)).deleteById(penalty.getId());
        verify(mockCoupFrancSearchRepository, times(1)).deleteById(coupFranc.getId());
        verify(mockCornerSearchRepository, times(1)).deleteById(corner.getId());
        verify(mockRemplacementSearchRepository, times(1)).deleteById(remplacement.getId());
        verify(mockTirSearchRepository, times(1)).deleteById(tir.getId());
        verify(mockHorsJeuSearchRepository, times(1)).deleteById(horsJeu.getId());
    }
}
