package org.nddngroup.navetfoot.core.web.rest;

import org.nddngroup.navetfoot.core.NavetfootCore;
import org.nddngroup.navetfoot.core.domain.DetailPoule;
import org.nddngroup.navetfoot.core.repository.DetailPouleRepository;
import org.nddngroup.navetfoot.core.repository.search.DetailPouleSearchRepository;
import org.nddngroup.navetfoot.core.service.DetailPouleService;
import org.nddngroup.navetfoot.core.service.dto.DetailPouleDTO;
import org.nddngroup.navetfoot.core.service.mapper.DetailPouleMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.nddngroup.navetfoot.core.repository.search.DetailPouleSearchRepositoryMockConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static org.nddngroup.navetfoot.core.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link DetailPouleResource} REST controller.
 */
@SpringBootTest(classes = NavetfootCore.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class DetailPouleResourceIT {

    private static final Integer DEFAULT_NOMBRE_DE_POINT = 1;
    private static final Integer UPDATED_NOMBRE_DE_POINT = 2;

    private static final Integer DEFAULT_GOAL_DIFFERENCE = 1;
    private static final Integer UPDATED_GOAL_DIFFERENCE = 2;

    private static final Integer DEFAULT_JOURNEE = 1;
    private static final Integer UPDATED_JOURNEE = 2;

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_HASHCODE = "AAAAAAAAAA";
    private static final String UPDATED_HASHCODE = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATE_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATE_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Boolean DEFAULT_DELETED = false;
    private static final Boolean UPDATED_DELETED = true;

    private static final Integer DEFAULT_VICTOIRE = 1;
    private static final Integer UPDATED_VICTOIRE = 2;

    private static final Integer DEFAULT_MATCH_NULL = 1;
    private static final Integer UPDATED_MATCH_NULL = 2;

    private static final Integer DEFAULT_DEFAITE = 1;
    private static final Integer UPDATED_DEFAITE = 2;

    private static final Integer DEFAULT_BUTS_POUR = 1;
    private static final Integer UPDATED_BUTS_POUR = 2;

    private static final Integer DEFAULT_BUTS_CONTRE = 1;
    private static final Integer UPDATED_BUTS_CONTRE = 2;

    @Autowired
    private DetailPouleRepository detailPouleRepository;

    @Autowired
    private DetailPouleMapper detailPouleMapper;

    @Autowired
    private DetailPouleService detailPouleService;

    /**
     * This repository is mocked in the org.nddngroup.navetfoot.core.repository.search test package.
     *
     * @see DetailPouleSearchRepositoryMockConfiguration
     */
    @Autowired
    private DetailPouleSearchRepository mockDetailPouleSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDetailPouleMockMvc;

    private DetailPoule detailPoule;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DetailPoule createEntity(EntityManager em) {
        DetailPoule detailPoule = new DetailPoule()
            .nombreDePoint(DEFAULT_NOMBRE_DE_POINT)
            .goalDifference(DEFAULT_GOAL_DIFFERENCE)
            .journee(DEFAULT_JOURNEE)
            .code(DEFAULT_CODE)
            .hashcode(DEFAULT_HASHCODE)
            .createdAt(DEFAULT_CREATED_AT)
            .updateAt(DEFAULT_UPDATE_AT)
            .deleted(DEFAULT_DELETED)
            .victoire(DEFAULT_VICTOIRE)
            .matchNull(DEFAULT_MATCH_NULL)
            .defaite(DEFAULT_DEFAITE)
            .butsPour(DEFAULT_BUTS_POUR)
            .butsContre(DEFAULT_BUTS_CONTRE);
        return detailPoule;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DetailPoule createUpdatedEntity(EntityManager em) {
        DetailPoule detailPoule = new DetailPoule()
            .nombreDePoint(UPDATED_NOMBRE_DE_POINT)
            .goalDifference(UPDATED_GOAL_DIFFERENCE)
            .journee(UPDATED_JOURNEE)
            .code(UPDATED_CODE)
            .hashcode(UPDATED_HASHCODE)
            .createdAt(UPDATED_CREATED_AT)
            .updateAt(UPDATED_UPDATE_AT)
            .deleted(UPDATED_DELETED)
            .victoire(UPDATED_VICTOIRE)
            .matchNull(UPDATED_MATCH_NULL)
            .defaite(UPDATED_DEFAITE)
            .butsPour(UPDATED_BUTS_POUR)
            .butsContre(UPDATED_BUTS_CONTRE);
        return detailPoule;
    }

    @BeforeEach
    public void initTest() {
        detailPoule = createEntity(em);
    }

    @Test
    @Transactional
    public void createDetailPoule() throws Exception {
        int databaseSizeBeforeCreate = detailPouleRepository.findAll().size();
        // Create the DetailPoule
        DetailPouleDTO detailPouleDTO = detailPouleMapper.toDto(detailPoule);
        restDetailPouleMockMvc.perform(post("/api/detail-poules")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(detailPouleDTO)))
            .andExpect(status().isCreated());

        // Validate the DetailPoule in the database
        List<DetailPoule> detailPouleList = detailPouleRepository.findAll();
        assertThat(detailPouleList).hasSize(databaseSizeBeforeCreate + 1);
        DetailPoule testDetailPoule = detailPouleList.get(detailPouleList.size() - 1);
        assertThat(testDetailPoule.getNombreDePoint()).isEqualTo(DEFAULT_NOMBRE_DE_POINT);
        assertThat(testDetailPoule.getGoalDifference()).isEqualTo(DEFAULT_GOAL_DIFFERENCE);
        assertThat(testDetailPoule.getJournee()).isEqualTo(DEFAULT_JOURNEE);
        assertThat(testDetailPoule.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testDetailPoule.getHashcode()).isEqualTo(DEFAULT_HASHCODE);
        assertThat(testDetailPoule.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testDetailPoule.getUpdateAt()).isEqualTo(DEFAULT_UPDATE_AT);
        assertThat(testDetailPoule.isDeleted()).isEqualTo(DEFAULT_DELETED);
        assertThat(testDetailPoule.getVictoire()).isEqualTo(DEFAULT_VICTOIRE);
        assertThat(testDetailPoule.getMatchNull()).isEqualTo(DEFAULT_MATCH_NULL);
        assertThat(testDetailPoule.getDefaite()).isEqualTo(DEFAULT_DEFAITE);
        assertThat(testDetailPoule.getButsPour()).isEqualTo(DEFAULT_BUTS_POUR);
        assertThat(testDetailPoule.getButsContre()).isEqualTo(DEFAULT_BUTS_CONTRE);

        // Validate the DetailPoule in Elasticsearch
        verify(mockDetailPouleSearchRepository, times(1)).save(testDetailPoule);
    }

    @Test
    @Transactional
    public void createDetailPouleWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = detailPouleRepository.findAll().size();

        // Create the DetailPoule with an existing ID
        detailPoule.setId(1L);
        DetailPouleDTO detailPouleDTO = detailPouleMapper.toDto(detailPoule);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDetailPouleMockMvc.perform(post("/api/detail-poules")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(detailPouleDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DetailPoule in the database
        List<DetailPoule> detailPouleList = detailPouleRepository.findAll();
        assertThat(detailPouleList).hasSize(databaseSizeBeforeCreate);

        // Validate the DetailPoule in Elasticsearch
        verify(mockDetailPouleSearchRepository, times(0)).save(detailPoule);
    }


    @Test
    @Transactional
    public void getAllDetailPoules() throws Exception {
        // Initialize the database
        detailPouleRepository.saveAndFlush(detailPoule);

        // Get all the detailPouleList
        restDetailPouleMockMvc.perform(get("/api/detail-poules?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(detailPoule.getId().intValue())))
            .andExpect(jsonPath("$.[*].nombreDePoint").value(hasItem(DEFAULT_NOMBRE_DE_POINT)))
            .andExpect(jsonPath("$.[*].goalDifference").value(hasItem(DEFAULT_GOAL_DIFFERENCE)))
            .andExpect(jsonPath("$.[*].journee").value(hasItem(DEFAULT_JOURNEE)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].hashcode").value(hasItem(DEFAULT_HASHCODE)))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updateAt").value(hasItem(sameInstant(DEFAULT_UPDATE_AT))))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].victoire").value(hasItem(DEFAULT_VICTOIRE)))
            .andExpect(jsonPath("$.[*].matchNull").value(hasItem(DEFAULT_MATCH_NULL)))
            .andExpect(jsonPath("$.[*].defaite").value(hasItem(DEFAULT_DEFAITE)))
            .andExpect(jsonPath("$.[*].butsPour").value(hasItem(DEFAULT_BUTS_POUR)))
            .andExpect(jsonPath("$.[*].butsContre").value(hasItem(DEFAULT_BUTS_CONTRE)));
    }

    @Test
    @Transactional
    public void getDetailPoule() throws Exception {
        // Initialize the database
        detailPouleRepository.saveAndFlush(detailPoule);

        // Get the detailPoule
        restDetailPouleMockMvc.perform(get("/api/detail-poules/{id}", detailPoule.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(detailPoule.getId().intValue()))
            .andExpect(jsonPath("$.nombreDePoint").value(DEFAULT_NOMBRE_DE_POINT))
            .andExpect(jsonPath("$.goalDifference").value(DEFAULT_GOAL_DIFFERENCE))
            .andExpect(jsonPath("$.journee").value(DEFAULT_JOURNEE))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE))
            .andExpect(jsonPath("$.hashcode").value(DEFAULT_HASHCODE))
            .andExpect(jsonPath("$.createdAt").value(sameInstant(DEFAULT_CREATED_AT)))
            .andExpect(jsonPath("$.updateAt").value(sameInstant(DEFAULT_UPDATE_AT)))
            .andExpect(jsonPath("$.deleted").value(DEFAULT_DELETED.booleanValue()))
            .andExpect(jsonPath("$.victoire").value(DEFAULT_VICTOIRE))
            .andExpect(jsonPath("$.matchNull").value(DEFAULT_MATCH_NULL))
            .andExpect(jsonPath("$.defaite").value(DEFAULT_DEFAITE))
            .andExpect(jsonPath("$.butsPour").value(DEFAULT_BUTS_POUR))
            .andExpect(jsonPath("$.butsContre").value(DEFAULT_BUTS_CONTRE));
    }
    @Test
    @Transactional
    public void getNonExistingDetailPoule() throws Exception {
        // Get the detailPoule
        restDetailPouleMockMvc.perform(get("/api/detail-poules/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDetailPoule() throws Exception {
        // Initialize the database
        detailPouleRepository.saveAndFlush(detailPoule);

        int databaseSizeBeforeUpdate = detailPouleRepository.findAll().size();

        // Update the detailPoule
        DetailPoule updatedDetailPoule = detailPouleRepository.findById(detailPoule.getId()).get();
        // Disconnect from session so that the updates on updatedDetailPoule are not directly saved in db
        em.detach(updatedDetailPoule);
        updatedDetailPoule
            .nombreDePoint(UPDATED_NOMBRE_DE_POINT)
            .goalDifference(UPDATED_GOAL_DIFFERENCE)
            .journee(UPDATED_JOURNEE)
            .code(UPDATED_CODE)
            .hashcode(UPDATED_HASHCODE)
            .createdAt(UPDATED_CREATED_AT)
            .updateAt(UPDATED_UPDATE_AT)
            .victoire(UPDATED_VICTOIRE)
            .matchNull(UPDATED_MATCH_NULL)
            .defaite(UPDATED_DEFAITE)
            .butsPour(UPDATED_BUTS_POUR)
            .butsContre(UPDATED_BUTS_CONTRE);
        DetailPouleDTO detailPouleDTO = detailPouleMapper.toDto(updatedDetailPoule);

        restDetailPouleMockMvc.perform(put("/api/detail-poules")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(detailPouleDTO)))
            .andExpect(status().isOk());

        // Validate the DetailPoule in the database
        List<DetailPoule> detailPouleList = detailPouleRepository.findAll();
        assertThat(detailPouleList).hasSize(databaseSizeBeforeUpdate);
        DetailPoule testDetailPoule = detailPouleList.get(detailPouleList.size() - 1);
        assertThat(testDetailPoule.getNombreDePoint()).isEqualTo(UPDATED_NOMBRE_DE_POINT);
        assertThat(testDetailPoule.getGoalDifference()).isEqualTo(UPDATED_GOAL_DIFFERENCE);
        assertThat(testDetailPoule.getJournee()).isEqualTo(UPDATED_JOURNEE);
        assertThat(testDetailPoule.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testDetailPoule.getHashcode()).isEqualTo(UPDATED_HASHCODE);
        assertThat(testDetailPoule.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testDetailPoule.getUpdateAt()).isEqualTo(UPDATED_UPDATE_AT);
        assertThat(testDetailPoule.getVictoire()).isEqualTo(UPDATED_VICTOIRE);
        assertThat(testDetailPoule.getMatchNull()).isEqualTo(UPDATED_MATCH_NULL);
        assertThat(testDetailPoule.getDefaite()).isEqualTo(UPDATED_DEFAITE);
        assertThat(testDetailPoule.getButsPour()).isEqualTo(UPDATED_BUTS_POUR);
        assertThat(testDetailPoule.getButsContre()).isEqualTo(UPDATED_BUTS_CONTRE);

        // Validate the DetailPoule in Elasticsearch
        verify(mockDetailPouleSearchRepository, times(1)).save(testDetailPoule);
    }

    @Test
    @Transactional
    public void updateNonExistingDetailPoule() throws Exception {
        int databaseSizeBeforeUpdate = detailPouleRepository.findAll().size();

        // Create the DetailPoule
        DetailPouleDTO detailPouleDTO = detailPouleMapper.toDto(detailPoule);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDetailPouleMockMvc.perform(put("/api/detail-poules")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(detailPouleDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DetailPoule in the database
        List<DetailPoule> detailPouleList = detailPouleRepository.findAll();
        assertThat(detailPouleList).hasSize(databaseSizeBeforeUpdate);

        // Validate the DetailPoule in Elasticsearch
        verify(mockDetailPouleSearchRepository, times(0)).save(detailPoule);
    }

    @Test
    @Transactional
    public void deleteDetailPoule() throws Exception {
        // Initialize the database
        detailPouleRepository.saveAndFlush(detailPoule);

        int databaseSizeBeforeDelete = detailPouleRepository.findAll().size();

        // Delete the detailPoule
        restDetailPouleMockMvc.perform(delete("/api/detail-poules/{id}", detailPoule.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DetailPoule> detailPouleList = detailPouleRepository.findAll();
        assertThat(detailPouleList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the DetailPoule in Elasticsearch
        verify(mockDetailPouleSearchRepository, times(1)).deleteById(detailPoule.getId());
    }

    @Test
    @Transactional
    public void searchDetailPoule() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        detailPouleRepository.saveAndFlush(detailPoule);
        when(mockDetailPouleSearchRepository.search(queryStringQuery("id:" + detailPoule.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(detailPoule), PageRequest.of(0, 1), 1));

        // Search the detailPoule
        restDetailPouleMockMvc.perform(get("/api/_search/detail-poules?query=id:" + detailPoule.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(detailPoule.getId().intValue())))
            .andExpect(jsonPath("$.[*].nombreDePoint").value(hasItem(DEFAULT_NOMBRE_DE_POINT)))
            .andExpect(jsonPath("$.[*].goalDifference").value(hasItem(DEFAULT_GOAL_DIFFERENCE)))
            .andExpect(jsonPath("$.[*].journee").value(hasItem(DEFAULT_JOURNEE)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].hashcode").value(hasItem(DEFAULT_HASHCODE)))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updateAt").value(hasItem(sameInstant(DEFAULT_UPDATE_AT))))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].victoire").value(hasItem(DEFAULT_VICTOIRE)))
            .andExpect(jsonPath("$.[*].matchNull").value(hasItem(DEFAULT_MATCH_NULL)))
            .andExpect(jsonPath("$.[*].defaite").value(hasItem(DEFAULT_DEFAITE)))
            .andExpect(jsonPath("$.[*].butsPour").value(hasItem(DEFAULT_BUTS_POUR)))
            .andExpect(jsonPath("$.[*].butsContre").value(hasItem(DEFAULT_BUTS_CONTRE)));
    }
}
