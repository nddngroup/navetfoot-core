package org.nddngroup.navetfoot.core.web.rest;

import org.nddngroup.navetfoot.core.NavetfootCore;
import org.nddngroup.navetfoot.core.domain.Corner;
import org.nddngroup.navetfoot.core.repository.CornerRepository;
import org.nddngroup.navetfoot.core.repository.search.CornerSearchRepository;
import org.nddngroup.navetfoot.core.service.CornerService;
import org.nddngroup.navetfoot.core.service.dto.CornerDTO;
import org.nddngroup.navetfoot.core.service.mapper.CornerMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.nddngroup.navetfoot.core.repository.search.CornerSearchRepositoryMockConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static org.nddngroup.navetfoot.core.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;
/**
 * Integration tests for the {@link CornerResource} REST controller.
 */
@SpringBootTest(classes = NavetfootCore.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class CornerResourceIT {

    private static final Equipe DEFAULT_EQUIPE = Equipe.LOCAL;
    private static final Equipe UPDATED_EQUIPE = Equipe.VISITEUR;

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_HASHCODE = "AAAAAAAAAA";
    private static final String UPDATED_HASHCODE = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Boolean DEFAULT_DELETED = false;
    private static final Boolean UPDATED_DELETED = true;

    private static final Integer DEFAULT_INSTANT = 1;
    private static final Integer UPDATED_INSTANT = 2;

    @Autowired
    private CornerRepository cornerRepository;

    @Autowired
    private CornerMapper cornerMapper;

    @Autowired
    private CornerService cornerService;

    /**
     * This repository is mocked in the org.nddngroup.navetfoot.core.repository.search test package.
     *
     * @see CornerSearchRepositoryMockConfiguration
     */
    @Autowired
    private CornerSearchRepository mockCornerSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCornerMockMvc;

    private Corner corner;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Corner createEntity(EntityManager em) {
        Corner corner = new Corner()
            .equipe(DEFAULT_EQUIPE)
            .code(DEFAULT_CODE)
            .hashcode(DEFAULT_HASHCODE)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT)
            .deleted(DEFAULT_DELETED)
            .instant(DEFAULT_INSTANT);
        return corner;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Corner createUpdatedEntity(EntityManager em) {
        Corner corner = new Corner()
            .equipe(UPDATED_EQUIPE)
            .code(UPDATED_CODE)
            .hashcode(UPDATED_HASHCODE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .deleted(UPDATED_DELETED)
            .instant(UPDATED_INSTANT);
        return corner;
    }

    @BeforeEach
    public void initTest() {
        corner = createEntity(em);
    }

    @Test
    @Transactional
    public void createCorner() throws Exception {
        int databaseSizeBeforeCreate = cornerRepository.findAll().size();
        // Create the Corner
        CornerDTO cornerDTO = cornerMapper.toDto(corner);
        restCornerMockMvc.perform(post("/api/corners")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(cornerDTO)))
            .andExpect(status().isCreated());

        // Validate the Corner in the database
        List<Corner> cornerList = cornerRepository.findAll();
        assertThat(cornerList).hasSize(databaseSizeBeforeCreate + 1);
        Corner testCorner = cornerList.get(cornerList.size() - 1);
        assertThat(testCorner.getEquipe()).isEqualTo(DEFAULT_EQUIPE);
        assertThat(testCorner.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testCorner.getHashcode()).isEqualTo(DEFAULT_HASHCODE);
        assertThat(testCorner.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testCorner.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testCorner.isDeleted()).isEqualTo(DEFAULT_DELETED);
        assertThat(testCorner.getInstant()).isEqualTo(DEFAULT_INSTANT);

        // Validate the Corner in Elasticsearch
        verify(mockCornerSearchRepository, times(1)).save(testCorner);
    }

    @Test
    @Transactional
    public void createCornerWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = cornerRepository.findAll().size();

        // Create the Corner with an existing ID
        corner.setId(1L);
        CornerDTO cornerDTO = cornerMapper.toDto(corner);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCornerMockMvc.perform(post("/api/corners")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(cornerDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Corner in the database
        List<Corner> cornerList = cornerRepository.findAll();
        assertThat(cornerList).hasSize(databaseSizeBeforeCreate);

        // Validate the Corner in Elasticsearch
        verify(mockCornerSearchRepository, times(0)).save(corner);
    }


    @Test
    @Transactional
    public void getAllCorners() throws Exception {
        // Initialize the database
        cornerRepository.saveAndFlush(corner);

        // Get all the cornerList
        restCornerMockMvc.perform(get("/api/corners?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(corner.getId().intValue())))
            .andExpect(jsonPath("$.[*].equipe").value(hasItem(DEFAULT_EQUIPE.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].hashcode").value(hasItem(DEFAULT_HASHCODE)))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].instant").value(hasItem(DEFAULT_INSTANT)));
    }

    @Test
    @Transactional
    public void getCorner() throws Exception {
        // Initialize the database
        cornerRepository.saveAndFlush(corner);

        // Get the corner
        restCornerMockMvc.perform(get("/api/corners/{id}", corner.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(corner.getId().intValue()))
            .andExpect(jsonPath("$.equipe").value(DEFAULT_EQUIPE.toString()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE))
            .andExpect(jsonPath("$.hashcode").value(DEFAULT_HASHCODE))
            .andExpect(jsonPath("$.createdAt").value(sameInstant(DEFAULT_CREATED_AT)))
            .andExpect(jsonPath("$.updatedAt").value(sameInstant(DEFAULT_UPDATED_AT)))
            .andExpect(jsonPath("$.deleted").value(DEFAULT_DELETED.booleanValue()))
            .andExpect(jsonPath("$.instant").value(DEFAULT_INSTANT));
    }
    @Test
    @Transactional
    public void getNonExistingCorner() throws Exception {
        // Get the corner
        restCornerMockMvc.perform(get("/api/corners/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCorner() throws Exception {
        // Initialize the database
        cornerRepository.saveAndFlush(corner);

        int databaseSizeBeforeUpdate = cornerRepository.findAll().size();

        // Update the corner
        Corner updatedCorner = cornerRepository.findById(corner.getId()).get();
        // Disconnect from session so that the updates on updatedCorner are not directly saved in db
        em.detach(updatedCorner);
        updatedCorner
            .equipe(UPDATED_EQUIPE)
            .code(UPDATED_CODE)
            .hashcode(UPDATED_HASHCODE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .instant(UPDATED_INSTANT);
        CornerDTO cornerDTO = cornerMapper.toDto(updatedCorner);

        restCornerMockMvc.perform(put("/api/corners")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(cornerDTO)))
            .andExpect(status().isOk());

        // Validate the Corner in the database
        List<Corner> cornerList = cornerRepository.findAll();
        assertThat(cornerList).hasSize(databaseSizeBeforeUpdate);
        Corner testCorner = cornerList.get(cornerList.size() - 1);
        assertThat(testCorner.getEquipe()).isEqualTo(UPDATED_EQUIPE);
        assertThat(testCorner.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testCorner.getHashcode()).isEqualTo(UPDATED_HASHCODE);
        assertThat(testCorner.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testCorner.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testCorner.getInstant()).isEqualTo(UPDATED_INSTANT);

        // Validate the Corner in Elasticsearch
        verify(mockCornerSearchRepository, times(1)).save(testCorner);
    }

    @Test
    @Transactional
    public void updateNonExistingCorner() throws Exception {
        int databaseSizeBeforeUpdate = cornerRepository.findAll().size();

        // Create the Corner
        CornerDTO cornerDTO = cornerMapper.toDto(corner);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCornerMockMvc.perform(put("/api/corners")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(cornerDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Corner in the database
        List<Corner> cornerList = cornerRepository.findAll();
        assertThat(cornerList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Corner in Elasticsearch
        verify(mockCornerSearchRepository, times(0)).save(corner);
    }

    @Test
    @Transactional
    public void deleteCorner() throws Exception {
        // Initialize the database
        cornerRepository.saveAndFlush(corner);

        int databaseSizeBeforeDelete = cornerRepository.findAll().size();

        // Delete the corner
        restCornerMockMvc.perform(delete("/api/corners/{id}", corner.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Corner> cornerList = cornerRepository.findAll();
        assertThat(cornerList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Corner in Elasticsearch
        verify(mockCornerSearchRepository, times(1)).deleteById(corner.getId());
    }

    @Test
    @Transactional
    public void searchCorner() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        cornerRepository.saveAndFlush(corner);
        when(mockCornerSearchRepository.search(queryStringQuery("id:" + corner.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(corner), PageRequest.of(0, 1), 1));

        // Search the corner
        restCornerMockMvc.perform(get("/api/_search/corners?query=id:" + corner.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(corner.getId().intValue())))
            .andExpect(jsonPath("$.[*].equipe").value(hasItem(DEFAULT_EQUIPE.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].hashcode").value(hasItem(DEFAULT_HASHCODE)))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].instant").value(hasItem(DEFAULT_INSTANT)));
    }
}
