package org.nddngroup.navetfoot.core.web.rest;

import org.nddngroup.navetfoot.core.NavetfootCore;
import org.nddngroup.navetfoot.core.domain.Saison;
import org.nddngroup.navetfoot.core.domain.Organisation;
import org.nddngroup.navetfoot.core.repository.SaisonRepository;
import org.nddngroup.navetfoot.core.repository.search.SaisonSearchRepository;
import org.nddngroup.navetfoot.core.service.SaisonService;
import org.nddngroup.navetfoot.core.service.dto.SaisonDTO;
import org.nddngroup.navetfoot.core.service.mapper.SaisonMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.nddngroup.navetfoot.core.repository.search.SaisonSearchRepositoryMockConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static org.nddngroup.navetfoot.core.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link SaisonResource} REST controller.
 */
@SpringBootTest(classes = NavetfootCore.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class SaisonResourceIT {

    private static final String DEFAULT_LIBELLE = "AAAAAAAAAA";
    private static final String UPDATED_LIBELLE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATE_DEBUT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_DEBUT = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_DATE_FIN = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_FIN = LocalDate.now(ZoneId.systemDefault());

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Boolean DEFAULT_DELETED = false;
    private static final Boolean UPDATED_DELETED = true;

    private static final String DEFAULT_HASHCODE = "AAAAAAAAAA";
    private static final String UPDATED_HASHCODE = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    @Autowired
    private SaisonRepository saisonRepository;

    @Autowired
    private SaisonMapper saisonMapper;

    @Autowired
    private SaisonService saisonService;

    /**
     * This repository is mocked in the org.nddngroup.navetfoot.core.repository.search test package.
     *
     * @see SaisonSearchRepositoryMockConfiguration
     */
    @Autowired
    private SaisonSearchRepository mockSaisonSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restSaisonMockMvc;

    private Saison saison;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Saison createEntity(EntityManager em) {
        Saison saison = new Saison()
            .libelle(DEFAULT_LIBELLE)
            .dateDebut(DEFAULT_DATE_DEBUT)
            .dateFin(DEFAULT_DATE_FIN)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT)
            .deleted(DEFAULT_DELETED)
            .hashcode(DEFAULT_HASHCODE)
            .code(DEFAULT_CODE);
        // Add required entity
        Organisation organisation;
        if (TestUtil.findAll(em, Organisation.class).isEmpty()) {
            organisation = OrganisationResourceIT.createEntity(em);
            em.persist(organisation);
            em.flush();
        } else {
            organisation = TestUtil.findAll(em, Organisation.class).get(0);
        }
        saison.setOrganisation(organisation);
        return saison;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Saison createUpdatedEntity(EntityManager em) {
        Saison saison = new Saison()
            .libelle(UPDATED_LIBELLE)
            .dateDebut(UPDATED_DATE_DEBUT)
            .dateFin(UPDATED_DATE_FIN)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .deleted(UPDATED_DELETED)
            .hashcode(UPDATED_HASHCODE)
            .code(UPDATED_CODE);
        // Add required entity
        Organisation organisation;
        if (TestUtil.findAll(em, Organisation.class).isEmpty()) {
            organisation = OrganisationResourceIT.createUpdatedEntity(em);
            em.persist(organisation);
            em.flush();
        } else {
            organisation = TestUtil.findAll(em, Organisation.class).get(0);
        }
        saison.setOrganisation(organisation);
        return saison;
    }

    @BeforeEach
    public void initTest() {
        saison = createEntity(em);
    }

    @Test
    @Transactional
    public void createSaison() throws Exception {
        int databaseSizeBeforeCreate = saisonRepository.findAll().size();
        // Create the Saison
        SaisonDTO saisonDTO = saisonMapper.toDto(saison);
        restSaisonMockMvc.perform(post("/api/saisons")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(saisonDTO)))
            .andExpect(status().isCreated());

        // Validate the Saison in the database
        List<Saison> saisonList = saisonRepository.findAll();
        assertThat(saisonList).hasSize(databaseSizeBeforeCreate + 1);
        Saison testSaison = saisonList.get(saisonList.size() - 1);
        assertThat(testSaison.getLibelle()).isEqualTo(DEFAULT_LIBELLE);
        assertThat(testSaison.getDateDebut()).isEqualTo(DEFAULT_DATE_DEBUT);
        assertThat(testSaison.getDateFin()).isEqualTo(DEFAULT_DATE_FIN);
        assertThat(testSaison.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testSaison.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testSaison.isDeleted()).isEqualTo(DEFAULT_DELETED);
        assertThat(testSaison.getHashcode()).isEqualTo(DEFAULT_HASHCODE);
        assertThat(testSaison.getCode()).isEqualTo(DEFAULT_CODE);

        // Validate the Saison in Elasticsearch
        verify(mockSaisonSearchRepository, times(1)).save(testSaison);
    }

    @Test
    @Transactional
    public void createSaisonWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = saisonRepository.findAll().size();

        // Create the Saison with an existing ID
        saison.setId(1L);
        SaisonDTO saisonDTO = saisonMapper.toDto(saison);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSaisonMockMvc.perform(post("/api/saisons")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(saisonDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Saison in the database
        List<Saison> saisonList = saisonRepository.findAll();
        assertThat(saisonList).hasSize(databaseSizeBeforeCreate);

        // Validate the Saison in Elasticsearch
        verify(mockSaisonSearchRepository, times(0)).save(saison);
    }


    @Test
    @Transactional
    public void checkLibelleIsRequired() throws Exception {
        int databaseSizeBeforeTest = saisonRepository.findAll().size();
        // set the field null
        saison.setLibelle(null);

        // Create the Saison, which fails.
        SaisonDTO saisonDTO = saisonMapper.toDto(saison);


        restSaisonMockMvc.perform(post("/api/saisons")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(saisonDTO)))
            .andExpect(status().isBadRequest());

        List<Saison> saisonList = saisonRepository.findAll();
        assertThat(saisonList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkHashcodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = saisonRepository.findAll().size();
        // set the field null
        saison.setHashcode(null);

        // Create the Saison, which fails.
        SaisonDTO saisonDTO = saisonMapper.toDto(saison);


        restSaisonMockMvc.perform(post("/api/saisons")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(saisonDTO)))
            .andExpect(status().isBadRequest());

        List<Saison> saisonList = saisonRepository.findAll();
        assertThat(saisonList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = saisonRepository.findAll().size();
        // set the field null
        saison.setCode(null);

        // Create the Saison, which fails.
        SaisonDTO saisonDTO = saisonMapper.toDto(saison);


        restSaisonMockMvc.perform(post("/api/saisons")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(saisonDTO)))
            .andExpect(status().isBadRequest());

        List<Saison> saisonList = saisonRepository.findAll();
        assertThat(saisonList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSaisons() throws Exception {
        // Initialize the database
        saisonRepository.saveAndFlush(saison);

        // Get all the saisonList
        restSaisonMockMvc.perform(get("/api/saisons?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(saison.getId().intValue())))
            .andExpect(jsonPath("$.[*].libelle").value(hasItem(DEFAULT_LIBELLE)))
            .andExpect(jsonPath("$.[*].dateDebut").value(hasItem(DEFAULT_DATE_DEBUT.toString())))
            .andExpect(jsonPath("$.[*].dateFin").value(hasItem(DEFAULT_DATE_FIN.toString())))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].hashcode").value(hasItem(DEFAULT_HASHCODE)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)));
    }

    @Test
    @Transactional
    public void getSaison() throws Exception {
        // Initialize the database
        saisonRepository.saveAndFlush(saison);

        // Get the saison
        restSaisonMockMvc.perform(get("/api/saisons/{id}", saison.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(saison.getId().intValue()))
            .andExpect(jsonPath("$.libelle").value(DEFAULT_LIBELLE))
            .andExpect(jsonPath("$.dateDebut").value(DEFAULT_DATE_DEBUT.toString()))
            .andExpect(jsonPath("$.dateFin").value(DEFAULT_DATE_FIN.toString()))
            .andExpect(jsonPath("$.createdAt").value(sameInstant(DEFAULT_CREATED_AT)))
            .andExpect(jsonPath("$.updatedAt").value(sameInstant(DEFAULT_UPDATED_AT)))
            .andExpect(jsonPath("$.deleted").value(DEFAULT_DELETED.booleanValue()))
            .andExpect(jsonPath("$.hashcode").value(DEFAULT_HASHCODE))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE));
    }
    @Test
    @Transactional
    public void getNonExistingSaison() throws Exception {
        // Get the saison
        restSaisonMockMvc.perform(get("/api/saisons/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSaison() throws Exception {
        // Initialize the database
        saisonRepository.saveAndFlush(saison);

        int databaseSizeBeforeUpdate = saisonRepository.findAll().size();

        // Update the saison
        Saison updatedSaison = saisonRepository.findById(saison.getId()).get();
        // Disconnect from session so that the updates on updatedSaison are not directly saved in db
        em.detach(updatedSaison);
        updatedSaison
            .libelle(UPDATED_LIBELLE)
            .dateDebut(UPDATED_DATE_DEBUT)
            .dateFin(UPDATED_DATE_FIN)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .hashcode(UPDATED_HASHCODE)
            .code(UPDATED_CODE);
        SaisonDTO saisonDTO = saisonMapper.toDto(updatedSaison);

        restSaisonMockMvc.perform(put("/api/saisons")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(saisonDTO)))
            .andExpect(status().isOk());

        // Validate the Saison in the database
        List<Saison> saisonList = saisonRepository.findAll();
        assertThat(saisonList).hasSize(databaseSizeBeforeUpdate);
        Saison testSaison = saisonList.get(saisonList.size() - 1);
        assertThat(testSaison.getLibelle()).isEqualTo(UPDATED_LIBELLE);
        assertThat(testSaison.getDateDebut()).isEqualTo(UPDATED_DATE_DEBUT);
        assertThat(testSaison.getDateFin()).isEqualTo(UPDATED_DATE_FIN);
        assertThat(testSaison.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testSaison.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testSaison.getHashcode()).isEqualTo(UPDATED_HASHCODE);
        assertThat(testSaison.getCode()).isEqualTo(UPDATED_CODE);

        // Validate the Saison in Elasticsearch
        verify(mockSaisonSearchRepository, times(1)).save(testSaison);
    }

    @Test
    @Transactional
    public void updateNonExistingSaison() throws Exception {
        int databaseSizeBeforeUpdate = saisonRepository.findAll().size();

        // Create the Saison
        SaisonDTO saisonDTO = saisonMapper.toDto(saison);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSaisonMockMvc.perform(put("/api/saisons")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(saisonDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Saison in the database
        List<Saison> saisonList = saisonRepository.findAll();
        assertThat(saisonList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Saison in Elasticsearch
        verify(mockSaisonSearchRepository, times(0)).save(saison);
    }

    @Test
    @Transactional
    public void deleteSaison() throws Exception {
        // Initialize the database
        saisonRepository.saveAndFlush(saison);

        int databaseSizeBeforeDelete = saisonRepository.findAll().size();

        // Delete the saison
        restSaisonMockMvc.perform(delete("/api/saisons/{id}", saison.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Saison> saisonList = saisonRepository.findAll();
        assertThat(saisonList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Saison in Elasticsearch
        verify(mockSaisonSearchRepository, times(1)).deleteById(saison.getId());
    }

    @Test
    @Transactional
    public void searchSaison() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        saisonRepository.saveAndFlush(saison);
        when(mockSaisonSearchRepository.search(queryStringQuery("id:" + saison.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(saison), PageRequest.of(0, 1), 1));

        // Search the saison
        restSaisonMockMvc.perform(get("/api/_search/saisons?query=id:" + saison.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(saison.getId().intValue())))
            .andExpect(jsonPath("$.[*].libelle").value(hasItem(DEFAULT_LIBELLE)))
            .andExpect(jsonPath("$.[*].dateDebut").value(hasItem(DEFAULT_DATE_DEBUT.toString())))
            .andExpect(jsonPath("$.[*].dateFin").value(hasItem(DEFAULT_DATE_FIN.toString())))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].hashcode").value(hasItem(DEFAULT_HASHCODE)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)));
    }
}
