package org.nddngroup.navetfoot.core.web.rest;

import org.nddngroup.navetfoot.core.NavetfootCore;
import org.nddngroup.navetfoot.core.domain.TirAuBut;
import org.nddngroup.navetfoot.core.repository.TirAuButRepository;
import org.nddngroup.navetfoot.core.repository.search.TirAuButSearchRepository;
import org.nddngroup.navetfoot.core.service.TirAuButService;
import org.nddngroup.navetfoot.core.service.dto.TirAuButDTO;
import org.nddngroup.navetfoot.core.service.mapper.TirAuButMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.nddngroup.navetfoot.core.repository.search.TirAuButSearchRepositoryMockConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static org.nddngroup.navetfoot.core.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TirAuButResource} REST controller.
 */
@SpringBootTest(classes = NavetfootCore.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class TirAuButResourceIT {

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_HASHCODE = "AAAAAAAAAA";
    private static final String UPDATED_HASHCODE = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Boolean DEFAULT_DELETED = false;
    private static final Boolean UPDATED_DELETED = true;

    @Autowired
    private TirAuButRepository tirAuButRepository;

    @Autowired
    private TirAuButMapper tirAuButMapper;

    @Autowired
    private TirAuButService tirAuButService;

    /**
     * This repository is mocked in the org.nddngroup.navetfoot.core.repository.search test package.
     *
     * @see TirAuButSearchRepositoryMockConfiguration
     */
    @Autowired
    private TirAuButSearchRepository mockTirAuButSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restTirAuButMockMvc;

    private TirAuBut tirAuBut;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TirAuBut createEntity(EntityManager em) {
        TirAuBut tirAuBut = new TirAuBut()
            .code(DEFAULT_CODE)
            .hashcode(DEFAULT_HASHCODE)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT)
            .deleted(DEFAULT_DELETED);
        return tirAuBut;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TirAuBut createUpdatedEntity(EntityManager em) {
        TirAuBut tirAuBut = new TirAuBut()
            .code(UPDATED_CODE)
            .hashcode(UPDATED_HASHCODE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .deleted(UPDATED_DELETED);
        return tirAuBut;
    }

    @BeforeEach
    public void initTest() {
        tirAuBut = createEntity(em);
    }

    @Test
    @Transactional
    public void createTirAuBut() throws Exception {
        int databaseSizeBeforeCreate = tirAuButRepository.findAll().size();
        // Create the TirAuBut
        TirAuButDTO tirAuButDTO = tirAuButMapper.toDto(tirAuBut);
        restTirAuButMockMvc.perform(post("/api/tir-au-buts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tirAuButDTO)))
            .andExpect(status().isCreated());

        // Validate the TirAuBut in the database
        List<TirAuBut> tirAuButList = tirAuButRepository.findAll();
        assertThat(tirAuButList).hasSize(databaseSizeBeforeCreate + 1);
        TirAuBut testTirAuBut = tirAuButList.get(tirAuButList.size() - 1);
        assertThat(testTirAuBut.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testTirAuBut.getHashcode()).isEqualTo(DEFAULT_HASHCODE);
        assertThat(testTirAuBut.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testTirAuBut.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testTirAuBut.isDeleted()).isEqualTo(DEFAULT_DELETED);

        // Validate the TirAuBut in Elasticsearch
        verify(mockTirAuButSearchRepository, times(1)).save(testTirAuBut);
    }

    @Test
    @Transactional
    public void createTirAuButWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tirAuButRepository.findAll().size();

        // Create the TirAuBut with an existing ID
        tirAuBut.setId(1L);
        TirAuButDTO tirAuButDTO = tirAuButMapper.toDto(tirAuBut);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTirAuButMockMvc.perform(post("/api/tir-au-buts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tirAuButDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TirAuBut in the database
        List<TirAuBut> tirAuButList = tirAuButRepository.findAll();
        assertThat(tirAuButList).hasSize(databaseSizeBeforeCreate);

        // Validate the TirAuBut in Elasticsearch
        verify(mockTirAuButSearchRepository, times(0)).save(tirAuBut);
    }


    @Test
    @Transactional
    public void getAllTirAuButs() throws Exception {
        // Initialize the database
        tirAuButRepository.saveAndFlush(tirAuBut);

        // Get all the tirAuButList
        restTirAuButMockMvc.perform(get("/api/tir-au-buts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tirAuBut.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].hashcode").value(hasItem(DEFAULT_HASHCODE)))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())));
    }

    @Test
    @Transactional
    public void getTirAuBut() throws Exception {
        // Initialize the database
        tirAuButRepository.saveAndFlush(tirAuBut);

        // Get the tirAuBut
        restTirAuButMockMvc.perform(get("/api/tir-au-buts/{id}", tirAuBut.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(tirAuBut.getId().intValue()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE))
            .andExpect(jsonPath("$.hashcode").value(DEFAULT_HASHCODE))
            .andExpect(jsonPath("$.createdAt").value(sameInstant(DEFAULT_CREATED_AT)))
            .andExpect(jsonPath("$.updatedAt").value(sameInstant(DEFAULT_UPDATED_AT)))
            .andExpect(jsonPath("$.deleted").value(DEFAULT_DELETED.booleanValue()));
    }
    @Test
    @Transactional
    public void getNonExistingTirAuBut() throws Exception {
        // Get the tirAuBut
        restTirAuButMockMvc.perform(get("/api/tir-au-buts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTirAuBut() throws Exception {
        // Initialize the database
        tirAuButRepository.saveAndFlush(tirAuBut);

        int databaseSizeBeforeUpdate = tirAuButRepository.findAll().size();

        // Update the tirAuBut
        TirAuBut updatedTirAuBut = tirAuButRepository.findById(tirAuBut.getId()).get();
        // Disconnect from session so that the updates on updatedTirAuBut are not directly saved in db
        em.detach(updatedTirAuBut);
        updatedTirAuBut
            .code(UPDATED_CODE)
            .hashcode(UPDATED_HASHCODE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT);
        TirAuButDTO tirAuButDTO = tirAuButMapper.toDto(updatedTirAuBut);

        restTirAuButMockMvc.perform(put("/api/tir-au-buts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tirAuButDTO)))
            .andExpect(status().isOk());

        // Validate the TirAuBut in the database
        List<TirAuBut> tirAuButList = tirAuButRepository.findAll();
        assertThat(tirAuButList).hasSize(databaseSizeBeforeUpdate);
        TirAuBut testTirAuBut = tirAuButList.get(tirAuButList.size() - 1);
        assertThat(testTirAuBut.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testTirAuBut.getHashcode()).isEqualTo(UPDATED_HASHCODE);
        assertThat(testTirAuBut.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testTirAuBut.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);

        // Validate the TirAuBut in Elasticsearch
        verify(mockTirAuButSearchRepository, times(1)).save(testTirAuBut);
    }

    @Test
    @Transactional
    public void updateNonExistingTirAuBut() throws Exception {
        int databaseSizeBeforeUpdate = tirAuButRepository.findAll().size();

        // Create the TirAuBut
        TirAuButDTO tirAuButDTO = tirAuButMapper.toDto(tirAuBut);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTirAuButMockMvc.perform(put("/api/tir-au-buts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tirAuButDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TirAuBut in the database
        List<TirAuBut> tirAuButList = tirAuButRepository.findAll();
        assertThat(tirAuButList).hasSize(databaseSizeBeforeUpdate);

        // Validate the TirAuBut in Elasticsearch
        verify(mockTirAuButSearchRepository, times(0)).save(tirAuBut);
    }

    @Test
    @Transactional
    public void deleteTirAuBut() throws Exception {
        // Initialize the database
        tirAuButRepository.saveAndFlush(tirAuBut);

        int databaseSizeBeforeDelete = tirAuButRepository.findAll().size();

        // Delete the tirAuBut
        restTirAuButMockMvc.perform(delete("/api/tir-au-buts/{id}", tirAuBut.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TirAuBut> tirAuButList = tirAuButRepository.findAll();
        assertThat(tirAuButList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the TirAuBut in Elasticsearch
        verify(mockTirAuButSearchRepository, times(1)).deleteById(tirAuBut.getId());
    }

    @Test
    @Transactional
    public void searchTirAuBut() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        tirAuButRepository.saveAndFlush(tirAuBut);
        when(mockTirAuButSearchRepository.search(queryStringQuery("id:" + tirAuBut.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(tirAuBut), PageRequest.of(0, 1), 1));

        // Search the tirAuBut
        restTirAuButMockMvc.perform(get("/api/_search/tir-au-buts?query=id:" + tirAuBut.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tirAuBut.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].hashcode").value(hasItem(DEFAULT_HASHCODE)))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())));
    }
}
