package org.nddngroup.navetfoot.core.web.rest;

import org.hamcrest.Matchers;
import org.nddngroup.navetfoot.core.NavetfootCore;
import org.nddngroup.navetfoot.core.domain.Penalty;
import org.nddngroup.navetfoot.core.repository.PenaltyRepository;
import org.nddngroup.navetfoot.core.repository.search.PenaltySearchRepository;
import org.nddngroup.navetfoot.core.service.PenaltyService;
import org.nddngroup.navetfoot.core.service.dto.PenaltyDTO;
import org.nddngroup.navetfoot.core.service.mapper.PenaltyMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.nddngroup.navetfoot.core.repository.search.PenaltySearchRepositoryMockConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.nddngroup.navetfoot.core.domain.enumeration.Issu;
import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;
/**
 * Integration tests for the {@link PenaltyResource} REST controller.
 */
@SpringBootTest(classes = NavetfootCore.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class PenaltyResourceIT {

    private static final Issu DEFAULT_ISSU = Issu.MARQUE;
    private static final Issu UPDATED_ISSU = Issu.RATE;

    private static final Equipe DEFAULT_EQUIPE = Equipe.LOCAL;
    private static final Equipe UPDATED_EQUIPE = Equipe.VISITEUR;

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_HASHCODE = "AAAAAAAAAA";
    private static final String UPDATED_HASHCODE = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Boolean DEFAULT_DELETED = false;
    private static final Boolean UPDATED_DELETED = true;

    private static final Integer DEFAULT_INSTANT = 1;
    private static final Integer UPDATED_INSTANT = 2;

    @Autowired
    private PenaltyRepository penaltyRepository;

    @Autowired
    private PenaltyMapper penaltyMapper;

    @Autowired
    private PenaltyService penaltyService;

    /**
     * This repository is mocked in the org.nddngroup.navetfoot.core.repository.search test package.
     *
     * @see PenaltySearchRepositoryMockConfiguration
     */
    @Autowired
    private PenaltySearchRepository mockPenaltySearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPenaltyMockMvc;

    private Penalty penalty;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Penalty createEntity(EntityManager em) {
        Penalty penalty = new Penalty()
            .issu(DEFAULT_ISSU)
            .equipe(DEFAULT_EQUIPE)
            .code(DEFAULT_CODE)
            .hashcode(DEFAULT_HASHCODE)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT)
            .deleted(DEFAULT_DELETED)
            .instant(DEFAULT_INSTANT);
        return penalty;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Penalty createUpdatedEntity(EntityManager em) {
        Penalty penalty = new Penalty()
            .issu(UPDATED_ISSU)
            .equipe(UPDATED_EQUIPE)
            .code(UPDATED_CODE)
            .hashcode(UPDATED_HASHCODE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .deleted(UPDATED_DELETED)
            .instant(UPDATED_INSTANT);
        return penalty;
    }

    @BeforeEach
    public void initTest() {
        penalty = createEntity(em);
    }

    @Test
    @Transactional
    public void createPenalty() throws Exception {
        int databaseSizeBeforeCreate = penaltyRepository.findAll().size();
        // Create the Penalty
        PenaltyDTO penaltyDTO = penaltyMapper.toDto(penalty);
        restPenaltyMockMvc.perform(post("/api/penalties")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(penaltyDTO)))
            .andExpect(status().isCreated());

        // Validate the Penalty in the database
        List<Penalty> penaltyList = penaltyRepository.findAll();
        assertThat(penaltyList).hasSize(databaseSizeBeforeCreate + 1);
        Penalty testPenalty = penaltyList.get(penaltyList.size() - 1);
        assertThat(testPenalty.getIssu()).isEqualTo(DEFAULT_ISSU);
        assertThat(testPenalty.getEquipe()).isEqualTo(DEFAULT_EQUIPE);
        assertThat(testPenalty.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testPenalty.getHashcode()).isEqualTo(DEFAULT_HASHCODE);
        assertThat(testPenalty.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testPenalty.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testPenalty.isDeleted()).isEqualTo(DEFAULT_DELETED);
        assertThat(testPenalty.getInstant()).isEqualTo(DEFAULT_INSTANT);

        // Validate the Penalty in Elasticsearch
        verify(mockPenaltySearchRepository, times(1)).save(testPenalty);
    }

    @Test
    @Transactional
    public void createPenaltyWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = penaltyRepository.findAll().size();

        // Create the Penalty with an existing ID
        penalty.setId(1L);
        PenaltyDTO penaltyDTO = penaltyMapper.toDto(penalty);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPenaltyMockMvc.perform(post("/api/penalties")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(penaltyDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Penalty in the database
        List<Penalty> penaltyList = penaltyRepository.findAll();
        assertThat(penaltyList).hasSize(databaseSizeBeforeCreate);

        // Validate the Penalty in Elasticsearch
        verify(mockPenaltySearchRepository, times(0)).save(penalty);
    }


    @Test
    @Transactional
    public void getAllPenalties() throws Exception {
        // Initialize the database
        penaltyRepository.saveAndFlush(penalty);

        // Get all the penaltyList
        restPenaltyMockMvc.perform(get("/api/penalties?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(penalty.getId().intValue())))
            .andExpect(jsonPath("$.[*].issu").value(hasItem(DEFAULT_ISSU.toString())))
            .andExpect(jsonPath("$.[*].equipe").value(hasItem(DEFAULT_EQUIPE.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].hashcode").value(hasItem(DEFAULT_HASHCODE)))
            .andExpect(jsonPath("$.[*].createdAt").value(Matchers.hasItem(TestUtil.sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(Matchers.hasItem(TestUtil.sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].instant").value(hasItem(DEFAULT_INSTANT)));
    }

    @Test
    @Transactional
    public void getPenalty() throws Exception {
        // Initialize the database
        penaltyRepository.saveAndFlush(penalty);

        // Get the penalty
        restPenaltyMockMvc.perform(get("/api/penalties/{id}", penalty.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(penalty.getId().intValue()))
            .andExpect(jsonPath("$.issu").value(DEFAULT_ISSU.toString()))
            .andExpect(jsonPath("$.equipe").value(DEFAULT_EQUIPE.toString()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE))
            .andExpect(jsonPath("$.hashcode").value(DEFAULT_HASHCODE))
            .andExpect(jsonPath("$.createdAt").value(TestUtil.sameInstant(DEFAULT_CREATED_AT)))
            .andExpect(jsonPath("$.updatedAt").value(TestUtil.sameInstant(DEFAULT_UPDATED_AT)))
            .andExpect(jsonPath("$.deleted").value(DEFAULT_DELETED.booleanValue()))
            .andExpect(jsonPath("$.instant").value(DEFAULT_INSTANT));
    }
    @Test
    @Transactional
    public void getNonExistingPenalty() throws Exception {
        // Get the penalty
        restPenaltyMockMvc.perform(get("/api/penalties/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePenalty() throws Exception {
        // Initialize the database
        penaltyRepository.saveAndFlush(penalty);

        int databaseSizeBeforeUpdate = penaltyRepository.findAll().size();

        // Update the penalty
        Penalty updatedPenalty = penaltyRepository.findById(penalty.getId()).get();
        // Disconnect from session so that the updates on updatedPenalty are not directly saved in db
        em.detach(updatedPenalty);
        updatedPenalty
            .issu(UPDATED_ISSU)
            .equipe(UPDATED_EQUIPE)
            .code(UPDATED_CODE)
            .hashcode(UPDATED_HASHCODE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .instant(UPDATED_INSTANT);
        PenaltyDTO penaltyDTO = penaltyMapper.toDto(updatedPenalty);

        restPenaltyMockMvc.perform(put("/api/penalties")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(penaltyDTO)))
            .andExpect(status().isOk());

        // Validate the Penalty in the database
        List<Penalty> penaltyList = penaltyRepository.findAll();
        assertThat(penaltyList).hasSize(databaseSizeBeforeUpdate);
        Penalty testPenalty = penaltyList.get(penaltyList.size() - 1);
        assertThat(testPenalty.getIssu()).isEqualTo(UPDATED_ISSU);
        assertThat(testPenalty.getEquipe()).isEqualTo(UPDATED_EQUIPE);
        assertThat(testPenalty.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testPenalty.getHashcode()).isEqualTo(UPDATED_HASHCODE);
        assertThat(testPenalty.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testPenalty.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testPenalty.getInstant()).isEqualTo(UPDATED_INSTANT);

        // Validate the Penalty in Elasticsearch
        verify(mockPenaltySearchRepository, times(1)).save(testPenalty);
    }

    @Test
    @Transactional
    public void updateNonExistingPenalty() throws Exception {
        int databaseSizeBeforeUpdate = penaltyRepository.findAll().size();

        // Create the Penalty
        PenaltyDTO penaltyDTO = penaltyMapper.toDto(penalty);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPenaltyMockMvc.perform(put("/api/penalties")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(penaltyDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Penalty in the database
        List<Penalty> penaltyList = penaltyRepository.findAll();
        assertThat(penaltyList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Penalty in Elasticsearch
        verify(mockPenaltySearchRepository, times(0)).save(penalty);
    }

    @Test
    @Transactional
    public void deletePenalty() throws Exception {
        // Initialize the database
        penaltyRepository.saveAndFlush(penalty);

        int databaseSizeBeforeDelete = penaltyRepository.findAll().size();

        // Delete the penalty
        restPenaltyMockMvc.perform(delete("/api/penalties/{id}", penalty.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Penalty> penaltyList = penaltyRepository.findAll();
        assertThat(penaltyList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Penalty in Elasticsearch
        verify(mockPenaltySearchRepository, times(1)).deleteById(penalty.getId());
    }

    @Test
    @Transactional
    public void searchPenalty() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        penaltyRepository.saveAndFlush(penalty);
        when(mockPenaltySearchRepository.search(queryStringQuery("id:" + penalty.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(penalty), PageRequest.of(0, 1), 1));

        // Search the penalty
        restPenaltyMockMvc.perform(get("/api/_search/penalties?query=id:" + penalty.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(penalty.getId().intValue())))
            .andExpect(jsonPath("$.[*].issu").value(hasItem(DEFAULT_ISSU.toString())))
            .andExpect(jsonPath("$.[*].equipe").value(hasItem(DEFAULT_EQUIPE.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].hashcode").value(hasItem(DEFAULT_HASHCODE)))
            .andExpect(jsonPath("$.[*].createdAt").value(Matchers.hasItem(TestUtil.sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(Matchers.hasItem(TestUtil.sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].instant").value(hasItem(DEFAULT_INSTANT)));
    }
}
