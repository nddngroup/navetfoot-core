package org.nddngroup.navetfoot.core.web.rest;

import org.hamcrest.Matchers;
import org.nddngroup.navetfoot.core.NavetfootCore;
import org.nddngroup.navetfoot.core.domain.Delegue;
import org.nddngroup.navetfoot.core.repository.DelegueRepository;
import org.nddngroup.navetfoot.core.repository.search.DelegueSearchRepository;
import org.nddngroup.navetfoot.core.service.DelegueService;
import org.nddngroup.navetfoot.core.service.dto.DelegueDTO;
import org.nddngroup.navetfoot.core.service.mapper.DelegueMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.nddngroup.navetfoot.core.repository.search.DelegueSearchRepositoryMockConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link DelegueResource} REST controller.
 */
@SpringBootTest(classes = NavetfootCore.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class DelegueResourceIT {

    private static final String DEFAULT_HASHCODE = "AAAAAAAAAA";
    private static final String UPDATED_HASHCODE = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_PRENOM = "AAAAAAAAAA";
    private static final String UPDATED_PRENOM = "BBBBBBBBBB";

    private static final String DEFAULT_NOM = "AAAAAAAAAA";
    private static final String UPDATED_NOM = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_TELEPHONE = "AAAAAAAAAA";
    private static final String UPDATED_TELEPHONE = "BBBBBBBBBB";

    private static final Boolean DEFAULT_DELETED = false;
    private static final Boolean UPDATED_DELETED = true;

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_ADRESSE = "AAAAAAAAAA";
    private static final String UPDATED_ADRESSE = "BBBBBBBBBB";

    private static final String DEFAULT_IMAGE_URL = "AAAAAAAAAA";
    private static final String UPDATED_IMAGE_URL = "BBBBBBBBBB";

    @Autowired
    private DelegueRepository delegueRepository;

    @Autowired
    private DelegueMapper delegueMapper;

    @Autowired
    private DelegueService delegueService;

    /**
     * This repository is mocked in the org.nddngroup.navetfoot.core.repository.search test package.
     *
     * @see DelegueSearchRepositoryMockConfiguration
     */
    @Autowired
    private DelegueSearchRepository mockDelegueSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDelegueMockMvc;

    private Delegue delegue;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Delegue createEntity(EntityManager em) {
        Delegue delegue = new Delegue()
            .hashcode(DEFAULT_HASHCODE)
            .code(DEFAULT_CODE)
            .prenom(DEFAULT_PRENOM)
            .nom(DEFAULT_NOM)
            .email(DEFAULT_EMAIL)
            .telephone(DEFAULT_TELEPHONE)
            .deleted(DEFAULT_DELETED)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT)
            .adresse(DEFAULT_ADRESSE)
            .imageUrl(DEFAULT_IMAGE_URL);
        return delegue;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Delegue createUpdatedEntity(EntityManager em) {
        Delegue delegue = new Delegue()
            .hashcode(UPDATED_HASHCODE)
            .code(UPDATED_CODE)
            .prenom(UPDATED_PRENOM)
            .nom(UPDATED_NOM)
            .email(UPDATED_EMAIL)
            .telephone(UPDATED_TELEPHONE)
            .deleted(UPDATED_DELETED)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .adresse(UPDATED_ADRESSE)
            .imageUrl(UPDATED_IMAGE_URL);
        return delegue;
    }

    @BeforeEach
    public void initTest() {
        delegue = createEntity(em);
    }

    @Test
    @Transactional
    public void createDelegue() throws Exception {
        int databaseSizeBeforeCreate = delegueRepository.findAll().size();
        // Create the Delegue
        DelegueDTO delegueDTO = delegueMapper.toDto(delegue);
        restDelegueMockMvc.perform(post("/api/delegues")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(delegueDTO)))
            .andExpect(status().isCreated());

        // Validate the Delegue in the database
        List<Delegue> delegueList = delegueRepository.findAll();
        assertThat(delegueList).hasSize(databaseSizeBeforeCreate + 1);
        Delegue testDelegue = delegueList.get(delegueList.size() - 1);
        assertThat(testDelegue.getHashcode()).isEqualTo(DEFAULT_HASHCODE);
        assertThat(testDelegue.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testDelegue.getPrenom()).isEqualTo(DEFAULT_PRENOM);
        assertThat(testDelegue.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testDelegue.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testDelegue.getTelephone()).isEqualTo(DEFAULT_TELEPHONE);
        assertThat(testDelegue.isDeleted()).isEqualTo(DEFAULT_DELETED);
        assertThat(testDelegue.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testDelegue.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testDelegue.getAdresse()).isEqualTo(DEFAULT_ADRESSE);
        assertThat(testDelegue.getImageUrl()).isEqualTo(DEFAULT_IMAGE_URL);

        // Validate the Delegue in Elasticsearch
        verify(mockDelegueSearchRepository, times(1)).save(testDelegue);
    }

    @Test
    @Transactional
    public void createDelegueWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = delegueRepository.findAll().size();

        // Create the Delegue with an existing ID
        delegue.setId(1L);
        DelegueDTO delegueDTO = delegueMapper.toDto(delegue);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDelegueMockMvc.perform(post("/api/delegues")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(delegueDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Delegue in the database
        List<Delegue> delegueList = delegueRepository.findAll();
        assertThat(delegueList).hasSize(databaseSizeBeforeCreate);

        // Validate the Delegue in Elasticsearch
        verify(mockDelegueSearchRepository, times(0)).save(delegue);
    }


    @Test
    @Transactional
    public void checkHashcodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = delegueRepository.findAll().size();
        // set the field null
        delegue.setHashcode(null);

        // Create the Delegue, which fails.
        DelegueDTO delegueDTO = delegueMapper.toDto(delegue);


        restDelegueMockMvc.perform(post("/api/delegues")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(delegueDTO)))
            .andExpect(status().isBadRequest());

        List<Delegue> delegueList = delegueRepository.findAll();
        assertThat(delegueList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = delegueRepository.findAll().size();
        // set the field null
        delegue.setCode(null);

        // Create the Delegue, which fails.
        DelegueDTO delegueDTO = delegueMapper.toDto(delegue);


        restDelegueMockMvc.perform(post("/api/delegues")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(delegueDTO)))
            .andExpect(status().isBadRequest());

        List<Delegue> delegueList = delegueRepository.findAll();
        assertThat(delegueList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDelegues() throws Exception {
        // Initialize the database
        delegueRepository.saveAndFlush(delegue);

        // Get all the delegueList
        restDelegueMockMvc.perform(get("/api/delegues?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(delegue.getId().intValue())))
            .andExpect(jsonPath("$.[*].hashcode").value(hasItem(DEFAULT_HASHCODE)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].prenom").value(hasItem(DEFAULT_PRENOM)))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].telephone").value(hasItem(DEFAULT_TELEPHONE)))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].createdAt").value(Matchers.hasItem(TestUtil.sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(Matchers.hasItem(TestUtil.sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].adresse").value(hasItem(DEFAULT_ADRESSE)))
            .andExpect(jsonPath("$.[*].imageUrl").value(hasItem(DEFAULT_IMAGE_URL)));
    }

    @Test
    @Transactional
    public void getDelegue() throws Exception {
        // Initialize the database
        delegueRepository.saveAndFlush(delegue);

        // Get the delegue
        restDelegueMockMvc.perform(get("/api/delegues/{id}", delegue.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(delegue.getId().intValue()))
            .andExpect(jsonPath("$.hashcode").value(DEFAULT_HASHCODE))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE))
            .andExpect(jsonPath("$.prenom").value(DEFAULT_PRENOM))
            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.telephone").value(DEFAULT_TELEPHONE))
            .andExpect(jsonPath("$.deleted").value(DEFAULT_DELETED.booleanValue()))
            .andExpect(jsonPath("$.createdAt").value(TestUtil.sameInstant(DEFAULT_CREATED_AT)))
            .andExpect(jsonPath("$.updatedAt").value(TestUtil.sameInstant(DEFAULT_UPDATED_AT)))
            .andExpect(jsonPath("$.adresse").value(DEFAULT_ADRESSE))
            .andExpect(jsonPath("$.imageUrl").value(DEFAULT_IMAGE_URL));
    }
    @Test
    @Transactional
    public void getNonExistingDelegue() throws Exception {
        // Get the delegue
        restDelegueMockMvc.perform(get("/api/delegues/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDelegue() throws Exception {
        // Initialize the database
        delegueRepository.saveAndFlush(delegue);

        int databaseSizeBeforeUpdate = delegueRepository.findAll().size();

        // Update the delegue
        Delegue updatedDelegue = delegueRepository.findById(delegue.getId()).get();
        // Disconnect from session so that the updates on updatedDelegue are not directly saved in db
        em.detach(updatedDelegue);
        updatedDelegue
            .hashcode(UPDATED_HASHCODE)
            .code(UPDATED_CODE)
            .prenom(UPDATED_PRENOM)
            .nom(UPDATED_NOM)
            .email(UPDATED_EMAIL)
            .telephone(UPDATED_TELEPHONE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .adresse(UPDATED_ADRESSE)
            .imageUrl(UPDATED_IMAGE_URL);
        DelegueDTO delegueDTO = delegueMapper.toDto(updatedDelegue);

        restDelegueMockMvc.perform(put("/api/delegues")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(delegueDTO)))
            .andExpect(status().isOk());

        // Validate the Delegue in the database
        List<Delegue> delegueList = delegueRepository.findAll();
        assertThat(delegueList).hasSize(databaseSizeBeforeUpdate);
        Delegue testDelegue = delegueList.get(delegueList.size() - 1);
        assertThat(testDelegue.getHashcode()).isEqualTo(UPDATED_HASHCODE);
        assertThat(testDelegue.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testDelegue.getPrenom()).isEqualTo(UPDATED_PRENOM);
        assertThat(testDelegue.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testDelegue.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testDelegue.getTelephone()).isEqualTo(UPDATED_TELEPHONE);
        assertThat(testDelegue.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testDelegue.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testDelegue.getAdresse()).isEqualTo(UPDATED_ADRESSE);
        assertThat(testDelegue.getImageUrl()).isEqualTo(UPDATED_IMAGE_URL);

        // Validate the Delegue in Elasticsearch
        verify(mockDelegueSearchRepository, times(1)).save(testDelegue);
    }

    @Test
    @Transactional
    public void updateNonExistingDelegue() throws Exception {
        int databaseSizeBeforeUpdate = delegueRepository.findAll().size();

        // Create the Delegue
        DelegueDTO delegueDTO = delegueMapper.toDto(delegue);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDelegueMockMvc.perform(put("/api/delegues")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(delegueDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Delegue in the database
        List<Delegue> delegueList = delegueRepository.findAll();
        assertThat(delegueList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Delegue in Elasticsearch
        verify(mockDelegueSearchRepository, times(0)).save(delegue);
    }

    @Test
    @Transactional
    public void deleteDelegue() throws Exception {
        // Initialize the database
        delegueRepository.saveAndFlush(delegue);

        int databaseSizeBeforeDelete = delegueRepository.findAll().size();

        // Delete the delegue
        restDelegueMockMvc.perform(delete("/api/delegues/{id}", delegue.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Delegue> delegueList = delegueRepository.findAll();
        assertThat(delegueList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Delegue in Elasticsearch
        verify(mockDelegueSearchRepository, times(1)).deleteById(delegue.getId());
    }

    @Test
    @Transactional
    public void searchDelegue() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        delegueRepository.saveAndFlush(delegue);
        when(mockDelegueSearchRepository.search(queryStringQuery("id:" + delegue.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(delegue), PageRequest.of(0, 1), 1));

        // Search the delegue
        restDelegueMockMvc.perform(get("/api/_search/delegues?query=id:" + delegue.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(delegue.getId().intValue())))
            .andExpect(jsonPath("$.[*].hashcode").value(hasItem(DEFAULT_HASHCODE)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].prenom").value(hasItem(DEFAULT_PRENOM)))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].telephone").value(hasItem(DEFAULT_TELEPHONE)))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].createdAt").value(Matchers.hasItem(TestUtil.sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(Matchers.hasItem(TestUtil.sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].adresse").value(hasItem(DEFAULT_ADRESSE)))
            .andExpect(jsonPath("$.[*].imageUrl").value(hasItem(DEFAULT_IMAGE_URL)));
    }
}
