package org.nddngroup.navetfoot.core.web.rest;

import org.hamcrest.Matchers;
import org.nddngroup.navetfoot.core.NavetfootCore;
import org.nddngroup.navetfoot.core.domain.Carton;
import org.nddngroup.navetfoot.core.domain.Joueur;
import org.nddngroup.navetfoot.core.domain.Match;
import org.nddngroup.navetfoot.core.repository.CartonRepository;
import org.nddngroup.navetfoot.core.repository.search.CartonSearchRepository;
import org.nddngroup.navetfoot.core.service.CartonService;
import org.nddngroup.navetfoot.core.service.dto.CartonDTO;
import org.nddngroup.navetfoot.core.service.mapper.CartonMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.nddngroup.navetfoot.core.repository.search.CartonSearchRepositoryMockConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.nddngroup.navetfoot.core.domain.enumeration.Couleur;
import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;
/**
 * Integration tests for the {@link CartonResource} REST controller.
 */
@SpringBootTest(classes = NavetfootCore.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class CartonResourceIT {

    private static final Couleur DEFAULT_COULEUR = Couleur.JAUNE;
    private static final Couleur UPDATED_COULEUR = Couleur.ROUGE;

    private static final Equipe DEFAULT_EQUIPE = Equipe.LOCAL;
    private static final Equipe UPDATED_EQUIPE = Equipe.VISITEUR;

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_HASHCODE = "AAAAAAAAAA";
    private static final String UPDATED_HASHCODE = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Boolean DEFAULT_DELETED = false;
    private static final Boolean UPDATED_DELETED = true;

    private static final Integer DEFAULT_INSTANT = 1;
    private static final Integer UPDATED_INSTANT = 2;

    @Autowired
    private CartonRepository cartonRepository;

    @Autowired
    private CartonMapper cartonMapper;

    @Autowired
    private CartonService cartonService;

    /**
     * This repository is mocked in the org.nddngroup.navetfoot.core.repository.search test package.
     *
     * @see CartonSearchRepositoryMockConfiguration
     */
    @Autowired
    private CartonSearchRepository mockCartonSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCartonMockMvc;

    private Carton carton;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Carton createEntity(EntityManager em) {
        Carton carton = new Carton()
            .couleur(DEFAULT_COULEUR)
            .equipe(DEFAULT_EQUIPE)
            .code(DEFAULT_CODE)
            .hashcode(DEFAULT_HASHCODE)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT)
            .deleted(DEFAULT_DELETED)
            .instant(DEFAULT_INSTANT);
        // Add required entity
        Joueur joueur;
        if (TestUtil.findAll(em, Joueur.class).isEmpty()) {
            joueur = JoueurResourceIT.createUpdatedEntity(em);
            em.persist(joueur);
            em.flush();
        } else {
            joueur = TestUtil.findAll(em, Joueur.class).get(0);
        }
        carton.setJoueur(joueur);

        Match match;
        if (TestUtil.findAll(em, Match.class).isEmpty()) {
            match = MatchResourceIT.createUpdatedEntity(em);
            em.persist(match);
            em.flush();
        } else {
            match = TestUtil.findAll(em, Match.class).get(0);
        }
        carton.setMatch(match);
        return carton;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Carton createUpdatedEntity(EntityManager em) {
        Carton carton = new Carton()
            .couleur(UPDATED_COULEUR)
            .equipe(UPDATED_EQUIPE)
            .code(UPDATED_CODE)
            .hashcode(UPDATED_HASHCODE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .deleted(UPDATED_DELETED)
            .instant(UPDATED_INSTANT);
        return carton;
    }

    @BeforeEach
    public void initTest() {
        carton = createEntity(em);
    }

    @Test
    @Transactional
    public void createCarton() throws Exception {
        int databaseSizeBeforeCreate = cartonRepository.findAll().size();
        // Create the Carton
        CartonDTO cartonDTO = cartonMapper.toDto(carton);
        restCartonMockMvc.perform(post("/api/cartons")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(cartonDTO)))
            .andExpect(status().isCreated());

        // Validate the Carton in the database
        List<Carton> cartonList = cartonRepository.findAll();
        assertThat(cartonList).hasSize(databaseSizeBeforeCreate + 1);
        Carton testCarton = cartonList.get(cartonList.size() - 1);
        assertThat(testCarton.getCouleur()).isEqualTo(DEFAULT_COULEUR);
        assertThat(testCarton.getEquipe()).isEqualTo(DEFAULT_EQUIPE);
        assertThat(testCarton.getHashcode()).isEqualTo(DEFAULT_HASHCODE);
        assertThat(testCarton.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testCarton.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testCarton.isDeleted()).isEqualTo(DEFAULT_DELETED);
        assertThat(testCarton.getInstant()).isEqualTo(DEFAULT_INSTANT);

        // Validate the Carton in Elasticsearch
        verify(mockCartonSearchRepository, times(1)).save(testCarton);
    }

    @Test
    @Transactional
    public void createCartonWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = cartonRepository.findAll().size();

        // Create the Carton with an existing ID
        carton.setId(1L);
        CartonDTO cartonDTO = cartonMapper.toDto(carton);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCartonMockMvc.perform(post("/api/cartons")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(cartonDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Carton in the database
        List<Carton> cartonList = cartonRepository.findAll();
        assertThat(cartonList).hasSize(databaseSizeBeforeCreate);

        // Validate the Carton in Elasticsearch
        verify(mockCartonSearchRepository, times(0)).save(carton);
    }


    @Test
    @Transactional
    public void getAllCartons() throws Exception {
        // Initialize the database
        cartonRepository.saveAndFlush(carton);

        // Get all the cartonList
        restCartonMockMvc.perform(get("/api/cartons?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(carton.getId().intValue())))
            .andExpect(jsonPath("$.[*].couleur").value(hasItem(DEFAULT_COULEUR.toString())))
            .andExpect(jsonPath("$.[*].equipe").value(hasItem(DEFAULT_EQUIPE.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].hashcode").value(hasItem(DEFAULT_HASHCODE)))
            .andExpect(jsonPath("$.[*].createdAt").value(Matchers.hasItem(TestUtil.sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(Matchers.hasItem(TestUtil.sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].instant").value(hasItem(DEFAULT_INSTANT)));
    }

    @Test
    @Transactional
    public void getCarton() throws Exception {
        // Initialize the database
        cartonRepository.saveAndFlush(carton);

        // Get the carton
        restCartonMockMvc.perform(get("/api/cartons/{id}", carton.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(carton.getId().intValue()))
            .andExpect(jsonPath("$.couleur").value(DEFAULT_COULEUR.toString()))
            .andExpect(jsonPath("$.equipe").value(DEFAULT_EQUIPE.toString()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE))
            .andExpect(jsonPath("$.hashcode").value(DEFAULT_HASHCODE))
            .andExpect(jsonPath("$.createdAt").value(TestUtil.sameInstant(DEFAULT_CREATED_AT)))
            .andExpect(jsonPath("$.updatedAt").value(TestUtil.sameInstant(DEFAULT_UPDATED_AT)))
            .andExpect(jsonPath("$.deleted").value(DEFAULT_DELETED.booleanValue()))
            .andExpect(jsonPath("$.instant").value(DEFAULT_INSTANT));
    }
    @Test
    @Transactional
    public void getNonExistingCarton() throws Exception {
        // Get the carton
        restCartonMockMvc.perform(get("/api/cartons/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCarton() throws Exception {
        // Initialize the database
        cartonRepository.saveAndFlush(carton);

        int databaseSizeBeforeUpdate = cartonRepository.findAll().size();

        // Update the carton
        Carton updatedCarton = cartonRepository.findById(carton.getId()).get();
        // Disconnect from session so that the updates on updatedCarton are not directly saved in db
        em.detach(updatedCarton);
        updatedCarton
            .couleur(UPDATED_COULEUR)
            .equipe(UPDATED_EQUIPE)
            .code(UPDATED_CODE)
            .hashcode(UPDATED_HASHCODE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .instant(UPDATED_INSTANT);
        CartonDTO cartonDTO = cartonMapper.toDto(updatedCarton);

        restCartonMockMvc.perform(put("/api/cartons")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(cartonDTO)))
            .andExpect(status().isOk());

        // Validate the Carton in the database
        List<Carton> cartonList = cartonRepository.findAll();
        assertThat(cartonList).hasSize(databaseSizeBeforeUpdate);
        Carton testCarton = cartonList.get(cartonList.size() - 1);
        assertThat(testCarton.getCouleur()).isEqualTo(UPDATED_COULEUR);
        assertThat(testCarton.getEquipe()).isEqualTo(UPDATED_EQUIPE);
        assertThat(testCarton.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testCarton.getHashcode()).isEqualTo(UPDATED_HASHCODE);
        assertThat(testCarton.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testCarton.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testCarton.getInstant()).isEqualTo(UPDATED_INSTANT);

        // Validate the Carton in Elasticsearch
        verify(mockCartonSearchRepository, times(1)).save(testCarton);
    }

    @Test
    @Transactional
    public void updateNonExistingCarton() throws Exception {
        int databaseSizeBeforeUpdate = cartonRepository.findAll().size();

        // Create the Carton
        CartonDTO cartonDTO = cartonMapper.toDto(carton);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCartonMockMvc.perform(put("/api/cartons")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(cartonDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Carton in the database
        List<Carton> cartonList = cartonRepository.findAll();
        assertThat(cartonList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Carton in Elasticsearch
        verify(mockCartonSearchRepository, times(0)).save(carton);
    }

    @Test
    @Transactional
    public void deleteCarton() throws Exception {
        // Initialize the database
        cartonRepository.saveAndFlush(carton);

        int databaseSizeBeforeDelete = cartonRepository.findAll().size();

        // Delete the carton
        restCartonMockMvc.perform(delete("/api/cartons/{id}", carton.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Carton> cartonList = cartonRepository.findAll();
        assertThat(cartonList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Carton in Elasticsearch
        verify(mockCartonSearchRepository, times(1)).deleteById(carton.getId());
    }

    @Test
    @Transactional
    public void searchCarton() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        cartonRepository.saveAndFlush(carton);
        when(mockCartonSearchRepository.search(queryStringQuery("id:" + carton.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(carton), PageRequest.of(0, 1), 1));

        // Search the carton
        restCartonMockMvc.perform(get("/api/_search/cartons?query=id:" + carton.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(carton.getId().intValue())))
            .andExpect(jsonPath("$.[*].couleur").value(hasItem(DEFAULT_COULEUR.toString())))
            .andExpect(jsonPath("$.[*].equipe").value(hasItem(DEFAULT_EQUIPE.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].hashcode").value(hasItem(DEFAULT_HASHCODE)))
            .andExpect(jsonPath("$.[*].createdAt").value(Matchers.hasItem(TestUtil.sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(Matchers.hasItem(TestUtil.sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].instant").value(hasItem(DEFAULT_INSTANT)));
    }
}
