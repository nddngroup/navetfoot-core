package org.nddngroup.navetfoot.core.web.rest;

import org.nddngroup.navetfoot.core.NavetfootCore;
import org.nddngroup.navetfoot.core.domain.CoupFranc;
import org.nddngroup.navetfoot.core.repository.CoupFrancRepository;
import org.nddngroup.navetfoot.core.repository.search.CoupFrancSearchRepository;
import org.nddngroup.navetfoot.core.service.CoupFrancService;
import org.nddngroup.navetfoot.core.service.dto.CoupFrancDTO;
import org.nddngroup.navetfoot.core.service.mapper.CoupFrancMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.nddngroup.navetfoot.core.repository.search.CoupFrancSearchRepositoryMockConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static org.nddngroup.navetfoot.core.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;
/**
 * Integration tests for the {@link CoupFrancResource} REST controller.
 */
@SpringBootTest(classes = NavetfootCore.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class CoupFrancResourceIT {

    private static final Equipe DEFAULT_EQUIPE = Equipe.LOCAL;
    private static final Equipe UPDATED_EQUIPE = Equipe.VISITEUR;

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_HASHCODE = "AAAAAAAAAA";
    private static final String UPDATED_HASHCODE = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Boolean DEFAULT_DELETED = false;
    private static final Boolean UPDATED_DELETED = true;

    private static final Integer DEFAULT_INSTANT = 1;
    private static final Integer UPDATED_INSTANT = 2;

    @Autowired
    private CoupFrancRepository coupFrancRepository;

    @Autowired
    private CoupFrancMapper coupFrancMapper;

    @Autowired
    private CoupFrancService coupFrancService;

    /**
     * This repository is mocked in the org.nddngroup.navetfoot.core.repository.search test package.
     *
     * @see CoupFrancSearchRepositoryMockConfiguration
     */
    @Autowired
    private CoupFrancSearchRepository mockCoupFrancSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCoupFrancMockMvc;

    private CoupFranc coupFranc;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CoupFranc createEntity(EntityManager em) {
        CoupFranc coupFranc = new CoupFranc()
            .equipe(DEFAULT_EQUIPE)
            .code(DEFAULT_CODE)
            .hashcode(DEFAULT_HASHCODE)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT)
            .deleted(DEFAULT_DELETED)
            .instant(DEFAULT_INSTANT);
        return coupFranc;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CoupFranc createUpdatedEntity(EntityManager em) {
        CoupFranc coupFranc = new CoupFranc()
            .equipe(UPDATED_EQUIPE)
            .code(UPDATED_CODE)
            .hashcode(UPDATED_HASHCODE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .deleted(UPDATED_DELETED)
            .instant(UPDATED_INSTANT);
        return coupFranc;
    }

    @BeforeEach
    public void initTest() {
        coupFranc = createEntity(em);
    }

    @Test
    @Transactional
    public void createCoupFranc() throws Exception {
        int databaseSizeBeforeCreate = coupFrancRepository.findAll().size();
        // Create the CoupFranc
        CoupFrancDTO coupFrancDTO = coupFrancMapper.toDto(coupFranc);
        restCoupFrancMockMvc.perform(post("/api/coup-francs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(coupFrancDTO)))
            .andExpect(status().isCreated());

        // Validate the CoupFranc in the database
        List<CoupFranc> coupFrancList = coupFrancRepository.findAll();
        assertThat(coupFrancList).hasSize(databaseSizeBeforeCreate + 1);
        CoupFranc testCoupFranc = coupFrancList.get(coupFrancList.size() - 1);
        assertThat(testCoupFranc.getEquipe()).isEqualTo(DEFAULT_EQUIPE);
        assertThat(testCoupFranc.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testCoupFranc.getHashcode()).isEqualTo(DEFAULT_HASHCODE);
        assertThat(testCoupFranc.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testCoupFranc.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testCoupFranc.isDeleted()).isEqualTo(DEFAULT_DELETED);
        assertThat(testCoupFranc.getInstant()).isEqualTo(DEFAULT_INSTANT);

        // Validate the CoupFranc in Elasticsearch
        verify(mockCoupFrancSearchRepository, times(1)).save(testCoupFranc);
    }

    @Test
    @Transactional
    public void createCoupFrancWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = coupFrancRepository.findAll().size();

        // Create the CoupFranc with an existing ID
        coupFranc.setId(1L);
        CoupFrancDTO coupFrancDTO = coupFrancMapper.toDto(coupFranc);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCoupFrancMockMvc.perform(post("/api/coup-francs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(coupFrancDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CoupFranc in the database
        List<CoupFranc> coupFrancList = coupFrancRepository.findAll();
        assertThat(coupFrancList).hasSize(databaseSizeBeforeCreate);

        // Validate the CoupFranc in Elasticsearch
        verify(mockCoupFrancSearchRepository, times(0)).save(coupFranc);
    }


    @Test
    @Transactional
    public void getAllCoupFrancs() throws Exception {
        // Initialize the database
        coupFrancRepository.saveAndFlush(coupFranc);

        // Get all the coupFrancList
        restCoupFrancMockMvc.perform(get("/api/coup-francs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(coupFranc.getId().intValue())))
            .andExpect(jsonPath("$.[*].equipe").value(hasItem(DEFAULT_EQUIPE.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].hashcode").value(hasItem(DEFAULT_HASHCODE)))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].instant").value(hasItem(DEFAULT_INSTANT)));
    }

    @Test
    @Transactional
    public void getCoupFranc() throws Exception {
        // Initialize the database
        coupFrancRepository.saveAndFlush(coupFranc);

        // Get the coupFranc
        restCoupFrancMockMvc.perform(get("/api/coup-francs/{id}", coupFranc.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(coupFranc.getId().intValue()))
            .andExpect(jsonPath("$.equipe").value(DEFAULT_EQUIPE.toString()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE))
            .andExpect(jsonPath("$.hashcode").value(DEFAULT_HASHCODE))
            .andExpect(jsonPath("$.createdAt").value(sameInstant(DEFAULT_CREATED_AT)))
            .andExpect(jsonPath("$.updatedAt").value(sameInstant(DEFAULT_UPDATED_AT)))
            .andExpect(jsonPath("$.deleted").value(DEFAULT_DELETED.booleanValue()))
            .andExpect(jsonPath("$.instant").value(DEFAULT_INSTANT));
    }
    @Test
    @Transactional
    public void getNonExistingCoupFranc() throws Exception {
        // Get the coupFranc
        restCoupFrancMockMvc.perform(get("/api/coup-francs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCoupFranc() throws Exception {
        // Initialize the database
        coupFrancRepository.saveAndFlush(coupFranc);

        int databaseSizeBeforeUpdate = coupFrancRepository.findAll().size();

        // Update the coupFranc
        CoupFranc updatedCoupFranc = coupFrancRepository.findById(coupFranc.getId()).get();
        // Disconnect from session so that the updates on updatedCoupFranc are not directly saved in db
        em.detach(updatedCoupFranc);
        updatedCoupFranc
            .equipe(UPDATED_EQUIPE)
            .code(UPDATED_CODE)
            .hashcode(UPDATED_HASHCODE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .instant(UPDATED_INSTANT);
        CoupFrancDTO coupFrancDTO = coupFrancMapper.toDto(updatedCoupFranc);

        restCoupFrancMockMvc.perform(put("/api/coup-francs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(coupFrancDTO)))
            .andExpect(status().isOk());

        // Validate the CoupFranc in the database
        List<CoupFranc> coupFrancList = coupFrancRepository.findAll();
        assertThat(coupFrancList).hasSize(databaseSizeBeforeUpdate);
        CoupFranc testCoupFranc = coupFrancList.get(coupFrancList.size() - 1);
        assertThat(testCoupFranc.getEquipe()).isEqualTo(UPDATED_EQUIPE);
        assertThat(testCoupFranc.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testCoupFranc.getHashcode()).isEqualTo(UPDATED_HASHCODE);
        assertThat(testCoupFranc.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testCoupFranc.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testCoupFranc.getInstant()).isEqualTo(UPDATED_INSTANT);

        // Validate the CoupFranc in Elasticsearch
        verify(mockCoupFrancSearchRepository, times(1)).save(testCoupFranc);
    }

    @Test
    @Transactional
    public void updateNonExistingCoupFranc() throws Exception {
        int databaseSizeBeforeUpdate = coupFrancRepository.findAll().size();

        // Create the CoupFranc
        CoupFrancDTO coupFrancDTO = coupFrancMapper.toDto(coupFranc);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCoupFrancMockMvc.perform(put("/api/coup-francs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(coupFrancDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CoupFranc in the database
        List<CoupFranc> coupFrancList = coupFrancRepository.findAll();
        assertThat(coupFrancList).hasSize(databaseSizeBeforeUpdate);

        // Validate the CoupFranc in Elasticsearch
        verify(mockCoupFrancSearchRepository, times(0)).save(coupFranc);
    }

    @Test
    @Transactional
    public void deleteCoupFranc() throws Exception {
        // Initialize the database
        coupFrancRepository.saveAndFlush(coupFranc);

        int databaseSizeBeforeDelete = coupFrancRepository.findAll().size();

        // Delete the coupFranc
        restCoupFrancMockMvc.perform(delete("/api/coup-francs/{id}", coupFranc.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CoupFranc> coupFrancList = coupFrancRepository.findAll();
        assertThat(coupFrancList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the CoupFranc in Elasticsearch
        verify(mockCoupFrancSearchRepository, times(1)).deleteById(coupFranc.getId());
    }

    @Test
    @Transactional
    public void searchCoupFranc() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        coupFrancRepository.saveAndFlush(coupFranc);
        when(mockCoupFrancSearchRepository.search(queryStringQuery("id:" + coupFranc.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(coupFranc), PageRequest.of(0, 1), 1));

        // Search the coupFranc
        restCoupFrancMockMvc.perform(get("/api/_search/coup-francs?query=id:" + coupFranc.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(coupFranc.getId().intValue())))
            .andExpect(jsonPath("$.[*].equipe").value(hasItem(DEFAULT_EQUIPE.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].hashcode").value(hasItem(DEFAULT_HASHCODE)))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].instant").value(hasItem(DEFAULT_INSTANT)));
    }
}
