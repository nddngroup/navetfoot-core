package org.nddngroup.navetfoot.core.web.rest;

import org.hamcrest.Matchers;
import org.nddngroup.navetfoot.core.NavetfootCore;
import org.nddngroup.navetfoot.core.domain.But;
import org.nddngroup.navetfoot.core.domain.Joueur;
import org.nddngroup.navetfoot.core.domain.Match;
import org.nddngroup.navetfoot.core.repository.ButRepository;
import org.nddngroup.navetfoot.core.repository.search.ButSearchRepository;
import org.nddngroup.navetfoot.core.service.ButService;
import org.nddngroup.navetfoot.core.service.dto.ButDTO;
import org.nddngroup.navetfoot.core.service.mapper.ButMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.nddngroup.navetfoot.core.repository.search.ButSearchRepositoryMockConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.nddngroup.navetfoot.core.domain.enumeration.Cause;
import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;
/**
 * Integration tests for the {@link ButResource} REST controller.
 */
@SpringBootTest(classes = NavetfootCore.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class ButResourceIT {

    private static final Cause DEFAULT_CAUSE = Cause.PENALTY;
    private static final Cause UPDATED_CAUSE = Cause.COUP_FRANC;

    private static final Equipe DEFAULT_EQUIPE = Equipe.LOCAL;
    private static final Equipe UPDATED_EQUIPE = Equipe.VISITEUR;

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_HASHCODE = "AAAAAAAAAA";
    private static final String UPDATED_HASHCODE = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Boolean DEFAULT_DELETED = false;
    private static final Boolean UPDATED_DELETED = true;

    private static final Integer DEFAULT_INSTANT = 1;
    private static final Integer UPDATED_INSTANT = 2;

    private static final Boolean DEFAULT_ANNULE = false;
    private static final Boolean UPDATED_ANNULE = true;

    @Autowired
    private ButRepository butRepository;

    @Autowired
    private ButMapper butMapper;

    @Autowired
    private ButService butService;

    /**
     * This repository is mocked in the org.nddngroup.navetfoot.core.repository.search test package.
     *
     * @see ButSearchRepositoryMockConfiguration
     */
    @Autowired
    private ButSearchRepository mockButSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restButMockMvc;

    private But but;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static But createEntity(EntityManager em) {
        But but = new But()
            .cause(DEFAULT_CAUSE)
            .equipe(DEFAULT_EQUIPE)
            .code(DEFAULT_CODE)
            .hashcode(DEFAULT_HASHCODE)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT)
            .deleted(DEFAULT_DELETED)
            .instant(DEFAULT_INSTANT)
            .annule(DEFAULT_ANNULE);

        // Add required entity
        Joueur joueur;
        if (TestUtil.findAll(em, Joueur.class).isEmpty()) {
            joueur = JoueurResourceIT.createUpdatedEntity(em);
            em.persist(joueur);
            em.flush();
        } else {
            joueur = TestUtil.findAll(em, Joueur.class).get(0);
        }
        but.setJoueur(joueur);

        Match match;
        if (TestUtil.findAll(em, Match.class).isEmpty()) {
            match = MatchResourceIT.createUpdatedEntity(em);
            em.persist(match);
            em.flush();
        } else {
            match = TestUtil.findAll(em, Match.class).get(0);
        }
        but.setMatch(match);

        return but;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static But createUpdatedEntity(EntityManager em) {
        But but = new But()
            .cause(UPDATED_CAUSE)
            .equipe(UPDATED_EQUIPE)
            .code(UPDATED_CODE)
            .hashcode(UPDATED_HASHCODE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .deleted(UPDATED_DELETED)
            .instant(UPDATED_INSTANT)
            .annule(UPDATED_ANNULE);
        return but;
    }

    @BeforeEach
    public void initTest() {
        but = createEntity(em);
    }

    @Test
    @Transactional
    public void createBut() throws Exception {
        int databaseSizeBeforeCreate = butRepository.findAll().size();
        // Create the But
        ButDTO butDTO = butMapper.toDto(but);
        restButMockMvc.perform(post("/api/buts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(butDTO)))
            .andExpect(status().isCreated());

        // Validate the But in the database
        List<But> butList = butRepository.findAll();
        assertThat(butList).hasSize(databaseSizeBeforeCreate + 1);
        But testBut = butList.get(butList.size() - 1);
        assertThat(testBut.getCause()).isEqualTo(DEFAULT_CAUSE);
        assertThat(testBut.getEquipe()).isEqualTo(DEFAULT_EQUIPE);
        assertThat(testBut.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testBut.getHashcode()).isEqualTo(DEFAULT_HASHCODE);
        assertThat(testBut.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testBut.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testBut.isDeleted()).isEqualTo(DEFAULT_DELETED);
        assertThat(testBut.getInstant()).isEqualTo(DEFAULT_INSTANT);
        assertThat(testBut.isAnnule()).isEqualTo(DEFAULT_ANNULE);

        // Validate the But in Elasticsearch
        verify(mockButSearchRepository, times(1)).save(testBut);
    }

    @Test
    @Transactional
    public void createButWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = butRepository.findAll().size();

        // Create the But with an existing ID
        but.setId(1L);
        ButDTO butDTO = butMapper.toDto(but);

        // An entity with an existing ID cannot be created, so this API call must fail
        restButMockMvc.perform(post("/api/buts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(butDTO)))
            .andExpect(status().isBadRequest());

        // Validate the But in the database
        List<But> butList = butRepository.findAll();
        assertThat(butList).hasSize(databaseSizeBeforeCreate);

        // Validate the But in Elasticsearch
        verify(mockButSearchRepository, times(0)).save(but);
    }


    @Test
    @Transactional
    public void getAllButs() throws Exception {
        // Initialize the database
        butRepository.saveAndFlush(but);

        // Get all the butList
        restButMockMvc.perform(get("/api/buts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(but.getId().intValue())))
            .andExpect(jsonPath("$.[*].cause").value(hasItem(DEFAULT_CAUSE.toString())))
            .andExpect(jsonPath("$.[*].equipe").value(hasItem(DEFAULT_EQUIPE.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].hashcode").value(hasItem(DEFAULT_HASHCODE)))
            .andExpect(jsonPath("$.[*].createdAt").value(Matchers.hasItem(TestUtil.sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(Matchers.hasItem(TestUtil.sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].instant").value(hasItem(DEFAULT_INSTANT)))
            .andExpect(jsonPath("$.[*].annule").value(hasItem(DEFAULT_ANNULE.booleanValue())));
    }

    @Test
    @Transactional
    public void getBut() throws Exception {
        // Initialize the database
        butRepository.saveAndFlush(but);

        // Get the but
        restButMockMvc.perform(get("/api/buts/{id}", but.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(but.getId().intValue()))
            .andExpect(jsonPath("$.cause").value(DEFAULT_CAUSE.toString()))
            .andExpect(jsonPath("$.equipe").value(DEFAULT_EQUIPE.toString()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE))
            .andExpect(jsonPath("$.hashcode").value(DEFAULT_HASHCODE))
            .andExpect(jsonPath("$.createdAt").value(TestUtil.sameInstant(DEFAULT_CREATED_AT)))
            .andExpect(jsonPath("$.updatedAt").value(TestUtil.sameInstant(DEFAULT_UPDATED_AT)))
            .andExpect(jsonPath("$.deleted").value(DEFAULT_DELETED.booleanValue()))
            .andExpect(jsonPath("$.instant").value(DEFAULT_INSTANT))
            .andExpect(jsonPath("$.annule").value(DEFAULT_ANNULE.booleanValue()));
    }
    @Test
    @Transactional
    public void getNonExistingBut() throws Exception {
        // Get the but
        restButMockMvc.perform(get("/api/buts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBut() throws Exception {
        // Initialize the database
        butRepository.saveAndFlush(but);

        int databaseSizeBeforeUpdate = butRepository.findAll().size();

        // Update the but
        But updatedBut = butRepository.findById(but.getId()).get();
        // Disconnect from session so that the updates on updatedBut are not directly saved in db
        em.detach(updatedBut);
        updatedBut
            .cause(UPDATED_CAUSE)
            .equipe(UPDATED_EQUIPE)
            .code(UPDATED_CODE)
            .hashcode(UPDATED_HASHCODE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .instant(UPDATED_INSTANT)
            .annule(UPDATED_ANNULE);
        ButDTO butDTO = butMapper.toDto(updatedBut);

        restButMockMvc.perform(put("/api/buts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(butDTO)))
            .andExpect(status().isOk());

        // Validate the But in the database
        List<But> butList = butRepository.findAll();
        assertThat(butList).hasSize(databaseSizeBeforeUpdate);
        But testBut = butList.get(butList.size() - 1);
        assertThat(testBut.getCause()).isEqualTo(UPDATED_CAUSE);
        assertThat(testBut.getEquipe()).isEqualTo(UPDATED_EQUIPE);
        assertThat(testBut.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testBut.getHashcode()).isEqualTo(UPDATED_HASHCODE);
        assertThat(testBut.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testBut.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testBut.getInstant()).isEqualTo(UPDATED_INSTANT);
        assertThat(testBut.isAnnule()).isEqualTo(UPDATED_ANNULE);

        // Validate the But in Elasticsearch
        verify(mockButSearchRepository, times(1)).save(testBut);
    }

    @Test
    @Transactional
    public void updateNonExistingBut() throws Exception {
        int databaseSizeBeforeUpdate = butRepository.findAll().size();

        // Create the But
        ButDTO butDTO = butMapper.toDto(but);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restButMockMvc.perform(put("/api/buts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(butDTO)))
            .andExpect(status().isBadRequest());

        // Validate the But in the database
        List<But> butList = butRepository.findAll();
        assertThat(butList).hasSize(databaseSizeBeforeUpdate);

        // Validate the But in Elasticsearch
        verify(mockButSearchRepository, times(0)).save(but);
    }

    @Test
    @Transactional
    public void deleteBut() throws Exception {
        // Initialize the database
        butRepository.saveAndFlush(but);

        int databaseSizeBeforeDelete = butRepository.findAll().size();

        // Delete the but
        restButMockMvc.perform(delete("/api/buts/{id}", but.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<But> butList = butRepository.findAll();
        assertThat(butList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the But in Elasticsearch
        verify(mockButSearchRepository, times(1)).deleteById(but.getId());
    }

    @Test
    @Transactional
    public void searchBut() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        butRepository.saveAndFlush(but);
        when(mockButSearchRepository.search(queryStringQuery("id:" + but.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(but), PageRequest.of(0, 1), 1));

        // Search the but
        restButMockMvc.perform(get("/api/_search/buts?query=id:" + but.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(but.getId().intValue())))
            .andExpect(jsonPath("$.[*].cause").value(hasItem(DEFAULT_CAUSE.toString())))
            .andExpect(jsonPath("$.[*].equipe").value(hasItem(DEFAULT_EQUIPE.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].hashcode").value(hasItem(DEFAULT_HASHCODE)))
            .andExpect(jsonPath("$.[*].createdAt").value(Matchers.hasItem(TestUtil.sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(Matchers.hasItem(TestUtil.sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].instant").value(hasItem(DEFAULT_INSTANT)))
            .andExpect(jsonPath("$.[*].annule").value(hasItem(DEFAULT_ANNULE.booleanValue())));
    }
}
