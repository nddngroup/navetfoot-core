package org.nddngroup.navetfoot.core.web.rest.custom;

import org.nddngroup.navetfoot.core.NavetfootCore;
import org.nddngroup.navetfoot.core.domain.*;
import org.nddngroup.navetfoot.core.domain.*;
import org.nddngroup.navetfoot.core.repository.MatchRepository;
import org.nddngroup.navetfoot.core.web.rest.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.nddngroup.navetfoot.core.web.rest.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Integration tests for the {@link CustomEventsResource} REST controller.
 */
@SpringBootTest(classes = NavetfootCore.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class CustomEventsResourceIT {
    @Autowired
    private EntityManager em;

    @Autowired
    private MatchRepository matchRepository;

    @Autowired
    private MockMvc restMatchMockMvc;

    private Match match;

    @BeforeEach
    public void initTest() {
        match = MatchResourceIT.createEntity(em);
    }

    @Test
    @Transactional
    public void getEventDetails() throws Exception {
        // Initialize the database
        Organisation organisation = CustomMatchResourceIT.createCompleteEntity(match, em);
        matchRepository.saveAndFlush(match);

        // créer un joueur
        Joueur joueur = JoueurResourceIT.createEntity(em);
        em.persist(joueur);
        Joueur sortant = JoueurResourceIT.createEntity(em);
        em.persist(sortant);

        But but1 = ButResourceIT.createEntity(em);
        but1.setMatch(match);
        but1.setJoueur(joueur);
        em.persist(but1);
        But but2 = ButResourceIT.createEntity(em);
        but2.setMatch(match);
        but2.setJoueur(joueur);
        em.persist(but2);

        Carton carton = CartonResourceIT.createEntity(em);
        carton.setMatch(match);
        carton.setJoueur(joueur);
        em.persist(carton);

        Corner corner = CornerResourceIT.createEntity(em);
        corner.setMatch(match);
        em.persist(corner);

        Penalty penalty = PenaltyResourceIT.createEntity(em);
        penalty.setMatch(match);
        penalty.setJoueur(joueur);
        em.persist(penalty);

        CoupFranc coupFranc = CoupFrancResourceIT.createEntity(em);
        coupFranc.setMatch(match);
        em.persist(coupFranc);

        Remplacement remplacement = RemplacementResourceIT.createEntity(em);
        remplacement.setMatch(match);
        remplacement.setEntrant(joueur);
        remplacement.setSortant(sortant);
        em.persist(remplacement);

        HorsJeu horsJeu = HorsJeuResourceIT.createEntity(em);
        horsJeu.setMatch(match);
        em.persist(horsJeu);

        Tir tir1 = TirResourceIT.createEntity(em);
        tir1.setMatch(match);
        tir1.setJoueur(joueur);
        em.persist(tir1);
        Tir tir2 = TirResourceIT.createEntity(em);
        tir2.setJoueur(sortant);
        tir2.setMatch(match);
        em.persist(tir2);

        em.flush();

        // Delete the match
        restMatchMockMvc.perform(get("/api/organisations/{organisationId}/matchs/{id}/events", organisation.getId(), match.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.butsDetail").isArray())
            .andExpect(jsonPath("$.butsDetail", hasSize(2)))
            .andExpect(jsonPath("$.butsDetail.[0].prenomJoueur").value("AAAAAAAAAA"))
            .andExpect(jsonPath("$.butsDetail.[0].nomJoueur").value("AAAAAAAAAA"))
            .andExpect(jsonPath("$.cartonsDetail").isArray())
            .andExpect(jsonPath("$.cartonsDetail", hasSize(1)))
            .andExpect(jsonPath("$.cornersDetail").isArray())
            .andExpect(jsonPath("$.cornersDetail", hasSize(1)))
            .andExpect(jsonPath("$.penaltiesDetail").isArray())
            .andExpect(jsonPath("$.penaltiesDetail", hasSize(1)))
            .andExpect(jsonPath("$.coupFrancsDetail").isArray())
            .andExpect(jsonPath("$.coupFrancsDetail", hasSize(1)))
            .andExpect(jsonPath("$.remplacementsDetail").isArray())
            .andExpect(jsonPath("$.remplacementsDetail", hasSize(1)))
            .andExpect(jsonPath("$.horsJeusDetail").isArray())
            .andExpect(jsonPath("$.horsJeusDetail", hasSize(1)))
            .andExpect(jsonPath("$.tirsDetail").isArray())
            .andExpect(jsonPath("$.tirsDetail", hasSize(2)))
        ;
    }
}
