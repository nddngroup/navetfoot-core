package org.nddngroup.navetfoot.core.web.rest;

import org.nddngroup.navetfoot.core.NavetfootCore;
import org.nddngroup.navetfoot.core.domain.Etape;
import org.nddngroup.navetfoot.core.repository.EtapeRepository;
import org.nddngroup.navetfoot.core.repository.search.EtapeSearchRepository;
import org.nddngroup.navetfoot.core.service.EtapeService;
import org.nddngroup.navetfoot.core.service.dto.EtapeDTO;
import org.nddngroup.navetfoot.core.service.mapper.EtapeMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.nddngroup.navetfoot.core.repository.search.EtapeSearchRepositoryMockConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link EtapeResource} REST controller.
 */
@SpringBootTest(classes = NavetfootCore.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class EtapeResourceIT {

    private static final String DEFAULT_LIBELLE = "AAAAAAAAAA";
    private static final String UPDATED_LIBELLE = "BBBBBBBBBB";

    private static final Integer DEFAULT_NIVEAU = 1;
    private static final Integer UPDATED_NIVEAU = 2;

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    @Autowired
    private EtapeRepository etapeRepository;

    @Autowired
    private EtapeMapper etapeMapper;

    @Autowired
    private EtapeService etapeService;

    /**
     * This repository is mocked in the org.nddngroup.navetfoot.core.repository.search test package.
     *
     * @see EtapeSearchRepositoryMockConfiguration
     */
    @Autowired
    private EtapeSearchRepository mockEtapeSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restEtapeMockMvc;

    private Etape etape;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Etape createEntity(EntityManager em) {
        Etape etape = new Etape()
            .libelle(DEFAULT_LIBELLE)
            .niveau(DEFAULT_NIVEAU)
            .code(DEFAULT_CODE);
        return etape;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Etape createUpdatedEntity(EntityManager em) {
        Etape etape = new Etape()
            .libelle(UPDATED_LIBELLE)
            .niveau(UPDATED_NIVEAU)
            .code(UPDATED_CODE);
        return etape;
    }

    @BeforeEach
    public void initTest() {
        etape = createEntity(em);
    }

    @Test
    @Transactional
    public void createEtape() throws Exception {
        int databaseSizeBeforeCreate = etapeRepository.findAll().size();
        // Create the Etape
        EtapeDTO etapeDTO = etapeMapper.toDto(etape);
        restEtapeMockMvc.perform(post("/api/etapes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(etapeDTO)))
            .andExpect(status().isCreated());

        // Validate the Etape in the database
        List<Etape> etapeList = etapeRepository.findAll();
        assertThat(etapeList).hasSize(databaseSizeBeforeCreate + 1);
        Etape testEtape = etapeList.get(etapeList.size() - 1);
        assertThat(testEtape.getLibelle()).isEqualTo(DEFAULT_LIBELLE);
        assertThat(testEtape.getNiveau()).isEqualTo(DEFAULT_NIVEAU);
        assertThat(testEtape.getCode()).isEqualTo(DEFAULT_CODE);

        // Validate the Etape in Elasticsearch
        verify(mockEtapeSearchRepository, times(1)).save(testEtape);
    }

    @Test
    @Transactional
    public void createEtapeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = etapeRepository.findAll().size();

        // Create the Etape with an existing ID
        etape.setId(1L);
        EtapeDTO etapeDTO = etapeMapper.toDto(etape);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEtapeMockMvc.perform(post("/api/etapes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(etapeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Etape in the database
        List<Etape> etapeList = etapeRepository.findAll();
        assertThat(etapeList).hasSize(databaseSizeBeforeCreate);

        // Validate the Etape in Elasticsearch
        verify(mockEtapeSearchRepository, times(0)).save(etape);
    }


    @Test
    @Transactional
    public void checkLibelleIsRequired() throws Exception {
        int databaseSizeBeforeTest = etapeRepository.findAll().size();
        // set the field null
        etape.setLibelle(null);

        // Create the Etape, which fails.
        EtapeDTO etapeDTO = etapeMapper.toDto(etape);


        restEtapeMockMvc.perform(post("/api/etapes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(etapeDTO)))
            .andExpect(status().isBadRequest());

        List<Etape> etapeList = etapeRepository.findAll();
        assertThat(etapeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNiveauIsRequired() throws Exception {
        int databaseSizeBeforeTest = etapeRepository.findAll().size();
        // set the field null
        etape.setNiveau(null);

        // Create the Etape, which fails.
        EtapeDTO etapeDTO = etapeMapper.toDto(etape);


        restEtapeMockMvc.perform(post("/api/etapes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(etapeDTO)))
            .andExpect(status().isBadRequest());

        List<Etape> etapeList = etapeRepository.findAll();
        assertThat(etapeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = etapeRepository.findAll().size();
        // set the field null
        etape.setCode(null);

        // Create the Etape, which fails.
        EtapeDTO etapeDTO = etapeMapper.toDto(etape);


        restEtapeMockMvc.perform(post("/api/etapes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(etapeDTO)))
            .andExpect(status().isBadRequest());

        List<Etape> etapeList = etapeRepository.findAll();
        assertThat(etapeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllEtapes() throws Exception {
        // Initialize the database
        etapeRepository.saveAndFlush(etape);

        // Get all the etapeList
        restEtapeMockMvc.perform(get("/api/etapes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(etape.getId().intValue())))
            .andExpect(jsonPath("$.[*].libelle").value(hasItem(DEFAULT_LIBELLE)))
            .andExpect(jsonPath("$.[*].niveau").value(hasItem(DEFAULT_NIVEAU)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)));
    }

    @Test
    @Transactional
    public void getEtape() throws Exception {
        // Initialize the database
        etapeRepository.saveAndFlush(etape);

        // Get the etape
        restEtapeMockMvc.perform(get("/api/etapes/{id}", etape.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(etape.getId().intValue()))
            .andExpect(jsonPath("$.libelle").value(DEFAULT_LIBELLE))
            .andExpect(jsonPath("$.niveau").value(DEFAULT_NIVEAU))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE));
    }
    @Test
    @Transactional
    public void getNonExistingEtape() throws Exception {
        // Get the etape
        restEtapeMockMvc.perform(get("/api/etapes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEtape() throws Exception {
        // Initialize the database
        etapeRepository.saveAndFlush(etape);

        int databaseSizeBeforeUpdate = etapeRepository.findAll().size();

        // Update the etape
        Etape updatedEtape = etapeRepository.findById(etape.getId()).get();
        // Disconnect from session so that the updates on updatedEtape are not directly saved in db
        em.detach(updatedEtape);
        updatedEtape
            .libelle(UPDATED_LIBELLE)
            .niveau(UPDATED_NIVEAU)
            .code(UPDATED_CODE);
        EtapeDTO etapeDTO = etapeMapper.toDto(updatedEtape);

        restEtapeMockMvc.perform(put("/api/etapes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(etapeDTO)))
            .andExpect(status().isOk());

        // Validate the Etape in the database
        List<Etape> etapeList = etapeRepository.findAll();
        assertThat(etapeList).hasSize(databaseSizeBeforeUpdate);
        Etape testEtape = etapeList.get(etapeList.size() - 1);
        assertThat(testEtape.getLibelle()).isEqualTo(UPDATED_LIBELLE);
        assertThat(testEtape.getNiveau()).isEqualTo(UPDATED_NIVEAU);
        assertThat(testEtape.getCode()).isEqualTo(UPDATED_CODE);

        // Validate the Etape in Elasticsearch
        verify(mockEtapeSearchRepository, times(1)).save(testEtape);
    }

    @Test
    @Transactional
    public void updateNonExistingEtape() throws Exception {
        int databaseSizeBeforeUpdate = etapeRepository.findAll().size();

        // Create the Etape
        EtapeDTO etapeDTO = etapeMapper.toDto(etape);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEtapeMockMvc.perform(put("/api/etapes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(etapeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Etape in the database
        List<Etape> etapeList = etapeRepository.findAll();
        assertThat(etapeList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Etape in Elasticsearch
        verify(mockEtapeSearchRepository, times(0)).save(etape);
    }

    @Test
    @Transactional
    public void deleteEtape() throws Exception {
        // Initialize the database
        etapeRepository.saveAndFlush(etape);

        int databaseSizeBeforeDelete = etapeRepository.findAll().size();

        // Delete the etape
        restEtapeMockMvc.perform(delete("/api/etapes/{id}", etape.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Etape> etapeList = etapeRepository.findAll();
        assertThat(etapeList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Etape in Elasticsearch
        verify(mockEtapeSearchRepository, times(1)).deleteById(etape.getId());
    }

    @Test
    @Transactional
    public void searchEtape() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        etapeRepository.saveAndFlush(etape);
        when(mockEtapeSearchRepository.search(queryStringQuery("id:" + etape.getId())))
            .thenReturn(Collections.singletonList(etape));

        // Search the etape
        restEtapeMockMvc.perform(get("/api/_search/etapes?query=id:" + etape.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(etape.getId().intValue())))
            .andExpect(jsonPath("$.[*].libelle").value(hasItem(DEFAULT_LIBELLE)))
            .andExpect(jsonPath("$.[*].niveau").value(hasItem(DEFAULT_NIVEAU)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)));
    }
}
