package org.nddngroup.navetfoot.core.web.rest;

import org.nddngroup.navetfoot.core.NavetfootCore;
import org.nddngroup.navetfoot.core.domain.Poule;
import org.nddngroup.navetfoot.core.repository.PouleRepository;
import org.nddngroup.navetfoot.core.repository.search.PouleSearchRepository;
import org.nddngroup.navetfoot.core.service.PouleService;
import org.nddngroup.navetfoot.core.service.dto.PouleDTO;
import org.nddngroup.navetfoot.core.service.mapper.PouleMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.nddngroup.navetfoot.core.repository.search.PouleSearchRepositoryMockConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static org.nddngroup.navetfoot.core.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PouleResource} REST controller.
 */
@SpringBootTest(classes = NavetfootCore.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class PouleResourceIT {

    private static final String DEFAULT_LIBELLE = "AAAAAAAAAA";
    private static final String UPDATED_LIBELLE = "BBBBBBBBBB";

    private static final Integer DEFAULT_NOMBRE_EQUIPE = 1;
    private static final Integer UPDATED_NOMBRE_EQUIPE = 2;

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_HASHCODE = "AAAAAAAAAA";
    private static final String UPDATED_HASHCODE = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Boolean DEFAULT_DELETED = false;
    private static final Boolean UPDATED_DELETED = true;

    private static final Integer DEFAULT_NOMBRE_JOURNEE = 1;
    private static final Integer UPDATED_NOMBRE_JOURNEE = 2;

    @Autowired
    private PouleRepository pouleRepository;

    @Autowired
    private PouleMapper pouleMapper;

    @Autowired
    private PouleService pouleService;

    /**
     * This repository is mocked in the org.nddngroup.navetfoot.core.repository.search test package.
     *
     * @see PouleSearchRepositoryMockConfiguration
     */
    @Autowired
    private PouleSearchRepository mockPouleSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPouleMockMvc;

    private Poule poule;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Poule createEntity(EntityManager em) {
        Poule poule = new Poule()
            .libelle(DEFAULT_LIBELLE)
            .nombreEquipe(DEFAULT_NOMBRE_EQUIPE)
            .code(DEFAULT_CODE)
            .hashcode(DEFAULT_HASHCODE)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT)
            .deleted(DEFAULT_DELETED)
            .nombreJournee(DEFAULT_NOMBRE_JOURNEE);
        return poule;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Poule createUpdatedEntity(EntityManager em) {
        Poule poule = new Poule()
            .libelle(UPDATED_LIBELLE)
            .nombreEquipe(UPDATED_NOMBRE_EQUIPE)
            .code(UPDATED_CODE)
            .hashcode(UPDATED_HASHCODE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .deleted(UPDATED_DELETED)
            .nombreJournee(UPDATED_NOMBRE_JOURNEE);
        return poule;
    }

    @BeforeEach
    public void initTest() {
        poule = createEntity(em);
    }

    @Test
    @Transactional
    public void createPoule() throws Exception {
        int databaseSizeBeforeCreate = pouleRepository.findAll().size();
        // Create the Poule
        PouleDTO pouleDTO = pouleMapper.toDto(poule);
        restPouleMockMvc.perform(post("/api/poules")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(pouleDTO)))
            .andExpect(status().isCreated());

        // Validate the Poule in the database
        List<Poule> pouleList = pouleRepository.findAll();
        assertThat(pouleList).hasSize(databaseSizeBeforeCreate + 1);
        Poule testPoule = pouleList.get(pouleList.size() - 1);
        assertThat(testPoule.getLibelle()).isEqualTo(DEFAULT_LIBELLE);
        assertThat(testPoule.getNombreEquipe()).isEqualTo(DEFAULT_NOMBRE_EQUIPE);
        assertThat(testPoule.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testPoule.getHashcode()).isEqualTo(DEFAULT_HASHCODE);
        assertThat(testPoule.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testPoule.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testPoule.isDeleted()).isEqualTo(DEFAULT_DELETED);
        assertThat(testPoule.getNombreJournee()).isEqualTo(DEFAULT_NOMBRE_JOURNEE);

        // Validate the Poule in Elasticsearch
        verify(mockPouleSearchRepository, times(1)).save(testPoule);
    }

    @Test
    @Transactional
    public void createPouleWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = pouleRepository.findAll().size();

        // Create the Poule with an existing ID
        poule.setId(1L);
        PouleDTO pouleDTO = pouleMapper.toDto(poule);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPouleMockMvc.perform(post("/api/poules")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(pouleDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Poule in the database
        List<Poule> pouleList = pouleRepository.findAll();
        assertThat(pouleList).hasSize(databaseSizeBeforeCreate);

        // Validate the Poule in Elasticsearch
        verify(mockPouleSearchRepository, times(0)).save(poule);
    }


    @Test
    @Transactional
    public void checkLibelleIsRequired() throws Exception {
        int databaseSizeBeforeTest = pouleRepository.findAll().size();
        // set the field null
        poule.setLibelle(null);

        // Create the Poule, which fails.
        PouleDTO pouleDTO = pouleMapper.toDto(poule);


        restPouleMockMvc.perform(post("/api/poules")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(pouleDTO)))
            .andExpect(status().isBadRequest());

        List<Poule> pouleList = pouleRepository.findAll();
        assertThat(pouleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNombreEquipeIsRequired() throws Exception {
        int databaseSizeBeforeTest = pouleRepository.findAll().size();
        // set the field null
        poule.setNombreEquipe(null);

        // Create the Poule, which fails.
        PouleDTO pouleDTO = pouleMapper.toDto(poule);


        restPouleMockMvc.perform(post("/api/poules")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(pouleDTO)))
            .andExpect(status().isBadRequest());

        List<Poule> pouleList = pouleRepository.findAll();
        assertThat(pouleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPoules() throws Exception {
        // Initialize the database
        pouleRepository.saveAndFlush(poule);

        // Get all the pouleList
        restPouleMockMvc.perform(get("/api/poules?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(poule.getId().intValue())))
            .andExpect(jsonPath("$.[*].libelle").value(hasItem(DEFAULT_LIBELLE)))
            .andExpect(jsonPath("$.[*].nombreEquipe").value(hasItem(DEFAULT_NOMBRE_EQUIPE)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].hashcode").value(hasItem(DEFAULT_HASHCODE)))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].nombreJournee").value(hasItem(DEFAULT_NOMBRE_JOURNEE)));
    }

    @Test
    @Transactional
    public void getPoule() throws Exception {
        // Initialize the database
        pouleRepository.saveAndFlush(poule);

        // Get the poule
        restPouleMockMvc.perform(get("/api/poules/{id}", poule.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(poule.getId().intValue()))
            .andExpect(jsonPath("$.libelle").value(DEFAULT_LIBELLE))
            .andExpect(jsonPath("$.nombreEquipe").value(DEFAULT_NOMBRE_EQUIPE))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE))
            .andExpect(jsonPath("$.hashcode").value(DEFAULT_HASHCODE))
            .andExpect(jsonPath("$.createdAt").value(sameInstant(DEFAULT_CREATED_AT)))
            .andExpect(jsonPath("$.updatedAt").value(sameInstant(DEFAULT_UPDATED_AT)))
            .andExpect(jsonPath("$.deleted").value(DEFAULT_DELETED.booleanValue()))
            .andExpect(jsonPath("$.nombreJournee").value(DEFAULT_NOMBRE_JOURNEE));
    }
    @Test
    @Transactional
    public void getNonExistingPoule() throws Exception {
        // Get the poule
        restPouleMockMvc.perform(get("/api/poules/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePoule() throws Exception {
        // Initialize the database
        pouleRepository.saveAndFlush(poule);

        int databaseSizeBeforeUpdate = pouleRepository.findAll().size();

        // Update the poule
        Poule updatedPoule = pouleRepository.findById(poule.getId()).get();
        // Disconnect from session so that the updates on updatedPoule are not directly saved in db
        em.detach(updatedPoule);
        updatedPoule
            .libelle(UPDATED_LIBELLE)
            .nombreEquipe(UPDATED_NOMBRE_EQUIPE)
            .code(UPDATED_CODE)
            .hashcode(UPDATED_HASHCODE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .nombreJournee(UPDATED_NOMBRE_JOURNEE);
        PouleDTO pouleDTO = pouleMapper.toDto(updatedPoule);

        restPouleMockMvc.perform(put("/api/poules")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(pouleDTO)))
            .andExpect(status().isOk());

        // Validate the Poule in the database
        List<Poule> pouleList = pouleRepository.findAll();
        assertThat(pouleList).hasSize(databaseSizeBeforeUpdate);
        Poule testPoule = pouleList.get(pouleList.size() - 1);
        assertThat(testPoule.getLibelle()).isEqualTo(UPDATED_LIBELLE);
        assertThat(testPoule.getNombreEquipe()).isEqualTo(UPDATED_NOMBRE_EQUIPE);
        assertThat(testPoule.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testPoule.getHashcode()).isEqualTo(UPDATED_HASHCODE);
        assertThat(testPoule.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testPoule.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testPoule.getNombreJournee()).isEqualTo(UPDATED_NOMBRE_JOURNEE);

        // Validate the Poule in Elasticsearch
        verify(mockPouleSearchRepository, times(1)).save(testPoule);
    }

    @Test
    @Transactional
    public void updateNonExistingPoule() throws Exception {
        int databaseSizeBeforeUpdate = pouleRepository.findAll().size();

        // Create the Poule
        PouleDTO pouleDTO = pouleMapper.toDto(poule);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPouleMockMvc.perform(put("/api/poules")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(pouleDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Poule in the database
        List<Poule> pouleList = pouleRepository.findAll();
        assertThat(pouleList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Poule in Elasticsearch
        verify(mockPouleSearchRepository, times(0)).save(poule);
    }

    @Test
    @Transactional
    public void deletePoule() throws Exception {
        // Initialize the database
        pouleRepository.saveAndFlush(poule);

        int databaseSizeBeforeDelete = pouleRepository.findAll().size();

        // Delete the poule
        restPouleMockMvc.perform(delete("/api/poules/{id}", poule.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Poule> pouleList = pouleRepository.findAll();
        assertThat(pouleList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Poule in Elasticsearch
        verify(mockPouleSearchRepository, times(1)).deleteById(poule.getId());
    }

    @Test
    @Transactional
    public void searchPoule() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        pouleRepository.saveAndFlush(poule);
        when(mockPouleSearchRepository.search(queryStringQuery("id:" + poule.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(poule), PageRequest.of(0, 1), 1));

        // Search the poule
        restPouleMockMvc.perform(get("/api/_search/poules?query=id:" + poule.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(poule.getId().intValue())))
            .andExpect(jsonPath("$.[*].libelle").value(hasItem(DEFAULT_LIBELLE)))
            .andExpect(jsonPath("$.[*].nombreEquipe").value(hasItem(DEFAULT_NOMBRE_EQUIPE)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].hashcode").value(hasItem(DEFAULT_HASHCODE)))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].nombreJournee").value(hasItem(DEFAULT_NOMBRE_JOURNEE)));
    }
}
