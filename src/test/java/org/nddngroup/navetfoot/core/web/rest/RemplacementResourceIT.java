package org.nddngroup.navetfoot.core.web.rest;

import org.nddngroup.navetfoot.core.NavetfootCore;
import org.nddngroup.navetfoot.core.domain.Remplacement;
import org.nddngroup.navetfoot.core.repository.RemplacementRepository;
import org.nddngroup.navetfoot.core.repository.search.RemplacementSearchRepository;
import org.nddngroup.navetfoot.core.service.RemplacementService;
import org.nddngroup.navetfoot.core.service.dto.RemplacementDTO;
import org.nddngroup.navetfoot.core.service.mapper.RemplacementMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.nddngroup.navetfoot.core.repository.search.RemplacementSearchRepositoryMockConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static org.nddngroup.navetfoot.core.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;
/**
 * Integration tests for the {@link RemplacementResource} REST controller.
 */
@SpringBootTest(classes = NavetfootCore.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class RemplacementResourceIT {

    private static final String DEFAULT_HASHCODE = "AAAAAAAAAA";
    private static final String UPDATED_HASHCODE = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final Integer DEFAULT_INSTANT = 1;
    private static final Integer UPDATED_INSTANT = 2;

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Equipe DEFAULT_EQUIPE = Equipe.LOCAL;
    private static final Equipe UPDATED_EQUIPE = Equipe.VISITEUR;

    private static final Boolean DEFAULT_DELETED = false;
    private static final Boolean UPDATED_DELETED = true;

    @Autowired
    private RemplacementRepository remplacementRepository;

    @Autowired
    private RemplacementMapper remplacementMapper;

    @Autowired
    private RemplacementService remplacementService;

    /**
     * This repository is mocked in the org.nddngroup.navetfoot.core.repository.search test package.
     *
     * @see RemplacementSearchRepositoryMockConfiguration
     */
    @Autowired
    private RemplacementSearchRepository mockRemplacementSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restRemplacementMockMvc;

    private Remplacement remplacement;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Remplacement createEntity(EntityManager em) {
        Remplacement remplacement = new Remplacement()
            .hashcode(DEFAULT_HASHCODE)
            .code(DEFAULT_CODE)
            .instant(DEFAULT_INSTANT)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT)
            .equipe(DEFAULT_EQUIPE)
            .deleted(DEFAULT_DELETED);
        return remplacement;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Remplacement createUpdatedEntity(EntityManager em) {
        Remplacement remplacement = new Remplacement()
            .hashcode(UPDATED_HASHCODE)
            .code(UPDATED_CODE)
            .instant(UPDATED_INSTANT)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .equipe(UPDATED_EQUIPE)
            .deleted(UPDATED_DELETED);
        return remplacement;
    }

    @BeforeEach
    public void initTest() {
        remplacement = createEntity(em);
    }

    @Test
    @Transactional
    public void createRemplacement() throws Exception {
        int databaseSizeBeforeCreate = remplacementRepository.findAll().size();
        // Create the Remplacement
        RemplacementDTO remplacementDTO = remplacementMapper.toDto(remplacement);
        restRemplacementMockMvc.perform(post("/api/remplacements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(remplacementDTO)))
            .andExpect(status().isCreated());

        // Validate the Remplacement in the database
        List<Remplacement> remplacementList = remplacementRepository.findAll();
        assertThat(remplacementList).hasSize(databaseSizeBeforeCreate + 1);
        Remplacement testRemplacement = remplacementList.get(remplacementList.size() - 1);
        assertThat(testRemplacement.getHashcode()).isEqualTo(DEFAULT_HASHCODE);
        assertThat(testRemplacement.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testRemplacement.getInstant()).isEqualTo(DEFAULT_INSTANT);
        assertThat(testRemplacement.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testRemplacement.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testRemplacement.getEquipe()).isEqualTo(DEFAULT_EQUIPE);
        assertThat(testRemplacement.isDeleted()).isEqualTo(DEFAULT_DELETED);

        // Validate the Remplacement in Elasticsearch
        verify(mockRemplacementSearchRepository, times(1)).save(testRemplacement);
    }

    @Test
    @Transactional
    public void createRemplacementWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = remplacementRepository.findAll().size();

        // Create the Remplacement with an existing ID
        remplacement.setId(1L);
        RemplacementDTO remplacementDTO = remplacementMapper.toDto(remplacement);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRemplacementMockMvc.perform(post("/api/remplacements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(remplacementDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Remplacement in the database
        List<Remplacement> remplacementList = remplacementRepository.findAll();
        assertThat(remplacementList).hasSize(databaseSizeBeforeCreate);

        // Validate the Remplacement in Elasticsearch
        verify(mockRemplacementSearchRepository, times(0)).save(remplacement);
    }


    @Test
    @Transactional
    public void getAllRemplacements() throws Exception {
        // Initialize the database
        remplacementRepository.saveAndFlush(remplacement);

        // Get all the remplacementList
        restRemplacementMockMvc.perform(get("/api/remplacements?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(remplacement.getId().intValue())))
            .andExpect(jsonPath("$.[*].hashcode").value(hasItem(DEFAULT_HASHCODE)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].instant").value(hasItem(DEFAULT_INSTANT)))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].equipe").value(hasItem(DEFAULT_EQUIPE.toString())))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())));
    }

    @Test
    @Transactional
    public void getRemplacement() throws Exception {
        // Initialize the database
        remplacementRepository.saveAndFlush(remplacement);

        // Get the remplacement
        restRemplacementMockMvc.perform(get("/api/remplacements/{id}", remplacement.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(remplacement.getId().intValue()))
            .andExpect(jsonPath("$.hashcode").value(DEFAULT_HASHCODE))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE))
            .andExpect(jsonPath("$.instant").value(DEFAULT_INSTANT))
            .andExpect(jsonPath("$.createdAt").value(sameInstant(DEFAULT_CREATED_AT)))
            .andExpect(jsonPath("$.updatedAt").value(sameInstant(DEFAULT_UPDATED_AT)))
            .andExpect(jsonPath("$.equipe").value(DEFAULT_EQUIPE.toString()))
            .andExpect(jsonPath("$.deleted").value(DEFAULT_DELETED.booleanValue()));
    }
    @Test
    @Transactional
    public void getNonExistingRemplacement() throws Exception {
        // Get the remplacement
        restRemplacementMockMvc.perform(get("/api/remplacements/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRemplacement() throws Exception {
        // Initialize the database
        remplacementRepository.saveAndFlush(remplacement);

        int databaseSizeBeforeUpdate = remplacementRepository.findAll().size();

        // Update the remplacement
        Remplacement updatedRemplacement = remplacementRepository.findById(remplacement.getId()).get();
        // Disconnect from session so that the updates on updatedRemplacement are not directly saved in db
        em.detach(updatedRemplacement);
        updatedRemplacement
            .hashcode(UPDATED_HASHCODE)
            .code(UPDATED_CODE)
            .instant(UPDATED_INSTANT)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .equipe(UPDATED_EQUIPE);
        RemplacementDTO remplacementDTO = remplacementMapper.toDto(updatedRemplacement);

        restRemplacementMockMvc.perform(put("/api/remplacements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(remplacementDTO)))
            .andExpect(status().isOk());

        // Validate the Remplacement in the database
        List<Remplacement> remplacementList = remplacementRepository.findAll();
        assertThat(remplacementList).hasSize(databaseSizeBeforeUpdate);
        Remplacement testRemplacement = remplacementList.get(remplacementList.size() - 1);
        assertThat(testRemplacement.getHashcode()).isEqualTo(UPDATED_HASHCODE);
        assertThat(testRemplacement.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testRemplacement.getInstant()).isEqualTo(UPDATED_INSTANT);
        assertThat(testRemplacement.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testRemplacement.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testRemplacement.getEquipe()).isEqualTo(UPDATED_EQUIPE);

        // Validate the Remplacement in Elasticsearch
        verify(mockRemplacementSearchRepository, times(1)).save(testRemplacement);
    }

    @Test
    @Transactional
    public void updateNonExistingRemplacement() throws Exception {
        int databaseSizeBeforeUpdate = remplacementRepository.findAll().size();

        // Create the Remplacement
        RemplacementDTO remplacementDTO = remplacementMapper.toDto(remplacement);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRemplacementMockMvc.perform(put("/api/remplacements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(remplacementDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Remplacement in the database
        List<Remplacement> remplacementList = remplacementRepository.findAll();
        assertThat(remplacementList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Remplacement in Elasticsearch
        verify(mockRemplacementSearchRepository, times(0)).save(remplacement);
    }

    @Test
    @Transactional
    public void deleteRemplacement() throws Exception {
        // Initialize the database
        remplacementRepository.saveAndFlush(remplacement);

        int databaseSizeBeforeDelete = remplacementRepository.findAll().size();

        // Delete the remplacement
        restRemplacementMockMvc.perform(delete("/api/remplacements/{id}", remplacement.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Remplacement> remplacementList = remplacementRepository.findAll();
        assertThat(remplacementList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Remplacement in Elasticsearch
        verify(mockRemplacementSearchRepository, times(1)).deleteById(remplacement.getId());
    }

    @Test
    @Transactional
    public void searchRemplacement() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        remplacementRepository.saveAndFlush(remplacement);
        when(mockRemplacementSearchRepository.search(queryStringQuery("id:" + remplacement.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(remplacement), PageRequest.of(0, 1), 1));

        // Search the remplacement
        restRemplacementMockMvc.perform(get("/api/_search/remplacements?query=id:" + remplacement.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(remplacement.getId().intValue())))
            .andExpect(jsonPath("$.[*].hashcode").value(hasItem(DEFAULT_HASHCODE)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].instant").value(hasItem(DEFAULT_INSTANT)))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].equipe").value(hasItem(DEFAULT_EQUIPE.toString())))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())));
    }
}
