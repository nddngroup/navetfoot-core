package org.nddngroup.navetfoot.core.web.rest;

import org.nddngroup.navetfoot.core.NavetfootCore;
import org.nddngroup.navetfoot.core.domain.DelegueMatch;
import org.nddngroup.navetfoot.core.domain.Match;
import org.nddngroup.navetfoot.core.domain.Delegue;
import org.nddngroup.navetfoot.core.repository.DelegueMatchRepository;
import org.nddngroup.navetfoot.core.repository.search.DelegueMatchSearchRepository;
import org.nddngroup.navetfoot.core.service.DelegueMatchService;
import org.nddngroup.navetfoot.core.service.dto.DelegueMatchDTO;
import org.nddngroup.navetfoot.core.service.mapper.DelegueMatchMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.nddngroup.navetfoot.core.repository.search.DelegueMatchSearchRepositoryMockConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static org.nddngroup.navetfoot.core.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link DelegueMatchResource} REST controller.
 */
@SpringBootTest(classes = NavetfootCore.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class DelegueMatchResourceIT {

    private static final String DEFAULT_HASHCODE = "AAAAAAAAAA";
    private static final String UPDATED_HASHCODE = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_MATCH_CODE = "AAAAAAAAAA";
    private static final String UPDATED_MATCH_CODE = "BBBBBBBBBB";

    private static final Boolean DEFAULT_DELETED = false;
    private static final Boolean UPDATED_DELETED = true;

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_CONFIRM_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CONFIRM_CODE = "BBBBBBBBBB";

    @Autowired
    private DelegueMatchRepository delegueMatchRepository;

    @Autowired
    private DelegueMatchMapper delegueMatchMapper;

    @Autowired
    private DelegueMatchService delegueMatchService;

    /**
     * This repository is mocked in the org.nddngroup.navetfoot.core.repository.search test package.
     *
     * @see DelegueMatchSearchRepositoryMockConfiguration
     */
    @Autowired
    private DelegueMatchSearchRepository mockDelegueMatchSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDelegueMatchMockMvc;

    private DelegueMatch delegueMatch;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DelegueMatch createEntity(EntityManager em) {
        DelegueMatch delegueMatch = new DelegueMatch()
            .hashcode(DEFAULT_HASHCODE)
            .code(DEFAULT_CODE)
            .matchCode(DEFAULT_MATCH_CODE)
            .deleted(DEFAULT_DELETED)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT)
            .status(DEFAULT_STATUS)
            .confirmCode(DEFAULT_CONFIRM_CODE);
        // Add required entity
        Match match;
        if (TestUtil.findAll(em, Match.class).isEmpty()) {
            match = MatchResourceIT.createEntity(em);
            em.persist(match);
            em.flush();
        } else {
            match = TestUtil.findAll(em, Match.class).get(0);
        }
        delegueMatch.setMatch(match);
        // Add required entity
        Delegue delegue;
        if (TestUtil.findAll(em, Delegue.class).isEmpty()) {
            delegue = DelegueResourceIT.createEntity(em);
            em.persist(delegue);
            em.flush();
        } else {
            delegue = TestUtil.findAll(em, Delegue.class).get(0);
        }
        delegueMatch.setDelegue(delegue);
        return delegueMatch;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DelegueMatch createUpdatedEntity(EntityManager em) {
        DelegueMatch delegueMatch = new DelegueMatch()
            .hashcode(UPDATED_HASHCODE)
            .code(UPDATED_CODE)
            .matchCode(UPDATED_MATCH_CODE)
            .deleted(UPDATED_DELETED)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .status(UPDATED_STATUS)
            .confirmCode(UPDATED_CONFIRM_CODE);
        // Add required entity
        Match match;
        if (TestUtil.findAll(em, Match.class).isEmpty()) {
            match = MatchResourceIT.createUpdatedEntity(em);
            em.persist(match);
            em.flush();
        } else {
            match = TestUtil.findAll(em, Match.class).get(0);
        }
        delegueMatch.setMatch(match);
        // Add required entity
        Delegue delegue;
        if (TestUtil.findAll(em, Delegue.class).isEmpty()) {
            delegue = DelegueResourceIT.createUpdatedEntity(em);
            em.persist(delegue);
            em.flush();
        } else {
            delegue = TestUtil.findAll(em, Delegue.class).get(0);
        }
        delegueMatch.setDelegue(delegue);
        return delegueMatch;
    }

    @BeforeEach
    public void initTest() {
        delegueMatch = createEntity(em);
    }

    @Test
    @Transactional
    public void createDelegueMatch() throws Exception {
        int databaseSizeBeforeCreate = delegueMatchRepository.findAll().size();
        // Create the DelegueMatch
        DelegueMatchDTO delegueMatchDTO = delegueMatchMapper.toDto(delegueMatch);
        restDelegueMatchMockMvc.perform(post("/api/delegue-matches")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(delegueMatchDTO)))
            .andExpect(status().isCreated());

        // Validate the DelegueMatch in the database
        List<DelegueMatch> delegueMatchList = delegueMatchRepository.findAll();
        assertThat(delegueMatchList).hasSize(databaseSizeBeforeCreate + 1);
        DelegueMatch testDelegueMatch = delegueMatchList.get(delegueMatchList.size() - 1);
        assertThat(testDelegueMatch.getHashcode()).isEqualTo(DEFAULT_HASHCODE);
        assertThat(testDelegueMatch.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testDelegueMatch.getMatchCode()).isEqualTo(DEFAULT_MATCH_CODE);
        assertThat(testDelegueMatch.isDeleted()).isEqualTo(DEFAULT_DELETED);
        assertThat(testDelegueMatch.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testDelegueMatch.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testDelegueMatch.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testDelegueMatch.getConfirmCode()).isEqualTo(DEFAULT_CONFIRM_CODE);

        // Validate the DelegueMatch in Elasticsearch
        verify(mockDelegueMatchSearchRepository, times(1)).save(testDelegueMatch);
    }

    @Test
    @Transactional
    public void createDelegueMatchWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = delegueMatchRepository.findAll().size();

        // Create the DelegueMatch with an existing ID
        delegueMatch.setId(1L);
        DelegueMatchDTO delegueMatchDTO = delegueMatchMapper.toDto(delegueMatch);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDelegueMatchMockMvc.perform(post("/api/delegue-matches")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(delegueMatchDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DelegueMatch in the database
        List<DelegueMatch> delegueMatchList = delegueMatchRepository.findAll();
        assertThat(delegueMatchList).hasSize(databaseSizeBeforeCreate);

        // Validate the DelegueMatch in Elasticsearch
        verify(mockDelegueMatchSearchRepository, times(0)).save(delegueMatch);
    }


    @Test
    @Transactional
    public void checkHashcodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = delegueMatchRepository.findAll().size();
        // set the field null
        delegueMatch.setHashcode(null);

        // Create the DelegueMatch, which fails.
        DelegueMatchDTO delegueMatchDTO = delegueMatchMapper.toDto(delegueMatch);


        restDelegueMatchMockMvc.perform(post("/api/delegue-matches")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(delegueMatchDTO)))
            .andExpect(status().isBadRequest());

        List<DelegueMatch> delegueMatchList = delegueMatchRepository.findAll();
        assertThat(delegueMatchList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = delegueMatchRepository.findAll().size();
        // set the field null
        delegueMatch.setCode(null);

        // Create the DelegueMatch, which fails.
        DelegueMatchDTO delegueMatchDTO = delegueMatchMapper.toDto(delegueMatch);


        restDelegueMatchMockMvc.perform(post("/api/delegue-matches")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(delegueMatchDTO)))
            .andExpect(status().isBadRequest());

        List<DelegueMatch> delegueMatchList = delegueMatchRepository.findAll();
        assertThat(delegueMatchList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDelegueMatches() throws Exception {
        // Initialize the database
        delegueMatchRepository.saveAndFlush(delegueMatch);

        // Get all the delegueMatchList
        restDelegueMatchMockMvc.perform(get("/api/delegue-matches?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(delegueMatch.getId().intValue())))
            .andExpect(jsonPath("$.[*].hashcode").value(hasItem(DEFAULT_HASHCODE)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].matchCode").value(hasItem(DEFAULT_MATCH_CODE)))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].confirmCode").value(hasItem(DEFAULT_CONFIRM_CODE)));
    }

    @Test
    @Transactional
    public void getDelegueMatch() throws Exception {
        // Initialize the database
        delegueMatchRepository.saveAndFlush(delegueMatch);

        // Get the delegueMatch
        restDelegueMatchMockMvc.perform(get("/api/delegue-matches/{id}", delegueMatch.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(delegueMatch.getId().intValue()))
            .andExpect(jsonPath("$.hashcode").value(DEFAULT_HASHCODE))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE))
            .andExpect(jsonPath("$.matchCode").value(DEFAULT_MATCH_CODE))
            .andExpect(jsonPath("$.deleted").value(DEFAULT_DELETED.booleanValue()))
            .andExpect(jsonPath("$.createdAt").value(sameInstant(DEFAULT_CREATED_AT)))
            .andExpect(jsonPath("$.updatedAt").value(sameInstant(DEFAULT_UPDATED_AT)))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.confirmCode").value(DEFAULT_CONFIRM_CODE));
    }
    @Test
    @Transactional
    public void getNonExistingDelegueMatch() throws Exception {
        // Get the delegueMatch
        restDelegueMatchMockMvc.perform(get("/api/delegue-matches/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDelegueMatch() throws Exception {
        // Initialize the database
        delegueMatchRepository.saveAndFlush(delegueMatch);

        int databaseSizeBeforeUpdate = delegueMatchRepository.findAll().size();

        // Update the delegueMatch
        DelegueMatch updatedDelegueMatch = delegueMatchRepository.findById(delegueMatch.getId()).get();
        // Disconnect from session so that the updates on updatedDelegueMatch are not directly saved in db
        em.detach(updatedDelegueMatch);
        updatedDelegueMatch
            .hashcode(UPDATED_HASHCODE)
            .code(UPDATED_CODE)
            .matchCode(UPDATED_MATCH_CODE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .status(UPDATED_STATUS)
            .confirmCode(UPDATED_CONFIRM_CODE);
        DelegueMatchDTO delegueMatchDTO = delegueMatchMapper.toDto(updatedDelegueMatch);

        restDelegueMatchMockMvc.perform(put("/api/delegue-matches")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(delegueMatchDTO)))
            .andExpect(status().isOk());

        // Validate the DelegueMatch in the database
        List<DelegueMatch> delegueMatchList = delegueMatchRepository.findAll();
        assertThat(delegueMatchList).hasSize(databaseSizeBeforeUpdate);
        DelegueMatch testDelegueMatch = delegueMatchList.get(delegueMatchList.size() - 1);
        assertThat(testDelegueMatch.getHashcode()).isEqualTo(UPDATED_HASHCODE);
        assertThat(testDelegueMatch.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testDelegueMatch.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testDelegueMatch.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testDelegueMatch.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testDelegueMatch.getConfirmCode()).isEqualTo(UPDATED_CONFIRM_CODE);

        // Validate the DelegueMatch in Elasticsearch
        verify(mockDelegueMatchSearchRepository, times(1)).save(testDelegueMatch);
    }

    @Test
    @Transactional
    public void updateNonExistingDelegueMatch() throws Exception {
        int databaseSizeBeforeUpdate = delegueMatchRepository.findAll().size();

        // Create the DelegueMatch
        DelegueMatchDTO delegueMatchDTO = delegueMatchMapper.toDto(delegueMatch);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDelegueMatchMockMvc.perform(put("/api/delegue-matches")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(delegueMatchDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DelegueMatch in the database
        List<DelegueMatch> delegueMatchList = delegueMatchRepository.findAll();
        assertThat(delegueMatchList).hasSize(databaseSizeBeforeUpdate);

        // Validate the DelegueMatch in Elasticsearch
        verify(mockDelegueMatchSearchRepository, times(0)).save(delegueMatch);
    }

    @Test
    @Transactional
    public void deleteDelegueMatch() throws Exception {
        // Initialize the database
        delegueMatchRepository.saveAndFlush(delegueMatch);

        int databaseSizeBeforeDelete = delegueMatchRepository.findAll().size();

        // Delete the delegueMatch
        restDelegueMatchMockMvc.perform(delete("/api/delegue-matches/{id}", delegueMatch.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DelegueMatch> delegueMatchList = delegueMatchRepository.findAll();
        assertThat(delegueMatchList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the DelegueMatch in Elasticsearch
        verify(mockDelegueMatchSearchRepository, times(1)).deleteById(delegueMatch.getId());
    }

    @Test
    @Transactional
    public void searchDelegueMatch() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        delegueMatchRepository.saveAndFlush(delegueMatch);
        when(mockDelegueMatchSearchRepository.search(queryStringQuery("id:" + delegueMatch.getId())))
            .thenReturn(Collections.singletonList(delegueMatch));

        // Search the delegueMatch
        restDelegueMatchMockMvc.perform(get("/api/_search/delegue-matches?query=id:" + delegueMatch.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(delegueMatch.getId().intValue())))
            .andExpect(jsonPath("$.[*].hashcode").value(hasItem(DEFAULT_HASHCODE)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].matchCode").value(hasItem(DEFAULT_MATCH_CODE)))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].confirmCode").value(hasItem(DEFAULT_CONFIRM_CODE)));
    }
}
