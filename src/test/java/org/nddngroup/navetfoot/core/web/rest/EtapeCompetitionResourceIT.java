package org.nddngroup.navetfoot.core.web.rest;

import org.nddngroup.navetfoot.core.NavetfootCore;
import org.nddngroup.navetfoot.core.domain.CompetitionSaison;
import org.nddngroup.navetfoot.core.domain.EtapeCompetition;
import org.nddngroup.navetfoot.core.repository.EtapeCompetitionRepository;
import org.nddngroup.navetfoot.core.repository.search.EtapeCompetitionSearchRepository;
import org.nddngroup.navetfoot.core.service.EtapeCompetitionService;
import org.nddngroup.navetfoot.core.service.dto.EtapeCompetitionDTO;
import org.nddngroup.navetfoot.core.service.mapper.EtapeCompetitionMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.nddngroup.navetfoot.core.repository.search.EtapeCompetitionSearchRepositoryMockConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static org.nddngroup.navetfoot.core.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link EtapeCompetitionResource} REST controller.
 */
@SpringBootTest(classes = NavetfootCore.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class EtapeCompetitionResourceIT {

    private static final String DEFAULT_NOM = "AAAAAAAAAA";
    private static final String UPDATED_NOM = "BBBBBBBBBB";

    private static final String DEFAULT_HASHCODE = "AAAAAAAAAA";
    private static final String UPDATED_HASHCODE = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_DATE_DEBUT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_DEBUT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_DATE_FIN = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_FIN = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Boolean DEFAULT_DELETED = false;
    private static final Boolean UPDATED_DELETED = true;

    private static final Boolean DEFAULT_HAS_POULE = false;
    private static final Boolean UPDATED_HAS_POULE = true;

    private static final Boolean DEFAULT_ALLER_RETOUR = false;
    private static final Boolean UPDATED_ALLER_RETOUR = true;

    @Autowired
    private EtapeCompetitionRepository etapeCompetitionRepository;

    @Autowired
    private EtapeCompetitionMapper etapeCompetitionMapper;

    @Autowired
    private EtapeCompetitionService etapeCompetitionService;

    /**
     * This repository is mocked in the org.nddngroup.navetfoot.core.repository.search test package.
     *
     * @see EtapeCompetitionSearchRepositoryMockConfiguration
     */
    @Autowired
    private EtapeCompetitionSearchRepository mockEtapeCompetitionSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restEtapeCompetitionMockMvc;

    private EtapeCompetition etapeCompetition;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EtapeCompetition createEntity(EntityManager em) {
        EtapeCompetition etapeCompetition = new EtapeCompetition()
            .nom(DEFAULT_NOM)
            .hashcode(DEFAULT_HASHCODE)
            .code(DEFAULT_CODE)
            .dateDebut(DEFAULT_DATE_DEBUT)
            .dateFin(DEFAULT_DATE_FIN)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT)
            .deleted(DEFAULT_DELETED)
            .hasPoule(DEFAULT_HAS_POULE)
            .allerRetour(DEFAULT_ALLER_RETOUR);

        // add required entities
        CompetitionSaison competitionSaison;
        if (TestUtil.findAll(em, CompetitionSaison.class).isEmpty()) {
            competitionSaison = CompetitionSaisonResourceIT.createUpdatedEntity(em);
            em.persist(competitionSaison);
            em.flush();
        } else {
            competitionSaison = TestUtil.findAll(em, CompetitionSaison.class).get(0);
        }
        etapeCompetition.setCompetitionSaison(competitionSaison);

        return etapeCompetition;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EtapeCompetition createUpdatedEntity(EntityManager em) {
        EtapeCompetition etapeCompetition = new EtapeCompetition()
            .nom(UPDATED_NOM)
            .hashcode(UPDATED_HASHCODE)
            .code(UPDATED_CODE)
            .dateDebut(UPDATED_DATE_DEBUT)
            .dateFin(UPDATED_DATE_FIN)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .deleted(UPDATED_DELETED)
            .hasPoule(UPDATED_HAS_POULE)
            .allerRetour(UPDATED_ALLER_RETOUR);
        return etapeCompetition;
    }

    @BeforeEach
    public void initTest() {
        etapeCompetition = createEntity(em);
    }

    @Test
    @Transactional
    public void createEtapeCompetition() throws Exception {
        int databaseSizeBeforeCreate = etapeCompetitionRepository.findAll().size();
        // Create the EtapeCompetition
        EtapeCompetitionDTO etapeCompetitionDTO = etapeCompetitionMapper.toDto(etapeCompetition);
        restEtapeCompetitionMockMvc.perform(post("/api/etape-competitions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(etapeCompetitionDTO)))
            .andExpect(status().isCreated());

        // Validate the EtapeCompetition in the database
        List<EtapeCompetition> etapeCompetitionList = etapeCompetitionRepository.findAll();
        assertThat(etapeCompetitionList).hasSize(databaseSizeBeforeCreate + 1);
        EtapeCompetition testEtapeCompetition = etapeCompetitionList.get(etapeCompetitionList.size() - 1);
        assertThat(testEtapeCompetition.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testEtapeCompetition.getHashcode()).isEqualTo(DEFAULT_HASHCODE);
        assertThat(testEtapeCompetition.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testEtapeCompetition.getDateDebut()).isEqualTo(DEFAULT_DATE_DEBUT);
        assertThat(testEtapeCompetition.getDateFin()).isEqualTo(DEFAULT_DATE_FIN);
        assertThat(testEtapeCompetition.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testEtapeCompetition.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testEtapeCompetition.isDeleted()).isEqualTo(DEFAULT_DELETED);
        assertThat(testEtapeCompetition.isHasPoule()).isEqualTo(DEFAULT_HAS_POULE);
        assertThat(testEtapeCompetition.isAllerRetour()).isEqualTo(DEFAULT_ALLER_RETOUR);

        // Validate the EtapeCompetition in Elasticsearch
        verify(mockEtapeCompetitionSearchRepository, times(1)).save(testEtapeCompetition);
    }

    @Test
    @Transactional
    public void createEtapeCompetitionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = etapeCompetitionRepository.findAll().size();

        // Create the EtapeCompetition with an existing ID
        etapeCompetition.setId(1L);
        EtapeCompetitionDTO etapeCompetitionDTO = etapeCompetitionMapper.toDto(etapeCompetition);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEtapeCompetitionMockMvc.perform(post("/api/etape-competitions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(etapeCompetitionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the EtapeCompetition in the database
        List<EtapeCompetition> etapeCompetitionList = etapeCompetitionRepository.findAll();
        assertThat(etapeCompetitionList).hasSize(databaseSizeBeforeCreate);

        // Validate the EtapeCompetition in Elasticsearch
        verify(mockEtapeCompetitionSearchRepository, times(0)).save(etapeCompetition);
    }


    @Test
    @Transactional
    public void getAllEtapeCompetitions() throws Exception {
        // Initialize the database
        etapeCompetitionRepository.saveAndFlush(etapeCompetition);

        // Get all the etapeCompetitionList
        restEtapeCompetitionMockMvc.perform(get("/api/etape-competitions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(etapeCompetition.getId().intValue())))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)))
            .andExpect(jsonPath("$.[*].hashcode").value(hasItem(DEFAULT_HASHCODE)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].dateDebut").value(hasItem(sameInstant(DEFAULT_DATE_DEBUT))))
            .andExpect(jsonPath("$.[*].dateFin").value(hasItem(sameInstant(DEFAULT_DATE_FIN))))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].hasPoule").value(hasItem(DEFAULT_HAS_POULE.booleanValue())))
            .andExpect(jsonPath("$.[*].allerRetour").value(hasItem(DEFAULT_ALLER_RETOUR.booleanValue())));
    }

    @Test
    @Transactional
    public void getEtapeCompetition() throws Exception {
        // Initialize the database
        etapeCompetitionRepository.saveAndFlush(etapeCompetition);

        // Get the etapeCompetition
        restEtapeCompetitionMockMvc.perform(get("/api/etape-competitions/{id}", etapeCompetition.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(etapeCompetition.getId().intValue()))
            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM))
            .andExpect(jsonPath("$.hashcode").value(DEFAULT_HASHCODE))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE))
            .andExpect(jsonPath("$.dateDebut").value(sameInstant(DEFAULT_DATE_DEBUT)))
            .andExpect(jsonPath("$.dateFin").value(sameInstant(DEFAULT_DATE_FIN)))
            .andExpect(jsonPath("$.createdAt").value(sameInstant(DEFAULT_CREATED_AT)))
            .andExpect(jsonPath("$.updatedAt").value(sameInstant(DEFAULT_UPDATED_AT)))
            .andExpect(jsonPath("$.deleted").value(DEFAULT_DELETED.booleanValue()))
            .andExpect(jsonPath("$.hasPoule").value(DEFAULT_HAS_POULE.booleanValue()))
            .andExpect(jsonPath("$.allerRetour").value(DEFAULT_ALLER_RETOUR.booleanValue()));
    }
    @Test
    @Transactional
    public void getNonExistingEtapeCompetition() throws Exception {
        // Get the etapeCompetition
        restEtapeCompetitionMockMvc.perform(get("/api/etape-competitions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEtapeCompetition() throws Exception {
        // Initialize the database
        etapeCompetitionRepository.saveAndFlush(etapeCompetition);

        int databaseSizeBeforeUpdate = etapeCompetitionRepository.findAll().size();

        // Update the etapeCompetition
        EtapeCompetition updatedEtapeCompetition = etapeCompetitionRepository.findById(etapeCompetition.getId()).get();
        // Disconnect from session so that the updates on updatedEtapeCompetition are not directly saved in db
        em.detach(updatedEtapeCompetition);
        updatedEtapeCompetition
            .nom(UPDATED_NOM)
            .hashcode(UPDATED_HASHCODE)
            .code(UPDATED_CODE)
            .dateDebut(UPDATED_DATE_DEBUT)
            .dateFin(UPDATED_DATE_FIN)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .hasPoule(UPDATED_HAS_POULE)
            .allerRetour(UPDATED_ALLER_RETOUR);
        EtapeCompetitionDTO etapeCompetitionDTO = etapeCompetitionMapper.toDto(updatedEtapeCompetition);

        restEtapeCompetitionMockMvc.perform(put("/api/etape-competitions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(etapeCompetitionDTO)))
            .andExpect(status().isOk());

        // Validate the EtapeCompetition in the database
        List<EtapeCompetition> etapeCompetitionList = etapeCompetitionRepository.findAll();
        assertThat(etapeCompetitionList).hasSize(databaseSizeBeforeUpdate);
        EtapeCompetition testEtapeCompetition = etapeCompetitionList.get(etapeCompetitionList.size() - 1);
        assertThat(testEtapeCompetition.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testEtapeCompetition.getHashcode()).isEqualTo(UPDATED_HASHCODE);
        assertThat(testEtapeCompetition.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testEtapeCompetition.getDateDebut()).isEqualTo(UPDATED_DATE_DEBUT);
        assertThat(testEtapeCompetition.getDateFin()).isEqualTo(UPDATED_DATE_FIN);
        assertThat(testEtapeCompetition.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testEtapeCompetition.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testEtapeCompetition.isHasPoule()).isEqualTo(UPDATED_HAS_POULE);
        assertThat(testEtapeCompetition.isAllerRetour()).isEqualTo(UPDATED_ALLER_RETOUR);

        // Validate the EtapeCompetition in Elasticsearch
        verify(mockEtapeCompetitionSearchRepository, times(1)).save(testEtapeCompetition);
    }

    @Test
    @Transactional
    public void updateNonExistingEtapeCompetition() throws Exception {
        int databaseSizeBeforeUpdate = etapeCompetitionRepository.findAll().size();

        // Create the EtapeCompetition
        EtapeCompetitionDTO etapeCompetitionDTO = etapeCompetitionMapper.toDto(etapeCompetition);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEtapeCompetitionMockMvc.perform(put("/api/etape-competitions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(etapeCompetitionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the EtapeCompetition in the database
        List<EtapeCompetition> etapeCompetitionList = etapeCompetitionRepository.findAll();
        assertThat(etapeCompetitionList).hasSize(databaseSizeBeforeUpdate);

        // Validate the EtapeCompetition in Elasticsearch
        verify(mockEtapeCompetitionSearchRepository, times(0)).save(etapeCompetition);
    }

    @Test
    @Transactional
    public void deleteEtapeCompetition() throws Exception {
        // Initialize the database
        etapeCompetitionRepository.saveAndFlush(etapeCompetition);

        int databaseSizeBeforeDelete = etapeCompetitionRepository.findAll().size();

        // Delete the etapeCompetition
        restEtapeCompetitionMockMvc.perform(delete("/api/etape-competitions/{id}", etapeCompetition.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<EtapeCompetition> etapeCompetitionList = etapeCompetitionRepository.findAll();
        assertThat(etapeCompetitionList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the EtapeCompetition in Elasticsearch
        verify(mockEtapeCompetitionSearchRepository, times(1)).deleteById(etapeCompetition.getId());
    }

    @Test
    @Transactional
    public void searchEtapeCompetition() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        etapeCompetitionRepository.saveAndFlush(etapeCompetition);
        when(mockEtapeCompetitionSearchRepository.search(queryStringQuery("id:" + etapeCompetition.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(etapeCompetition), PageRequest.of(0, 1), 1));

        // Search the etapeCompetition
        restEtapeCompetitionMockMvc.perform(get("/api/_search/etape-competitions?query=id:" + etapeCompetition.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(etapeCompetition.getId().intValue())))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)))
            .andExpect(jsonPath("$.[*].hashcode").value(hasItem(DEFAULT_HASHCODE)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].dateDebut").value(hasItem(sameInstant(DEFAULT_DATE_DEBUT))))
            .andExpect(jsonPath("$.[*].dateFin").value(hasItem(sameInstant(DEFAULT_DATE_FIN))))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].hasPoule").value(hasItem(DEFAULT_HAS_POULE.booleanValue())))
            .andExpect(jsonPath("$.[*].allerRetour").value(hasItem(DEFAULT_ALLER_RETOUR.booleanValue())));
    }
}
