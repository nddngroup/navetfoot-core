package org.nddngroup.navetfoot.core.repository.search;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure a Mock version of {@link TirSearchRepository} to test the
 * application without starting Elasticsearch.
 */
@Configuration
public class TirSearchRepositoryMockConfiguration {

    @MockBean
    private TirSearchRepository mockTirSearchRepository;

}
