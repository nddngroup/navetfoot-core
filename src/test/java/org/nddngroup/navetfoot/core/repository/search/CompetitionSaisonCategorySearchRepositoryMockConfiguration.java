package org.nddngroup.navetfoot.core.repository.search;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure a Mock version of {@link CompetitionSaisonCategorySearchRepository} to test the
 * application without starting Elasticsearch.
 */
@Configuration
public class CompetitionSaisonCategorySearchRepositoryMockConfiguration {

    @MockBean
    private CompetitionSaisonCategorySearchRepository mockCompetitionSaisonCategorySearchRepository;

}
