package org.nddngroup.navetfoot.core.repository.search;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure a Mock version of {@link ContratSearchRepository} to test the
 * application without starting Elasticsearch.
 */
@Configuration
public class ContratSearchRepositoryMockConfiguration {

    @MockBean
    private ContratSearchRepository mockContratSearchRepository;

}
