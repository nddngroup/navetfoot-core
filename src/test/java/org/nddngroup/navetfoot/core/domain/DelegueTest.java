package org.nddngroup.navetfoot.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.nddngroup.navetfoot.core.web.rest.TestUtil;

public class DelegueTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Delegue.class);
        Delegue delegue1 = new Delegue();
        delegue1.setId(1L);
        Delegue delegue2 = new Delegue();
        delegue2.setId(delegue1.getId());
        assertThat(delegue1).isEqualTo(delegue2);
        delegue2.setId(2L);
        assertThat(delegue1).isNotEqualTo(delegue2);
        delegue1.setId(null);
        assertThat(delegue1).isNotEqualTo(delegue2);
    }
}
