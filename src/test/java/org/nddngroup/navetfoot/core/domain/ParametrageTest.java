package org.nddngroup.navetfoot.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.nddngroup.navetfoot.core.web.rest.TestUtil;

public class ParametrageTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Parametrage.class);
        Parametrage parametrage1 = new Parametrage();
        parametrage1.setId(1L);
        Parametrage parametrage2 = new Parametrage();
        parametrage2.setId(parametrage1.getId());
        assertThat(parametrage1).isEqualTo(parametrage2);
        parametrage2.setId(2L);
        assertThat(parametrage1).isNotEqualTo(parametrage2);
        parametrage1.setId(null);
        assertThat(parametrage1).isNotEqualTo(parametrage2);
    }
}
