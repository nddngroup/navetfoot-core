package org.nddngroup.navetfoot.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.nddngroup.navetfoot.core.web.rest.TestUtil;

public class TirTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Tir.class);
        Tir tir1 = new Tir();
        tir1.setId(1L);
        Tir tir2 = new Tir();
        tir2.setId(tir1.getId());
        assertThat(tir1).isEqualTo(tir2);
        tir2.setId(2L);
        assertThat(tir1).isNotEqualTo(tir2);
        tir1.setId(null);
        assertThat(tir1).isNotEqualTo(tir2);
    }
}
