package org.nddngroup.navetfoot.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.nddngroup.navetfoot.core.web.rest.TestUtil;

public class HorsJeuTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(HorsJeu.class);
        HorsJeu horsJeu1 = new HorsJeu();
        horsJeu1.setId(1L);
        HorsJeu horsJeu2 = new HorsJeu();
        horsJeu2.setId(horsJeu1.getId());
        assertThat(horsJeu1).isEqualTo(horsJeu2);
        horsJeu2.setId(2L);
        assertThat(horsJeu1).isNotEqualTo(horsJeu2);
        horsJeu1.setId(null);
        assertThat(horsJeu1).isNotEqualTo(horsJeu2);
    }
}
