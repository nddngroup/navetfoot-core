package org.nddngroup.navetfoot.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.nddngroup.navetfoot.core.web.rest.TestUtil;

public class AffiliationTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Affiliation.class);
        Affiliation affiliation1 = new Affiliation();
        affiliation1.setId(1L);
        Affiliation affiliation2 = new Affiliation();
        affiliation2.setId(affiliation1.getId());
        assertThat(affiliation1).isEqualTo(affiliation2);
        affiliation2.setId(2L);
        assertThat(affiliation1).isNotEqualTo(affiliation2);
        affiliation1.setId(null);
        assertThat(affiliation1).isNotEqualTo(affiliation2);
    }
}
