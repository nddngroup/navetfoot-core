package org.nddngroup.navetfoot.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.nddngroup.navetfoot.core.web.rest.TestUtil;

public class DelegueMatchTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DelegueMatch.class);
        DelegueMatch delegueMatch1 = new DelegueMatch();
        delegueMatch1.setId(1L);
        DelegueMatch delegueMatch2 = new DelegueMatch();
        delegueMatch2.setId(delegueMatch1.getId());
        assertThat(delegueMatch1).isEqualTo(delegueMatch2);
        delegueMatch2.setId(2L);
        assertThat(delegueMatch1).isNotEqualTo(delegueMatch2);
        delegueMatch1.setId(null);
        assertThat(delegueMatch1).isNotEqualTo(delegueMatch2);
    }
}
