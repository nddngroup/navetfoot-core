package org.nddngroup.navetfoot.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.nddngroup.navetfoot.core.web.rest.TestUtil;

public class DetailFeuilleDeMatchTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DetailFeuilleDeMatch.class);
        DetailFeuilleDeMatch detailFeuilleDeMatch1 = new DetailFeuilleDeMatch();
        detailFeuilleDeMatch1.setId(1L);
        DetailFeuilleDeMatch detailFeuilleDeMatch2 = new DetailFeuilleDeMatch();
        detailFeuilleDeMatch2.setId(detailFeuilleDeMatch1.getId());
        assertThat(detailFeuilleDeMatch1).isEqualTo(detailFeuilleDeMatch2);
        detailFeuilleDeMatch2.setId(2L);
        assertThat(detailFeuilleDeMatch1).isNotEqualTo(detailFeuilleDeMatch2);
        detailFeuilleDeMatch1.setId(null);
        assertThat(detailFeuilleDeMatch1).isNotEqualTo(detailFeuilleDeMatch2);
    }
}
