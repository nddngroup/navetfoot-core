package org.nddngroup.navetfoot.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.nddngroup.navetfoot.core.web.rest.TestUtil;

public class FeuilleDeMatchTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FeuilleDeMatch.class);
        FeuilleDeMatch feuilleDeMatch1 = new FeuilleDeMatch();
        feuilleDeMatch1.setId(1L);
        FeuilleDeMatch feuilleDeMatch2 = new FeuilleDeMatch();
        feuilleDeMatch2.setId(feuilleDeMatch1.getId());
        assertThat(feuilleDeMatch1).isEqualTo(feuilleDeMatch2);
        feuilleDeMatch2.setId(2L);
        assertThat(feuilleDeMatch1).isNotEqualTo(feuilleDeMatch2);
        feuilleDeMatch1.setId(null);
        assertThat(feuilleDeMatch1).isNotEqualTo(feuilleDeMatch2);
    }
}
