package org.nddngroup.navetfoot.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.nddngroup.navetfoot.core.web.rest.TestUtil;

public class PouleTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Poule.class);
        Poule poule1 = new Poule();
        poule1.setId(1L);
        Poule poule2 = new Poule();
        poule2.setId(poule1.getId());
        assertThat(poule1).isEqualTo(poule2);
        poule2.setId(2L);
        assertThat(poule1).isNotEqualTo(poule2);
        poule1.setId(null);
        assertThat(poule1).isNotEqualTo(poule2);
    }
}
