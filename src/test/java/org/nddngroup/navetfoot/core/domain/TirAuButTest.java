package org.nddngroup.navetfoot.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.nddngroup.navetfoot.core.web.rest.TestUtil;

public class TirAuButTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TirAuBut.class);
        TirAuBut tirAuBut1 = new TirAuBut();
        tirAuBut1.setId(1L);
        TirAuBut tirAuBut2 = new TirAuBut();
        tirAuBut2.setId(tirAuBut1.getId());
        assertThat(tirAuBut1).isEqualTo(tirAuBut2);
        tirAuBut2.setId(2L);
        assertThat(tirAuBut1).isNotEqualTo(tirAuBut2);
        tirAuBut1.setId(null);
        assertThat(tirAuBut1).isNotEqualTo(tirAuBut2);
    }
}
