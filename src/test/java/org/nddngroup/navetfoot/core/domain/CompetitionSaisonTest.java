package org.nddngroup.navetfoot.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.nddngroup.navetfoot.core.web.rest.TestUtil;

public class CompetitionSaisonTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CompetitionSaison.class);
        CompetitionSaison competitionSaison1 = new CompetitionSaison();
        competitionSaison1.setId(1L);
        CompetitionSaison competitionSaison2 = new CompetitionSaison();
        competitionSaison2.setId(competitionSaison1.getId());
        assertThat(competitionSaison1).isEqualTo(competitionSaison2);
        competitionSaison2.setId(2L);
        assertThat(competitionSaison1).isNotEqualTo(competitionSaison2);
        competitionSaison1.setId(null);
        assertThat(competitionSaison1).isNotEqualTo(competitionSaison2);
    }
}
