package org.nddngroup.navetfoot.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.nddngroup.navetfoot.core.web.rest.TestUtil;

public class DetailPouleTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DetailPoule.class);
        DetailPoule detailPoule1 = new DetailPoule();
        detailPoule1.setId(1L);
        DetailPoule detailPoule2 = new DetailPoule();
        detailPoule2.setId(detailPoule1.getId());
        assertThat(detailPoule1).isEqualTo(detailPoule2);
        detailPoule2.setId(2L);
        assertThat(detailPoule1).isNotEqualTo(detailPoule2);
        detailPoule1.setId(null);
        assertThat(detailPoule1).isNotEqualTo(detailPoule2);
    }
}
