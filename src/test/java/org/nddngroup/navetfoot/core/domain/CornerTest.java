package org.nddngroup.navetfoot.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.nddngroup.navetfoot.core.web.rest.TestUtil;

public class CornerTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Corner.class);
        Corner corner1 = new Corner();
        corner1.setId(1L);
        Corner corner2 = new Corner();
        corner2.setId(corner1.getId());
        assertThat(corner1).isEqualTo(corner2);
        corner2.setId(2L);
        assertThat(corner1).isNotEqualTo(corner2);
        corner1.setId(null);
        assertThat(corner1).isNotEqualTo(corner2);
    }
}
