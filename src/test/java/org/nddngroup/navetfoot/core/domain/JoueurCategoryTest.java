package org.nddngroup.navetfoot.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.nddngroup.navetfoot.core.web.rest.TestUtil;

public class JoueurCategoryTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(JoueurCategory.class);
        JoueurCategory joueurCategory1 = new JoueurCategory();
        joueurCategory1.setId(1L);
        JoueurCategory joueurCategory2 = new JoueurCategory();
        joueurCategory2.setId(joueurCategory1.getId());
        assertThat(joueurCategory1).isEqualTo(joueurCategory2);
        joueurCategory2.setId(2L);
        assertThat(joueurCategory1).isNotEqualTo(joueurCategory2);
        joueurCategory1.setId(null);
        assertThat(joueurCategory1).isNotEqualTo(joueurCategory2);
    }
}
