package org.nddngroup.navetfoot.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.nddngroup.navetfoot.core.web.rest.TestUtil;

public class ButTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(But.class);
        But but1 = new But();
        but1.setId(1L);
        But but2 = new But();
        but2.setId(but1.getId());
        assertThat(but1).isEqualTo(but2);
        but2.setId(2L);
        assertThat(but1).isNotEqualTo(but2);
        but1.setId(null);
        assertThat(but1).isNotEqualTo(but2);
    }
}
