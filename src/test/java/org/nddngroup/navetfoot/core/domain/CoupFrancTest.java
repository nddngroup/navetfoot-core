package org.nddngroup.navetfoot.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.nddngroup.navetfoot.core.web.rest.TestUtil;

public class CoupFrancTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CoupFranc.class);
        CoupFranc coupFranc1 = new CoupFranc();
        coupFranc1.setId(1L);
        CoupFranc coupFranc2 = new CoupFranc();
        coupFranc2.setId(coupFranc1.getId());
        assertThat(coupFranc1).isEqualTo(coupFranc2);
        coupFranc2.setId(2L);
        assertThat(coupFranc1).isNotEqualTo(coupFranc2);
        coupFranc1.setId(null);
        assertThat(coupFranc1).isNotEqualTo(coupFranc2);
    }
}
