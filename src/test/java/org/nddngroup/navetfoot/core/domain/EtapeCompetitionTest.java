package org.nddngroup.navetfoot.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.nddngroup.navetfoot.core.web.rest.TestUtil;

public class EtapeCompetitionTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(EtapeCompetition.class);
        EtapeCompetition etapeCompetition1 = new EtapeCompetition();
        etapeCompetition1.setId(1L);
        EtapeCompetition etapeCompetition2 = new EtapeCompetition();
        etapeCompetition2.setId(etapeCompetition1.getId());
        assertThat(etapeCompetition1).isEqualTo(etapeCompetition2);
        etapeCompetition2.setId(2L);
        assertThat(etapeCompetition1).isNotEqualTo(etapeCompetition2);
        etapeCompetition1.setId(null);
        assertThat(etapeCompetition1).isNotEqualTo(etapeCompetition2);
    }
}
