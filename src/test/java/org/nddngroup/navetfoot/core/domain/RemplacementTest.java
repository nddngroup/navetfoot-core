package org.nddngroup.navetfoot.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.nddngroup.navetfoot.core.web.rest.TestUtil;

public class RemplacementTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Remplacement.class);
        Remplacement remplacement1 = new Remplacement();
        remplacement1.setId(1L);
        Remplacement remplacement2 = new Remplacement();
        remplacement2.setId(remplacement1.getId());
        assertThat(remplacement1).isEqualTo(remplacement2);
        remplacement2.setId(2L);
        assertThat(remplacement1).isNotEqualTo(remplacement2);
        remplacement1.setId(null);
        assertThat(remplacement1).isNotEqualTo(remplacement2);
    }
}
