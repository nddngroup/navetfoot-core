package org.nddngroup.navetfoot.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.nddngroup.navetfoot.core.web.rest.TestUtil;

public class CompetitionSaisonCategoryTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CompetitionSaisonCategory.class);
        CompetitionSaisonCategory competitionSaisonCategory1 = new CompetitionSaisonCategory();
        competitionSaisonCategory1.setId(1L);
        CompetitionSaisonCategory competitionSaisonCategory2 = new CompetitionSaisonCategory();
        competitionSaisonCategory2.setId(competitionSaisonCategory1.getId());
        assertThat(competitionSaisonCategory1).isEqualTo(competitionSaisonCategory2);
        competitionSaisonCategory2.setId(2L);
        assertThat(competitionSaisonCategory1).isNotEqualTo(competitionSaisonCategory2);
        competitionSaisonCategory1.setId(null);
        assertThat(competitionSaisonCategory1).isNotEqualTo(competitionSaisonCategory2);
    }
}
