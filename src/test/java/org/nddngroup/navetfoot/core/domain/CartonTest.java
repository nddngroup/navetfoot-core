package org.nddngroup.navetfoot.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.nddngroup.navetfoot.core.web.rest.TestUtil;

public class CartonTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Carton.class);
        Carton carton1 = new Carton();
        carton1.setId(1L);
        Carton carton2 = new Carton();
        carton2.setId(carton1.getId());
        assertThat(carton1).isEqualTo(carton2);
        carton2.setId(2L);
        assertThat(carton1).isNotEqualTo(carton2);
        carton1.setId(null);
        assertThat(carton1).isNotEqualTo(carton2);
    }
}
