package org.nddngroup.navetfoot.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.nddngroup.navetfoot.core.web.rest.TestUtil;

public class DetailFeuilleDeMatchDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DetailFeuilleDeMatchDTO.class);
        DetailFeuilleDeMatchDTO detailFeuilleDeMatchDTO1 = new DetailFeuilleDeMatchDTO();
        detailFeuilleDeMatchDTO1.setId(1L);
        DetailFeuilleDeMatchDTO detailFeuilleDeMatchDTO2 = new DetailFeuilleDeMatchDTO();
        assertThat(detailFeuilleDeMatchDTO1).isNotEqualTo(detailFeuilleDeMatchDTO2);
        detailFeuilleDeMatchDTO2.setId(detailFeuilleDeMatchDTO1.getId());
        assertThat(detailFeuilleDeMatchDTO1).isEqualTo(detailFeuilleDeMatchDTO2);
        detailFeuilleDeMatchDTO2.setId(2L);
        assertThat(detailFeuilleDeMatchDTO1).isNotEqualTo(detailFeuilleDeMatchDTO2);
        detailFeuilleDeMatchDTO1.setId(null);
        assertThat(detailFeuilleDeMatchDTO1).isNotEqualTo(detailFeuilleDeMatchDTO2);
    }
}
