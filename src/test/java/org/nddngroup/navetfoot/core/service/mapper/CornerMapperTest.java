package org.nddngroup.navetfoot.core.service.mapper;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class CornerMapperTest {

    private CornerMapper cornerMapper;

    @BeforeEach
    public void setUp() {
        cornerMapper = new CornerMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        Assertions.assertThat(cornerMapper.fromId(id).getId()).isEqualTo(id);
        Assertions.assertThat(cornerMapper.fromId(null)).isNull();
    }
}
