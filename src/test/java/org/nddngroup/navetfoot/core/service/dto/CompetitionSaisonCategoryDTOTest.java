package org.nddngroup.navetfoot.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.nddngroup.navetfoot.core.web.rest.TestUtil;

public class CompetitionSaisonCategoryDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CompetitionSaisonCategoryDTO.class);
        CompetitionSaisonCategoryDTO competitionSaisonCategoryDTO1 = new CompetitionSaisonCategoryDTO();
        competitionSaisonCategoryDTO1.setId(1L);
        CompetitionSaisonCategoryDTO competitionSaisonCategoryDTO2 = new CompetitionSaisonCategoryDTO();
        assertThat(competitionSaisonCategoryDTO1).isNotEqualTo(competitionSaisonCategoryDTO2);
        competitionSaisonCategoryDTO2.setId(competitionSaisonCategoryDTO1.getId());
        assertThat(competitionSaisonCategoryDTO1).isEqualTo(competitionSaisonCategoryDTO2);
        competitionSaisonCategoryDTO2.setId(2L);
        assertThat(competitionSaisonCategoryDTO1).isNotEqualTo(competitionSaisonCategoryDTO2);
        competitionSaisonCategoryDTO1.setId(null);
        assertThat(competitionSaisonCategoryDTO1).isNotEqualTo(competitionSaisonCategoryDTO2);
    }
}
