package org.nddngroup.navetfoot.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.nddngroup.navetfoot.core.web.rest.TestUtil;

public class PouleDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PouleDTO.class);
        PouleDTO pouleDTO1 = new PouleDTO();
        pouleDTO1.setId(1L);
        PouleDTO pouleDTO2 = new PouleDTO();
        assertThat(pouleDTO1).isNotEqualTo(pouleDTO2);
        pouleDTO2.setId(pouleDTO1.getId());
        assertThat(pouleDTO1).isEqualTo(pouleDTO2);
        pouleDTO2.setId(2L);
        assertThat(pouleDTO1).isNotEqualTo(pouleDTO2);
        pouleDTO1.setId(null);
        assertThat(pouleDTO1).isNotEqualTo(pouleDTO2);
    }
}
