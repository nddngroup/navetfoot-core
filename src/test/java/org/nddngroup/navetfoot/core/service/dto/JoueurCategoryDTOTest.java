package org.nddngroup.navetfoot.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.nddngroup.navetfoot.core.web.rest.TestUtil;

public class JoueurCategoryDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(JoueurCategoryDTO.class);
        JoueurCategoryDTO joueurCategoryDTO1 = new JoueurCategoryDTO();
        joueurCategoryDTO1.setId(1L);
        JoueurCategoryDTO joueurCategoryDTO2 = new JoueurCategoryDTO();
        assertThat(joueurCategoryDTO1).isNotEqualTo(joueurCategoryDTO2);
        joueurCategoryDTO2.setId(joueurCategoryDTO1.getId());
        assertThat(joueurCategoryDTO1).isEqualTo(joueurCategoryDTO2);
        joueurCategoryDTO2.setId(2L);
        assertThat(joueurCategoryDTO1).isNotEqualTo(joueurCategoryDTO2);
        joueurCategoryDTO1.setId(null);
        assertThat(joueurCategoryDTO1).isNotEqualTo(joueurCategoryDTO2);
    }
}
