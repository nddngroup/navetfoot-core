package org.nddngroup.navetfoot.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.nddngroup.navetfoot.core.web.rest.TestUtil;

public class CornerDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CornerDTO.class);
        CornerDTO cornerDTO1 = new CornerDTO();
        cornerDTO1.setId(1L);
        CornerDTO cornerDTO2 = new CornerDTO();
        assertThat(cornerDTO1).isNotEqualTo(cornerDTO2);
        cornerDTO2.setId(cornerDTO1.getId());
        assertThat(cornerDTO1).isEqualTo(cornerDTO2);
        cornerDTO2.setId(2L);
        assertThat(cornerDTO1).isNotEqualTo(cornerDTO2);
        cornerDTO1.setId(null);
        assertThat(cornerDTO1).isNotEqualTo(cornerDTO2);
    }
}
