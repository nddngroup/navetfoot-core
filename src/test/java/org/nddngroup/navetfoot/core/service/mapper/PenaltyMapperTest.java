package org.nddngroup.navetfoot.core.service.mapper;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class PenaltyMapperTest {

    private PenaltyMapper penaltyMapper;

    @BeforeEach
    public void setUp() {
        penaltyMapper = new PenaltyMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        Assertions.assertThat(penaltyMapper.fromId(id).getId()).isEqualTo(id);
        Assertions.assertThat(penaltyMapper.fromId(null)).isNull();
    }
}
