package org.nddngroup.navetfoot.core.service.mapper;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ParametrageMapperTest {

    private ParametrageMapper parametrageMapper;

    @BeforeEach
    public void setUp() {
        parametrageMapper = new ParametrageMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        Assertions.assertThat(parametrageMapper.fromId(id).getId()).isEqualTo(id);
        Assertions.assertThat(parametrageMapper.fromId(null)).isNull();
    }
}
