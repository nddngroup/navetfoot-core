package org.nddngroup.navetfoot.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.nddngroup.navetfoot.core.web.rest.TestUtil;

public class CoupFrancDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CoupFrancDTO.class);
        CoupFrancDTO coupFrancDTO1 = new CoupFrancDTO();
        coupFrancDTO1.setId(1L);
        CoupFrancDTO coupFrancDTO2 = new CoupFrancDTO();
        assertThat(coupFrancDTO1).isNotEqualTo(coupFrancDTO2);
        coupFrancDTO2.setId(coupFrancDTO1.getId());
        assertThat(coupFrancDTO1).isEqualTo(coupFrancDTO2);
        coupFrancDTO2.setId(2L);
        assertThat(coupFrancDTO1).isNotEqualTo(coupFrancDTO2);
        coupFrancDTO1.setId(null);
        assertThat(coupFrancDTO1).isNotEqualTo(coupFrancDTO2);
    }
}
