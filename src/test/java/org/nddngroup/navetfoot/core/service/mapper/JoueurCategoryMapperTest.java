package org.nddngroup.navetfoot.core.service.mapper;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class JoueurCategoryMapperTest {

    private JoueurCategoryMapper joueurCategoryMapper;

    @BeforeEach
    public void setUp() {
        joueurCategoryMapper = new JoueurCategoryMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        Assertions.assertThat(joueurCategoryMapper.fromId(id).getId()).isEqualTo(id);
        Assertions.assertThat(joueurCategoryMapper.fromId(null)).isNull();
    }
}
