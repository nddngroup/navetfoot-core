package org.nddngroup.navetfoot.core.service.mapper;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class CompetitionMapperTest {

    private CompetitionMapper competitionMapper;

    @BeforeEach
    public void setUp() {
        competitionMapper = new CompetitionMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        Assertions.assertThat(competitionMapper.fromId(id).getId()).isEqualTo(id);
        Assertions.assertThat(competitionMapper.fromId(null)).isNull();
    }
}
