package org.nddngroup.navetfoot.core.service.mapper;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class DelegueMatchMapperTest {

    private DelegueMatchMapper delegueMatchMapper;

    @BeforeEach
    public void setUp() {
        delegueMatchMapper = new DelegueMatchMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        Assertions.assertThat(delegueMatchMapper.fromId(id).getId()).isEqualTo(id);
        Assertions.assertThat(delegueMatchMapper.fromId(null)).isNull();
    }
}
