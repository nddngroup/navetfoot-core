package org.nddngroup.navetfoot.core.service.mapper;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class AffiliationMapperTest {

    private AffiliationMapper affiliationMapper;

    @BeforeEach
    public void setUp() {
        affiliationMapper = new AffiliationMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        Assertions.assertThat(affiliationMapper.fromId(id).getId()).isEqualTo(id);
        Assertions.assertThat(affiliationMapper.fromId(null)).isNull();
    }
}
