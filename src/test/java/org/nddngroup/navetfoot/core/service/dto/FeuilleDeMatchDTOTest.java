package org.nddngroup.navetfoot.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.nddngroup.navetfoot.core.web.rest.TestUtil;

public class FeuilleDeMatchDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(FeuilleDeMatchDTO.class);
        FeuilleDeMatchDTO feuilleDeMatchDTO1 = new FeuilleDeMatchDTO();
        feuilleDeMatchDTO1.setId(1L);
        FeuilleDeMatchDTO feuilleDeMatchDTO2 = new FeuilleDeMatchDTO();
        assertThat(feuilleDeMatchDTO1).isNotEqualTo(feuilleDeMatchDTO2);
        feuilleDeMatchDTO2.setId(feuilleDeMatchDTO1.getId());
        assertThat(feuilleDeMatchDTO1).isEqualTo(feuilleDeMatchDTO2);
        feuilleDeMatchDTO2.setId(2L);
        assertThat(feuilleDeMatchDTO1).isNotEqualTo(feuilleDeMatchDTO2);
        feuilleDeMatchDTO1.setId(null);
        assertThat(feuilleDeMatchDTO1).isNotEqualTo(feuilleDeMatchDTO2);
    }
}
