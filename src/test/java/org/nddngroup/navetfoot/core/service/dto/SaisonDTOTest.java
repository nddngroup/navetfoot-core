package org.nddngroup.navetfoot.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.nddngroup.navetfoot.core.web.rest.TestUtil;

public class SaisonDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SaisonDTO.class);
        SaisonDTO saisonDTO1 = new SaisonDTO();
        saisonDTO1.setId(1L);
        SaisonDTO saisonDTO2 = new SaisonDTO();
        assertThat(saisonDTO1).isNotEqualTo(saisonDTO2);
        saisonDTO2.setId(saisonDTO1.getId());
        assertThat(saisonDTO1).isEqualTo(saisonDTO2);
        saisonDTO2.setId(2L);
        assertThat(saisonDTO1).isNotEqualTo(saisonDTO2);
        saisonDTO1.setId(null);
        assertThat(saisonDTO1).isNotEqualTo(saisonDTO2);
    }
}
