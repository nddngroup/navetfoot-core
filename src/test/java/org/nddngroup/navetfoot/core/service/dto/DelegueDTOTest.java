package org.nddngroup.navetfoot.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.nddngroup.navetfoot.core.web.rest.TestUtil;

public class DelegueDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DelegueDTO.class);
        DelegueDTO delegueDTO1 = new DelegueDTO();
        delegueDTO1.setId(1L);
        DelegueDTO delegueDTO2 = new DelegueDTO();
        assertThat(delegueDTO1).isNotEqualTo(delegueDTO2);
        delegueDTO2.setId(delegueDTO1.getId());
        assertThat(delegueDTO1).isEqualTo(delegueDTO2);
        delegueDTO2.setId(2L);
        assertThat(delegueDTO1).isNotEqualTo(delegueDTO2);
        delegueDTO1.setId(null);
        assertThat(delegueDTO1).isNotEqualTo(delegueDTO2);
    }
}
