package org.nddngroup.navetfoot.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.nddngroup.navetfoot.core.web.rest.TestUtil;

public class ButDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ButDTO.class);
        ButDTO butDTO1 = new ButDTO();
        butDTO1.setId(1L);
        ButDTO butDTO2 = new ButDTO();
        assertThat(butDTO1).isNotEqualTo(butDTO2);
        butDTO2.setId(butDTO1.getId());
        assertThat(butDTO1).isEqualTo(butDTO2);
        butDTO2.setId(2L);
        assertThat(butDTO1).isNotEqualTo(butDTO2);
        butDTO1.setId(null);
        assertThat(butDTO1).isNotEqualTo(butDTO2);
    }
}
