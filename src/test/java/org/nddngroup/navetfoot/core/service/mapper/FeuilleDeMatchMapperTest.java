package org.nddngroup.navetfoot.core.service.mapper;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class FeuilleDeMatchMapperTest {

    private FeuilleDeMatchMapper feuilleDeMatchMapper;

    @BeforeEach
    public void setUp() {
        feuilleDeMatchMapper = new FeuilleDeMatchMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        Assertions.assertThat(feuilleDeMatchMapper.fromId(id).getId()).isEqualTo(id);
        Assertions.assertThat(feuilleDeMatchMapper.fromId(null)).isNull();
    }
}
