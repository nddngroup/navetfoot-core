package org.nddngroup.navetfoot.core.service.mapper;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class CoupFrancMapperTest {

    private CoupFrancMapper coupFrancMapper;

    @BeforeEach
    public void setUp() {
        coupFrancMapper = new CoupFrancMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        Assertions.assertThat(coupFrancMapper.fromId(id).getId()).isEqualTo(id);
        Assertions.assertThat(coupFrancMapper.fromId(null)).isNull();
    }
}
