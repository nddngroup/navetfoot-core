package org.nddngroup.navetfoot.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.nddngroup.navetfoot.core.web.rest.TestUtil;

public class DelegueMatchDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DelegueMatchDTO.class);
        DelegueMatchDTO delegueMatchDTO1 = new DelegueMatchDTO();
        delegueMatchDTO1.setId(1L);
        DelegueMatchDTO delegueMatchDTO2 = new DelegueMatchDTO();
        assertThat(delegueMatchDTO1).isNotEqualTo(delegueMatchDTO2);
        delegueMatchDTO2.setId(delegueMatchDTO1.getId());
        assertThat(delegueMatchDTO1).isEqualTo(delegueMatchDTO2);
        delegueMatchDTO2.setId(2L);
        assertThat(delegueMatchDTO1).isNotEqualTo(delegueMatchDTO2);
        delegueMatchDTO1.setId(null);
        assertThat(delegueMatchDTO1).isNotEqualTo(delegueMatchDTO2);
    }
}
