package org.nddngroup.navetfoot.core.service.mapper;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class CompetitionSaisonCategoryMapperTest {

    private CompetitionSaisonCategoryMapper competitionSaisonCategoryMapper;

    @BeforeEach
    public void setUp() {
        competitionSaisonCategoryMapper = new CompetitionSaisonCategoryMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        Assertions.assertThat(competitionSaisonCategoryMapper.fromId(id).getId()).isEqualTo(id);
        Assertions.assertThat(competitionSaisonCategoryMapper.fromId(null)).isNull();
    }
}
