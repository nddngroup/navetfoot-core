package org.nddngroup.navetfoot.core.service.mapper;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class EtapeCompetitionMapperTest {

    private EtapeCompetitionMapper etapeCompetitionMapper;

    @BeforeEach
    public void setUp() {
        etapeCompetitionMapper = new EtapeCompetitionMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        Assertions.assertThat(etapeCompetitionMapper.fromId(id).getId()).isEqualTo(id);
        Assertions.assertThat(etapeCompetitionMapper.fromId(null)).isNull();
    }
}
