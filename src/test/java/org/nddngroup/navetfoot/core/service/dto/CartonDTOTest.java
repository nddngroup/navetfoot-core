package org.nddngroup.navetfoot.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.nddngroup.navetfoot.core.web.rest.TestUtil;

public class CartonDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CartonDTO.class);
        CartonDTO cartonDTO1 = new CartonDTO();
        cartonDTO1.setId(1L);
        CartonDTO cartonDTO2 = new CartonDTO();
        assertThat(cartonDTO1).isNotEqualTo(cartonDTO2);
        cartonDTO2.setId(cartonDTO1.getId());
        assertThat(cartonDTO1).isEqualTo(cartonDTO2);
        cartonDTO2.setId(2L);
        assertThat(cartonDTO1).isNotEqualTo(cartonDTO2);
        cartonDTO1.setId(null);
        assertThat(cartonDTO1).isNotEqualTo(cartonDTO2);
    }
}
