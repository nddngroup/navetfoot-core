package org.nddngroup.navetfoot.core.service.mapper;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ParticipationMapperTest {

    private ParticipationMapper participationMapper;

    @BeforeEach
    public void setUp() {
        participationMapper = new ParticipationMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        Assertions.assertThat(participationMapper.fromId(id).getId()).isEqualTo(id);
        Assertions.assertThat(participationMapper.fromId(null)).isNull();
    }
}
