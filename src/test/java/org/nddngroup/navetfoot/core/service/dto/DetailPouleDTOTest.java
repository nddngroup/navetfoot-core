package org.nddngroup.navetfoot.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.nddngroup.navetfoot.core.web.rest.TestUtil;

public class DetailPouleDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DetailPouleDTO.class);
        DetailPouleDTO detailPouleDTO1 = new DetailPouleDTO();
        detailPouleDTO1.setId(1L);
        DetailPouleDTO detailPouleDTO2 = new DetailPouleDTO();
        assertThat(detailPouleDTO1).isNotEqualTo(detailPouleDTO2);
        detailPouleDTO2.setId(detailPouleDTO1.getId());
        assertThat(detailPouleDTO1).isEqualTo(detailPouleDTO2);
        detailPouleDTO2.setId(2L);
        assertThat(detailPouleDTO1).isNotEqualTo(detailPouleDTO2);
        detailPouleDTO1.setId(null);
        assertThat(detailPouleDTO1).isNotEqualTo(detailPouleDTO2);
    }
}
