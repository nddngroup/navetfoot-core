package org.nddngroup.navetfoot.core.service.mapper;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class DetailFeuilleDeMatchMapperTest {

    private DetailFeuilleDeMatchMapper detailFeuilleDeMatchMapper;

    @BeforeEach
    public void setUp() {
        detailFeuilleDeMatchMapper = new DetailFeuilleDeMatchMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        Assertions.assertThat(detailFeuilleDeMatchMapper.fromId(id).getId()).isEqualTo(id);
        Assertions.assertThat(detailFeuilleDeMatchMapper.fromId(null)).isNull();
    }
}
