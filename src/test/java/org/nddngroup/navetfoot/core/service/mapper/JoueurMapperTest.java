package org.nddngroup.navetfoot.core.service.mapper;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class JoueurMapperTest {

    private JoueurMapper joueurMapper;

    @BeforeEach
    public void setUp() {
        joueurMapper = new JoueurMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        Assertions.assertThat(joueurMapper.fromId(id).getId()).isEqualTo(id);
        Assertions.assertThat(joueurMapper.fromId(null)).isNull();
    }
}
