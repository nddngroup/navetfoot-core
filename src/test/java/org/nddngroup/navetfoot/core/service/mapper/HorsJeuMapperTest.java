package org.nddngroup.navetfoot.core.service.mapper;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class HorsJeuMapperTest {

    private HorsJeuMapper horsJeuMapper;

    @BeforeEach
    public void setUp() {
        horsJeuMapper = new HorsJeuMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        Assertions.assertThat(horsJeuMapper.fromId(id).getId()).isEqualTo(id);
        Assertions.assertThat(horsJeuMapper.fromId(null)).isNull();
    }
}
