package org.nddngroup.navetfoot.core.service.mapper;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class DelegueMapperTest {

    private DelegueMapper delegueMapper;

    @BeforeEach
    public void setUp() {
        delegueMapper = new DelegueMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        Assertions.assertThat(delegueMapper.fromId(id).getId()).isEqualTo(id);
        Assertions.assertThat(delegueMapper.fromId(null)).isNull();
    }
}
