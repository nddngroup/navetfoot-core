package org.nddngroup.navetfoot.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.nddngroup.navetfoot.core.web.rest.TestUtil;

public class RemplacementDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RemplacementDTO.class);
        RemplacementDTO remplacementDTO1 = new RemplacementDTO();
        remplacementDTO1.setId(1L);
        RemplacementDTO remplacementDTO2 = new RemplacementDTO();
        assertThat(remplacementDTO1).isNotEqualTo(remplacementDTO2);
        remplacementDTO2.setId(remplacementDTO1.getId());
        assertThat(remplacementDTO1).isEqualTo(remplacementDTO2);
        remplacementDTO2.setId(2L);
        assertThat(remplacementDTO1).isNotEqualTo(remplacementDTO2);
        remplacementDTO1.setId(null);
        assertThat(remplacementDTO1).isNotEqualTo(remplacementDTO2);
    }
}
