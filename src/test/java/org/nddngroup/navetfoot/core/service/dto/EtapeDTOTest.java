package org.nddngroup.navetfoot.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.nddngroup.navetfoot.core.web.rest.TestUtil;

public class EtapeDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(EtapeDTO.class);
        EtapeDTO etapeDTO1 = new EtapeDTO();
        etapeDTO1.setId(1L);
        EtapeDTO etapeDTO2 = new EtapeDTO();
        assertThat(etapeDTO1).isNotEqualTo(etapeDTO2);
        etapeDTO2.setId(etapeDTO1.getId());
        assertThat(etapeDTO1).isEqualTo(etapeDTO2);
        etapeDTO2.setId(2L);
        assertThat(etapeDTO1).isNotEqualTo(etapeDTO2);
        etapeDTO1.setId(null);
        assertThat(etapeDTO1).isNotEqualTo(etapeDTO2);
    }
}
