package org.nddngroup.navetfoot.core.service.mapper;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ButMapperTest {

    private ButMapper butMapper;

    @BeforeEach
    public void setUp() {
        butMapper = new ButMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        Assertions.assertThat(butMapper.fromId(id).getId()).isEqualTo(id);
        Assertions.assertThat(butMapper.fromId(null)).isNull();
    }
}
