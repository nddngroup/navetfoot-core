package org.nddngroup.navetfoot.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.nddngroup.navetfoot.core.web.rest.TestUtil;

public class AffiliationDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AffiliationDTO.class);
        AffiliationDTO affiliationDTO1 = new AffiliationDTO();
        affiliationDTO1.setId(1L);
        AffiliationDTO affiliationDTO2 = new AffiliationDTO();
        assertThat(affiliationDTO1).isNotEqualTo(affiliationDTO2);
        affiliationDTO2.setId(affiliationDTO1.getId());
        assertThat(affiliationDTO1).isEqualTo(affiliationDTO2);
        affiliationDTO2.setId(2L);
        assertThat(affiliationDTO1).isNotEqualTo(affiliationDTO2);
        affiliationDTO1.setId(null);
        assertThat(affiliationDTO1).isNotEqualTo(affiliationDTO2);
    }
}
