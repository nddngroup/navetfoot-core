package org.nddngroup.navetfoot.core.service.mapper;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class PouleMapperTest {

    private PouleMapper pouleMapper;

    @BeforeEach
    public void setUp() {
        pouleMapper = new PouleMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        Assertions.assertThat(pouleMapper.fromId(id).getId()).isEqualTo(id);
        Assertions.assertThat(pouleMapper.fromId(null)).isNull();
    }
}
