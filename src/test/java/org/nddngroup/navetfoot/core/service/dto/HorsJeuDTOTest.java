package org.nddngroup.navetfoot.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.nddngroup.navetfoot.core.web.rest.TestUtil;

public class HorsJeuDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(HorsJeuDTO.class);
        HorsJeuDTO horsJeuDTO1 = new HorsJeuDTO();
        horsJeuDTO1.setId(1L);
        HorsJeuDTO horsJeuDTO2 = new HorsJeuDTO();
        assertThat(horsJeuDTO1).isNotEqualTo(horsJeuDTO2);
        horsJeuDTO2.setId(horsJeuDTO1.getId());
        assertThat(horsJeuDTO1).isEqualTo(horsJeuDTO2);
        horsJeuDTO2.setId(2L);
        assertThat(horsJeuDTO1).isNotEqualTo(horsJeuDTO2);
        horsJeuDTO1.setId(null);
        assertThat(horsJeuDTO1).isNotEqualTo(horsJeuDTO2);
    }
}
