package org.nddngroup.navetfoot.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.nddngroup.navetfoot.core.web.rest.TestUtil;

public class ParametrageDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ParametrageDTO.class);
        ParametrageDTO parametrageDTO1 = new ParametrageDTO();
        parametrageDTO1.setId(1L);
        ParametrageDTO parametrageDTO2 = new ParametrageDTO();
        assertThat(parametrageDTO1).isNotEqualTo(parametrageDTO2);
        parametrageDTO2.setId(parametrageDTO1.getId());
        assertThat(parametrageDTO1).isEqualTo(parametrageDTO2);
        parametrageDTO2.setId(2L);
        assertThat(parametrageDTO1).isNotEqualTo(parametrageDTO2);
        parametrageDTO1.setId(null);
        assertThat(parametrageDTO1).isNotEqualTo(parametrageDTO2);
    }
}
