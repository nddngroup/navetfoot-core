package org.nddngroup.navetfoot.core.service.mapper;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class CompetitionSaisonMapperTest {

    private CompetitionSaisonMapper competitionSaisonMapper;

    @BeforeEach
    public void setUp() {
        competitionSaisonMapper = new CompetitionSaisonMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        Assertions.assertThat(competitionSaisonMapper.fromId(id).getId()).isEqualTo(id);
        Assertions.assertThat(competitionSaisonMapper.fromId(null)).isNull();
    }
}
