package org.nddngroup.navetfoot.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.nddngroup.navetfoot.core.web.rest.TestUtil;

public class PenaltyDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PenaltyDTO.class);
        PenaltyDTO penaltyDTO1 = new PenaltyDTO();
        penaltyDTO1.setId(1L);
        PenaltyDTO penaltyDTO2 = new PenaltyDTO();
        assertThat(penaltyDTO1).isNotEqualTo(penaltyDTO2);
        penaltyDTO2.setId(penaltyDTO1.getId());
        assertThat(penaltyDTO1).isEqualTo(penaltyDTO2);
        penaltyDTO2.setId(2L);
        assertThat(penaltyDTO1).isNotEqualTo(penaltyDTO2);
        penaltyDTO1.setId(null);
        assertThat(penaltyDTO1).isNotEqualTo(penaltyDTO2);
    }
}
