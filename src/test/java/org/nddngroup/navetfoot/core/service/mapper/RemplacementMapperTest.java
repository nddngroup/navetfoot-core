package org.nddngroup.navetfoot.core.service.mapper;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class RemplacementMapperTest {

    private RemplacementMapper remplacementMapper;

    @BeforeEach
    public void setUp() {
        remplacementMapper = new RemplacementMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        Assertions.assertThat(remplacementMapper.fromId(id).getId()).isEqualTo(id);
        Assertions.assertThat(remplacementMapper.fromId(null)).isNull();
    }
}
