package org.nddngroup.navetfoot.core.service.mapper;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class EtapeMapperTest {

    private EtapeMapper etapeMapper;

    @BeforeEach
    public void setUp() {
        etapeMapper = new EtapeMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        Assertions.assertThat(etapeMapper.fromId(id).getId()).isEqualTo(id);
        Assertions.assertThat(etapeMapper.fromId(null)).isNull();
    }
}
