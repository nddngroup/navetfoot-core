package org.nddngroup.navetfoot.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.nddngroup.navetfoot.core.web.rest.TestUtil;

public class EtapeCompetitionDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(EtapeCompetitionDTO.class);
        EtapeCompetitionDTO etapeCompetitionDTO1 = new EtapeCompetitionDTO();
        etapeCompetitionDTO1.setId(1L);
        EtapeCompetitionDTO etapeCompetitionDTO2 = new EtapeCompetitionDTO();
        assertThat(etapeCompetitionDTO1).isNotEqualTo(etapeCompetitionDTO2);
        etapeCompetitionDTO2.setId(etapeCompetitionDTO1.getId());
        assertThat(etapeCompetitionDTO1).isEqualTo(etapeCompetitionDTO2);
        etapeCompetitionDTO2.setId(2L);
        assertThat(etapeCompetitionDTO1).isNotEqualTo(etapeCompetitionDTO2);
        etapeCompetitionDTO1.setId(null);
        assertThat(etapeCompetitionDTO1).isNotEqualTo(etapeCompetitionDTO2);
    }
}
