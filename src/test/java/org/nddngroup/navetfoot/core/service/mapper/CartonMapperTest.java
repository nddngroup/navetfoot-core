package org.nddngroup.navetfoot.core.service.mapper;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class CartonMapperTest {

    private CartonMapper cartonMapper;

    @BeforeEach
    public void setUp() {
        cartonMapper = new CartonMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        Assertions.assertThat(cartonMapper.fromId(id).getId()).isEqualTo(id);
        Assertions.assertThat(cartonMapper.fromId(null)).isNull();
    }
}
