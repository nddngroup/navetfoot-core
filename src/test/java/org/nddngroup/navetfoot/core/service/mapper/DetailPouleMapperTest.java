package org.nddngroup.navetfoot.core.service.mapper;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class DetailPouleMapperTest {

    private DetailPouleMapper detailPouleMapper;

    @BeforeEach
    public void setUp() {
        detailPouleMapper = new DetailPouleMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        Assertions.assertThat(detailPouleMapper.fromId(id).getId()).isEqualTo(id);
        Assertions.assertThat(detailPouleMapper.fromId(null)).isNull();
    }
}
