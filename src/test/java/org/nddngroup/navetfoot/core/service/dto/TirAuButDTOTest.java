package org.nddngroup.navetfoot.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.nddngroup.navetfoot.core.web.rest.TestUtil;

public class TirAuButDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TirAuButDTO.class);
        TirAuButDTO tirAuButDTO1 = new TirAuButDTO();
        tirAuButDTO1.setId(1L);
        TirAuButDTO tirAuButDTO2 = new TirAuButDTO();
        assertThat(tirAuButDTO1).isNotEqualTo(tirAuButDTO2);
        tirAuButDTO2.setId(tirAuButDTO1.getId());
        assertThat(tirAuButDTO1).isEqualTo(tirAuButDTO2);
        tirAuButDTO2.setId(2L);
        assertThat(tirAuButDTO1).isNotEqualTo(tirAuButDTO2);
        tirAuButDTO1.setId(null);
        assertThat(tirAuButDTO1).isNotEqualTo(tirAuButDTO2);
    }
}
