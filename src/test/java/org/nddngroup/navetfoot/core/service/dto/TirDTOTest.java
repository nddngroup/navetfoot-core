package org.nddngroup.navetfoot.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.nddngroup.navetfoot.core.web.rest.TestUtil;

public class TirDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TirDTO.class);
        TirDTO tirDTO1 = new TirDTO();
        tirDTO1.setId(1L);
        TirDTO tirDTO2 = new TirDTO();
        assertThat(tirDTO1).isNotEqualTo(tirDTO2);
        tirDTO2.setId(tirDTO1.getId());
        assertThat(tirDTO1).isEqualTo(tirDTO2);
        tirDTO2.setId(2L);
        assertThat(tirDTO1).isNotEqualTo(tirDTO2);
        tirDTO1.setId(null);
        assertThat(tirDTO1).isNotEqualTo(tirDTO2);
    }
}
