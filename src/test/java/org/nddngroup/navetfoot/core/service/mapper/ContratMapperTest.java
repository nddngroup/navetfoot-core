package org.nddngroup.navetfoot.core.service.mapper;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ContratMapperTest {

    private ContratMapper contratMapper;

    @BeforeEach
    public void setUp() {
        contratMapper = new ContratMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        Assertions.assertThat(contratMapper.fromId(id).getId()).isEqualTo(id);
        Assertions.assertThat(contratMapper.fromId(null)).isNull();
    }
}
