package org.nddngroup.navetfoot.core.service.mapper;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class TirAuButMapperTest {

    private TirAuButMapper tirAuButMapper;

    @BeforeEach
    public void setUp() {
        tirAuButMapper = new TirAuButMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        Assertions.assertThat(tirAuButMapper.fromId(id).getId()).isEqualTo(id);
        Assertions.assertThat(tirAuButMapper.fromId(null)).isNull();
    }
}
