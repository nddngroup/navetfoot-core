package org.nddngroup.navetfoot.core.service.mapper;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class TirMapperTest {

    private TirMapper tirMapper;

    @BeforeEach
    public void setUp() {
        tirMapper = new TirMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        Assertions.assertThat(tirMapper.fromId(id).getId()).isEqualTo(id);
        Assertions.assertThat(tirMapper.fromId(null)).isNull();
    }
}
