package org.nddngroup.navetfoot.core.service.mapper;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class SaisonMapperTest {

    private SaisonMapper saisonMapper;

    @BeforeEach
    public void setUp() {
        saisonMapper = new SaisonMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        Assertions.assertThat(saisonMapper.fromId(id).getId()).isEqualTo(id);
        Assertions.assertThat(saisonMapper.fromId(null)).isNull();
    }
}
