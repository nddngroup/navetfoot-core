package org.nddngroup.navetfoot.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.nddngroup.navetfoot.core.web.rest.TestUtil;

public class CompetitionSaisonDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CompetitionSaisonDTO.class);
        CompetitionSaisonDTO competitionSaisonDTO1 = new CompetitionSaisonDTO();
        competitionSaisonDTO1.setId(1L);
        CompetitionSaisonDTO competitionSaisonDTO2 = new CompetitionSaisonDTO();
        assertThat(competitionSaisonDTO1).isNotEqualTo(competitionSaisonDTO2);
        competitionSaisonDTO2.setId(competitionSaisonDTO1.getId());
        assertThat(competitionSaisonDTO1).isEqualTo(competitionSaisonDTO2);
        competitionSaisonDTO2.setId(2L);
        assertThat(competitionSaisonDTO1).isNotEqualTo(competitionSaisonDTO2);
        competitionSaisonDTO1.setId(null);
        assertThat(competitionSaisonDTO1).isNotEqualTo(competitionSaisonDTO2);
    }
}
