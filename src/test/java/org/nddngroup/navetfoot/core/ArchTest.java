package org.nddngroup.navetfoot.core;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {
        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("org.nddngroup.navetfoot.core");

        noClasses()
            .that()
            .resideInAnyPackage("org.nddngroup.navetfoot.core.service..")
            .or()
            .resideInAnyPackage("org.nddngroup.navetfoot.core.repository..")
            .should()
            .dependOnClassesThat()
            .resideInAnyPackage("..org.nddngroup.navetfoot.core.web..")
            .because("Services and repositories should not depend on web layer")
            .check(importedClasses);
    }
}
