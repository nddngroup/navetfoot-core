package org.nddngroup.navetfoot.core.domain.custom;

import org.nddngroup.navetfoot.core.service.dto.AssociationDTO;
import org.nddngroup.navetfoot.core.service.dto.PouleDTO;

import java.util.List;

public class PouleWithAssociation {
    private PouleDTO poule;
    private List<AssociationDTO> associations;

    public PouleDTO getPoule() {
        return poule;
    }

    public void setPoule(PouleDTO poule) {
        this.poule = poule;
    }

    public List<AssociationDTO> getAssociations() {
        return associations;
    }

    public void setAssociations(List<AssociationDTO> associations) {
        this.associations = associations;
    }
}
