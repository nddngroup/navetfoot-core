package org.nddngroup.navetfoot.core.domain.custom.stats;

import org.nddngroup.navetfoot.core.service.dto.PouleDTO;

import java.io.Serializable;
import java.util.List;

/**
 * Created by souleymane91 on 11/11/2018.
 */
public class PouleDetail implements Serializable {
    private PouleDTO pouleDTO;
    private List<DetailPouleDetail> detailPouleDetails;

    public PouleDTO getPouleDTO() {
        return pouleDTO;
    }

    public void setPouleDTO(PouleDTO pouleDTO) {
        this.pouleDTO = pouleDTO;
    }

    public List<DetailPouleDetail> getDetailPouleDetails() {
        return detailPouleDetails;
    }

    public void setDetailPouleDetails(List<DetailPouleDetail> detailPouleDetails) {
        this.detailPouleDetails = detailPouleDetails;
    }
}
