package org.nddngroup.navetfoot.core.domain.es;

import javax.persistence.Transient;

public class TirStats extends MatchStats {
    @Transient
    private Long joueurId;
    @Transient
    private String joueurNom;
    @Transient
    private String joueurPrenom;
    @Transient
    private String joueurImage;

    @Transient
    private Long matchId;


    public Long getJoueurId() {
        return joueurId;
    }

    public TirStats joueurId(Long joueurId) {
        this.joueurId = joueurId;
        return this;
    }

    public void setJoueurId(Long joueurId) {
        this.joueurId = joueurId;
    }

    public String getJoueurNom() {
        return joueurNom;
    }

    public TirStats joueurNom(String joueurNom) {
        this.joueurNom = joueurNom;
        return this;
    }

    public void setJoueurNom(String joueurNom) {
        this.joueurNom = joueurNom;
    }

    public String getJoueurPrenom() {
        return joueurPrenom;
    }

    public TirStats joueurPrenom(String joueurPrenom) {
        this.joueurPrenom = joueurPrenom;
        return this;
    }

    public void setJoueurPrenom(String joueurPrenom) {
        this.joueurPrenom = joueurPrenom;
    }

    public String getJoueurImage() {
        return joueurImage;
    }

    public TirStats joueurImage(String joueurImage) {
        this.joueurImage = joueurImage;
        return this;
    }

    public void setJoueurImage(String joueurImage) {
        this.joueurImage = joueurImage;
    }

    public Long getMatchId() {
        return matchId;
    }

    public TirStats matchId(Long matchId) {
        this.matchId = matchId;
        return this;
    }

    public void setMatchId(Long matchId) {
        this.matchId = matchId;
    }

}
