package org.nddngroup.navetfoot.core.domain.custom.sync;

import org.nddngroup.navetfoot.core.service.dto.PouleDTO;

/**
 * Created by souleymane91 on 06/10/2018.
 */
public class CustomPoule {
    private String etapeCompetitionHashcode;
    private PouleDTO pouleDTO;

    public String getEtapeCompetitionHashcode() {
        return etapeCompetitionHashcode;
    }

    public void setEtapeCompetitionHashcode(String etapeCompetitionHashcode) {
        this.etapeCompetitionHashcode = etapeCompetitionHashcode;
    }

    public PouleDTO getPouleDTO() {
        return pouleDTO;
    }

    public void setPouleDTO(PouleDTO pouleDTO) {
        this.pouleDTO = pouleDTO;
    }
}
