package org.nddngroup.navetfoot.core.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * A Association.
 */
@Entity
@Table(name = "association")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "association")
public class Association implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "nom", nullable = false)
    private String nom;

    @Column(name = "adresse")
    private String adresse;

    @Column(name = "couleurs")
    private String couleurs;

    @Column(name = "hashcode")
    private String hashcode;

    @Column(name = "created_at")
    private ZonedDateTime createdAt;

    @Column(name = "updated_at")
    private ZonedDateTime updatedAt;

    @Column(name = "deleted")
    private Boolean deleted;

    @Column(name = "code")
    private String code;

    @Column(name = "user_id")
    private Long userId;

    @OneToMany(mappedBy = "association")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Affiliation> affiliations = new HashSet<>();

    @ManyToMany(mappedBy = "associations")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnore
    private Set<Organisation> organisations = new HashSet<>();

    @PostPersist
    public void setReferencePostPersist() {
        if(id != null) {
            code = code != null ? code + "_" + id : "REF_" + id;
        }
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public Association nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAdresse() {
        return adresse;
    }

    public Association adresse(String adresse) {
        this.adresse = adresse;
        return this;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getCouleurs() {
        return couleurs;
    }

    public Association couleurs(String couleurs) {
        this.couleurs = couleurs;
        return this;
    }

    public void setCouleurs(String couleurs) {
        this.couleurs = couleurs;
    }

    public String getHashcode() {
        return hashcode;
    }

    public Association hashcode(String hashcode) {
        this.hashcode = hashcode;
        return this;
    }

    public void setHashcode(String hashcode) {
        this.hashcode = hashcode;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public Association createdAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public Association updatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public Association deleted(Boolean deleted) {
        this.deleted = deleted;
        return this;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public String getCode() {
        return code;
    }

    public Association code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getUserId() {
        return userId;
    }

    public Association userId(Long userId) {
        this.userId = userId;
        return this;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Set<Affiliation> getAffiliations() {
        return affiliations;
    }

    public Association affiliations(Set<Affiliation> affiliations) {
        this.affiliations = affiliations;
        return this;
    }

    public Association addAffiliation(Affiliation affiliation) {
        this.affiliations.add(affiliation);
        affiliation.setAssociation(this);
        return this;
    }

    public Association removeAffiliation(Affiliation affiliation) {
        this.affiliations.remove(affiliation);
        affiliation.setAssociation(null);
        return this;
    }

    public void setAffiliations(Set<Affiliation> affiliations) {
        this.affiliations = affiliations;
    }

    public Set<Organisation> getOrganisations() {
        return organisations;
    }

    public Association organisations(Set<Organisation> organisations) {
        this.organisations = organisations;
        return this;
    }

    public Association addOrganisations(Organisation organisation) {
        this.organisations.add(organisation);
        organisation.getAssociations().add(this);
        return this;
    }

    public Association removeOrganisations(Organisation organisation) {
        this.organisations.remove(organisation);
        organisation.getAssociations().remove(this);
        return this;
    }

    public void setOrganisations(Set<Organisation> organisations) {
        this.organisations = organisations;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Association)) {
            return false;
        }
        return id != null && id.equals(((Association) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Association{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", adresse='" + getAdresse() + "'" +
            ", couleurs='" + getCouleurs() + "'" +
            ", hashcode='" + getHashcode() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", deleted='" + isDeleted() + "'" +
            ", code='" + getCode() + "'" +
            ", userId=" + getUserId() +
            "}";
    }
}
