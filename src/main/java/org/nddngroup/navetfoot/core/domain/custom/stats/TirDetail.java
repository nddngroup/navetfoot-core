package org.nddngroup.navetfoot.core.domain.custom.stats;

import org.nddngroup.navetfoot.core.service.dto.TirDTO;

import java.io.Serializable;

/**
 * Created by souleymane91 on 10/11/2018.
 */
public class TirDetail implements Serializable {
    private TirDTO tirDTO;
    private String prenomJoueur;
    private String nomJoueur;
    private String imageUrlJoueur;
    private String joueurHashcode;

    public TirDTO getTirDTO() {
        return tirDTO;
    }

    public void setTirDTO(TirDTO tirDTO) {
        this.tirDTO = tirDTO;
    }

    public String getPrenomJoueur() {
        return prenomJoueur;
    }

    public void setPrenomJoueur(String prenomJoueur) {
        this.prenomJoueur = prenomJoueur;
    }

    public String getNomJoueur() {
        return nomJoueur;
    }

    public void setNomJoueur(String nomJoueur) {
        this.nomJoueur = nomJoueur;
    }

    public String getImageUrlJoueur() {
        return imageUrlJoueur;
    }

    public void setImageUrlJoueur(String imageUrlJoueur) {
        this.imageUrlJoueur = imageUrlJoueur;
    }

    public String getJoueurHashcode() {
        return joueurHashcode;
    }

    public void setJoueurHashcode(String joueurHashcode) {
        this.joueurHashcode = joueurHashcode;
    }
}
