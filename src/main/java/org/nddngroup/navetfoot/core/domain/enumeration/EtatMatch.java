package org.nddngroup.navetfoot.core.domain.enumeration;

/**
 * The EtatMatch enumeration.
 */
public enum EtatMatch {
    PROGRAMME, EN_COURS_1, EN_COURS_2, EN_COURS_3, EN_COURS_4, MI_TEMPS_1, MI_TEMPS_2, RESERVE, TERMINE, PROLONGATION, TIR_AU_BUT, ANNULE, FORFAIT
}
