package org.nddngroup.navetfoot.core.domain.custom.sync;

import org.nddngroup.navetfoot.core.service.dto.CompetitionDTO;

/**
 * Created by souleymane91 on 06/10/2018.
 */
public class CustomCompetition {
    private String organisationHashcode;
    private CompetitionDTO competitionDTO;

    public String getOrganisationHashcode() {
        return organisationHashcode;
    }

    public void setOrganisationHashcode(String organisationHashcode) {
        this.organisationHashcode = organisationHashcode;
    }

    public CompetitionDTO getCompetitionDTO() {
        return competitionDTO;
    }

    public void setCompetitionDTO(CompetitionDTO competitionDTO) {
        this.competitionDTO = competitionDTO;
    }
}
