package org.nddngroup.navetfoot.core.domain.custom;

import org.nddngroup.navetfoot.core.service.dto.EtapeDTO;

public class EtapeForCompetitionSaison {
    private EtapeDTO etapeDTO;
    private boolean status;

    public EtapeForCompetitionSaison(EtapeDTO etapeDTO, boolean status) {
        this.etapeDTO = etapeDTO;
        this.status = status;
    }

    public EtapeDTO getEtapeDTO() {
        return etapeDTO;
    }

    public void setEtapeDTO(EtapeDTO etapeDTO) {
        this.etapeDTO = etapeDTO;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
