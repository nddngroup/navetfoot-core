package org.nddngroup.navetfoot.core.domain.enumeration;

/**
 * The Issu enumeration.
 */
public enum Issu {
    MARQUE, RATE
}
