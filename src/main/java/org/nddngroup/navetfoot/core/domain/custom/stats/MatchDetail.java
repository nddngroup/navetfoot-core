package org.nddngroup.navetfoot.core.domain.custom.stats;

import org.nddngroup.navetfoot.core.service.dto.MatchDTO;

import java.io.Serializable;

/**
 * Created by souleymane91 on 03/11/2018.
 */
public class MatchDetail implements Serializable, Comparable<MatchDetail> {
    private MatchDTO matchDTO;
    private String localNom;
    private String visiteurNom;
    private String localImage;
    private String visiteurImage;

    private Integer nbButsLocal;
    private Integer nbButsVisiteur;

    private Boolean hasTirAuBut;
    private Integer nbTirAuButsLocalMarque;
    private Integer nbTirAuButsLocalRate;
    private Integer nbTirAuButsVisiteurMarque;
    private Integer nbTirAuButsVisiteurRate;
    private Long saisonId;
    private String saisonNom;
    private Long competitionId;
    private String competitionNom;
    private Long categoryId;
    private String categoryNom;
    private boolean isExternal;

    public MatchDTO getMatchDTO() {
        return matchDTO;
    }

    public void setMatchDTO(MatchDTO matchDTO) {
        this.matchDTO = matchDTO;
    }

    public String getLocalNom() {
        return localNom;
    }

    public void setLocalNom(String localNom) {
        this.localNom = localNom;
    }

    public String getLocalImage() {
        return localImage;
    }

    public void setLocalImage(String localImage) {
        this.localImage = localImage;
    }

    public String getVisiteurNom() {
        return visiteurNom;
    }

    public void setVisiteurNom(String visiteurNom) {
        this.visiteurNom = visiteurNom;
    }

    public String getVisiteurImage() {
        return visiteurImage;
    }

    public void setVisiteurImage(String visiteurImage) {
        this.visiteurImage = visiteurImage;
    }

    public Integer getNbButsLocal() {
        return nbButsLocal;
    }

    public void setNbButsLocal(Integer nbButsLocal) {
        this.nbButsLocal = nbButsLocal;
    }

    public Integer getNbButsVisiteur() {
        return nbButsVisiteur;
    }

    public void setNbButsVisiteur(Integer nbButsVisiteur) {
        this.nbButsVisiteur = nbButsVisiteur;
    }

    public Boolean getHasTirAuBut() {
        return hasTirAuBut;
    }

    public void setHasTirAuBut(Boolean hasTirAuBut) {
        this.hasTirAuBut = hasTirAuBut;
    }

    public Integer getNbTirAuButsLocalMarque() {
        return nbTirAuButsLocalMarque;
    }

    public void setNbTirAuButsLocalMarque(Integer nbTirAuButsLocalMarque) {
        this.nbTirAuButsLocalMarque = nbTirAuButsLocalMarque;
    }

    public Integer getNbTirAuButsLocalRate() {
        return nbTirAuButsLocalRate;
    }

    public void setNbTirAuButsLocalRate(Integer nbTirAuButsLocalRate) {
        this.nbTirAuButsLocalRate = nbTirAuButsLocalRate;
    }

    public Integer getNbTirAuButsVisiteurMarque() {
        return nbTirAuButsVisiteurMarque;
    }

    public void setNbTirAuButsVisiteurMarque(Integer nbTirAuButsVisiteurMarque) {
        this.nbTirAuButsVisiteurMarque = nbTirAuButsVisiteurMarque;
    }

    public Integer getNbTirAuButsVisiteurRate() {
        return nbTirAuButsVisiteurRate;
    }

    public void setNbTirAuButsVisiteurRate(Integer nbTirAuButsVisiteurRate) {
        this.nbTirAuButsVisiteurRate = nbTirAuButsVisiteurRate;
    }

    public Long getSaisonId() {
        return saisonId;
    }

    public void setSaisonId(Long saisonId) {
        this.saisonId = saisonId;
    }

    public String getSaisonNom() {
        return saisonNom;
    }

    public void setSaisonNom(String saisonNom) {
        this.saisonNom = saisonNom;
    }

    public Long getCompetitionId() {
        return competitionId;
    }

    public void setCompetitionId(Long competitionId) {
        this.competitionId = competitionId;
    }

    public String getCompetitionNom() {
        return competitionNom;
    }

    public void setCompetitionNom(String competitionNom) {
        this.competitionNom = competitionNom;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryNom() {
        return categoryNom;
    }

    public void setCategoryNom(String categoryNom) {
        this.categoryNom = categoryNom;
    }

    public boolean isExternal() {
        return isExternal;
    }

    public void setExternal(boolean external) {
        isExternal = external;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public int compareTo(MatchDetail o) {
        return getMatchDTO().getDateMatch().compareTo(o.getMatchDTO().getDateMatch());
    }

    @Override
    public String toString() {
        return "MatchDetail{" +
            "matchDTO=" + matchDTO +
            ", localNom='" + localNom + '\'' +
            ", visiteurNom='" + visiteurNom + '\'' +
            ", localImage='" + localImage + '\'' +
            ", visiteurImage='" + visiteurImage + '\'' +
            ", nbButsLocal=" + nbButsLocal +
            ", nbButsVisiteur=" + nbButsVisiteur +
            ", hasTirAuBut=" + hasTirAuBut +
            ", nbTirAuButsLocalMarque=" + nbTirAuButsLocalMarque +
            ", nbTirAuButsLocalRate=" + nbTirAuButsLocalRate +
            ", nbTirAuButsVisiteurMarque=" + nbTirAuButsVisiteurMarque +
            ", nbTirAuButsVisiteurRate=" + nbTirAuButsVisiteurRate +
            ", saisonId=" + saisonId +
            ", saisonNom='" + saisonNom + '\'' +
            ", competitionId=" + competitionId +
            ", competitionNom='" + competitionNom + '\'' +
            ", categoryId=" + categoryId +
            ", categoryNom='" + categoryNom + '\'' +
            ", isExternal='" + isExternal + '\'' +
            '}';
    }
}
