package org.nddngroup.navetfoot.core.domain.custom.sync;

import org.nddngroup.navetfoot.core.service.dto.AffiliationDTO;

/**
 * Created by souleymane91 on 06/10/2018.
 */
public class CustomAffiliation {
    private String organisationHashcode;
    private String associationHashcode;

    private AffiliationDTO affiliationDTO;

    public String getOrganisationHashcode() {
        return organisationHashcode;
    }

    public void setOrganisationHashcode(String organisatitionHashcode) {
        this.organisationHashcode = organisatitionHashcode;
    }

    public String getAssociationHashcode() {
        return associationHashcode;
    }

    public void setAssociationHashcode(String associationHashcode) {
        this.associationHashcode = associationHashcode;
    }

    public AffiliationDTO getAffiliationDTO() {
        return affiliationDTO;
    }

    public void setAffiliationDTO(AffiliationDTO affiliation) {
        this.affiliationDTO = affiliation;
    }
}
