package org.nddngroup.navetfoot.core.domain.custom.sync;

import org.nddngroup.navetfoot.core.service.dto.FeuilleDeMatchDTO;

/**
 * Created by souleymane91 on 06/10/2018.
 */
public class CustomFeuilleDeMatch {
    private String matchHashcode;
    private FeuilleDeMatchDTO feuilleDeMatchDTO;

    public String getMatchHashcode() {
        return matchHashcode;
    }

    public void setMatchHashcode(String matchHashcode) {
        this.matchHashcode = matchHashcode;
    }

    public FeuilleDeMatchDTO getFeuilleDeMatchDTO() {
        return feuilleDeMatchDTO;
    }

    public void setFeuilleDeMatchDTO(FeuilleDeMatchDTO feuilleDeMatchDTO) {
        this.feuilleDeMatchDTO = feuilleDeMatchDTO;
    }
}
