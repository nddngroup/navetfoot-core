package org.nddngroup.navetfoot.core.domain.custom.stats;

import org.nddngroup.navetfoot.core.service.dto.EtapeCompetitionDTO;

import java.util.List;

public class EtapeDetail {
    private EtapeCompetitionDTO etape;
    private int totalJournee;
    private List<MatchJournee> journees;

    public EtapeCompetitionDTO getEtape() {
        return etape;
    }

    public void setEtape(EtapeCompetitionDTO etape) {
        this.etape = etape;
    }

    public int getTotalJournee() {
        return totalJournee;
    }

    public void setTotalJournee(int totalJournee) {
        this.totalJournee = totalJournee;
    }

    public List<MatchJournee> getJournees() {
        return journees;
    }

    public void setJournees(List<MatchJournee> journees) {
        this.journees = journees;
    }

    @Override
    public String toString() {
        return "EtapeDetail{" +
            "etape=" + etape +
            ", totalJournee=" + totalJournee +
            ", journees=" + journees +
            '}';
    }
}
