package org.nddngroup.navetfoot.core.domain.custom.stats;

import org.nddngroup.navetfoot.core.domain.enumeration.Position;

public class JoueurDetail {
    private Long id;
    private String prenom;
    private String nom;
    private String identifiant;
    private String image;
    private String dateDeNaissance;
    private String lieuDeNaissance;
    private Integer numero;
    private int cartonJaune;
    private int cartonRouge;
    private Position position;

    public Long getId() {
        return id;
    }

    public JoueurDetail setId(Long id) {
        this.id = id;
        return this;
    }

    public String getPrenom() {
        return prenom;
    }

    public JoueurDetail setPrenom(String prenom) {
        this.prenom = prenom;
        return this;
    }

    public String getNom() {
        return nom;
    }

    public JoueurDetail setNom(String nom) {
        this.nom = nom;
        return this;
    }

    public String getIdentifiant() {
        return identifiant;
    }

    public JoueurDetail setIdentifiant(String identifiant) {
        this.identifiant = identifiant;
        return this;
    }

    public String getImage() {
        return image;
    }

    public JoueurDetail setImage(String image) {
        this.image = image;
        return this;
    }

    public String getDateDeNaissance() {
        return dateDeNaissance;
    }

    public JoueurDetail setDateDeNaissance(String dateDeNaissance) {
        this.dateDeNaissance = dateDeNaissance;
        return this;
    }

    public String getLieuDeNaissance() {
        return lieuDeNaissance;
    }

    public JoueurDetail setLieuDeNaissance(String lieuDeNaissance) {
        this.lieuDeNaissance = lieuDeNaissance;
        return this;
    }

    public Integer getNumero() {
        return numero;
    }

    public JoueurDetail setNumero(Integer numero) {
        this.numero = numero;
        return this;
    }

    public int getCartonJaune() {
        return cartonJaune;
    }

    public JoueurDetail setCartonJaune(int cartonJaune) {
        this.cartonJaune = cartonJaune;
        return this;
    }

    public int getCartonRouge() {
        return cartonRouge;
    }

    public JoueurDetail setCartonRouge(int cartonRouge) {
        this.cartonRouge = cartonRouge;
        return this;
    }

    public Position getPosition() {
        return position;
    }

    public JoueurDetail setPosition(Position position) {
        this.position = position;
        return this;
    }
}
