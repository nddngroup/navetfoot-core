package org.nddngroup.navetfoot.core.domain.custom.stats;

import org.nddngroup.navetfoot.core.service.dto.HorsJeuDTO;

import java.io.Serializable;

/**
 * Created by souleymane91 on 10/11/2018.
 */
public class HorsJeuDetail implements Serializable {
    private HorsJeuDTO horsJeuDTO;

    public HorsJeuDTO getHorsJeuDTO() {
        return horsJeuDTO;
    }

    public void setHorsJeuDTO(HorsJeuDTO horsJeuDTO) {
        this.horsJeuDTO = horsJeuDTO;
    }
}
