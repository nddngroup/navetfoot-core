package org.nddngroup.navetfoot.core.domain.custom.sync;

import org.nddngroup.navetfoot.core.service.dto.DetailPouleDTO;

/**
 * Created by souleymane91 on 06/10/2018.
 */
public class CustomDetailPoule {
    private String pouleHashcode;
    private String associationHashcode;
    private DetailPouleDTO detailPouleDTO;

    public String getPouleHashcode() {
        return pouleHashcode;
    }

    public void setPouleHashcode(String pouleHashcode) {
        this.pouleHashcode = pouleHashcode;
    }

    public String getAssociationHashcode() {
        return associationHashcode;
    }

    public void setAssociationHashcode(String associationHashcode) {
        this.associationHashcode = associationHashcode;
    }

    public DetailPouleDTO getDetailPouleDTO() {
        return detailPouleDTO;
    }

    public void setDetailPouleDTO(DetailPouleDTO detailPouleDTO) {
        this.detailPouleDTO = detailPouleDTO;
    }
}
