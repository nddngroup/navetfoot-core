package org.nddngroup.navetfoot.core.domain.es;

import javax.persistence.Transient;

public class RemplacementStats extends MatchStats {
    @Transient
    private Long entrantId;
    @Transient
    private String entrantNom;
    @Transient
    private String entrantPrenom;
    @Transient
    private String entrantImage;
    @Transient
    private Long sortantId;
    @Transient
    private String sortantNom;
    @Transient
    private String sortantPrenom;
    @Transient
    private String sortantImage;

    @Transient
    private Long matchId;

    public Long getEntrantId() {
        return entrantId;
    }

    public void setEntrantId(Long entrantId) {
        this.entrantId = entrantId;
    }

    public String getEntrantNom() {
        return entrantNom;
    }

    public void setEntrantNom(String entrantNom) {
        this.entrantNom = entrantNom;
    }

    public String getEntrantPrenom() {
        return entrantPrenom;
    }

    public void setEntrantPrenom(String entrantPrenom) {
        this.entrantPrenom = entrantPrenom;
    }

    public String getEntrantImage() {
        return entrantImage;
    }

    public void setEntrantImage(String entrantImage) {
        this.entrantImage = entrantImage;
    }

    public Long getSortantId() {
        return sortantId;
    }

    public void setSortantId(Long sortantId) {
        this.sortantId = sortantId;
    }

    public String getSortantNom() {
        return sortantNom;
    }

    public void setSortantNom(String sortantNom) {
        this.sortantNom = sortantNom;
    }

    public String getSortantPrenom() {
        return sortantPrenom;
    }

    public void setSortantPrenom(String sortantPrenom) {
        this.sortantPrenom = sortantPrenom;
    }

    public String getSortantImage() {
        return sortantImage;
    }

    public void setSortantImage(String sortantImage) {
        this.sortantImage = sortantImage;
    }

    public Long getMatchId() {
        return matchId;
    }

    public void setMatchId(Long matchId) {
        this.matchId = matchId;
    }
}
