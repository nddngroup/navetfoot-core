package org.nddngroup.navetfoot.core.domain.enumeration;

/**
 * The Type enumeration.
 */
public enum Type {
    CHAMPIONNAT, MATCH_DIRECT, ALLEZ_RETOUR
}
