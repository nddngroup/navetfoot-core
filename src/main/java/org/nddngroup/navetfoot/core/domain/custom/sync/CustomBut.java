package org.nddngroup.navetfoot.core.domain.custom.sync;

import org.nddngroup.navetfoot.core.service.dto.ButDTO;

/**
 * Created by souleymane91 on 06/10/2018.
 */
public class CustomBut {
    private String matchHashcode;
    private String joueurHashcode;
    private ButDTO butDTO;

    public String getMatchHashcode() {
        return matchHashcode;
    }

    public void setMatchHashcode(String matchHashcode) {
        this.matchHashcode = matchHashcode;
    }

    public String getJoueurHashcode() {
        return joueurHashcode;
    }

    public void setJoueurHashcode(String joueurHashcode) {
        this.joueurHashcode = joueurHashcode;
    }

    public ButDTO getButDTO() {
        return butDTO;
    }

    public void setButDTO(ButDTO butDTO) {
        this.butDTO = butDTO;
    }
}
