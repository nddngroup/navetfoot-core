package org.nddngroup.navetfoot.core.domain.contant;

public class Constant {
    public static final int DELEGUE_CODE_SIZE  = 6;
    public static final String PHASE_DE_GROUPE  = "Phase de poules";
    public static final String CODE_PHASE_DE_GROUPE  = "C100";
    public static final String CHAMPIONNAT  = "Championnat";
    public static final String CODE_CHAMPIONNAT  = "C200";
}
