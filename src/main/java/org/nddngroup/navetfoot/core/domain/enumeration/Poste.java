package org.nddngroup.navetfoot.core.domain.enumeration;

/**
 * The Position enumeration.
 */
public enum Poste {
    GARDIEN, DENFENSEUR, MILIEU, ATTAQUANT, NONE
}
