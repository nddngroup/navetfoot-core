package org.nddngroup.navetfoot.core.domain.custom.sync;

import org.nddngroup.navetfoot.core.service.dto.ContratDTO;

/**
 * Created by souleymane91 on 06/10/2018.
 */
public class CustomContrat {
    private String saisonHashcode;
    private String associationHashcode;
    private String joueurHashcode;
    private ContratDTO contratDTO;

    public String getSaisonHashcode() {
        return saisonHashcode;
    }

    public void setSaisonHashcode(String saisonHashcode) {
        this.saisonHashcode = saisonHashcode;
    }

    public String getAssociationHashcode() {
        return associationHashcode;
    }

    public void setAssociationHashcode(String associationHashcode) {
        this.associationHashcode = associationHashcode;
    }

    public String getJoueurHashcode() {
        return joueurHashcode;
    }

    public void setJoueurHashcode(String joueurHashcode) {
        this.joueurHashcode = joueurHashcode;
    }

    public ContratDTO getContratDTO() {
        return contratDTO;
    }

    public void setContratDTO(ContratDTO contratDTO) {
        this.contratDTO = contratDTO;
    }
}
