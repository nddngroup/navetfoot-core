package org.nddngroup.navetfoot.core.domain.custom.sync;

import org.nddngroup.navetfoot.core.service.dto.CoupFrancDTO;

/**
 * Created by souleymane91 on 06/10/2018.
 */
public class CustomCoupFranc {
    private String matchHashcode;
    private CoupFrancDTO coupFrancDTO;

    public String getMatchHashcode() {
        return matchHashcode;
    }

    public void setMatchHashcode(String matchHashcode) {
        this.matchHashcode = matchHashcode;
    }

    public CoupFrancDTO getCoupFrancDTO() {
        return coupFrancDTO;
    }

    public void setCoupFrancDTO(CoupFrancDTO coupFrancDTO) {
        this.coupFrancDTO = coupFrancDTO;
    }
}
