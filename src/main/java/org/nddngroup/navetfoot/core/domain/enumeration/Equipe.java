package org.nddngroup.navetfoot.core.domain.enumeration;

/**
 * The Equipe enumeration.
 */
public enum Equipe {
    LOCAL, VISITEUR
}
