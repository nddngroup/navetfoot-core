package org.nddngroup.navetfoot.core.domain.custom;

import org.nddngroup.navetfoot.core.domain.custom.stats.MatchDetail;
import org.nddngroup.navetfoot.core.service.dto.CompetitionDTO;
import org.nddngroup.navetfoot.core.service.dto.OrganisationDTO;
import org.nddngroup.navetfoot.core.service.dto.SaisonDTO;

import java.io.Serializable;
import java.util.List;

public class CompetitionWithSaisonsAndMatchs implements Serializable {
    private CompetitionDTO competition;
    private OrganisationDTO organisation;
    private List<SaisonDTO> saisons;
    private List<MatchDetail> matchs;

    public CompetitionDTO getCompetition() {
        return competition;
    }

    public void setCompetition(CompetitionDTO competition) {
        this.competition = competition;
    }

    public OrganisationDTO getOrganisation() {
        return organisation;
    }

    public void setOrganisation(OrganisationDTO organisation) {
        this.organisation = organisation;
    }

    public List<SaisonDTO> getSaisons() {
        return saisons;
    }

    public void setSaisons(List<SaisonDTO> saisons) {
        this.saisons = saisons;
    }

    public List<MatchDetail> getMatchs() {
        return matchs;
    }

    public void setMatchs(List<MatchDetail> matchs) {
        this.matchs = matchs;
    }

    @Override
    public String toString() {
        return "CompetitionWithSaisonsAndMatchs{" +
            "competition=" + competition +
            ", organisation=" + organisation +
            ", saisons=" + saisons +
            ", matchs=" + matchs +
            '}';
    }
}
