package org.nddngroup.navetfoot.core.domain.custom.stats;

import org.nddngroup.navetfoot.core.service.dto.DetailFeuilleDeMatchDTO;

import java.io.Serializable;

/**
 * Created by souleymane91 on 10/11/2018.
 */
public class DetailFeuilleDeMatchDetail implements Serializable {
    private DetailFeuilleDeMatchDTO detailFeuilleDeMatchDTO;
    private String prenomJoueur;
    private String nomJoueur;
    private String imageUrlJoueur;
    private String joueurHashcode;
    private String identifiant;

    public DetailFeuilleDeMatchDTO getDetailFeuilleDeMatchDTO() {
        return detailFeuilleDeMatchDTO;
    }

    public void setDetailFeuilleDeMatchDTO(DetailFeuilleDeMatchDTO detailFeuilleDeMatchDTO) {
        this.detailFeuilleDeMatchDTO = detailFeuilleDeMatchDTO;
    }

    public String getPrenomJoueur() {
        return prenomJoueur;
    }

    public void setPrenomJoueur(String prenomJoueur) {
        this.prenomJoueur = prenomJoueur;
    }

    public String getNomJoueur() {
        return nomJoueur;
    }

    public void setNomJoueur(String nomJoueur) {
        this.nomJoueur = nomJoueur;
    }

    public String getImageUrlJoueur() {
        return imageUrlJoueur;
    }

    public void setImageUrlJoueur(String imageUrlJoueur) {
        this.imageUrlJoueur = imageUrlJoueur;
    }

    public String getJoueurHashcode() {
        return joueurHashcode;
    }

    public void setJoueurHashcode(String joueurHashcode) {
        this.joueurHashcode = joueurHashcode;
    }

    public String getIdentifiant() {
        return identifiant;
    }

    public void setIdentifiant(String identifiant) {
        this.identifiant = identifiant;
    }
}
