package org.nddngroup.navetfoot.core.domain.enumeration;

/**
 * The Cause enumeration.
 */
public enum Cause {
    PENALTY, COUP_FRANC, CORNER, COURS_JEU, FORFAIT, RESERVE, CSC
}
