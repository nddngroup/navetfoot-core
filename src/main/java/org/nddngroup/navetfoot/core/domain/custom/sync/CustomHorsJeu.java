package org.nddngroup.navetfoot.core.domain.custom.sync;

import org.nddngroup.navetfoot.core.service.dto.HorsJeuDTO;

/**
 * Created by souleymane91 on 06/10/2018.
 */
public class CustomHorsJeu {
    private String matchHashcode;
    private HorsJeuDTO horsJeuDTO;

    public String getMatchHashcode() {
        return matchHashcode;
    }

    public void setMatchHashcode(String matchHashcode) {
        this.matchHashcode = matchHashcode;
    }

    public HorsJeuDTO getHorsJeuDTO() {
        return horsJeuDTO;
    }

    public void setHorsJeuDTO(HorsJeuDTO horsJeuDTO) {
        this.horsJeuDTO = horsJeuDTO;
    }
}
