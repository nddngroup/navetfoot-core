package org.nddngroup.navetfoot.core.domain.custom.sync;

import org.nddngroup.navetfoot.core.service.dto.TirDTO;

/**
 * Created by souleymane91 on 06/10/2018.
 */
public class CustomTir {
    private String matchHashcode;
    private String joueurHashcode;
    private TirDTO tirDTO;

    public String getMatchHashcode() {
        return matchHashcode;
    }

    public void setMatchHashcode(String matchHashcode) {
        this.matchHashcode = matchHashcode;
    }

    public String getJoueurHashcode() {
        return joueurHashcode;
    }

    public void setJoueurHashcode(String joueurHashcode) {
        this.joueurHashcode = joueurHashcode;
    }

    public TirDTO getTirDTO() {
        return tirDTO;
    }

    public void setTirDTO(TirDTO tirDTO) {
        this.tirDTO = tirDTO;
    }
}
