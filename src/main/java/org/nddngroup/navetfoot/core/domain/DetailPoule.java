package org.nddngroup.navetfoot.core.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * A DetailPoule.
 */
@Entity
@Table(name = "detail_poule")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "detailpoule")
public class DetailPoule implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "nombre_de_point")
    private Integer nombreDePoint;

    @Column(name = "goal_difference")
    private Integer goalDifference;

    @Column(name = "journee")
    private Integer journee;

    @Column(name = "code")
    private String code;

    @Column(name = "hashcode")
    private String hashcode;

    @Column(name = "created_at")
    private ZonedDateTime createdAt;

    @Column(name = "update_at")
    private ZonedDateTime updateAt;

    @Column(name = "deleted")
    private Boolean deleted;

    @Column(name = "victoire")
    private Integer victoire;

    @Column(name = "match_null")
    private Integer matchNull;

    @Column(name = "defaite")
    private Integer defaite;

    @Column(name = "buts_pour")
    private Integer butsPour;

    @Column(name = "buts_contre")
    private Integer butsContre;

    @ManyToOne
    @JsonIgnoreProperties(value = "detailPoules", allowSetters = true)
    private Association association;

    @ManyToOne
    @JsonIgnoreProperties(value = "detailPoules", allowSetters = true)
    private Poule poule;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNombreDePoint() {
        return nombreDePoint;
    }

    public DetailPoule nombreDePoint(Integer nombreDePoint) {
        this.nombreDePoint = nombreDePoint;
        return this;
    }

    public void setNombreDePoint(Integer nombreDePoint) {
        this.nombreDePoint = nombreDePoint;
    }

    public Integer getGoalDifference() {
        return goalDifference;
    }

    public DetailPoule goalDifference(Integer goalDifference) {
        this.goalDifference = goalDifference;
        return this;
    }

    public void setGoalDifference(Integer goalDifference) {
        this.goalDifference = goalDifference;
    }

    public Integer getJournee() {
        return journee;
    }

    public DetailPoule journee(Integer journee) {
        this.journee = journee;
        return this;
    }

    public void setJournee(Integer journee) {
        this.journee = journee;
    }

    public String getCode() {
        return code;
    }

    public DetailPoule code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getHashcode() {
        return hashcode;
    }

    public DetailPoule hashcode(String hashcode) {
        this.hashcode = hashcode;
        return this;
    }

    public void setHashcode(String hashcode) {
        this.hashcode = hashcode;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public DetailPoule createdAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdateAt() {
        return updateAt;
    }

    public DetailPoule updateAt(ZonedDateTime updateAt) {
        this.updateAt = updateAt;
        return this;
    }

    public void setUpdateAt(ZonedDateTime updateAt) {
        this.updateAt = updateAt;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public DetailPoule deleted(Boolean deleted) {
        this.deleted = deleted;
        return this;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Integer getVictoire() {
        return victoire;
    }

    public DetailPoule victoire(Integer victoire) {
        this.victoire = victoire;
        return this;
    }

    public void setVictoire(Integer victoire) {
        this.victoire = victoire;
    }

    public Integer getMatchNull() {
        return matchNull;
    }

    public DetailPoule matchNull(Integer matchNull) {
        this.matchNull = matchNull;
        return this;
    }

    public void setMatchNull(Integer matchNull) {
        this.matchNull = matchNull;
    }

    public Integer getDefaite() {
        return defaite;
    }

    public DetailPoule defaite(Integer defaite) {
        this.defaite = defaite;
        return this;
    }

    public void setDefaite(Integer defaite) {
        this.defaite = defaite;
    }

    public Integer getButsPour() {
        return butsPour;
    }

    public DetailPoule butsPour(Integer butsPour) {
        this.butsPour = butsPour;
        return this;
    }

    public void setButsPour(Integer butsPour) {
        this.butsPour = butsPour;
    }

    public Integer getButsContre() {
        return butsContre;
    }

    public DetailPoule butsContre(Integer butsContre) {
        this.butsContre = butsContre;
        return this;
    }

    public void setButsContre(Integer butsContre) {
        this.butsContre = butsContre;
    }

    public Association getAssociation() {
        return association;
    }

    public DetailPoule association(Association association) {
        this.association = association;
        return this;
    }

    public void setAssociation(Association association) {
        this.association = association;
    }

    public Poule getPoule() {
        return poule;
    }

    public DetailPoule poule(Poule poule) {
        this.poule = poule;
        return this;
    }

    public void setPoule(Poule poule) {
        this.poule = poule;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DetailPoule)) {
            return false;
        }
        return id != null && id.equals(((DetailPoule) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DetailPoule{" +
            "id=" + getId() +
            ", nombreDePoint=" + getNombreDePoint() +
            ", goalDifference=" + getGoalDifference() +
            ", journee=" + getJournee() +
            ", code='" + getCode() + "'" +
            ", hashcode='" + getHashcode() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updateAt='" + getUpdateAt() + "'" +
            ", deleted='" + isDeleted() + "'" +
            ", victoire=" + getVictoire() +
            ", matchNull=" + getMatchNull() +
            ", defaite=" + getDefaite() +
            ", butsPour=" + getButsPour() +
            ", butsContre=" + getButsContre() +
            "}";
    }
}
