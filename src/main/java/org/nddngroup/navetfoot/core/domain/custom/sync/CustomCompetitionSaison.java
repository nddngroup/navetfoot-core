package org.nddngroup.navetfoot.core.domain.custom.sync;

import org.nddngroup.navetfoot.core.service.dto.CompetitionSaisonDTO;

/**
 * Created by souleymane91 on 06/10/2018.
 */
public class CustomCompetitionSaison {
    private String competitionHashcode;
    private String SaisonHashcode;
    private CompetitionSaisonDTO competitionSaisonDTO;

    public String getCompetitionHashcode() {
        return competitionHashcode;
    }

    public void setCompetitionHashcode(String competitionHashcode) {
        this.competitionHashcode = competitionHashcode;
    }

    public String getSaisonHashcode() {
        return SaisonHashcode;
    }

    public void setSaisonHashcode(String saisonHashcode) {
        SaisonHashcode = saisonHashcode;
    }

    public CompetitionSaisonDTO getCompetitionSaisonDTO() {
        return competitionSaisonDTO;
    }

    public void setCompetitionSaisonDTO(CompetitionSaisonDTO competitionSaisonDTO) {
        this.competitionSaisonDTO = competitionSaisonDTO;
    }
}
