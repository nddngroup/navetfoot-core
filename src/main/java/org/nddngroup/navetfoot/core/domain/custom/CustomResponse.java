package org.nddngroup.navetfoot.core.domain.custom;

/**
 * Created by souleymane91 on 19/08/2018.
 */
public class CustomResponse {
    private Integer code;
    private String message;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
