package org.nddngroup.navetfoot.core.domain.es;

import javax.persistence.*;

public class MatchStats {
    @Transient
    protected Long localId;
    @Transient
    protected String localNom;
    @Transient
    protected String localImageUrl;
    @Transient
    protected Long visiteurId;
    @Transient
    protected String visiteurNom;
    @Transient
    protected String visiteurImageUrl;
    protected Long etapeCompetitionId;
    @Transient
    protected String etapeCompetitionNom;
    @Transient
    protected Long saisonId;
    @Transient
    protected String saisonLibelle;
    @Transient
    private Long competitionId;
    @Transient
    private String competitionNom;
    @Transient
    protected Long organisationId;
    @Transient
    protected String organisationNom;

    public Long getLocalId() {
        return localId;
    }

    public MatchStats localId(Long localId) {
        this.localId = localId;
        return this;
    }

    public void setLocalId(Long localId) {
        this.localId = localId;
    }

    public String getLocalNom() {
        return localNom;
    }

    public MatchStats localNom(String localNom) {
        this.localNom = localNom;
        return this;
    }

    public void setLocalNom(String localNom) {
        this.localNom = localNom;
    }

    public String getLocalImageUrl() {
        return localImageUrl;
    }

    public MatchStats localImageUrl(String localImageUrl) {
        this.localImageUrl = localImageUrl;
        return this;
    }

    public void setLocalImageUrl(String localImageUrl) {
        this.localImageUrl = localImageUrl;
    }

    public Long getVisiteurId() {
        return visiteurId;
    }

    public MatchStats visiteurId(Long visiteurId) {
        this.visiteurId = visiteurId;
        return this;
    }

    public void setVisiteurId(Long visiteurId) {
        this.visiteurId = visiteurId;
    }

    public String getVisiteurNom() {
        return visiteurNom;
    }

    public MatchStats visiteurNom(String visiteurNom) {
        this.visiteurNom = visiteurNom;
        return this;
    }

    public void setVisiteurNom(String visiteurNom) {
        this.visiteurNom = visiteurNom;
    }


    public String getVisiteurImageUrl() {
        return visiteurImageUrl;
    }

    public MatchStats visiteurImageUrl(String visiteurImageUrl) {
        this.visiteurImageUrl = visiteurImageUrl;
        return this;
    }

    public void setVisiteurImageUrl(String visiteurImageUrl) {
        this.visiteurImageUrl = visiteurImageUrl;
    }

    public Long getEtapeCompetitionId() {
        return etapeCompetitionId;
    }

    public MatchStats etapeCompetitionId(Long etapeCompetitionId) {
        this.etapeCompetitionId = etapeCompetitionId;
        return this;
    }

    public void setEtapeCompetitionId(Long etapeCompetitionId) {
        this.etapeCompetitionId = etapeCompetitionId;
    }

    public String getEtapeCompetitionNom() {
        return etapeCompetitionNom;
    }

    public MatchStats etapeCompetitionNom(String etapeCompetitionNom) {
        this.etapeCompetitionNom = etapeCompetitionNom;
        return this;
    }

    public void setEtapeCompetitionNom(String etapeCompetitionNom) {
        this.etapeCompetitionNom = etapeCompetitionNom;
    }

    public Long getSaisonId() {
        return saisonId;
    }

    public MatchStats saisonId(Long saisonId) {
        this.saisonId = saisonId;
        return this;
    }

    public void setSaisonId(Long saisonId) {
        this.saisonId = saisonId;
    }

    public String getSaisonLibelle() {
        return saisonLibelle;
    }

    public MatchStats saisonLibelle(String saisonLibelle) {
        this.saisonLibelle = saisonLibelle;
        return this;
    }

    public void setSaisonLibelle(String saisonLibelle) {
        this.saisonLibelle = saisonLibelle;
    }

    public Long getCompetitionId() {
        return competitionId;
    }

    public MatchStats competitionId(Long competitionId) {
        this.competitionId = competitionId;
        return this;
    }

    public void setCompetitionId(Long competitionId) {
        this.competitionId = competitionId;
    }

    public String getCompetitionNom() {
        return competitionNom;
    }

    public MatchStats competitionNom(String competitionNom) {
        this.competitionNom = competitionNom;
        return this;
    }

    public void setCompetitionNom(String competitionNom) {
        this.competitionNom = competitionNom;
    }

    public Long getOrganisationId() {
        return organisationId;
    }

    public MatchStats organisationId(Long organisationId) {
        this.organisationId = organisationId;
        return this;
    }

    public void setOrganisationId(Long organisationId) {
        this.organisationId = organisationId;
    }

    public String getOrganisationNom() {
        return organisationNom;
    }

    public MatchStats organisationNom(String organisationNom) {
        this.organisationNom = organisationNom;
        return this;
    }

    public void setOrganisationNom(String organisationNom) {
        this.organisationNom = organisationNom;
    }

    @Override
    public String toString() {
        return "MatchStats{" +
            "etapeCompetitionNom='" + etapeCompetitionNom + '\'' +
            ", saisonId=" + saisonId +
            ", saisonLibelle='" + saisonLibelle + '\'' +
            ", competitionId=" + competitionId +
            ", competitionNom='" + competitionNom + '\'' +
            ", organisationId=" + organisationId +
            ", organisationNom='" + organisationNom + '\'' +
            '}';
    }
}
