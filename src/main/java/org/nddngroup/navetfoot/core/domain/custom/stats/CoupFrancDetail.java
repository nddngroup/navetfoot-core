package org.nddngroup.navetfoot.core.domain.custom.stats;

import org.nddngroup.navetfoot.core.service.dto.CoupFrancDTO;

import java.io.Serializable;

/**
 * Created by souleymane91 on 10/11/2018.
 */
public class CoupFrancDetail implements Serializable {
    private CoupFrancDTO coupFrancDTO;

    public CoupFrancDTO getCoupFrancDTO() {
        return coupFrancDTO;
    }

    public void setCoupFrancDTO(CoupFrancDTO coupFrancDTO) {
        this.coupFrancDTO = coupFrancDTO;
    }
}
