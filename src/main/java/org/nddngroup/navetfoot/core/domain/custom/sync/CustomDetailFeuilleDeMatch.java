package org.nddngroup.navetfoot.core.domain.custom.sync;

import org.nddngroup.navetfoot.core.service.dto.DetailFeuilleDeMatchDTO;

/**
 * Created by souleymane91 on 06/10/2018.
 */
public class CustomDetailFeuilleDeMatch {
    private String feuilleDeMatchHashcode;
    private String joueurHashcode;
    private DetailFeuilleDeMatchDTO detailFeuilleDeMatchDTO;

    public String getFeuilleDeMatchHashcode() {
        return feuilleDeMatchHashcode;
    }

    public void setFeuilleDeMatchHashcode(String feuilleDeMatchHashcode) {
        this.feuilleDeMatchHashcode = feuilleDeMatchHashcode;
    }

    public String getJoueurHashcode() {
        return joueurHashcode;
    }

    public void setJoueurHashcode(String joueurHashcode) {
        this.joueurHashcode = joueurHashcode;
    }

    public DetailFeuilleDeMatchDTO getDetailFeuilleDeMatchDTO() {
        return detailFeuilleDeMatchDTO;
    }

    public void setDetailFeuilleDeMatchDTO(DetailFeuilleDeMatchDTO detailFeuilleDeMatchDTO) {
        this.detailFeuilleDeMatchDTO = detailFeuilleDeMatchDTO;
    }
}
