package org.nddngroup.navetfoot.core.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import org.nddngroup.navetfoot.core.domain.enumeration.TypeParamatrage;

import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * A Parametrage.
 */
@Entity
@Table(name = "parametrage")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "parametrage")
public class Parametrage implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "tag")
    private String tag;

    @Column(name = "libelle")
    private String libelle;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private TypeParamatrage type;

    @Column(name = "valeur")
    private String valeur;

    @Column(name = "fonction_de_choix")
    private String fonctionDeChoix;

    @Column(name = "created_at")
    private ZonedDateTime createdAt;

    @Column(name = "updated_at")
    private ZonedDateTime updatedAt;

    @Column(name = "reference_tag")
    private String referenceTag;

    @Column(name = "reference_id")
    private Long referenceId;

    @Column(name = "description")
    private String description;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTag() {
        return tag;
    }

    public Parametrage tag(String tag) {
        this.tag = tag;
        return this;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getLibelle() {
        return libelle;
    }

    public Parametrage libelle(String libelle) {
        this.libelle = libelle;
        return this;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public TypeParamatrage getType() {
        return type;
    }

    public Parametrage type(TypeParamatrage type) {
        this.type = type;
        return this;
    }

    public void setType(TypeParamatrage type) {
        this.type = type;
    }

    public String getValeur() {
        return valeur;
    }

    public Parametrage valeur(String valeur) {
        this.valeur = valeur;
        return this;
    }

    public void setValeur(String valeur) {
        this.valeur = valeur;
    }

    public String getFonctionDeChoix() {
        return fonctionDeChoix;
    }

    public Parametrage fonctionDeChoix(String fonctionDeChoix) {
        this.fonctionDeChoix = fonctionDeChoix;
        return this;
    }

    public void setFonctionDeChoix(String fonctionDeChoix) {
        this.fonctionDeChoix = fonctionDeChoix;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public Parametrage createdAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public Parametrage updatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getReferenceTag() {
        return referenceTag;
    }

    public Parametrage referenceTag(String referenceTag) {
        this.referenceTag = referenceTag;
        return this;
    }

    public void setReferenceTag(String referenceTag) {
        this.referenceTag = referenceTag;
    }

    public Long getReferenceId() {
        return referenceId;
    }

    public Parametrage referenceId(Long referenceId) {
        this.referenceId = referenceId;
        return this;
    }

    public void setReferenceId(Long referenceId) {
        this.referenceId = referenceId;
    }

    public String getDescription() {
        return description;
    }

    public Parametrage description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Parametrage)) {
            return false;
        }
        return id != null && id.equals(((Parametrage) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Parametrage{" +
            "id=" + getId() +
            ", tag='" + getTag() + "'" +
            ", libelle='" + getLibelle() + "'" +
            ", type='" + getType() + "'" +
            ", valeur='" + getValeur() + "'" +
            ", fonctionDeChoix='" + getFonctionDeChoix() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", referenceTag='" + getReferenceTag() + "'" +
            ", referenceId=" + getReferenceId() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
