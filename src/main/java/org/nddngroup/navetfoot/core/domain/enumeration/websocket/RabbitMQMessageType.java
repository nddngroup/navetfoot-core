package org.nddngroup.navetfoot.core.domain.enumeration.websocket;

public enum RabbitMQMessageType {
    MATCH_CHRONO,
    MATCH_STATE,
    MATCH_EVENT
}
