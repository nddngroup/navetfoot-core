package org.nddngroup.navetfoot.core.domain.custom.stats;

import org.nddngroup.navetfoot.core.service.dto.FeuilleDeMatchDTO;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by souleymane91 on 10/11/2018.
 */
public class FeuilleDeMatchDetail implements Serializable {
    private FeuilleDeMatchDTO feuilleDeMatchDTO;
    private String matchHashcode;
    private List<DetailFeuilleDeMatchDetail> detailsFeuilleDeMatchDetail;

    public FeuilleDeMatchDetail() {
        this.detailsFeuilleDeMatchDetail = new ArrayList<>();
    }

    public FeuilleDeMatchDTO getFeuilleDeMatchDTO() {
        return feuilleDeMatchDTO;
    }

    public void setFeuilleDeMatchDTO(FeuilleDeMatchDTO feuilleDeMatchDTO) {
        this.feuilleDeMatchDTO = feuilleDeMatchDTO;
    }

    public String getMatchHashcode() {
        return matchHashcode;
    }

    public void setMatchHashcode(String matchHashcode) {
        this.matchHashcode = matchHashcode;
    }

    public List<DetailFeuilleDeMatchDetail> getDetailsFeuilleDeMatchDetail() {
        return detailsFeuilleDeMatchDetail;
    }

    public void setDetailsFeuilleDeMatchDetail(List<DetailFeuilleDeMatchDetail> detailsFeuilleDeMatchDetail) {
        this.detailsFeuilleDeMatchDetail = detailsFeuilleDeMatchDetail;
    }
}
