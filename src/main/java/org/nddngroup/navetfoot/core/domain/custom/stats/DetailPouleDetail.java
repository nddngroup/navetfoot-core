package org.nddngroup.navetfoot.core.domain.custom.stats;

import org.nddngroup.navetfoot.core.service.dto.DetailPouleDTO;

import java.io.Serializable;
import java.util.List;

/**
 * Created by souleymane91 on 11/11/2018.
 */
public class DetailPouleDetail implements Serializable {
    private List<DetailPouleDTO> detailsPouleDTO;
    private String equipe;
    private String image;


    public List<DetailPouleDTO> getDetailsPouleDTO() {
        return detailsPouleDTO;
    }

    public void setDetailsPouleDTO(List<DetailPouleDTO> detailsPouleDTO) {
        this.detailsPouleDTO = detailsPouleDTO;
    }

    public String getEquipe() {
        return this.equipe;
    }

    public void setEquipe(String equipe) {
        this.equipe = equipe;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
