package org.nddngroup.navetfoot.core.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import org.nddngroup.navetfoot.core.domain.es.RemplacementStats;
import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;

import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * A Remplacement.
 */
@Entity
@Table(name = "remplacement")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "remplacement")
public class Remplacement extends RemplacementStats implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "hashcode")
    private String hashcode;

    @Column(name = "code")
    private String code;

    @Column(name = "instant")
    private Integer instant;

    @Column(name = "created_at")
    private ZonedDateTime createdAt;

    @Column(name = "updated_at")
    private ZonedDateTime updatedAt;

    @Enumerated(EnumType.STRING)
    @Column(name = "equipe")
    private Equipe equipe;

    @Column(name = "deleted")
    private Boolean deleted;

    @ManyToOne
    @JsonIgnoreProperties(value = "remplacements", allowSetters = true)
    private Match match;

    @ManyToOne
    @JsonIgnoreProperties(value = "remplacements", allowSetters = true)
    private Joueur sortant;

    @ManyToOne
    @JsonIgnoreProperties(value = "remplacements", allowSetters = true)
    private Joueur entrant;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHashcode() {
        return hashcode;
    }

    public Remplacement hashcode(String hashcode) {
        this.hashcode = hashcode;
        return this;
    }

    public void setHashcode(String hashcode) {
        this.hashcode = hashcode;
    }

    public String getCode() {
        return code;
    }

    public Remplacement code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getInstant() {
        return instant;
    }

    public Remplacement instant(Integer instant) {
        this.instant = instant;
        return this;
    }

    public void setInstant(Integer instant) {
        this.instant = instant;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public Remplacement createdAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public Remplacement updatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Equipe getEquipe() {
        return equipe;
    }

    public Remplacement equipe(Equipe equipe) {
        this.equipe = equipe;
        return this;
    }

    public void setEquipe(Equipe equipe) {
        this.equipe = equipe;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public Remplacement deleted(Boolean deleted) {
        this.deleted = deleted;
        return this;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Match getMatch() {
        return match;
    }

    public Remplacement match(Match match) {
        this.match = match;
        return this;
    }

    public void setMatch(Match match) {
        this.match = match;
    }

    public Joueur getSortant() {
        return sortant;
    }

    public Remplacement sortant(Joueur joueur) {
        this.sortant = joueur;
        return this;
    }

    public void setSortant(Joueur joueur) {
        this.sortant = joueur;
    }

    public Joueur getEntrant() {
        return entrant;
    }

    public Remplacement entrant(Joueur joueur) {
        this.entrant = joueur;
        return this;
    }

    public void setEntrant(Joueur joueur) {
        this.entrant = joueur;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Remplacement)) {
            return false;
        }
        return id != null && id.equals(((Remplacement) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Remplacement{" +
            "id=" + getId() +
            ", hashcode='" + getHashcode() + "'" +
            ", code='" + getCode() + "'" +
            ", instant=" + getInstant() +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", equipe='" + getEquipe() + "'" +
            ", deleted='" + isDeleted() + "'" +
            "}";
    }
}
