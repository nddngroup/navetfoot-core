package org.nddngroup.navetfoot.core.domain.custom;

/**
 * Created by souleymane91 on 06/01/2019.
 */
public class NotificationObject {
    private String deviceId;
    private String type;
    private Long matchId;
    private String equipe;
    private Integer nbButsLocal;
    private Integer nbButsVisiteur;
    private String joueurPrenom;
    private String joueurNom;
    private String joueurImage;
    private String joueurHashcode;
    private String entrantPrenom;
    private String entrantNom;
    private String entrantImage;
    private String entrantHashcode;
    private String local;
    private String visiteur;
    private String localImage;
    private String visiteurImage;
    private Integer instant;
    private String couleur;
    private String issu;

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getMatchId() {
        return matchId;
    }

    public void setMatchId(Long matchId) {
        this.matchId = matchId;
    }

    public String getEquipe() {
        return equipe;
    }

    public void setEquipe(String equipe) {
        this.equipe = equipe;
    }

    public Integer getNbButsLocal() {
        return nbButsLocal;
    }

    public void setNbButsLocal(Integer nbButsLocal) {
        this.nbButsLocal = nbButsLocal;
    }

    public Integer getNbButsVisiteur() {
        return nbButsVisiteur;
    }

    public void setNbButsVisiteur(Integer nbButsVisiteur) {
        this.nbButsVisiteur = nbButsVisiteur;
    }

    public String getJoueurPrenom() {
        return joueurPrenom;
    }

    public void setJoueurPrenom(String joueurPrenom) {
        this.joueurPrenom = joueurPrenom;
    }

    public String getJoueurNom() {
        return joueurNom;
    }

    public void setJoueurNom(String joueurNom) {
        this.joueurNom = joueurNom;
    }

    public String getJoueurImage() {
        return joueurImage;
    }

    public void setJoueurImage(String joueurImage) {
        this.joueurImage = joueurImage;
    }

    public String getJoueurHashcode() {
        return joueurHashcode;
    }

    public void setJoueurHashcode(String joueurHashcode) {
        this.joueurHashcode = joueurHashcode;
    }

    public String getEntrantPrenom() {
        return entrantPrenom;
    }

    public void setEntrantPrenom(String entrantPrenom) {
        this.entrantPrenom = entrantPrenom;
    }

    public String getEntrantNom() {
        return entrantNom;
    }

    public void setEntrantNom(String entrantNom) {
        this.entrantNom = entrantNom;
    }

    public String getEntrantImage() {
        return entrantImage;
    }

    public void setEntrantImage(String entrantImage) {
        this.entrantImage = entrantImage;
    }

    public String getEntrantHashcode() {
        return entrantHashcode;
    }

    public void setEntrantHashcode(String entrantHashcode) {
        this.entrantHashcode = entrantHashcode;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getVisiteur() {
        return visiteur;
    }

    public void setVisiteur(String visiteur) {
        this.visiteur = visiteur;
    }

    public String getLocalImage() {
        return localImage;
    }

    public void setLocalImage(String localImage) {
        this.localImage = localImage;
    }

    public String getVisiteurImage() {
        return visiteurImage;
    }

    public void setVisiteurImage(String visiteurImage) {
        this.visiteurImage = visiteurImage;
    }

    public Integer getInstant() {
        return instant;
    }

    public void setInstant(Integer instant) {
        this.instant = instant;
    }

    public String getCouleur() {
        return couleur;
    }

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    public String getIssu() {
        return issu;
    }

    public void setIssu(String issu) {
        this.issu = issu;
    }

    @Override
    public String toString() {
        return "NotificationObject{" +
            "deviceId='" + deviceId + '\'' +
            ", type='" + type + '\'' +
            ", matchId=" + matchId +
            ", equipe='" + equipe + '\'' +
            ", nbButsLocal=" + nbButsLocal +
            ", nbButsVisiteur=" + nbButsVisiteur +
            ", joueurPrenom='" + joueurPrenom + '\'' +
            ", joueurNom='" + joueurNom + '\'' +
            ", joueurImage='" + joueurImage + '\'' +
            ", joueurHashcode='" + joueurHashcode + '\'' +
            ", entrantPrenom='" + entrantPrenom + '\'' +
            ", entrantNom='" + entrantNom + '\'' +
            ", entrantImage='" + entrantImage + '\'' +
            ", entrantHashcode='" + entrantHashcode + '\'' +
            ", local='" + local + '\'' +
            ", visiteur='" + visiteur + '\'' +
            ", localImage='" + localImage + '\'' +
            ", visiteurImage='" + visiteurImage + '\'' +
            ", instant=" + instant +
            ", couleur='" + couleur + '\'' +
            ", issu='" + issu + '\'' +
            '}';
    }
}
