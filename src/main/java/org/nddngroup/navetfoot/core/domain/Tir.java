package org.nddngroup.navetfoot.core.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.nddngroup.navetfoot.core.domain.es.TirStats;
import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;

import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * A Tir.
 */
@Entity
@Table(name = "tir")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "tir")
public class Tir extends TirStats implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "code", nullable = false)
    private String code;

    @NotNull
    @Column(name = "hashcode", nullable = false)
    private String hashcode;

    @Column(name = "instant")
    private Integer instant;

    @Column(name = "deleted")
    private Boolean deleted;

    @Column(name = "cadre")
    private Boolean cadre;

    @Column(name = "created_at")
    private ZonedDateTime createdAt;

    @Column(name = "updated_at")
    private ZonedDateTime updatedAt;

    @Enumerated(EnumType.STRING)
    @Column(name = "equipe")
    private Equipe equipe;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "tirs", allowSetters = true)
    private Match match;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "tirs", allowSetters = true)
    private Joueur joueur;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public Tir code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getHashcode() {
        return hashcode;
    }

    public Tir hashcode(String hashcode) {
        this.hashcode = hashcode;
        return this;
    }

    public void setHashcode(String hashcode) {
        this.hashcode = hashcode;
    }

    public Integer getInstant() {
        return instant;
    }

    public Tir instant(Integer instant) {
        this.instant = instant;
        return this;
    }

    public void setInstant(Integer instant) {
        this.instant = instant;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public Tir deleted(Boolean deleted) {
        this.deleted = deleted;
        return this;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Boolean isCadre() {
        return cadre;
    }

    public Tir cadre(Boolean cadre) {
        this.cadre = cadre;
        return this;
    }

    public void setCadre(Boolean cadre) {
        this.cadre = cadre;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public Tir createdAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public Tir updatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Equipe getEquipe() {
        return equipe;
    }

    public Tir equipe(Equipe equipe) {
        this.equipe = equipe;
        return this;
    }

    public void setEquipe(Equipe equipe) {
        this.equipe = equipe;
    }

    public Match getMatch() {
        return match;
    }

    public Tir match(Match match) {
        this.match = match;
        return this;
    }

    public void setMatch(Match match) {
        this.match = match;
    }

    public Joueur getJoueur() {
        return joueur;
    }

    public Tir joueur(Joueur joueur) {
        this.joueur = joueur;
        return this;
    }

    public void setJoueur(Joueur joueur) {
        this.joueur = joueur;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Tir)) {
            return false;
        }
        return id != null && id.equals(((Tir) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Tir{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", hashcode='" + getHashcode() + "'" +
            ", instant=" + getInstant() +
            ", deleted='" + isDeleted() + "'" +
            ", cadre='" + isCadre() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", equipe='" + getEquipe() + "'" +
            "}";
    }
}
