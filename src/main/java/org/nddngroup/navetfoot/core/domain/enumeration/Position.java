package org.nddngroup.navetfoot.core.domain.enumeration;

/**
 * The Position enumeration.
 */
public enum Position {
    TITULAIRE, SUPPLEANT
}
