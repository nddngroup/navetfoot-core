package org.nddngroup.navetfoot.core.domain.custom.stats;

import java.util.List;

public class MatchJournee {
    private int journee;
    private List<MatchDetail> matchs;

    public int getJournee() {
        return journee;
    }

    public void setJournee(int journee) {
        this.journee = journee;
    }

    public List<MatchDetail> getMatchs() {
        return matchs;
    }

    public void setMatchs(List<MatchDetail> matchs) {
        this.matchs = matchs;
    }

    @Override
    public String toString() {
        return "MatchJournee{" +
            "journee=" + journee +
            ", matchs=" + matchs +
            '}';
    }
}
