package org.nddngroup.navetfoot.core.domain.custom;

import org.nddngroup.navetfoot.core.service.dto.CategoryDTO;

public class CategorieItem {
    private Long competitionId;
    private Boolean etat;
    private CategoryDTO categoryDTO;

    public Long getCompetitionId() {
        return competitionId;
    }

    public void setCompetitionId(Long competitionId) {
        this.competitionId = competitionId;
    }

    public Boolean getEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    public CategoryDTO getCategoryDTO() {
        return categoryDTO;
    }

    public void setCategoryDTO(CategoryDTO categoryDTO) {
        this.categoryDTO = categoryDTO;
    }

    @Override
    public String toString() {
        return "CategorieItem{" +
            "competitionId=" + competitionId +
            ", etat=" + etat +
            ", categoryDTO=" + categoryDTO +
            '}';
    }
}
