package org.nddngroup.navetfoot.core.domain.custom.sync;

import org.nddngroup.navetfoot.core.service.dto.PenaltyDTO;

/**
 * Created by souleymane91 on 06/10/2018.
 */
public class CustomPenalty {
    private String matchHashcode;
    private String joueurHashcode;
    private String tirAuButHashcode;
    private PenaltyDTO penaltyDTO;

    public String getMatchHashcode() {
        return matchHashcode;
    }

    public void setMatchHashcode(String matchHashcode) {
        this.matchHashcode = matchHashcode;
    }

    public String getJoueurHashcode() {
        return joueurHashcode;
    }

    public void setJoueurHashcode(String joueurHashcode) {
        this.joueurHashcode = joueurHashcode;
    }

    public String getTirAuButHashcode() {
        return tirAuButHashcode;
    }

    public void setTirAuButHashcode(String tirAuButHashcode) {
        this.tirAuButHashcode = tirAuButHashcode;
    }

    public PenaltyDTO getPenaltyDTO() {
        return penaltyDTO;
    }

    public void setPenaltyDTO(PenaltyDTO penaltyDTO) {
        this.penaltyDTO = penaltyDTO;
    }
}
