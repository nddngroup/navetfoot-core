package org.nddngroup.navetfoot.core.domain.custom;

import java.util.List;

public class ToAddOrRemove {
    List<Long> toAdd;
    List<Long> toRemove;

    public List<Long> getToAdd() {
        return toAdd;
    }

    public void setToAdd(List<Long> toAdd) {
        this.toAdd = toAdd;
    }

    public List<Long> getToRemove() {
        return toRemove;
    }

    public void setToRemove(List<Long> toRemove) {
        this.toRemove = toRemove;
    }

    @Override
    public String toString() {
        return "ToAddOrRemove{" +
            "toAdd=" + toAdd +
            ", toRemove=" + toRemove +
            '}';
    }
}
