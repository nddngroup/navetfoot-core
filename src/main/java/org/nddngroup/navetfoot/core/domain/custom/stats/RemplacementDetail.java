package org.nddngroup.navetfoot.core.domain.custom.stats;

import org.nddngroup.navetfoot.core.service.dto.RemplacementDTO;

import java.io.Serializable;

/**
 * Created by souleymane91 on 10/11/2018.
 */
public class RemplacementDetail implements Serializable {
    private RemplacementDTO remplacementDTO;
    private String prenomEntrant;
    private String nomEntrant;
    private String imageUrlEntrant;
    private String prenomSortant;
    private String nomSortant;
    private String imageUrlSortant;
    private String entrantHashcode;
    private String sortantHashcode;

    public RemplacementDTO getRemplacementDTO() {
        return remplacementDTO;
    }

    public void setRemplacementDTO(RemplacementDTO remplacementDTO) {
        this.remplacementDTO = remplacementDTO;
    }

    public String getPrenomEntrant() {
        return prenomEntrant;
    }

    public void setPrenomEntrant(String prenomEntrant) {
        this.prenomEntrant = prenomEntrant;
    }

    public String getNomEntrant() {
        return nomEntrant;
    }

    public void setNomEntrant(String nomEntrant) {
        this.nomEntrant = nomEntrant;
    }

    public String getImageUrlEntrant() {
        return imageUrlEntrant;
    }

    public void setImageUrlEntrant(String imageUrlEntrant) {
        this.imageUrlEntrant = imageUrlEntrant;
    }

    public String getPrenomSortant() {
        return prenomSortant;
    }

    public void setPrenomSortant(String prenomSortant) {
        this.prenomSortant = prenomSortant;
    }

    public String getNomSortant() {
        return nomSortant;
    }

    public void setNomSortant(String nomSortant) {
        this.nomSortant = nomSortant;
    }

    public String getImageUrlSortant() {
        return imageUrlSortant;
    }

    public void setImageUrlSortant(String imageUrlSortant) {
        this.imageUrlSortant = imageUrlSortant;
    }
    public String getEntrantHashcode() {
        return entrantHashcode;
    }

    public void setEntrantHashcode(String entrantHashcode) {
        this.entrantHashcode = entrantHashcode;
    }

    public String getSortantHashcode() {
        return sortantHashcode;
    }

    public void setSortantHashcode(String sortantHashcode) {
        this.sortantHashcode = sortantHashcode;
    }
}
