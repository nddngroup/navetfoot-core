package org.nddngroup.navetfoot.core.domain.enumeration;

public enum TypeNotif {
    CHRONO, RAPPEL, CONFIRM, BUT, CARTONJAUNE, CARTONROUGE, TIR, PENALTY, PENALTY_SERIE, COUPFRANC, CORNER, HORSJEU, REMPLACEMENT
}
