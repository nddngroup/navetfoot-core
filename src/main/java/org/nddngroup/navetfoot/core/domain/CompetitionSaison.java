package org.nddngroup.navetfoot.core.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * A CompetitionSaison.
 */
@Entity
@Table(name = "competition_saison")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "competitionsaison")
public class CompetitionSaison implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "hashcode")
    private String hashcode;

    @Column(name = "code")
    private String code;

    @Column(name = "created_at")
    private ZonedDateTime createdAt;

    @Column(name = "updated_at")
    private ZonedDateTime updatedAt;

    @Column(name = "deleted")
    private Boolean deleted;

    @OneToMany(mappedBy = "competitionSaison")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<EtapeCompetition> etapeCompetitions = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = "competitionSaisons", allowSetters = true)
    private Competition competition;

    @ManyToOne
    @JsonIgnoreProperties(value = "competitionSaisons", allowSetters = true)
    private Saison saison;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHashcode() {
        return hashcode;
    }

    public CompetitionSaison hashcode(String hashcode) {
        this.hashcode = hashcode;
        return this;
    }

    public void setHashcode(String hashcode) {
        this.hashcode = hashcode;
    }

    public String getCode() {
        return code;
    }

    public CompetitionSaison code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public CompetitionSaison createdAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public CompetitionSaison updatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public CompetitionSaison deleted(Boolean deleted) {
        this.deleted = deleted;
        return this;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Set<EtapeCompetition> getEtapeCompetitions() {
        return etapeCompetitions;
    }

    public CompetitionSaison etapeCompetitions(Set<EtapeCompetition> etapeCompetitions) {
        this.etapeCompetitions = etapeCompetitions;
        return this;
    }

    public CompetitionSaison addEtapeCompetition(EtapeCompetition etapeCompetition) {
        this.etapeCompetitions.add(etapeCompetition);
        etapeCompetition.setCompetitionSaison(this);
        return this;
    }

    public CompetitionSaison removeEtapeCompetition(EtapeCompetition etapeCompetition) {
        this.etapeCompetitions.remove(etapeCompetition);
        etapeCompetition.setCompetitionSaison(null);
        return this;
    }

    public void setEtapeCompetitions(Set<EtapeCompetition> etapeCompetitions) {
        this.etapeCompetitions = etapeCompetitions;
    }

    public Competition getCompetition() {
        return competition;
    }

    public CompetitionSaison competition(Competition competition) {
        this.competition = competition;
        return this;
    }

    public void setCompetition(Competition competition) {
        this.competition = competition;
    }

    public Saison getSaison() {
        return saison;
    }

    public CompetitionSaison saison(Saison saison) {
        this.saison = saison;
        return this;
    }

    public void setSaison(Saison saison) {
        this.saison = saison;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CompetitionSaison)) {
            return false;
        }
        return id != null && id.equals(((CompetitionSaison) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CompetitionSaison{" +
            "id=" + getId() +
            ", hashcode='" + getHashcode() + "'" +
            ", code='" + getCode() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", deleted='" + isDeleted() + "'" +
            "}";
    }
}
