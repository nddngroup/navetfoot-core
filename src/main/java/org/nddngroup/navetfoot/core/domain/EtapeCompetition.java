package org.nddngroup.navetfoot.core.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * A EtapeCompetition.
 */
@Entity
@Table(name = "etape_competition")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "etapecompetition")
public class EtapeCompetition implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "nom")
    private String nom;

    @Column(name = "hashcode")
    private String hashcode;

    @Column(name = "code")
    private String code;

    @Column(name = "date_debut")
    private ZonedDateTime dateDebut;

    @Column(name = "date_fin")
    private ZonedDateTime dateFin;

    @Column(name = "created_at")
    private ZonedDateTime createdAt;

    @Column(name = "updated_at")
    private ZonedDateTime updatedAt;

    @Column(name = "deleted")
    private Boolean deleted;

    @Column(name = "has_poule")
    private Boolean hasPoule;

    @Column(name = "aller_retour")
    private Boolean allerRetour;

    @OneToMany(mappedBy = "etapeCompetition")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Match> matches = new HashSet<>();

    @OneToMany(mappedBy = "etapeCompetition")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Poule> poules = new HashSet<>();

    @OneToMany(mappedBy = "etapeCompetition")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Participation> participations = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = "etapeCompetitions", allowSetters = true)
    private CompetitionSaisonCategory competitionSaisonCategory;

    @ManyToOne
    @JsonIgnoreProperties(value = "etapeCompetitions", allowSetters = true)
    private CompetitionSaison competitionSaison;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public EtapeCompetition nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getHashcode() {
        return hashcode;
    }

    public EtapeCompetition hashcode(String hashcode) {
        this.hashcode = hashcode;
        return this;
    }

    public void setHashcode(String hashcode) {
        this.hashcode = hashcode;
    }

    public String getCode() {
        return code;
    }

    public EtapeCompetition code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public ZonedDateTime getDateDebut() {
        return dateDebut;
    }

    public EtapeCompetition dateDebut(ZonedDateTime dateDebut) {
        this.dateDebut = dateDebut;
        return this;
    }

    public void setDateDebut(ZonedDateTime dateDebut) {
        this.dateDebut = dateDebut;
    }

    public ZonedDateTime getDateFin() {
        return dateFin;
    }

    public EtapeCompetition dateFin(ZonedDateTime dateFin) {
        this.dateFin = dateFin;
        return this;
    }

    public void setDateFin(ZonedDateTime dateFin) {
        this.dateFin = dateFin;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public EtapeCompetition createdAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public EtapeCompetition updatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public EtapeCompetition deleted(Boolean deleted) {
        this.deleted = deleted;
        return this;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Boolean isHasPoule() {
        return hasPoule;
    }

    public EtapeCompetition hasPoule(Boolean hasPoule) {
        this.hasPoule = hasPoule;
        return this;
    }

    public void setHasPoule(Boolean hasPoule) {
        this.hasPoule = hasPoule;
    }

    public Boolean isAllerRetour() {
        return allerRetour;
    }

    public EtapeCompetition allerRetour(Boolean allerRetour) {
        this.allerRetour = allerRetour;
        return this;
    }

    public void setAllerRetour(Boolean allerRetour) {
        this.allerRetour = allerRetour;
    }

    public Set<Match> getMatches() {
        return matches;
    }

    public EtapeCompetition matches(Set<Match> matches) {
        this.matches = matches;
        return this;
    }

    public EtapeCompetition addMatch(Match match) {
        this.matches.add(match);
        match.setEtapeCompetition(this);
        return this;
    }

    public EtapeCompetition removeMatch(Match match) {
        this.matches.remove(match);
        match.setEtapeCompetition(null);
        return this;
    }

    public void setMatches(Set<Match> matches) {
        this.matches = matches;
    }

    public Set<Poule> getPoules() {
        return poules;
    }

    public EtapeCompetition poules(Set<Poule> poules) {
        this.poules = poules;
        return this;
    }

    public EtapeCompetition addPoule(Poule poule) {
        this.poules.add(poule);
        poule.setEtapeCompetition(this);
        return this;
    }

    public EtapeCompetition removePoule(Poule poule) {
        this.poules.remove(poule);
        poule.setEtapeCompetition(null);
        return this;
    }

    public void setPoules(Set<Poule> poules) {
        this.poules = poules;
    }

    public Set<Participation> getParticipations() {
        return participations;
    }

    public EtapeCompetition participations(Set<Participation> participations) {
        this.participations = participations;
        return this;
    }

    public EtapeCompetition addParticipation(Participation participation) {
        this.participations.add(participation);
        participation.setEtapeCompetition(this);
        return this;
    }

    public EtapeCompetition removeParticipation(Participation participation) {
        this.participations.remove(participation);
        participation.setEtapeCompetition(null);
        return this;
    }

    public void setParticipations(Set<Participation> participations) {
        this.participations = participations;
    }

    public CompetitionSaisonCategory getCompetitionSaisonCategory() {
        return competitionSaisonCategory;
    }

    public EtapeCompetition competitionSaisonCategory(CompetitionSaisonCategory competitionSaisonCategory) {
        this.competitionSaisonCategory = competitionSaisonCategory;
        return this;
    }

    public void setCompetitionSaisonCategory(CompetitionSaisonCategory competitionSaisonCategory) {
        this.competitionSaisonCategory = competitionSaisonCategory;
    }

    public CompetitionSaison getCompetitionSaison() {
        return competitionSaison;
    }

    public EtapeCompetition competitionSaison(CompetitionSaison competitionSaison) {
        this.competitionSaison = competitionSaison;
        return this;
    }

    public void setCompetitionSaison(CompetitionSaison competitionSaison) {
        this.competitionSaison = competitionSaison;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EtapeCompetition)) {
            return false;
        }
        return id != null && id.equals(((EtapeCompetition) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "EtapeCompetition{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", hashcode='" + getHashcode() + "'" +
            ", code='" + getCode() + "'" +
            ", dateDebut='" + getDateDebut() + "'" +
            ", dateFin='" + getDateFin() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", deleted='" + isDeleted() + "'" +
            ", hasPoule='" + isHasPoule() + "'" +
            ", allerRetour='" + isAllerRetour() + "'" +
            "}";
    }
}
