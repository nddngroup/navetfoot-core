package org.nddngroup.navetfoot.core.domain.custom;

import java.io.Serializable;
import java.time.ZonedDateTime;

public class Abonnement implements Serializable {
    private Long id;

    private String deviceId;

    private Long matchId;

    private Boolean but;

    private Boolean cartonRouge;

    private Boolean cartonJaune;

    private Boolean penalty;

    private Boolean remplacement;

    private Boolean coupFranc;

    private Boolean corner;

    private ZonedDateTime createdAt;

    private ZonedDateTime updatedAt;

    private Boolean deleted;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public Long getMatchId() {
        return matchId;
    }

    public void setMatchId(Long matchId) {
        this.matchId = matchId;
    }

    public Boolean getBut() {
        return but;
    }

    public void setBut(Boolean but) {
        this.but = but;
    }

    public Boolean getCartonRouge() {
        return cartonRouge;
    }

    public void setCartonRouge(Boolean cartonRouge) {
        this.cartonRouge = cartonRouge;
    }

    public Boolean getCartonJaune() {
        return cartonJaune;
    }

    public void setCartonJaune(Boolean cartonJaune) {
        this.cartonJaune = cartonJaune;
    }

    public Boolean getPenalty() {
        return penalty;
    }

    public void setPenalty(Boolean penalty) {
        this.penalty = penalty;
    }

    public Boolean getRemplacement() {
        return remplacement;
    }

    public void setRemplacement(Boolean remplacement) {
        this.remplacement = remplacement;
    }

    public Boolean getCoupFranc() {
        return coupFranc;
    }

    public void setCoupFranc(Boolean coupFranc) {
        this.coupFranc = coupFranc;
    }

    public Boolean getCorner() {
        return corner;
    }

    public void setCorner(Boolean corner) {
        this.corner = corner;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "Abonnement{" +
            "id=" + id +
            ", deviceId='" + deviceId + '\'' +
            ", matchId=" + matchId +
            ", but=" + but +
            ", cartonRouge=" + cartonRouge +
            ", cartonJaune=" + cartonJaune +
            ", penalty=" + penalty +
            ", remplacement=" + remplacement +
            ", coupFranc=" + coupFranc +
            ", corner=" + corner +
            ", createdAt=" + createdAt +
            ", updatedAt=" + updatedAt +
            ", deleted=" + deleted +
            '}';
    }
}
