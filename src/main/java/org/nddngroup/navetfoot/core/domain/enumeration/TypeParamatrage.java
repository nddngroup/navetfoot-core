package org.nddngroup.navetfoot.core.domain.enumeration;

/**
 * The TypeParamatrage enumeration.
 */
public enum TypeParamatrage {
    VALEUR, SELECT, RADIO, SWITCH
}
