package org.nddngroup.navetfoot.core.domain.custom.sync;

import org.nddngroup.navetfoot.core.service.dto.CornerDTO;

/**
 * Created by souleymane91 on 06/10/2018.
 */
public class CustomCorner {
    private String matchHashcode;
    private CornerDTO cornerDTO;

    public String getMatchHashcode() {
        return matchHashcode;
    }

    public void setMatchHashcode(String matchHashcode) {
        this.matchHashcode = matchHashcode;
    }

    public CornerDTO getCornerDTO() {
        return cornerDTO;
    }

    public void setCornerDTO(CornerDTO cornerDTO) {
        this.cornerDTO = cornerDTO;
    }
}
