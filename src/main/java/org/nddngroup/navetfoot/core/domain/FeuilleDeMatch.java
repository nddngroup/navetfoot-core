package org.nddngroup.navetfoot.core.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * A FeuilleDeMatch.
 */
@Entity
@Table(name = "feuille_de_match")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "feuilledematch")
public class FeuilleDeMatch implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "code")
    private String code;

    @Column(name = "hashcode")
    private String hashcode;

    @Column(name = "updated_at")
    private ZonedDateTime updatedAt;

    @Column(name = "created_at")
    private ZonedDateTime createdAt;

    @Column(name = "deleted")
    private Boolean deleted;

    @OneToOne
    @JoinColumn(unique = true)
    private Match match;

    @OneToMany(mappedBy = "feuilleDeMatch")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<DetailFeuilleDeMatch> detailFeuilleDeMatchs = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public FeuilleDeMatch code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getHashcode() {
        return hashcode;
    }

    public FeuilleDeMatch hashcode(String hashcode) {
        this.hashcode = hashcode;
        return this;
    }

    public void setHashcode(String hashcode) {
        this.hashcode = hashcode;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public FeuilleDeMatch updatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public FeuilleDeMatch createdAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public FeuilleDeMatch deleted(Boolean deleted) {
        this.deleted = deleted;
        return this;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Match getMatch() {
        return match;
    }

    public FeuilleDeMatch match(Match match) {
        this.match = match;
        return this;
    }

    public void setMatch(Match match) {
        this.match = match;
    }

    public Set<DetailFeuilleDeMatch> getDetailFeuilleDeMatchs() {
        return detailFeuilleDeMatchs;
    }

    public FeuilleDeMatch detailFeuilleDeMatchs(Set<DetailFeuilleDeMatch> detailFeuilleDeMatches) {
        this.detailFeuilleDeMatchs = detailFeuilleDeMatches;
        return this;
    }

    public FeuilleDeMatch addDetailFeuilleDeMatchs(DetailFeuilleDeMatch detailFeuilleDeMatch) {
        this.detailFeuilleDeMatchs.add(detailFeuilleDeMatch);
        detailFeuilleDeMatch.setFeuilleDeMatch(this);
        return this;
    }

    public FeuilleDeMatch removeDetailFeuilleDeMatchs(DetailFeuilleDeMatch detailFeuilleDeMatch) {
        this.detailFeuilleDeMatchs.remove(detailFeuilleDeMatch);
        detailFeuilleDeMatch.setFeuilleDeMatch(null);
        return this;
    }

    public void setDetailFeuilleDeMatchs(Set<DetailFeuilleDeMatch> detailFeuilleDeMatches) {
        this.detailFeuilleDeMatchs = detailFeuilleDeMatches;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FeuilleDeMatch)) {
            return false;
        }
        return id != null && id.equals(((FeuilleDeMatch) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FeuilleDeMatch{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", hashcode='" + getHashcode() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", deleted='" + isDeleted() + "'" +
            "}";
    }
}
