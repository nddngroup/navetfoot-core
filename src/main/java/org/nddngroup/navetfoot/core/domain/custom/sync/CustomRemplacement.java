package org.nddngroup.navetfoot.core.domain.custom.sync;

import org.nddngroup.navetfoot.core.service.dto.RemplacementDTO;

/**
 * Created by souleymane91 on 06/10/2018.
 */
public class CustomRemplacement {
    private String matchHashcode;
    private String entrantHashcode;
    private String sortantHashcode;
    private RemplacementDTO remplacementDTO;

    public String getMatchHashcode() {
        return matchHashcode;
    }

    public void setMatchHashcode(String matchHashcode) {
        this.matchHashcode = matchHashcode;
    }

    public String getEntrantHashcode() {
        return entrantHashcode;
    }

    public void setEntrantHashcode(String entrantHashcode) {
        this.entrantHashcode = entrantHashcode;
    }

    public String getSortantHashcode() {
        return sortantHashcode;
    }

    public void setSortantHashcode(String sortantHashcode) {
        this.sortantHashcode = sortantHashcode;
    }

    public RemplacementDTO getRemplacementDTO() {
        return remplacementDTO;
    }

    public void setRemplacementDTO(RemplacementDTO remplacementDTO) {
        this.remplacementDTO = remplacementDTO;
    }
}
