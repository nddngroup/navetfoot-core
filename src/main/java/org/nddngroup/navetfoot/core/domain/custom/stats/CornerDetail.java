package org.nddngroup.navetfoot.core.domain.custom.stats;

import org.nddngroup.navetfoot.core.service.dto.CornerDTO;

import java.io.Serializable;

/**
 * Created by souleymane91 on 10/11/2018.
 */
public class CornerDetail implements Serializable {
    private CornerDTO cornerDTO;

    public CornerDTO getCornerDTO() {
        return cornerDTO;
    }

    public void setCornerDTO(CornerDTO cornerDTO) {
        this.cornerDTO = cornerDTO;
    }
}
