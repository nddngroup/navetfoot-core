package org.nddngroup.navetfoot.core.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.nddngroup.navetfoot.core.domain.enumeration.Poste;
import org.nddngroup.navetfoot.core.domain.es.DetailFeuilleDeMatchStats;
import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;
import org.nddngroup.navetfoot.core.domain.enumeration.Position;

import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * A DetailFeuilleDeMatch.
 */
@Entity
@Table(name = "detail_feuille_de_match")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "detailfeuilledematch")
public class DetailFeuilleDeMatch extends DetailFeuilleDeMatchStats implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "equipe")
    private Equipe equipe;

    @Column(name = "numero_joueur")
    private Integer numeroJoueur;

    @Column(name = "code")
    private String code;

    @Column(name = "hashcode")
    private String hashcode;

    @Column(name = "updated_at")
    private ZonedDateTime updatedAt;

    @Column(name = "created_at")
    private ZonedDateTime createdAt;

    @Column(name = "deleted")
    private Boolean deleted;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "position", nullable = false)
    private Position position;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "poste", nullable = false)
    private Poste poste = Poste.NONE;

    @Column(name = "grille")
    private String grille;

    @ManyToOne
    @JsonIgnoreProperties(value = "detailFeuilleDeMatches", allowSetters = true)
    private Joueur joueur;

    @ManyToOne
    @JsonIgnoreProperties(value = "detailFeuilleDeMatchs", allowSetters = true)
    private FeuilleDeMatch feuilleDeMatch;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Equipe getEquipe() {
        return equipe;
    }

    public DetailFeuilleDeMatch equipe(Equipe equipe) {
        this.equipe = equipe;
        return this;
    }

    public void setEquipe(Equipe equipe) {
        this.equipe = equipe;
    }

    public Integer getNumeroJoueur() {
        return numeroJoueur;
    }

    public DetailFeuilleDeMatch numeroJoueur(Integer numeroJoueur) {
        this.numeroJoueur = numeroJoueur;
        return this;
    }

    public void setNumeroJoueur(Integer numeroJoueur) {
        this.numeroJoueur = numeroJoueur;
    }

    public String getCode() {
        return code;
    }

    public DetailFeuilleDeMatch code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getHashcode() {
        return hashcode;
    }

    public DetailFeuilleDeMatch hashcode(String hashcode) {
        this.hashcode = hashcode;
        return this;
    }

    public void setHashcode(String hashcode) {
        this.hashcode = hashcode;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public DetailFeuilleDeMatch updatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public DetailFeuilleDeMatch createdAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public DetailFeuilleDeMatch deleted(Boolean deleted) {
        this.deleted = deleted;
        return this;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Position getPosition() {
        return position;
    }

    public DetailFeuilleDeMatch position(Position position) {
        this.position = position;
        return this;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Poste getPoste() {
        return poste;
    }

    public DetailFeuilleDeMatch poste(Poste poste) {
        this.poste = poste;
        return this;
    }

    public void setPoste(Poste poste) {
        this.poste = poste;
    }

    public String getGrille() {
        return grille;
    }

    public DetailFeuilleDeMatch grille(String grille) {
        this.grille = grille;
        return this;
    }

    public void setGrille(String grille) {
        this.grille = grille;
    }

    public Joueur getJoueur() {
        return joueur;
    }

    public DetailFeuilleDeMatch joueur(Joueur joueur) {
        this.joueur = joueur;
        return this;
    }

    public void setJoueur(Joueur joueur) {
        this.joueur = joueur;
    }

    public FeuilleDeMatch getFeuilleDeMatch() {
        return feuilleDeMatch;
    }

    public DetailFeuilleDeMatch feuilleDeMatch(FeuilleDeMatch feuilleDeMatch) {
        this.feuilleDeMatch = feuilleDeMatch;
        return this;
    }

    public void setFeuilleDeMatch(FeuilleDeMatch feuilleDeMatch) {
        this.feuilleDeMatch = feuilleDeMatch;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DetailFeuilleDeMatch)) {
            return false;
        }
        return id != null && id.equals(((DetailFeuilleDeMatch) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DetailFeuilleDeMatch{" +
            "id=" + getId() +
            ", equipe='" + getEquipe() + "'" +
            ", numeroJoueur=" + getNumeroJoueur() +
            ", code='" + getCode() + "'" +
            ", hashcode='" + getHashcode() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", deleted='" + isDeleted() + "'" +
            ", position='" + getPosition() + "'" +
            ", poste='" + getPoste() + "'" +
            ", grille='" + getGrille() + "'" +
            "}";
    }
}
