package org.nddngroup.navetfoot.core.domain.custom.sync;

import org.nddngroup.navetfoot.core.service.dto.JoueurDTO;

/**
 * Created by souleymane91 on 06/10/2018.
 */
public class CustomJoueur {
    private JoueurDTO joueurDTO;
    private String contratHashcode;
    private String contratCode;
    private Long saisonId;
    private Long associationId;

    public JoueurDTO getJoueurDTO() {
        return joueurDTO;
    }

    public void setJoueurDTO(JoueurDTO joueurDTO) {
        this.joueurDTO = joueurDTO;
    }

    public String getContratHashcode() {
        return this.contratHashcode;
    }

    public void setContratHashcode(String value) {
        this.contratHashcode = value;
    }

    public String getContratCode() {
        return this.contratCode;
    }

    public void setContratCode(String value) {
        this.contratCode = value;
    }

    public Long getSaisonId() {
        return this.saisonId;
    }

    public void setSaisonId(Long value) {
        this.saisonId = value;
    }

    public Long getAssociationId() {
        return this.associationId;
    }

    public void setAssociationId(Long value) {
        this.associationId = value;
    }
}
