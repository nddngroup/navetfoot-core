package org.nddngroup.navetfoot.core.domain.custom;

public class CustomOption {
    private String libelle;
    private String value;

    public CustomOption(String libelle, String value) {
        this.libelle = libelle;
        this.value = value;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
