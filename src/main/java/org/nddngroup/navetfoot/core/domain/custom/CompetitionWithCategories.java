package org.nddngroup.navetfoot.core.domain.custom;

import org.nddngroup.navetfoot.core.service.dto.CategoryDTO;
import org.nddngroup.navetfoot.core.service.dto.CompetitionDTO;

import java.util.List;

public class CompetitionWithCategories {
    private CompetitionDTO competition;
    private List<CategoryDTO> categories;

    public CompetitionDTO getCompetition() {
        return competition;
    }

    public void setCompetition(CompetitionDTO competition) {
        this.competition = competition;
    }

    public List<CategoryDTO> getCategories() {
        return categories;
    }

    public void setCategories(List<CategoryDTO> categories) {
        this.categories = categories;
    }

    @Override
    public String toString() {
        return "CompetitionWithCategories{" +
            "competition=" + competition +
            ", categories=" + categories +
            '}';
    }
}
