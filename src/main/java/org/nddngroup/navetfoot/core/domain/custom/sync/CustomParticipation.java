package org.nddngroup.navetfoot.core.domain.custom.sync;

import org.nddngroup.navetfoot.core.service.dto.ParticipationDTO;

/**
 * Created by souleymane91 on 07/10/2018.
 */
public class CustomParticipation {
    private String etapeCompetitionHashcode;
    private String associationHashcode;
    private String saisonHashcode;
    private ParticipationDTO participationDTO;

    public String getEtapeCompetitionHashcode() {
        return etapeCompetitionHashcode;
    }

    public void setEtapeCompetitionHashcode(String etapeCompetitionHashcode) {
        this.etapeCompetitionHashcode = etapeCompetitionHashcode;
    }

    public String getAssociationHashcode() {
        return associationHashcode;
    }

    public void setAssociationHashcode(String associationHashcode) {
        this.associationHashcode = associationHashcode;
    }

    public String getSaisonHashcode() {
        return saisonHashcode;
    }

    public void setSaisonHashcode(String saisonHashcode) {
        this.saisonHashcode = saisonHashcode;
    }

    public ParticipationDTO getParticipationDTO() {
        return participationDTO;
    }

    public void setParticipationDTO(ParticipationDTO participationDTO) {
        this.participationDTO = participationDTO;
    }
}
