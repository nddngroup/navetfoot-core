package org.nddngroup.navetfoot.core.domain.custom.sync;

import org.nddngroup.navetfoot.core.service.dto.MatchDTO;

/**
 * Created by souleymane91 on 06/10/2018.
 */
public class CustomMatch {
    private String etapeCompetitionHashcode;
    private String localHashcode;
    private String visiteurHashcode;
    private String tirAuButHashcode;
    private MatchDTO matchDTO;

    public String getEtapeCompetitionHashcode() {
        return etapeCompetitionHashcode;
    }

    public void setEtapeCompetitionHashcode(String etapeCompetitionHashcode) {
        this.etapeCompetitionHashcode = etapeCompetitionHashcode;
    }

    public String getLocalHashcode() {
        return localHashcode;
    }

    public void setLocalHashcode(String localHashcode) {
        this.localHashcode = localHashcode;
    }

    public String getVisiteurHashcode() {
        return visiteurHashcode;
    }

    public void setVisiteurHashcode(String visiteurHashcode) {
        this.visiteurHashcode = visiteurHashcode;
    }

    public String getTirAuButHashcode() {
        return tirAuButHashcode;
    }

    public void setTirAuButHashcode(String tirAuButHashcode) {
        this.tirAuButHashcode = tirAuButHashcode;
    }

    public MatchDTO getMatchDTO() {
        return matchDTO;
    }

    public void setMatchDTO(MatchDTO matchDTO) {
        this.matchDTO = matchDTO;
    }
}
