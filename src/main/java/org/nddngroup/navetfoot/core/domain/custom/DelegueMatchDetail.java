package org.nddngroup.navetfoot.core.domain.custom;

import org.nddngroup.navetfoot.core.domain.custom.stats.EventsDetail;
import org.nddngroup.navetfoot.core.domain.custom.stats.FeuilleDeMatchDetail;
import org.nddngroup.navetfoot.core.domain.custom.stats.MatchDetail;
import org.nddngroup.navetfoot.core.service.dto.DelegueDTO;
import org.nddngroup.navetfoot.core.service.dto.JoueurDTO;

import java.io.Serializable;
import java.util.List;

public class DelegueMatchDetail implements Serializable {
    Long id;
    String code;
    MatchDetail match;
    DelegueDTO delegue;
    FeuilleDeMatchDetail feuilleDeMatch;
    List<JoueurDTO> localJoueurs;
    List<JoueurDTO> visiteurJoueurs;
    EventsDetail events;
    Long organisationId;
    Long saisonId;
    Long competitionId;
    Long localId;
    Long visiteurId;
    String saisonHashcode;
    String competitionHashcode;
    String saisonNom;
    String competitionNom;
    String etapeNom;

    public DelegueMatchDetail() {}

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public DelegueDTO getDelegue() {
        return this.delegue;
    }

    public void setDelegue(DelegueDTO delegue) {
        this.delegue = delegue;
    }

    public MatchDetail getMatch() {
        return this.match;
    }

    public void setMatch(MatchDetail match) {
        this.match = match;
    }

    public FeuilleDeMatchDetail getFeuilleDeMatch() {
        return this.feuilleDeMatch;
    }

    public void setFeuilleDeMatch(FeuilleDeMatchDetail feuilleDeMatch) {
        this.feuilleDeMatch = feuilleDeMatch;
    }

    public List<JoueurDTO> getLocalJoueurs() {
        return this.localJoueurs;
    }

    public void setLocalJoueurs(List<JoueurDTO> localJoueurs) {
        this.localJoueurs = localJoueurs;
    }

    public List<JoueurDTO> getVisiteurJoueurs() {
        return this.visiteurJoueurs;
    }

    public void setVisiteurJoueurs(List<JoueurDTO> visiteurJoueurs) {
        this.visiteurJoueurs = visiteurJoueurs;
    }

    public Long getOrganisationId() {
        return this.organisationId;
    }

    public void setOrganisationId(Long value) {
        this.organisationId = value;
    }

    public Long getSaisonId() {
        return this.saisonId;
    }

    public void setSaisonId(Long value) {
        this.saisonId = value;
    }

    public Long getCompetitionId() {
        return this.competitionId;
    }

    public void setCompetitionId(Long value) {
        this.competitionId = value;
    }

    public Long getLocalId() {
        return this.localId;
    }

    public void setLocalId(Long value) {
        this.localId= value;
    }

    public Long getVisiteurId() {
        return this.visiteurId;
    }

    public void setVisiteurId(Long value) {
        this.visiteurId= value;
    }

    public String getSaisonHashcode() {
        return this.saisonHashcode;
    }

    public void setSaisonHashcode(String value) {
        this.saisonHashcode = value;
    }

    public String getCompetitionHashcode() {
        return this.competitionHashcode;
    }

    public void setCompetitionHashcode(String value) {
        this.competitionHashcode = value;
    }

    public String getSaisonNom() {
        return this.saisonNom;
    }

    public void setSaisonNom(String value) {
        this.saisonNom = value;
    }

    public String getCompetitionNom() {
        return this.competitionNom;
    }

    public void setCompetitionNom(String value) {
        this.competitionNom = value;
    }

    public String getEtapeNom() {
        return this.etapeNom;
    }

    public void setEtapeNom(String value) {
        this.etapeNom= value;
    }

    public EventsDetail getEvents() {
        return this.events;
    }

    public void setEvents(EventsDetail events) {
        this.events = events;
    }
}
