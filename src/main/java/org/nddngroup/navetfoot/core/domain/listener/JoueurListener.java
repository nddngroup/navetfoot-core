package org.nddngroup.navetfoot.core.domain.listener;

import org.nddngroup.navetfoot.core.domain.Joueur;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PostPersist;

@Component
public class JoueurListener {
    @PersistenceContext
    private EntityManager entityManager;

    @PostPersist
    public void setCodePostPersist(Joueur joueur) {
        Long id = joueur.getId();
        String code = joueur.getCode();

        if (id != null) {
            code = code != null ? code + "_" + id : "REF_" + id;
        }

        joueur.setCode(code);

        System.out.println("$$$$$$$$$$$$$ edit code after persist : id = " + id + ", code = " + code);

        // entityManager.flush();
    }
}
