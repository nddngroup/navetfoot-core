package org.nddngroup.navetfoot.core.domain.custom.sync;

import org.nddngroup.navetfoot.core.service.dto.AssociationDTO;

/**
 * Created by souleymane91 on 06/10/2018.
 */
public class CustomAssociation {
    private AssociationDTO associationDTO;

    public AssociationDTO getAssociationDTO() {
        return associationDTO;
    }

    public void setAssociationDTO(AssociationDTO association) {
        this.associationDTO = association;
    }
}
