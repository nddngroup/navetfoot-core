package org.nddngroup.navetfoot.core.domain.websocket;

import org.nddngroup.navetfoot.core.domain.enumeration.websocket.RabbitMQMessageType;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.UUID;

public class RabbitMQMessage implements Serializable {
    private static final long serialVersionUID = 1L;

    private final String messageId;
    private final ZonedDateTime messageDate;
    private RabbitMQMessageType messageType;
    private Object body;

    public String getMessageId() {
        return messageId;
    }

    public ZonedDateTime getMessageDate() {
        return messageDate;
    }

    public RabbitMQMessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(RabbitMQMessageType messageType) {
        this.messageType = messageType;
    }

    public Object getBody() {
        return body;
    }

    public void setBody(Object body) {
        this.body = body;
    }

    public RabbitMQMessage() {
        this.messageId = UUID.randomUUID().toString();
        this.messageDate = ZonedDateTime.now();
    }

    public RabbitMQMessage body(Object body) {
        this.body = body;
        return this;
    }

    public static RabbitMQMessage build(Object body, RabbitMQMessageType type) {
        var message = new RabbitMQMessage();
        message.body(body);
        message.setMessageType(type);
        return message;
    }

    @Override
    public String toString() {
        return "RabbitMQMessage{" +
            "messageId='" + messageId + '\'' +
            ", messageDate=" + messageDate +
            ", body=" + body +
            '}';
    }
}
