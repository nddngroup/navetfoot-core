package org.nddngroup.navetfoot.core.domain.custom.stats;

import java.io.Serializable;
import java.util.List;

/**
 * Created by souleymane91 on 10/11/2018.
 */
public class EventsDetail implements Serializable {
    private List<ButDetail> butsDetail;
    private List<CartonDetail> cartonsDetail;
    private List<CoupFrancDetail> coupFrancsDetail;
    private List<CornerDetail> cornersDetail;
    private List<PenaltyDetail> penaltiesDetail;
    private List<RemplacementDetail> remplacementsDetail;
    private List<HorsJeuDetail> horsJeusDetail;
    private List<TirDetail> tirsDetail;

    public List<ButDetail> getButsDetail() {
        return butsDetail;
    }

    public void setButsDetail(List<ButDetail> butDetail) {
        this.butsDetail = butDetail;
    }

    public List<CartonDetail> getCartonsDetail() {
        return cartonsDetail;
    }

    public void setCartonsDetail(List<CartonDetail> cartonDetail) {
        this.cartonsDetail = cartonDetail;
    }

    public List<CoupFrancDetail> getCoupFrancsDetail() {
        return coupFrancsDetail;
    }

    public void setCoupFrancsDetail(List<CoupFrancDetail> coupFrancDetail) {
        this.coupFrancsDetail = coupFrancDetail;
    }

    public List<CornerDetail> getCornersDetail() {
        return cornersDetail;
    }

    public void setCornersDetail(List<CornerDetail> cornerDetail) {
        this.cornersDetail = cornerDetail;
    }

    public List<PenaltyDetail> getPenaltiesDetail() {
        return penaltiesDetail;
    }

    public void setPenaltiesDetail(List<PenaltyDetail> penaltyDetail) {
        this.penaltiesDetail = penaltyDetail;
    }

    public List<RemplacementDetail> getRemplacementsDetail() {
        return remplacementsDetail;
    }

    public void setRemplacementsDetail(List<RemplacementDetail> remplacementDetail) {
        this.remplacementsDetail = remplacementDetail;
    }

    public List<HorsJeuDetail> getHorsJeusDetail() {
        return horsJeusDetail;
    }

    public void setHorsJeusDetail(List<HorsJeuDetail> horsJeusDetail) {
        this.horsJeusDetail = horsJeusDetail;
    }

    public List<TirDetail> getTirsDetail() {
        return tirsDetail;
    }

    public void setTirsDetail(List<TirDetail> tirsDetail) {
        this.tirsDetail = tirsDetail;
    }
}
