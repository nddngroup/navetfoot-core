package org.nddngroup.navetfoot.core.domain.custom;

import java.util.List;

public class CompetitionToEdit {
    private String type; // add or remove
    private Long competitionId;
    private List<Long> categoryToAdd;
    private List<Long> categoryToRemove;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getCompetitionId() {
        return competitionId;
    }

    public void setCompetitionId(Long competitionId) {
        this.competitionId = competitionId;
    }

    public List<Long> getCategoryToAdd() {
        return categoryToAdd;
    }

    public void setCategoryToAdd(List<Long> categoryToAdd) {
        this.categoryToAdd = categoryToAdd;
    }

    public List<Long> getCategoryToRemove() {
        return categoryToRemove;
    }

    public void setCategoryToRemove(List<Long> categoryToRemove) {
        this.categoryToRemove = categoryToRemove;
    }

    @Override
    public String toString() {
        return "CompetitionToEdit{" +
            "type='" + type + '\'' +
            ", competitionId=" + competitionId +
            ", categoryToAdd=" + categoryToAdd +
            ", categoryToRemove=" + categoryToRemove +
            '}';
    }
}
