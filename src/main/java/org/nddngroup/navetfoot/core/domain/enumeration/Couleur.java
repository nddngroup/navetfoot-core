package org.nddngroup.navetfoot.core.domain.enumeration;

/**
 * The Couleur enumeration.
 */
public enum Couleur {
    JAUNE, ROUGE
}
