package org.nddngroup.navetfoot.core.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * A TirAuBut.
 */
@Entity
@Table(name = "tir_au_but")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "tiraubut")
public class TirAuBut implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "code")
    private String code;

    @Column(name = "hashcode")
    private String hashcode;

    @Column(name = "created_at")
    private ZonedDateTime createdAt;

    @Column(name = "updated_at")
    private ZonedDateTime updatedAt;

    @Column(name = "deleted")
    private Boolean deleted;

    @OneToOne
    @JoinColumn(unique = true)
    private Match match;

    @OneToMany(mappedBy = "tirAuBut")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Penalty> penalties = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public TirAuBut code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getHashcode() {
        return hashcode;
    }

    public TirAuBut hashcode(String hashcode) {
        this.hashcode = hashcode;
        return this;
    }

    public void setHashcode(String hashcode) {
        this.hashcode = hashcode;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public TirAuBut createdAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public TirAuBut updatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public TirAuBut deleted(Boolean deleted) {
        this.deleted = deleted;
        return this;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Match getMatch() {
        return match;
    }

    public TirAuBut match(Match match) {
        this.match = match;
        return this;
    }

    public void setMatch(Match match) {
        this.match = match;
    }

    public Set<Penalty> getPenalties() {
        return penalties;
    }

    public TirAuBut penalties(Set<Penalty> penalties) {
        this.penalties = penalties;
        return this;
    }

    public TirAuBut addPenalties(Penalty penalty) {
        this.penalties.add(penalty);
        penalty.setTirAuBut(this);
        return this;
    }

    public TirAuBut removePenalties(Penalty penalty) {
        this.penalties.remove(penalty);
        penalty.setTirAuBut(null);
        return this;
    }

    public void setPenalties(Set<Penalty> penalties) {
        this.penalties = penalties;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TirAuBut)) {
            return false;
        }
        return id != null && id.equals(((TirAuBut) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TirAuBut{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", hashcode='" + getHashcode() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", deleted='" + isDeleted() + "'" +
            "}";
    }
}
