package org.nddngroup.navetfoot.core.domain.custom.stats;

import java.io.Serializable;

/**
 * Created by souleymane91 on 02/08/2019.
 */
public class Stat implements Serializable, Comparable<Stat> {
    private String critere; // ex. club, joueur, ...
    private Long id;
    private String reference;
    private String libelle;
    private String type;    // ex. but, cartonJaune, cartonRouge, ...
    private int total;

    public String getCritere() {
        return critere;
    }

    public void setCritere(String critere) {
        this.critere = critere;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public int compareTo(Stat stat) {
        if (this.getTotal() > stat.getTotal()) {
            return 1;
        }
        else if (this.getTotal() < stat.getTotal()) {
            return -1;
        }

        return 0;
    }
}
