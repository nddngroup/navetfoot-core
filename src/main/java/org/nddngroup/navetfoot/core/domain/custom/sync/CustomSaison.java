package org.nddngroup.navetfoot.core.domain.custom.sync;

import org.nddngroup.navetfoot.core.service.dto.SaisonDTO;

/**
 * Created by souleymane91 on 06/10/2018.
 */
public class CustomSaison {
    private String organisationHashcode;
    private SaisonDTO saisonDTO;

    public String getOrganisationHashcode() {
        return organisationHashcode;
    }

    public void setOrganisationHashcode(String organisationHashcode) {
        this.organisationHashcode = organisationHashcode;
    }

    public SaisonDTO getSaisonDTO() {
        return saisonDTO;
    }

    public void setSaisonDTO(SaisonDTO saisonDTO) {
        this.saisonDTO = saisonDTO;
    }
}
