package org.nddngroup.navetfoot.core.domain.custom.sync;

import org.nddngroup.navetfoot.core.service.dto.CartonDTO;

/**
 * Created by souleymane91 on 06/10/2018.
 */
public class CustomCarton {
    private String matchHashcode;
    private String joueurHashcode;
    private CartonDTO cartonDTO;

    public String getMatchHashcode() {
        return matchHashcode;
    }

    public void setMatchHashcode(String matchHashcode) {
        this.matchHashcode = matchHashcode;
    }

    public String getJoueurHashcode() {
        return joueurHashcode;
    }

    public void setJoueurHashcode(String joueurHashcode) {
        this.joueurHashcode = joueurHashcode;
    }

    public CartonDTO getCartonDTO() {
        return cartonDTO;
    }

    public void setCartonDTO(CartonDTO cartonDTO) {
        this.cartonDTO = cartonDTO;
    }
}
