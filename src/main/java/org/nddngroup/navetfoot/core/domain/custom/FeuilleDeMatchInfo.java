package org.nddngroup.navetfoot.core.domain.custom;

public class FeuilleDeMatchInfo {
    private AssociationFeuilleDeMatch local;
    private AssociationFeuilleDeMatch visiteur;

    public AssociationFeuilleDeMatch getLocal() {
        return local;
    }

    public void setLocal(AssociationFeuilleDeMatch local) {
        this.local = local;
    }

    public AssociationFeuilleDeMatch getVisiteur() {
        return visiteur;
    }

    public void setVisiteur(AssociationFeuilleDeMatch visiteur) {
        this.visiteur = visiteur;
    }
}
