package org.nddngroup.navetfoot.core.domain.websocket;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

public enum WebSocketTopic {
    MATCH_CHRONO("/topic/matchs/%s/chrono"),
    MATCH_STATE("/topic/matchs/%s/state"),
    MATCH_EVENT("/topic/matchs/%s/event");

    private final String topic;

    WebSocketTopic(String topic) {
        this.topic = topic;
    }

    public String getTopic(Object ...args) {
        return URLEncoder.encode(String.format(topic, args), StandardCharsets.UTF_8);
    }
}
