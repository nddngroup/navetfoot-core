package org.nddngroup.navetfoot.core.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * A Poule.
 */
@Entity
@Table(name = "poule")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "poule")
public class Poule implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "libelle", nullable = false)
    private String libelle;

    @NotNull
    @Column(name = "nombre_equipe", nullable = false)
    private Integer nombreEquipe;

    @Column(name = "code")
    private String code;

    @Column(name = "hashcode")
    private String hashcode;

    @Column(name = "created_at")
    private ZonedDateTime createdAt;

    @Column(name = "updated_at")
    private ZonedDateTime updatedAt;

    @Column(name = "deleted")
    private Boolean deleted;

    @Column(name = "nombre_journee")
    private Integer nombreJournee;

    @ManyToOne
    @JsonIgnoreProperties(value = "poules", allowSetters = true)
    private EtapeCompetition etapeCompetition;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public Poule libelle(String libelle) {
        this.libelle = libelle;
        return this;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Integer getNombreEquipe() {
        return nombreEquipe;
    }

    public Poule nombreEquipe(Integer nombreEquipe) {
        this.nombreEquipe = nombreEquipe;
        return this;
    }

    public void setNombreEquipe(Integer nombreEquipe) {
        this.nombreEquipe = nombreEquipe;
    }

    public String getCode() {
        return code;
    }

    public Poule code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getHashcode() {
        return hashcode;
    }

    public Poule hashcode(String hashcode) {
        this.hashcode = hashcode;
        return this;
    }

    public void setHashcode(String hashcode) {
        this.hashcode = hashcode;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public Poule createdAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public Poule updatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public Poule deleted(Boolean deleted) {
        this.deleted = deleted;
        return this;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Integer getNombreJournee() {
        return nombreJournee;
    }

    public Poule nombreJournee(Integer nombreJournee) {
        this.nombreJournee = nombreJournee;
        return this;
    }

    public void setNombreJournee(Integer nombreJournee) {
        this.nombreJournee = nombreJournee;
    }

    public EtapeCompetition getEtapeCompetition() {
        return etapeCompetition;
    }

    public Poule etapeCompetition(EtapeCompetition etapeCompetition) {
        this.etapeCompetition = etapeCompetition;
        return this;
    }

    public void setEtapeCompetition(EtapeCompetition etapeCompetition) {
        this.etapeCompetition = etapeCompetition;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Poule)) {
            return false;
        }
        return id != null && id.equals(((Poule) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Poule{" +
            "id=" + getId() +
            ", libelle='" + getLibelle() + "'" +
            ", nombreEquipe=" + getNombreEquipe() +
            ", code='" + getCode() + "'" +
            ", hashcode='" + getHashcode() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", deleted='" + isDeleted() + "'" +
            ", nombreJournee=" + getNombreJournee() +
            "}";
    }
}
