package org.nddngroup.navetfoot.core.domain.custom.sync;

import org.nddngroup.navetfoot.core.service.dto.TirAuButDTO;

/**
 * Created by souleymane91 on 06/10/2018.
 */
public class CustomTirAuBut {
    private String matchHashcode;

    private TirAuButDTO tirAuButDTO;

    public String getMatchHashcode() {
        return matchHashcode;
    }

    public void setMatchHashcode(String matchHashcode) {
        this.matchHashcode = matchHashcode;
    }

    public TirAuButDTO getTirAuButDTO() {
        return tirAuButDTO;
    }

    public void setTirAuButDTO(TirAuButDTO tirAuButDTO) {
        this.tirAuButDTO = tirAuButDTO;
    }
}
