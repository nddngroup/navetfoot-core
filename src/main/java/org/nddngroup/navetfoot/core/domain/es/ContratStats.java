package org.nddngroup.navetfoot.core.domain.es;

import javax.persistence.Transient;

public class ContratStats {
    @Transient
    private Long joueurId;
    @Transient
    private String joueurNom;
    @Transient
    private String joueurPrenom;
    @Transient
    private String joueurImage;
    @Transient
    private Long associationId;
    @Transient
    private String associationNom;
    @Transient
    private Long saisonId;
    @Transient
    private String saisonNom;
    @Transient
    private Long organisationId;
    @Transient
    private String organisationNom;

    public Long getJoueurId() {
        return joueurId;
    }

    public ContratStats joueurId(Long joueurId) {
        this.joueurId = joueurId;
        return this;
    }

    public void setJoueurId(Long joueurId) {
        this.joueurId = joueurId;
    }

    public String getJoueurNom() {
        return joueurNom;
    }

    public ContratStats joueurNom(String joueurNom) {
        this.joueurNom = joueurNom;
        return this;
    }

    public void setJoueurNom(String joueurNom) {
        this.joueurNom = joueurNom;
    }

    public String getJoueurPrenom() {
        return joueurPrenom;
    }

    public ContratStats joueurPrenom(String joueurPrenom) {
        this.joueurPrenom = joueurPrenom;
        return this;
    }

    public void setJoueurPrenom(String joueurPrenom) {
        this.joueurPrenom = joueurPrenom;
    }

    public String getJoueurImage() {
        return joueurImage;
    }

    public ContratStats joueurImage(String joueurImage) {
        this.joueurImage = joueurImage;
        return this;
    }

    public void setJoueurImage(String joueurImage) {
        this.joueurImage = joueurImage;
    }
    public Long getAssociationId() {
        return associationId;
    }

    public void setAssociationId(Long associationId) {
        this.associationId = associationId;
    }

    public String getAssociationNom() {
        return associationNom;
    }

    public void setAssociationNom(String associationNom) {
        this.associationNom = associationNom;
    }

    public Long getSaisonId() {
        return saisonId;
    }

    public void setSaisonId(Long saisonId) {
        this.saisonId = saisonId;
    }

    public String getSaisonNom() {
        return saisonNom;
    }

    public void setSaisonNom(String saisonNom) {
        this.saisonNom = saisonNom;
    }

    public Long getOrganisationId() {
        return organisationId;
    }

    public void setOrganisationId(Long organisationId) {
        this.organisationId = organisationId;
    }

    public String getOrganisationNom() {
        return organisationNom;
    }

    public void setOrganisationNom(String organisationNom) {
        this.organisationNom = organisationNom;
    }
}
