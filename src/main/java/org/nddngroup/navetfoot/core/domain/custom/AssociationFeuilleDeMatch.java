package org.nddngroup.navetfoot.core.domain.custom;

import org.nddngroup.navetfoot.core.domain.custom.stats.JoueurDetail;

import java.util.ArrayList;
import java.util.List;

public class AssociationFeuilleDeMatch {
    List<JoueurDetail> partants;
    List<JoueurDetail> suppleants;

    public AssociationFeuilleDeMatch() {
        this.partants   = new ArrayList<>();
        this.suppleants = new ArrayList<>();
    }

    public List<JoueurDetail> getPartants() {
        return partants;
    }

    public void setPartants(List<JoueurDetail> partants) {
        this.partants = partants;
    }

    public List<JoueurDetail> getSuppleants() {
        return suppleants;
    }

    public void setSuppleants(List<JoueurDetail> suppleants) {
        this.suppleants = suppleants;
    }
}
