package org.nddngroup.navetfoot.core.domain.custom.sync;

import org.nddngroup.navetfoot.core.service.dto.EtapeCompetitionDTO;

/**
 * Created by souleymane91 on 06/10/2018.
 */
public class CustomEtapeCompetition {
    private String competitionSaisonHashcode;
    private EtapeCompetitionDTO etapeCompetitionDTO;

    public String getCompetitionSaisonHashcode() {
        return competitionSaisonHashcode;
    }

    public void setCompetitionSaisonHashcode(String competitionSaisonHashcode) {
        this.competitionSaisonHashcode = competitionSaisonHashcode;
    }

    public EtapeCompetitionDTO getEtapeCompetitionDTO() {
        return etapeCompetitionDTO;
    }

    public void setEtapeCompetitionDTO(EtapeCompetitionDTO etapeCompetitionDTO) {
        this.etapeCompetitionDTO = etapeCompetitionDTO;
    }
}
