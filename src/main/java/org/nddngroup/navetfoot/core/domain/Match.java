package org.nddngroup.navetfoot.core.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.nddngroup.navetfoot.core.domain.es.MatchStats;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

import org.nddngroup.navetfoot.core.domain.enumeration.EtatMatch;

/**
 * A Match.
 */
@Entity
@Table(name = "match")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "match")
public class Match extends MatchStats implements Serializable {
    private static final long serialVersionUID = 1L;

    public Match() {
        super();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "coup_d_envoi")
    private String coupDEnvoi;

    @Column(name = "hashcode")
    private String hashcode;

    @Column(name = "created_at")
    private ZonedDateTime createdAt;

    @Column(name = "updated_at")
    private ZonedDateTime updatedAt;

    @Column(name = "deleted")
    private Boolean deleted;

    @Column(name = "date_match")
    private LocalDate dateMatch;

    @Enumerated(EnumType.STRING)
    @Column(name = "etat_match")
    private EtatMatch etatMatch;

    @NotNull
    @Column(name = "code", nullable = false)
    private String code;

    @Column(name = "journee")
    private Integer journee;

    @Column(name = "chrono")
    private String chrono;

    @OneToMany(mappedBy = "match")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<But> buts = new HashSet<>();

    @OneToMany(mappedBy = "match")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Carton> cartons = new HashSet<>();

    @OneToMany(mappedBy = "match")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<CoupFranc> coupFrancs = new HashSet<>();

    @OneToMany(mappedBy = "match")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Penalty> penalties = new HashSet<>();

    @OneToMany(mappedBy = "match")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Corner> corners = new HashSet<>();

    @OneToMany(mappedBy = "match")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<DelegueMatch> delegueMatches = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = "matches", allowSetters = true)
    private Association visiteur;

    @ManyToOne
    @JsonIgnoreProperties(value = "matches", allowSetters = true)
    private Association local;

    @OneToOne(mappedBy = "match")
    @JsonIgnore
    private TirAuBut tirAuBut;

    @ManyToOne
    @JsonIgnoreProperties(value = "matches", allowSetters = true)
    private EtapeCompetition etapeCompetition;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCoupDEnvoi() {
        return coupDEnvoi;
    }

    public Match coupDEnvoi(String coupDEnvoi) {
        this.coupDEnvoi = coupDEnvoi;
        return this;
    }

    public void setCoupDEnvoi(String coupDEnvoi) {
        this.coupDEnvoi = coupDEnvoi;
    }

    public String getHashcode() {
        return hashcode;
    }

    public Match hashcode(String hashcode) {
        this.hashcode = hashcode;
        return this;
    }

    public void setHashcode(String hashcode) {
        this.hashcode = hashcode;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public Match createdAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public Match updatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public Match deleted(Boolean deleted) {
        this.deleted = deleted;
        return this;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public LocalDate getDateMatch() {
        return dateMatch;
    }

    public Match dateMatch(LocalDate dateMatch) {
        this.dateMatch = dateMatch;
        return this;
    }

    public void setDateMatch(LocalDate dateMatch) {
        this.dateMatch = dateMatch;
    }

    public EtatMatch getEtatMatch() {
        return etatMatch;
    }

    public Match etatMatch(EtatMatch etatMatch) {
        this.etatMatch = etatMatch;
        return this;
    }

    public void setEtatMatch(EtatMatch etatMatch) {
        this.etatMatch = etatMatch;
    }

    public String getCode() {
        return code;
    }

    public Match code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getJournee() {
        return journee;
    }

    public Match journee(Integer journee) {
        this.journee = journee;
        return this;
    }

    public void setJournee(Integer journee) {
        this.journee = journee;
    }

    public String getChrono() {
        return chrono;
    }

    public Match chrono(String chrono) {
        this.chrono = chrono;
        return this;
    }

    public void setChrono(String chrono) {
        this.chrono = chrono;
    }

    public Set<But> getButs() {
        return buts;
    }

    public Match buts(Set<But> buts) {
        this.buts = buts;
        return this;
    }

    public Match addButs(But but) {
        this.buts.add(but);
        but.setMatch(this);
        return this;
    }

    public Match removeButs(But but) {
        this.buts.remove(but);
        but.setMatch(null);
        return this;
    }

    public void setButs(Set<But> buts) {
        this.buts = buts;
    }

    public Set<Carton> getCartons() {
        return cartons;
    }

    public Match cartons(Set<Carton> cartons) {
        this.cartons = cartons;
        return this;
    }

    public Match addCartons(Carton carton) {
        this.cartons.add(carton);
        carton.setMatch(this);
        return this;
    }

    public Match removeCartons(Carton carton) {
        this.cartons.remove(carton);
        carton.setMatch(null);
        return this;
    }

    public void setCartons(Set<Carton> cartons) {
        this.cartons = cartons;
    }

    public Set<CoupFranc> getCoupFrancs() {
        return coupFrancs;
    }

    public Match coupFrancs(Set<CoupFranc> coupFrancs) {
        this.coupFrancs = coupFrancs;
        return this;
    }

    public Match addCoupFrancs(CoupFranc coupFranc) {
        this.coupFrancs.add(coupFranc);
        coupFranc.setMatch(this);
        return this;
    }

    public Match removeCoupFrancs(CoupFranc coupFranc) {
        this.coupFrancs.remove(coupFranc);
        coupFranc.setMatch(null);
        return this;
    }

    public void setCoupFrancs(Set<CoupFranc> coupFrancs) {
        this.coupFrancs = coupFrancs;
    }

    public Set<Penalty> getPenalties() {
        return penalties;
    }

    public Match penalties(Set<Penalty> penalties) {
        this.penalties = penalties;
        return this;
    }

    public Match addPenalties(Penalty penalty) {
        this.penalties.add(penalty);
        penalty.setMatch(this);
        return this;
    }

    public Match removePenalties(Penalty penalty) {
        this.penalties.remove(penalty);
        penalty.setMatch(null);
        return this;
    }

    public void setPenalties(Set<Penalty> penalties) {
        this.penalties = penalties;
    }

    public Set<Corner> getCorners() {
        return corners;
    }

    public Match corners(Set<Corner> corners) {
        this.corners = corners;
        return this;
    }

    public Match addCorners(Corner corner) {
        this.corners.add(corner);
        corner.setMatch(this);
        return this;
    }

    public Match removeCorners(Corner corner) {
        this.corners.remove(corner);
        corner.setMatch(null);
        return this;
    }

    public void setCorners(Set<Corner> corners) {
        this.corners = corners;
    }

    public Set<DelegueMatch> getDelegueMatches() {
        return delegueMatches;
    }

    public Match delegueMatches(Set<DelegueMatch> delegueMatches) {
        this.delegueMatches = delegueMatches;
        return this;
    }

    public Match addDelegueMatch(DelegueMatch delegueMatch) {
        this.delegueMatches.add(delegueMatch);
        delegueMatch.setMatch(this);
        return this;
    }

    public Match removeDelegueMatch(DelegueMatch delegueMatch) {
        this.delegueMatches.remove(delegueMatch);
        delegueMatch.setMatch(null);
        return this;
    }

    public void setDelegueMatches(Set<DelegueMatch> delegueMatches) {
        this.delegueMatches = delegueMatches;
    }

    public Association getVisiteur() {
        return visiteur;
    }

    public Match visiteur(Association association) {
        this.visiteur = association;
        return this;
    }

    public void setVisiteur(Association association) {
        this.visiteur = association;
    }

    public Association getLocal() {
        return local;
    }

    public Match local(Association association) {
        this.local = association;
        return this;
    }

    public void setLocal(Association association) {
        this.local = association;
    }

    public TirAuBut getTirAuBut() {
        return tirAuBut;
    }

    public Match tirAuBut(TirAuBut tirAuBut) {
        this.tirAuBut = tirAuBut;
        return this;
    }

    public void setTirAuBut(TirAuBut tirAuBut) {
        this.tirAuBut = tirAuBut;
    }

    public EtapeCompetition getEtapeCompetition() {
        return etapeCompetition;
    }

    public Match etapeCompetition(EtapeCompetition etapeCompetition) {
        this.etapeCompetition = etapeCompetition;
        return this;
    }

    public void setEtapeCompetition(EtapeCompetition etapeCompetition) {
        this.etapeCompetition = etapeCompetition;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Match)) {
            return false;
        }
        return id != null && id.equals(((Match) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Match{" +
            "id=" + getId() +
            ", coupDEnvoi='" + getCoupDEnvoi() + "'" +
            ", hashcode='" + getHashcode() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", deleted='" + isDeleted() + "'" +
            ", dateMatch='" + getDateMatch() + "'" +
            ", etatMatch='" + getEtatMatch() + "'" +
            ", code='" + getCode() + "'" +
            ", journee=" + getJournee() +
            ", chrono='" + getChrono() + "'" +
            "}";
    }
}
