package org.nddngroup.navetfoot.core.web.rest;

import org.nddngroup.navetfoot.core.domain.JoueurCategory;
import org.nddngroup.navetfoot.core.service.JoueurCategoryService;
import org.nddngroup.navetfoot.core.service.dto.JoueurCategoryDTO;
import org.nddngroup.navetfoot.core.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link JoueurCategory}.
 */
@RestController
@RequestMapping("/api")
public class JoueurCategoryResource {

    private final Logger log = LoggerFactory.getLogger(JoueurCategoryResource.class);

    private static final String ENTITY_NAME = "navetfootCoreJoueurCategory";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final JoueurCategoryService joueurCategoryService;

    public JoueurCategoryResource(JoueurCategoryService joueurCategoryService) {
        this.joueurCategoryService = joueurCategoryService;
    }

    /**
     * {@code POST  /joueur-categories} : Create a new joueurCategory.
     *
     * @param joueurCategoryDTO the joueurCategoryDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new joueurCategoryDTO, or with status {@code 400 (Bad Request)} if the joueurCategory has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/joueur-categories")
    public ResponseEntity<JoueurCategoryDTO> createJoueurCategory(@RequestBody JoueurCategoryDTO joueurCategoryDTO) throws URISyntaxException {
        log.debug("REST request to save JoueurCategory : {}", joueurCategoryDTO);
        if (joueurCategoryDTO.getId() != null) {
            throw new BadRequestAlertException("A new joueurCategory cannot already have an ID", ENTITY_NAME, "idexists");
        }
        JoueurCategoryDTO result = joueurCategoryService.save(joueurCategoryDTO);
        return ResponseEntity.created(new URI("/api/joueur-categories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /joueur-categories} : Updates an existing joueurCategory.
     *
     * @param joueurCategoryDTO the joueurCategoryDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated joueurCategoryDTO,
     * or with status {@code 400 (Bad Request)} if the joueurCategoryDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the joueurCategoryDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/joueur-categories")
    public ResponseEntity<JoueurCategoryDTO> updateJoueurCategory(@RequestBody JoueurCategoryDTO joueurCategoryDTO) throws URISyntaxException {
        log.debug("REST request to update JoueurCategory : {}", joueurCategoryDTO);
        if (joueurCategoryDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        JoueurCategoryDTO result = joueurCategoryService.save(joueurCategoryDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, joueurCategoryDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /joueur-categories} : get all the joueurCategories.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of joueurCategories in body.
     */
    @GetMapping("/joueur-categories")
    public ResponseEntity<List<JoueurCategoryDTO>> getAllJoueurCategories(Pageable pageable) {
        log.debug("REST request to get a page of JoueurCategories");
        Page<JoueurCategoryDTO> page = joueurCategoryService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /joueur-categories/:id} : get the "id" joueurCategory.
     *
     * @param id the id of the joueurCategoryDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the joueurCategoryDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/joueur-categories/{id}")
    public ResponseEntity<JoueurCategoryDTO> getJoueurCategory(@PathVariable Long id) {
        log.debug("REST request to get JoueurCategory : {}", id);
        Optional<JoueurCategoryDTO> joueurCategoryDTO = joueurCategoryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(joueurCategoryDTO);
    }

    /**
     * {@code DELETE  /joueur-categories/:id} : delete the "id" joueurCategory.
     *
     * @param id the id of the joueurCategoryDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/joueur-categories/{id}")
    public ResponseEntity<Void> deleteJoueurCategory(@PathVariable Long id) {
        log.debug("REST request to delete JoueurCategory : {}", id);
        joueurCategoryService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/joueur-categories?query=:query} : search for the joueurCategory corresponding
     * to the query.
     *
     * @param query the query of the joueurCategory search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/joueur-categories")
    public ResponseEntity<List<JoueurCategoryDTO>> searchJoueurCategories(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of JoueurCategories for query {}", query);
        Page<JoueurCategoryDTO> page = joueurCategoryService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
        }
}
