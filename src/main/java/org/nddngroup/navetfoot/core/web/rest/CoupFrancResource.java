package org.nddngroup.navetfoot.core.web.rest;

import org.nddngroup.navetfoot.core.domain.CoupFranc;
import org.nddngroup.navetfoot.core.service.CoupFrancService;
import org.nddngroup.navetfoot.core.service.dto.CoupFrancDTO;
import org.nddngroup.navetfoot.core.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link CoupFranc}.
 */
@RestController
@RequestMapping("/api")
public class CoupFrancResource {

    private final Logger log = LoggerFactory.getLogger(CoupFrancResource.class);

    private static final String ENTITY_NAME = "navetfootCoreCoupFranc";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CoupFrancService coupFrancService;

    public CoupFrancResource(CoupFrancService coupFrancService) {
        this.coupFrancService = coupFrancService;
    }

    /**
     * {@code POST  /coup-francs} : Create a new coupFranc.
     *
     * @param coupFrancDTO the coupFrancDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new coupFrancDTO, or with status {@code 400 (Bad Request)} if the coupFranc has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/coup-francs")
    public ResponseEntity<CoupFrancDTO> createCoupFranc(@RequestBody CoupFrancDTO coupFrancDTO) throws URISyntaxException {
        log.debug("REST request to save CoupFranc : {}", coupFrancDTO);
        if (coupFrancDTO.getId() != null) {
            throw new BadRequestAlertException("A new coupFranc cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CoupFrancDTO result = coupFrancService.save(coupFrancDTO);
        return ResponseEntity.created(new URI("/api/coup-francs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /coup-francs} : Updates an existing coupFranc.
     *
     * @param coupFrancDTO the coupFrancDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated coupFrancDTO,
     * or with status {@code 400 (Bad Request)} if the coupFrancDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the coupFrancDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/coup-francs")
    public ResponseEntity<CoupFrancDTO> updateCoupFranc(@RequestBody CoupFrancDTO coupFrancDTO) throws URISyntaxException {
        log.debug("REST request to update CoupFranc : {}", coupFrancDTO);
        if (coupFrancDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CoupFrancDTO result = coupFrancService.save(coupFrancDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, coupFrancDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /coup-francs} : get all the coupFrancs.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of coupFrancs in body.
     */
    @GetMapping("/coup-francs")
    public ResponseEntity<List<CoupFrancDTO>> getAllCoupFrancs(Pageable pageable) {
        log.debug("REST request to get a page of CoupFrancs");
        Page<CoupFrancDTO> page = coupFrancService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /coup-francs/:id} : get the "id" coupFranc.
     *
     * @param id the id of the coupFrancDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the coupFrancDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/coup-francs/{id}")
    public ResponseEntity<CoupFrancDTO> getCoupFranc(@PathVariable Long id) {
        log.debug("REST request to get CoupFranc : {}", id);
        Optional<CoupFrancDTO> coupFrancDTO = coupFrancService.findOne(id);
        return ResponseUtil.wrapOrNotFound(coupFrancDTO);
    }

    /**
     * {@code DELETE  /coup-francs/:id} : delete the "id" coupFranc.
     *
     * @param id the id of the coupFrancDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/coup-francs/{id}")
    public ResponseEntity<Void> deleteCoupFranc(@PathVariable Long id) {
        log.debug("REST request to delete CoupFranc : {}", id);
        coupFrancService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/coup-francs?query=:query} : search for the coupFranc corresponding
     * to the query.
     *
     * @param query the query of the coupFranc search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/coup-francs")
    public ResponseEntity<List<CoupFrancDTO>> searchCoupFrancs(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of CoupFrancs for query {}", query);
        Page<CoupFrancDTO> page = coupFrancService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
        }
}
