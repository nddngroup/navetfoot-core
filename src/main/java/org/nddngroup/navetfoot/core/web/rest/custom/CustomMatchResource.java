package org.nddngroup.navetfoot.core.web.rest.custom;

import org.nddngroup.navetfoot.core.domain.Match;
import org.nddngroup.navetfoot.core.domain.custom.stats.MatchDetail;
import org.nddngroup.navetfoot.core.service.AssociationService;
import org.nddngroup.navetfoot.core.service.EtapeCompetitionService;
import org.nddngroup.navetfoot.core.service.MatchService;
import org.nddngroup.navetfoot.core.service.custom.CommunService;
import org.nddngroup.navetfoot.core.service.custom.DeleteService;
import org.nddngroup.navetfoot.core.service.dto.EtapeCompetitionDTO;
import org.nddngroup.navetfoot.core.service.dto.MatchDTO;
import org.nddngroup.navetfoot.core.service.dto.OrganisationDTO;
import org.nddngroup.navetfoot.core.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import org.nddngroup.navetfoot.core.service.*;
import org.nddngroup.navetfoot.core.service.dto.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * REST controller for managing {@link Match}.
 */
@RestController
@RequestMapping("/api")
public class CustomMatchResource {

    private final Logger log = LoggerFactory.getLogger(CustomMatchResource.class);

    private static final String ENTITY_NAME = "navetfootCoreMatch";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MatchService matchService;

    @Autowired
    private DeleteService deleteService;
    @Autowired
    private CommunService communService;
    @Autowired
    private EtapeCompetitionService etapeCompetitionService;
    @Autowired
    private AssociationService associationService;

    public CustomMatchResource(MatchService matchService) {
        this.matchService = matchService;
    }

    /**
     * POST  /organisations/:id/matches : Create a new match.
     *
     * @param id L'id de l'organisation
     * @param matchDTO the matchDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new matchDTO, or with status 400 (Bad Request) if the match has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/organisations/{id}/matches")
    public ResponseEntity<MatchDTO> createMatchForOrganisation(@PathVariable Long id,
                                                               @Valid @RequestBody MatchDTO matchDTO) throws URISyntaxException {
        log.debug("REST request to save Match : {}", matchDTO);

        if (matchDTO.getId() != null) {
            throw new BadRequestAlertException("A new match cannot already have an ID", ENTITY_NAME, "idexists");
        }

        // find organisation
        OrganisationDTO organisationDTO = this.communService.checkOrganisation(id);

        // save match
        MatchDTO result = matchService.save(matchDTO, organisationDTO);

        return ResponseEntity.created(new URI("/api/organisations/" + id + "/matches/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }
    /**
     * GET  /organisations/:organisationId/matches : get all the matches.
     *
     * @param organisationId L'id de l'organisation
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of matches in body
     */
    @GetMapping("/organisations/{organisationId}/matches")
    public ResponseEntity<List<MatchDetail>> getAllMatchesForOrganisation(@PathVariable Long organisationId,
                                                                          @ApiParam Pageable pageable) {
        log.debug("REST request to get Matches by organisationId: {}", organisationId);

        // get organisation
        OrganisationDTO organisationDTO = this.communService.checkOrganisation(organisationId);

        // get matchs
        List<Long> matchIds = communService.getMatchsByOrganisation(organisationDTO)
            .stream().map(MatchDTO::getId).collect(Collectors.toList());

        Page<MatchDetail> page = matchIds.size() != 0 ? matchService.findByIdIn(matchIds, pageable): Page.empty();

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);

    }

    /**
     * {@code GET  /organisations/:organisationId/etape-competitions/:etapeCompetitionId/matchs} : get all the matchs by etape.
     *
     * @param organisationId L'id de l'organisation
     * @param etapeCompetitionIds L'id de l'étape
     * @return List pouleDetails
     */
    @GetMapping("/organisations/{organisationId}/etape-competitions/matchs")
    public ResponseEntity<List<MatchDetail>> getCustomMatchs(@PathVariable Long organisationId,
                                                             @RequestParam(name = "etapeIds") String etapeCompetitionIds,
                                                             @RequestParam(required = false) Integer journee) {
        List<MatchDetail> matchDetails = new ArrayList<>();

        // check organisation
        OrganisationDTO organisationDTO = communService.checkOrganisation(organisationId);

        Arrays.stream(etapeCompetitionIds.split(","))
            .map(Long::parseLong)
            .collect(Collectors.toList()).forEach(etapeCompetitionId -> {
                // check etapeCompetition
                EtapeCompetitionDTO etapeCompetitionDTO = communService.checkEtapeCompetition(etapeCompetitionId, organisationDTO);

                this.matchService.findByEtapeCompetition(etapeCompetitionDTO, journee)
                    .forEach(matchDTO -> {
                        MatchDetail matchDetail = communService.getMatchDetail(matchDTO);
                        matchDetails.add(matchDetail);
                    });
            });

        return new ResponseEntity<>(matchDetails, HttpStatus.OK);
    }

    /**
     * DELETE /organisations/:organisationId/matchs : remove list of matchs
     *
     * @param headers HttpHeaders
     * @return List<MatchDTO>
     */
    @DeleteMapping("/organisations/{organisationId}/matchs")
    public ResponseEntity<List<MatchDTO>> removeMatchs(@RequestHeader HttpHeaders headers,
                                                              @PathVariable Long organisationId) throws URISyntaxException {
        OrganisationDTO organisationDTO = communService.checkOrganisation(organisationId);
        List<String> matchIds = headers.getValuesAsList("ids");

        List<MatchDTO> result = deleteService.removeMatchList(organisationDTO, matchIds);

        return ResponseEntity.created(new URI("/api/organisations/" + organisationId + "/matchs/"))
            .body(result);
    }

    /**
     * DELETE  /organisations/:organisationId/matches/:id : delete the "id" match.
     *
     * @param id the id of the matchDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/organisations/{organisationId}/matches/{id}")
    public ResponseEntity<Void> deleteMatchForOrganisation(@PathVariable Long organisationId,
                                                           @PathVariable Long id) {
        log.debug("REST request to delete Match : {}", id);

        // find organisation
        OrganisationDTO organisationDTO = this.communService.checkOrganisation(organisationId);

        MatchDTO matchDTO = this.communService.checkMatch(id, organisationDTO);

        deleteService.deleteMatch(matchDTO);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }

    /**
     * GET  /_search/organisations/:organisationId/matches : search all the matches.
     *
     * @param organisationId L'id de l'organisation
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of matches in body
     */
    @GetMapping("/_search/organisations/{organisationId}/matches")
    public ResponseEntity<List<MatchDetail>> searchMatchesForOrganisation(@RequestParam String query,
                                                                          @PathVariable Long organisationId,
                                                                          @ApiParam Pageable pageable) {
        log.debug("REST request to get Matches by organisationId: {}", organisationId);

        // get organisation
        OrganisationDTO organisationDTO = this.communService.checkOrganisation(organisationId);

        // get matchs
        Page<MatchDetail> page = matchService.search(organisationDTO, query, pageable);

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);

    }
}
