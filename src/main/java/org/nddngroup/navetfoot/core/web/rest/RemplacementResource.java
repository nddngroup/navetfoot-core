package org.nddngroup.navetfoot.core.web.rest;

import org.nddngroup.navetfoot.core.domain.Remplacement;
import org.nddngroup.navetfoot.core.service.RemplacementService;
import org.nddngroup.navetfoot.core.service.dto.RemplacementDTO;
import org.nddngroup.navetfoot.core.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link Remplacement}.
 */
@RestController
@RequestMapping("/api")
public class RemplacementResource {

    private final Logger log = LoggerFactory.getLogger(RemplacementResource.class);

    private static final String ENTITY_NAME = "navetfootCoreRemplacement";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RemplacementService remplacementService;

    public RemplacementResource(RemplacementService remplacementService) {
        this.remplacementService = remplacementService;
    }

    /**
     * {@code POST  /remplacements} : Create a new remplacement.
     *
     * @param remplacementDTO the remplacementDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new remplacementDTO, or with status {@code 400 (Bad Request)} if the remplacement has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/remplacements")
    public ResponseEntity<RemplacementDTO> createRemplacement(@RequestBody RemplacementDTO remplacementDTO) throws URISyntaxException {
        log.debug("REST request to save Remplacement : {}", remplacementDTO);
        if (remplacementDTO.getId() != null) {
            throw new BadRequestAlertException("A new remplacement cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RemplacementDTO result = remplacementService.save(remplacementDTO);
        return ResponseEntity.created(new URI("/api/remplacements/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /remplacements} : Updates an existing remplacement.
     *
     * @param remplacementDTO the remplacementDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated remplacementDTO,
     * or with status {@code 400 (Bad Request)} if the remplacementDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the remplacementDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/remplacements")
    public ResponseEntity<RemplacementDTO> updateRemplacement(@RequestBody RemplacementDTO remplacementDTO) throws URISyntaxException {
        log.debug("REST request to update Remplacement : {}", remplacementDTO);
        if (remplacementDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RemplacementDTO result = remplacementService.save(remplacementDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, remplacementDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /remplacements} : get all the remplacements.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of remplacements in body.
     */
    @GetMapping("/remplacements")
    public ResponseEntity<List<RemplacementDTO>> getAllRemplacements(Pageable pageable) {
        log.debug("REST request to get a page of Remplacements");
        Page<RemplacementDTO> page = remplacementService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /remplacements/:id} : get the "id" remplacement.
     *
     * @param id the id of the remplacementDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the remplacementDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/remplacements/{id}")
    public ResponseEntity<RemplacementDTO> getRemplacement(@PathVariable Long id) {
        log.debug("REST request to get Remplacement : {}", id);
        Optional<RemplacementDTO> remplacementDTO = remplacementService.findOne(id);
        return ResponseUtil.wrapOrNotFound(remplacementDTO);
    }

    /**
     * {@code DELETE  /remplacements/:id} : delete the "id" remplacement.
     *
     * @param id the id of the remplacementDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/remplacements/{id}")
    public ResponseEntity<Void> deleteRemplacement(@PathVariable Long id) {
        log.debug("REST request to delete Remplacement : {}", id);
        remplacementService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/remplacements?query=:query} : search for the remplacement corresponding
     * to the query.
     *
     * @param query the query of the remplacement search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/remplacements")
    public ResponseEntity<List<RemplacementDTO>> searchRemplacements(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Remplacements for query {}", query);
        Page<RemplacementDTO> page = remplacementService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
        }
}
