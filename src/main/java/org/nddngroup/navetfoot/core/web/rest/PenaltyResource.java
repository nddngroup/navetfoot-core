package org.nddngroup.navetfoot.core.web.rest;

import org.nddngroup.navetfoot.core.domain.Penalty;
import org.nddngroup.navetfoot.core.service.PenaltyService;
import org.nddngroup.navetfoot.core.service.dto.PenaltyDTO;
import org.nddngroup.navetfoot.core.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link Penalty}.
 */
@RestController
@RequestMapping("/api")
public class PenaltyResource {

    private final Logger log = LoggerFactory.getLogger(PenaltyResource.class);

    private static final String ENTITY_NAME = "navetfootCorePenalty";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PenaltyService penaltyService;

    public PenaltyResource(PenaltyService penaltyService) {
        this.penaltyService = penaltyService;
    }

    /**
     * {@code POST  /penalties} : Create a new penalty.
     *
     * @param penaltyDTO the penaltyDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new penaltyDTO, or with status {@code 400 (Bad Request)} if the penalty has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/penalties")
    public ResponseEntity<PenaltyDTO> createPenalty(@RequestBody PenaltyDTO penaltyDTO) throws URISyntaxException {
        log.debug("REST request to save Penalty : {}", penaltyDTO);
        if (penaltyDTO.getId() != null) {
            throw new BadRequestAlertException("A new penalty cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PenaltyDTO result = penaltyService.save(penaltyDTO);
        return ResponseEntity.created(new URI("/api/penalties/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /penalties} : Updates an existing penalty.
     *
     * @param penaltyDTO the penaltyDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated penaltyDTO,
     * or with status {@code 400 (Bad Request)} if the penaltyDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the penaltyDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/penalties")
    public ResponseEntity<PenaltyDTO> updatePenalty(@RequestBody PenaltyDTO penaltyDTO) throws URISyntaxException {
        log.debug("REST request to update Penalty : {}", penaltyDTO);
        if (penaltyDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PenaltyDTO result = penaltyService.save(penaltyDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, penaltyDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /penalties} : get all the penalties.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of penalties in body.
     */
    @GetMapping("/penalties")
    public ResponseEntity<List<PenaltyDTO>> getAllPenalties(Pageable pageable) {
        log.debug("REST request to get a page of Penalties");
        Page<PenaltyDTO> page = penaltyService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /penalties/:id} : get the "id" penalty.
     *
     * @param id the id of the penaltyDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the penaltyDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/penalties/{id}")
    public ResponseEntity<PenaltyDTO> getPenalty(@PathVariable Long id) {
        log.debug("REST request to get Penalty : {}", id);
        Optional<PenaltyDTO> penaltyDTO = penaltyService.findOne(id);
        return ResponseUtil.wrapOrNotFound(penaltyDTO);
    }

    /**
     * {@code DELETE  /penalties/:id} : delete the "id" penalty.
     *
     * @param id the id of the penaltyDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/penalties/{id}")
    public ResponseEntity<Void> deletePenalty(@PathVariable Long id) {
        log.debug("REST request to delete Penalty : {}", id);
        penaltyService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/penalties?query=:query} : search for the penalty corresponding
     * to the query.
     *
     * @param query the query of the penalty search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/penalties")
    public ResponseEntity<List<PenaltyDTO>> searchPenalties(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Penalties for query {}", query);
        Page<PenaltyDTO> page = penaltyService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
        }
}
