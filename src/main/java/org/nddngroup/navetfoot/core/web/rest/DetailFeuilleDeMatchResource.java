package org.nddngroup.navetfoot.core.web.rest;

import org.nddngroup.navetfoot.core.service.DetailFeuilleDeMatchService;
import org.nddngroup.navetfoot.core.web.rest.errors.BadRequestAlertException;
import org.nddngroup.navetfoot.core.service.dto.DetailFeuilleDeMatchDTO;

import org.nddngroup.navetfoot.core.domain.DetailFeuilleDeMatch;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link DetailFeuilleDeMatch}.
 */
@RestController
@RequestMapping("/api")
public class DetailFeuilleDeMatchResource {

    private final Logger log = LoggerFactory.getLogger(DetailFeuilleDeMatchResource.class);

    private static final String ENTITY_NAME = "navetfootCoreDetailFeuilleDeMatch";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DetailFeuilleDeMatchService detailFeuilleDeMatchService;

    public DetailFeuilleDeMatchResource(DetailFeuilleDeMatchService detailFeuilleDeMatchService) {
        this.detailFeuilleDeMatchService = detailFeuilleDeMatchService;
    }

    /**
     * {@code POST  /detail-feuille-de-matches} : Create a new detailFeuilleDeMatch.
     *
     * @param detailFeuilleDeMatchDTO the detailFeuilleDeMatchDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new detailFeuilleDeMatchDTO, or with status {@code 400 (Bad Request)} if the detailFeuilleDeMatch has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/detail-feuille-de-matches")
    public ResponseEntity<DetailFeuilleDeMatchDTO> createDetailFeuilleDeMatch(@Valid @RequestBody DetailFeuilleDeMatchDTO detailFeuilleDeMatchDTO) throws URISyntaxException {
        log.debug("REST request to save DetailFeuilleDeMatch : {}", detailFeuilleDeMatchDTO);
        if (detailFeuilleDeMatchDTO.getId() != null) {
            throw new BadRequestAlertException("A new detailFeuilleDeMatch cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DetailFeuilleDeMatchDTO result = detailFeuilleDeMatchService.save(detailFeuilleDeMatchDTO);
        return ResponseEntity.created(new URI("/api/detail-feuille-de-matches/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /detail-feuille-de-matches} : Updates an existing detailFeuilleDeMatch.
     *
     * @param detailFeuilleDeMatchDTO the detailFeuilleDeMatchDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated detailFeuilleDeMatchDTO,
     * or with status {@code 400 (Bad Request)} if the detailFeuilleDeMatchDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the detailFeuilleDeMatchDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/detail-feuille-de-matches")
    public ResponseEntity<DetailFeuilleDeMatchDTO> updateDetailFeuilleDeMatch(@Valid @RequestBody DetailFeuilleDeMatchDTO detailFeuilleDeMatchDTO) throws URISyntaxException {
        log.debug("REST request to update DetailFeuilleDeMatch : {}", detailFeuilleDeMatchDTO);
        if (detailFeuilleDeMatchDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DetailFeuilleDeMatchDTO result = detailFeuilleDeMatchService.save(detailFeuilleDeMatchDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, detailFeuilleDeMatchDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /detail-feuille-de-matches} : get all the detailFeuilleDeMatches.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of detailFeuilleDeMatches in body.
     */
    @GetMapping("/detail-feuille-de-matches")
    public ResponseEntity<List<DetailFeuilleDeMatchDTO>> getAllDetailFeuilleDeMatches(Pageable pageable) {
        log.debug("REST request to get a page of DetailFeuilleDeMatches");
        Page<DetailFeuilleDeMatchDTO> page = detailFeuilleDeMatchService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /detail-feuille-de-matches/:id} : get the "id" detailFeuilleDeMatch.
     *
     * @param id the id of the detailFeuilleDeMatchDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the detailFeuilleDeMatchDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/detail-feuille-de-matches/{id}")
    public ResponseEntity<DetailFeuilleDeMatchDTO> getDetailFeuilleDeMatch(@PathVariable Long id) {
        log.debug("REST request to get DetailFeuilleDeMatch : {}", id);
        Optional<DetailFeuilleDeMatchDTO> detailFeuilleDeMatchDTO = detailFeuilleDeMatchService.findOne(id);
        return ResponseUtil.wrapOrNotFound(detailFeuilleDeMatchDTO);
    }

    /**
     * {@code DELETE  /detail-feuille-de-matches/:id} : delete the "id" detailFeuilleDeMatch.
     *
     * @param id the id of the detailFeuilleDeMatchDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/detail-feuille-de-matches/{id}")
    public ResponseEntity<Void> deleteDetailFeuilleDeMatch(@PathVariable Long id) {
        log.debug("REST request to delete DetailFeuilleDeMatch : {}", id);
        detailFeuilleDeMatchService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/detail-feuille-de-matches?query=:query} : search for the detailFeuilleDeMatch corresponding
     * to the query.
     *
     * @param query the query of the detailFeuilleDeMatch search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/detail-feuille-de-matches")
    public ResponseEntity<List<DetailFeuilleDeMatchDTO>> searchDetailFeuilleDeMatches(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of DetailFeuilleDeMatches for query {}", query);
        Page<DetailFeuilleDeMatchDTO> page = detailFeuilleDeMatchService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
        }
}
