package org.nddngroup.navetfoot.core.web.rest;

import org.nddngroup.navetfoot.core.domain.Poule;
import org.nddngroup.navetfoot.core.service.PouleService;
import org.nddngroup.navetfoot.core.service.dto.PouleDTO;
import org.nddngroup.navetfoot.core.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link Poule}.
 */
@RestController
@RequestMapping("/api")
public class PouleResource {

    private final Logger log = LoggerFactory.getLogger(PouleResource.class);

    private static final String ENTITY_NAME = "navetfootCorePoule";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PouleService pouleService;

    public PouleResource(PouleService pouleService) {
        this.pouleService = pouleService;
    }

    /**
     * {@code POST  /poules} : Create a new poule.
     *
     * @param pouleDTO the pouleDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new pouleDTO, or with status {@code 400 (Bad Request)} if the poule has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/poules")
    public ResponseEntity<PouleDTO> createPoule(@Valid @RequestBody PouleDTO pouleDTO) throws URISyntaxException {
        log.debug("REST request to save Poule : {}", pouleDTO);
        if (pouleDTO.getId() != null) {
            throw new BadRequestAlertException("A new poule cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PouleDTO result = pouleService.save(pouleDTO);
        return ResponseEntity.created(new URI("/api/poules/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /poules} : Updates an existing poule.
     *
     * @param pouleDTO the pouleDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated pouleDTO,
     * or with status {@code 400 (Bad Request)} if the pouleDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the pouleDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/poules")
    public ResponseEntity<PouleDTO> updatePoule(@Valid @RequestBody PouleDTO pouleDTO) throws URISyntaxException {
        log.debug("REST request to update Poule : {}", pouleDTO);
        if (pouleDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PouleDTO result = pouleService.save(pouleDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, pouleDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /poules} : get all the poules.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of poules in body.
     */
    @GetMapping("/poules")
    public ResponseEntity<List<PouleDTO>> getAllPoules(Pageable pageable) {
        log.debug("REST request to get a page of Poules");
        Page<PouleDTO> page = pouleService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /poules/:id} : get the "id" poule.
     *
     * @param id the id of the pouleDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the pouleDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/poules/{id}")
    public ResponseEntity<PouleDTO> getPoule(@PathVariable Long id) {
        log.debug("REST request to get Poule : {}", id);
        Optional<PouleDTO> pouleDTO = pouleService.findOne(id);
        return ResponseUtil.wrapOrNotFound(pouleDTO);
    }

    /**
     * {@code DELETE  /poules/:id} : delete the "id" poule.
     *
     * @param id the id of the pouleDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/poules/{id}")
    public ResponseEntity<Void> deletePoule(@PathVariable Long id) {
        log.debug("REST request to delete Poule : {}", id);
        pouleService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/poules?query=:query} : search for the poule corresponding
     * to the query.
     *
     * @param query the query of the poule search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/poules")
    public ResponseEntity<List<PouleDTO>> searchPoules(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Poules for query {}", query);
        Page<PouleDTO> page = pouleService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
        }
}
