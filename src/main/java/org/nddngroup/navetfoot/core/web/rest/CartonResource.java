package org.nddngroup.navetfoot.core.web.rest;

import org.nddngroup.navetfoot.core.domain.Carton;
import org.nddngroup.navetfoot.core.service.CartonService;
import org.nddngroup.navetfoot.core.service.dto.CartonDTO;
import org.nddngroup.navetfoot.core.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link Carton}.
 */
@RestController
@RequestMapping("/api")
public class CartonResource {

    private final Logger log = LoggerFactory.getLogger(CartonResource.class);

    private static final String ENTITY_NAME = "navetfootCoreCarton";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CartonService cartonService;

    public CartonResource(CartonService cartonService) {
        this.cartonService = cartonService;
    }

    /**
     * {@code POST  /cartons} : Create a new carton.
     *
     * @param cartonDTO the cartonDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new cartonDTO, or with status {@code 400 (Bad Request)} if the carton has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/cartons")
    public ResponseEntity<CartonDTO> createCarton(@RequestBody CartonDTO cartonDTO) throws URISyntaxException {
        log.debug("REST request to save Carton : {}", cartonDTO);
        if (cartonDTO.getId() != null) {
            throw new BadRequestAlertException("A new carton cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CartonDTO result = cartonService.save(cartonDTO);
        return ResponseEntity.created(new URI("/api/cartons/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /cartons} : Updates an existing carton.
     *
     * @param cartonDTO the cartonDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated cartonDTO,
     * or with status {@code 400 (Bad Request)} if the cartonDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the cartonDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/cartons")
    public ResponseEntity<CartonDTO> updateCarton(@RequestBody CartonDTO cartonDTO) throws URISyntaxException {
        log.debug("REST request to update Carton : {}", cartonDTO);
        if (cartonDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CartonDTO result = cartonService.save(cartonDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, cartonDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /cartons} : get all the cartons.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of cartons in body.
     */
    @GetMapping("/cartons")
    public ResponseEntity<List<CartonDTO>> getAllCartons(Pageable pageable) {
        log.debug("REST request to get a page of Cartons");
        Page<CartonDTO> page = cartonService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /cartons/:id} : get the "id" carton.
     *
     * @param id the id of the cartonDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the cartonDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/cartons/{id}")
    public ResponseEntity<CartonDTO> getCarton(@PathVariable Long id) {
        log.debug("REST request to get Carton : {}", id);
        Optional<CartonDTO> cartonDTO = cartonService.findOne(id);
        return ResponseUtil.wrapOrNotFound(cartonDTO);
    }

    /**
     * {@code DELETE  /cartons/:id} : delete the "id" carton.
     *
     * @param id the id of the cartonDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/cartons/{id}")
    public ResponseEntity<Void> deleteCarton(@PathVariable Long id) {
        log.debug("REST request to delete Carton : {}", id);
        cartonService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/cartons?query=:query} : search for the carton corresponding
     * to the query.
     *
     * @param query the query of the carton search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/cartons")
    public ResponseEntity<List<CartonDTO>> searchCartons(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Cartons for query {}", query);
        Page<CartonDTO> page = cartonService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
        }
}
