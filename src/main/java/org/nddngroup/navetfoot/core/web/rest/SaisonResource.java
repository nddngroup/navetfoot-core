package org.nddngroup.navetfoot.core.web.rest;

import org.nddngroup.navetfoot.core.domain.Saison;
import org.nddngroup.navetfoot.core.service.SaisonService;
import org.nddngroup.navetfoot.core.service.dto.SaisonDTO;
import org.nddngroup.navetfoot.core.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link Saison}.
 */
@RestController
@RequestMapping("/api")
public class SaisonResource {

    private final Logger log = LoggerFactory.getLogger(SaisonResource.class);

    private static final String ENTITY_NAME = "navetfootCoreSaison";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SaisonService saisonService;

    public SaisonResource(SaisonService saisonService) {
        this.saisonService = saisonService;
    }

    /**
     * {@code POST  /saisons} : Create a new saison.
     *
     * @param saisonDTO the saisonDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new saisonDTO, or with status {@code 400 (Bad Request)} if the saison has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/saisons")
    public ResponseEntity<SaisonDTO> createSaison(@Valid @RequestBody SaisonDTO saisonDTO) throws URISyntaxException {
        log.debug("REST request to save Saison : {}", saisonDTO);
        if (saisonDTO.getId() != null) {
            throw new BadRequestAlertException("A new saison cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SaisonDTO result = saisonService.save(saisonDTO);
        return ResponseEntity.created(new URI("/api/saisons/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /saisons} : Updates an existing saison.
     *
     * @param saisonDTO the saisonDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated saisonDTO,
     * or with status {@code 400 (Bad Request)} if the saisonDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the saisonDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/saisons")
    public ResponseEntity<SaisonDTO> updateSaison(@Valid @RequestBody SaisonDTO saisonDTO) throws URISyntaxException {
        log.debug("REST request to update Saison : {}", saisonDTO);
        if (saisonDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SaisonDTO result = saisonService.save(saisonDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, saisonDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /saisons} : get all the saisons.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of saisons in body.
     */
    @GetMapping("/saisons")
    public ResponseEntity<List<SaisonDTO>> getAllSaisons(Pageable pageable) {
        log.debug("REST request to get a page of Saisons");
        Page<SaisonDTO> page = saisonService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /saisons/:id} : get the "id" saison.
     *
     * @param id the id of the saisonDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the saisonDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/saisons/{id}")
    public ResponseEntity<SaisonDTO> getSaison(@PathVariable Long id) {
        log.debug("REST request to get Saison : {}", id);
        Optional<SaisonDTO> saisonDTO = saisonService.findOne(id);
        return ResponseUtil.wrapOrNotFound(saisonDTO);
    }

    /**
     * {@code DELETE  /saisons/:id} : delete the "id" saison.
     *
     * @param id the id of the saisonDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/saisons/{id}")
    public ResponseEntity<Void> deleteSaison(@PathVariable Long id) {
        log.debug("REST request to delete Saison : {}", id);
        saisonService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/saisons?query=:query} : search for the saison corresponding
     * to the query.
     *
     * @param query the query of the saison search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/saisons")
    public ResponseEntity<List<SaisonDTO>> searchSaisons(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Saisons for query {}", query);
        Page<SaisonDTO> page = saisonService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
        }
}
