package org.nddngroup.navetfoot.core.web.rest.custom;

import feign.FeignException;
import org.nddngroup.navetfoot.core.domain.DelegueMatch;
import org.nddngroup.navetfoot.core.domain.contant.Constant;
import org.nddngroup.navetfoot.core.exception.ApiRequestException;
import org.nddngroup.navetfoot.core.exception.CustomError;
import org.nddngroup.navetfoot.core.exception.ExceptionCode;
import org.nddngroup.navetfoot.core.exception.ExceptionLevel;
import org.nddngroup.navetfoot.core.service.DelegueMatchService;
import org.nddngroup.navetfoot.core.service.DelegueService;
import org.nddngroup.navetfoot.core.service.MatchService;
import org.nddngroup.navetfoot.core.service.client.FinanceApiClient;
import org.nddngroup.navetfoot.core.service.custom.CommunService;
import org.nddngroup.navetfoot.core.service.custom.NotificationService;
import org.nddngroup.navetfoot.core.service.dto.DelegueMatchDTO;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.web.util.HeaderUtil;
import org.nddngroup.navetfoot.core.service.*;
import org.nddngroup.navetfoot.core.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.ZonedDateTime;
import java.util.List;

/**
 * REST controller for managing {@link DelegueMatch}.
 */
@RestController
@RequestMapping("/api")
public class CustomDelegueMatchResource {

    private final Logger log = LoggerFactory.getLogger(CustomDelegueMatchResource.class);

    private static final String ENTITY_NAME = "navetfootCoreDelegueMatch";

    private static final String NOT_FOUND_DELEGUE = "delegue_inexistant";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DelegueMatchService delegueMatchService;
    @Autowired
    private MatchService matchService;
    @Autowired
    private DelegueService delegueService;
    @Autowired
    private NotificationService notificationService;
    @Autowired
    private CommunService communService;
    @Autowired
    private FinanceApiClient financeApiClient;

    public CustomDelegueMatchResource(DelegueMatchService delegueMatchService) {
        this.delegueMatchService = delegueMatchService;
    }

    /**
     * {@code POST  /organisations/:organisationId/delegue-matchs : Create a new delegueMatch.
     *
     * @param delegueMatchDTO the delegueMatchDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new delegueMatchDTO, or with status {@code 400 (Bad Request)} if the delegueMatch has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/organisations/{organisationId}/delegue-matchs")
    public ResponseEntity<DelegueMatchDTO> createDelegueMatch(@PathVariable Long organisationId,
                                                              @Valid @RequestBody DelegueMatchDTO delegueMatchDTO) throws URISyntaxException {
        log.debug("REST request to save DelegueMatch : {}", delegueMatchDTO);
        if (delegueMatchDTO.getId() != null) {
            throw new BadRequestAlertException("A new delegueMatch cannot already have an ID", ENTITY_NAME, "idexists");
        }

        // find match by id
        var matchDTO = matchService.findOne(delegueMatchDTO.getMatchId())
            .orElseThrow(() -> new ApiRequestException(
                ExceptionCode.MATCH_NOT_FOUND.getMessage(),
                ExceptionCode.MATCH_NOT_FOUND.getValue(),
                ExceptionLevel.ERROR
            ));

        // vérifier si le match a déja un délégué
        delegueMatchService.findByMatch(matchDTO)
            .ifPresent(delegueMatchDTO1 -> {
                throw new ApiRequestException(
                    ExceptionCode.MATCH_ALREADY_HAS_DELEGUE.getMessage(),
                    ExceptionCode.MATCH_ALREADY_HAS_DELEGUE.getValue(),
                    ExceptionLevel.ERROR
                );
            });

        // vérifier si le quota permet de générer un code
        try {
            ResponseEntity response = financeApiClient.debit(organisationId, matchDTO.getId());
            if (response.getStatusCode().is2xxSuccessful() && response.getBody() == null) {
                throw new ApiRequestException(
                    ExceptionCode.UNSUFFISANT_ACCOUNT.getMessage(),
                    ExceptionCode.UNSUFFISANT_ACCOUNT.getValue(),
                    ExceptionLevel.WARNING
                );
            }
        } catch (FeignException e) {
            throw new BadRequestAlertException("Veuillez réessayer plutard !", "CustomDelegueMatchResource", "error-debit-compte");
        }

        delegueMatchDTO.setStatus("disabled");
        delegueMatchDTO.setDeleted(false);
        delegueMatchDTO.setCreatedAt(ZonedDateTime.now());
        delegueMatchDTO.setUpdatedAt(ZonedDateTime.now());

        try {
            delegueMatchDTO.setConfirmCode(communService.randomNumeric(Constant.DELEGUE_CODE_SIZE));
        } catch (Exception e) {
            log.debug("***** Erreur génération code delegue : {}", e.getMessage());
            throw new ApiRequestException(
                ExceptionCode.DELEGUE_CODE_NOT_GENERATED.getMessage(),
                ExceptionCode.DELEGUE_CODE_NOT_GENERATED.getValue(),
                ExceptionLevel.ERROR
            );
        }

        DelegueMatchDTO result = delegueMatchService.save(delegueMatchDTO);

        return ResponseEntity.created(new URI("/organisations/" + organisationId + "/delegue-match/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }


    /**
     * {@code GET  /organisations/:organisationId/matchs/:matchId/delegue-match : get delegueMatch.
     *
     * @param matchId L'id du match
     * @param organisationId L'id de l'organisation
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the delegueMatchDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/organisations/{organisationId}/matchs/{matchId}/delegue-match")
    public ResponseEntity<DelegueMatchDTO> getDelegueMatch(@PathVariable Long matchId, @PathVariable Long organisationId) {
        log.debug("REST request to get DelegueMatch by matchId : {}", matchId);

        // find match
        var matchDTO = matchService.findOne(matchId)
            .orElseThrow(
                () -> new ApiRequestException(
                    ExceptionCode.MATCH_NOT_FOUND.getMessage(),
                    ExceptionCode.MATCH_NOT_FOUND.getValue(),
                    ExceptionLevel.ERROR
                )
            );

        var delegueMatchOptional = delegueMatchService.findByMatch(matchDTO);

        if (delegueMatchOptional.isPresent()) {
            return ResponseEntity.ok(delegueMatchOptional.get());
        }

        return ResponseEntity.ok().build();

    }


    /**
     * {@code GET  /organisations/:organisationId/delegue-matchs/send-code : Create a new delegueMatch.
     *
     * @param delegueMatchDTO the delegueMatchDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new delegueMatchDTO, or with status {@code 400 (Bad Request)} if the delegueMatch has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @GetMapping("/organisations/{organisationId}/matchs/{matchId}/delegue-matchs/send-code")
    public ResponseEntity<Object> delegueMatchSendCode(@PathVariable Long organisationId,
                                                       @PathVariable Long matchId) {

        var matchOptional = this.matchService.findOne(matchId);

        if (matchOptional.isEmpty() || Boolean.TRUE.equals(matchOptional.get().isDeleted())) {
            var error = new CustomError(4000, HttpStatus.BAD_REQUEST, "L'id " + matchId + " ne correspond à aucun match.", "Match inexistant");
            return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
        }

        var matchDTO = matchOptional.get();

        // get delegue match
        var longFilter = new LongFilter();
        var booleanFilter = new BooleanFilter();

        longFilter.setEquals(matchId);
        booleanFilter.setEquals(false);

        var delegueMatchOptional = this.delegueMatchService.findByMatch(matchDTO);

        if (delegueMatchOptional.isEmpty()) {
            var error = new CustomError(4001, HttpStatus.BAD_REQUEST, ExceptionCode.DELEGUE_NOT_FOUND_FOR_MATCH.getMessage(), "");
            return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
        }

        var delegueMatchDTO = delegueMatchOptional.get();

        if (Boolean.TRUE.equals(delegueMatchDTO.isDeleted())) {
            var error = new CustomError(4001, HttpStatus.BAD_REQUEST, ExceptionCode.DELEGUE_NOT_FOUND_FOR_MATCH.getMessage(), NOT_FOUND_DELEGUE);
            return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
        }

        String codeMatch = delegueMatchDTO.getConfirmCode();

        if (codeMatch == null || codeMatch.equals("")) {
            var error = new CustomError(4002, HttpStatus.BAD_REQUEST, "Aucun code d'autorisation pour ce match.", "Code inexistant");
            return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
        }

        var delegueOptional = this.delegueService.findOne(delegueMatchDTO.getDelegueId());

        if (delegueOptional.isEmpty()) {
            var error = new CustomError(4002, HttpStatus.BAD_REQUEST, ExceptionCode.DELEGUE_NOT_FOUND.getMessage(), NOT_FOUND_DELEGUE);
            return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
        }

        // get délégue
        var delegueDTO = delegueOptional.get();

        if (Boolean.TRUE.equals(delegueDTO.isDeleted())) {
            var error = new CustomError(4002, HttpStatus.BAD_REQUEST, ExceptionCode.DELEGUE_NOT_FOUND.getMessage(), NOT_FOUND_DELEGUE);
            return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
        }

        // envoi du code d'authorisation par sms
        this.notificationService.sendCodeMatch(codeMatch, delegueDTO, this.log);

        return new ResponseEntity<>(codeMatch, HttpStatus.OK);
    }


    @GetMapping("/matchs/{matchId}/delegue-match/send-code")
    public ResponseEntity<Object> delegueMatchSendCode(@PathVariable Long matchId) {
        var matchDTO = this.matchService.findOne(matchId).orElseGet(() -> null);

        if (matchDTO == null || matchDTO.isDeleted()) {
            var error = new CustomError(4000, HttpStatus.BAD_REQUEST, "L'id " + matchId + " ne correspond à aucun match.", "Match inexistant");
            return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
        }

        // get delegue match
         var delegueMatchDTO = this.delegueMatchService.findByMatch(matchDTO).orElseGet(() -> null);

        if (delegueMatchDTO == null) {
            var error = new CustomError(4001, HttpStatus.BAD_REQUEST, "Il n'existe pas de délégué pour ce match.", "Délégué inexistant");
            return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
        }

        String codeMatch = delegueMatchDTO.getMatchCode();

        if (codeMatch == null || codeMatch.equals("")) {
            var error = new CustomError(4002, HttpStatus.BAD_REQUEST, "Aucun code d'autorisation pour ce match.", "Code inexistant");
            return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
        }

        // get délégue
        var delegueDTO = this.delegueService.findOne(delegueMatchDTO.getDelegueId()).orElseGet(() -> null);

        if (delegueDTO == null) {
            var error = new CustomError(4002, HttpStatus.BAD_REQUEST, "Délégué n'existe pas.", "Délégué inexistant");
            return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
        }

        // envoi du code d'authorisation par sms
        this.notificationService.sendCodeMatch(codeMatch, delegueDTO, this.log);

        return new ResponseEntity<>(codeMatch, HttpStatus.OK);
    }

    /**
     * {@code DELETE  /organisations/:organisationId/delegue-matches/:id} : delete the "id" delegueMatch.
     *
     * @param organisationId L'id de l'organisation.
     * @param id the id of the delegueMatchDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/organisations/{organisationId}/delegue-matchs/{id}")
    public ResponseEntity<Void> deleteDelegueMatch(@PathVariable Long id,
                                                   @PathVariable Long organisationId) {
        log.debug("REST request to delete DelegueMatch : {}", id);

        // find delegueMatch
        var delegueMatchDTO = delegueMatchService.findOne(id)
            .orElseThrow(
                () -> new ApiRequestException(
                    ExceptionCode.DELEGUE_NOT_FOUND.getMessage(),
                    ExceptionCode.DELEGUE_NOT_FOUND.getValue(),
                    ExceptionLevel.WARNING
                )
            );

        delegueMatchDTO.setUpdatedAt(ZonedDateTime.now());
        delegueMatchDTO.setDeleted(true);

        delegueMatchService.save(delegueMatchDTO);

        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
