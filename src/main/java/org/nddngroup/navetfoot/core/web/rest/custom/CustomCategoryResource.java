package org.nddngroup.navetfoot.core.web.rest.custom;

import org.nddngroup.navetfoot.core.domain.custom.CategorieItem;
import org.nddngroup.navetfoot.core.exception.ApiRequestException;
import org.nddngroup.navetfoot.core.exception.ExceptionCode;
import org.nddngroup.navetfoot.core.exception.ExceptionLevel;
import org.nddngroup.navetfoot.core.service.CategoryService;
import org.nddngroup.navetfoot.core.service.CompetitionSaisonCategoryService;
import org.nddngroup.navetfoot.core.service.custom.CommunService;
import org.nddngroup.navetfoot.core.service.dto.*;
import tech.jhipster.web.util.HeaderUtil;
import org.nddngroup.navetfoot.core.service.dto.*;
import org.nddngroup.navetfoot.core.web.rest.CategoryResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class CustomCategoryResource {

    private final Logger log = LoggerFactory.getLogger(CategoryResource.class);

    private static final String ENTITY_NAME = "navetfootCoreCategory";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CategoryService categoryService;

    @Autowired
    private CommunService communService;
    @Autowired
    private CompetitionSaisonCategoryService competitionSaisonCategoryService;

    public CustomCategoryResource(CategoryService categoryService) {
        this.categoryService = categoryService;
    }


    /**
     * POST  /organisations/:organisationId/saisons/:saisonId/competitions/:competitionId/categories : Add category to competition.
     *
     * @param saisonId the saison id
     * @param competitionId the competition id
     * @param categoryDTO the category
     * @return the ResponseEntity with status 201 (Created) and with body the new categoryDTO, or with status 400 (Bad Request) if the category has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/organisations/{organisationId}/saisons/{saisonId}/competitions/{competitionId}/categories")
    public ResponseEntity<CompetitionSaisonCategoryDTO> addCategoryToCompetition(
        @PathVariable Long organisationId,
        @PathVariable Long saisonId,
        @PathVariable Long competitionId,
        @RequestBody CategoryDTO categoryDTO) throws URISyntaxException {
        log.debug("REST request to add Category '{}' to competiton '{}' for saison '{}'", categoryDTO.getId(), competitionId, saisonId);

        OrganisationDTO organisationDTO = communService.checkOrganisation(organisationId);

        SaisonDTO saisonDTO = communService.checkSaisonForOrganisation(saisonId, organisationDTO);
        CompetitionDTO competitionDTO = communService.checkCompetitionForOrganisation(competitionId, organisationDTO);

        CompetitionSaisonCategoryDTO result = competitionSaisonCategoryService.save(categoryDTO, organisationDTO, saisonDTO, competitionDTO);

        return ResponseEntity.created(new URI("/organisations/" + organisationId + "/saisons/" + saisonId + "/competitions/" + competitionId + "/categories" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * GET  /organisations/:organisationId/saisons/:saisonId/competitions/:competitionId/categories : get all categories for competition.
     *
     * @param organisationId L'id de l'organisation
     * @param saisonId L'id de la saison
     * @param competitionId L'id de la compétition
     * @return the CustomResponse with status 200 (OK) and the list of competitions in body
     */
    @GetMapping("/organisations/{organisationId}/saisons/{saisonId}/competitions/{competitionId}/categories")
    public ResponseEntity<List<CategorieItem>> getCategories(@PathVariable Long organisationId,
                                                             @PathVariable Long saisonId,
                                                             @PathVariable Long competitionId) {
        log.debug("REST request to get categories by competition: {}, and saison: {}", competitionId, saisonId);

        List<CategorieItem> categorieItems = new ArrayList<>();

        // get organisation
        OrganisationDTO organisationDTO = this.communService.checkOrganisation(organisationId);

        // find competition
        CompetitionDTO competitionDTO = communService.checkCompetitionForOrganisation(competitionId, organisationDTO);

        // find saison
        SaisonDTO saisonDTO = communService.checkSaisonForOrganisation(saisonId, organisationDTO);

        // find all categories
        List<CategoryDTO> categoryDTOS = categoryService.findAll();

        if (categoryDTOS == null || categoryDTOS.size() == 0) {
            throw new ApiRequestException(
                ExceptionCode.CATEGORY_LIST_EMPTY.getMessage(),
                ExceptionCode.CATEGORY_LIST_EMPTY.getValue(),
                ExceptionLevel.ERROR
            );
        }

        categoryDTOS.forEach(categoryDTO -> {
            CategorieItem categorieItem = new CategorieItem();
            categorieItem.setCompetitionId(competitionDTO.getId());
            categorieItem.setCategoryDTO(categoryDTO);

            List<CategoryDTO> competitionCategories = communService.getCompetitionSaisonCategories(competitionDTO, saisonDTO);

            categorieItem.setEtat(communService.checkCategoryExist(categoryDTO, competitionCategories));

            categorieItems.add(categorieItem);
        });

        return new ResponseEntity<>(categorieItems, HttpStatus.OK);
    }


    /**
     * DELETE  /organisations/:organisationId/saisons/:saisonId/competitions/:competitionId/categories/:id : delete the "id" category from competition.
     *
     * @param id the id of the categoryDTO to delete
     * @param organisationId L'id de l'organisation
     * @param saisonId L'id de la saison
     * @param competitionId L'id de la compétition
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/organisations/{organisationId}/saisons/{saisonId}/competitions/{competitionId}/categories/{id}")
    public ResponseEntity<Void> deleteCategory(@PathVariable Long id,
                                               @PathVariable Long organisationId,
                                               @PathVariable Long saisonId,
                                               @PathVariable Long competitionId) {
        log.debug("REST request to delete Category {} from competition {} for saison {}", id, competitionId, saisonId);

        // find organisation
        OrganisationDTO organisationDTO = communService.checkOrganisation(organisationId);
        // check saison
        SaisonDTO saisonDTO =communService.checkSaisonForOrganisation(saisonId, organisationDTO);
        // check competition
        CompetitionDTO competitionDTO =communService.checkCompetitionForOrganisation(competitionId, organisationDTO);
        // find competitionSaison
        CompetitionSaisonDTO competitionSaisonDTO = communService.getCompetitionSaison(competitionDTO, saisonDTO);

        // find competitionSaisonCategory
        CompetitionSaisonCategoryDTO competitionSaisonCategoryDTO = this.communService.getCompetitionSaisonCategory(competitionSaisonDTO, id);
        competitionSaisonCategoryDTO.setDeleted(true);
        competitionSaisonCategoryDTO.setUpdatedAt(ZonedDateTime.now());
        competitionSaisonCategoryService.save(competitionSaisonCategoryDTO);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }

    /**
     * {@code DELETE  /categories/:id} : delete the "id" category.
     *
     * @param id the id of the categoryDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/categories/{id}/remove")
    public ResponseEntity<Void> deleteCategory(@PathVariable Long id) {
        log.debug("REST request to delete Category : {}", id);

        // find category by id
        categoryService.deleteCategory(id);

        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
