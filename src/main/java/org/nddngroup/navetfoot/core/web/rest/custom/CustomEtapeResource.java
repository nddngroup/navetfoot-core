package org.nddngroup.navetfoot.core.web.rest.custom;

import org.nddngroup.navetfoot.core.domain.contant.Constant;
import org.nddngroup.navetfoot.core.domain.custom.EtapeForCompetitionSaison;
import org.nddngroup.navetfoot.core.service.EtapeService;
import org.nddngroup.navetfoot.core.service.custom.CommunService;
import org.nddngroup.navetfoot.core.service.dto.*;
import org.nddngroup.navetfoot.core.web.rest.EtapeResource;
import org.nddngroup.navetfoot.core.service.dto.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class CustomEtapeResource {

    private final Logger log = LoggerFactory.getLogger(EtapeResource.class);

    private static final String ENTITY_NAME = "navetfootCoreEtape";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EtapeService etapeService;

    @Autowired
    private CommunService communService;

    public CustomEtapeResource(EtapeService etapeService) {
        this.etapeService = etapeService;
    }

    /**
     * GET  /organisations/:organisationId/saisons/:saisonId/competitions/:competitionId/categories/:categoryId/etapes : get all the etapes by saison and competition.
     *
     * @param organisationId L'id de l'organisation
     * @param saisonId L'id de la saison
     * @param competitionId L'id de la compétition
     * @param categoryId L'id de la catégorie
     * @return the ResponseEntity with status 200 (OK) and the list of etapes in body
     */
    @GetMapping("/organisations/{organisationId}/saisons/{saisonId}/competitions/{competitionId}/categories/{categoryId}/etapes")
    public ResponseEntity<List<EtapeForCompetitionSaison>> getAllEtapes(@PathVariable Long organisationId,
                                                                        @PathVariable Long saisonId,
                                                                        @PathVariable Long competitionId,
                                                                        @PathVariable Long categoryId
    ) {
        log.debug("REST request to get Etapes");
        List<EtapeForCompetitionSaison> etapeForCompetitionSaisons = new ArrayList<>();

        OrganisationDTO organisationDTO = this.communService.checkOrganisation(organisationId);

        // check competition
        CompetitionDTO competitionDTO = this.communService.checkCompetitionForOrganisation(competitionId, organisationDTO);

        // check saison
        SaisonDTO saisonDTO = this.communService.checkSaisonForOrganisation(saisonId, organisationDTO);

        // find all etapeCompetitions
        List<EtapeCompetitionDTO> etapeCompetitionDTOS = this.communService.findEtapeCompetitonsBySaisonAndCompetitionAndCategory(saisonDTO, competitionDTO, categoryId);

        boolean isChampionnat = false;
        for (EtapeCompetitionDTO etapeCompetitionDTO: etapeCompetitionDTOS) {
            if (etapeCompetitionDTO.getNom().equalsIgnoreCase(Constant.CHAMPIONNAT)) {
                isChampionnat = true;
                break;
            }
        }

        List<EtapeDTO> entityList = etapeService.findAll();

        for (EtapeDTO etape: entityList) {

            if (Boolean.TRUE.equals(isChampionnat) || this.communService.etapeInEtapeCompetition(etape, etapeCompetitionDTOS)) {
                etapeForCompetitionSaisons.add(new EtapeForCompetitionSaison(etape, true));
            } else {
                etapeForCompetitionSaisons.add(new EtapeForCompetitionSaison(etape, false));
            }
        }

        return ResponseEntity.ok().body(etapeForCompetitionSaisons);
    }
}
