package org.nddngroup.navetfoot.core.web.rest;

import org.nddngroup.navetfoot.core.domain.Parametrage;
import org.nddngroup.navetfoot.core.service.ParametrageService;
import org.nddngroup.navetfoot.core.service.dto.ParametrageDTO;
import org.nddngroup.navetfoot.core.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link Parametrage}.
 */
@RestController
@RequestMapping("/api")
public class ParametrageResource {

    private final Logger log = LoggerFactory.getLogger(ParametrageResource.class);

    private static final String ENTITY_NAME = "navetfootCoreParametrage";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ParametrageService parametrageService;

    public ParametrageResource(ParametrageService parametrageService) {
        this.parametrageService = parametrageService;
    }

    /**
     * {@code POST  /parametrages} : Create a new parametrage.
     *
     * @param parametrageDTO the parametrageDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new parametrageDTO, or with status {@code 400 (Bad Request)} if the parametrage has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/parametrages")
    public ResponseEntity<ParametrageDTO> createParametrage(@RequestBody ParametrageDTO parametrageDTO) throws URISyntaxException {
        log.debug("REST request to save Parametrage : {}", parametrageDTO);
        if (parametrageDTO.getId() != null) {
            throw new BadRequestAlertException("A new parametrage cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ParametrageDTO result = parametrageService.save(parametrageDTO);
        return ResponseEntity.created(new URI("/api/parametrages/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /parametrages} : Updates an existing parametrage.
     *
     * @param parametrageDTO the parametrageDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated parametrageDTO,
     * or with status {@code 400 (Bad Request)} if the parametrageDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the parametrageDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/parametrages")
    public ResponseEntity<ParametrageDTO> updateParametrage(@RequestBody ParametrageDTO parametrageDTO) throws URISyntaxException {
        log.debug("REST request to update Parametrage : {}", parametrageDTO);
        if (parametrageDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ParametrageDTO result = parametrageService.save(parametrageDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, parametrageDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /parametrages} : get all the parametrages.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of parametrages in body.
     */
    @GetMapping("/parametrages")
    public ResponseEntity<List<ParametrageDTO>> getAllParametrages(Pageable pageable) {
        log.debug("REST request to get a page of Parametrages");
        Page<ParametrageDTO> page = parametrageService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /parametrages/:id} : get the "id" parametrage.
     *
     * @param id the id of the parametrageDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the parametrageDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/parametrages/{id}")
    public ResponseEntity<ParametrageDTO> getParametrage(@PathVariable Long id) {
        log.debug("REST request to get Parametrage : {}", id);
        Optional<ParametrageDTO> parametrageDTO = parametrageService.findOne(id);
        return ResponseUtil.wrapOrNotFound(parametrageDTO);
    }

    /**
     * {@code DELETE  /parametrages/:id} : delete the "id" parametrage.
     *
     * @param id the id of the parametrageDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/parametrages/{id}")
    public ResponseEntity<Void> deleteParametrage(@PathVariable Long id) {
        log.debug("REST request to delete Parametrage : {}", id);
        parametrageService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/parametrages?query=:query} : search for the parametrage corresponding
     * to the query.
     *
     * @param query the query of the parametrage search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/parametrages")
    public ResponseEntity<List<ParametrageDTO>> searchParametrages(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Parametrages for query {}", query);
        Page<ParametrageDTO> page = parametrageService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
        }
}
