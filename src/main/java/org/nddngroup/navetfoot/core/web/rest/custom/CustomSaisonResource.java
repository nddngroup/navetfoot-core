package org.nddngroup.navetfoot.core.web.rest.custom;

import org.nddngroup.navetfoot.core.exception.ApiRequestException;
import org.nddngroup.navetfoot.core.exception.ExceptionCode;
import org.nddngroup.navetfoot.core.exception.ExceptionLevel;
import org.nddngroup.navetfoot.core.service.SaisonService;
import org.nddngroup.navetfoot.core.service.custom.CommunService;
import org.nddngroup.navetfoot.core.service.custom.DeleteService;
import org.nddngroup.navetfoot.core.service.dto.CompetitionDTO;
import org.nddngroup.navetfoot.core.service.dto.OrganisationDTO;
import org.nddngroup.navetfoot.core.service.dto.SaisonDTO;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import org.nddngroup.navetfoot.core.web.rest.RegionResource;
import org.nddngroup.navetfoot.core.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.ZonedDateTime;
import java.util.List;

@RestController
@RequestMapping("/api")
public class CustomSaisonResource {
    private final Logger log = LoggerFactory.getLogger(RegionResource.class);

    private static final String ENTITY_NAME = "navetfootCoreSaison";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SaisonService saisonService;

    @Autowired
    private DeleteService deleteService;
    @Autowired
    private CommunService communService;

    public CustomSaisonResource(SaisonService saisonService) {
        this.saisonService = saisonService;
    }


    /**
     * POST  /organisations/:id/saisons : Create a new saison.
     *
     * @param saisonDTO the saisonDTO to create
     * @return the CustomResponse with status 201 (Created) and with body the new saisonDTO, or with status 400 (Bad Request) if the saison has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/organisations/{id}/saisons")
    public ResponseEntity<SaisonDTO> createSaisonForOrganisation(@PathVariable Long id, @Valid @RequestBody SaisonDTO saisonDTO) throws Exception {
        log.debug("REST request to save Saison : {}", saisonDTO);
        if (saisonDTO.getId() != null) {
            throw new BadRequestAlertException("A new saison cannot already have an ID", ENTITY_NAME, "idexists");
        }

        // get organisation
        OrganisationDTO organisationDTO = this.communService.checkOrganisation(id);

        SaisonDTO result = saisonService.save(organisationDTO, saisonDTO);

        return ResponseEntity.created(new URI("/api/organisations/" + id + "/saisons/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }


    /**
     * POST /organisations/:organisationId/saisons : import list of saisons
     *
     * @param saisonDTOS Liste des saisons
     * @param organisationId L'id de l'organisation
     * @return List<SaisonDTO>
     */
    @PostMapping("/organisations/{organisationId}/import/saisons")
    public ResponseEntity<List<SaisonDTO>> importSaisons(@RequestBody List<SaisonDTO> saisonDTOS,
                                                              @PathVariable Long organisationId
    ) throws URISyntaxException {
        OrganisationDTO organisationDTO = communService.checkOrganisation(organisationId);

        List<SaisonDTO> result = saisonService.save(organisationDTO, saisonDTOS);

        return ResponseEntity.created(new URI("/api/organisations/" + organisationId + "/import/saisons/"))
            .body(result);
    }

    /**
     * PUT  /organisations/:id/saisons : Updates an existing saison.
     *
     * @param saisonDTO the saisonDTO to update
     * @return the CustomResponse with status 200 (OK) and with body the updated saisonDTO,
     * or with status 400 (Bad Request) if the saisonDTO is not valid,
     * or with status 500 (Internal Server Error) if the saisonDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/organisations/{id}/saisons")
    public ResponseEntity<SaisonDTO> updateSaison(@PathVariable Long id, @Valid @RequestBody SaisonDTO saisonDTO) throws URISyntaxException, Exception {
        log.debug("REST request to update Saison : {}", saisonDTO);
        if (saisonDTO.getId() == null) {
            return createSaisonForOrganisation(id, saisonDTO);
        }

        saisonDTO.setUpdatedAt(ZonedDateTime.now());
        SaisonDTO result = saisonService.save(saisonDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }



    /**
     * GET  /organisations/:id/saisons : get all the saisons by organisation.
     *
     * @param pageable the pagination information
     * @return the CustomResponse with status 200 (OK) and the list of saisons in body
     */
    @GetMapping("/organisations/{id}/saisons")
    public ResponseEntity<List<SaisonDTO>> getAllSaisonsByOrganisation(@ApiParam Pageable pageable,
                                                                       @PathVariable Long id) {
        log.debug("REST request to get Saisons by organisation: {}", id);

        /* get organisation */
        OrganisationDTO organisationDTO = this.communService.checkOrganisation(id);

        Page<SaisonDTO> page = saisonService.findAllByOrganisation(organisationDTO, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /organisations/:organisationId/competitions/:competitionId/saisons : get all the saisons.
     *
     * @param competitionId L'id de la compétition
     * @param organisationId L'id de l'organisation
     * @param pageable the pagination information
     * @return the CustomResponse with status 200 (OK) and the list of competitions in body
     */
    @GetMapping("/organisations/{organisationId}/competitions/{competitionId}/saisons")
    public ResponseEntity<List<SaisonDTO>> getAllSaisonsByCompetition(@PathVariable Long competitionId,
                                                                           @PathVariable Long organisationId,
                                                                           @ApiParam Pageable pageable) throws Exception {
        log.debug("REST request to get saisons by competiton: {}", competitionId);

        // get organisation
        OrganisationDTO organisationDTO = this.communService.checkOrganisation(organisationId);

        // find saison
        CompetitionDTO competitionDTO = communService.checkCompetitionForOrganisation(competitionId, organisationDTO);

        List<Long> saisonIds = communService.getSaisonIds(organisationDTO, competitionDTO);

        Page<SaisonDTO> page = saisonService.findByIdIn(saisonIds, pageable);

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /organisations/:id/saisons/all : get all the saisons by organisation.
     *
     * @param id L'id de l'organisation
     * @return the CustomResponse with status 200 (OK) and the list of saisons in body
     */
    @GetMapping("/organisations/{id}/saisons/all")
    public ResponseEntity<List<SaisonDTO>> getAllSaisonsByOrganisation(@PathVariable Long id) {
        log.debug("REST request to get Saisons by organisation: {}", id);

        // get organisation
        OrganisationDTO organisationDTO = this.communService.checkOrganisation(id);

        List<SaisonDTO> saisonDTOS = saisonService.findAllByOrganisation(organisationDTO);
        return ResponseEntity.ok()
            .body(saisonDTOS);
    }

    /**
     * GET  /organisations/:id/saisons/:saisonId/previous : get all the previous saisons of id saison by organisation.
     *
     * @param saisonId L'id de la saison
     * @param id L'id de l'organisation
     * @return the CustomResponse with status 200 (OK) and the list of saisons in body
     */
    @GetMapping("/organisations/{id}/saisons/{saisonId}/previous")
    public ResponseEntity<List<SaisonDTO>> getAllSaisonsByOrganisation(@PathVariable Long saisonId, @PathVariable Long id) {
        log.debug("REST request to get previous Saisons of id saison by organisation: {}", id);

        // get organisation
        OrganisationDTO organisationDTO = this.communService.checkOrganisation(id);

        // find saison
        SaisonDTO saisonDTO = saisonService.findOne(saisonId)
            .orElseThrow(
                () -> new ApiRequestException(
                    ExceptionCode.SAISON_NOT_FOUND.getMessage(),
                    ExceptionCode.SAISON_NOT_FOUND.getValue(),
                    ExceptionLevel.ERROR
                )
            );

        List<SaisonDTO> saisonDTOS = saisonService.findPreviousByOrganisation(saisonDTO, organisationDTO);
        return ResponseEntity.ok()
            .body(saisonDTOS);
    }

    /**
     * DELETE /organisations/:organisationId/saisons : remove list of competitions
     *
     * @param headers HttpHeaders
     * @return List<SaisonDTO>
     */
    @DeleteMapping("/organisations/{organisationId}/saisons")
    public ResponseEntity<List<SaisonDTO>> removeSaisons(@RequestHeader HttpHeaders headers,
                                                                   @PathVariable Long organisationId) throws URISyntaxException {
        OrganisationDTO organisationDTO = communService.checkOrganisation(organisationId);
        List<String> saisonIds = headers.getValuesAsList("ids");

        List<SaisonDTO> result = deleteService.removeSaisonList(organisationDTO, saisonIds);

        return ResponseEntity.created(new URI("/api/organisations/" + organisationId + "/competitions/"))
            .body(result);
    }

    /**
     * DELETE  /organisations/:organisationId/saisons/:id : delete the "id" saison.
     *
     * @param id the id of the saisonDTO to delete
     * @return the CustomResponse with status 200 (OK)
     */
    @DeleteMapping("/organisations/{organisationId}/saisons/{id}")
    public ResponseEntity<Boolean> deleteSaison(@PathVariable Long organisationId,
                                                @PathVariable Long id) {
        log.debug("REST request to delete Saison : {}", id);

        OrganisationDTO organisationDTO = communService.checkOrganisation(organisationId);
        SaisonDTO saisonDTO = communService.checkSaisonForOrganisation(id, organisationDTO);

        deleteService.deleteSaison(saisonDTO);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .body(true);
    }

    /**
     * {@code SEARCH  /_search/organisations/:organisationId/saisons?query=:query} : search for the saison corresponding
     * to the query.
     *
     * @param query the query of the saison search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/organisations/{organisationId}/saisons")
    public ResponseEntity<List<SaisonDTO>> searchSaisons(@RequestParam String query,
                                                         @PathVariable Long organisationId,
                                                         Pageable pageable) {
        log.debug("REST request to search for a page of Saisons for query {}", query);

        // find organisation
        OrganisationDTO organisationDTO = communService.checkOrganisation(organisationId);

        Page<SaisonDTO> page = saisonService.search(organisationDTO, query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
