package org.nddngroup.navetfoot.core.web.rest;

import org.nddngroup.navetfoot.core.domain.DelegueMatch;
import org.nddngroup.navetfoot.core.service.DelegueMatchService;
import org.nddngroup.navetfoot.core.service.dto.DelegueMatchDTO;
import org.nddngroup.navetfoot.core.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link DelegueMatch}.
 */
@RestController
@RequestMapping("/api")
public class DelegueMatchResource {

    private final Logger log = LoggerFactory.getLogger(DelegueMatchResource.class);

    private static final String ENTITY_NAME = "navetfootCoreDelegueMatch";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DelegueMatchService delegueMatchService;

    public DelegueMatchResource(DelegueMatchService delegueMatchService) {
        this.delegueMatchService = delegueMatchService;
    }

    /**
     * {@code POST  /delegue-matches} : Create a new delegueMatch.
     *
     * @param delegueMatchDTO the delegueMatchDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new delegueMatchDTO, or with status {@code 400 (Bad Request)} if the delegueMatch has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/delegue-matches")
    public ResponseEntity<DelegueMatchDTO> createDelegueMatch(@Valid @RequestBody DelegueMatchDTO delegueMatchDTO) throws URISyntaxException {
        log.debug("REST request to save DelegueMatch : {}", delegueMatchDTO);
        if (delegueMatchDTO.getId() != null) {
            throw new BadRequestAlertException("A new delegueMatch cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DelegueMatchDTO result = delegueMatchService.save(delegueMatchDTO);
        return ResponseEntity.created(new URI("/api/delegue-matches/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /delegue-matches} : Updates an existing delegueMatch.
     *
     * @param delegueMatchDTO the delegueMatchDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated delegueMatchDTO,
     * or with status {@code 400 (Bad Request)} if the delegueMatchDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the delegueMatchDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/delegue-matches")
    public ResponseEntity<DelegueMatchDTO> updateDelegueMatch(@Valid @RequestBody DelegueMatchDTO delegueMatchDTO) throws URISyntaxException {
        log.debug("REST request to update DelegueMatch : {}", delegueMatchDTO);
        if (delegueMatchDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DelegueMatchDTO result = delegueMatchService.save(delegueMatchDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, delegueMatchDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /delegue-matches} : get all the delegueMatches.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of delegueMatches in body.
     */
    @GetMapping("/delegue-matches")
    public List<DelegueMatchDTO> getAllDelegueMatches() {
        log.debug("REST request to get all DelegueMatches");
        return delegueMatchService.findAll();
    }

    /**
     * {@code GET  /delegue-matches/:id} : get the "id" delegueMatch.
     *
     * @param id the id of the delegueMatchDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the delegueMatchDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/delegue-matches/{id}")
    public ResponseEntity<DelegueMatchDTO> getDelegueMatch(@PathVariable Long id) {
        log.debug("REST request to get DelegueMatch : {}", id);
        Optional<DelegueMatchDTO> delegueMatchDTO = delegueMatchService.findOne(id);
        return ResponseUtil.wrapOrNotFound(delegueMatchDTO);
    }

    /**
     * {@code DELETE  /delegue-matches/:id} : delete the "id" delegueMatch.
     *
     * @param id the id of the delegueMatchDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/delegue-matches/{id}")
    public ResponseEntity<Void> deleteDelegueMatch(@PathVariable Long id) {
        log.debug("REST request to delete DelegueMatch : {}", id);
        delegueMatchService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/delegue-matches?query=:query} : search for the delegueMatch corresponding
     * to the query.
     *
     * @param query the query of the delegueMatch search.
     * @return the result of the search.
     */
    @GetMapping("/_search/delegue-matches")
    public List<DelegueMatchDTO> searchDelegueMatches(@RequestParam String query) {
        log.debug("REST request to search DelegueMatches for query {}", query);
        return delegueMatchService.search(query);
    }
}
