package org.nddngroup.navetfoot.core.web.rest;

import org.nddngroup.navetfoot.core.domain.EtapeCompetition;
import org.nddngroup.navetfoot.core.service.EtapeCompetitionService;
import org.nddngroup.navetfoot.core.service.dto.EtapeCompetitionDTO;
import org.nddngroup.navetfoot.core.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link EtapeCompetition}.
 */
@RestController
@RequestMapping("/api")
public class EtapeCompetitionResource {

    private final Logger log = LoggerFactory.getLogger(EtapeCompetitionResource.class);

    private static final String ENTITY_NAME = "navetfootCoreEtapeCompetition";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EtapeCompetitionService etapeCompetitionService;

    public EtapeCompetitionResource(EtapeCompetitionService etapeCompetitionService) {
        this.etapeCompetitionService = etapeCompetitionService;
    }

    /**
     * {@code POST  /etape-competitions} : Create a new etapeCompetition.
     *
     * @param etapeCompetitionDTO the etapeCompetitionDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new etapeCompetitionDTO, or with status {@code 400 (Bad Request)} if the etapeCompetition has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/etape-competitions")
    public ResponseEntity<EtapeCompetitionDTO> createEtapeCompetition(@RequestBody EtapeCompetitionDTO etapeCompetitionDTO) throws URISyntaxException {
        log.debug("REST request to save EtapeCompetition : {}", etapeCompetitionDTO);
        if (etapeCompetitionDTO.getId() != null) {
            throw new BadRequestAlertException("A new etapeCompetition cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EtapeCompetitionDTO result = etapeCompetitionService.save(etapeCompetitionDTO);
        return ResponseEntity.created(new URI("/api/etape-competitions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /etape-competitions} : Updates an existing etapeCompetition.
     *
     * @param etapeCompetitionDTO the etapeCompetitionDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated etapeCompetitionDTO,
     * or with status {@code 400 (Bad Request)} if the etapeCompetitionDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the etapeCompetitionDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/etape-competitions")
    public ResponseEntity<EtapeCompetitionDTO> updateEtapeCompetition(@RequestBody EtapeCompetitionDTO etapeCompetitionDTO) throws URISyntaxException {
        log.debug("REST request to update EtapeCompetition : {}", etapeCompetitionDTO);
        if (etapeCompetitionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        EtapeCompetitionDTO result = etapeCompetitionService.save(etapeCompetitionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, etapeCompetitionDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /etape-competitions} : get all the etapeCompetitions.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of etapeCompetitions in body.
     */
    @GetMapping("/etape-competitions")
    public ResponseEntity<List<EtapeCompetitionDTO>> getAllEtapeCompetitions(Pageable pageable) {
        log.debug("REST request to get a page of EtapeCompetitions");
        Page<EtapeCompetitionDTO> page = etapeCompetitionService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /etape-competitions/:id} : get the "id" etapeCompetition.
     *
     * @param id the id of the etapeCompetitionDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the etapeCompetitionDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/etape-competitions/{id}")
    public ResponseEntity<EtapeCompetitionDTO> getEtapeCompetition(@PathVariable Long id) {
        log.debug("REST request to get EtapeCompetition : {}", id);
        Optional<EtapeCompetitionDTO> etapeCompetitionDTO = etapeCompetitionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(etapeCompetitionDTO);
    }

    /**
     * {@code DELETE  /etape-competitions/:id} : delete the "id" etapeCompetition.
     *
     * @param id the id of the etapeCompetitionDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/etape-competitions/{id}")
    public ResponseEntity<Void> deleteEtapeCompetition(@PathVariable Long id) {
        log.debug("REST request to delete EtapeCompetition : {}", id);
        etapeCompetitionService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/etape-competitions?query=:query} : search for the etapeCompetition corresponding
     * to the query.
     *
     * @param query the query of the etapeCompetition search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/etape-competitions")
    public ResponseEntity<List<EtapeCompetitionDTO>> searchEtapeCompetitions(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of EtapeCompetitions for query {}", query);
        Page<EtapeCompetitionDTO> page = etapeCompetitionService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
        }
}
