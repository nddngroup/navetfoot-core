package org.nddngroup.navetfoot.core.web.rest.custom;

import org.nddngroup.navetfoot.core.domain.custom.stats.EtapeDetail;
import org.nddngroup.navetfoot.core.domain.custom.stats.MatchJournee;
import org.nddngroup.navetfoot.core.service.CompetitionService;
import org.nddngroup.navetfoot.core.service.PouleService;
import org.nddngroup.navetfoot.core.service.SaisonService;
import org.nddngroup.navetfoot.core.domain.EtapeCompetition;
import org.nddngroup.navetfoot.core.exception.ApiRequestException;
import org.nddngroup.navetfoot.core.exception.ExceptionCode;
import org.nddngroup.navetfoot.core.exception.ExceptionLevel;
import org.nddngroup.navetfoot.core.service.EtapeCompetitionService;
import org.nddngroup.navetfoot.core.service.custom.CommunService;
import org.nddngroup.navetfoot.core.service.custom.DeleteService;
import org.nddngroup.navetfoot.core.service.dto.*;
import tech.jhipster.web.util.HeaderUtil;
import org.nddngroup.navetfoot.core.service.dto.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * REST controller for managing {@link EtapeCompetition}.
 */
@RestController
@RequestMapping("/api")
public class CustomEtapeCompetitionResource {

    private final Logger log = LoggerFactory.getLogger(CustomEtapeCompetitionResource.class);

    private static final String ENTITY_NAME = "navetfootCoreEtapeCompetition";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EtapeCompetitionService etapeCompetitionService;

    @Autowired
    private CommunService communService;
    @Autowired
    private DeleteService deleteService;
    @Autowired
    private PouleService pouleService;
    @Autowired
    private CompetitionService competitionService;
    @Autowired
    private SaisonService saisonService;

    public CustomEtapeCompetitionResource(EtapeCompetitionService etapeCompetitionService) {
        this.etapeCompetitionService = etapeCompetitionService;
    }


    /**
     * POST  /organisations/:organisationId/saisons/:saisonId/competitions/:competitionId/categories/:categoryId/etape-competitions : Create a new etapeCompetition.
     *
     * @param organisationId the saisonId for competitionSaison
     * @param saisonId the saisonId for competitionSaison
     * @param competitionId the competitionId for competitionSaison
     * @param etapeDTO EtapeDTO
     * @param hasPoule etape has poule or not
     * @param allerRetour etape has status 'allezRetour' or not
     * @return the CustomResponse with status 201 (Created) and with body the new etapeCompetitionDTO, or with status 400 (Bad Request) if the etapeCompetition has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/organisations/{organisationId}/saisons/{saisonId}/competitions/{competitionId}/categories/{categoryId}/etape-competitions")
    public ResponseEntity<EtapeCompetitionDTO> createEtapeCompetition(
                          @PathVariable Long organisationId,
                          @PathVariable Long saisonId,
                          @PathVariable Long competitionId,
                          @PathVariable Long categoryId,
                          @RequestBody EtapeDTO etapeDTO,
                          @RequestParam(defaultValue = "false") boolean hasPoule,
                          @RequestParam(defaultValue = "false") boolean allerRetour) throws Exception {
        log.debug("REST request to save EtapeCompetition : saisonId = {}, competitionId = {}, etapeId = {}", saisonId, competitionId, etapeDTO.getId());

        // get organisation
        OrganisationDTO organisationDTO = this.communService.checkOrganisation(organisationId);

        // check saison
        SaisonDTO saisonDTO = this.communService.checkSaisonForOrganisation(saisonId, organisationDTO);
        // check competition
        CompetitionDTO competitionDTO = this.communService.checkCompetitionForOrganisation(competitionId, organisationDTO);

        EtapeCompetitionDTO result = etapeCompetitionService.save(etapeDTO, organisationDTO, saisonDTO, competitionDTO, categoryId, allerRetour, hasPoule);

        return ResponseEntity.created(new URI("organisations/" + organisationId + "/saisons/" + saisonId + "/competitions/" + competitionId + "/categories/:categoryId/etape-competitions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }



    /**
     * GET  /organisations/:organisationId/saisons/:saisonId/competitions/:competitionId/categories/:categoryId/etape-competitions : get all the etapeCompetitions.
     *
     * @param organisationId L'id de l'organisation
     * @param saisonId L'id de la saison
     * @param competitionId L'id de la competition
     * @param categoryId L'id de la catégorie
     * @return the CustomResponse with status 200 (OK) and the list of etapeCompetitions in body
     */
    @GetMapping("/organisations/{organisationId}/saisons/{saisonId}/competitions/{competitionId}/categories/{categoryId}/etape-competitions")
    public ResponseEntity<List<EtapeCompetitionDTO>> getAllEtapeCompetitionsByCompetitionAndSaison(
                                               @PathVariable(required = false) Long organisationId,
                                               @PathVariable(required = false) Long saisonId,
                                               @PathVariable(required = false) Long competitionId,
                                               @PathVariable(required = false) Long categoryId) throws Exception{
        log.debug("REST request to get EtapeCompetitions");

        List<EtapeCompetitionDTO> etapeCompetitionDTOS = null;

        if (saisonId != null && competitionId != null && categoryId != null) {
            OrganisationDTO organisationDTO = communService.checkOrganisation(organisationId);
            SaisonDTO saisonDTO = communService.checkSaisonForOrganisation(saisonId, organisationDTO);
            CompetitionDTO competitionDTO = communService.checkCompetitionForOrganisation(competitionId, organisationDTO);

            etapeCompetitionDTOS = communService.findEtapeCompetitonsBySaisonAndCompetitionAndCategory(saisonDTO, competitionDTO, categoryId);
        }

        return new ResponseEntity<>(etapeCompetitionDTOS, HttpStatus.OK);
    }

    /**
     * GET  /mobile/saisons/:saisonId/competitions/:competitionId/categories/:categoryId/etape-details : get all the etapeCompetitions.
     *
     * @param saisonId L'id de la saison
     * @param competitionId L'id de la competition
     * @param categoryId L'id de la catégorie
     * @return the CustomResponse with status 200 (OK) and the list of etapeCompetitions in body
     */
    @GetMapping("/mobile/saisons/{saisonId}/competitions/{competitionId}/categories/{categoryId}/etape-details")
    public ResponseEntity<List<EtapeDetail>> getAllEtapeCompetitionsForMobile(
                                               @PathVariable(required = false) Long saisonId,
                                               @PathVariable(required = false) Long competitionId,
                                               @PathVariable(required = false) Long categoryId) {
        log.debug("REST request to get EtapeCompetitions");

        List<EtapeDetail> etapeDetails = new ArrayList<>();

        if (saisonId != null && competitionId != null && categoryId != null) {
            // find saison
            var saisonDTO = saisonService.findOne(saisonId)
                .orElseThrow(() -> new ApiRequestException(
                    ExceptionCode.SAISON_NOT_FOUND.getMessage(),
                    ExceptionCode.SAISON_NOT_FOUND.getValue(),
                    ExceptionLevel.ERROR
                ));

            // find competition
            var competitionDTO = competitionService.findOne(competitionId)
                .orElseThrow(() -> new ApiRequestException(
                    ExceptionCode.COMPETITION_NOT_FOUND.getMessage(),
                    ExceptionCode.COMPETITION_NOT_FOUND.getValue(),
                    ExceptionLevel.ERROR
                ));

            etapeDetails = communService.findEtapeCompetitonsBySaisonAndCompetitionAndCategory(saisonDTO, competitionDTO, categoryId)
                .stream().map(etapeCompetitionDTO -> {
                    var etapeDetail = new EtapeDetail();
                    etapeDetail.setEtape(etapeCompetitionDTO);

                    if (Boolean.TRUE.equals(etapeCompetitionDTO.isHasPoule())) {
                        // find poule
                         pouleService.findByEtapeCompetition(etapeCompetitionDTO)
                            .stream().max(Comparator.comparing(PouleDTO::getNombreJournee)).ifPresent(pouleDTO ->
                                 etapeDetail.setTotalJournee(pouleDTO.getNombreJournee())
                            );

                         // get all matchs of each journee
                        int totalJournees = etapeDetail.getTotalJournee();
                        List<MatchJournee> journees = new ArrayList<>();

                         if (totalJournees != 0) {
                             for (int i = totalJournees; i >= 1; i--) {
                                 var matchJournee = communService.createMatchJournee(i, etapeCompetitionDTO);
                                 journees.add(matchJournee);
                             }

                             etapeDetail.setJournees(journees);
                         }
                    } else {
                        var matchJournee = communService.createMatchJournee(0, etapeCompetitionDTO);
                        etapeDetail.setJournees(Collections.singletonList(matchJournee));
                    }

                    return etapeDetail;
                }).collect(Collectors.toList());
        }

        return new ResponseEntity<>(etapeDetails, HttpStatus.OK);
    }


    /**
     * DELETE  /organisations/:organisationId/saisons/:saisonId/competitions/:competitionId/categories/:categoryId/etape-competitions/last : delete etape for saison and competition.
     *
     * @param organisationId L'id de l'organisation
     * @param saisonId l'id de la saison
     * @param competitionId L'id de la compétition
     * @param categoryId L'id de la catégorie
     * @return the CustomResponse with status 200 (OK)
     */
    @DeleteMapping("/organisations/{organisationId}/saisons/{saisonId}/competitions/{competitionId}/categories/{categoryId}/etape-competitions/last")
    public ResponseEntity<Void> deleteLastEtapeCompetition(@PathVariable Long organisationId,
                                                           @PathVariable Long saisonId,
                                                           @PathVariable Long competitionId,
                                                           @PathVariable Long categoryId) {
        log.debug("REST request to delete last EtapeCompetition : saisonId = {}, competitionId = {}", saisonId, competitionId);

        // get organisation
        OrganisationDTO organisationDTO = this.communService.checkOrganisation(organisationId);

        // check saison for organisation
        SaisonDTO saisonDTO = this.communService.checkSaisonForOrganisation(saisonId, organisationDTO);

        // check competition for organisation
        CompetitionDTO competitionDTO = this.communService.checkCompetitionForOrganisation(competitionId, organisationDTO);

        // get last etape
        List<EtapeCompetitionDTO> etapeCompetitionDTOs = this.communService.findEtapeCompetitonsBySaisonAndCompetitionAndCategory(saisonDTO, competitionDTO, categoryId);

        if (etapeCompetitionDTOs != null && etapeCompetitionDTOs.size() != 0) {
            EtapeCompetitionDTO last;
            if (etapeCompetitionDTOs.size() == 1) {
                last = etapeCompetitionDTOs.get(0);
            } else {
                last = etapeCompetitionDTOs.get(0);
                for (EtapeCompetitionDTO etape: etapeCompetitionDTOs) {
                    if (last != null && etape.getId() > last.getId()) {
                        last = etape;
                    }
                }
            }

            if (last != null) {
                this.deleteService.deleteEtapeCompetition(Collections.singletonList(last));

                return ResponseEntity.ok()
                    .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, last.getId().toString()))
                    .build();
            }
        }

        throw new ApiRequestException(
            ExceptionCode.ETAPE_NOT_FOUND.getMessage(),
            ExceptionCode.ETAPE_NOT_FOUND.getValue(),
            ExceptionLevel.ERROR
        );
    }
}
