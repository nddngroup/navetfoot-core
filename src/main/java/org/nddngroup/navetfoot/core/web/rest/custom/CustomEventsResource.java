package org.nddngroup.navetfoot.core.web.rest.custom;

import org.nddngroup.navetfoot.core.domain.custom.stats.EventsDetail;
import org.nddngroup.navetfoot.core.service.*;
import org.nddngroup.navetfoot.core.service.custom.CommunService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by souleymane91 on 10/11/2018.
 */
@RestController
@RequestMapping("/api")
public class CustomEventsResource {
    private final Logger log = LoggerFactory.getLogger(CustomEventsResource.class);

    private final CommunService communService;

    public CustomEventsResource(CommunService communService) {
        this.communService = communService;
    }

    /**
     * {@code GET  /organisations/:organisationId/matchs/:matchId/events : recupérer tous les évenement d'un match.
     *
     * @param organisationId L'id de l'organisation
     * @param matchId        L'id du match
     * @return EventsDetail
     */
    @GetMapping("/organisations/{organisationId}/matchs/{matchId}/events")
    public ResponseEntity<EventsDetail> getEventsDetails(@PathVariable Long organisationId,
                                                         @PathVariable Long matchId) {
        communService.checkOrganisation(organisationId);

        EventsDetail eventsDetail = communService.getEventsDetails(matchId);

        log.info("____ Event details ____ : {}", eventsDetail);

        return ResponseEntity.ok(eventsDetail);
    }
}
