package org.nddngroup.navetfoot.core.web.rest.custom;

import org.nddngroup.navetfoot.core.service.OrganisationService;
import org.nddngroup.navetfoot.core.service.dto.OrganisationDTO;
import org.nddngroup.navetfoot.core.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import org.nddngroup.navetfoot.core.web.rest.RegionResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.ZonedDateTime;
import java.util.List;

@RestController
@RequestMapping("/api")
public class CustomOrganisationResource {
    private final Logger log = LoggerFactory.getLogger(RegionResource.class);

    private static final String ENTITY_NAME = "navetfootCoreOrganisation";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OrganisationService organisationService;

    public CustomOrganisationResource(OrganisationService organisationService) {
        this.organisationService = organisationService;
    }

    /**
     * {@code GET  /organisations/all} : get all the organisations.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of organisations in body.
     */
    @GetMapping("/organisations/all")
    public ResponseEntity<List<OrganisationDTO>> getAllOrganisations() {
        log.debug("REST request to get a page of Organisations");
        List<OrganisationDTO> organisationDTOS = organisationService.findAllOrganisations();

        return ResponseEntity.ok()
            .body(organisationDTOS);
    }

    /**
     * {@code GET  /users/:id/organisations} : get all the organisations by userId.
     *
     * @param userId the user id.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of organisations in body.
     */
    @GetMapping("/users/{userId}/organisations")
    public ResponseEntity<List<OrganisationDTO>> getAllOrganisations(@PathVariable Long userId) {
        log.debug("REST request to get a page of Organisations");
        List<OrganisationDTO> organisations = organisationService.findAll(userId);

        return ResponseEntity.ok().body(organisations);
    }


    /**
     * {@code POST  /account/organisations} : Create a new organisation.
     *
     * @param organisationDTO the organisationDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new organisationDTO, or with status {@code 400 (Bad Request)} if the organisation has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/account/organisations")
    public ResponseEntity<OrganisationDTO> createOrganisation(@Valid @RequestBody OrganisationDTO organisationDTO) throws URISyntaxException {
        log.debug("REST request to save Organisation : {}", organisationDTO);
        if (organisationDTO.getId() != null) {
            throw new BadRequestAlertException("A new organisation cannot already have an ID", ENTITY_NAME, "idexists");
        }

        organisationDTO.setCreatedAt(ZonedDateTime.now());
        organisationDTO.setUpdatedAt(ZonedDateTime.now());
        organisationDTO.setDeleted(false);

        OrganisationDTO result = organisationService.save(organisationDTO);

        result.setCode("REF_" + result.getId());
        result = organisationService.save(result);

        return ResponseEntity.created(new URI("/api/account/organisations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

}
