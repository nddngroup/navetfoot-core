package org.nddngroup.navetfoot.core.web.rest.custom;

import org.nddngroup.navetfoot.core.domain.Poule;
import org.nddngroup.navetfoot.core.domain.custom.PouleWithAssociation;
import org.nddngroup.navetfoot.core.domain.custom.stats.DetailPouleDetail;
import org.nddngroup.navetfoot.core.domain.custom.stats.PouleDetail;
import org.nddngroup.navetfoot.core.exception.ApiRequestException;
import org.nddngroup.navetfoot.core.exception.ExceptionCode;
import org.nddngroup.navetfoot.core.exception.ExceptionLevel;
import org.nddngroup.navetfoot.core.service.AssociationService;
import org.nddngroup.navetfoot.core.service.DetailPouleService;
import org.nddngroup.navetfoot.core.service.EtapeCompetitionService;
import org.nddngroup.navetfoot.core.service.PouleService;
import org.nddngroup.navetfoot.core.service.custom.CommunService;
import org.nddngroup.navetfoot.core.service.dto.*;
import tech.jhipster.web.util.HeaderUtil;
import org.nddngroup.navetfoot.core.service.dto.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * REST controller for managing {@link Poule}.
 */
@RestController
@RequestMapping("/api")
public class CustomPouleResource {

    private final Logger log = LoggerFactory.getLogger(CustomPouleResource.class);

    private static final String ENTITY_NAME = "navetfootCorePoule";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PouleService pouleService;
    @Autowired
    private CommunService communService;
    @Autowired
    private AssociationService associationService;
    @Autowired
    private EtapeCompetitionService etapeCompetitionService;
    @Autowired
    private DetailPouleService detailPouleService;

    public CustomPouleResource(PouleService pouleService) {
        this.pouleService = pouleService;
    }


    /**
     * POST  /organisations/:id/poules : Create a new poule.
     *
     * @param pouleWithAssociation the pouleDTO to create with associations
     * @return the ResponseEntity with status 201 (Created) and with body the new pouleDTO, or with status 400 (Bad Request) if the poule has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/organisations/{id}/poules")
    public ResponseEntity<PouleDTO> createPoule(@PathVariable Long id,
                                                @Valid @RequestBody PouleWithAssociation pouleWithAssociation) throws URISyntaxException {
        log.debug("REST request to save Poule : {}", pouleWithAssociation);

        // get organisation
        OrganisationDTO organisationDTO = this.communService.checkOrganisation(id);
        PouleDTO pouleDTO = pouleWithAssociation.getPoule();
        pouleDTO.setCreatedAt(ZonedDateTime.now());
        PouleDTO result = pouleService.save(pouleDTO, pouleWithAssociation.getAssociations(), organisationDTO);

        return ResponseEntity.created(new URI("/api/organisations/" + id + "/poules/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /organisations/:organisationId/etape-competitions/:etapeCompetitionId/poules} : get all the poules.
     *
     * @param organisationId L'id de l'organisation
     * @param etapeCompetitionId L'id de l'étape
     * @return List pouleDetails
     */
    @GetMapping("/organisations/{organisationId}/etape-competitions/{etapeCompetitionId}/poules")
    public ResponseEntity<List<PouleDetail>> getPoulesDetails(@PathVariable Long organisationId, @PathVariable Long etapeCompetitionId) {
        List<PouleDetail> pouleDetails = new ArrayList<>();

        OrganisationDTO organisationDTO = communService.checkOrganisation(organisationId);

        // find etapeCompetition by id
        EtapeCompetitionDTO etapeCompetitionDTO = etapeCompetitionService.findOne(etapeCompetitionId)
            .orElseThrow(
                () -> new ApiRequestException(
                    ExceptionCode.ETAPE_NOT_FOUND.getMessage(),
                    ExceptionCode.ETAPE_NOT_FOUND.getValue(),
                    ExceptionLevel.ERROR
                )
            );

        //get all poules
        List<PouleDTO> pouleDTOS = this.pouleService.findByEtapeCompetition(etapeCompetitionDTO);

        if(pouleDTOS != null) {
            for(PouleDTO pouleDTO: pouleDTOS) {
                PouleDetail pouleDetail = new PouleDetail();
                pouleDetail.setPouleDTO(pouleDTO);

                //get details poules
                List<DetailPouleDTO> detailPouleDTOS = this.detailPouleService.findByPoule(pouleDTO);

                if(detailPouleDTOS != null) {
                    HashMap<String, List<DetailPouleDTO>> pouleItems = new HashMap<>();
                    List<String> equipes = new ArrayList<>();
                    List<String> images  = new ArrayList<>();

                    for(DetailPouleDTO detailPouleDTO: detailPouleDTOS) {

                        //get association name
                        ;

                        if(this.associationService.findOne(detailPouleDTO.getAssociationId()).isPresent()) {
                            AssociationDTO associationDTO = this.associationService.findOne(detailPouleDTO.getAssociationId()).get();
                            String name = associationDTO.getNom();
                            String image = associationDTO.getCouleurs();

                            if(!equipes.contains(name)) {
                                equipes.add(name);
                                images.add(image);
                            }

                            List<DetailPouleDTO> items = pouleItems.get(name);
                            if(items == null) {
                                items = new ArrayList<>();
                            }

                            items.add(detailPouleDTO);
                            pouleItems.put(name, items);
                        }
                    }

                    List<DetailPouleDetail> detailPouleDetails = new ArrayList<>();

                    int i = 0;

                    for(String equipe: equipes) {
                        List<DetailPouleDTO> details = pouleItems.get(equipe);
                        DetailPouleDetail detailPouleDetail = new DetailPouleDetail();

                        String image = images.get(i);
                        i++;
                        detailPouleDetail.setImage(image);
                        detailPouleDetail.setEquipe(equipe);
                        detailPouleDetail.setDetailsPouleDTO(details);

                        detailPouleDetails.add(detailPouleDetail);
                    }


                    pouleDetail.setDetailPouleDetails(detailPouleDetails);
                }

                pouleDetails.add(pouleDetail);
            }
        }

        return new ResponseEntity<>(pouleDetails, HttpStatus.OK);
    }


    /**
     * DELETE  /organisations/:organisationId/poules/:id : delete the "id" poule.
     *
     * @param id the id of the pouleDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/organisations/{organisationId}/poules/{id}")
    public ResponseEntity<Void> deletePouleForOrganisation(@PathVariable Long organisationId, @PathVariable Long id) {
        log.debug("REST request to delete Poule : {}", id);

        // find organisation
        OrganisationDTO organisationDTO = this.communService.checkOrganisation(organisationId);

        // find poule by id
        PouleDTO pouleDTO = this.pouleService.findOne(id)
            .orElseThrow(() ->  new ApiRequestException(
                ExceptionCode.POULE_NOT_FOUND.getMessage(),
                ExceptionCode.POULE_NOT_FOUND.getValue(),
                ExceptionLevel.ERROR
            ));

        // find etapeCompetition
        EtapeCompetitionDTO etapeCompetitionDTO = this.etapeCompetitionService.findOne(pouleDTO.getEtapeCompetitionId())
            .orElseThrow(() ->  new ApiRequestException(
                ExceptionCode.ETAPE_COMPETITION_NOT_FOUND_FOR_POULE.getMessage(),
                ExceptionCode.ETAPE_COMPETITION_NOT_FOUND_FOR_POULE.getValue(),
                ExceptionLevel.ERROR
            ));


        if (this.communService.checkPouleHasMatch(pouleDTO, etapeCompetitionDTO, organisationDTO)) {
            throw new ApiRequestException(
                ExceptionCode.POULE_HAS_MATCHS.getMessage(),
                ExceptionCode.POULE_HAS_MATCHS.getValue(),
                ExceptionLevel.WARNING
            );
        }

        pouleDTO.setDeleted(true);
        pouleDTO.setUpdatedAt(ZonedDateTime.now());

        this.pouleService.save(pouleDTO);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
