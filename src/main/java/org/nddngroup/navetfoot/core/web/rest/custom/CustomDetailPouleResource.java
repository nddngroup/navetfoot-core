package org.nddngroup.navetfoot.core.web.rest.custom;

import org.nddngroup.navetfoot.core.exception.ApiRequestException;
import org.nddngroup.navetfoot.core.exception.ExceptionCode;
import org.nddngroup.navetfoot.core.exception.ExceptionLevel;
import org.nddngroup.navetfoot.core.service.DetailPouleService;
import org.nddngroup.navetfoot.core.service.PouleService;
import org.nddngroup.navetfoot.core.service.dto.DetailPouleDTO;
import org.nddngroup.navetfoot.core.service.dto.PouleDTO;
import org.nddngroup.navetfoot.core.domain.DetailPoule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * REST controller for managing {@link DetailPoule}.
 */
@RestController
@RequestMapping("/api")
public class CustomDetailPouleResource {

    private final Logger log = LoggerFactory.getLogger(CustomDetailPouleResource.class);

    private static final String ENTITY_NAME = "navetfootCoreDetailPoule";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DetailPouleService detailPouleService;
    @Autowired
    private PouleService pouleService;

    public CustomDetailPouleResource(DetailPouleService detailPouleService) {
        this.detailPouleService = detailPouleService;
    }

    /**
     * {@code GET  /organisations/:organisationId/poules/:pouleId/detail-poules} : get all the detailPoules.
     *
     * @param organisationId L'id de l'organisation.
     * @param pouleId L'id de la poule.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of detailPoules in body.
     */
    @GetMapping("/organisations/{organisationId}/poules/{pouleId}/detail-poules")
    public ResponseEntity<List<DetailPouleDTO>> getAllDetailPoules(@PathVariable Long organisationId,
                                                                   @PathVariable Long pouleId) {
        log.debug("REST request to get a list of DetailPoules");

        // get poule
        PouleDTO pouleDTO = pouleService.findOne(pouleId)
            .orElseThrow(
                () -> new ApiRequestException(
                    ExceptionCode.POULE_NOT_FOUND.getMessage(),
                    ExceptionCode.POULE_NOT_FOUND.getValue(),
                    ExceptionLevel.ERROR
                )
            );

        List<DetailPouleDTO> detailPouleDTOS = detailPouleService.findByPoule(pouleDTO);

        return ResponseEntity.ok().body(detailPouleDTOS);
    }

}
