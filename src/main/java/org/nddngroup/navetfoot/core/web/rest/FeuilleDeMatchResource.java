package org.nddngroup.navetfoot.core.web.rest;

import org.nddngroup.navetfoot.core.domain.FeuilleDeMatch;
import org.nddngroup.navetfoot.core.service.FeuilleDeMatchService;
import org.nddngroup.navetfoot.core.service.dto.FeuilleDeMatchDTO;
import org.nddngroup.navetfoot.core.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link FeuilleDeMatch}.
 */
@RestController
@RequestMapping("/api")
public class FeuilleDeMatchResource {

    private final Logger log = LoggerFactory.getLogger(FeuilleDeMatchResource.class);

    private static final String ENTITY_NAME = "navetfootCoreFeuilleDeMatch";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FeuilleDeMatchService feuilleDeMatchService;

    public FeuilleDeMatchResource(FeuilleDeMatchService feuilleDeMatchService) {
        this.feuilleDeMatchService = feuilleDeMatchService;
    }

    /**
     * {@code POST  /feuille-de-matches} : Create a new feuilleDeMatch.
     *
     * @param feuilleDeMatchDTO the feuilleDeMatchDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new feuilleDeMatchDTO, or with status {@code 400 (Bad Request)} if the feuilleDeMatch has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/feuille-de-matches")
    public ResponseEntity<FeuilleDeMatchDTO> createFeuilleDeMatch(@RequestBody FeuilleDeMatchDTO feuilleDeMatchDTO) throws URISyntaxException {
        log.debug("REST request to save FeuilleDeMatch : {}", feuilleDeMatchDTO);
        if (feuilleDeMatchDTO.getId() != null) {
            throw new BadRequestAlertException("A new feuilleDeMatch cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FeuilleDeMatchDTO result = feuilleDeMatchService.save(feuilleDeMatchDTO);
        return ResponseEntity.created(new URI("/api/feuille-de-matches/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /feuille-de-matches} : Updates an existing feuilleDeMatch.
     *
     * @param feuilleDeMatchDTO the feuilleDeMatchDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated feuilleDeMatchDTO,
     * or with status {@code 400 (Bad Request)} if the feuilleDeMatchDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the feuilleDeMatchDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/feuille-de-matches")
    public ResponseEntity<FeuilleDeMatchDTO> updateFeuilleDeMatch(@RequestBody FeuilleDeMatchDTO feuilleDeMatchDTO) throws URISyntaxException {
        log.debug("REST request to update FeuilleDeMatch : {}", feuilleDeMatchDTO);
        if (feuilleDeMatchDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        FeuilleDeMatchDTO result = feuilleDeMatchService.save(feuilleDeMatchDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, feuilleDeMatchDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /feuille-de-matches} : get all the feuilleDeMatches.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of feuilleDeMatches in body.
     */
    @GetMapping("/feuille-de-matches")
    public ResponseEntity<List<FeuilleDeMatchDTO>> getAllFeuilleDeMatches(Pageable pageable) {
        log.debug("REST request to get a page of FeuilleDeMatches");
        Page<FeuilleDeMatchDTO> page = feuilleDeMatchService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /feuille-de-matches/:id} : get the "id" feuilleDeMatch.
     *
     * @param id the id of the feuilleDeMatchDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the feuilleDeMatchDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/feuille-de-matches/{id}")
    public ResponseEntity<FeuilleDeMatchDTO> getFeuilleDeMatch(@PathVariable Long id) {
        log.debug("REST request to get FeuilleDeMatch : {}", id);
        Optional<FeuilleDeMatchDTO> feuilleDeMatchDTO = feuilleDeMatchService.findOne(id);
        return ResponseUtil.wrapOrNotFound(feuilleDeMatchDTO);
    }

    /**
     * {@code DELETE  /feuille-de-matches/:id} : delete the "id" feuilleDeMatch.
     *
     * @param id the id of the feuilleDeMatchDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/feuille-de-matches/{id}")
    public ResponseEntity<Void> deleteFeuilleDeMatch(@PathVariable Long id) {
        log.debug("REST request to delete FeuilleDeMatch : {}", id);
        feuilleDeMatchService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/feuille-de-matches?query=:query} : search for the feuilleDeMatch corresponding
     * to the query.
     *
     * @param query the query of the feuilleDeMatch search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/feuille-de-matches")
    public ResponseEntity<List<FeuilleDeMatchDTO>> searchFeuilleDeMatches(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of FeuilleDeMatches for query {}", query);
        Page<FeuilleDeMatchDTO> page = feuilleDeMatchService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
        }
}
