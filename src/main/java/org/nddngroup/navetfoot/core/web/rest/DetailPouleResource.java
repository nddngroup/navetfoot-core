package org.nddngroup.navetfoot.core.web.rest;

import org.nddngroup.navetfoot.core.domain.DetailPoule;
import org.nddngroup.navetfoot.core.service.DetailPouleService;
import org.nddngroup.navetfoot.core.service.dto.DetailPouleDTO;
import org.nddngroup.navetfoot.core.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link DetailPoule}.
 */
@RestController
@RequestMapping("/api")
public class DetailPouleResource {

    private final Logger log = LoggerFactory.getLogger(DetailPouleResource.class);

    private static final String ENTITY_NAME = "navetfootCoreDetailPoule";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DetailPouleService detailPouleService;

    public DetailPouleResource(DetailPouleService detailPouleService) {
        this.detailPouleService = detailPouleService;
    }

    /**
     * {@code POST  /detail-poules} : Create a new detailPoule.
     *
     * @param detailPouleDTO the detailPouleDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new detailPouleDTO, or with status {@code 400 (Bad Request)} if the detailPoule has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/detail-poules")
    public ResponseEntity<DetailPouleDTO> createDetailPoule(@RequestBody DetailPouleDTO detailPouleDTO) throws URISyntaxException {
        log.debug("REST request to save DetailPoule : {}", detailPouleDTO);
        if (detailPouleDTO.getId() != null) {
            throw new BadRequestAlertException("A new detailPoule cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DetailPouleDTO result = detailPouleService.save(detailPouleDTO);
        return ResponseEntity.created(new URI("/api/detail-poules/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /detail-poules} : Updates an existing detailPoule.
     *
     * @param detailPouleDTO the detailPouleDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated detailPouleDTO,
     * or with status {@code 400 (Bad Request)} if the detailPouleDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the detailPouleDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/detail-poules")
    public ResponseEntity<DetailPouleDTO> updateDetailPoule(@RequestBody DetailPouleDTO detailPouleDTO) throws URISyntaxException {
        log.debug("REST request to update DetailPoule : {}", detailPouleDTO);
        if (detailPouleDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DetailPouleDTO result = detailPouleService.save(detailPouleDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, detailPouleDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /detail-poules} : get all the detailPoules.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of detailPoules in body.
     */
    @GetMapping("/detail-poules")
    public ResponseEntity<List<DetailPouleDTO>> getAllDetailPoules(Pageable pageable) {
        log.debug("REST request to get a page of DetailPoules");
        Page<DetailPouleDTO> page = detailPouleService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /detail-poules/:id} : get the "id" detailPoule.
     *
     * @param id the id of the detailPouleDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the detailPouleDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/detail-poules/{id}")
    public ResponseEntity<DetailPouleDTO> getDetailPoule(@PathVariable Long id) {
        log.debug("REST request to get DetailPoule : {}", id);
        Optional<DetailPouleDTO> detailPouleDTO = detailPouleService.findOne(id);
        return ResponseUtil.wrapOrNotFound(detailPouleDTO);
    }

    /**
     * {@code DELETE  /detail-poules/:id} : delete the "id" detailPoule.
     *
     * @param id the id of the detailPouleDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/detail-poules/{id}")
    public ResponseEntity<Void> deleteDetailPoule(@PathVariable Long id) {
        log.debug("REST request to delete DetailPoule : {}", id);
        detailPouleService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/detail-poules?query=:query} : search for the detailPoule corresponding
     * to the query.
     *
     * @param query the query of the detailPoule search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/detail-poules")
    public ResponseEntity<List<DetailPouleDTO>> searchDetailPoules(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of DetailPoules for query {}", query);
        Page<DetailPouleDTO> page = detailPouleService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
        }
}
