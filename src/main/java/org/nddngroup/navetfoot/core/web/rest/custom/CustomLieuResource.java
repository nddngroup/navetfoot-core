package org.nddngroup.navetfoot.core.web.rest.custom;

import org.nddngroup.navetfoot.core.domain.Pays;
import org.nddngroup.navetfoot.core.service.DepartementService;
import org.nddngroup.navetfoot.core.service.PaysService;
import org.nddngroup.navetfoot.core.service.RegionService;
import org.nddngroup.navetfoot.core.service.dto.DepartementDTO;
import org.nddngroup.navetfoot.core.service.dto.PaysDTO;
import org.nddngroup.navetfoot.core.service.dto.RegionDTO;
import org.nddngroup.navetfoot.core.web.rest.RegionResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class CustomLieuResource {
    private final Logger log = LoggerFactory.getLogger(RegionResource.class);

    private static final String ENTITY_NAME = "navetfootCoreLieu";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    @Autowired
    private PaysService paysService;

    private final RegionService regionService;
    @Autowired
    DepartementService departementService;

    public CustomLieuResource(RegionService regionService) {
        this.regionService = regionService;
    }

    /**
     * {@code GET  /pays/all} : get all the pays.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of pays in body.
     */
    @GetMapping("/pays/all")
    public ResponseEntity<List<Pays>> getAllRegions() {
        log.debug("REST request to get pays");

        List<Pays> pays = paysService.findAll();

        return new ResponseEntity<>(pays, HttpStatus.OK);
    }

    /**
     * {@code GET  /pays/:paysId/regions} : get all the regions by pays id.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of regions in body.
     */
    @GetMapping("/pays/{paysId}/regions")
    public ResponseEntity<List<RegionDTO>> getAllRegions(@PathVariable Long paysId) throws Exception {
        log.debug("REST request to get regions by paysID");

        PaysDTO paysDTO = paysService.findOne(paysId).orElseThrow(() -> new Exception("Pays non trouvée : id = " + paysId));

        if(paysDTO == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        List<RegionDTO> regionDTOS = regionService.findRegionsByPays(paysDTO);

        return new ResponseEntity<>(regionDTOS, HttpStatus.OK);
    }

    /**
     * {@code GET  /regions/:regionId/departements} : get all the departements by region id.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of regions in body.
     */
    @GetMapping("/regions/{regionId}/departements")
    public ResponseEntity<List<DepartementDTO>> getAllDepartements(@PathVariable Long regionId) throws Exception {
        log.debug("REST request to get departements by regionId");

        RegionDTO region = regionService.findOne(regionId).orElseThrow(() -> new Exception("Région non trouvée : id = " + regionId));

        if(region == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        List<DepartementDTO> departementDTOS = departementService.findDepartementsByRegion(region);

        return new ResponseEntity<>(departementDTOS, HttpStatus.OK);
    }
}
