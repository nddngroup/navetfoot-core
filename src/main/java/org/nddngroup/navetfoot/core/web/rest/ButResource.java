package org.nddngroup.navetfoot.core.web.rest;

import org.nddngroup.navetfoot.core.domain.But;
import org.nddngroup.navetfoot.core.service.ButService;
import org.nddngroup.navetfoot.core.service.dto.ButDTO;
import org.nddngroup.navetfoot.core.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link But}.
 */
@RestController
@RequestMapping("/api")
public class ButResource {

    private final Logger log = LoggerFactory.getLogger(ButResource.class);

    private static final String ENTITY_NAME = "navetfootCoreBut";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ButService butService;

    public ButResource(ButService butService) {
        this.butService = butService;
    }

    /**
     * {@code POST  /buts} : Create a new but.
     *
     * @param butDTO the butDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new butDTO, or with status {@code 400 (Bad Request)} if the but has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/buts")
    public ResponseEntity<ButDTO> createBut(@RequestBody ButDTO butDTO) throws URISyntaxException {
        log.debug("REST request to save But : {}", butDTO);
        if (butDTO.getId() != null) {
            throw new BadRequestAlertException("A new but cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ButDTO result = butService.save(butDTO);
        return ResponseEntity.created(new URI("/api/buts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /buts} : Updates an existing but.
     *
     * @param butDTO the butDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated butDTO,
     * or with status {@code 400 (Bad Request)} if the butDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the butDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/buts")
    public ResponseEntity<ButDTO> updateBut(@RequestBody ButDTO butDTO) throws URISyntaxException {
        log.debug("REST request to update But : {}", butDTO);
        if (butDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ButDTO result = butService.save(butDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, butDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /buts} : get all the buts.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of buts in body.
     */
    @GetMapping("/buts")
    public ResponseEntity<List<ButDTO>> getAllButs(Pageable pageable) {
        log.debug("REST request to get a page of Buts");
        Page<ButDTO> page = butService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /buts/:id} : get the "id" but.
     *
     * @param id the id of the butDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the butDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/buts/{id}")
    public ResponseEntity<ButDTO> getBut(@PathVariable Long id) {
        log.debug("REST request to get But : {}", id);
        Optional<ButDTO> butDTO = butService.findOne(id);
        return ResponseUtil.wrapOrNotFound(butDTO);
    }

    /**
     * {@code DELETE  /buts/:id} : delete the "id" but.
     *
     * @param id the id of the butDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/buts/{id}")
    public ResponseEntity<Void> deleteBut(@PathVariable Long id) {
        log.debug("REST request to delete But : {}", id);
        butService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/buts?query=:query} : search for the but corresponding
     * to the query.
     *
     * @param query the query of the but search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/buts")
    public ResponseEntity<List<ButDTO>> searchButs(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Buts for query {}", query);
        Page<ButDTO> page = butService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
        }
}
