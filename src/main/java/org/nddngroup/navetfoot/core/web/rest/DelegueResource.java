package org.nddngroup.navetfoot.core.web.rest;

import org.nddngroup.navetfoot.core.domain.Delegue;
import org.nddngroup.navetfoot.core.service.DelegueService;
import org.nddngroup.navetfoot.core.service.dto.DelegueDTO;
import org.nddngroup.navetfoot.core.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link Delegue}.
 */
@RestController
@RequestMapping("/api")
public class DelegueResource {

    private final Logger log = LoggerFactory.getLogger(DelegueResource.class);

    private static final String ENTITY_NAME = "navetfootCoreDelegue";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DelegueService delegueService;

    public DelegueResource(DelegueService delegueService) {
        this.delegueService = delegueService;
    }

    /**
     * {@code POST  /delegues} : Create a new delegue.
     *
     * @param delegueDTO the delegueDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new delegueDTO, or with status {@code 400 (Bad Request)} if the delegue has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/delegues")
    public ResponseEntity<DelegueDTO> createDelegue(@Valid @RequestBody DelegueDTO delegueDTO) throws URISyntaxException {
        log.debug("REST request to save Delegue : {}", delegueDTO);
        if (delegueDTO.getId() != null) {
            throw new BadRequestAlertException("A new delegue cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DelegueDTO result = delegueService.save(delegueDTO);
        return ResponseEntity.created(new URI("/api/delegues/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /delegues} : Updates an existing delegue.
     *
     * @param delegueDTO the delegueDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated delegueDTO,
     * or with status {@code 400 (Bad Request)} if the delegueDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the delegueDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/delegues")
    public ResponseEntity<DelegueDTO> updateDelegue(@Valid @RequestBody DelegueDTO delegueDTO) throws URISyntaxException {
        log.debug("REST request to update Delegue : {}", delegueDTO);
        if (delegueDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DelegueDTO result = delegueService.save(delegueDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, delegueDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /delegues} : get all the delegues.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of delegues in body.
     */
    @GetMapping("/delegues")
    public ResponseEntity<List<DelegueDTO>> getAllDelegues(Pageable pageable) {
        log.debug("REST request to get a page of Delegues");
        Page<DelegueDTO> page = delegueService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /delegues/:id} : get the "id" delegue.
     *
     * @param id the id of the delegueDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the delegueDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/delegues/{id}")
    public ResponseEntity<DelegueDTO> getDelegue(@PathVariable Long id) {
        log.debug("REST request to get Delegue : {}", id);
        Optional<DelegueDTO> delegueDTO = delegueService.findOne(id);
        return ResponseUtil.wrapOrNotFound(delegueDTO);
    }

    /**
     * {@code DELETE  /delegues/:id} : delete the "id" delegue.
     *
     * @param id the id of the delegueDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/delegues/{id}")
    public ResponseEntity<Void> deleteDelegue(@PathVariable Long id) {
        log.debug("REST request to delete Delegue : {}", id);
        delegueService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/delegues?query=:query} : search for the delegue corresponding
     * to the query.
     *
     * @param query the query of the delegue search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/delegues")
    public ResponseEntity<List<DelegueDTO>> searchDelegues(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Delegues for query {}", query);
        Page<DelegueDTO> page = delegueService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
        }
}
