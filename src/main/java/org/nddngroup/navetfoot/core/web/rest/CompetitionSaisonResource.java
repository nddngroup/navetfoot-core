package org.nddngroup.navetfoot.core.web.rest;

import org.nddngroup.navetfoot.core.service.CompetitionSaisonService;
import org.nddngroup.navetfoot.core.web.rest.errors.BadRequestAlertException;
import org.nddngroup.navetfoot.core.service.dto.CompetitionSaisonDTO;

import org.nddngroup.navetfoot.core.domain.CompetitionSaison;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link CompetitionSaison}.
 */
@RestController
@RequestMapping("/api")
public class CompetitionSaisonResource {

    private final Logger log = LoggerFactory.getLogger(CompetitionSaisonResource.class);

    private static final String ENTITY_NAME = "navetfootCoreCompetitionSaison";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CompetitionSaisonService competitionSaisonService;

    public CompetitionSaisonResource(CompetitionSaisonService competitionSaisonService) {
        this.competitionSaisonService = competitionSaisonService;
    }

    /**
     * {@code POST  /competition-saisons} : Create a new competitionSaison.
     *
     * @param competitionSaisonDTO the competitionSaisonDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new competitionSaisonDTO, or with status {@code 400 (Bad Request)} if the competitionSaison has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/competition-saisons")
    public ResponseEntity<CompetitionSaisonDTO> createCompetitionSaison(@RequestBody CompetitionSaisonDTO competitionSaisonDTO) throws URISyntaxException {
        log.debug("REST request to save CompetitionSaison : {}", competitionSaisonDTO);
        if (competitionSaisonDTO.getId() != null) {
            throw new BadRequestAlertException("A new competitionSaison cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CompetitionSaisonDTO result = competitionSaisonService.save(competitionSaisonDTO);
        return ResponseEntity.created(new URI("/api/competition-saisons/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /competition-saisons} : Updates an existing competitionSaison.
     *
     * @param competitionSaisonDTO the competitionSaisonDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated competitionSaisonDTO,
     * or with status {@code 400 (Bad Request)} if the competitionSaisonDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the competitionSaisonDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/competition-saisons")
    public ResponseEntity<CompetitionSaisonDTO> updateCompetitionSaison(@RequestBody CompetitionSaisonDTO competitionSaisonDTO) throws URISyntaxException {
        log.debug("REST request to update CompetitionSaison : {}", competitionSaisonDTO);
        if (competitionSaisonDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CompetitionSaisonDTO result = competitionSaisonService.save(competitionSaisonDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, competitionSaisonDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /competition-saisons} : get all the competitionSaisons.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of competitionSaisons in body.
     */
    @GetMapping("/competition-saisons")
    public ResponseEntity<List<CompetitionSaisonDTO>> getAllCompetitionSaisons(Pageable pageable) {
        log.debug("REST request to get a page of CompetitionSaisons");
        Page<CompetitionSaisonDTO> page = competitionSaisonService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /competition-saisons/:id} : get the "id" competitionSaison.
     *
     * @param id the id of the competitionSaisonDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the competitionSaisonDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/competition-saisons/{id}")
    public ResponseEntity<CompetitionSaisonDTO> getCompetitionSaison(@PathVariable Long id) {
        log.debug("REST request to get CompetitionSaison : {}", id);
        Optional<CompetitionSaisonDTO> competitionSaisonDTO = competitionSaisonService.findOne(id);
        return ResponseUtil.wrapOrNotFound(competitionSaisonDTO);
    }

    /**
     * {@code DELETE  /competition-saisons/:id} : delete the "id" competitionSaison.
     *
     * @param id the id of the competitionSaisonDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/competition-saisons/{id}")
    public ResponseEntity<Void> deleteCompetitionSaison(@PathVariable Long id) {
        log.debug("REST request to delete CompetitionSaison : {}", id);
        competitionSaisonService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/competition-saisons?query=:query} : search for the competitionSaison corresponding
     * to the query.
     *
     * @param query the query of the competitionSaison search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/competition-saisons")
    public ResponseEntity<List<CompetitionSaisonDTO>> searchCompetitionSaisons(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of CompetitionSaisons for query {}", query);
        Page<CompetitionSaisonDTO> page = competitionSaisonService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
        }
}
