package org.nddngroup.navetfoot.core.web.rest.custom;

import org.nddngroup.navetfoot.core.domain.FeuilleDeMatch;
import org.nddngroup.navetfoot.core.domain.custom.AssociationFeuilleDeMatch;
import org.nddngroup.navetfoot.core.domain.custom.FeuilleDeMatchInfo;
import org.nddngroup.navetfoot.core.domain.custom.stats.JoueurDetail;
import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;
import org.nddngroup.navetfoot.core.domain.enumeration.EtatMatch;
import org.nddngroup.navetfoot.core.domain.enumeration.Position;
import org.nddngroup.navetfoot.core.exception.ApiRequestException;
import org.nddngroup.navetfoot.core.exception.ExceptionCode;
import org.nddngroup.navetfoot.core.exception.ExceptionLevel;
import org.nddngroup.navetfoot.core.service.*;
import org.nddngroup.navetfoot.core.service.custom.CommunService;
import org.nddngroup.navetfoot.core.service.dto.AssociationDTO;
import org.nddngroup.navetfoot.core.service.dto.DetailFeuilleDeMatchDTO;
import org.nddngroup.navetfoot.core.service.dto.FeuilleDeMatchDTO;
import tech.jhipster.web.util.HeaderUtil;
import org.nddngroup.navetfoot.core.service.*;
import org.nddngroup.navetfoot.core.service.dto.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.ZonedDateTime;
import java.util.List;

/**
 * REST controller for managing {@link FeuilleDeMatch}.
 */
@RestController
@RequestMapping("/api")
public class CustomFeuilleDeMatchResource {

    private static final String ENTITY_NAME = "navetfootCoreFeuilleDeMatch";

    private final Logger log = LoggerFactory.getLogger(CustomFeuilleDeMatchResource.class);

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FeuilleDeMatchService feuilleDeMatchService;

    @Autowired
    private CommunService communService;
    @Autowired
    private DetailFeuilleDeMatchService detailFeuilleDeMatchService;
    @Autowired
    private JoueurService joueurService;
    @Autowired
    private AssociationService associationService;
    @Autowired
    private MatchService matchService;


    public CustomFeuilleDeMatchResource(FeuilleDeMatchService feuilleDeMatchService) {
        this.feuilleDeMatchService = feuilleDeMatchService;
    }


    /**
     * GET  /organisations/:organisationId/matchs/:matchId/feuille-de-match : add feuilleDeMatch for match.
     *
     * @param organisationId L'id de l'organisation
     * @param matchId L'id du match
     * @return the CustomResponse with status 200 (OK) and with body the feuilleDeMatchDTO, or with status 404 (Not Found)
     */
    @PostMapping("/organisations/{organisationId}/matchs/{matchId}/feuille-de-match")
    public ResponseEntity<FeuilleDeMatchDTO> addFeuilleDeMatch(@PathVariable Long organisationId,
                                                               @PathVariable Long matchId) throws URISyntaxException {
        log.debug("REST request to get FeuilleDeMatch by match id : {}", matchId);

        // find organisation
        var organisationDTO = this.communService.checkOrganisation(organisationId);

        FeuilleDeMatchDTO result = this.feuilleDeMatchService.save(matchId, organisationDTO);

        return ResponseEntity.created(new URI("/api/organisations/" + organisationId + "/matchs/" + matchId + "/feuille-de-match/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }


    /**
     * PUT  /organisations/:organisationId/feuille-de-matches/:matchId/details : Updates feuilleDeMatch details.
     *
     * @param joueurDetails joueurs to update
     * @param organisationId L'id de l'organisation
     * @param matchId L'id du match
     * @param equipe L'équipe
     * @return the CustomResponse with status 200 (OK) and with body the updated feuilleDeMatchDTO,
     * or with status 400 (Bad Request) if the feuilleDeMatchDTO is not valid,
     * or with status 500 (Internal Server Error) if the feuilleDeMatchDTO couldn't be updated
     */
    @PutMapping("/organisations/{organisationId}/feuille-de-matches/{matchId}/details")
    public ResponseEntity<List<JoueurDetail>> updateFeuilleDeMatch(@RequestBody List<JoueurDetail> joueurDetails,
                                                                   @PathVariable Long organisationId,
                                                                   @PathVariable Long matchId,
                                                                   @RequestParam Equipe equipe) {
        log.debug("REST request to update FeuilleDeMatch details with id = : {}", matchId);

        // find organisation
        var organisationDTO = this.communService.checkOrganisation(organisationId);

        // find feuilleDeMatch by id
        var feuilleDeMatchDTO = this.feuilleDeMatchService.findOne(matchId)
            .orElseThrow(() -> new ApiRequestException(
                ExceptionCode.FEULLE_DE_MATCH_NOT_FOUND.getMessage(),
                ExceptionCode.FEULLE_DE_MATCH_NOT_FOUND.getValue(),
                ExceptionLevel.ERROR
            ));

        // find match
        var matchDTO = this.matchService.findOne(feuilleDeMatchDTO.getMatchId())
            .orElseThrow(() -> new ApiRequestException(
                ExceptionCode.MATCH_NOT_FOUND.getMessage(),
                ExceptionCode.MATCH_NOT_FOUND.getValue(),
                ExceptionLevel.ERROR
            ));

        if (matchDTO.getEtatMatch() == EtatMatch.ANNULE ||
            matchDTO.getEtatMatch() == EtatMatch.TERMINE ||
            matchDTO.getEtatMatch() == EtatMatch.FORFAIT ||
            matchDTO.getEtatMatch() == EtatMatch.RESERVE
        ) {
            throw new ApiRequestException(
                ExceptionCode.CAN_NOT_EDIT_FEUILLE_DE_MATCH.getMessage(),
                ExceptionCode.CAN_NOT_EDIT_FEUILLE_DE_MATCH.getValue(),
                ExceptionLevel.WARNING
            );
        }

        if (joueurDetails != null && !joueurDetails.isEmpty()) {
            for (JoueurDetail joueurDetail : joueurDetails) {
                if (joueurDetail != null) {
                    // find joueurDto
                    var joueurDTO = this.joueurService.findOne(joueurDetail.getId())
                        .orElseThrow(() -> new ApiRequestException(
                            ExceptionCode.JOUEUR_NOT_FOUND.getMessage(),
                            ExceptionCode.JOUEUR_NOT_FOUND.getValue(),
                            ExceptionLevel.ERROR
                        ));

                    // find joueur in feuille de match
                    var detailFeuilleDeMatchDTO = this.communService.getDetailInFeuilleDeMatch(joueurDTO, feuilleDeMatchDTO, equipe);

                    if (joueurDetail.getPosition() == null && detailFeuilleDeMatchDTO != null) {
                        detailFeuilleDeMatchDTO.setDeleted(true);
                        detailFeuilleDeMatchDTO.setUpdatedAt(ZonedDateTime.now());

                        this.detailFeuilleDeMatchService.save(detailFeuilleDeMatchDTO);

                    } else if (joueurDetail.getPosition() != null) {
                        if (detailFeuilleDeMatchDTO == null) {
                            // create a new DetailFeuilleDeMatch
                            detailFeuilleDeMatchDTO = new DetailFeuilleDeMatchDTO();
                            detailFeuilleDeMatchDTO.setCode("REF_" + organisationDTO.getId() + "_" + joueurDetail.getId());
                            detailFeuilleDeMatchDTO.setHashcode(this.communService.toHash(organisationDTO.getId() + joueurDetail.getId() + feuilleDeMatchDTO.getId().toString() + ZonedDateTime.now()));
                            detailFeuilleDeMatchDTO.setCreatedAt(ZonedDateTime.now());
                            detailFeuilleDeMatchDTO.setUpdatedAt(ZonedDateTime.now());
                            detailFeuilleDeMatchDTO.setDeleted(false);
                            detailFeuilleDeMatchDTO.setJoueurId(joueurDetail.getId());
                            detailFeuilleDeMatchDTO.setEquipe(equipe);
                            detailFeuilleDeMatchDTO.setPosition(joueurDetail.getPosition());
                            detailFeuilleDeMatchDTO.setNumeroJoueur(joueurDetail.getNumero());
                            detailFeuilleDeMatchDTO.setFeuilleDeMatchId(feuilleDeMatchDTO.getId());

                            DetailFeuilleDeMatchDTO result = this.detailFeuilleDeMatchService.save(detailFeuilleDeMatchDTO);

                            // update detailFeuilleDeMatch code
                            if (result != null) {
                                result.setCode("REF_" + organisationDTO.getId() + "_" + result.getId());
                                result.setHashcode(this.communService.toHash(result.getId().toString()));

                                this.detailFeuilleDeMatchService.save(result);
                            }
                        } else {
                            // update detailFeuilleDeMatch
                            detailFeuilleDeMatchDTO.setPosition(joueurDetail.getPosition());
                            detailFeuilleDeMatchDTO.setNumeroJoueur(joueurDetail.getNumero());
                            this.detailFeuilleDeMatchService.save(detailFeuilleDeMatchDTO);
                        }
                    }
                }
            }
        }

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, feuilleDeMatchDTO.getId().toString()))
            .body(joueurDetails);
    }

    /**
     * GET  /organisations/:organisationId/matchs/:matchId/feuille-de-match : get feuilleDeMatch by match id.
     *
     * @param organisationId L'id de l'organisation
     * @param matchId L'id du match
     * @return the CustomResponse with status 200 (OK) and with body the feuilleDeMatchDTO, or with status 404 (Not Found)
     */
    @GetMapping("/organisations/{organisationId}/matchs/{matchId}/feuille-de-match")
    public ResponseEntity<FeuilleDeMatchDTO> getFeuilleDeMatchForOrganisation(@PathVariable Long organisationId,
                                                                              @PathVariable Long matchId) {
        log.debug("REST request to get FeuilleDeMatch by match id : {}", matchId);

        // find organisation
        var organisationDTO = this.communService.checkOrganisation(organisationId);

        var matchDTO = this.communService.checkMatch(matchId, organisationDTO);

        // find feuilleDeMatch by matchId

        var feuilleDeMatchOptional = feuilleDeMatchService.findByMatch(matchDTO);
        if (feuilleDeMatchOptional.isPresent()) {
            var feuilleDeMatchDTO = feuilleDeMatchOptional.get();
            return ResponseEntity.ok(feuilleDeMatchDTO);
        } else {
            return ResponseEntity.ok(null);
        }
    }


    /**
     * GET  /organisations/:organisationId/feuille-de-matches/:id : get the "id" feuilleDeMatch.
     *
     * @param id the id of the feuilleDeMatchDTO to retrieve
     * @return the CustomResponse with status 200 (OK) and with body the feuilleDeMatchDTO, or with status 404 (Not Found)
     */
    @GetMapping("/organisations/{organisationId}/feuille-de-matches/{id}")
    public ResponseEntity<FeuilleDeMatchInfo> getFeuilleDeMatchInfos(@PathVariable Long organisationId,
                                                                     @PathVariable Long id) {
        log.debug("REST request to get FeuilleDeMatch : {}", id);

        // find organisation
        var organisationDTO = this.communService.checkOrganisation(organisationId);

        // find feuille de match by id
        var feuilleDeMatchDTO = feuilleDeMatchService.findOne(id)
            .orElseThrow(
                () -> new ApiRequestException(
                    ExceptionCode.FEUILLE_DE_MATCH_ALREADY_EXISTS.getMessage(),
                    ExceptionCode.FEUILLE_DE_MATCH_ALREADY_EXISTS.getValue(),
                    ExceptionLevel.ERROR
                )
            );

        // find match
        this.communService.checkMatch(feuilleDeMatchDTO.getMatchId(), organisationDTO);

        var feuilleDeMatchInfo = new FeuilleDeMatchInfo();

        // find details feuilleDeMatch
        List<DetailFeuilleDeMatchDTO> detailFeuilleDeMatchDTOS = this.detailFeuilleDeMatchService.findByFeuilleDeMatch(feuilleDeMatchDTO);

        if (detailFeuilleDeMatchDTOS != null && !detailFeuilleDeMatchDTOS.isEmpty()) {
            var local = new AssociationFeuilleDeMatch();
            var visiteur = new AssociationFeuilleDeMatch();

            for (DetailFeuilleDeMatchDTO detailFeuilleDeMatchDTO : detailFeuilleDeMatchDTOS) {
                var joueurDetail = new JoueurDetail();

                if (detailFeuilleDeMatchDTO != null) {
                    // find joueur by id
                    var joueurDTO = this.joueurService.findOne(detailFeuilleDeMatchDTO.getJoueurId())
                        .orElseThrow(
                            () -> new ApiRequestException(
                                ExceptionCode.JOUEUR_NOT_FOUND_IN_FEUILLE_DE_MATCH.getMessage(),
                                ExceptionCode.JOUEUR_NOT_FOUND_IN_FEUILLE_DE_MATCH.getValue(),
                                ExceptionLevel.ERROR
                            )
                        );

                    joueurDetail.setNumero(detailFeuilleDeMatchDTO.getNumeroJoueur());

                    joueurDetail.setId(joueurDTO.getId());
                    joueurDetail.setPrenom(joueurDTO.getPrenom());
                    joueurDetail.setNom(joueurDTO.getNom());
                    joueurDetail.setImage(joueurDTO.getImageUrl());
                    joueurDetail.setIdentifiant(joueurDTO.getIdentifiant());
                    joueurDetail.setDateDeNaissance(joueurDTO.getDateDeNaissance());
                    joueurDetail.setLieuDeNaissance(joueurDTO.getLieuDeNaissance());

                    if (detailFeuilleDeMatchDTO.getEquipe().equals(Equipe.LOCAL)) {
                        if (detailFeuilleDeMatchDTO.getPosition().equals(Position.TITULAIRE)) {
                            local.getPartants().add(joueurDetail);
                        } else if (detailFeuilleDeMatchDTO.getPosition().equals(Position.SUPPLEANT)) {
                            local.getSuppleants().add(joueurDetail);
                        }
                    } else if (detailFeuilleDeMatchDTO.getEquipe().equals(Equipe.VISITEUR)) {
                        if (detailFeuilleDeMatchDTO.getPosition().equals(Position.TITULAIRE)) {
                            visiteur.getPartants().add(joueurDetail);
                        } else if (detailFeuilleDeMatchDTO.getPosition().equals(Position.SUPPLEANT)) {
                            visiteur.getSuppleants().add(joueurDetail);
                        }
                    }
                }
            }

            feuilleDeMatchInfo.setLocal(local);
            feuilleDeMatchInfo.setVisiteur(visiteur);
        }

        return ResponseEntity.ok(feuilleDeMatchInfo);
    }


    /**
     * GET  /organisations/:organisationId/feuille-de-matches/:id : get the "id" feuilleDeMatch.
     *
     * @param id the id of the feuilleDeMatchDTO to retrieve
     * @return the CustomResponse with status 200 (OK) and with body the feuilleDeMatchDTO, or with status 404 (Not Found)
     */
    @GetMapping("/organisations/{organisationId}/feuille-de-matches/{id}/joueurs")
    public ResponseEntity<List<JoueurDetail>> getFeuilleDeMatchInfos(@PathVariable Long organisationId,
                                                                     @PathVariable Long id,
                                                                     @RequestParam Equipe equipe,
                                                                     @RequestParam(defaultValue = "") Position position) {
        // find organisation
        var organisationDTO = this.communService.checkOrganisation(organisationId);

        // find feuilleDeMatch by id
        var feuilleDeMatchDTO = this.feuilleDeMatchService.findOne(id)
            .orElseThrow(
                () -> new ApiRequestException(
                    ExceptionCode.FEULLE_DE_MATCH_NOT_FOUND.getMessage(),
                    ExceptionCode.FEULLE_DE_MATCH_NOT_FOUND.getValue(),
                    ExceptionLevel.ERROR
                )
            );

        // check match
        var matchDTO = this.communService.checkMatch(feuilleDeMatchDTO.getMatchId(), organisationDTO);

        AssociationDTO associationDTO = null;

        if (equipe.equals(Equipe.LOCAL)) {
            associationDTO = this.associationService.findOne(matchDTO.getLocalId())
                .orElseThrow(
                    () -> new ApiRequestException(
                        ExceptionCode.ASSOCIATIONS_NOT_FOUND_IN_FEUILLE_DE_MATCH.getMessage(),
                        ExceptionCode.ASSOCIATIONS_NOT_FOUND_IN_FEUILLE_DE_MATCH.getValue(),
                        ExceptionLevel.ERROR
                    )
                );
        } else if (equipe.equals(Equipe.VISITEUR)) {
            associationDTO = this.associationService.findOne(matchDTO.getVisiteurId())
                .orElseThrow(
                    () -> new ApiRequestException(
                        ExceptionCode.ASSOCIATIONS_NOT_FOUND_IN_FEUILLE_DE_MATCH.getMessage(),
                        ExceptionCode.ASSOCIATIONS_NOT_FOUND_IN_FEUILLE_DE_MATCH.getValue(),
                        ExceptionLevel.ERROR
                    )
                );
        }

        List<JoueurDetail> joueurDetails = this.communService.getJoueurForClubInFeuilleDeMatch(associationDTO, feuilleDeMatchDTO, matchDTO, organisationDTO, position, equipe);

        return ResponseEntity.ok(joueurDetails);
    }
}
