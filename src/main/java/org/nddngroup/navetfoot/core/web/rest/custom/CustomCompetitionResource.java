package org.nddngroup.navetfoot.core.web.rest.custom;

import org.nddngroup.navetfoot.core.domain.custom.CompetitionWithCategories;
import org.nddngroup.navetfoot.core.exception.ApiRequestException;
import org.nddngroup.navetfoot.core.exception.ExceptionCode;
import org.nddngroup.navetfoot.core.exception.ExceptionLevel;
import org.nddngroup.navetfoot.core.service.CompetitionSaisonService;
import org.nddngroup.navetfoot.core.service.CompetitionService;
import org.nddngroup.navetfoot.core.service.SaisonService;
import org.nddngroup.navetfoot.core.service.custom.CommunService;
import org.nddngroup.navetfoot.core.service.custom.DeleteService;
import org.nddngroup.navetfoot.core.service.dto.*;
import org.nddngroup.navetfoot.core.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import org.nddngroup.navetfoot.core.service.dto.*;
import org.nddngroup.navetfoot.core.web.rest.RegionResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class CustomCompetitionResource {
    private final Logger log = LoggerFactory.getLogger(RegionResource.class);

    private static final String ENTITY_NAME = "navetfootCoreSaison";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CompetitionService competitionService;

    @Autowired
    private DeleteService deleteService;
    @Autowired
    private CommunService communService;
    @Autowired
    private SaisonService saisonService;
    @Autowired
    private CompetitionSaisonService competitionSaisonService;

    public CustomCompetitionResource(CompetitionService competitionService) {
        this.competitionService = competitionService;
    }


    /**
     * POST  /organisations/:id/competitions : Create a new competition.
     *
     * @param competitionDTO the competitionDTO to create
     * @return the CustomResponse with status 201 (Created) and with body the new competitionDTO, or with status 400 (Bad Request) if the competition has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/organisations/{id}/competitions")
    public ResponseEntity<CompetitionDTO> createCompetitionForOrganisation(@PathVariable Long id,
                                                                           @Valid @RequestBody CompetitionDTO competitionDTO) throws Exception {
        log.debug("REST request to save Competition : {}", competitionDTO);

        if (competitionDTO.getId() != null) {
            throw new BadRequestAlertException("A new competition cannot already have an ID", ENTITY_NAME, "idexists");
        }

        // get organisation
        OrganisationDTO organisationDTO = this.communService.checkOrganisation(id);

        CompetitionDTO result = competitionService.save(organisationDTO, competitionDTO);

        return ResponseEntity.created(new URI("/api/competitions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }


    /**
     * POST  /organisations/:organisationId/saisons/:saisonId/competitions : Create a new competitionSaison.
     *
     * @param organisationId L'id de l'organisation
     * @param saisonId L'id de la saison
     * @param competitionDTO the competitionDTO to add
     * @return the CustomResponse with status 201 (Created) and with body the new competitionSaisonDTO, or with status 400 (Bad Request) if the competitionSaison has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/organisations/{organisationId}/saisons/{saisonId}/competitions")
    public ResponseEntity<CompetitionSaisonDTO> createCompetitionSaisonByOrganisation(@PathVariable Long organisationId,
                                                                                      @PathVariable Long saisonId,
                                                                                      @RequestBody CompetitionDTO competitionDTO) throws Exception {
        log.debug("REST request to save Competition for a 'saison' : {}", competitionDTO);

        // get organisation
        OrganisationDTO organisationDTO = this.communService.checkOrganisation(organisationId);

        CompetitionSaisonDTO competitionSaisonDTO = new CompetitionSaisonDTO();
        if (organisationDTO != null) {
            competitionSaisonDTO.setCode("REF_" + organisationDTO.getId().toString());
        }

        competitionSaisonDTO.setCompetitionId(competitionDTO.getId());
        competitionSaisonDTO.setSaisonId(saisonId);
        competitionSaisonDTO.setCreatedAt(ZonedDateTime.now());
        competitionSaisonDTO.setUpdatedAt(ZonedDateTime.now());
        competitionSaisonDTO.setDeleted(false);

        CompetitionSaisonDTO result = competitionSaisonService.save(competitionSaisonDTO);

        return ResponseEntity.created(new URI("/api/organisations/" + organisationId + "/saisons/" + saisonId + "/competitions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }


    /**
     * POST /organisations/:organisationId/import/competitions : import list of competitions
     *
     * @param competitionDTOS Liste des competitions
     * @param organisationId L'id de l'organisation
     * @return List<CompetitionDTO>
     */
    @PostMapping("/organisations/{organisationId}/import/competitions")
    public ResponseEntity<List<CompetitionDTO>> importCompetitions(@RequestBody List<CompetitionDTO> competitionDTOS,
                                                              @PathVariable Long organisationId
    ) throws URISyntaxException {
        OrganisationDTO organisationDTO = communService.checkOrganisation(organisationId);

        List<CompetitionDTO> result = competitionService.save(organisationDTO, competitionDTOS);

        return ResponseEntity.created(new URI("/api/organisations/" + organisationId + "/import/competitions/"))
            .body(result);
    }

    /**
     * {@code PUT  /organisations/:id/competitions} : Updates an existing competition.
     *
     * @param competitionDTO the competitionDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated competitionDTO,
     * or with status {@code 400 (Bad Request)} if the competitionDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the competitionDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/organisations/{id}/competitions")
    public ResponseEntity<CompetitionDTO> updateCompetition(@PathVariable Long id,
                                                            @Valid @RequestBody CompetitionDTO competitionDTO) throws URISyntaxException {
        log.debug("REST request to update Competition : {}", competitionDTO);
        if (competitionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }

        communService.checkOrganisation(id);

        competitionDTO.setUpdatedAt(ZonedDateTime.now());

        CompetitionDTO result = competitionService.save(competitionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, competitionDTO.getId().toString()))
            .body(result);
    }


    /**
     * {@code GET  /organisations/:id/competitions} : get all the competitions.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of competitions in body.
     */
    @GetMapping("/organisations/{id}/competitions")
    public ResponseEntity<List<CompetitionDTO>> getAllCompetitions(@PathVariable Long id, Pageable pageable) {
        log.debug("REST request to get a page of Competitions");

        // get organisation
        OrganisationDTO organisationDTO = this.communService.checkOrganisation(id);

        Page<CompetitionDTO> page = competitionService.findAllByOrganisation(organisationDTO, pageable);

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }


    /**
     * GET  /organisations/:organisationId/saisons/:saisonId/competitions/with-categories : get all the added competitions for a 'saison'.
     *
     * @param organisationId L'id de l'organisation
     * @param saisonId the 'saison' id
     * @return the CustomResponse with status 200 (OK) and the list of competitions in body
     */
    @GetMapping("/organisations/{organisationId}/saisons/{saisonId}/competitions/with-categories")
    public ResponseEntity<List<CompetitionWithCategories>> getAddedCompetitionsBySaison(@PathVariable Long organisationId,
                                                                                        @PathVariable Long saisonId) throws Exception {
        log.debug("REST request to get Competitions by saisonId: {}", saisonId);

        List<CompetitionWithCategories> competitionWithCategories = new ArrayList<>();

        // get organisation
        OrganisationDTO organisationDTO = this.communService.checkOrganisation(organisationId);

        if (organisationDTO == null) {
            throw new ApiRequestException(
                ExceptionCode.ORGANISATION_NOT_FOUND.getMessage(),
                ExceptionCode.ORGANISATION_NOT_FOUND.getValue(),
                ExceptionLevel.ERROR
            );
        }

        if (saisonId != null) {
            // find saison
            SaisonDTO saisonDTO = communService.checkSaisonForOrganisation(saisonId, organisationDTO);

            List<CompetitionDTO> competitionDTOS = this.competitionService.findAllByOrganisation(organisationDTO);
            if (competitionDTOS != null && competitionDTOS.size() != 0) {
                for (CompetitionDTO competitionDTO : competitionDTOS) {
                    if (competitionDTO != null) {
                        try {
                            List<CategoryDTO> categoryDTOS = communService.getCompetitionSaisonCategories(competitionDTO, saisonDTO);
                            CompetitionWithCategories val = new CompetitionWithCategories();
                            val.setCompetition(competitionDTO);
                            val.setCategories(categoryDTOS);

                            competitionWithCategories.add(val);
                        } catch (Exception ignored) {}
                    }
                }
            }
        }

        return new ResponseEntity<>(competitionWithCategories, HttpStatus.OK);
    }


    /**
     * GET  /organisations/:organisationId/saisons/:saisonId/competitions : get all the competitions.
     *
     * @param saisonId L'id de la saison
     * @param organisationId L'id de l'organisation
     * @param pageable the pagination information
     * @return the CustomResponse with status 200 (OK) and the list of competitions in body
     */
    @GetMapping("/organisations/{organisationId}/saisons/{saisonId}/competitions")
    public ResponseEntity<List<CompetitionDTO>> getAllCompetitionsBySaison(@PathVariable Long saisonId,
                                                                           @PathVariable Long organisationId,
                                                                           @ApiParam Pageable pageable) throws Exception {
        log.debug("REST request to get Competitions by saison: {}", saisonId);

        // get organisation
        OrganisationDTO organisationDTO = this.communService.checkOrganisation(organisationId);

        // find saison
        SaisonDTO saisonDTO = communService.checkSaisonForOrganisation(saisonId, organisationDTO);

        List<Long> competitionIds = communService.getCompetitionIds(organisationDTO, saisonDTO);

        Page<CompetitionDTO> page = competitionService.findByIdIn(competitionIds, pageable);

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /organisations/:organisationId/saisons/:saisonId/competitions/all : get all the competitions.
     *
     * @param saisonId L'id de la saison
     * @param organisationId L'id de l'organisation
     * @return the CustomResponse with status 200 (OK) and the list of competitions in body
     */
    @GetMapping("/organisations/{organisationId}/saisons/{saisonId}/competitions/all")
    public ResponseEntity<List<CompetitionDTO>> getAllCompetitionsBySaison(@PathVariable Long saisonId,
                                                                           @PathVariable Long organisationId) throws Exception {
        log.debug("REST request to get Competitions by saison: {}", saisonId);

        // get organisation
        OrganisationDTO organisationDTO = this.communService.checkOrganisation(organisationId);

        // find saison
        SaisonDTO saisonDTO = communService.checkSaisonForOrganisation(saisonId, organisationDTO);

        List<Long> competitionIds = communService.getCompetitionIds(organisationDTO, saisonDTO);

        List<CompetitionDTO> competitionDTOS = competitionService.findByIdIn(competitionIds);

        return ResponseEntity.ok()
            .body(competitionDTOS);
    }

    /**
     * GET  /organisations/:organisationId/saisons/:saisonId/competitions/not-added : get all the none added competitions for a 'saison'.
     *
     * @param saisonId L'id de la saison
     * @param organisationId L'id de l'organisation
     * @return the CustomResponse with status 200 (OK) and the list of competitions in body
     */
    @GetMapping("/organisations/{organisationId}/saisons/{saisonId}/competitions/not-added")
    public ResponseEntity<List<CompetitionDTO>> getAllNotAddedCompetitionsBySaison(
                                                                @PathVariable Long saisonId,
                                                                @PathVariable Long organisationId) throws URISyntaxException {
        log.debug("REST request to get none added Competitions by saison: {}", saisonId);

        // get organisation
        OrganisationDTO organisationDTO = this.communService.checkOrganisation(organisationId);

        // find saison
        SaisonDTO saisonDTO = communService.checkSaisonForOrganisation(saisonId, organisationDTO);

        List<Long> competitionIds = new ArrayList<>();

        if (saisonDTO != null) {
            List<CompetitionDTO> competitionDTOS = this.competitionService.findAllByOrganisation(organisationDTO);
            if (competitionDTOS != null && competitionDTOS.size() != 0) {
                for (CompetitionDTO competitionDTO : competitionDTOS) {
                    if (competitionDTO != null) {
                        // check if competition is in saison
                        List<CompetitionSaisonDTO> competitionSaisonDTOs = this.competitionSaisonService
                            .findBySaisonAndCompetition(saisonDTO, competitionDTO);

                        if (competitionSaisonDTOs == null || competitionSaisonDTOs.size() == 0) {
                            competitionIds.add(competitionDTO.getId());
                        }
                    }
                }
            }
        }

        List<CompetitionDTO> competitions;

        competitions = competitionService.findByIdIn(competitionIds);
        return new ResponseEntity<>(competitions, HttpStatus.OK);
    }


    /**
     * DELETE /organisations/:organisationId/competitions : remove list of competitions
     *
     * @param headers HttpHeaders
     * @return List<CompetitionDTO>
     */
    @DeleteMapping("/organisations/{organisationId}/competitions")
    public ResponseEntity<List<CompetitionDTO>> removeAssociations(@RequestHeader HttpHeaders headers,
                                                              @PathVariable Long organisationId) throws URISyntaxException {
        OrganisationDTO organisationDTO = communService.checkOrganisation(organisationId);
        List<String> competitionIds = headers.getValuesAsList("ids");

        List<CompetitionDTO> result = deleteService.removeCompetitionList(organisationDTO, competitionIds);

        return ResponseEntity.created(new URI("/api/organisations/" + organisationId + "/competitions/"))
            .body(result);
    }

    /**
     * DELETE  /organisations/:id/competitions/:id : delete the "id" competition.
     *
     * @param organisationId the id of organisation
     * @param id the id of the competitionDTO to delete
     * @return the CustomResponse with status 200 (OK)
     */
    @DeleteMapping("/organisations/{organisationId}/competitions/{id}")
    public ResponseEntity<Boolean> deleteCompetition(@PathVariable Long organisationId,
                                                     @PathVariable Long id) {
        log.debug("REST request to delete Competition : {}", id);

        OrganisationDTO organisationDTO = communService.checkOrganisation(organisationId);

        CompetitionDTO competition = communService.checkCompetitionForOrganisation(id, organisationDTO);

        deleteService.deleteCompetition(competition);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .body(true);
    }

    /**
     * {@code SEARCH  /_search/organisations/:organisationId/competitions?query=:query} : search for the competition corresponding
     * to the query.
     *
     * @param query the query of the competition search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/organisations/{organisationId}/competitions")
    public ResponseEntity<List<CompetitionDTO>> searchCompetitions(@RequestParam String query,
                                                                   @PathVariable Long organisationId,
                                                                   Pageable pageable) {
        log.debug("REST request to search for a page of Competitions for query {}", query);

        //find organisation
        OrganisationDTO organisationDTO = communService.checkOrganisation(organisationId);

        Page<CompetitionDTO> page = competitionService.search(organisationDTO, query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
