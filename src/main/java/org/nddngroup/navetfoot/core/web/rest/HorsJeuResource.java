package org.nddngroup.navetfoot.core.web.rest;

import org.nddngroup.navetfoot.core.domain.HorsJeu;
import org.nddngroup.navetfoot.core.service.HorsJeuService;
import org.nddngroup.navetfoot.core.service.dto.HorsJeuDTO;
import org.nddngroup.navetfoot.core.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link HorsJeu}.
 */
@RestController
@RequestMapping("/api")
public class HorsJeuResource {

    private final Logger log = LoggerFactory.getLogger(HorsJeuResource.class);

    private static final String ENTITY_NAME = "navetfootCoreHorsJeu";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final HorsJeuService horsJeuService;

    public HorsJeuResource(HorsJeuService horsJeuService) {
        this.horsJeuService = horsJeuService;
    }

    /**
     * {@code POST  /hors-jeus} : Create a new horsJeu.
     *
     * @param horsJeuDTO the horsJeuDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new horsJeuDTO, or with status {@code 400 (Bad Request)} if the horsJeu has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/hors-jeus")
    public ResponseEntity<HorsJeuDTO> createHorsJeu(@Valid @RequestBody HorsJeuDTO horsJeuDTO) throws URISyntaxException {
        log.debug("REST request to save HorsJeu : {}", horsJeuDTO);
        if (horsJeuDTO.getId() != null) {
            throw new BadRequestAlertException("A new horsJeu cannot already have an ID", ENTITY_NAME, "idexists");
        }
        HorsJeuDTO result = horsJeuService.save(horsJeuDTO);
        return ResponseEntity.created(new URI("/api/hors-jeus/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /hors-jeus} : Updates an existing horsJeu.
     *
     * @param horsJeuDTO the horsJeuDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated horsJeuDTO,
     * or with status {@code 400 (Bad Request)} if the horsJeuDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the horsJeuDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/hors-jeus")
    public ResponseEntity<HorsJeuDTO> updateHorsJeu(@Valid @RequestBody HorsJeuDTO horsJeuDTO) throws URISyntaxException {
        log.debug("REST request to update HorsJeu : {}", horsJeuDTO);
        if (horsJeuDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        HorsJeuDTO result = horsJeuService.save(horsJeuDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, horsJeuDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /hors-jeus} : get all the horsJeus.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of horsJeus in body.
     */
    @GetMapping("/hors-jeus")
    public List<HorsJeuDTO> getAllHorsJeus() {
        log.debug("REST request to get all HorsJeus");
        return horsJeuService.findAll();
    }

    /**
     * {@code GET  /hors-jeus/:id} : get the "id" horsJeu.
     *
     * @param id the id of the horsJeuDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the horsJeuDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/hors-jeus/{id}")
    public ResponseEntity<HorsJeuDTO> getHorsJeu(@PathVariable Long id) {
        log.debug("REST request to get HorsJeu : {}", id);
        Optional<HorsJeuDTO> horsJeuDTO = horsJeuService.findOne(id);
        return ResponseUtil.wrapOrNotFound(horsJeuDTO);
    }

    /**
     * {@code DELETE  /hors-jeus/:id} : delete the "id" horsJeu.
     *
     * @param id the id of the horsJeuDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/hors-jeus/{id}")
    public ResponseEntity<Void> deleteHorsJeu(@PathVariable Long id) {
        log.debug("REST request to delete HorsJeu : {}", id);
        horsJeuService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/hors-jeus?query=:query} : search for the horsJeu corresponding
     * to the query.
     *
     * @param query the query of the horsJeu search.
     * @return the result of the search.
     */
    @GetMapping("/_search/hors-jeus")
    public List<HorsJeuDTO> searchHorsJeus(@RequestParam String query) {
        log.debug("REST request to search HorsJeus for query {}", query);
        return horsJeuService.search(query);
    }
}
