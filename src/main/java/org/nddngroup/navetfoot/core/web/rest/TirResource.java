package org.nddngroup.navetfoot.core.web.rest;

import org.nddngroup.navetfoot.core.domain.Tir;
import org.nddngroup.navetfoot.core.service.TirService;
import org.nddngroup.navetfoot.core.service.dto.TirDTO;
import org.nddngroup.navetfoot.core.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link Tir}.
 */
@RestController
@RequestMapping("/api")
public class TirResource {

    private final Logger log = LoggerFactory.getLogger(TirResource.class);

    private static final String ENTITY_NAME = "navetfootCoreTir";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TirService tirService;

    public TirResource(TirService tirService) {
        this.tirService = tirService;
    }

    /**
     * {@code POST  /tirs} : Create a new tir.
     *
     * @param tirDTO the tirDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new tirDTO, or with status {@code 400 (Bad Request)} if the tir has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/tirs")
    public ResponseEntity<TirDTO> createTir(@Valid @RequestBody TirDTO tirDTO) throws URISyntaxException {
        log.debug("REST request to save Tir : {}", tirDTO);
        if (tirDTO.getId() != null) {
            throw new BadRequestAlertException("A new tir cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TirDTO result = tirService.save(tirDTO);
        return ResponseEntity.created(new URI("/api/tirs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /tirs} : Updates an existing tir.
     *
     * @param tirDTO the tirDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tirDTO,
     * or with status {@code 400 (Bad Request)} if the tirDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the tirDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/tirs")
    public ResponseEntity<TirDTO> updateTir(@Valid @RequestBody TirDTO tirDTO) throws URISyntaxException {
        log.debug("REST request to update Tir : {}", tirDTO);
        if (tirDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TirDTO result = tirService.save(tirDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, tirDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /tirs} : get all the tirs.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of tirs in body.
     */
    @GetMapping("/tirs")
    public List<TirDTO> getAllTirs() {
        log.debug("REST request to get all Tirs");
        return tirService.findAll();
    }

    /**
     * {@code GET  /tirs/:id} : get the "id" tir.
     *
     * @param id the id of the tirDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the tirDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/tirs/{id}")
    public ResponseEntity<TirDTO> getTir(@PathVariable Long id) {
        log.debug("REST request to get Tir : {}", id);
        Optional<TirDTO> tirDTO = tirService.findOne(id);
        return ResponseUtil.wrapOrNotFound(tirDTO);
    }

    /**
     * {@code DELETE  /tirs/:id} : delete the "id" tir.
     *
     * @param id the id of the tirDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/tirs/{id}")
    public ResponseEntity<Void> deleteTir(@PathVariable Long id) {
        log.debug("REST request to delete Tir : {}", id);
        tirService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/tirs?query=:query} : search for the tir corresponding
     * to the query.
     *
     * @param query the query of the tir search.
     * @return the result of the search.
     */
    @GetMapping("/_search/tirs")
    public List<TirDTO> searchTirs(@RequestParam String query) {
        log.debug("REST request to search Tirs for query {}", query);
        return tirService.search(query);
    }
}
