package org.nddngroup.navetfoot.core.web.rest.custom;

import org.nddngroup.navetfoot.core.config.rabbitMQ.RabbitMQConfig;
import org.nddngroup.navetfoot.core.domain.contant.Constant;
import org.nddngroup.navetfoot.core.domain.custom.Abonnement;
import org.nddngroup.navetfoot.core.domain.custom.CompetitionWithSaisonsAndMatchs;
import org.nddngroup.navetfoot.core.domain.custom.DelegueMatchDetail;
import org.nddngroup.navetfoot.core.domain.custom.stats.*;
import org.nddngroup.navetfoot.core.domain.enumeration.EtatMatch;
import org.nddngroup.navetfoot.core.domain.enumeration.websocket.RabbitMQMessageType;
import org.nddngroup.navetfoot.core.domain.websocket.RabbitMQMessage;
import org.nddngroup.navetfoot.core.domain.websocket.WebSocketTopic;
import org.nddngroup.navetfoot.core.exception.ApiRequestException;
import org.nddngroup.navetfoot.core.exception.CustomError;
import org.nddngroup.navetfoot.core.exception.ExceptionCode;
import org.nddngroup.navetfoot.core.exception.ExceptionLevel;
import org.nddngroup.navetfoot.core.security.AuthoritiesConstants;
import org.nddngroup.navetfoot.core.service.*;
import org.nddngroup.navetfoot.core.service.custom.CommunService;
import org.nddngroup.navetfoot.core.service.custom.NotificationService;
import org.nddngroup.navetfoot.core.service.dto.*;
import org.nddngroup.navetfoot.core.service.external.ExternalApiService;
import org.nddngroup.navetfoot.core.service.stats.ClassementService;
import org.nddngroup.navetfoot.core.service.websocket.WebSocketApiClientService;
import org.nddngroup.navetfoot.externalapi.model.Statistic;
import org.nddngroup.navetfoot.externalapi.model.TeamStatistic;
import org.springframework.web.multipart.MultipartFile;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.*;

@RestController
@RequestMapping("/api")
public class MobileResource {
    private final Logger log = LoggerFactory.getLogger(CustomMatchResource.class);

    private static final String ENTITY_NAME = "navetfootCoreMobile";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    @Autowired
    private MatchService matchService;
    @Autowired
    private JoueurService joueurService;
    @Autowired
    private AssociationService associationService;
    @Autowired
    private CompetitionSaisonService competitionSaisonService;
    @Autowired
    private SaisonService saisonService;
    @Autowired
    private EtapeCompetitionService etapeCompetitionService;
    @Autowired
    private ParticipationService participationService;
    @Autowired
    private ClassementService classementService;
    @Autowired
    private CompetitionService competitionService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private PouleService pouleService;
    @Autowired
    private DetailPouleService detailPouleService;
    @Autowired
    private CommunService communService;
    @Autowired
    private DelegueService delegueService;
    @Autowired
    private DelegueMatchService delegueMatchService;
    @Autowired
    private ContratService contratService;
    @Autowired
    private OrganisationService organisationService;
    @Autowired
    private WebSocketApiClientService webSocketApiClientService;
    @Autowired
    private NotificationService notificationService;
    @Autowired
    private ExternalApiService externalApiService;

    @PostMapping("/mobile/abonnements")
    public ResponseEntity<Abonnement> newAbonnement(@RequestBody Abonnement abonnement) throws URISyntaxException {
        log.debug("REST request to add new Abonnement: {}", abonnement);

        Abonnement result = notificationService.addAbonnement(abonnement);

        return ResponseEntity.created(new URI("/api/mobile/abonnements/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /organisations} : get all the organisations.
     *
     * @param pageable the pagination information.
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of organisations in body.
     */
    @GetMapping("/mobile/organisations")
    public ResponseEntity<List<OrganisationDTO>> getAllOrganisations(Pageable pageable,
                                                                     @RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get a page of Organisations");
        Page<OrganisationDTO> page;
        if (eagerload) {
            page = organisationService.findAllWithEagerRelationships(pageable);
        } else {
            page = organisationService.findAll(pageable);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/mobile/match-details")
    @Secured({AuthoritiesConstants.MOBILE})
    public ResponseEntity<List<MatchDetail>> getAllMatchs(Pageable pageable,
                                                          @RequestParam(required = false) String keyword,
                                                          @RequestParam(required = false) String dateMatch) {
        List<Long> clubIds = new ArrayList<>();

        LocalDate dateStart = null;

        if (dateMatch != null && dateMatch.equals("now")) {
            dateStart = LocalDate.now().minusDays(1);
        }

        if (keyword != null && !keyword.equals("")) {
            log.info("_______ KEYWORD = {}", keyword);

            // find clubs by keyword
            List<AssociationDTO> associationDTOS = associationService.findByNom(keyword);

            if (associationDTOS != null && associationDTOS.size() != 0) {
                for (AssociationDTO associationDTO : associationDTOS) {
                    clubIds.add(associationDTO.getId());
                }
            }

            log.info("_________ CLUB IDS = {}", clubIds.toString());

            Page<MatchDetail> page = this.matchService.findAllByAssociationIdIn(clubIds, dateStart, pageable);

            if (page.getContent().size() == 0) {
                page = matchService.findAllByAssociationIdIn(clubIds, pageable);
            }

            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
            return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
        } else {
            // find all match
            Page<MatchDetail> page = this.matchService.findAllMatchDetails(pageable);


            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
            return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
        }
    }


    @GetMapping("mobile/etape-competitions/{etapeId}/match-details")
    @Secured({AuthoritiesConstants.MOBILE})
    public ResponseEntity<List<MatchDetail>> getCustomMatchs(@PathVariable Long etapeId,
                                                             @RequestParam(required = false) Integer journee) {

        EtapeCompetitionDTO etapeCompetitionDTO = etapeCompetitionService.findOne(etapeId)
            .orElseThrow(
                () -> new ApiRequestException(
                    ExceptionCode.ETAPE_NOT_FOUND.getMessage(),
                    ExceptionCode.ETAPE_NOT_FOUND.getValue(),
                    ExceptionLevel.ERROR
                )
            );

        List<MatchDetail> matchDetails = communService.getMatchDetailList(etapeCompetitionDTO, journee);

        return new ResponseEntity<>(matchDetails, HttpStatus.OK);
    }

    @GetMapping("/mobile/matchs/{matchId}/events")
    @Secured({AuthoritiesConstants.MOBILE})
    public ResponseEntity<EventsDetail> getEventsDetails(@PathVariable Long matchId, @RequestParam(name = "external", defaultValue = "false") boolean external) {
        if (Boolean.TRUE.equals(external)) {
            return new ResponseEntity<>(externalApiService.getEventsDetails(matchId), HttpStatus.OK);
        }

        return new ResponseEntity<>(communService.getEventsDetails(matchId), HttpStatus.OK);
    }

    @GetMapping("/mobile/matchs/{matchId}/feuille-de-match/details")
    @Secured({AuthoritiesConstants.MOBILE})
    public ResponseEntity<FeuilleDeMatchDetail> getCustomFeuilleDeMatch(@PathVariable Long matchId,
                                                                        @RequestParam(name = "external", defaultValue = "false") boolean external) {
        if (Boolean.TRUE.equals(external)) {
            return new ResponseEntity<>(externalApiService.getExternalClassement(matchId), HttpStatus.OK);
        }

        return new ResponseEntity<>(communService.getFeuilleDeMatchDetails(matchId), HttpStatus.OK);
    }

    /**
     * GET  /mobile/saisons/:id/competition-saisons : get all the competitionSaisons by saison.
     *
     * @param id L'id de la saison
     * @return the CustomResponse with status 200 (OK) and the list of competitionSaisons in body
     */
    @GetMapping("/mobile/saisons/{id}/competition-saisons")
    @Secured({AuthoritiesConstants.MOBILE})
    public ResponseEntity<List<CompetitionSaisonDTO>> getAllCompetitionSaisonsBySaison(@PathVariable Long id) {
        log.debug("REST request to get CompetitionSaisons by saison: {}", id );
        SaisonDTO saisonDTO = saisonService.findOne(id)
            .orElseThrow(
                () -> new ApiRequestException(
                    ExceptionCode.MATCH_NOT_FOUND.getMessage(),
                    ExceptionCode.MATCH_NOT_FOUND.getValue(),
                    ExceptionLevel.ERROR
                )
            );

        List<CompetitionSaisonDTO> competitionSaisonDTOS = competitionSaisonService.findBySaison(saisonDTO);
        return ResponseEntity.ok().body(competitionSaisonDTOS);
    }


    @GetMapping("/mobile/classement/{critere}/{type}")
    @Secured({AuthoritiesConstants.MOBILE})
    public ResponseEntity<List<Stat>> getClassement(
        @PathVariable String critere,
        @PathVariable String type,
        @RequestParam Long saisonId,
        @RequestParam Long competitionId,
        @RequestParam Long categoryId,
        @RequestParam int nbRows) {

        List<Stat> stats = new ArrayList<>();

        SaisonDTO saisonDTO = saisonService.findOne(saisonId)
            .orElseThrow(
                () -> new ApiRequestException(
                    ExceptionCode.SAISON_NOT_FOUND.getMessage(),
                    ExceptionCode.SAISON_NOT_FOUND.getValue(),
                    ExceptionLevel.ERROR
                )
            );

        CompetitionDTO competitionDTO = competitionService.findOne(competitionId)
            .orElseThrow(
                () -> new ApiRequestException(
                    ExceptionCode.COMPETITION_NOT_FOUND.getMessage(),
                    ExceptionCode.COMPETITION_NOT_FOUND.getValue(),
                    ExceptionLevel.ERROR
                )
            );

        // get competitionSaison
        CompetitionSaisonDTO competitionSaisonDTO = communService.getCompetitionSaison(competitionDTO, saisonDTO);

        // get competitionSaisonCategory
        CompetitionSaisonCategoryDTO competitionSaisonCategoryDTO = communService.getCompetitionSaisonCategory(competitionSaisonDTO, categoryId);

        if (competitionSaisonDTO == null) {
            this.log.info("CompetitionSaison n'existe pas.");
        }

        // get all etapes
        List<EtapeCompetitionDTO> etapeCompetitionDTOS = this.etapeCompetitionService.findByCompetitionSaisonAndCompetitionSaisonCategory(competitionSaisonDTO, competitionSaisonCategoryDTO);

        if (etapeCompetitionDTOS == null || etapeCompetitionDTOS.isEmpty()) {
            this.log.warn("Aucune étape trouvée.");

            return null;
        }

        for (EtapeCompetitionDTO etapeCompetitionDTO : etapeCompetitionDTOS) {
            this.log.debug("___________________________________________________");
            this.log.debug("______ Etape : {}", etapeCompetitionDTO.getNom());
            this.log.debug("___________________________________________________");

            if (Boolean.FALSE.equals(etapeCompetitionDTO.isDeleted())) {
                if (critere.equals("club")) {
                    // get all participaitons
                    List<ParticipationDTO> participationDTOS = this.participationService.findByEtapeCompetition(etapeCompetitionDTO);

                    if (participationDTOS != null && !participationDTOS.isEmpty()) {
                        for (ParticipationDTO participationDTO : participationDTOS) {
                            if (Boolean.FALSE.equals(participationDTO.isDeleted())) {
                                Stat stat = this.classementService.getAssociationTotalEvents(etapeCompetitionDTO.getId(), participationDTO.getAssociationId(), type);

                                stats = this.classementService.addStat(stat, stats, type);
                            }
                        }
                    }
                }
                else if (critere.equals("joueur")) {
                    List<MatchDTO> matchDTOS = this.matchService.findByEtapeCompetition(etapeCompetitionDTO);

                    if (matchDTOS != null && !matchDTOS.isEmpty()) {
                        List<Stat> statList = this.classementService.getJoueurStats(matchDTOS, type);

                        stats = this.mergeStatsEtape(statList, stats, type);
                    }
                }
            }
        }

        if (type.equals("butContre")) {
            Collections.sort(stats);
        }
        else {
            Collections.sort(stats, Comparator.reverseOrder());
        }

        return new ResponseEntity<>(stats, HttpStatus.OK);
    }


    /**
     * GET  /mobile/saisons/:saisonId/competitions/:competitionId/categories/:categoryId/etape-competitions : get all the etapeCompetitions.
     *
     * @param competitionId L'id de la compétition
     * @param saisonId L'id de la saison
     * @param categoryId L'id de la catégorie
     * @return the CustomResponse with status 200 (OK) and the list of etapeCompetitions in body
     */
    @GetMapping("/mobile/saisons/{saisonId}/competitions/{competitionId}/categories/{categoryId}/etape-competitions")
    @Secured({AuthoritiesConstants.MOBILE})
    public ResponseEntity<List<EtapeCompetitionDTO>> getAllEtapeCompetitionsByCompetitionAndSaisonAndCategory(@PathVariable Long competitionId,
                                                                                                              @PathVariable Long saisonId,
                                                                                                              @PathVariable Long categoryId) throws Exception{
        log.debug("REST request to get EtapeCompetitions by competiton : {} and saison : {} and category : {}", competitionId, saisonId, categoryId);

        List<EtapeCompetitionDTO> etapeCompetitionDTOS = null;

        if (saisonId != null && competitionId != null && categoryId != null) {
            // find saison by id
            SaisonDTO  saisonDTO = saisonService.findOne(saisonId)
                .orElseThrow(() -> new ApiRequestException(
                    ExceptionCode.SAISON_NOT_FOUND.getMessage(),
                    ExceptionCode.SAISON_NOT_FOUND.getValue(),
                    ExceptionLevel.ERROR
                ));

            // find competition by id
            CompetitionDTO competitionDTO = competitionService.findOne(competitionId)
                .orElseThrow(() -> new ApiRequestException(
                    ExceptionCode.COMPETITION_NOT_FOUND.getMessage(),
                    ExceptionCode.COMPETITION_NOT_FOUND.getValue(),
                    ExceptionLevel.ERROR
                ));

            etapeCompetitionDTOS = communService.findEtapeCompetitonsBySaisonAndCompetitionAndCategory(saisonDTO, competitionDTO, categoryId);
        }

        return new ResponseEntity<>(etapeCompetitionDTOS, HttpStatus.OK);
    }


    /**
     * GET  /mobile/saisons/:saisonId/competitions/!competitionId/categories/:categoryId/poules-details : get poules-details.
     *
     * @return List of PouleDetail
     */
    @GetMapping("/mobile/saisons/{saisonId}/competitions/{competitionId}/categories/{categoryId}/poules-details")
    @Secured({AuthoritiesConstants.MOBILE})
    public ResponseEntity<List<PouleDetail>> getPoulesDetails(@PathVariable Long saisonId,
                                                              @PathVariable Long competitionId,
                                                              @PathVariable Long categoryId) {
        List<PouleDetail> pouleDetails = new ArrayList<>();

        // find saison by id
        SaisonDTO saisonDTO = saisonService.findOne(saisonId)
            .orElseThrow(() -> new ApiRequestException(
                ExceptionCode.SAISON_NOT_FOUND.getMessage(),
                ExceptionCode.SAISON_NOT_FOUND.getValue(),
                ExceptionLevel.ERROR
            ));

        // find competition by id
        CompetitionDTO competitionDTO = competitionService.findOne(competitionId)
            .orElseThrow(() -> new ApiRequestException(
                ExceptionCode.COMPETITION_NOT_FOUND.getMessage(),
                ExceptionCode.COMPETITION_NOT_FOUND.getValue(),
                ExceptionLevel.ERROR
            ));

        // find etape by id
        List<EtapeCompetitionDTO> etapeCompetitionDTOS = communService.findEtapeCompetitonsBySaisonAndCompetitionAndCategory(saisonDTO, competitionDTO, categoryId);

        List<PouleDTO> pouleDTOS = null;

        for (EtapeCompetitionDTO etapeCompetitionDTO : etapeCompetitionDTOS) {
            if (etapeCompetitionDTO.getNom().equals(Constant.PHASE_DE_GROUPE) ||
                etapeCompetitionDTO.getNom().equals(Constant.CHAMPIONNAT)) {
                //get all poules
                pouleDTOS = this.pouleService.findByEtapeCompetition(etapeCompetitionDTO);
                break;
            }
        }

        if(pouleDTOS != null) {
            for(PouleDTO pouleDTO: pouleDTOS) {
                PouleDetail pouleDetail = new PouleDetail();
                pouleDetail.setPouleDTO(pouleDTO);

                //get details poules
                List<DetailPouleDTO> detailPouleDTOS = this.detailPouleService.findByPoule(pouleDTO);

                if(detailPouleDTOS != null) {
                    HashMap<String, List<DetailPouleDTO>> pouleItems = new HashMap<>();
                    List<String> equipes = new ArrayList<>();
                    List<String> images  = new ArrayList<>();

                    for(DetailPouleDTO detailPouleDTO: detailPouleDTOS) {

                        //get association name
                        AssociationDTO associationDTO = this.associationService.findOne(detailPouleDTO.getAssociationId())
                            .orElseGet(() -> null);

                        if(associationDTO != null) {
                            String name = associationDTO.getNom();
                            String image = associationDTO.getCouleurs();

                            if(!equipes.contains(name)) {
                                equipes.add(name);
                                images.add(image);
                            }

                            List<DetailPouleDTO> items = pouleItems.get(name);
                            if(items == null) {
                                items = new ArrayList<>();
                            }

                            items.add(detailPouleDTO);
                            pouleItems.put(name, items);
                        }
                    }

                    List<DetailPouleDetail> detailPouleDetails = new ArrayList<>();

                    int i = 0;

                    for(String equipe: equipes) {
                        List<DetailPouleDTO> details = pouleItems.get(equipe);
                        DetailPouleDetail detailPouleDetail = new DetailPouleDetail();

                        String image = images.get(i);
                        i++;
                        detailPouleDetail.setImage(image);
                        detailPouleDetail.setEquipe(equipe);
                        detailPouleDetail.setDetailsPouleDTO(details);

                        detailPouleDetails.add(detailPouleDetail);
                    }


                    pouleDetail.setDetailPouleDetails(detailPouleDetails);
                }

                pouleDetails.add(pouleDetail);
            }
        }

        return new ResponseEntity<>(pouleDetails, HttpStatus.OK);
    }


    /**
     * GET  /mobile/saisons/:saisonId/competitions : get all the competitions.
     *
     * @param saisonId the 'saison' id
     * @return the CustomResponse with status 200 (OK) and the list of competitions in body
     */
    @GetMapping("/mobile/saisons/{saisonId}/competitions")
    @Secured({AuthoritiesConstants.MOBILE})
    public ResponseEntity<List<CompetitionDTO>> getAllCompetitionsBySaison(@PathVariable Long saisonId) throws Exception {
        log.debug("REST request to get Competitions by saison : {}", saisonId);

        List<Long> competitionIds = new ArrayList<>();

        // find saison by id
        SaisonDTO saisonDTO = saisonService.findOne(saisonId)
            .orElseThrow(() -> new ApiRequestException(
                ExceptionCode.SAISON_NOT_FOUND.getMessage(),
                ExceptionCode.SAISON_NOT_FOUND.getValue(),
                ExceptionLevel.ERROR
            ));

        // get competitionSaison
        List<CompetitionSaisonDTO> competitionSaisonDTOS = this.competitionSaisonService.findBySaison(saisonDTO);

        if (competitionSaisonDTOS != null && competitionSaisonDTOS.size() != 0) {

            for (CompetitionSaisonDTO competitionSaison: competitionSaisonDTOS) {
                competitionIds.add(competitionSaison.getCompetitionId());
            }
        }

        if (competitionIds.size() != 0) {
            List<CompetitionDTO> competitionDTOS = competitionService.findByIdIn(competitionIds);

            return new ResponseEntity<>(competitionDTOS, HttpStatus.OK);
        }

        return new ResponseEntity<>(null, HttpStatus.OK);
    }


    /**
     * GET  /mobile/organisations/:id/saisons : get all the saisons by organisation.
     *
     * @param pageable the pagination information
     * @return the CustomResponse with status 200 (OK) and the list of saisons in body
     */
    @GetMapping("/mobile/organisations/{id}/saisons")
    @Secured({AuthoritiesConstants.MOBILE})
    public ResponseEntity<List<SaisonDTO>> getAllSaisonsByOrganisation(@ApiParam Pageable pageable,
                                                                       @PathVariable Long id) {
        log.debug("REST request to get Saisons by organisation: {}", id);

        // get organisation
        OrganisationDTO organisationDTO = this.communService.checkOrganisation(id);

        Page<SaisonDTO> page = saisonService.findAllByOrganisation(organisationDTO, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /mobile/saisons/:saisonId/competitions/:competitionId/categories : get all categories for competition.
     *
     * @param competitionId the 'competition' id
     * @param saisonId the 'saison' id
     * @return the CustomResponse with status 200 (OK) and the list of competitions in body
     */
    @GetMapping("/mobile/saisons/{saisonId}/competitions/{competitionId}/categories")
    @Secured({AuthoritiesConstants.MOBILE})
    public ResponseEntity<List<CategoryDTO>> getCategoriesByCompetitionAndSaison(@PathVariable Long competitionId,
                                                                                 @PathVariable Long saisonId) {
        log.debug("REST request to get categories by competiton : {} and saison: {}", competitionId, saisonId);

        List<CategoryDTO> categories = new ArrayList<>();

        // recupérer l'objet saison
        SaisonDTO saisonDTO = saisonService.findOne(saisonId)
            .orElseThrow(
                () -> new ApiRequestException(
                    ExceptionCode.SAISON_NOT_FOUND.getMessage(),
                    ExceptionCode.SAISON_NOT_FOUND.getValue(),
                    ExceptionLevel.ERROR
                )
            );

        // recupérer l'objet competition
        CompetitionDTO competitionDTO = competitionService.findOne(competitionId)
            .orElseThrow(
                () -> new ApiRequestException(
                    ExceptionCode.COMPETITION_NOT_FOUND.getMessage(),
                    ExceptionCode.COMPETITION_NOT_FOUND.getValue(),
                    ExceptionLevel.ERROR
                )
            );

        // find all categories
        List<CategoryDTO> categoryDTOS = categoryService.findAll();

        if (categoryDTOS == null || categoryDTOS.size() == 0) {
            throw new ApiRequestException(
                ExceptionCode.CATEGORY_LIST_EMPTY.getMessage(),
                ExceptionCode.CATEGORY_LIST_EMPTY.getValue(),
                ExceptionLevel.ERROR
            );
        }

        for (CategoryDTO categoryDTO : categoryDTOS) {
            List<CategoryDTO> competitionCategories = communService.getCompetitionSaisonCategories(competitionDTO, saisonDTO);

            if (communService.checkCategoryExist(categoryDTO, competitionCategories)) {
                categories.add(categoryDTO);
            }
        }

        return new ResponseEntity<>(categories, HttpStatus.OK);
    }


    /**
     * GET  /mobile/matchs/delegue/details : get delegueMatch details.
     *
     * @param codeMatch
     * @return
     */
    @GetMapping("/mobile/matchs/delegue/details")
    public ResponseEntity<Object> getDelegueMatch(@RequestParam String codeMatch) {

        // get delegueMatch by code
        DelegueMatchDTO delegueMatchDTO = this.delegueMatchService.findByConfirmCode(codeMatch).orElseGet(() -> null);


        if (delegueMatchDTO == null) {
            CustomError error = new CustomError(4000, HttpStatus.BAD_REQUEST, "Le code d'authorisation n'est pas valide.", "Mauvais code");
            return new ResponseEntity<Object>(error, HttpStatus.BAD_REQUEST);
        }

//        log.debug("code actuel: {}", delegueMatchDTO.getConfirmCode());
//        log.debug("code confirm: {}", confirmCode);

//        if(!delegueMatchDTO.getConfirmCode().equals(confirmCode)) {
//            CustomError error = new CustomError(4006, HttpStatus.BAD_REQUEST, "Le code de confirmation est invalide.", "Echec confirmation");
//            return new ResponseEntity<Object>(error, HttpStatus.BAD_REQUEST);
//        }

        if(delegueMatchDTO.isDeleted()) {
            CustomError error = new CustomError(4002, HttpStatus.BAD_REQUEST, "Le code d'authorisation est annulé.", "Code annulé");
            return new ResponseEntity<Object>(error, HttpStatus.BAD_REQUEST);
        }

        if(delegueMatchDTO.getStatus().equals("disabled")) {
            CustomError error = new CustomError(4003, HttpStatus.BAD_REQUEST, "Le code d'authorisation n'est pas activé.", "Code désactivé");
            return new ResponseEntity<Object>(error, HttpStatus.BAD_REQUEST);
        }

        DelegueMatchDetail delegueMatchDetail = new DelegueMatchDetail();

        Long delegueId = delegueMatchDTO.getDelegueId();
        Long matchId   = delegueMatchDTO.getMatchId();

        ///***********************
        ///get matchDTO
        ///***********************
        MatchDTO matchDTO = this.matchService.findOne(matchId).orElseGet(() -> null);
        if(matchDTO == null || matchDTO.isDeleted()) {
            CustomError error = new CustomError(4004, HttpStatus.BAD_REQUEST, "Aucun match trouvé pour le code " + codeMatch, "Mauvais code");
            return new ResponseEntity<Object>(error, HttpStatus.BAD_REQUEST);
        }

        ///**********************
        ///get DelegueDTO
        ///**********************
        DelegueDTO delegueDTO = this.delegueService.findOne(delegueId).orElseGet(() -> null);
        if(delegueDTO == null || delegueDTO.isDeleted()) {
            CustomError error = new CustomError(4005, HttpStatus.BAD_REQUEST, "Aucun délégué trouvé pour le code " + codeMatch, "Mauvais code");
            return new ResponseEntity<Object>(error, HttpStatus.BAD_REQUEST);
        }

        ///************************
        ///get local and visiteur
        ///************************
        Long localId    = matchDTO.getLocalId();
        Long visiteurId = matchDTO.getVisiteurId();

        AssociationDTO local    = this.associationService.findOne(localId)
            .orElseThrow(() -> new ApiRequestException(
                ExceptionCode.CLUB_LOCAL_NOT_FOUND_FOR_MATCH.getMessage(),
                ExceptionCode.CLUB_LOCAL_NOT_FOUND_FOR_MATCH.getValue(),
                ExceptionLevel.ERROR
            ));

        AssociationDTO visiteur = this.associationService.findOne(visiteurId)
            .orElseThrow(() -> new ApiRequestException(
                ExceptionCode.CLUB_LOCAL_NOT_FOUND_FOR_MATCH.getMessage(),
                ExceptionCode.CLUB_LOCAL_NOT_FOUND_FOR_MATCH.getValue(),
                ExceptionLevel.ERROR
            ));

        ///************************
        ///get feuille de match
        ///************************
        FeuilleDeMatchDetail feuilleDeMatchDetail = communService.getFeuilleDeMatch(matchDTO);

        ///*************************
        ///get joueurs
        ///*************************
        List<JoueurDTO> localJoueurs    = new ArrayList<>();
        List<JoueurDTO> visiteurJoueurs = new ArrayList<>();

        Long saisonId = null;
        Long competitionId = null;

        EtapeCompetitionDTO etape = this.etapeCompetitionService.findOne(matchDTO.getEtapeCompetitionId()).orElseGet(() -> null);
        if (etape != null && !etape.isDeleted()) {
            ///********************************
            /// get etapeCompetition infos
            ///********************************
            delegueMatchDetail.setEtapeNom(etape.getNom());

            CompetitionSaisonDTO csDTO = this.competitionSaisonService.findOne(etape.getCompetitionSaisonId()).orElseGet(() -> null);
            if (csDTO != null) {
                saisonId      = csDTO.getSaisonId();
                competitionId = csDTO.getCompetitionId();
            }
        }

        ///********************************
        /// get competition infos
        ///********************************
        if (competitionId != null) {
            CompetitionDTO competitionDTO = this.competitionService.findOne(competitionId).orElseGet(() -> null);

            if (competitionDTO != null && !competitionDTO.isDeleted()) {
                delegueMatchDetail.setCompetitionId(competitionDTO.getId());
                delegueMatchDetail.setCompetitionHashcode(competitionDTO.getHashcode());
                delegueMatchDetail.setCompetitionNom(competitionDTO.getNom());
            }
            else {
                CustomError error = new CustomError(4006, HttpStatus.BAD_REQUEST, "Compétition inexistante!" + codeMatch, "Aucune competition");
                return new ResponseEntity<Object>(error, HttpStatus.BAD_REQUEST);
            }
        }

        ///********************************
        /// get saison infos
        ///********************************
        if (saisonId != null) {
            SaisonDTO saisonDTO = this.saisonService.findOne(saisonId).orElseGet(() -> null);
            if (saisonDTO != null && !saisonDTO.isDeleted()) {
                delegueMatchDetail.setSaisonId(saisonDTO.getId());
                delegueMatchDetail.setSaisonHashcode(saisonDTO.getHashcode());
                delegueMatchDetail.setSaisonNom(saisonDTO.getLibelle());
            }
            else {
                CustomError error = new CustomError(4007, HttpStatus.BAD_REQUEST, "Saison inexistante!" + codeMatch, "Aucune saison");
                return new ResponseEntity<Object>(error, HttpStatus.BAD_REQUEST);
            }

            /// set organisationId
            delegueMatchDetail.setOrganisationId(saisonDTO.getOrganisationId());

            List<ContratDTO> localContratDTOs = this.contratService.findBySaisonAndAssociation(saisonDTO, local);
            List<ContratDTO> visiteurContratDTOs = this.contratService.findBySaisonAndAssociation(saisonDTO, visiteur);

            if (localContratDTOs != null && visiteurContratDTOs != null) {
                //get local joueurs
                if (localContratDTOs.size() != 0) {
                    for (ContratDTO contratDTO : localContratDTOs) {
                        JoueurDTO joueurDTO = this.joueurService.findOne(contratDTO.getJoueurId())
                            .orElseThrow(() -> new ApiRequestException(
                                ExceptionCode.JOUEUR_NOT_FOUND.getMessage(),
                                ExceptionCode.JOUEUR_NOT_FOUND.getValue(),
                                ExceptionLevel.ERROR
                            ));

                        if (!joueurDTO.isDeleted()) {
                            localJoueurs.add(joueurDTO);
                        }
                    }
                }

                //get visiteur joueurs
                if (visiteurContratDTOs.size() != 0) {
                    for (ContratDTO contratDTO : visiteurContratDTOs) {
                        JoueurDTO joueurDTO = this.joueurService.findOne(contratDTO.getJoueurId())
                            .orElseThrow(() -> new ApiRequestException(
                                ExceptionCode.JOUEUR_NOT_FOUND.getMessage(),
                                ExceptionCode.JOUEUR_NOT_FOUND.getValue(),
                                ExceptionLevel.ERROR
                            ));

                        if (!joueurDTO.isDeleted()) {
                            visiteurJoueurs.add(joueurDTO);
                        }
                    }
                }
            }
        }

        ///********************************
        /// get all events
        ///********************************

        EventsDetail events = communService.getEventsDetails(matchDTO.getId());

        this.log.debug("********************************************");
        this.log.debug("**********  buts : {}          *************", events.getButsDetail().toArray());
        this.log.debug("**********  cartons : {}       *************", events.getCartonsDetail().toArray());
        this.log.debug("**********  penalties : {}     *************", events.getPenaltiesDetail().toArray());
        this.log.debug("**********  corners : {}       *************", events.getCornersDetail().toArray());
        this.log.debug("**********  coupFrancs : {}    *************", events.getCoupFrancsDetail().toArray());
        this.log.debug("**********  remplacements : {} *************", events.getRemplacementsDetail().toArray());
        this.log.debug("**********  horsJeus : {}      *************", events.getHorsJeusDetail().toArray());
        this.log.debug("**********  tirs : {}          *************", events.getTirsDetail().toArray());
        this.log.debug("********************************************");

        // set matchDetail
        MatchDetail matchDetail = new MatchDetail();
        matchDetail.setLocalNom(local.getNom());
        matchDetail.setLocalImage(local.getCouleurs());
        matchDetail.setVisiteurNom(visiteur.getNom());
        matchDetail.setVisiteurImage(visiteur.getCouleurs());
        matchDetail.setMatchDTO(matchDTO);

        // set events
        delegueMatchDetail.setEvents(events);

        // set delegueMatchDetail
        delegueMatchDetail.setId(delegueMatchDTO.getId());
        delegueMatchDetail.setCode(delegueMatchDTO.getConfirmCode());
        delegueMatchDetail.setMatch(matchDetail);
        delegueMatchDetail.setDelegue(delegueDTO);
        delegueMatchDetail.setFeuilleDeMatch(feuilleDeMatchDetail);
        delegueMatchDetail.setLocalJoueurs(localJoueurs);
        delegueMatchDetail.setVisiteurJoueurs(visiteurJoueurs);
        delegueMatchDetail.setLocalId(local.getId());
        delegueMatchDetail.setVisiteurId(visiteur.getId());

        //delete matchCode
        delegueMatchDTO.setConfirmCode("");
        this.delegueMatchService.save(delegueMatchDTO);

        return new ResponseEntity<>(delegueMatchDetail, HttpStatus.OK);
    }

    @GetMapping("/mobile/matchs/state")
    public ResponseEntity<Object> changeMatchState(@RequestParam Long matchId,
                                                   @RequestParam String etatMatch,
                                                   @RequestParam String chrono,
                                                   @RequestParam Boolean notif) {
        MatchDTO matchDTO;

        this.log.info("******** ETAT MATCH {}", etatMatch);

        /// get match by id
        Optional<MatchDTO> matchDTOOptional = this.matchService.findOne(matchId);

        if (matchDTOOptional.isEmpty() || Boolean.TRUE.equals(matchDTOOptional.get().isDeleted())) {
            var error = new CustomError(4008, HttpStatus.BAD_REQUEST, "Match inexistant!" + matchId, "Aucun match");
            return new ResponseEntity<Object>(error, HttpStatus.BAD_REQUEST);
        } else {
            matchDTO = matchDTOOptional.get();
        }


        EtatMatch oldEtatMatch = matchDTO.getEtatMatch();

        // set chrono
        matchDTO.setChrono(chrono);

        if (etatMatch.equals("PROGRAMME")) {
            matchDTO.setEtatMatch(EtatMatch.PROGRAMME);
        }
        else if (etatMatch.equals("EN_COURS_1")) {
            matchDTO.setEtatMatch(EtatMatch.EN_COURS_1);
        }
        else if (etatMatch.equals("EN_COURS_2")) {
            matchDTO.setEtatMatch(EtatMatch.EN_COURS_2);
        }
        else if (etatMatch.equals("EN_COURS_3")) {
            matchDTO.setEtatMatch(EtatMatch.EN_COURS_3);
        }
        else if (etatMatch.equals("EN_COURS_4")) {
            matchDTO.setEtatMatch(EtatMatch.EN_COURS_4);
        }
        else if (etatMatch.equals("MI_TEMPS_1")) {
            matchDTO.setEtatMatch(EtatMatch.MI_TEMPS_1);
        }
        else if (etatMatch.equals("MI_TEMPS_2")) {
            matchDTO.setEtatMatch(EtatMatch.MI_TEMPS_2);
        }
        else if (etatMatch.equals("TERMINE")) {
            matchDTO.setEtatMatch(EtatMatch.TERMINE);
        }
        else if (etatMatch.equals("ANNULE")) {
            matchDTO.setEtatMatch(EtatMatch.ANNULE);
        }
        else if (etatMatch.equals("PROLONGATION")) {
            matchDTO.setEtatMatch(EtatMatch.PROLONGATION);
        }
        else if (etatMatch.equals("TIR_AU_BUT")) {
            matchDTO.setEtatMatch(EtatMatch.TIR_AU_BUT);
        }

        matchDTO = this.matchService.save(matchDTO);

        if (Boolean.TRUE.equals(notif) && !etatMatch.equals(oldEtatMatch.name())) {
            /// send to notif server
            this.notificationService.sendEtatMatchNotif(matchDTO, etatMatch);
        }

        // send etatMatch to clients
        this.webSocketApiClientService.sendToClientViaWebSocket(
            WebSocketTopic.MATCH_STATE.getTopic(matchDTO.getId()),
            matchDTO.getEtatMatch());
        this.webSocketApiClientService.sendToRabbitMQ(RabbitMQConfig.MATCH_LIVE_ROUTING_KEY, List.of(matchDTO.getId().toString()), RabbitMQMessage.build(matchDTO.getEtatMatch(), RabbitMQMessageType.MATCH_STATE));

        this.log.info("******* ETAT MATCH RESPONSE {}", matchDTO);

        return ResponseEntity.ok(matchDTO);
    }

    @GetMapping("/mobile/matchs/chrono")
    public ResponseEntity<Object> changeMatchChrono(@RequestParam Long matchId, @RequestParam String chrono,
                                                    @RequestParam Boolean notif) {
        MatchDTO matchDTO;

        /// get match by id
        Optional<MatchDTO> matchDTOOptional = this.matchService.findOne(matchId);

        if (matchDTOOptional.isEmpty() || Boolean.TRUE.equals(matchDTOOptional.get().isDeleted())) {
            var error = new CustomError(4009, HttpStatus.BAD_REQUEST, "Match inexistant!" + matchId, "Aucun match");
            return new ResponseEntity<Object>(error, HttpStatus.BAD_REQUEST);
        }

        matchDTO = matchDTOOptional.get();
        matchDTO.setChrono(chrono);

        matchDTO = this.matchService.save(matchDTO);

        if (Boolean.TRUE.equals(notif)) {
            /// send to notif server
            // this.notificationService.sendChronoNotif(matchDTO, chrono);
            this.webSocketApiClientService.sendToClientViaWebSocket(
                WebSocketTopic.MATCH_CHRONO.getTopic(matchDTO.getId()),
                Integer.parseInt(chrono)
            );
            this.webSocketApiClientService.sendToRabbitMQ(RabbitMQConfig.MATCH_LIVE_ROUTING_KEY, List.of(matchDTO.getId().toString()), RabbitMQMessage.build(Integer.parseInt(chrono), RabbitMQMessageType.MATCH_CHRONO));

            // vérifier si on doit envoyer un notif de rappel
            Integer mn = Integer.parseInt(matchDTO.getChrono());
            List<Integer> inter = List.of(10, 20, 30, 40, 55, 65, 75, 85, 100, 115);

            if (inter.contains(mn)) {
                this.notificationService.sendRappel(matchDTO);
            }
        }

        return ResponseEntity.ok(matchDTO);
    }

    /**
     * GET  /mobile/competitions : get all competitions with 'saisons'.
     *
     * @return the CustomResponse with status 200 (OK) and the list of competitions in body
     */
    @GetMapping("/mobile/competitions")
    public ResponseEntity<List<CompetitionWithSaisonsAndMatchs>> getCompetitionsWithSaisons(@RequestParam(required = false) String keyword,
                                                                                            @RequestParam(required = false) String selectedDate) {
        // find all competitions
        List<CompetitionWithSaisonsAndMatchs> competitions  = competitionService.findAllCompetitionsWithSaisonsAndMatchs(keyword, selectedDate);

        return ResponseEntity.ok().body(competitions);
    }

    /**
     * POST  /mobile/joueurs/:id/image : Ajouter une image au joueur.
     *
     * Upload joueur image
     */
    @PostMapping("/mobile/joueurs/image")
    public ResponseEntity<JoueurDTO> uploadImage(@RequestParam("file") MultipartFile file, @RequestParam String hashcode) {
        // find joueur by id
        var joueurDTO = joueurService.findByHashcode(hashcode)
            .orElseThrow(
                () -> new ApiRequestException(
                    ExceptionCode.JOUEUR_NOT_FOUND.getMessage(),
                    ExceptionCode.JOUEUR_NOT_FOUND.getValue(),
                    ExceptionLevel.ERROR
                )
            );

        return ResponseEntity.ok().body(communService.addJoueurImage(file, joueurDTO));
    }

    /**
     * GET  /mobile/matchs/:matchId/externalstatse : Get external stats
     */
    @GetMapping("/mobile/matchs/{matchId}/externalstats")
    public ResponseEntity<List<TeamStatistic>> getExternalStats(@PathVariable Long matchId) {
        return ResponseEntity.ok(externalApiService.getExternalStats(matchId));
    }
    /**
     * GET  /majfixtures
     */
    @GetMapping("/majfixtures")
    public ResponseEntity<String> majFixtures() {
        externalApiService.majFixtures();
        return ResponseEntity.ok("OK");
    }

    private List<Stat> mergeStatsEtape(List<Stat> statsNew, List<Stat> statsOld, String type) {
        if (statsNew == null || statsNew.isEmpty()) {
            return statsOld;
        }
        else {
            for (Stat stat : statsNew) {
                statsOld = this.classementService.addStat(stat, statsOld, type);
            }

            return statsOld;
        }
    }
}
