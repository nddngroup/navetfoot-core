package org.nddngroup.navetfoot.core.web.rest;

import org.nddngroup.navetfoot.core.domain.TirAuBut;
import org.nddngroup.navetfoot.core.service.TirAuButService;
import org.nddngroup.navetfoot.core.service.dto.TirAuButDTO;
import org.nddngroup.navetfoot.core.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link TirAuBut}.
 */
@RestController
@RequestMapping("/api")
public class TirAuButResource {

    private final Logger log = LoggerFactory.getLogger(TirAuButResource.class);

    private static final String ENTITY_NAME = "navetfootCoreTirAuBut";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TirAuButService tirAuButService;

    public TirAuButResource(TirAuButService tirAuButService) {
        this.tirAuButService = tirAuButService;
    }

    /**
     * {@code POST  /tir-au-buts} : Create a new tirAuBut.
     *
     * @param tirAuButDTO the tirAuButDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new tirAuButDTO, or with status {@code 400 (Bad Request)} if the tirAuBut has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/tir-au-buts")
    public ResponseEntity<TirAuButDTO> createTirAuBut(@RequestBody TirAuButDTO tirAuButDTO) throws URISyntaxException {
        log.debug("REST request to save TirAuBut : {}", tirAuButDTO);
        if (tirAuButDTO.getId() != null) {
            throw new BadRequestAlertException("A new tirAuBut cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TirAuButDTO result = tirAuButService.save(tirAuButDTO);
        return ResponseEntity.created(new URI("/api/tir-au-buts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /tir-au-buts} : Updates an existing tirAuBut.
     *
     * @param tirAuButDTO the tirAuButDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tirAuButDTO,
     * or with status {@code 400 (Bad Request)} if the tirAuButDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the tirAuButDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/tir-au-buts")
    public ResponseEntity<TirAuButDTO> updateTirAuBut(@RequestBody TirAuButDTO tirAuButDTO) throws URISyntaxException {
        log.debug("REST request to update TirAuBut : {}", tirAuButDTO);
        if (tirAuButDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TirAuButDTO result = tirAuButService.save(tirAuButDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, tirAuButDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /tir-au-buts} : get all the tirAuButs.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of tirAuButs in body.
     */
    @GetMapping("/tir-au-buts")
    public ResponseEntity<List<TirAuButDTO>> getAllTirAuButs(Pageable pageable) {
        log.debug("REST request to get a page of TirAuButs");
        Page<TirAuButDTO> page = tirAuButService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /tir-au-buts/:id} : get the "id" tirAuBut.
     *
     * @param id the id of the tirAuButDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the tirAuButDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/tir-au-buts/{id}")
    public ResponseEntity<TirAuButDTO> getTirAuBut(@PathVariable Long id) {
        log.debug("REST request to get TirAuBut : {}", id);
        Optional<TirAuButDTO> tirAuButDTO = tirAuButService.findOne(id);
        return ResponseUtil.wrapOrNotFound(tirAuButDTO);
    }

    /**
     * {@code DELETE  /tir-au-buts/:id} : delete the "id" tirAuBut.
     *
     * @param id the id of the tirAuButDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/tir-au-buts/{id}")
    public ResponseEntity<Void> deleteTirAuBut(@PathVariable Long id) {
        log.debug("REST request to delete TirAuBut : {}", id);
        tirAuButService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/tir-au-buts?query=:query} : search for the tirAuBut corresponding
     * to the query.
     *
     * @param query the query of the tirAuBut search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/tir-au-buts")
    public ResponseEntity<List<TirAuButDTO>> searchTirAuButs(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of TirAuButs for query {}", query);
        Page<TirAuButDTO> page = tirAuButService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
        }
}
