package org.nddngroup.navetfoot.core.web.rest.custom;

import org.nddngroup.navetfoot.core.exception.ApiRequestException;
import org.nddngroup.navetfoot.core.exception.ExceptionCode;
import org.nddngroup.navetfoot.core.exception.ExceptionLevel;
import org.nddngroup.navetfoot.core.service.*;
import org.nddngroup.navetfoot.core.service.custom.CommunService;
import org.nddngroup.navetfoot.core.service.custom.DeleteService;
import org.nddngroup.navetfoot.core.service.dto.*;
import org.nddngroup.navetfoot.core.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import org.nddngroup.navetfoot.core.service.*;
import org.nddngroup.navetfoot.core.service.dto.*;
import org.nddngroup.navetfoot.core.web.rest.RegionResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.ZonedDateTime;
import java.util.List;

@RestController
@RequestMapping("/api")
public class CustomJoueurResource {
    private final Logger log = LoggerFactory.getLogger(RegionResource.class);

    private static final String ENTITY_NAME = "navetfootCoreJoueur";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final JoueurService joueurService;

    @Autowired
    private CommunService communService;
    @Autowired
    private SaisonService saisonService;
    @Autowired
    private AssociationService associationService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private ContratService contratService;
    @Autowired
    private JoueurCategoryService joueurCategoryService;
    @Autowired
    private DeleteService deleteService;

    public CustomJoueurResource(JoueurService joueurService) throws Exception {
        this.joueurService = joueurService;
    }

    /**
     * POST  /organisations/:id/joueurs : Create a new joueur.
     *
     * @param id L'id de l'organisation
     * @param joueurDTO Le joueur à ajouter
     * @param saisonId La saison
     * @param associationId Le club
     * @param categoryId La catégorie
     * @return the CustomResponse with status 201 (Created) and with body the new joueurDTO, or with status 400 (Bad Request) if the joueur has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/organisations/{id}/joueurs")
    public ResponseEntity<JoueurDTO> createJoueur(@PathVariable Long id,
                                                  @Valid @RequestBody JoueurDTO joueurDTO,
                                                  @RequestParam Long saisonId,
                                                  @RequestParam Long associationId,
                                                  @RequestParam(required = false) Long categoryId
    ) throws Exception {
        log.debug("REST request to save Joueur : {}", joueurDTO);

        // find organisation
        OrganisationDTO organisationDTO = this.communService.checkOrganisation(id);

        // find saison
        SaisonDTO saisonDTO = communService.checkSaisonForOrganisation(saisonId, organisationDTO);

        // find association
        AssociationDTO associationDTO = this.associationService.findOne(associationId)
            .orElseThrow(
                () -> new ApiRequestException(ExceptionCode.ASSOCIATIONS_NOT_FOUND.getMessage(), ExceptionCode.ASSOCIATIONS_NOT_FOUND.getValue(), ExceptionLevel.ERROR)
            );

        // vérifier si categoryId est null
        if (categoryId == null) {
            if (joueurDTO.getId() == null) { // nouveau joueur
                throw new ApiRequestException(
                    ExceptionCode.CATEGORY_NOT_FOUND.getMessage(),
                    ExceptionCode.CATEGORY_NOT_FOUND.getValue(),
                    ExceptionLevel.ERROR
                );
            } else {
                // find joueur category
                JoueurCategoryDTO joueurCategoryDTO = joueurCategoryService.findByJoueur(joueurDTO)
                    .orElseThrow(() -> new ApiRequestException(
                            ExceptionCode.CATEGORY_NOT_FOUND.getMessage(),
                            ExceptionCode.CATEGORY_NOT_FOUND.getValue(),
                            ExceptionLevel.ERROR
                        )
                    );

                categoryId = joueurCategoryDTO.getCategoryId();
            }

        }

        // find category by id
        CategoryDTO categoryDTO = categoryService.findOne(categoryId)
            .orElseThrow(
                () -> new ApiRequestException(
                    ExceptionCode.CATEGORY_NOT_FOUND.getMessage(),
                    ExceptionCode.CATEGORY_NOT_FOUND.getValue(),
                    ExceptionLevel.ERROR
                )
            );

        joueurDTO = joueurService.save(joueurDTO, organisationDTO, saisonDTO, associationDTO, categoryDTO);


        return ResponseEntity.created(new URI("/api/organisations/" + id + "/joueurs/" + joueurDTO.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, joueurDTO.getId().toString()))
            .body(joueurDTO);
    }

    /**
     * POST /organisations/:organisationId/saisons/:saisonId/associations/:associationId/import/joueurs : import list of joueurs
     *
     * @param joueurDTOS Liste des joueurs
     * @param organisationId L'id de l'organisation
     * @param saisonId L'id de la saison
     * @param associationId L'id du club
     * @return List<JoueurDTO>
     */
    @PostMapping("/organisations/{organisationId}/saisons/{saisonId}/associations/{associationId}/import/joueurs")
    public ResponseEntity<List<JoueurDTO>> importJoueurs(@RequestBody List<JoueurDTO> joueurDTOS,
                                                                   @PathVariable Long organisationId,
                                                                   @PathVariable Long saisonId,
                                                                   @PathVariable Long associationId
                                                              ) throws URISyntaxException {
        OrganisationDTO organisationDTO = communService.checkOrganisation(organisationId);

        SaisonDTO saisonDTO = communService.checkSaisonForOrganisation(saisonId, organisationDTO);

        // find association by id
        AssociationDTO associationDTO = associationService.findOne(associationId)
            .orElseThrow(() -> new ApiRequestException(
                ExceptionCode.ASSOCIATIONS_NOT_FOUND.getMessage(),
                ExceptionCode.ASSOCIATIONS_NOT_FOUND.getValue(),
                ExceptionLevel.ERROR
            ));

        List<JoueurDTO> result = joueurService.save(organisationDTO, saisonDTO, associationDTO, joueurDTOS);

        return ResponseEntity.created(new URI("/api/organisations/" + organisationId + "/saisons/" + saisonId + "/associations/" + associationId + "/import/joueurs"))
            .body(result);
    }

    /**
     * DELETE /organisations/:organisationId/joueurs : remove list of joueurs
     *
     * @param headers HttpHeaders
     * @return List<JoueurDTO>
     */
    @DeleteMapping("/organisations/{organisationId}/joueurs")
    public ResponseEntity<List<JoueurDTO>> removeJoueurs(@RequestHeader HttpHeaders headers,
                                                                   @RequestParam(required = false) Boolean owner,
                                                                   @PathVariable Long organisationId) throws URISyntaxException {
        OrganisationDTO organisationDTO = communService.checkOrganisation(organisationId);
        List<String> joueurIds = headers.getValuesAsList("ids");

        List<JoueurDTO> result = deleteService.removeJoueurList(organisationDTO, null, joueurIds, owner);

        return ResponseEntity.created(new URI("/api/organisations/" + organisationId + "/joueurs/"))
            .body(result);
    }

    /**
     * POST /organisations/:organisationId/saisons/:saisonId/joueurs : remove list of joueurs
     *
     * @param headers HttpHeaders
     * @param organisationId L'id de l'organisation
     * @param associationId L'id du club
     * @return List<JoueurDTO>
     */
    @DeleteMapping("/organisations/{organisationId}/associations/{associationId}/joueurs")
    public ResponseEntity<List<JoueurDTO>> removeAssociations(@RequestHeader HttpHeaders headers,
                                                              @RequestParam(required = false) Boolean owner,
                                                              @PathVariable Long organisationId,
                                                              @PathVariable Long associationId
                                                            ) throws URISyntaxException {
        OrganisationDTO organisationDTO = communService.checkOrganisation(organisationId);

        List<String> joueurIds = headers.getValuesAsList("ids");

        // find association
        AssociationDTO associationDTO = associationService.findOne(associationId)
            .orElseThrow(() -> new ApiRequestException(
                ExceptionCode.ASSOCIATIONS_NOT_FOUND.getMessage(),
                ExceptionCode.ASSOCIATIONS_NOT_FOUND.getValue(),
                ExceptionLevel.ERROR
            ));

        List<JoueurDTO> result = deleteService.removeJoueurList(organisationDTO, associationDTO,  joueurIds, owner);

        return ResponseEntity.created(new URI("/api/organisations/" + organisationId + "/associations/" + associationId + "/joueurs/"))
            .body(result);
    }

    /**
     * POST  /joueurs/:id/image : Ajouter une image au joueur.
     *
     * Upload joueur image
     */
    @PostMapping("/joueurs/{id}/image")
    public ResponseEntity<JoueurDTO> uploadImage(@RequestParam("file") MultipartFile file, @PathVariable Long id) {
        // find joueur by id
        var joueurDTO = joueurService.findOne(id)
            .orElseThrow(
                () -> new ApiRequestException(
                    ExceptionCode.JOUEUR_NOT_FOUND.getMessage(),
                    ExceptionCode.JOUEUR_NOT_FOUND.getValue(),
                    ExceptionLevel.ERROR
                )
            );

        return ResponseEntity.ok().body(communService.addJoueurImage(file, joueurDTO));
    }

    /**
     * {@code PUT  /organisations/:id/joueurs} : Updates an existing joueur.
     *
     * @param joueurDTO the joueurDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated joueurDTO,
     * or with status {@code 400 (Bad Request)} if the joueurDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the joueurDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/organisations/{organisationId}/joueurs")
    public ResponseEntity<JoueurDTO> updateJoueur(@PathVariable Long organisationId,
                                                  @Valid @RequestBody JoueurDTO joueurDTO) throws URISyntaxException {
        log.debug("REST request to update Joueur : {}", joueurDTO);
        if (joueurDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }

        // check numero licence
        JoueurDTO found = joueurService.findByIdentifiant(joueurDTO.getIdentifiant())
            .orElseGet(() -> null);

        if (found != null && !found.getId().equals(joueurDTO.getId())) {
            throw new ApiRequestException(
                ExceptionCode.IDENTIFIANT_EXISTS.getMessage(),
                ExceptionCode.IDENTIFIANT_EXISTS.getValue(),
                ExceptionLevel.WARNING
            );
        }

        joueurDTO.setUpdatedAt(ZonedDateTime.now());

        JoueurDTO result = joueurService.save(joueurDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, joueurDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /organisations/:organisationId/joueurs : get all the joueurs.
     *
     * @param organisationId //
     * @param saisonId //
     * @param associationId //
     * @param pageable the pagination information
     * @return the CustomResponse with status 200 (OK) and the list of joueurs in body
     */
    @GetMapping("/organisations/{organisationId}/joueurs")
    public ResponseEntity<List<JoueurDTO>> getAllJoueurs(
        @PathVariable Long organisationId,
        @RequestParam(required = false) Long saisonId,
        @RequestParam(required = false) Long associationId,
        @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Joueurs");

        // list of id joueur
        List<Long> joueurIds;

        // get organisation
        OrganisationDTO organisationDTO = this.communService.checkOrganisation(organisationId);

        joueurIds = communService.findJoueurIds(organisationDTO, saisonId, associationId);

        Page<JoueurDTO> page = joueurService.findByIdIn(joueurIds, pageable);

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /organisations/:id/associations/:associationId/joueurs : get all the joueurs of club.
     *
     * @param id L'id de l'organisation
     * @param associationId Le club
     * @param saisonId La saison
     * @param categoryId La catégorie
     * @return the CustomResponse with status 200 (OK) and the list of joueurs in body
     */
    @GetMapping("/organisations/{id}/associations/{associationId}/joueurs")
    public ResponseEntity<List<JoueurDTO>> getAllJoueursByClubAndSaison(
        Pageable pageable,
        @PathVariable Long id,
        @PathVariable Long associationId,
        @RequestParam(required = false) Long saisonId,
        @RequestParam(required = false) Long categoryId
    ) {
        log.debug("REST request to get Joueurs of club");

        // list joueurs
        List<Long> joueurIds;

        // get organisation
        OrganisationDTO organisationDTO = this.communService.checkOrganisation(id);

        joueurIds = communService.findJoueurIds(organisationDTO, saisonId, associationId);

        joueurIds.addAll(communService.findJoueurIds(organisationDTO, saisonId, associationId));

        Page<JoueurDTO> page = joueurService.findByIdIn(joueurIds, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);

        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /joueurs/by/licence/:licence.
     *
     * @param licence the 'identifiant' of the joueurDTO to retrieve
     * @return the CustomResponse with status 200 (OK) and with body the joueurDTO, or with status 404 (Not Found)
     */
    @GetMapping("/joueurs/by/licence/{licence}")
    public ResponseEntity<JoueurDTO> getJoueurByLicense(@PathVariable String licence) {
        log.debug("REST request to get Joueur : {}", licence);

        JoueurDTO joueurDTO = joueurService.findByIdentifiant(licence)
            .orElseGet(() -> null);

        return new ResponseEntity<>(joueurDTO, HttpStatus.OK);
    }


    /**
     * DELETE  /organisations/:organisationId/joueurs/:id : delete the "id" joueur.
     *
     * @param organisationId L'id de l'organisation
     * @param id the id of the joueurDTO to delete
     * @return the CustomResponse with status 200 (OK)
     */
    @DeleteMapping("/organisations/{organisationId}/joueurs/{id}")
    public ResponseEntity<Boolean> deleteJoueur(@PathVariable Long organisationId,
                                                @RequestParam(required = false) Long associationId,
                                                @PathVariable Long id) {
        log.debug("REST request to delete Joueur : {}", id);

        OrganisationDTO organisationDTO = communService.checkOrganisation(organisationId);

        deleteService.deleteJoueur(id, associationId, organisationDTO);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .body(true);
    }

    /**
     * GET  /_search/organisations/:organisationId/joueurs : search all the joueurs.
     *
     * @param organisationId //
     * @param associationId //
     * @param pageable the pagination information
     * @return the CustomResponse with status 200 (OK) and the list of joueurs in body
     */
    @GetMapping("/_search/organisations/{organisationId}/joueurs")
    public ResponseEntity<List<JoueurDTO>> searchJoueurs(
        @RequestParam String query,
        @PathVariable Long organisationId,
        @RequestParam(required = false) Long associationId,
        @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Joueurs");

        // get organisation
        OrganisationDTO organisationDTO = this.communService.checkOrganisation(organisationId);

        Page<JoueurDTO> page = joueurService.search(organisationDTO, associationId, query, pageable);

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
}
