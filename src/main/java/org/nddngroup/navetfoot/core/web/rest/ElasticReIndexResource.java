package org.nddngroup.navetfoot.core.web.rest;

import org.nddngroup.navetfoot.core.service.ElasticReIndexService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ElasticReIndexResource {
    private final ElasticReIndexService elasticReIndexService;

    public ElasticReIndexResource(ElasticReIndexService elasticReIndexService) {
        this.elasticReIndexService = elasticReIndexService;
    }

    /**
     * {@code GET  /reindex} : reindex all.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)}.
     */
    @GetMapping("/reindex")
    public ResponseEntity<String> reIndex() {
        elasticReIndexService.reIndexAll();
        return ResponseEntity.ok("En cours !!!");
    }
}
