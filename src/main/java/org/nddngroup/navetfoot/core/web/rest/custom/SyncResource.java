package org.nddngroup.navetfoot.core.web.rest.custom;

import org.nddngroup.navetfoot.core.service.custom.SyncCheck;
import org.nddngroup.navetfoot.core.domain.custom.stats.*;
import org.nddngroup.navetfoot.core.domain.custom.sync.*;
import org.nddngroup.navetfoot.core.exception.ApiRequestException;
import org.nddngroup.navetfoot.core.exception.ExceptionCode;
import org.nddngroup.navetfoot.core.exception.ExceptionLevel;
import org.nddngroup.navetfoot.core.service.*;
import org.nddngroup.navetfoot.core.service.custom.NotificationService;
import org.nddngroup.navetfoot.core.service.dto.*;
import org.nddngroup.navetfoot.core.service.websocket.WebSocketApiClientService;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.LongFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class SyncResource {
    private final Logger log = LoggerFactory.getLogger(CustomMatchResource.class);

    @Autowired
    private MatchService matchService;
    @Autowired
    private FeuilleDeMatchService feuilleDeMatchService;
    @Autowired
    private DetailFeuilleDeMatchService detailFeuilleDeMatchService;
    @Autowired
    private JoueurService joueurService;
    @Autowired
    private OrganisationService organisationService;
    @Autowired
    private AssociationService associationService;
    @Autowired
    private AffiliationService affiliationService;
    @Autowired
    private ButService butService;
    @Autowired
    private CartonService cartonService;
    @Autowired
    private PenaltyService penaltyService;
    @Autowired
    private CoupFrancService coupFrancService;
    @Autowired
    private CornerService cornerService;
    @Autowired
    private RemplacementService remplacementService;
    @Autowired
    private TirService tirService;
    @Autowired
    private SaisonService saisonService;
    @Autowired
    private CompetitionService competitionService;
    @Autowired
    private CompetitionSaisonService competitionSaisonService;
    @Autowired
    private PouleService pouleService;
    @Autowired
    private NotificationService notificationService;
    @Autowired
    private HorsJeuService horsJeuService;
    @Autowired
    private EtapeCompetitionService etapeCompetitionService;
    @Autowired
    private DetailPouleService detailPouleService;
    @Autowired
    private ContratService contratService;
    @Autowired
    private TirAuButService tirAuButService;
    @Autowired
    private ParticipationService participationService;
    @Autowired
    private WebSocketApiClientService webSocketApiClientService;

    @PostMapping("/mobile/sync/feuilles-de-match")
    public ResponseEntity<List<FeuilleDeMatchDTO>> syncFeuilleDeMatchs(@RequestBody List<CustomFeuilleDeMatch> customFeuilleDeMatchs) {
        List<FeuilleDeMatchDTO> feuilleDeMatchToReturn = new ArrayList<>();

        if(customFeuilleDeMatchs == null || customFeuilleDeMatchs.isEmpty()) {
            this.log.info("Maj feuilleDeMatch: Aucune donnée à sauvegarder");
        }
        else {
            for (CustomFeuilleDeMatch customFeuilleDeMatch : customFeuilleDeMatchs) {
                if (customFeuilleDeMatch == null) {
                    this.log.info("Maj feuilleDeMatch: feuilleDeMatch n'existe pas");
                } else {
                    // Vérifier si c'est déja synchronisé
                    Optional<FeuilleDeMatchDTO> sync = feuilleDeMatchService.findByHashcode(customFeuilleDeMatch.getFeuilleDeMatchDTO().getHashcode());

                    if (sync.isPresent()) {
                        feuilleDeMatchToReturn.add(sync.get());
                        continue;
                    }

                    String matchHashcode = customFeuilleDeMatch.getMatchHashcode();
                    FeuilleDeMatchDTO feuilleDeMatchDTO = customFeuilleDeMatch.getFeuilleDeMatchDTO();

                    //get match by id
                    if (feuilleDeMatchDTO.getMatchId() != null) {
                        MatchDTO matchDTO = this.matchService.findOne(feuilleDeMatchDTO.getMatchId()).orElseGet(() -> null);

                        if (matchDTO != null) {
                            feuilleDeMatchDTO.setMatchId(matchDTO.getId());
                        } else {
                            feuilleDeMatchDTO.setMatchId(null);
                        }
                    }

                    //get match by hashcode
                    if (feuilleDeMatchDTO.getMatchId() == null) {
                        this.matchService.findByHashcode(matchHashcode)
                            .ifPresent(matchDTO -> feuilleDeMatchDTO.setMatchId(matchDTO.getId()));
                    }

                    if (feuilleDeMatchDTO.getMatchId() == null) {
                        this.log.info("Maj feuilleDeMatch: match n'existe pas");
                    } else {
                        if (feuilleDeMatchDTO.getCreatedAt() == null) {
                            feuilleDeMatchDTO.setCreatedAt(ZonedDateTime.now());
                        }
                        feuilleDeMatchDTO.setUpdatedAt(ZonedDateTime.now());

                        FeuilleDeMatchDTO result = this.feuilleDeMatchService.save(feuilleDeMatchDTO);
                        feuilleDeMatchToReturn.add(result);
                    }

                }
            };
        }

        return new ResponseEntity<>(feuilleDeMatchToReturn, HttpStatus.OK);
    }

    @PostMapping("/mobile/sync/details-feuille-de-match")
    public ResponseEntity<List<DetailFeuilleDeMatchDTO>> syncDetailFeuilleDeMatchs(@RequestBody List<CustomDetailFeuilleDeMatch> customDetailFeuilleDeMatchs) {
        List<DetailFeuilleDeMatchDTO> detailFeuilleDeMatchToReturn = new ArrayList<>();

        if(customDetailFeuilleDeMatchs == null || customDetailFeuilleDeMatchs.isEmpty()) {
            this.log.info("Maj detailFeuilleDeMatch: Aucune donnée à sauvegarder");
        }
        else {
            SyncCheck<DetailFeuilleDeMatchDTO, DetailFeuilleDeMatchService> syncCheck = new SyncCheck<>();

            for (CustomDetailFeuilleDeMatch detailFeuilleDeMatch : customDetailFeuilleDeMatchs) {
                if (detailFeuilleDeMatch == null) {
                    this.log.info("Maj detailFeuilleDeMatch: detailFeuilleDeMatch n'existe pas");
                } else {
                    // Vérifier si c'est déja synchronisé
                    DetailFeuilleDeMatchDTO detailFeuilleDeMatchDTO = syncCheck.alreadySynchronized(detailFeuilleDeMatch.getDetailFeuilleDeMatchDTO(), detailFeuilleDeMatchService);

                    if (detailFeuilleDeMatchDTO != null) {
                        String feuilleDeMatchHashcode = detailFeuilleDeMatch.getFeuilleDeMatchHashcode();
                        String joueurHashcode = detailFeuilleDeMatch.getJoueurHashcode();
                        FeuilleDeMatchDTO feuilleDeMatchDTO = null;
                        JoueurDTO joueurDTO = null;

                        //get feuilleDeMatch by id
                        if (detailFeuilleDeMatchDTO.getFeuilleDeMatchId() != null) {
                            Optional<FeuilleDeMatchDTO> feuilleDeMatchDTOOptional = this.feuilleDeMatchService.findOne(detailFeuilleDeMatchDTO.getFeuilleDeMatchId());
                            if (feuilleDeMatchDTOOptional.isPresent() && !feuilleDeMatchDTOOptional.get().isDeleted()) {
                                feuilleDeMatchDTO = feuilleDeMatchDTOOptional.get();
                                detailFeuilleDeMatchDTO.setFeuilleDeMatchId(feuilleDeMatchDTO.getId());
                            } else {
                                detailFeuilleDeMatchDTO.setFeuilleDeMatchId(null);
                            }
                        }

                        //get feuilleDeMatch by hashcode
                        if (detailFeuilleDeMatchDTO.getFeuilleDeMatchId() == null) {
                            Optional<FeuilleDeMatchDTO> feuilleDeMatchDTOOptional = this.feuilleDeMatchService.findByHashcode(feuilleDeMatchHashcode);
                            if (feuilleDeMatchDTOOptional.isPresent()) {
                                feuilleDeMatchDTO = feuilleDeMatchDTOOptional.get();
                                detailFeuilleDeMatchDTO.setFeuilleDeMatchId(feuilleDeMatchDTO.getId());
                            }
                        }

                        //get joueur by id
                        if (detailFeuilleDeMatchDTO.getJoueurId() != null) {
                            Optional<JoueurDTO> joueurDTOOptional = this.joueurService.findOne(detailFeuilleDeMatchDTO.getJoueurId());

                            if (joueurDTOOptional.isPresent() && !joueurDTOOptional.get().isDeleted()) {
                                joueurDTO = joueurDTOOptional.get();
                                detailFeuilleDeMatchDTO.setJoueurId(joueurDTO.getId());
                            } else {
                                detailFeuilleDeMatchDTO.setJoueurId(null);
                            }
                        }

                        //get joueur by hashcode
                        if (detailFeuilleDeMatchDTO.getJoueurId() == null) {
                            Optional<JoueurDTO> joueurDTOOptional = this.joueurService.findByHashcode(joueurHashcode);
                            if (joueurDTOOptional.isPresent() && !joueurDTOOptional.get().isDeleted()) {
                                joueurDTO = joueurDTOOptional.get();
                                detailFeuilleDeMatchDTO.setJoueurId(joueurDTO.getId());
                            }
                        }

                        if (feuilleDeMatchDTO == null || joueurDTO == null) {
                            this.log.info("Maj detailFeuilleDeMatch: feuilleDeMatch ou joueur n'existe pas");
                        } else {
                            // find detailFeuille for joueur
                            detailFeuilleDeMatchService.findByFeuilleDeMatchAndJoueur(feuilleDeMatchDTO, joueurDTO)
                                .ifPresent(deMatchDTO -> detailFeuilleDeMatchDTO.setId(deMatchDTO.getId()));

                            if (detailFeuilleDeMatchDTO.getCreatedAt() == null) {
                                detailFeuilleDeMatchDTO.setCreatedAt(ZonedDateTime.now());
                            }
                            detailFeuilleDeMatchDTO.setUpdatedAt(ZonedDateTime.now());

                            DetailFeuilleDeMatchDTO result = this.detailFeuilleDeMatchService.save(detailFeuilleDeMatchDTO);
                            detailFeuilleDeMatchToReturn.add(result);
                        }
                    }
                }
            }
        }

        return new ResponseEntity<>(detailFeuilleDeMatchToReturn, HttpStatus.OK);
    }

    @PostMapping("/mobile/sync/affiliations")
    public ResponseEntity<List<AffiliationDTO>> syncAffiliations(@RequestBody List<CustomAffiliation> customAffiliations) {
        List<AffiliationDTO> affiliationsToReturn = new ArrayList<>();

        if(customAffiliations == null || customAffiliations.isEmpty()) {
            this.log.info("Maj affiliation: Aucune donnée à sauvegarder");
        }
        else {
            for (CustomAffiliation customAffiliation : customAffiliations) {
                if (customAffiliation == null) {
                    this.log.info("Maj affiliation: affiliation n'existe pas");
                } else {
                    // Vérifier si c'est déja synchronisé
                    Optional<AffiliationDTO> sync = affiliationService.findByHashcode(customAffiliation.getAffiliationDTO().getHashcode());

                    if (sync.isPresent()) {
                        affiliationsToReturn.add(sync.get());
                        continue;
                    }

                    String organisationHashcode = customAffiliation.getOrganisationHashcode();
                    String associationHashcode = customAffiliation.getAssociationHashcode();
                    AffiliationDTO affiliationDTO = customAffiliation.getAffiliationDTO();

                    //get organisation by id
                    if (affiliationDTO.getOrganisationId() != null) {
                        Optional<OrganisationDTO> organisationDTO = this.organisationService.findOne(affiliationDTO.getOrganisationId());

                        if (organisationDTO.isPresent() && !organisationDTO.get().isDeleted()) {
                            affiliationDTO.setOrganisationId(organisationDTO.get().getId());
                        } else {
                            affiliationDTO.setOrganisationId(null);
                        }
                    }

                    //get organisation by hashcode
                    if (affiliationDTO.getOrganisationId() == null) {
                        this.organisationService.findByHashcode(organisationHashcode)
                            .ifPresent(organisationDTO -> affiliationDTO.setOrganisationId(organisationDTO.getId()));
                    }

                    //get association by id
                    if (affiliationDTO.getAssociationId() != null) {
                        Optional<AssociationDTO> associationDTO = this.associationService.findOne(affiliationDTO.getAssociationId());

                        if (associationDTO.isPresent() && !associationDTO.get().isDeleted()) {
                            affiliationDTO.setAssociationId(associationDTO.get().getId());
                        } else {
                            affiliationDTO.setAssociationId(null);
                        }
                    }

                    //get association by hashcode
                    if (affiliationDTO.getAssociationId() == null) {
                        this.associationService.findByHashcode(associationHashcode)
                            .ifPresent(associationDTO -> affiliationDTO.setAssociationId(associationDTO.getId()));
                    }

                    if (affiliationDTO.getOrganisationId() == null || affiliationDTO.getAssociationId() == null) {
                        this.log.info("Maj affiliation: organisation ou association n'existe pas");
                    } else {
                        AffiliationDTO result = this.affiliationService.save(affiliationDTO);
                        affiliationsToReturn.add(result);
                    }

                }
            }
        }

        return new ResponseEntity<>(affiliationsToReturn, HttpStatus.OK);
    }

    @PostMapping("/mobile/sync/associations")
    public ResponseEntity<List<AssociationDTO>> syncAssociations(@RequestBody List<CustomAssociation> customAssociations) {
        List<AssociationDTO> associationsToReturn = new ArrayList<>();

        if(customAssociations == null || customAssociations.isEmpty()) {
            this.log.info("Maj association: Aucune donnée à sauvegarder");
        }
        else {
            for(CustomAssociation customAssociation : customAssociations) {
                if(customAssociation == null) {
                    this.log.info("Maj association: association n'existe pas");
                }
                else {
                    // Vérifier si c'est déja synchronisé
                    Optional<AssociationDTO> sync = associationService.findByHashcode(customAssociation.getAssociationDTO().getHashcode());

                    if (sync.isPresent()) {
                        associationsToReturn.add(sync.get());
                        continue;
                    }

                    AssociationDTO associationDTO = customAssociation.getAssociationDTO();

                    AssociationDTO result = this.associationService.save(associationDTO);
                    associationsToReturn.add(result);

                }
            }
        }

        return new ResponseEntity<>(associationsToReturn, HttpStatus.OK);
    }

    @PostMapping("/mobile/sync/competitions")
    public ResponseEntity<List<CompetitionDTO>> syncCompetitions(@RequestBody List<CustomCompetition> customCompetitions) {
        log.debug("REST request to save competitions : {}", customCompetitions);

        List<CompetitionDTO> competitionsToReturn = new ArrayList<>();

        if(customCompetitions != null && !customCompetitions.isEmpty()) {
            for (CustomCompetition customCompetition : customCompetitions) {
                if (customCompetition != null) {
                    // Vérifier si c'est déja synchronisé
                    Optional<CompetitionDTO> sync = competitionService.findByHashcode(customCompetition.getCompetitionDTO().getHashcode());

                    if (sync.isPresent()) {
                        competitionsToReturn.add(sync.get());
                    } else {

                        CompetitionDTO competitionDTO = customCompetition.getCompetitionDTO();

                        //get organisation by hashcode if not exists
                        if (competitionDTO.getOrganisationId() == null) {
                            String hashcode = customCompetition.getOrganisationHashcode();

                            this.organisationService.findByHashcode(hashcode)
                                .ifPresent(organisationDTO -> {
                                    if (!organisationDTO.isDeleted()) {
                                        competitionDTO.setOrganisationId(organisationDTO.getId());

                                        CompetitionDTO result = this.competitionService.save(competitionDTO);
                                        competitionsToReturn.add(result);
                                    }
                                });
                        } else {
                            this.organisationService.findOne(competitionDTO.getOrganisationId())
                                .ifPresent(organisationDTO -> {
                                    competitionDTO.setOrganisationId(organisationDTO.getId());
                                    CompetitionDTO result = this.competitionService.save(competitionDTO);
                                    competitionsToReturn.add(result);
                                });
                        }
                    }

                } else {
                    this.log.info("Maj competition: Cette competition est null");
                }
            }
        }
        else {
            this.log.info("Maj competition: Il n'y a pas de competitions à sauvegarder");
        }

        return new ResponseEntity<>(competitionsToReturn, HttpStatus.OK);
    }

    @PostMapping("/mobile/sync/competitionSaisons")
    public ResponseEntity<List<CompetitionSaisonDTO>> syncCompetititonSasions(@RequestBody List<CustomCompetitionSaison> customCompetitionSaisons) {
        List<CompetitionSaisonDTO> competitionSaisonsToReturn = new ArrayList<>();

        if(customCompetitionSaisons == null || customCompetitionSaisons.isEmpty()) {
            this.log.info("Maj competitionSaison: Aucune donnée à sauvegarder");
        }
        else {
            for (CustomCompetitionSaison customCompetitionSaison : customCompetitionSaisons) {
                if (customCompetitionSaison == null) {
                    this.log.info("Maj competitionSaison: CompetitionSaison n'existe pas");
                } else {
                    // Vérifier si c'est déja synchronisé
                    Optional<CompetitionSaisonDTO> sync = competitionSaisonService.findByHashcode(customCompetitionSaison.getCompetitionSaisonDTO().getHashcode());

                    if (sync.isPresent()) {
                        competitionSaisonsToReturn.add(sync.get());
                    } else {
                        String saisonHashcode = customCompetitionSaison.getSaisonHashcode();
                        String competitionHashcode = customCompetitionSaison.getCompetitionHashcode();
                        CompetitionSaisonDTO competitionSaisonDTO = customCompetitionSaison.getCompetitionSaisonDTO();

                        //get competition by id
                        if (competitionSaisonDTO.getCompetitionId() != null) {
                            Optional<CompetitionDTO> competitionDTOOptional = this.competitionService.findOne(competitionSaisonDTO.getCompetitionId());

                            if (competitionDTOOptional.isPresent() && !competitionDTOOptional.get().isDeleted()) {
                                competitionSaisonDTO.setCompetitionId(competitionDTOOptional.get().getId());
                            } else {
                                competitionSaisonDTO.setCompetitionId(null);
                            }
                        }

                        //get competition by hashcode
                        if (competitionSaisonDTO.getCompetitionId() == null) {
                            //get competition by hashcode
                            this.competitionService.findByHashcode(competitionHashcode)
                                .ifPresent(competitionDTO -> competitionSaisonDTO.setCompetitionId(competitionDTO.getId()));
                        }

                        //get saison by id
                        if (competitionSaisonDTO.getSaisonId() != null) {
                            Optional<SaisonDTO> saisonDTOOptional = this.saisonService.findOne(competitionSaisonDTO.getSaisonId());
                            if (saisonDTOOptional.isPresent() && !saisonDTOOptional.get().isDeleted()) {
                                competitionSaisonDTO.setSaisonId(saisonDTOOptional.get().getId());
                            } else {
                                competitionSaisonDTO.setSaisonId(null);
                            }

                        }

                        //get saison by hashcode
                        if (competitionSaisonDTO.getSaisonId() == null) {
                            //get saison by hashcode
                            this.saisonService.findByHashcode(saisonHashcode)
                                .ifPresent(saisonDTO -> competitionSaisonDTO.setSaisonId(saisonDTO.getId()));
                        }

                        if (competitionSaisonDTO.getCompetitionId() == null || competitionSaisonDTO.getSaisonId() == null) {
                            this.log.info("Maj competitionSaison: competiton ou saison n'existe pas");
                        } else {
                            CompetitionSaisonDTO result = this.competitionSaisonService.save(competitionSaisonDTO);
                            competitionSaisonsToReturn.add(result);
                        }
                    }

                }
            }
        }

        return new ResponseEntity<>(competitionSaisonsToReturn, HttpStatus.OK);
    }

    @PostMapping("/mobile/sync/contrats")
    public ResponseEntity<List<ContratDTO>> syncContrats(@RequestBody List<CustomContrat> customContrats) {
        List<ContratDTO> contratsToReturn = new ArrayList<>();

        if(customContrats == null || customContrats.isEmpty()) {
            this.log.info("Maj contrat: Aucune donnée à sauvegarder");
        }
        else {
            for (CustomContrat customContrat : customContrats) {
                if (customContrat == null) {
                    this.log.info("Maj contrat: contrat n'existe pas");
                } else {
                    // Vérifier si c'est déja synchronisé
                    Optional<ContratDTO> sync = contratService.findByHashcode(customContrat.getContratDTO().getHashcode());

                    if (sync.isPresent()) {
                        contratsToReturn.add(sync.get());
                    } else {
                        String saisonHashcode = customContrat.getSaisonHashcode();
                        String associationHashcode = customContrat.getAssociationHashcode();
                        String joueurHashcode = customContrat.getJoueurHashcode();
                        ContratDTO contratDTO = customContrat.getContratDTO();

                        //get saison by id
                        if (contratDTO.getSaisonId() != null) {
                            Optional<SaisonDTO> saisonDTO = this.saisonService.findOne(contratDTO.getSaisonId());

                            if (saisonDTO.isPresent() && !saisonDTO.get().isDeleted()) {
                                contratDTO.setSaisonId(saisonDTO.get().getId());
                            } else {
                                contratDTO.setSaisonId(null);
                            }
                        }

                        //get saison by hashcode
                        if (contratDTO.getSaisonId() == null) {
                            this.saisonService.findByHashcode(saisonHashcode)
                                .ifPresent(saisonDTO -> contratDTO.setSaisonId(saisonDTO.getId()));
                        }

                        //get association by id
                        if (contratDTO.getAssociationId() != null) {
                            Optional<AssociationDTO> associationDTO = this.associationService.findOne(contratDTO.getAssociationId());

                            if (associationDTO.isPresent() && !associationDTO.get().isDeleted()) {
                                contratDTO.setAssociationId(associationDTO.get().getId());
                            } else {
                                contratDTO.setAssociationId(null);
                            }
                        }

                        //get association by hashcode
                        if (contratDTO.getAssociationId() == null) {
                            this.associationService.findByHashcode(associationHashcode)
                                .ifPresent(associationDTO -> contratDTO.setAssociationId(associationDTO.getId()));
                        }

                        //get joueur by id
                        if (contratDTO.getJoueurId() != null) {
                            Optional<JoueurDTO> joueurDTO = this.joueurService.findOne(contratDTO.getJoueurId());

                            if (joueurDTO.isPresent() && !joueurDTO.get().isDeleted()) {
                                contratDTO.setJoueurId(joueurDTO.get().getId());
                            } else {
                                contratDTO.setJoueurId(null);
                            }
                        }

                        //get joueur by hashcode
                        if (contratDTO.getJoueurId() == null) {
                            this.joueurService.findByHashcode(joueurHashcode)
                                .ifPresent(joueurDTO -> contratDTO.setJoueurId(joueurDTO.getId()));
                        }

                        if (contratDTO.getSaisonId() == null || contratDTO.getAssociationId() == null || contratDTO.getJoueurId() == null) {
                            this.log.info("Maj contrat: saison ou association ou joueur n'existe pas");
                        } else {
                            ContratDTO result = this.contratService.save(contratDTO);
                            contratsToReturn.add(result);
                        }
                    }

                }
            }
        }

        return new ResponseEntity<>(contratsToReturn, HttpStatus.OK);
    }

    @PostMapping("/mobile/sync/detailPoules")
    public ResponseEntity<List<DetailPouleDTO>> syncDetailPoules(@RequestBody List<CustomDetailPoule> customDetailPoules) {
        List<DetailPouleDTO> detailPouleToReturn = new ArrayList<>();

        if(customDetailPoules == null || customDetailPoules.isEmpty()) {
            this.log.info("Maj detailPoule: Aucune donnée à sauvegarder");
        }
        else {
            customDetailPoules.forEach(customDetailPoule -> {
                if (customDetailPoule == null) {
                    this.log.info("Maj detailPoule: detailPoule n'existe pas");
                } else {
                    // Vérifier si c'est déja synchronisé
                    Optional<DetailPouleDTO> sync = detailPouleService.findByHashcode(customDetailPoule.getDetailPouleDTO().getHashcode());

                    if (sync.isPresent()) {
                        detailPouleToReturn.add(sync.get());
                    } else {
                        String pouleHashcode = customDetailPoule.getPouleHashcode();
                        String associationHashcode = customDetailPoule.getAssociationHashcode();
                        DetailPouleDTO detailPouleDTO = customDetailPoule.getDetailPouleDTO();

                        //get poule by id
                        if (detailPouleDTO.getPouleId() != null) {
                            Optional<PouleDTO> pouleDTO = this.pouleService.findOne(detailPouleDTO.getPouleId());

                            if (pouleDTO.isPresent() && !pouleDTO.get().isDeleted()) {
                                detailPouleDTO.setPouleId(pouleDTO.get().getId());
                            } else {
                                detailPouleDTO.setPouleId(null);
                            }
                        }

                        //get poule by hashcode
                        if (detailPouleDTO.getPouleId() == null) {
                            this.pouleService.findByHashcode(pouleHashcode)
                                .ifPresent(pouleDTO -> detailPouleDTO.setPouleId(pouleDTO.getId()));
                        }

                        //get association by id
                        if (detailPouleDTO.getAssociationId() != null) {
                            Optional<AssociationDTO> associationDTO = this.associationService.findOne(detailPouleDTO.getAssociationId());

                            if (associationDTO.isPresent() && !associationDTO.get().isDeleted()) {
                                detailPouleDTO.setAssociationId(associationDTO.get().getId());
                            } else {
                                detailPouleDTO.setAssociationId(null);
                            }
                        }

                        //get association by hashcode
                        if (detailPouleDTO.getAssociationId() == null) {
                            this.associationService.findByHashcode(associationHashcode)
                                .ifPresent(associationDTO -> detailPouleDTO.setAssociationId(associationDTO.getId()));
                        }

                        if (detailPouleDTO.getPouleId() == null || detailPouleDTO.getAssociationId() == null) {
                            this.log.info("Maj detailPoule: poule ou association n'existe pas");
                        } else {
                            DetailPouleDTO result = this.detailPouleService.save(detailPouleDTO);
                            detailPouleToReturn.add(result);
                        }
                    }

                }
            });
        }

        return new ResponseEntity<>(detailPouleToReturn, HttpStatus.OK);
    }

    @PostMapping("/mobile/sync/etapeCompetitons")
    public ResponseEntity<List<EtapeCompetitionDTO>> syncEtapeCompetitions(@RequestBody List<CustomEtapeCompetition> customEtapeCompetitions) {
        List<EtapeCompetitionDTO> etapeCompetitionToReturn = new ArrayList<>();

        if(customEtapeCompetitions == null || customEtapeCompetitions.isEmpty()) {
            this.log.info("Maj etapeCompetition: Aucune donnée à sauvegarder");
        }
        else {
            for (CustomEtapeCompetition customEtapeCompetition : customEtapeCompetitions) {
                if (customEtapeCompetition == null) {
                    this.log.info("Maj etapeCompetition: etapeCompetition n'existe pas");
                } else {
                    // Vérifier si c'est déja synchronisé
                    Optional<EtapeCompetitionDTO> sync = etapeCompetitionService.findByHashcode(customEtapeCompetition.getEtapeCompetitionDTO().getHashcode());

                    if (sync.isPresent()) {
                        etapeCompetitionToReturn.add(sync.get());
                    } else {
                        String competitionSaisonHashcode = customEtapeCompetition.getCompetitionSaisonHashcode();
                        EtapeCompetitionDTO etapeCompetitionDTO = customEtapeCompetition.getEtapeCompetitionDTO();

                        //get competitionSaison by id
                        if (etapeCompetitionDTO.getCompetitionSaisonId() != null) {
                            Optional<CompetitionSaisonDTO> competitionSaisonDTO = this.competitionSaisonService.findOne(etapeCompetitionDTO.getCompetitionSaisonId());

                            if (competitionSaisonDTO.isPresent() && !competitionSaisonDTO.get().isDeleted()) {
                                etapeCompetitionDTO.setCompetitionSaisonId(competitionSaisonDTO.get().getId());
                            } else {
                                etapeCompetitionDTO.setCompetitionSaisonId(null);
                            }
                        }

                        //get competitionSaison by hashcode
                        if (etapeCompetitionDTO.getCompetitionSaisonId() == null) {
                            this.competitionSaisonService.findByHashcode(competitionSaisonHashcode)
                                .ifPresent(competitionSaisonDTO -> etapeCompetitionDTO.setCompetitionSaisonId(competitionSaisonDTO.getId()));
                        }

                        if (etapeCompetitionDTO.getCompetitionSaisonId() == null) {
                            this.log.info("Maj etapeCompetition: competitonSaison n'existe pas");
                        } else {
                            EtapeCompetitionDTO result = this.etapeCompetitionService.save(etapeCompetitionDTO);
                            etapeCompetitionToReturn.add(result);
                        }
                    }

                }
            }
        }

        return new ResponseEntity<>(etapeCompetitionToReturn, HttpStatus.OK);
    }

    @GetMapping("/mobile/sync/events")
    public ResponseEntity<EventsDetail> getEventsDetails(@RequestParam Long matchId) {

        EventsDetail eventsDetails = new EventsDetail();

        // find match by id
        MatchDTO matchDTO = matchService.findOne(matchId)
            .orElseThrow(() -> new ApiRequestException(
                ExceptionCode.MATCH_NOT_FOUND.getMessage(),
                ExceptionCode.MATCH_NOT_FOUND.getValue(),
                ExceptionLevel.ERROR
            ));


        LongFilter longFilter        = new LongFilter();
        BooleanFilter deletedFilter  = new BooleanFilter();

        longFilter.setEquals(matchId);
        deletedFilter.setEquals(false);

        //get all buts
        List<ButDTO> butDTOS = this.butService.findByMatch(matchDTO);
        List<ButDetail> butDetails = new ArrayList<>();
        if(butDTOS != null) {
            for(ButDTO butDTO: butDTOS) {
                ButDetail butDetail = new ButDetail();
                //get joueur
                JoueurDTO joueurDTO = this.joueurService.findOne(butDTO.getJoueurId())
                    .orElseThrow(() -> new ApiRequestException(
                        ExceptionCode.JOUEUR_NOT_FOUND.getMessage(),
                        ExceptionCode.JOUEUR_NOT_FOUND.getValue(),
                        ExceptionLevel.ERROR
                    ));

                //set butDetail
                butDetail.setButDTO(butDTO);
                butDetail.setPrenomJoueur(joueurDTO.getPrenom());
                butDetail.setNomJoueur(joueurDTO.getNom());
                butDetail.setJoueurHashcode(joueurDTO.getHashcode());

                //add butDetail to list
                butDetails.add(butDetail);
            }
        }

        //get all cartons
        List<CartonDTO> cartonDTOS = this.cartonService.findByMatch(matchDTO);
        List<CartonDetail> cartonDetails = new ArrayList<>();
        if(cartonDTOS != null) {
            for(CartonDTO cartonDTO : cartonDTOS) {
                CartonDetail cartonDetail = new CartonDetail();
                //get joueur
                JoueurDTO joueurDTO = this.joueurService.findOne(cartonDTO.getJoueurId())
                    .orElseThrow(() -> new ApiRequestException(
                        ExceptionCode.JOUEUR_NOT_FOUND.getMessage(),
                        ExceptionCode.JOUEUR_NOT_FOUND.getValue(),
                        ExceptionLevel.ERROR
                    ));

                //set cartonDetail
                cartonDetail.setCartonDTO(cartonDTO);
                cartonDetail.setPrenomJoueur(joueurDTO.getPrenom());
                cartonDetail.setNomJoueur(joueurDTO.getNom());
                cartonDetail.setJoueurHashcode(joueurDTO.getHashcode());

                //add cartonDetail to list
                cartonDetails.add(cartonDetail);
            }
        }

        //get all tirs
        List<TirDTO> tirDTOS = this.tirService.findByMatch(matchDTO);
        List<TirDetail> tirDetails = new ArrayList<>();
        if(tirDTOS != null) {
            for(TirDTO tirDTO : tirDTOS) {
                TirDetail tirDetail = new TirDetail();
                //get joueur
                JoueurDTO joueurDTO = this.joueurService.findOne(tirDTO.getJoueurId())
                    .orElseThrow(() -> new ApiRequestException(
                        ExceptionCode.JOUEUR_NOT_FOUND.getMessage(),
                        ExceptionCode.JOUEUR_NOT_FOUND.getValue(),
                        ExceptionLevel.ERROR
                    ));

                //set tirDetail
                tirDetail.setTirDTO(tirDTO);
                tirDetail.setPrenomJoueur(joueurDTO.getPrenom());
                tirDetail.setNomJoueur(joueurDTO.getNom());
                tirDetail.setJoueurHashcode(joueurDTO.getHashcode());

                //add tirDetail to list
                tirDetails.add(tirDetail);
            }
        }

        //get all coupFrancs
        List<CoupFrancDTO> coupFrancDTOS = this.coupFrancService.findByMatch(matchDTO);
        List<CoupFrancDetail> coupFrancDetails = new ArrayList<>();
        if(coupFrancDTOS != null) {
            for(CoupFrancDTO coupFrancDTO: coupFrancDTOS) {
                CoupFrancDetail coupFrancDetail = new CoupFrancDetail();

                coupFrancDetail.setCoupFrancDTO(coupFrancDTO);

                coupFrancDetails.add(coupFrancDetail);
            }

        }

        //get all corners
        List<CornerDTO> cornerDTOS = this.cornerService.findByMatch(matchDTO);
        List<CornerDetail> cornerDetails = new ArrayList<>();
        if(cornerDTOS != null) {
            for(CornerDTO cornerDTO : cornerDTOS) {
                CornerDetail cornerDetail = new CornerDetail();

                cornerDetail.setCornerDTO(cornerDTO);

                cornerDetails.add(cornerDetail);
            }

        }

        //get all 'horsJeus'
        List<HorsJeuDTO> horsJeuDTOS = this.horsJeuService.findByMatch(matchDTO);
        List<HorsJeuDetail> horsJeuDetails = new ArrayList<>();
        if(horsJeuDTOS != null) {
            for(HorsJeuDTO horsJeuDTO : horsJeuDTOS) {
                HorsJeuDetail horsJeuDetail = new HorsJeuDetail();

                horsJeuDetail.setHorsJeuDTO(horsJeuDTO);

                horsJeuDetails.add(horsJeuDetail);
            }

        }

        //get all penalties
        List<PenaltyDTO> penaltyDTOS = this.penaltyService.findByMatch(matchDTO);
        List<PenaltyDetail> penaltyDetails = new ArrayList<>();
        if(penaltyDTOS != null) {
            for(PenaltyDTO penaltyDTO : penaltyDTOS) {
                PenaltyDetail penaltyDetail = new PenaltyDetail();
                //get joueur
                JoueurDTO joueurDTO = this.joueurService.findOne(penaltyDTO.getJoueurId())
                    .orElseThrow(() -> new ApiRequestException(
                        ExceptionCode.JOUEUR_NOT_FOUND.getMessage(),
                        ExceptionCode.JOUEUR_NOT_FOUND.getValue(),
                        ExceptionLevel.ERROR
                    ));

                //set penaltyDetail
                penaltyDetail.setPenaltyDTO(penaltyDTO);
                penaltyDetail.setPrenomJoueur(joueurDTO.getPrenom());
                penaltyDetail.setNomJoueur(joueurDTO.getNom());
                penaltyDetail.setJoueurHashcode(joueurDTO.getHashcode());

                //add penaltyDetail to list
                penaltyDetails.add(penaltyDetail);
            }
        }

        //get all remplacements
        List<RemplacementDTO> remplacementDTOS = this.remplacementService.findByMatch(matchDTO);
        List<RemplacementDetail> remplacementDetails = new ArrayList<>();
        if(remplacementDTOS != null) {
            for(RemplacementDTO remplacementDTO : remplacementDTOS) {
                RemplacementDetail remplacementDetail = new RemplacementDetail();
                //get entrant
                JoueurDTO entrant = this.joueurService.findOne(remplacementDTO.getEntrantId())
                    .orElseThrow(() -> new ApiRequestException(
                        ExceptionCode.JOUEUR_NOT_FOUND.getMessage(),
                        ExceptionCode.JOUEUR_NOT_FOUND.getValue(),
                        ExceptionLevel.ERROR
                    ));
                //get sortant
                JoueurDTO sortant = this.joueurService.findOne(remplacementDTO.getSortantId())
                    .orElseThrow(() -> new ApiRequestException(
                        ExceptionCode.JOUEUR_NOT_FOUND.getMessage(),
                        ExceptionCode.JOUEUR_NOT_FOUND.getValue(),
                        ExceptionLevel.ERROR
                    ));

                //set remplacementDetail
                remplacementDetail.setRemplacementDTO(remplacementDTO);
                remplacementDetail.setPrenomEntrant(entrant.getPrenom());
                remplacementDetail.setNomEntrant(entrant.getNom());
                remplacementDetail.setEntrantHashcode(entrant.getHashcode());
                remplacementDetail.setPrenomSortant(sortant.getPrenom());
                remplacementDetail.setNomSortant(sortant.getNom());
                remplacementDetail.setSortantHashcode(sortant.getHashcode());

                //add remplacementDetail to list
                remplacementDetails.add(remplacementDetail);
            }
        }

        //set all events to list to return
        eventsDetails.setButsDetail(butDetails);
        eventsDetails.setCartonsDetail(cartonDetails);
        eventsDetails.setCornersDetail(cornerDetails);
        eventsDetails.setCoupFrancsDetail(coupFrancDetails);
        eventsDetails.setPenaltiesDetail(penaltyDetails);
        eventsDetails.setRemplacementsDetail(remplacementDetails);
        eventsDetails.setHorsJeusDetail(horsJeuDetails);
        eventsDetails.setTirsDetail(tirDetails);

        return new ResponseEntity<>(eventsDetails, HttpStatus.OK);
    }

    @PostMapping("/mobile/sync/joueurs")
    public ResponseEntity<List<JoueurDTO>> syncJoueurs(@RequestBody List<CustomJoueur> customJoueurs) {
        List<JoueurDTO> joueurssToReturn = new ArrayList<>();

        if(customJoueurs == null || customJoueurs.isEmpty()) {
            this.log.info("Maj joueur: Aucune donnée à sauvegarder");
        }
        else {
            for (CustomJoueur customJoueur : customJoueurs) {
                if (customJoueur == null) {
                    this.log.info("Maj joueur: joueur n'existe pas");
                } else {
                    // Vérifier si c'est déja synchronisé
                    Optional<JoueurDTO> sync = joueurService.findByHashcode(customJoueur.getJoueurDTO().getHashcode());

                    if (sync.isPresent()) {
                        joueurssToReturn.add(sync.get());
                    } else {
                        JoueurDTO joueurDTO = customJoueur.getJoueurDTO();

                        /// save contrat
                        String contratHashcode = customJoueur.getContratHashcode();
                        String contratCode = customJoueur.getContratCode();
                        Long saisonId = customJoueur.getSaisonId();
                        Long associationId = customJoueur.getAssociationId();

                        /// get saison by id
                        SaisonDTO saisonDTO = this.saisonService.findOne(saisonId).orElseGet(() -> null);
                        if (saisonDTO == null || saisonDTO.isDeleted()) {
                            this.log.info("Maj joueur: saison n'existe pas");
                        } else {
                            /// get association by id
                            AssociationDTO associationDTO = this.associationService.findOne(associationId).orElseGet(() -> null);
                            if (associationDTO == null || associationDTO.isDeleted()) {
                                this.log.info("Maj joueur: association n'existe pas");
                            } else {
                                ContratDTO contratDTO = new ContratDTO();
                                contratDTO.setCode(contratCode);
                                contratDTO.setHashcode(contratHashcode);
                                contratDTO.setAssociationId(associationId);
                                contratDTO.setSaisonId(saisonId);
                                contratDTO.setDeleted(false);
                                contratDTO.setCreatedAt(ZonedDateTime.now());
                                contratDTO.setUpdatedAt(ZonedDateTime.now());

                                if (joueurDTO.getCreatedAt() == null) {
                                    joueurDTO.setCreatedAt(ZonedDateTime.now());
                                }
                                joueurDTO.setUpdatedAt(ZonedDateTime.now());

                                JoueurDTO result = this.joueurService.save(joueurDTO);
                                contratDTO.setJoueurId(result.getId());

                                contratService.save(contratDTO);

                                joueurssToReturn.add(result);
                            }
                        }
                    }

                }
            }
        }

        return new ResponseEntity<>(joueurssToReturn, HttpStatus.OK);
    }

    @PostMapping("/mobile/sync/matchs")
    public ResponseEntity<List<MatchDTO>> syncMatchs(@RequestBody List<CustomMatch> customMatchs) {
        List<MatchDTO> matchToReturn = new ArrayList<>();

        if(customMatchs == null || customMatchs.isEmpty()) {
            this.log.info("Maj match: Aucune donnée à sauvegarder");
        }
        else {
            for (CustomMatch customMatch : customMatchs) {
                if (customMatch == null) {
                    this.log.info("Maj match: match n'existe pas");
                } else {
                    // Vérifier si c'est déja synchronisé
                    Optional<MatchDTO> sync = matchService.findByHashcode(customMatch.getMatchDTO().getHashcode());

                    if (sync.isPresent()) {
                        matchToReturn.add(sync.get());
                    } else {
                        String etapeCompetitionHashcode = customMatch.getEtapeCompetitionHashcode();
                        String localHashcode = customMatch.getLocalHashcode();
                        String visiteurHashcode = customMatch.getVisiteurHashcode();
                        String tirAuButHashcode = customMatch.getTirAuButHashcode();
                        MatchDTO matchDTO = customMatch.getMatchDTO();

                        //get etapeCompetition by id
                        if (matchDTO.getEtapeCompetitionId() != null) {
                            Optional<EtapeCompetitionDTO> etapeCompetitionDTO = this.etapeCompetitionService.findOne(matchDTO.getEtapeCompetitionId());

                            if (etapeCompetitionDTO.isPresent() && !etapeCompetitionDTO.get().isDeleted()) {
                                matchDTO.setEtapeCompetitionId(etapeCompetitionDTO.get().getId());
                            } else {
                                matchDTO.setEtapeCompetitionId(null);
                            }
                        }

                        //get etapeCompetition by hashcode
                        if (matchDTO.getEtapeCompetitionId() == null) {
                            this.etapeCompetitionService.findByHashcode(etapeCompetitionHashcode)
                                .ifPresent(etapeCompetitionDTO -> matchDTO.setEtapeCompetitionId(etapeCompetitionDTO.getId()));
                        }

                        //get local by id
                        if (matchDTO.getLocalId() != null) {
                            Optional<AssociationDTO> associationDTO = this.associationService.findOne(matchDTO.getLocalId());

                            if (associationDTO.isPresent() && !associationDTO.get().isDeleted()) {
                                matchDTO.setLocalId(associationDTO.get().getId());
                            } else {
                                matchDTO.setLocalId(null);
                            }
                        }

                        //get local by hashcode
                        if (matchDTO.getLocalId() == null) {
                            this.associationService.findByHashcode(localHashcode)
                                .ifPresent(associationDTO -> matchDTO.setLocalId(associationDTO.getId()));
                        }

                        //get visiteur by id
                        if (matchDTO.getVisiteurId() != null) {
                            Optional<AssociationDTO> associationDTO = this.associationService.findOne(matchDTO.getVisiteurId());

                            if (associationDTO.isPresent() && !associationDTO.get().isDeleted()) {
                                matchDTO.setVisiteurId(associationDTO.get().getId());
                            } else {
                                matchDTO.setVisiteurId(null);
                            }
                        }

                        //get local by hashcode
                        if (matchDTO.getVisiteurId() == null) {
                            this.associationService.findByHashcode(visiteurHashcode)
                                .ifPresent(associationDTO -> matchDTO.setVisiteurId(associationDTO.getId()));
                        }

                        if (matchDTO.getEtapeCompetitionId() == null || matchDTO.getLocalId() == null || matchDTO.getVisiteurId() == null) {
                            this.log.info("Maj match: etapeCompetition ou local ou visiteur n'existe pas");
                        } else {
                            MatchDTO result = this.matchService.save(matchDTO);
                            matchToReturn.add(result);
                        }
                    }

                }
            }
        }

        return new ResponseEntity<>(matchToReturn, HttpStatus.OK);
    }

    @PostMapping("/mobile/sync/participations")
    public ResponseEntity<List<ParticipationDTO>> syncParticipations(@RequestBody List<CustomParticipation> customParticipations) {
        List<ParticipationDTO> participationToReturn = new ArrayList<>();

        if(customParticipations == null || customParticipations.isEmpty()) {
            this.log.info("Maj participation: Aucune donnée à sauvegarder");
        }
        else {
            for (CustomParticipation customParticipation : customParticipations) {
                if (customParticipation == null) {
                    this.log.info("Maj participation: participation n'existe pas");
                } else {
                    // Vérifier si c'est déja synchronisé
                    Optional<ParticipationDTO> sync = participationService.findByHashcode(customParticipation.getParticipationDTO().getHashcode());

                    if (sync.isPresent()) {
                        participationToReturn.add(sync.get());
                    } else {
                        String etapeCompetitionHashcode = customParticipation.getEtapeCompetitionHashcode();
                        String associationHashcode = customParticipation.getAssociationHashcode();
                        String saisonHashcode = customParticipation.getSaisonHashcode();
                        ParticipationDTO participationDTO = customParticipation.getParticipationDTO();

                        //get etapeCompetition by id
                        if (participationDTO.getEtapeCompetitionId() != null) {
                            Optional<EtapeCompetitionDTO> etapeCompetitionDTO = this.etapeCompetitionService.findOne(participationDTO.getEtapeCompetitionId());

                            if (etapeCompetitionDTO.isPresent() && !etapeCompetitionDTO.get().isDeleted()) {
                                participationDTO.setEtapeCompetitionId(etapeCompetitionDTO.get().getId());
                            } else {
                                participationDTO.setEtapeCompetitionId(null);
                            }
                        }

                        //get etapeCompetition by hashcode
                        if (participationDTO.getEtapeCompetitionId() == null) {
                            this.etapeCompetitionService.findByHashcode(etapeCompetitionHashcode)
                                .ifPresent(etapeCompetitionDTO -> participationDTO.setEtapeCompetitionId(etapeCompetitionDTO.getId()));
                        }

                        //get association by id
                        if (participationDTO.getAssociationId() != null) {
                            Optional<AssociationDTO> associationDTO = this.associationService.findOne(participationDTO.getAssociationId());

                            if (associationDTO.isPresent() && !associationDTO.get().isDeleted()) {
                                participationDTO.setAssociationId(associationDTO.get().getId());
                            } else {
                                participationDTO.setAssociationId(null);
                            }
                        }

                        //get association by hashcode
                        if (participationDTO.getAssociationId() == null) {
                            this.associationService.findByHashcode(associationHashcode)
                                .ifPresent(associationDTO -> participationDTO.setAssociationId(associationDTO.getId()));
                        }

                        //get saison by id
                        if (participationDTO.getSaisonId() != null) {
                            Optional<SaisonDTO> saisonDTO = this.saisonService.findOne(participationDTO.getSaisonId());

                            if (saisonDTO.isPresent() && !saisonDTO.get().isDeleted()) {
                                participationDTO.setSaisonId(saisonDTO.get().getId());
                            } else {
                                participationDTO.setSaisonId(null);
                            }
                        }

                        //get saison by hashcode
                        if (participationDTO.getSaisonId() == null) {
                            this.saisonService.findByHashcode(saisonHashcode)
                                .ifPresent(saisonDTO -> participationDTO.setSaisonId(saisonDTO.getId()));
                        }

                        if (participationDTO.getEtapeCompetitionId() == null || participationDTO.getAssociationId() == null || participationDTO.getSaisonId() == null) {
                            this.log.info("Maj participation: etapeCompetition ou association n'existe pas");
                        } else {
                            ParticipationDTO result = this.participationService.save(participationDTO);
                            participationToReturn.add(result);
                        }
                    }

                }
            }
        }

        return new ResponseEntity<>(participationToReturn, HttpStatus.OK);
    }

    @PostMapping("/mobile/sync/poules")
    public ResponseEntity<List<PouleDTO>> syncPoules(@RequestBody List<CustomPoule> customPoules) {
        List<PouleDTO> pouleToReturn = new ArrayList<>();

        if(customPoules == null || customPoules.isEmpty()) {
            this.log.info("Maj poule: Aucune donnée à sauvegarder");
        }
        else {
            for (CustomPoule customPoule : customPoules) {
                if (customPoule == null) {
                    this.log.info("Maj poule: poule n'existe pas");
                } else {
                    // Vérifier si c'est déja synchronisé
                    Optional<PouleDTO> sync = pouleService.findByHashcode(customPoule.getPouleDTO().getHashcode());

                    if (sync.isPresent()) {
                        pouleToReturn.add(sync.get());
                    } else {
                        String etapeCompetitionHashcode = customPoule.getEtapeCompetitionHashcode();
                        PouleDTO pouleDTO = customPoule.getPouleDTO();

                        //get etapeCompetition by id
                        if (pouleDTO.getEtapeCompetitionId() != null) {
                            Optional<EtapeCompetitionDTO> etapeCompetitionDTO = this.etapeCompetitionService.findOne(pouleDTO.getEtapeCompetitionId());

                            if (etapeCompetitionDTO.isPresent() && !etapeCompetitionDTO.get().isDeleted()) {
                                pouleDTO.setEtapeCompetitionId(etapeCompetitionDTO.get().getId());
                            } else {
                                pouleDTO.setEtapeCompetitionId(null);
                            }
                        }

                        //get etapeCompetition by hashcode
                        if (pouleDTO.getEtapeCompetitionId() == null) {
                            this.etapeCompetitionService.findByHashcode(etapeCompetitionHashcode)
                                .ifPresent(etapeCompetitionDTO -> pouleDTO.setEtapeCompetitionId(etapeCompetitionDTO.getId()));
                        }

                        if (pouleDTO.getEtapeCompetitionId() == null) {
                            this.log.info("Maj poule: etapeCompetition n'existe pas");
                        } else {
                            PouleDTO result = this.pouleService.save(pouleDTO);
                            pouleToReturn.add(result);
                        }
                    }

                }
            }
        }

        return new ResponseEntity<>(pouleToReturn, HttpStatus.OK);
    }

    @PostMapping("/mobile/sync/saisons")
    public ResponseEntity<List<SaisonDTO>> syncSaisons(@RequestBody List<CustomSaison> customSaisons) {
        log.debug("REST request to save saisons : {}", customSaisons);

        List<SaisonDTO> saisonsToReturn = new ArrayList<>();

        if(customSaisons != null && !customSaisons.isEmpty()) {
            for (CustomSaison customSaison : customSaisons) {
                if (customSaison != null) {
                    // Vérifier si c'est déja synchronisé
                    Optional<SaisonDTO> sync = saisonService.findByHashcode(customSaison.getSaisonDTO().getHashcode());

                    if (sync.isPresent()) {
                        saisonsToReturn.add(sync.get());
                    } else {
                        SaisonDTO saisonDTO = customSaison.getSaisonDTO();

                        //get organisation by hashcode if not exists
                        if (saisonDTO.getOrganisationId() == null) {
                            String hashcode = customSaison.getOrganisationHashcode();

                            this.organisationService.findByHashcode(hashcode)
                                .ifPresent(organisationDTO -> {
                                    saisonDTO.setOrganisationId(organisationDTO.getId());

                                    SaisonDTO result = this.saisonService.save(saisonDTO);
                                    saisonsToReturn.add(result);
                                });
                        } else {
                            this.organisationService.findOne(saisonDTO.getOrganisationId())
                                .ifPresent(organisationDTO -> {
                                    saisonDTO.setOrganisationId(organisationDTO.getId());
                                    SaisonDTO result = this.saisonService.save(saisonDTO);
                                    saisonsToReturn.add(result);
                                });
                        }

                    }
                } else {
                    this.log.info("Maj saison: Cette saison est null");
                }

            }
        }
        else {
            this.log.info("Maj saison: Il n'y a pas de saisons à sauvegarder");
        }

        return new ResponseEntity<>(saisonsToReturn, HttpStatus.OK);
    }

    @PostMapping("/mobile/sync/tirs-au-but")
    public ResponseEntity<List<TirAuButDTO>> syncTirAuButs(@RequestBody List<CustomTirAuBut> customTirAuButs) {
        log.debug("REST request to save tirAuButs : {}", customTirAuButs);

        List<TirAuButDTO> tirAuButsToReturn = new ArrayList<>();

        if(customTirAuButs != null && !customTirAuButs.isEmpty()) {
            for (CustomTirAuBut customTirAuBut : customTirAuButs) {
                if (customTirAuBut != null) {
                    // Vérifier si c'est déja synchronisé
                    Optional<TirAuButDTO> sync = tirAuButService.findByHashcode(customTirAuBut.getTirAuButDTO().getHashcode());

                    if (sync.isPresent()) {
                        tirAuButsToReturn.add(sync.get());
                    } else {
                        TirAuButDTO tirAuButDTO = customTirAuBut.getTirAuButDTO();

                        if (tirAuButDTO.getCreatedAt() == null) {
                            tirAuButDTO.setCreatedAt(ZonedDateTime.now());
                        }
                        tirAuButDTO.setUpdatedAt(ZonedDateTime.now());

                        //get match by hashcode if not exists
                        if (tirAuButDTO.getMatchId() == null) {
                            String hashcode = customTirAuBut.getMatchHashcode();

                            this.matchService.findByHashcode(hashcode)
                                .ifPresent(matchDTO -> {
                                    tirAuButDTO.setMatchId(matchDTO.getId());

                                    TirAuButDTO result = this.tirAuButService.save(tirAuButDTO);
                                    tirAuButsToReturn.add(result);
                                });
                        } else {
                            this.matchService.findOne(tirAuButDTO.getMatchId())
                                .ifPresent(matchDTO -> {
                                    tirAuButDTO.setMatchId(matchDTO.getId());
                                    TirAuButDTO result = this.tirAuButService.save(tirAuButDTO);
                                    tirAuButsToReturn.add(result);
                                });
                        }
                    }

                } else {
                    this.log.info("Maj tirAuBut: Ce tirAuBut est null");
                }

            }
        }
        else {
            this.log.info("Maj tirAuBut: Il n'y a pas de tirAuBut à sauvegarder");
        }

        return new ResponseEntity<>(tirAuButsToReturn, HttpStatus.OK);
    }

    @PostMapping("/mobile/sync/buts")
    public ResponseEntity<List<ButDTO>> syncButs(@RequestBody List<CustomBut> customButs) {
        List<ButDTO> butToReturn = new ArrayList<>();

        if(customButs == null || customButs.isEmpty()) {
            this.log.info("Maj but: Aucune donnée à sauvegarder");
        }
        else {
            for (CustomBut customBut : customButs) {
                if (customBut == null) {
                    this.log.info("Maj but: but n'existe pas");
                } else {
                    // Vérifier si le but est déja synchronisé
                    Optional<ButDTO> syncBut = butService.findByHashcode(customBut.getButDTO().getHashcode());

                    if (syncBut.isPresent()) {
                        butToReturn.add(syncBut.get());
                    } else {
                        String matchHashcode = customBut.getMatchHashcode();
                        String joueurHashcode = customBut.getJoueurHashcode();
                        ButDTO butDTO = customBut.getButDTO();

                        //get match by id
                        if (butDTO.getMatchId() != null) {
                            Optional<MatchDTO> matchDTOOptional = this.matchService.findOne(butDTO.getMatchId());

                            if (matchDTOOptional.isPresent() && !matchDTOOptional.get().isDeleted()) {
                                butDTO.setMatchId(matchDTOOptional.get().getId());
                            } else {
                                butDTO.setMatchId(null);
                            }
                        }

                        //get match by hashcode
                        if (butDTO.getMatchId() == null) {
                            this.matchService.findByHashcode(matchHashcode)
                                .ifPresent(matchDTO -> butDTO.setMatchId(matchDTO.getId()));
                        }

                        //get joueur by id
                        if (butDTO.getJoueurId() != null) {
                            Optional<JoueurDTO> joueurDTOOptional = this.joueurService.findOne(butDTO.getJoueurId());

                            if (joueurDTOOptional.isPresent() && !joueurDTOOptional.get().isDeleted()) {
                                butDTO.setJoueurId(joueurDTOOptional.get().getId());
                            } else {
                                butDTO.setJoueurId(null);
                            }
                        }

                        //get joueur by hashcode
                        if (butDTO.getJoueurId() == null) {
                            this.joueurService.findByHashcode(joueurHashcode)
                                .ifPresent(joueurDTO1 -> butDTO.setJoueurId(joueurDTO1.getId()));
                        }

                        if (butDTO.getMatchId() == null || butDTO.getJoueurId() == null) {
                            this.log.info("Maj but: match ou joueur n'existe pas");
                        }
                        else {
                            if (butDTO.getCreatedAt() == null) {
                                butDTO.setCreatedAt(ZonedDateTime.now());
                            }
                            butDTO.setUpdatedAt(ZonedDateTime.now());


                            ButDTO result = this.butService.save(butDTO);

                            this.notificationService.sendButNotification(butDTO);

                            butToReturn.add(result);

                            // send to client
                            this.notificationService.sendButToClient(result);
                        }
                    }
                }
            }
        }

        return new ResponseEntity<>(butToReturn, HttpStatus.OK);
    }

    @PostMapping("/mobile/sync/cartons")
    public ResponseEntity<List<CartonDTO>> syncCartons(@RequestBody List<CustomCarton> customCartons) {
        List<CartonDTO> cartonToReturn = new ArrayList<>();

        if(customCartons == null || customCartons.isEmpty()) {
            this.log.info("Maj carton: Aucune donnée à sauvegarder");
        }
        else {
            for (CustomCarton customCarton : customCartons) {
                if (customCarton == null) {
                    this.log.info("Maj carton: carton n'existe pas");
                } else {
                    // Vérifier si le carton est déja synchronisé
                    Optional<CartonDTO> sync = cartonService.findByHashcode(customCarton.getCartonDTO().getHashcode());

                    if (sync.isPresent()) {
                        cartonToReturn.add(sync.get());
                    } else {
                        String matchHashcode = customCarton.getMatchHashcode();
                        String joueurHashcode = customCarton.getJoueurHashcode();
                        CartonDTO cartonDTO = customCarton.getCartonDTO();

                        //get match by id
                        if (cartonDTO.getMatchId() != null) {
                            Optional<MatchDTO> matchDTOOptional = this.matchService.findOne(cartonDTO.getMatchId());

                            if (matchDTOOptional.isPresent() && !matchDTOOptional.get().isDeleted()) {
                                cartonDTO.setMatchId(matchDTOOptional.get().getId());
                            } else {
                                cartonDTO.setMatchId(null);
                            }
                        }

                        //get match by hashcode
                        if (cartonDTO.getMatchId() == null) {
                            this.matchService.findByHashcode(matchHashcode)
                                .ifPresent(matchDTO -> cartonDTO.setMatchId(matchDTO.getId()));
                        }

                        //get joueur by id
                        if (cartonDTO.getJoueurId() != null) {
                            Optional<JoueurDTO> joueurDTOOptional = this.joueurService.findOne(cartonDTO.getJoueurId());

                            if (joueurDTOOptional.isPresent() && !joueurDTOOptional.get().isDeleted()) {
                                cartonDTO.setJoueurId(joueurDTOOptional.get().getId());
                            } else {
                                cartonDTO.setJoueurId(null);
                            }
                        }

                        //get joueur by hashcode
                        if (cartonDTO.getJoueurId() == null) {
                            this.joueurService.findByHashcode(joueurHashcode)
                                .ifPresent(joueurDTO -> cartonDTO.setJoueurId(joueurDTO.getId()));
                        }

                        if (cartonDTO.getMatchId() == null || cartonDTO.getJoueurId() == null) {
                            this.log.info("Maj carton: match ou joueur n'existe pas");
                        } else {
                            if (cartonDTO.getCreatedAt() == null) {
                                cartonDTO.setCreatedAt(ZonedDateTime.now());
                            }
                            cartonDTO.setUpdatedAt(ZonedDateTime.now());

                            CartonDTO result = this.cartonService.save(cartonDTO);

                            cartonToReturn.add(result);

                            // send to client
                            this.notificationService.sendCartonToClient(result);

                        }
                    }

                }
            }
        }

        return new ResponseEntity<>(cartonToReturn, HttpStatus.OK);
    }

    @PostMapping("/mobile/sync/penalties")
    public ResponseEntity<List<PenaltyDTO>> syncPenalties(@RequestBody List<CustomPenalty> customPenaltys) {
        List<PenaltyDTO> penaltyToReturn = new ArrayList<>();

        if(customPenaltys == null || customPenaltys.isEmpty()) {
            this.log.info("Maj penalty: Aucune donnée à sauvegarder");
        }
        else {
            for (CustomPenalty customPenalty : customPenaltys) {
                if (customPenalty == null) {
                    this.log.info("Maj penalty: penalty n'existe pas");
                } else {
                    this.log.info("----- SYNC PENALTY ------ {}", customPenalty);
                    Optional<PenaltyDTO> sync = penaltyService.findByHashcode(customPenalty.getPenaltyDTO().getHashcode());

                    if (sync.isPresent() && customPenalty.getTirAuButHashcode() != null && sync.get().getTirAuButId() == null) {
                        this.log.info("----- SYNC PENALTY FOR TIR AU BUT ------");
                        // sync for tirAuBut
                        var penaltyDTO = sync.get();

                        // find tir au but for this match
                        Optional<TirAuButDTO> tirAuButDTOOptional = this.tirAuButService.findByHashcode(customPenalty.getTirAuButHashcode());

                        if (tirAuButDTOOptional.isPresent()) {
                            TirAuButDTO tirAuButDTO = tirAuButDTOOptional.get();
                            penaltyDTO.setTirAuButId(tirAuButDTO.getId());
                            penaltyDTO.setUpdatedAt(ZonedDateTime.now());
                            penaltyDTO = this.penaltyService.save(penaltyDTO);
                        }

                        penaltyToReturn.add(penaltyDTO);

                    }
                    // Vérifier si c'est déja synchronisé
                    else if (sync.isPresent()) {
                        this.log.info("----- ALREADY SYNC ------");
                        penaltyToReturn.add(sync.get());
                    } else {
                        this.log.info("----- FIRST SYNC PENALTY ------");

                        String matchHashcode = customPenalty.getMatchHashcode();
                        String joueurHashcode = customPenalty.getJoueurHashcode();
                        String tirAuButHashcode = customPenalty.getTirAuButHashcode();
                        var penaltyDTO = customPenalty.getPenaltyDTO();

                        //get match by id
                        if (penaltyDTO.getMatchId() != null) {
                            Optional<MatchDTO> match = this.matchService.findOne(penaltyDTO.getMatchId());

                            if (match.isPresent() && Boolean.FALSE.equals(match.get().isDeleted())) {
                                penaltyDTO.setMatchId(match.get().getId());
                            } else {
                                penaltyDTO.setMatchId(null);
                            }
                        }

                        //get match by hashcode
                        if (penaltyDTO.getMatchId() == null) {
                            this.matchService.findByHashcode(matchHashcode)
                                .ifPresent(matchDTO -> penaltyDTO.setMatchId(matchDTO.getId()));
                        }

                        //get joueur by id
                        if (penaltyDTO.getJoueurId() != null) {
                            Optional<JoueurDTO> joueurDTO = this.joueurService.findOne(penaltyDTO.getJoueurId());

                            if (joueurDTO.isPresent() && Boolean.FALSE.equals(joueurDTO.get().isDeleted())) {
                                penaltyDTO.setJoueurId(joueurDTO.get().getId());
                            } else {
                                penaltyDTO.setJoueurId(null);
                            }
                        }

                        //get joueur by hashcode
                        if (penaltyDTO.getJoueurId() == null) {
                            this.joueurService.findByHashcode(joueurHashcode)
                                .ifPresent(joueurDTO -> penaltyDTO.setJoueurId(joueurDTO.getId()));
                        }

                        //get tirAuBut by id
                        if (penaltyDTO.getTirAuButId() != null) {
                            Optional<TirAuButDTO> tirAuButDTO = this.tirAuButService.findOne(penaltyDTO.getTirAuButId());

                            if (tirAuButDTO.isPresent() && Boolean.FALSE.equals(tirAuButDTO.get().isDeleted())) {
                                penaltyDTO.setTirAuButId(tirAuButDTO.get().getId());
                            } else {
                                penaltyDTO.setTirAuButId(null);
                            }
                        }

                        //get tirAuBut by hashcode
                        if (penaltyDTO.getTirAuButId() == null) {
                            this.tirAuButService.findByHashcode(tirAuButHashcode)
                                .ifPresent(tirAuButDTO -> penaltyDTO.setTirAuButId(tirAuButDTO.getId()));
                        }

                        if (penaltyDTO.getMatchId() == null || penaltyDTO.getJoueurId() == null) {
                            this.log.info("Maj penalty: match ou joueur n'existe pas");
                        } else {
                            if (penaltyDTO.getCreatedAt() == null) {
                                penaltyDTO.setCreatedAt(ZonedDateTime.now());
                            }
                            penaltyDTO.setUpdatedAt(ZonedDateTime.now());

                            PenaltyDTO result = this.penaltyService.save(penaltyDTO);

                            // send to client
                            this.notificationService.sendPenaltyToClient(result);

                            penaltyToReturn.add(result);
                        }
                    }
                }
            }
        }

        return new ResponseEntity<>(penaltyToReturn, HttpStatus.OK);
    }

    @PostMapping("/mobile/sync/corners")
    public ResponseEntity<List<CornerDTO>> syncCorners(@RequestBody List<CustomCorner> customCorners) {
        List<CornerDTO> cornerToReturn = new ArrayList<>();

        if(customCorners == null || customCorners.isEmpty()) {
            this.log.info("Maj corner: Aucune donnée à sauvegarder");
        }
        else {
            for (CustomCorner customCorner : customCorners) {
                if (customCorner == null) {
                    this.log.info("Maj corner: corner n'existe pas");
                } else {
                    // Vérifier si c'est déja synchronisé
                    Optional<CornerDTO> sync = cornerService.findByHashcode(customCorner.getCornerDTO().getHashcode());

                    if (sync.isPresent()) {
                        cornerToReturn.add(sync.get());
                    } else {
                        String matchHashcode = customCorner.getMatchHashcode();
                        CornerDTO cornerDTO = customCorner.getCornerDTO();

                        //get match by id
                        if (cornerDTO.getMatchId() != null) {
                            Optional<MatchDTO> match = this.matchService.findOne(cornerDTO.getMatchId());

                            if (match.isPresent() && !match.get().isDeleted()) {
                                cornerDTO.setMatchId(match.get().getId());
                            } else {
                                cornerDTO.setMatchId(null);
                            }
                        }

                        //get match by hashcode
                        if (cornerDTO.getMatchId() == null) {
                            this.matchService.findByHashcode(matchHashcode)
                                .ifPresent(matchDTO -> cornerDTO.setMatchId(matchDTO.getId()));
                        }

                        if (cornerDTO.getMatchId() == null) {
                            this.log.info("Maj corner: match ou joueur n'existe pas");
                        } else {
                            if (cornerDTO.getCreatedAt() == null) {
                                cornerDTO.setCreatedAt(ZonedDateTime.now());
                            }
                            cornerDTO.setUpdatedAt(ZonedDateTime.now());

                            CornerDTO result = this.cornerService.save(cornerDTO);

                            cornerToReturn.add(result);

                            // send to client
                            this.notificationService.sendCornerToClient(result);
                        }
                    }

                }
            }
        }

        return new ResponseEntity<>(cornerToReturn, HttpStatus.OK);
    }

    @PostMapping("/mobile/sync/coupFrancs")
    public ResponseEntity<List<CoupFrancDTO>> syncCoupFrancs(@RequestBody List<CustomCoupFranc> customCoupFrancs) {
        List<CoupFrancDTO> coupFrancToReturn = new ArrayList<>();

        if(customCoupFrancs == null || customCoupFrancs.isEmpty()) {
            this.log.info("Maj coupFranc: Aucune donnée à sauvegarder");
        }
        else {
            customCoupFrancs.forEach(customCoupFranc -> {
                if (customCoupFranc == null) {
                    this.log.info("Maj coupFranc: coupFranc n'existe pas");
                } else {
                    // Vérifier si c'est déja synchronisé
                    Optional<CoupFrancDTO> sync = coupFrancService.findByHashcode(customCoupFranc.getCoupFrancDTO().getHashcode());

                    if (sync.isPresent()) {
                        coupFrancToReturn.add(sync.get());
                    } else {
                        String matchHashcode = customCoupFranc.getMatchHashcode();
                        CoupFrancDTO coupFrancDTO = customCoupFranc.getCoupFrancDTO();

                        //get match by id
                        if (coupFrancDTO.getMatchId() != null) {
                            Optional<MatchDTO> match = this.matchService.findOne(coupFrancDTO.getMatchId());

                            if (match.isPresent() && !match.get().isDeleted()) {
                                coupFrancDTO.setMatchId(match.get().getId());
                            } else {
                                coupFrancDTO.setMatchId(null);
                            }
                        }

                        //get match by hashcode
                        if (coupFrancDTO.getMatchId() == null) {
                            this.matchService.findByHashcode(matchHashcode)
                                .ifPresent(matchDTO -> coupFrancDTO.setMatchId(matchDTO.getId()));
                        }

                        if (coupFrancDTO.getMatchId() == null) {
                            this.log.info("Maj coupFranc: match ou joueur n'existe pas");
                        } else {
                            if (coupFrancDTO.getCreatedAt() == null) {
                                coupFrancDTO.setCreatedAt(ZonedDateTime.now());
                            }
                            coupFrancDTO.setUpdatedAt(ZonedDateTime.now());

                            CoupFrancDTO result = this.coupFrancService.save(coupFrancDTO);

                            coupFrancToReturn.add(result);

                            // send to client
                            this.notificationService.sendCoupFrancToClient(result);
                        }
                    }
                }
            });
        }

        return new ResponseEntity<>(coupFrancToReturn, HttpStatus.OK);
    }

    @PostMapping("/mobile/sync/remplacements")
    public ResponseEntity<List<RemplacementDTO>> syncRemplacements(@RequestBody List<CustomRemplacement> customRemplacements) {
        List<RemplacementDTO> remplacementToReturn = new ArrayList<>();

        if(customRemplacements == null || customRemplacements.isEmpty()) {
            this.log.info("Maj remplacement: Aucune donnée à sauvegarder");
        }
        else {
            for (CustomRemplacement customRemplacement : customRemplacements) {
                if (customRemplacement == null) {
                    this.log.info("Maj remplacement: remplacement n'existe pas");
                } else {
                    // Vérifier si c'est déja synchronisé
                    Optional<RemplacementDTO> sync = remplacementService.findByHashcode(customRemplacement.getRemplacementDTO().getHashcode());

                    if (sync.isPresent()) {
                        remplacementToReturn.add(sync.get());
                    } else {
                        String matchHashcode = customRemplacement.getMatchHashcode();
                        String entrantHashcode = customRemplacement.getEntrantHashcode();
                        String sortantHashcode = customRemplacement.getSortantHashcode();
                        RemplacementDTO remplacementDTO = customRemplacement.getRemplacementDTO();

                        //get match by id
                        if (remplacementDTO.getMatchId() != null) {
                            Optional<MatchDTO> match = this.matchService.findOne(remplacementDTO.getMatchId());

                            if (match.isPresent() && !match.get().isDeleted()) {
                                remplacementDTO.setMatchId(match.get().getId());
                            } else {
                                remplacementDTO.setMatchId(null);
                            }
                        }

                        //get match by hashcode
                        if (remplacementDTO.getMatchId() == null) {
                            this.matchService.findByHashcode(matchHashcode)
                                .ifPresent(matchDTO -> remplacementDTO.setMatchId(matchDTO.getId()));
                        }

                        //get entrant by id
                        if (remplacementDTO.getEntrantId() != null) {
                            Optional<JoueurDTO> joueurDTO = this.joueurService.findOne(remplacementDTO.getEntrantId());

                            if (joueurDTO.isPresent() && !joueurDTO.get().isDeleted()) {
                                remplacementDTO.setEntrantId(joueurDTO.get().getId());
                            } else {
                                remplacementDTO.setEntrantId(null);
                            }
                        }

                        //get entrant by hashcode
                        if (remplacementDTO.getEntrantId() == null) {
                            this.joueurService.findByHashcode(entrantHashcode)
                                .ifPresent(joueurDTO -> remplacementDTO.setEntrantId(joueurDTO.getId()));
                        }

                        //get sortant by id
                        if (remplacementDTO.getSortantId() != null) {
                            Optional<JoueurDTO> joueurDTO = this.joueurService.findOne(remplacementDTO.getSortantId());

                            if (joueurDTO.isPresent() && !joueurDTO.get().isDeleted()) {
                                remplacementDTO.setSortantId(joueurDTO.get().getId());
                            } else {
                                remplacementDTO.setSortantId(null);
                            }
                        }

                        //get sortant by hashcode
                        if (remplacementDTO.getSortantId() == null) {
                            this.joueurService.findByHashcode(sortantHashcode)
                                .ifPresent(joueurDTO -> remplacementDTO.setSortantId(joueurDTO.getId()));

                        }

                        if (remplacementDTO.getMatchId() == null || remplacementDTO.getEntrantId() == null || remplacementDTO.getSortantId() == null) {
                            this.log.info("Maj remplacement: match ou entrant ou sortant n'existe pas");
                        } else {
                            if (remplacementDTO.getCreatedAt() == null) {
                                remplacementDTO.setCreatedAt(ZonedDateTime.now());
                            }
                            remplacementDTO.setUpdatedAt(ZonedDateTime.now());

                            RemplacementDTO result = this.remplacementService.save(remplacementDTO);

                            remplacementToReturn.add(result);

                            // send to client
                            this.notificationService.sendRemplacementToClient(result);
                        }
                    }
                }
            }
        }

        return new ResponseEntity<>(remplacementToReturn, HttpStatus.OK);
    }

    @PostMapping("/mobile/sync/horsjeus")
    public ResponseEntity<List<HorsJeuDTO>> syncHorsJeus(@RequestBody List<CustomHorsJeu> customHorsJeus) {
        List<HorsJeuDTO> toReturns = new ArrayList<>();

        if(customHorsJeus == null || customHorsJeus.isEmpty()) {
            this.log.info("Maj corner: Aucune donnée à sauvegarder");
        }
        else {
            customHorsJeus.forEach(customHorsJeu -> {
                if (customHorsJeu == null) {
                    this.log.info("Maj corner: corner n'existe pas");
                } else {
                    // Vérifier si c'est déja synchronisé
                    Optional<HorsJeuDTO> sync = horsJeuService.findByHashcode(customHorsJeu.getHorsJeuDTO().getHashcode());

                    if (sync.isPresent()) {
                        toReturns.add(sync.get());
                    } else {
                        String matchHashcode = customHorsJeu.getMatchHashcode();
                        HorsJeuDTO horsJeuDTO = customHorsJeu.getHorsJeuDTO();

                        //get match by id
                        if (horsJeuDTO.getMatchId() != null) {
                            Optional<MatchDTO> match = this.matchService.findOne(horsJeuDTO.getMatchId());

                            if (match.isPresent() && !match.get().isDeleted()) {
                                horsJeuDTO.setMatchId(match.get().getId());
                            } else {
                                horsJeuDTO.setMatchId(null);
                            }
                        }

                        //get match by hashcode
                        if (horsJeuDTO.getMatchId() == null) {
                            this.matchService.findByHashcode(matchHashcode)
                                .ifPresent(matchDTO -> horsJeuDTO.setMatchId(matchDTO.getId()));
                        }

                        if (horsJeuDTO.getMatchId() == null) {
                            this.log.info("Maj corner: match ou joueur n'existe pas");
                        } else {
                            if (horsJeuDTO.getCreatedAt() == null) {
                                horsJeuDTO.setCreatedAt(ZonedDateTime.now());
                            }
                            horsJeuDTO.setUpdatedAt(ZonedDateTime.now());

                            HorsJeuDTO result = this.horsJeuService.save(horsJeuDTO);

                            toReturns.add(result);

                            // send to client
                            this.notificationService.sendHorsJeuToClient(result);
                        }
                    }

                }
            });
        }

        return new ResponseEntity<>(toReturns, HttpStatus.OK);
    }

    @PostMapping("/mobile/sync/tirs")
    public ResponseEntity<List<TirDTO>> syncTirs(@RequestBody List<CustomTir> customTirs) {
        List<TirDTO> tirToReturn = new ArrayList<>();

        if(customTirs == null || customTirs.isEmpty()) {
            this.log.info("Maj tir: Aucune donnée à sauvegarder");
        }
        else {
            for (CustomTir customTir : customTirs) {
                if (customTir == null) {
                    this.log.info("Maj tir: tir n'existe pas");
                } else {
                    // Vérifier si c'est déja synchronisé
                    Optional<TirDTO> sync = tirService.findByHashcode(customTir.getTirDTO().getHashcode());

                    if (sync.isPresent()) {
                        tirToReturn.add(sync.get());
                    } else {
                        String matchHashcode = customTir.getMatchHashcode();
                        String joueurHashcode = customTir.getJoueurHashcode();
                        TirDTO tirDTO = customTir.getTirDTO();

                        //get match by id
                        if (tirDTO.getMatchId() != null) {
                            Optional<MatchDTO> match = this.matchService.findOne(tirDTO.getMatchId());

                            if (match.isPresent() && !match.get().isDeleted()) {
                                tirDTO.setMatchId(match.get().getId());
                            } else {
                                tirDTO.setMatchId(null);
                            }
                        }

                        //get match by hashcode
                        if (tirDTO.getMatchId() == null) {
                            this.matchService.findByHashcode(matchHashcode)
                                .ifPresent(matchDTO -> tirDTO.setMatchId(matchDTO.getId()));
                        }

                        //get joueur by id
                        if (tirDTO.getJoueurId() != null) {
                            Optional<JoueurDTO> joueurDTO = this.joueurService.findOne(tirDTO.getJoueurId());

                            if (joueurDTO.isPresent() && !joueurDTO.get().isDeleted()) {
                                tirDTO.setJoueurId(joueurDTO.get().getId());
                            } else {
                                tirDTO.setJoueurId(null);
                            }
                        }

                        //get joueur by hashcode
                        if (tirDTO.getJoueurId() == null) {
                            this.joueurService.findByHashcode(joueurHashcode)
                                .ifPresent(joueurDTO -> tirDTO.setJoueurId(joueurDTO.getId()));
                        }

                        if (tirDTO.getMatchId() == null || tirDTO.getJoueurId() == null) {
                            this.log.info("Maj tir: match ou joueur n'existe pas");
                        } else {
                            if (tirDTO.getCreatedAt() == null) {
                                tirDTO.setCreatedAt(ZonedDateTime.now());
                            }
                            tirDTO.setUpdatedAt(ZonedDateTime.now());

                            TirDTO result = this.tirService.save(tirDTO);

                            tirToReturn.add(result);

                            // send to client
                            this.notificationService.sendTirToClient(result);
                        }
                    }

                }
            }
        }

        return new ResponseEntity<>(tirToReturn, HttpStatus.OK);
    }
}
