package org.nddngroup.navetfoot.core.web.rest.custom;

import org.nddngroup.navetfoot.core.domain.custom.ToAddOrRemove;
import org.nddngroup.navetfoot.core.exception.ApiRequestException;
import org.nddngroup.navetfoot.core.exception.ExceptionCode;
import org.nddngroup.navetfoot.core.exception.ExceptionLevel;
import org.nddngroup.navetfoot.core.security.AuthoritiesConstants;
import org.nddngroup.navetfoot.core.service.AffiliationService;
import org.nddngroup.navetfoot.core.service.AssociationService;
import org.nddngroup.navetfoot.core.service.CompetitionSaisonService;
import org.nddngroup.navetfoot.core.service.ParticipationService;
import org.nddngroup.navetfoot.core.service.custom.CommunService;
import org.nddngroup.navetfoot.core.service.custom.DeleteService;
import org.nddngroup.navetfoot.core.service.dto.*;
import org.nddngroup.navetfoot.core.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import io.micrometer.core.annotation.Timed;
import io.swagger.annotations.ApiParam;
import org.nddngroup.navetfoot.core.service.dto.*;
import org.nddngroup.navetfoot.core.web.rest.RegionResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.ZonedDateTime;
import java.util.List;

@RestController
@RequestMapping("/api")
public class CustomAssociationResource {
    private final Logger log = LoggerFactory.getLogger(RegionResource.class);

    private static final String ENTITY_NAME = "navetfootCoreAssociation";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AssociationService associationService;
    @Autowired
    private AffiliationService affiliationService;
    @Autowired
    private CommunService communService;
    @Autowired
    private ParticipationService participationService;
    @Autowired
    private CompetitionSaisonService competitionSaisonService;
    @Autowired
    private DeleteService deleteService;

    @Value("${application.image.base-url}")
    private String imagesBaseUrl;
    @Value("${application.image.base-folder}")
    private String imagesBaseFolder;

    CustomAssociationResource(AssociationService associationService) throws Exception {
        this.associationService = associationService;
    }

    /**
     * POST  /organisations/:id/associations : Create a new association.
     *
     * @param associationDTO the associationDTO to create
     * @return the CustomResponse with status 201 (Created) and with body the new associationDTO, or with status 400 (Bad Request) if the association has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/organisations/{id}/associations")
    public ResponseEntity<AssociationDTO> createAssociationForOrganisation(@PathVariable Long id,
                                                                           @Valid @RequestBody AssociationDTO associationDTO) throws Exception {
        log.debug("REST request to save Association : {}", associationDTO);
        if (associationDTO.getId() != null) {
            throw new BadRequestAlertException("A new association cannot already have an ID", ENTITY_NAME, "idexists");
        }

        // get organisation
        OrganisationDTO organisationDTO = this.communService.checkOrganisation(id);

        AssociationDTO result = associationService.save(associationDTO, organisationDTO);

        return ResponseEntity.created(new URI("/api/organisations/" + id + "/associations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * POST  /organisations/:organisationId/etape-competitions/:etapeId/associations : add associations to etape.
     *
     * @param etapeId L'id de l'étape compétition
     * @param organisationId L'id de l'organisation
     * @param associationToAddOrRemove // liste des clubs à ajouter ou supprimer
     * @return the CustomResponse with status 201 (Created) and with body the new associationDTO, or with status 400 (Bad Request) if the association has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/organisations/{organisationId}/etape-competitions/{etapeId}/associations")
    public ResponseEntity<ToAddOrRemove> createAssociation(@PathVariable Long etapeId,
                                                           @PathVariable Long organisationId,
                                                           @RequestBody ToAddOrRemove associationToAddOrRemove) throws URISyntaxException {
        log.debug("REST request to add Associations to EtapeCompetition: idEtape = {}", etapeId);


        // get organisation
        OrganisationDTO organisationDTO = this.communService.checkOrganisation(organisationId);

        // check etapeCompetition
        EtapeCompetitionDTO etapeCompetitionDTO = this.communService.checkEtapeCompetition(etapeId, organisationDTO);

        CompetitionSaisonDTO competitionSaisonDTO = this.competitionSaisonService.findOne(etapeCompetitionDTO.getCompetitionSaisonId())
            .orElseThrow(
                () -> new ApiRequestException(
                    ExceptionCode.COMPETITION_SAISON_NOT_FOUND.getMessage(),
                    ExceptionCode.COMPETITION_SAISON_NOT_FOUND.getValue(),
                    ExceptionLevel.ERROR
                )
            );

        SaisonDTO saisonDTO = this.communService.checkSaisonForOrganisation(competitionSaisonDTO.getSaisonId(), organisationDTO);

        List<Long> toAdd    = associationToAddOrRemove.getToAdd();
        List<Long> toRemove = associationToAddOrRemove.getToRemove();

        communService.updateParticipations(toAdd, toRemove, etapeId, etapeCompetitionDTO, saisonDTO, organisationDTO);

        return ResponseEntity.created(new URI("/api/organisations/" + organisationId  + "/etape-competitions/" + etapeId + "/associations"))
            .body(associationToAddOrRemove);
    }

    /**
     * POST /organisations/:organisationId/import/associations : import list of association
     *
     * @param associationDTOS
     * @return
     */
    @PostMapping("/organisations/{organisationId}/import/associations")
    public ResponseEntity<List<AssociationDTO>> importAssociations(@RequestBody List<AssociationDTO> associationDTOS,
                                                                   @PathVariable Long organisationId) throws URISyntaxException {
        OrganisationDTO organisationDTO = communService.checkOrganisation(organisationId);

        List<AssociationDTO> result = associationService.save(organisationDTO, associationDTOS);

        return ResponseEntity.created(new URI("/api/organisations/" + organisationId + "/import/associations/"))
            .body(result);
    }



    /**
     * POST  /associations/:id/image : Ajouter une image au club.
     *
     * Upload joueur image
     */
    @PostMapping("/associations/{id}/image")
    public ResponseEntity<AssociationDTO> uploadImage(@RequestParam("file") MultipartFile file, @PathVariable Long id) {
        return ResponseEntity.ok().body(communService.addClubImage(file, id));
    }

    /**
     * {@code PUT  /organisations/:id/associations} : Updates an existing association.
     *
     * @param associationDTO the associationDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated associationDTO,
     * or with status {@code 400 (Bad Request)} if the associationDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the associationDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/organisations/{id}/associations")
    public ResponseEntity<AssociationDTO> updateAssociation(@PathVariable Long id, @Valid @RequestBody AssociationDTO associationDTO) throws URISyntaxException {
        log.debug("REST request to update Association : {}", associationDTO);
        if (associationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }

        communService.checkOrganisation(id);

        associationDTO.setUpdatedAt(ZonedDateTime.now());

        AssociationDTO result = associationService.save(associationDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, associationDTO.getId().toString()))
            .body(result);
    }


    /**
     * {@code GET  /organisations/:id/associations/all} : get associations by organisation
     *
     * Get associations of organisation
     * @param id L'id de l'organisation
     */
    @GetMapping("/organisations/{id}/associations/all")
    @Timed
    public ResponseEntity<List<AssociationDTO>> getAssociationsByOrganisation(@PathVariable Long id) {
        OrganisationDTO organisationDTO = this.communService.checkOrganisation(id);

        if(organisationDTO != null && !organisationDTO.isDeleted()) {
            List<AssociationDTO> associationDTOs = communService.findAssociationsByOrganisation(organisationDTO);
            return new ResponseEntity<>(associationDTOs, HttpStatus.OK);
        }
        else {
            this.log.info("Aucune organisation trouvé pour l'id " + id);
            throw new ApiRequestException(
                ExceptionCode.ORGANISATION_NOT_FOUND.getMessage(),
                ExceptionCode.ORGANISATION_NOT_FOUND.getValue(),
                ExceptionLevel.ERROR
            );
        }
    }

    /**
     * {@code GET  /organisations/:id/associations} : get associations by organisation
     *
     * Get associations of organisation
     * @param id L'id de l'organisation
     */
    @GetMapping("/organisations/{id}/associations")
    public ResponseEntity<List<AssociationDTO>> getAssociationsByOrganisation(@PathVariable Long id, Pageable pageable) {
        OrganisationDTO organisationDTO = this.communService.checkOrganisation(id);

        if(organisationDTO != null && !organisationDTO.isDeleted()) {
            Page<AssociationDTO> page = communService.findPageAssociationsByOrganisation(organisationDTO, pageable);
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);

            return ResponseEntity.ok().headers(headers).body(page.getContent());
        }
        else {
            this.log.info("Aucune organisation trouvé pour l'id " + id);
            throw new ApiRequestException(
                ExceptionCode.ORGANISATION_NOT_FOUND.getMessage(),
                ExceptionCode.ORGANISATION_NOT_FOUND.getValue(),
                ExceptionLevel.ERROR
            );
        }
    }

    /**
     * GET  /organisations/:organisationId/etape-competitions/:id/associations : get all the associations.
     *
     * @return the CustomResponse with status 200 (OK) and the list of associations in body
     */
    @GetMapping("/organisations/{organisationId}/etape-competitions/{id}/associations")
    public ResponseEntity<List<AssociationDTO>> getAllAssociations(@PathVariable Long organisationId, @PathVariable Long id) {
        log.debug("REST request to get a page of Associations");

        // get organisation
        OrganisationDTO organisationDTO = this.communService.checkOrganisation(organisationId);

        // check etapeCompetiton
        EtapeCompetitionDTO etapeCompetitionDTO = this.communService.checkEtapeCompetition(id, organisationDTO);

        List<AssociationDTO> associationDTOS = this.communService.getAssociationsByEtapeCompetition(etapeCompetitionDTO.getId());

        return new ResponseEntity<>(associationDTOS, HttpStatus.OK);
    }


    /**
     * GET  /organisations/:organisationId/etape-competitions/:etapeCompetitionId/associations/not-added : get all not added associations for 'etape'.
     *
     * @param organisationId L'id de l'organisation
     * @param etapeCompetitionId L'id de l'étape
     * @return the CustomResponse with status 200 (OK) and the list of associations in body
     */
    @GetMapping("/organisations/{organisationId}/etape-competitions/{etapeCompetitionId}/associations/not-added")
    public ResponseEntity<List<AssociationDTO>> getNotAddedAssociations(@PathVariable Long organisationId,
                                                                        @PathVariable Long etapeCompetitionId) {
        log.debug("REST request to get a page of Associations");

        // get organisation
        OrganisationDTO organisationDTO = this.communService.checkOrganisation(organisationId);

        // check if etape is valid
        EtapeCompetitionDTO etapeCompetitionDTO = this.communService.checkEtapeCompetition(etapeCompetitionId, organisationDTO);

        // get not add associations
        List<AssociationDTO> associationDTOS = this.communService.getNotAddedAssociations(etapeCompetitionDTO, organisationDTO);

        return new ResponseEntity<>(associationDTOS, HttpStatus.OK);
    }



    /**
     * GET  /associations : get all the associations.
     *
     * @param pageable the pagination information
     * @return the CustomResponse with status 200 (OK) and the list of associations in body
     */
    @GetMapping("/mobile/associations")
    @Timed
    @Secured({AuthoritiesConstants.MOBILE})
    public ResponseEntity<List<AssociationDTO>> getAllAssociationsForMobile(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Associations");
        Page<AssociationDTO> page = associationService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    /**
     * DELETE /organisations/:organisationId/associations : remove list of association
     *
     * @param headers HttpHeaders
     * @return List<AssociationDTO>
     */
    @DeleteMapping("/organisations/{organisationId}/associations")
    public ResponseEntity<List<AssociationDTO>> removeAssociations(@RequestHeader HttpHeaders headers,
                                                                   @RequestParam(required = false) Boolean owner,
                                                                   @PathVariable Long organisationId) throws URISyntaxException {
        OrganisationDTO organisationDTO = communService.checkOrganisation(organisationId);

        List<String> associationIds = headers.getValuesAsList("ids");

        List<AssociationDTO> result = deleteService.removeAssociationList(organisationDTO, associationIds, owner);

        return ResponseEntity.created(new URI("/api/organisations/" + organisationId + "/associations/"))
            .body(result);
    }

    /**
     * DELETE  /organisations/:organisationId/associations/:id : delete the "id" association.
     *
     * @param id the id of the associationDTO to delete
     * @return the CustomResponse with status 200 (OK)
     */
    @DeleteMapping("/organisations/{organisationId}/associations/{id}")
    @Timed
    public ResponseEntity<Void> deleteAssociationForOrganisation(@PathVariable Long organisationId, @PathVariable Long id) {
        log.debug("REST request to delete Association : {}", id);

        AssociationDTO associationDTO = associationService.findOne(id)
            .orElseThrow(
                () -> new ApiRequestException(
                    ExceptionCode.ASSOCIATIONS_NOT_FOUND.getMessage(),
                    ExceptionCode.ASSOCIATIONS_NOT_FOUND.getValue(),
                    ExceptionLevel.WARNING
                )
            );

        // get organisation
        OrganisationDTO organisationDTO = this.communService.checkOrganisation(organisationId);

        deleteService.deleteAssociation(associationDTO, organisationDTO);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, associationDTO.getId().toString()))
            .build();
    }

    @GetMapping("/mobile/_search/associations")
    public ResponseEntity<List<AssociationDTO>> searchAssociations(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Associations for query {}", query);
        Page<AssociationDTO> page = associationService.findByNomContains(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }


    /**
     * {@code GET  /_search/organisations/:id/associations?query=:query} : search associations by organisation
     *
     * Get associations of organisation
     * @param id L'id de l'organisation
     */
    @GetMapping("/_search/organisations/{id}/associations")
    public ResponseEntity<List<AssociationDTO>> searchAssociationsByOrganisation(@RequestParam String query,
                                                                                 @PathVariable Long id,
                                                                                 Pageable pageable) {
        OrganisationDTO organisationDTO = this.communService.checkOrganisation(id);

        if(organisationDTO != null && !organisationDTO.isDeleted()) {
            Page<AssociationDTO> page = associationService.search(organisationDTO, query, pageable);
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);

            return ResponseEntity.ok().headers(headers).body(page.getContent());
        }
        else {
            this.log.info("Aucune organisation trouvé pour l'id " + id);
            throw new ApiRequestException(
                ExceptionCode.ORGANISATION_NOT_FOUND.getMessage(),
                ExceptionCode.ORGANISATION_NOT_FOUND.getValue(),
                ExceptionLevel.ERROR
            );
        }
    }
}
