package org.nddngroup.navetfoot.core.web.rest;

import org.nddngroup.navetfoot.core.service.CompetitionSaisonCategoryService;
import org.nddngroup.navetfoot.core.web.rest.errors.BadRequestAlertException;
import org.nddngroup.navetfoot.core.service.dto.CompetitionSaisonCategoryDTO;

import org.nddngroup.navetfoot.core.domain.CompetitionSaisonCategory;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link CompetitionSaisonCategory}.
 */
@RestController
@RequestMapping("/api")
public class CompetitionSaisonCategoryResource {

    private final Logger log = LoggerFactory.getLogger(CompetitionSaisonCategoryResource.class);

    private static final String ENTITY_NAME = "navetfootCoreCompetitionSaisonCategory";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CompetitionSaisonCategoryService competitionSaisonCategoryService;

    public CompetitionSaisonCategoryResource(CompetitionSaisonCategoryService competitionSaisonCategoryService) {
        this.competitionSaisonCategoryService = competitionSaisonCategoryService;
    }

    /**
     * {@code POST  /competition-saison-categories} : Create a new competitionSaisonCategory.
     *
     * @param competitionSaisonCategoryDTO the competitionSaisonCategoryDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new competitionSaisonCategoryDTO, or with status {@code 400 (Bad Request)} if the competitionSaisonCategory has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/competition-saison-categories")
    public ResponseEntity<CompetitionSaisonCategoryDTO> createCompetitionSaisonCategory(@RequestBody CompetitionSaisonCategoryDTO competitionSaisonCategoryDTO) throws URISyntaxException {
        log.debug("REST request to save CompetitionSaisonCategory : {}", competitionSaisonCategoryDTO);
        if (competitionSaisonCategoryDTO.getId() != null) {
            throw new BadRequestAlertException("A new competitionSaisonCategory cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CompetitionSaisonCategoryDTO result = competitionSaisonCategoryService.save(competitionSaisonCategoryDTO);
        return ResponseEntity.created(new URI("/api/competition-saison-categories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /competition-saison-categories} : Updates an existing competitionSaisonCategory.
     *
     * @param competitionSaisonCategoryDTO the competitionSaisonCategoryDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated competitionSaisonCategoryDTO,
     * or with status {@code 400 (Bad Request)} if the competitionSaisonCategoryDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the competitionSaisonCategoryDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/competition-saison-categories")
    public ResponseEntity<CompetitionSaisonCategoryDTO> updateCompetitionSaisonCategory(@RequestBody CompetitionSaisonCategoryDTO competitionSaisonCategoryDTO) throws URISyntaxException {
        log.debug("REST request to update CompetitionSaisonCategory : {}", competitionSaisonCategoryDTO);
        if (competitionSaisonCategoryDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CompetitionSaisonCategoryDTO result = competitionSaisonCategoryService.save(competitionSaisonCategoryDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, competitionSaisonCategoryDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /competition-saison-categories} : get all the competitionSaisonCategories.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of competitionSaisonCategories in body.
     */
    @GetMapping("/competition-saison-categories")
    public ResponseEntity<List<CompetitionSaisonCategoryDTO>> getAllCompetitionSaisonCategories(Pageable pageable) {
        log.debug("REST request to get a page of CompetitionSaisonCategories");
        Page<CompetitionSaisonCategoryDTO> page = competitionSaisonCategoryService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /competition-saison-categories/:id} : get the "id" competitionSaisonCategory.
     *
     * @param id the id of the competitionSaisonCategoryDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the competitionSaisonCategoryDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/competition-saison-categories/{id}")
    public ResponseEntity<CompetitionSaisonCategoryDTO> getCompetitionSaisonCategory(@PathVariable Long id) {
        log.debug("REST request to get CompetitionSaisonCategory : {}", id);
        Optional<CompetitionSaisonCategoryDTO> competitionSaisonCategoryDTO = competitionSaisonCategoryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(competitionSaisonCategoryDTO);
    }

    /**
     * {@code DELETE  /competition-saison-categories/:id} : delete the "id" competitionSaisonCategory.
     *
     * @param id the id of the competitionSaisonCategoryDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/competition-saison-categories/{id}")
    public ResponseEntity<Void> deleteCompetitionSaisonCategory(@PathVariable Long id) {
        log.debug("REST request to delete CompetitionSaisonCategory : {}", id);
        competitionSaisonCategoryService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/competition-saison-categories?query=:query} : search for the competitionSaisonCategory corresponding
     * to the query.
     *
     * @param query the query of the competitionSaisonCategory search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/competition-saison-categories")
    public ResponseEntity<List<CompetitionSaisonCategoryDTO>> searchCompetitionSaisonCategories(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of CompetitionSaisonCategories for query {}", query);
        Page<CompetitionSaisonCategoryDTO> page = competitionSaisonCategoryService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
        }
}
