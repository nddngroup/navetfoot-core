package org.nddngroup.navetfoot.core.web.rest;

import org.nddngroup.navetfoot.core.domain.Corner;
import org.nddngroup.navetfoot.core.service.CornerService;
import org.nddngroup.navetfoot.core.service.dto.CornerDTO;
import org.nddngroup.navetfoot.core.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link Corner}.
 */
@RestController
@RequestMapping("/api")
public class CornerResource {

    private final Logger log = LoggerFactory.getLogger(CornerResource.class);

    private static final String ENTITY_NAME = "navetfootCoreCorner";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CornerService cornerService;

    public CornerResource(CornerService cornerService) {
        this.cornerService = cornerService;
    }

    /**
     * {@code POST  /corners} : Create a new corner.
     *
     * @param cornerDTO the cornerDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new cornerDTO, or with status {@code 400 (Bad Request)} if the corner has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/corners")
    public ResponseEntity<CornerDTO> createCorner(@RequestBody CornerDTO cornerDTO) throws URISyntaxException {
        log.debug("REST request to save Corner : {}", cornerDTO);
        if (cornerDTO.getId() != null) {
            throw new BadRequestAlertException("A new corner cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CornerDTO result = cornerService.save(cornerDTO);
        return ResponseEntity.created(new URI("/api/corners/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /corners} : Updates an existing corner.
     *
     * @param cornerDTO the cornerDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated cornerDTO,
     * or with status {@code 400 (Bad Request)} if the cornerDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the cornerDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/corners")
    public ResponseEntity<CornerDTO> updateCorner(@RequestBody CornerDTO cornerDTO) throws URISyntaxException {
        log.debug("REST request to update Corner : {}", cornerDTO);
        if (cornerDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CornerDTO result = cornerService.save(cornerDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, cornerDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /corners} : get all the corners.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of corners in body.
     */
    @GetMapping("/corners")
    public ResponseEntity<List<CornerDTO>> getAllCorners(Pageable pageable) {
        log.debug("REST request to get a page of Corners");
        Page<CornerDTO> page = cornerService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /corners/:id} : get the "id" corner.
     *
     * @param id the id of the cornerDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the cornerDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/corners/{id}")
    public ResponseEntity<CornerDTO> getCorner(@PathVariable Long id) {
        log.debug("REST request to get Corner : {}", id);
        Optional<CornerDTO> cornerDTO = cornerService.findOne(id);
        return ResponseUtil.wrapOrNotFound(cornerDTO);
    }

    /**
     * {@code DELETE  /corners/:id} : delete the "id" corner.
     *
     * @param id the id of the cornerDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/corners/{id}")
    public ResponseEntity<Void> deleteCorner(@PathVariable Long id) {
        log.debug("REST request to delete Corner : {}", id);
        cornerService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/corners?query=:query} : search for the corner corresponding
     * to the query.
     *
     * @param query the query of the corner search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/corners")
    public ResponseEntity<List<CornerDTO>> searchCorners(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Corners for query {}", query);
        Page<CornerDTO> page = cornerService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
        }
}
