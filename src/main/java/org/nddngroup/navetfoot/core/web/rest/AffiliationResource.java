package org.nddngroup.navetfoot.core.web.rest;

import org.nddngroup.navetfoot.core.domain.Affiliation;
import org.nddngroup.navetfoot.core.service.AffiliationService;
import org.nddngroup.navetfoot.core.service.dto.AffiliationDTO;
import org.nddngroup.navetfoot.core.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link Affiliation}.
 */
@RestController
@RequestMapping("/api")
public class AffiliationResource {

    private final Logger log = LoggerFactory.getLogger(AffiliationResource.class);

    private static final String ENTITY_NAME = "navetfootCoreAffiliation";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AffiliationService affiliationService;

    public AffiliationResource(AffiliationService affiliationService) {
        this.affiliationService = affiliationService;
    }

    /**
     * {@code POST  /affiliations} : Create a new affiliation.
     *
     * @param affiliationDTO the affiliationDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new affiliationDTO, or with status {@code 400 (Bad Request)} if the affiliation has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/affiliations")
    public ResponseEntity<AffiliationDTO> createAffiliation(@RequestBody AffiliationDTO affiliationDTO) throws URISyntaxException {
        log.debug("REST request to save Affiliation : {}", affiliationDTO);
        if (affiliationDTO.getId() != null) {
            throw new BadRequestAlertException("A new affiliation cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AffiliationDTO result = affiliationService.save(affiliationDTO);
        return ResponseEntity.created(new URI("/api/affiliations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /affiliations} : Updates an existing affiliation.
     *
     * @param affiliationDTO the affiliationDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated affiliationDTO,
     * or with status {@code 400 (Bad Request)} if the affiliationDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the affiliationDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/affiliations")
    public ResponseEntity<AffiliationDTO> updateAffiliation(@RequestBody AffiliationDTO affiliationDTO) throws URISyntaxException {
        log.debug("REST request to update Affiliation : {}", affiliationDTO);
        if (affiliationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AffiliationDTO result = affiliationService.save(affiliationDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, affiliationDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /affiliations} : get all the affiliations.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of affiliations in body.
     */
    @GetMapping("/affiliations")
    public ResponseEntity<List<AffiliationDTO>> getAllAffiliations(Pageable pageable) {
        log.debug("REST request to get a page of Affiliations");
        Page<AffiliationDTO> page = affiliationService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /affiliations/:id} : get the "id" affiliation.
     *
     * @param id the id of the affiliationDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the affiliationDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/affiliations/{id}")
    public ResponseEntity<AffiliationDTO> getAffiliation(@PathVariable Long id) {
        log.debug("REST request to get Affiliation : {}", id);
        Optional<AffiliationDTO> affiliationDTO = affiliationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(affiliationDTO);
    }

    /**
     * {@code DELETE  /affiliations/:id} : delete the "id" affiliation.
     *
     * @param id the id of the affiliationDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/affiliations/{id}")
    public ResponseEntity<Void> deleteAffiliation(@PathVariable Long id) {
        log.debug("REST request to delete Affiliation : {}", id);
        affiliationService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/affiliations?query=:query} : search for the affiliation corresponding
     * to the query.
     *
     * @param query the query of the affiliation search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/affiliations")
    public ResponseEntity<List<AffiliationDTO>> searchAffiliations(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Affiliations for query {}", query);
        Page<AffiliationDTO> page = affiliationService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
        }
}
