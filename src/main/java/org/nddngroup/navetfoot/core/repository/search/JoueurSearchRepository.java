package org.nddngroup.navetfoot.core.repository.search;

import org.nddngroup.navetfoot.core.domain.Joueur;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link Joueur} entity.
 */
public interface JoueurSearchRepository extends ElasticsearchRepository<Joueur, Long> {
}
