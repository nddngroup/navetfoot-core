package org.nddngroup.navetfoot.core.repository.search;

import org.nddngroup.navetfoot.core.domain.Pays;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link Pays} entity.
 */
public interface PaysSearchRepository extends ElasticsearchRepository<Pays, Long> {
}
