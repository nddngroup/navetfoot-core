package org.nddngroup.navetfoot.core.repository.search;

import org.nddngroup.navetfoot.core.domain.Affiliation;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link Affiliation} entity.
 */
public interface AffiliationSearchRepository extends ElasticsearchRepository<Affiliation, Long> {
}
