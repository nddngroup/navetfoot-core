package org.nddngroup.navetfoot.core.repository;

import org.nddngroup.navetfoot.core.domain.But;
import org.nddngroup.navetfoot.core.domain.Joueur;
import org.nddngroup.navetfoot.core.domain.Match;
import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the But entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ButRepository extends JpaRepository<But, Long> {
    List<But> findAllByDeletedIsFalse();
    List<But> findAllByMatchAndDeletedIsFalse(Match match);
    List<But> findAllByMatchAndEquipeAndDeletedIsFalse(Match match, Equipe equipe);
    List<But> findAllByMatchAndJoueurAndDeletedIsFalse(Match match, Joueur joueur);
    Optional<But> findByHashcodeAndDeletedIsFalse(String hashcode);
}
