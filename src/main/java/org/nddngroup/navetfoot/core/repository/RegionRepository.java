package org.nddngroup.navetfoot.core.repository;

import org.nddngroup.navetfoot.core.domain.Pays;
import org.nddngroup.navetfoot.core.domain.Region;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Region entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RegionRepository extends JpaRepository<Region, Long> {
    List<Region> findRegionsByPays(Pays pays);
}
