/**
 * Spring Data Elasticsearch repositories.
 */
package org.nddngroup.navetfoot.core.repository.search;
