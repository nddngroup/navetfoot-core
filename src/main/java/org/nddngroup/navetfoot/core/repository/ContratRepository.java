package org.nddngroup.navetfoot.core.repository;

import org.nddngroup.navetfoot.core.domain.*;
import org.nddngroup.navetfoot.core.domain.Association;
import org.nddngroup.navetfoot.core.domain.Contrat;
import org.nddngroup.navetfoot.core.domain.Joueur;
import org.nddngroup.navetfoot.core.domain.Saison;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Contrat entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ContratRepository extends JpaRepository<Contrat, Long> {
    List<Contrat> findAllByDeletedIsFalse();
    Optional<Contrat> findAllBySaisonAndAssociationAndJoueurAndDeletedIsFalse(Saison saison, Association association, Joueur joueur);
    List<Contrat> findAllBySaisonAndAssociationAndDeletedIsFalse(Saison saison, Association association);
    List<Contrat> findAllByAssociationAndDeletedIsFalse(Association association);
    List<Contrat> findAllByAssociationAndJoueurAndDeletedIsFalse(Association association, Joueur joueur);
    List<Contrat> findAllBySaisonAndDeletedIsFalse(Saison saison);
    List<Contrat> findAllByJoueurAndDeletedIsFalse(Joueur joueur);
    Optional<Contrat> findAllBySaisonAndJoueurAndDeletedIsFalse(Saison saison, Joueur joueur);
    Optional<Contrat> findByHashcodeAndDeletedIsFalse(String hashcode);
}
