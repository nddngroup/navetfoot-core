package org.nddngroup.navetfoot.core.repository;

import org.nddngroup.navetfoot.core.domain.Association;
import org.nddngroup.navetfoot.core.domain.EtapeCompetition;
import org.nddngroup.navetfoot.core.domain.Participation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Participation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ParticipationRepository extends JpaRepository<Participation, Long> {
    List<Participation> findAllByDeletedIsFalse();
    List<Participation> findAllByEtapeCompetitionAndDeletedIsFalse(EtapeCompetition etapeCompetition);
    Optional<Participation> findOneByEtapeCompetitionAndAssociationAndDeletedIsFalse(EtapeCompetition etapeCompetition, Association association);
    Optional<Participation> findByHashcodeAndDeletedIsFalse(String hashcode);
}
