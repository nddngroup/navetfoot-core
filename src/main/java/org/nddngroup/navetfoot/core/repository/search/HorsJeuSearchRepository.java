package org.nddngroup.navetfoot.core.repository.search;

import org.nddngroup.navetfoot.core.domain.HorsJeu;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link HorsJeu} entity.
 */
public interface HorsJeuSearchRepository extends ElasticsearchRepository<HorsJeu, Long> {
}
