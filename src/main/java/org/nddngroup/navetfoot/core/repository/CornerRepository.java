package org.nddngroup.navetfoot.core.repository;

import org.nddngroup.navetfoot.core.domain.Corner;
import org.nddngroup.navetfoot.core.domain.Match;
import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Corner entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CornerRepository extends JpaRepository<Corner, Long> {
    List<Corner> findAllByDeletedIsFalse();
    List<Corner> findAllByMatchAndDeletedIsFalse(Match match);
    List<Corner> findAllByMatchAndEquipeAndDeletedIsFalse(Match match, Equipe equipe);
    Optional<Corner> findByHashcodeAndDeletedIsFalse(String hashcode);

}
