package org.nddngroup.navetfoot.core.repository;

import org.nddngroup.navetfoot.core.domain.Departement;

import org.nddngroup.navetfoot.core.domain.Region;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Departement entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DepartementRepository extends JpaRepository<Departement, Long> {
    List<Departement> findDepartementByRegion(Region region);
}
