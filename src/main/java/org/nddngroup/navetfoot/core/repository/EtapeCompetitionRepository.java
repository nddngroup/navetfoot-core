package org.nddngroup.navetfoot.core.repository;

import org.nddngroup.navetfoot.core.domain.CompetitionSaison;
import org.nddngroup.navetfoot.core.domain.CompetitionSaisonCategory;
import org.nddngroup.navetfoot.core.domain.EtapeCompetition;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the EtapeCompetition entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EtapeCompetitionRepository extends JpaRepository<EtapeCompetition, Long> {
    List<EtapeCompetition> findAllByDeletedIsFalse();
    List<EtapeCompetition> findAllByCompetitionSaisonAndDeletedIsFalse(CompetitionSaison competitionSaison);
    List<EtapeCompetition> findAllByCompetitionSaisonIdInAndDeletedIsFalse(List<Long> competitionSaisonIds);
    List<EtapeCompetition> findAllByCompetitionSaisonAndCompetitionSaisonCategoryAndDeletedIsFalse(CompetitionSaison competitionSaison, CompetitionSaisonCategory competitionSaisonCategory);
    Optional<EtapeCompetition> findByHashcodeAndDeletedIsFalse(String hashcode);
}
