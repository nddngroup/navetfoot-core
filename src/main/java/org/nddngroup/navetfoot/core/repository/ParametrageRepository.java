package org.nddngroup.navetfoot.core.repository;

import org.nddngroup.navetfoot.core.domain.Parametrage;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Parametrage entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ParametrageRepository extends JpaRepository<Parametrage, Long> {
}
