package org.nddngroup.navetfoot.core.repository.search;

import org.nddngroup.navetfoot.core.domain.DetailFeuilleDeMatch;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link DetailFeuilleDeMatch} entity.
 */
public interface DetailFeuilleDeMatchSearchRepository extends ElasticsearchRepository<DetailFeuilleDeMatch, Long> {
}
