package org.nddngroup.navetfoot.core.repository.search;

import org.nddngroup.navetfoot.core.domain.Association;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link Association} entity.
 */
public interface AssociationSearchRepository extends ElasticsearchRepository<Association, Long> {
}
