package org.nddngroup.navetfoot.core.repository;

import org.nddngroup.navetfoot.core.domain.Match;
import org.nddngroup.navetfoot.core.domain.TirAuBut;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the TirAuBut entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TirAuButRepository extends JpaRepository<TirAuBut, Long> {
    Optional<TirAuBut> findOneByMatchAndDeletedIsFalse(Match match);
    Optional<TirAuBut> findByHashcodeAndDeletedIsFalse(String hashcode);
    List<TirAuBut> findAllByDeletedIsFalse();

}
