package org.nddngroup.navetfoot.core.repository.search;

import org.nddngroup.navetfoot.core.domain.Saison;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link Saison} entity.
 */
public interface SaisonSearchRepository extends ElasticsearchRepository<Saison, Long> {
}
