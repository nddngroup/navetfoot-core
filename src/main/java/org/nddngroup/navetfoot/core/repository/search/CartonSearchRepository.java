package org.nddngroup.navetfoot.core.repository.search;

import org.nddngroup.navetfoot.core.domain.Carton;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link Carton} entity.
 */
public interface CartonSearchRepository extends ElasticsearchRepository<Carton, Long> {
}
