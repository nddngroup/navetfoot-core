package org.nddngroup.navetfoot.core.repository;

import org.nddngroup.navetfoot.core.domain.Match;
import org.nddngroup.navetfoot.core.domain.Tir;
import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Tir entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TirRepository extends JpaRepository<Tir, Long> {
    List<Tir> findAllByMatchAndDeletedIsFalse(Match match);
    List<Tir> findAllByMatchAndEquipeAndDeletedIsFalse(Match match, Equipe equipe);
    List<Tir> findAllByMatchAndEquipeAndCadreAndDeletedIsFalse(Match match, Equipe equipe, Boolean cadre);
    Optional<Tir> findByHashcodeAndDeletedIsFalse(String hashcode);
    List<Tir> findAllByDeletedIsFalse();

}
