package org.nddngroup.navetfoot.core.repository.search;

import org.nddngroup.navetfoot.core.domain.Match;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link Match} entity.
 */
public interface MatchSearchRepository extends ElasticsearchRepository<Match, Long> {
}
