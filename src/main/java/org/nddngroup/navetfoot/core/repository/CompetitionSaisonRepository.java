package org.nddngroup.navetfoot.core.repository;

import org.nddngroup.navetfoot.core.domain.Competition;
import org.nddngroup.navetfoot.core.domain.CompetitionSaison;
import org.nddngroup.navetfoot.core.domain.Saison;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the CompetitionSaison entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CompetitionSaisonRepository extends JpaRepository<CompetitionSaison, Long> {
    List<CompetitionSaison> findAllByDeletedIsFalse();
    List<CompetitionSaison> findAllBySaisonAndDeletedIsFalse(Saison saison);
    List<CompetitionSaison> findAllBySaisonIdInAndDeletedIsFalse(List<Long> saisonIds);

    List<CompetitionSaison> findAllBySaisonAndCompetitionAndDeletedIsFalse(Saison saison, Competition competition);

    Optional<CompetitionSaison> findByHashcodeAndDeletedIsFalse(String hashcode);

    List<CompetitionSaison> findAllByCompetitionAndDeletedIsFalse(Competition competition);
}
