package org.nddngroup.navetfoot.core.repository.search;

import org.nddngroup.navetfoot.core.domain.DetailPoule;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link DetailPoule} entity.
 */
public interface DetailPouleSearchRepository extends ElasticsearchRepository<DetailPoule, Long> {
}
