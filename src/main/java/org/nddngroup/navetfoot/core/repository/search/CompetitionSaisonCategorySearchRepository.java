package org.nddngroup.navetfoot.core.repository.search;

import org.nddngroup.navetfoot.core.domain.CompetitionSaisonCategory;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link CompetitionSaisonCategory} entity.
 */
public interface CompetitionSaisonCategorySearchRepository extends ElasticsearchRepository<CompetitionSaisonCategory, Long> {
}
