package org.nddngroup.navetfoot.core.repository;

import org.nddngroup.navetfoot.core.domain.EtapeCompetition;
import org.nddngroup.navetfoot.core.domain.Poule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Poule entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PouleRepository extends JpaRepository<Poule, Long> {
    List<Poule> findAllByDeletedIsFalse();
    List<Poule> findAllByEtapeCompetitionAndDeletedIsFalse(EtapeCompetition etapeCompetition);
    Optional<Poule> findByHashcodeAndDeletedIsFalse(String hashcode);
}
