package org.nddngroup.navetfoot.core.repository;

import org.nddngroup.navetfoot.core.domain.Joueur;
import org.nddngroup.navetfoot.core.domain.JoueurCategory;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the JoueurCategory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface JoueurCategoryRepository extends JpaRepository<JoueurCategory, Long> {
    List<JoueurCategory> findAllByDeletedIsFalse();
    Optional<JoueurCategory> findByJoueurAndDeletedIsFalse(Joueur joueur);
}
