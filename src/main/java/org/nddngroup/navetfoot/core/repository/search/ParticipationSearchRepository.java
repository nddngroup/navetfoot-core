package org.nddngroup.navetfoot.core.repository.search;

import org.nddngroup.navetfoot.core.domain.Participation;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link Participation} entity.
 */
public interface ParticipationSearchRepository extends ElasticsearchRepository<Participation, Long> {
}
