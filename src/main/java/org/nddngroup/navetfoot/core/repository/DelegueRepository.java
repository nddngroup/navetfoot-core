package org.nddngroup.navetfoot.core.repository;

import org.nddngroup.navetfoot.core.domain.Delegue;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Delegue entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DelegueRepository extends JpaRepository<Delegue, Long> {
    List<Delegue> findAllByDeletedIsFalse();
}
