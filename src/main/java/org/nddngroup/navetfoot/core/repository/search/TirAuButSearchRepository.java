package org.nddngroup.navetfoot.core.repository.search;

import org.nddngroup.navetfoot.core.domain.TirAuBut;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link TirAuBut} entity.
 */
public interface TirAuButSearchRepository extends ElasticsearchRepository<TirAuBut, Long> {
}
