package org.nddngroup.navetfoot.core.repository;

import org.nddngroup.navetfoot.core.domain.Association;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Association entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AssociationRepository extends JpaRepository<Association, Long> {
    List<Association> findAllByDeletedIsFalse();
    Page<Association> findAllByIdInAndDeletedIsFalse(Pageable pageable, List<Long> ids);
    List<Association> findAllByIdInAndDeletedIsFalse(List<Long> ids);
    List<Association> findAllByNomAndDeletedIsFalse(String nom);
    Page<Association> findAllByNomContainingIgnoreCaseAndDeletedIsFalse(String nom, Pageable pageable);
    Optional<Association> findByHashcodeAndDeletedIsFalse(String hashcode);
    Optional<Association> findByNomAndAdresseAndDeletedIsFalse(String nom, String adresse);
}
