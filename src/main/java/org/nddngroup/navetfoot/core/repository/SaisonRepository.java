package org.nddngroup.navetfoot.core.repository;

import org.nddngroup.navetfoot.core.domain.Organisation;
import org.nddngroup.navetfoot.core.domain.Saison;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Saison entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SaisonRepository extends JpaRepository<Saison, Long> {
    Page<Saison> findAllByOrganisationAndDeletedIsFalse(Pageable pageable, Organisation organisation);
    List<Saison> findAllByDeletedIsFalse();
    List<Saison> findAllByOrganisationAndDeletedIsFalse(Organisation organisation);
    List<Saison> findAllByOrganisationAndCreatedAtBeforeAndDeletedIsFalse(Organisation organisation, ZonedDateTime createdAt);
    Optional<Saison> findByHashcodeAndDeletedIsFalse(String hashcode);
    Optional<Saison> findOneByOrganisationAndLibelleAndDeletedIsFalse(Organisation organisation, String libelle);
    Page<Saison> findAllByIdInAndDeletedIsFalse(Pageable pageable, List<Long> ids);

}
