package org.nddngroup.navetfoot.core.repository.search;

import org.nddngroup.navetfoot.core.domain.Tir;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link Tir} entity.
 */
public interface TirSearchRepository extends ElasticsearchRepository<Tir, Long> {
}
