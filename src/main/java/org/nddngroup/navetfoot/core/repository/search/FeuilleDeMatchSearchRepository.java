package org.nddngroup.navetfoot.core.repository.search;

import org.nddngroup.navetfoot.core.domain.FeuilleDeMatch;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link FeuilleDeMatch} entity.
 */
public interface FeuilleDeMatchSearchRepository extends ElasticsearchRepository<FeuilleDeMatch, Long> {
}
