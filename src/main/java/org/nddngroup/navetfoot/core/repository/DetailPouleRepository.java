package org.nddngroup.navetfoot.core.repository;

import org.nddngroup.navetfoot.core.domain.Association;
import org.nddngroup.navetfoot.core.domain.DetailPoule;
import org.nddngroup.navetfoot.core.domain.Poule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the DetailPoule entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DetailPouleRepository extends JpaRepository<DetailPoule, Long> {
    List<DetailPoule> findAllByDeletedIsFalse();
    List<DetailPoule> findAllByPouleAndDeletedIsFalse(Poule poule);
    Optional<DetailPoule> findByHashcodeAndDeletedIsFalse(String hashcode);
    Optional<DetailPoule> findAllByPouleAndAssociationAndJourneeAndDeletedIsFalse(Poule poule, Association association, int journee);
    List<DetailPoule> findAllByPouleIdInAndAssociationAndDeletedIsFalse(List<Long> pouleIds, Association association);
    List<DetailPoule> findAllByPouleAndJourneeGreaterThanAndDeletedIsFalse(Poule poule, int totalJournee);
}
