package org.nddngroup.navetfoot.core.repository;

import org.nddngroup.navetfoot.core.domain.Association;
import org.nddngroup.navetfoot.core.domain.EtapeCompetition;
import org.nddngroup.navetfoot.core.domain.Match;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Match entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MatchRepository extends JpaRepository<Match, Long> {
    List<Match> findAllByEtapeCompetitionAndDeletedIsFalseOrderByDateMatchAsc(EtapeCompetition etapeCompetition);
    List<Match> findAllByEtapeCompetitionAndDateMatchEqualsAndDeletedIsFalseOrderByDateMatchAsc(EtapeCompetition etapeCompetition, LocalDate dateMatch);
    Page<Match> findAllByIdInAndDeletedIsFalse(Pageable pageable, List<Long> ids);
    Page<Match> findAllByLocalIdInOrVisiteurIdInAndDeletedIsFalseAndDateMatchAfterOrderByDateMatchAsc(Pageable pageable, List<Long> ids, List<Long> clubIds, LocalDate startDate);
    Page<Match> findAllByLocalIdInOrVisiteurIdInAndDeletedIsFalseOrderByDateMatchAsc (Pageable pageable, List<Long> ids, List<Long> clubIds);
    Page<Match> findAllByDeletedIsFalseOrderByDateMatchAsc (Pageable pageable);
    List<Match> findAllByEtapeCompetitionAndLocalAndDeletedIsFalseOrderByDateMatchAsc(EtapeCompetition etapeCompetition, Association association);
    List<Match> findAllByEtapeCompetitionAndVisiteurAndDeletedIsFalseOrderByDateMatchAsc(EtapeCompetition etapeCompetition, Association association);
    List<Match> findAllByEtapeCompetitionAndJourneeAndDeletedIsFalseOrderByDateMatchAsc(EtapeCompetition etapeCompetition, Integer journee);
    Optional<Match> findByHashcodeAndDeletedIsFalseOrderByDateMatchAsc(String hashcode);
    List<Match> findAllByDeletedIsFalse();
    List<Match> findAllByEtapeCompetitionInAndDeletedIsFalse(List<EtapeCompetition> etapeCompetitions);
}
