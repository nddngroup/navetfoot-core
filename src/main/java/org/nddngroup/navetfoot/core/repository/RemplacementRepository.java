package org.nddngroup.navetfoot.core.repository;

import org.nddngroup.navetfoot.core.domain.Match;
import org.nddngroup.navetfoot.core.domain.Remplacement;
import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Remplacement entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RemplacementRepository extends JpaRepository<Remplacement, Long> {
    List<Remplacement> findAllByDeletedIsFalse();
    List<Remplacement> findAllByMatchAndDeletedIsFalse(Match match);
    List<Remplacement> findAllByMatchAndEquipeAndDeletedIsFalse(Match match, Equipe equipe);
    Optional<Remplacement> findByHashcodeAndDeletedIsFalse(String hashcode);

}
