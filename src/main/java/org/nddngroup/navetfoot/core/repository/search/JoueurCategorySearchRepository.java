package org.nddngroup.navetfoot.core.repository.search;

import org.nddngroup.navetfoot.core.domain.JoueurCategory;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link JoueurCategory} entity.
 */
public interface JoueurCategorySearchRepository extends ElasticsearchRepository<JoueurCategory, Long> {
}
