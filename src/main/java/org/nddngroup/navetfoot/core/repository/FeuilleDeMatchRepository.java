package org.nddngroup.navetfoot.core.repository;

import org.nddngroup.navetfoot.core.domain.FeuilleDeMatch;
import org.nddngroup.navetfoot.core.domain.Match;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the FeuilleDeMatch entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FeuilleDeMatchRepository extends JpaRepository<FeuilleDeMatch, Long> {
    List<FeuilleDeMatch> findAllByDeletedIsFalse();
    Optional<FeuilleDeMatch> findOneByMatchAndDeletedIsFalse(Match match);
    Optional<FeuilleDeMatch> findByHashcodeAndDeletedIsFalse(String hashcode);

}
