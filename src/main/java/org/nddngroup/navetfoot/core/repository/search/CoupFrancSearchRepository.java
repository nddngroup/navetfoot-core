package org.nddngroup.navetfoot.core.repository.search;

import org.nddngroup.navetfoot.core.domain.CoupFranc;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link CoupFranc} entity.
 */
public interface CoupFrancSearchRepository extends ElasticsearchRepository<CoupFranc, Long> {
}
