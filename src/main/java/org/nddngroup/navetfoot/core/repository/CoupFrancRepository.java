package org.nddngroup.navetfoot.core.repository;

import org.nddngroup.navetfoot.core.domain.CoupFranc;
import org.nddngroup.navetfoot.core.domain.Match;
import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the CoupFranc entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CoupFrancRepository extends JpaRepository<CoupFranc, Long> {
    List<CoupFranc> findAllByDeletedIsFalse();
    List<CoupFranc> findAllByMatchAndDeletedIsFalse(Match match);
    List<CoupFranc> findAllByMatchAndEquipeAndDeletedIsFalse(Match match, Equipe equipe);
    Optional<CoupFranc> findByHashcodeAndDeletedIsFalse(String hashcode);

}
