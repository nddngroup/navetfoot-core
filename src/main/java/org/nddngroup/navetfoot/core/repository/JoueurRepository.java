package org.nddngroup.navetfoot.core.repository;

import org.nddngroup.navetfoot.core.domain.Joueur;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Joueur entity.
 */
@SuppressWarnings("unused")
@Repository
public interface JoueurRepository extends JpaRepository<Joueur, Long> {
    List<Joueur> findAllByDeletedIsFalse();
    Optional<Joueur> findByIdentifiantAndDeletedIsFalse(String license);
    Page<Joueur> findAllByIdInAndDeletedIsFalse(List<Long> joueurIds, Pageable pageable);
    Optional<Joueur> findByHashcodeAndDeletedIsFalse(String hashcode);
}
