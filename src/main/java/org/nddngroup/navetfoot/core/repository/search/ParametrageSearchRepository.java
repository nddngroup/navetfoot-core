package org.nddngroup.navetfoot.core.repository.search;

import org.nddngroup.navetfoot.core.domain.Parametrage;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link Parametrage} entity.
 */
public interface ParametrageSearchRepository extends ElasticsearchRepository<Parametrage, Long> {
}
