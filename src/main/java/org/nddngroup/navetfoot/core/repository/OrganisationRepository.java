package org.nddngroup.navetfoot.core.repository;

import org.nddngroup.navetfoot.core.domain.Organisation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Organisation entity.
 */
@Repository
public interface OrganisationRepository extends JpaRepository<Organisation, Long> {

    @Query(value = "select distinct organisation from Organisation organisation left join fetch organisation.associations",
        countQuery = "select count(distinct organisation) from Organisation organisation")
    Page<Organisation> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct organisation from Organisation organisation left join fetch organisation.associations")
    List<Organisation> findAllWithEagerRelationships();

    @Query("select organisation from Organisation organisation left join fetch organisation.associations where organisation.id =:id")
    Optional<Organisation> findOneWithEagerRelationships(@Param("id") Long id);

    List<Organisation> findAllByDeletedIsFalse();
    List<Organisation> findAllByUserId(Long userId);
    List<Organisation> findAllOrganisationByDeletedIsFalse();
    Page<Organisation> findAllByDeletedIsFalse(Pageable pageable);
    Optional<Organisation> findByHashcodeAndDeletedIsFalse(String hashcode);
}
