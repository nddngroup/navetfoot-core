package org.nddngroup.navetfoot.core.repository.search;

import org.nddngroup.navetfoot.core.domain.But;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link But} entity.
 */
public interface ButSearchRepository extends ElasticsearchRepository<But, Long> {
}
