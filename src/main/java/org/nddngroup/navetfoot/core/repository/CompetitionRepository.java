package org.nddngroup.navetfoot.core.repository;

import org.nddngroup.navetfoot.core.domain.Competition;
import org.nddngroup.navetfoot.core.domain.Organisation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the Competition entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CompetitionRepository extends JpaRepository<Competition, Long> {
    Page<Competition> findAllByOrganisationAndDeletedIsFalse(Pageable pageable, Organisation organisation);
    List<Competition> findAllByOrganisationAndDeletedIsFalse(Organisation organisation);
    Page<Competition> findAllByIdInAndDeletedIsFalse(Pageable pageable, List<Long> ids);
    List<Competition> findAllByIdInAndDeletedIsFalse(List<Long> ids);
    Optional<Competition> findByHashcodeAndDeletedIsFalse(String hashcode);
    Page<Competition> findAllByDeletedIsFalse(Pageable pageable);
    List<Competition> findAllByDeletedIsFalse();
    List<Competition> findAllByNomContainingIgnoreCaseAndDeletedIsFalse(String nom);
    Optional<Competition> findOneByOrganisationAndNomAndDeletedIsFalse(Organisation organisation, String nom);
}
