package org.nddngroup.navetfoot.core.repository;

import org.nddngroup.navetfoot.core.domain.Category;
import org.nddngroup.navetfoot.core.domain.CompetitionSaison;
import org.nddngroup.navetfoot.core.domain.CompetitionSaisonCategory;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the CompetitionSaisonCategory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CompetitionSaisonCategoryRepository extends JpaRepository<CompetitionSaisonCategory, Long> {
    List<CompetitionSaisonCategory> findAllByDeletedIsFalse();
    List<CompetitionSaisonCategory> findAllByCompetitionSaisonAndDeletedIsFalse(CompetitionSaison competitionSaison);
    Optional<CompetitionSaisonCategory> findAllByCompetitionSaisonAndCategoryAndDeletedIsFalse(CompetitionSaison competitionSaison, Category category);
}
