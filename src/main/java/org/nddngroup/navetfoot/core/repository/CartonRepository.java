package org.nddngroup.navetfoot.core.repository;

import org.nddngroup.navetfoot.core.domain.Carton;
import org.nddngroup.navetfoot.core.domain.Joueur;
import org.nddngroup.navetfoot.core.domain.Match;
import org.nddngroup.navetfoot.core.domain.enumeration.Couleur;
import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Carton entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CartonRepository extends JpaRepository<Carton, Long> {
    List<Carton> findAllByDeletedIsFalse();
    List<Carton> findAllByMatchAndDeletedIsFalse(Match match);
    List<Carton> findAllByMatchAndJoueurAndCouleurAndDeletedIsFalse(Match match, Joueur joueur, Couleur couleur);
    List<Carton> findAllByMatchAndCouleurAndDeletedIsFalse(Match match, Couleur couleur);
    List<Carton> findAllByMatchAndEquipeAndDeletedIsFalse(Match match, Equipe equipe);
    List<Carton> findAllByMatchAndEquipeAndCouleurAndDeletedIsFalse(Match match, Equipe equipe, Couleur couleur);
    Optional<Carton> findByHashcodeAndDeletedIsFalse(String hashcode);

}
