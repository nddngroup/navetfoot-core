package org.nddngroup.navetfoot.core.repository;

import org.nddngroup.navetfoot.core.domain.HorsJeu;
import org.nddngroup.navetfoot.core.domain.Match;
import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the HorsJeu entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HorsJeuRepository extends JpaRepository<HorsJeu, Long> {
    List<HorsJeu> findAllByDeletedIsFalse();
    List<HorsJeu> findAllByMatchAndDeletedIsFalse(Match match);
    List<HorsJeu> findAllByMatchAndEquipeAndDeletedIsFalse(Match match, Equipe equipe);
    Optional<HorsJeu> findByHashcodeAndDeletedIsFalse(String hashcode);

}
