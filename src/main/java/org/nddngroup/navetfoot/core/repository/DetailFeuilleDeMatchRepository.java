package org.nddngroup.navetfoot.core.repository;

import org.nddngroup.navetfoot.core.domain.DetailFeuilleDeMatch;
import org.nddngroup.navetfoot.core.domain.FeuilleDeMatch;
import org.nddngroup.navetfoot.core.domain.Joueur;
import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the DetailFeuilleDeMatch entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DetailFeuilleDeMatchRepository extends JpaRepository<DetailFeuilleDeMatch, Long> {
    List<DetailFeuilleDeMatch> findAllByDeletedIsFalse();
    List<DetailFeuilleDeMatch> findAllByFeuilleDeMatchAndDeletedIsFalse(FeuilleDeMatch feuilleDeMatch);
    Optional<DetailFeuilleDeMatch> findAllByFeuilleDeMatchAndJoueurAndDeletedIsFalse(FeuilleDeMatch feuilleDeMatch, Joueur joueur);
    List<DetailFeuilleDeMatch> findAllByFeuilleDeMatchAndEquipeAndDeletedIsFalse(FeuilleDeMatch feuilleDeMatch, Equipe equipe);
    Optional<DetailFeuilleDeMatch> findByHashcodeAndDeletedIsFalse(String hashcode);
    List<DetailFeuilleDeMatch> findAllByHashcode(String hashcode);
}
