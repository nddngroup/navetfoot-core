package org.nddngroup.navetfoot.core.repository.search;

import org.nddngroup.navetfoot.core.domain.Contrat;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link Contrat} entity.
 */
public interface ContratSearchRepository extends ElasticsearchRepository<Contrat, Long> {
}
