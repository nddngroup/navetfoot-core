package org.nddngroup.navetfoot.core.repository.search;

import org.nddngroup.navetfoot.core.domain.Poule;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link Poule} entity.
 */
public interface PouleSearchRepository extends ElasticsearchRepository<Poule, Long> {
}
