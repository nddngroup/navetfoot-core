package org.nddngroup.navetfoot.core.repository;

import org.nddngroup.navetfoot.core.domain.Affiliation;
import org.nddngroup.navetfoot.core.domain.Association;
import org.nddngroup.navetfoot.core.domain.Organisation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Affiliation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AffiliationRepository extends JpaRepository<Affiliation, Long> {
    List<Affiliation> findAllByDeletedIsFalse();
    Optional<Affiliation> findOneByOrganisationAndAssociationAndDeletedIsFalse(Organisation organisation, Association association);
    List<Affiliation> findAllByOrganisationAndDeletedIsFalse(Organisation organisation);
    List<Affiliation> findAllByAssociationAndDeletedIsFalse(Association association);
    Optional<Affiliation> findByHashcodeAndDeletedIsFalse(String hashcode);
}
