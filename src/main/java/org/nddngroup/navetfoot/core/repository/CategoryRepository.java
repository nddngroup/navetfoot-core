package org.nddngroup.navetfoot.core.repository;

import org.nddngroup.navetfoot.core.domain.Category;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Category entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {
    Page<Category> findAllByDeletedIsFalse(Pageable pageable);
    List<Category> findAllByDeletedIsFalse();
}
