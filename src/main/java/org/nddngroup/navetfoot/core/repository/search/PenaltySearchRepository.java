package org.nddngroup.navetfoot.core.repository.search;

import org.nddngroup.navetfoot.core.domain.Penalty;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link Penalty} entity.
 */
public interface PenaltySearchRepository extends ElasticsearchRepository<Penalty, Long> {
}
