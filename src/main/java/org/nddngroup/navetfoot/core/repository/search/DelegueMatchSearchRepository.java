package org.nddngroup.navetfoot.core.repository.search;

import org.nddngroup.navetfoot.core.domain.DelegueMatch;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link DelegueMatch} entity.
 */
public interface DelegueMatchSearchRepository extends ElasticsearchRepository<DelegueMatch, Long> {
}
