package org.nddngroup.navetfoot.core.repository;

import org.nddngroup.navetfoot.core.domain.Match;
import org.nddngroup.navetfoot.core.domain.Penalty;
import org.nddngroup.navetfoot.core.domain.TirAuBut;
import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Penalty entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PenaltyRepository extends JpaRepository<Penalty, Long> {
    List<Penalty> findAllByTirAuButAndDeletedIsFalse(TirAuBut tirAuBut);
    List<Penalty> findAllByMatchAndDeletedIsFalse(Match match);
    List<Penalty> findAllByMatchAndEquipeAndDeletedIsFalse(Match match, Equipe equipe);
    Optional<Penalty> findByHashcodeAndDeletedIsFalse(String hashcode);
    List<Penalty> findAllByDeletedIsFalse();

}
