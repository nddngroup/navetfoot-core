package org.nddngroup.navetfoot.core.repository.search;

import org.nddngroup.navetfoot.core.domain.Corner;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link Corner} entity.
 */
public interface CornerSearchRepository extends ElasticsearchRepository<Corner, Long> {
}
