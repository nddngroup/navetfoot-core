package org.nddngroup.navetfoot.core.repository;

import org.nddngroup.navetfoot.core.domain.DelegueMatch;
import org.nddngroup.navetfoot.core.domain.Match;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the DelegueMatch entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DelegueMatchRepository extends JpaRepository<DelegueMatch, Long> {
    List<DelegueMatch> findAllByDeletedIsFalse();
    Optional<DelegueMatch> findOneByMatchAndDeletedIsFalse(Match match);
    Optional<DelegueMatch> findOneByConfirmCodeAndDeletedIsFalse(String codeMatch);
}
