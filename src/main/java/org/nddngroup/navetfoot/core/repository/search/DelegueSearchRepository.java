package org.nddngroup.navetfoot.core.repository.search;

import org.nddngroup.navetfoot.core.domain.Delegue;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link Delegue} entity.
 */
public interface DelegueSearchRepository extends ElasticsearchRepository<Delegue, Long> {
}
