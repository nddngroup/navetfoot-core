package org.nddngroup.navetfoot.core.repository;

import org.nddngroup.navetfoot.core.domain.Etape;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Spring Data  repository for the Etape entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EtapeRepository extends JpaRepository<Etape, Long> {
    Optional<Etape> findByLibelle(String libelle);
}
