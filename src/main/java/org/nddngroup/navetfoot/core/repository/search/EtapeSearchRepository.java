package org.nddngroup.navetfoot.core.repository.search;

import org.nddngroup.navetfoot.core.domain.Etape;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link Etape} entity.
 */
public interface EtapeSearchRepository extends ElasticsearchRepository<Etape, Long> {
}
