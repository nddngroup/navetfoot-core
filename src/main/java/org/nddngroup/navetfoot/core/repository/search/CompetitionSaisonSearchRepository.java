package org.nddngroup.navetfoot.core.repository.search;

import org.nddngroup.navetfoot.core.domain.CompetitionSaison;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link CompetitionSaison} entity.
 */
public interface CompetitionSaisonSearchRepository extends ElasticsearchRepository<CompetitionSaison, Long> {
}
