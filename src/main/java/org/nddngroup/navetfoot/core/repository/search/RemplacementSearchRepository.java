package org.nddngroup.navetfoot.core.repository.search;

import org.nddngroup.navetfoot.core.domain.Remplacement;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link Remplacement} entity.
 */
public interface RemplacementSearchRepository extends ElasticsearchRepository<Remplacement, Long> {
}
