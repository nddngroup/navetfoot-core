package org.nddngroup.navetfoot.core.config.rabbitMQ;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class RabbitMQConfig {
    public static final String EXCHANGE = "navetfoot";

    // ROUTING KEYS
    public static final String MATCH_LIVE_ROUTING_KEY = "match.live.*";
    public static final String STATS_ROUTING_KEY = "stats.*";

    // QUEUES
    public static final String MATCH_LIVE_QUEUE = "match-queue";
    public static final String STATS_QUEUE = "stats-queue";

    @Bean
    public MessageConverter messageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public AmqpTemplate template(ConnectionFactory connectionFactory) {
        RabbitTemplate template = new RabbitTemplate(connectionFactory);
        template.setMessageConverter(messageConverter());
        return template;
    }

    public static String formatRoutingKey(String routingKey) {
        return routingKey;
    }

    public static String formatRoutingKey(String routingKey, List<String> args) {
        if (args == null || args.size() == 0) {
            return routingKey;
        }

        StringBuilder chaines = new StringBuilder();
        for(String arg : args) {
            chaines.append((chaines.length() == 0) ? arg : "." + arg);
        }

        return routingKey.replace("*", chaines.toString());
    }
}
