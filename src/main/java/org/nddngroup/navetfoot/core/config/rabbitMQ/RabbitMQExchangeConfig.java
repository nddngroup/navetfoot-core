package org.nddngroup.navetfoot.core.config.rabbitMQ;

import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQExchangeConfig {
    @Bean
    public TopicExchange exchange() {
        return new TopicExchange(RabbitMQConfig.EXCHANGE);
    }
}
