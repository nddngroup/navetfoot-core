package org.nddngroup.navetfoot.core.config.rabbitMQ;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQQueueConfig {
    // @Bean
    public Queue matchLiveQueue() {
        return new Queue(RabbitMQConfig.MATCH_LIVE_QUEUE);
    }

    // @Bean
    public Queue statsQueue() {
        return new Queue(RabbitMQConfig.STATS_QUEUE);
    }
}
