package org.nddngroup.navetfoot.core.config;

/**
 * Application constants.
 */
public final class Constants {

    public static final String SYSTEM = "system";

    public static final String SAISON_LIBELLE_REGEX = "^20\\d{2}(-(\\d{2})||-(\\d{4}))?$";

    private Constants() {}
}
