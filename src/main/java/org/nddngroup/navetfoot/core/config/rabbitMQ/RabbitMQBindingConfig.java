package org.nddngroup.navetfoot.core.config.rabbitMQ;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQBindingConfig {
    // @Bean
    public Binding bindingMatchLive(@Qualifier("matchLiveQueue") Queue queue, TopicExchange exchange) {
        return BindingBuilder
            .bind(queue)
            .to(exchange)
            .with(RabbitMQConfig.MATCH_LIVE_ROUTING_KEY);
    }

    // @Bean
    public Binding bindingStats(@Qualifier("statsQueue") Queue queue, TopicExchange exchange) {
        return BindingBuilder
            .bind(queue)
            .to(exchange)
            .with(RabbitMQConfig.STATS_ROUTING_KEY);
    }
}
