package org.nddngroup.navetfoot.core.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

/**
 * Properties specific to Navetfoot Core.
 * <p>
 * Properties are configured in the {@code application.yml} file.
 * See {@link tech.jhipster.config.JHipsterProperties} for a good example.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {
    private final Notif notif = new Notif();
    private final Twilio twilio = new Twilio();
    private final Image image = new Image();
    private final ExternalApi externalApi = new ExternalApi();

    public Notif getNotif() {
        return notif;
    }

    public Twilio getTwilio() {
        return twilio;
    }

    public Image getImage() {
        return image;
    }
    public ExternalApi getExternalApi() {
        return externalApi;
    }

    public static class Notif {

        private String url = "";

        public String getUrl() {
            return url;
        }
        public void setUrl(String url) {
            this.url = url;
        }
    }

    public static class Twilio {

        private String accountSid;
        private String authToken;
        private String serviceSid;

        public String getAccountSid() {
            return accountSid;
        }

        public void setAccountSid(String accountSid) {
            this.accountSid = accountSid;
        }

        public String getAuthToken() {
            return authToken;
        }

        public void setAuthToken(String authToken) {
            this.authToken = authToken;
        }

        public String getServiceSid() {
            return serviceSid;
        }

        public void setServiceSid(String serviceSid) {
            this.serviceSid = serviceSid;
        }
    }

    public static class Image {
        private String baseFolder;
        private String baseUrl;

        public String getBaseFolder() {
            return baseFolder;
        }

        public void setBaseFolder(String baseFolder) {
            this.baseFolder = baseFolder;
        }

        public String getBaseUrl() {
            return baseUrl;
        }

        public void setBaseUrl(String baseUrl) {
            this.baseUrl = baseUrl;
        }
    }

    public static class ExternalApi {
        private String season;
        private int interval;
        List<String> leagueIds;

        public String getSeason() {
            return season;
        }

        public void setSeason(String season) {
            this.season = season;
        }

        public int getInterval() {
            return interval;
        }

        public void setInterval(int interval) {
            this.interval = interval;
        }

        public List<String> getLeagueIds() {
            return leagueIds;
        }

        public void setLeagueIds(List<String> leagueIds) {
            this.leagueIds = leagueIds;
        }
    }
}
