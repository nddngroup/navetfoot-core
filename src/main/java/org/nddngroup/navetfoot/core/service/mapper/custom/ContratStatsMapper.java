package org.nddngroup.navetfoot.core.service.mapper.custom;

import org.nddngroup.navetfoot.core.domain.Contrat;
import org.nddngroup.navetfoot.core.service.*;
import org.nddngroup.navetfoot.core.service.*;
import org.nddngroup.navetfoot.core.service.dto.ContratDTO;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring")
public abstract class ContratStatsMapper {
    @Autowired
    protected EtapeCompetitionService etapeCompetitionService;
    @Autowired
    protected CompetitionSaisonService competitionSaisonService;
    @Autowired
    protected SaisonService saisonService;
    @Autowired
    protected CompetitionService competitionService;
    @Autowired
    protected OrganisationService organisationService;
    @Autowired
    protected AssociationService associationService;
    @Autowired
    protected JoueurService joueurService;

    public Contrat toStats(ContratDTO contratDTO) {
        var stats = new Contrat()
            .code(contratDTO.getCode())
            .hashcode(contratDTO.getHashcode())
            .createdAt(contratDTO.getCreatedAt())
            .updatedAt(contratDTO.getUpdatedAt())
            .deleted(contratDTO.isDeleted());

        stats.setId(contratDTO.getId());

        // find association
        associationService.findOne(contratDTO.getAssociationId())
            .ifPresent(associationDTO -> {
                stats.setAssociationId(associationDTO.getId());
                stats.setAssociationNom(associationDTO.getNom());
            });

        // find joueur
        joueurService.findOne(contratDTO.getJoueurId())
            .ifPresent(joueurDTO -> {
                stats.setJoueurId(joueurDTO.getId());
                stats.setJoueurNom(joueurDTO.getNom());
                stats.setJoueurPrenom(joueurDTO.getPrenom());
                stats.setJoueurImage(joueurDTO.getImageUrl());
            });

        // find saison
        saisonService.findOne(contratDTO.getSaisonId())
            .ifPresent(saisonDTO -> {
                stats.setSaisonId(saisonDTO.getId());
                stats.setSaisonNom(saisonDTO.getLibelle());

                // find organisation
                organisationService.findOne(saisonDTO.getOrganisationId())
                    .ifPresent(organisationDTO -> {
                        stats.setOrganisationId(organisationDTO.getId());
                        stats.setOrganisationNom(organisationDTO.getNom());
                    });
            });

        return stats;
    }
}
