package org.nddngroup.navetfoot.core.service.impl;

import org.nddngroup.navetfoot.core.domain.HorsJeu;
import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;
import org.nddngroup.navetfoot.core.repository.HorsJeuRepository;
import org.nddngroup.navetfoot.core.repository.search.HorsJeuSearchRepository;
import org.nddngroup.navetfoot.core.service.HorsJeuService;
import org.nddngroup.navetfoot.core.service.dto.HorsJeuDTO;
import org.nddngroup.navetfoot.core.service.dto.MatchDTO;
import org.nddngroup.navetfoot.core.service.mapper.HorsJeuMapper;
import org.nddngroup.navetfoot.core.service.mapper.MatchMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link HorsJeu}.
 */
@Service
@Transactional
public class HorsJeuServiceImpl implements HorsJeuService {

    private final Logger log = LoggerFactory.getLogger(HorsJeuServiceImpl.class);

    private final HorsJeuRepository horsJeuRepository;

    private final HorsJeuMapper horsJeuMapper;

    private final HorsJeuSearchRepository horsJeuSearchRepository;

    @Autowired
    private MatchMapper matchMapper;

    public HorsJeuServiceImpl(HorsJeuRepository horsJeuRepository, HorsJeuMapper horsJeuMapper, HorsJeuSearchRepository horsJeuSearchRepository) {
        this.horsJeuRepository = horsJeuRepository;
        this.horsJeuMapper = horsJeuMapper;
        this.horsJeuSearchRepository = horsJeuSearchRepository;
    }

    @Override
    public HorsJeuDTO save(HorsJeuDTO horsJeuDTO) {
        log.debug("Request to save HorsJeu : {}", horsJeuDTO);

        horsJeuDTO.setUpdatedAt(ZonedDateTime.now());
        if (horsJeuDTO.getCreatedAt() == null) {
            horsJeuDTO.setCreatedAt(ZonedDateTime.now());
        }

        HorsJeu horsJeu = horsJeuMapper.toEntity(horsJeuDTO);
        horsJeu = horsJeuRepository.save(horsJeu);
        HorsJeuDTO result = horsJeuMapper.toDto(horsJeu);

        if (horsJeuDTO.isDeleted()) {
            horsJeuSearchRepository.deleteById(horsJeuDTO.getId());
        } else {
            horsJeuSearchRepository.save(horsJeuMapper.toEntity(result));
        }

        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public List<HorsJeuDTO> findAll() {
        log.debug("Request to get all HorsJeus");
        return horsJeuRepository.findAll().stream()
            .map(horsJeuMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<HorsJeuDTO> findOne(Long id) {
        log.debug("Request to get HorsJeu : {}", id);
        return horsJeuRepository.findById(id)
            .map(horsJeuMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete HorsJeu : {}", id);
        horsJeuRepository.deleteById(id);
        horsJeuSearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<HorsJeuDTO> search(String query) {
        log.debug("Request to search HorsJeus for query {}", query);
        return StreamSupport
            .stream(horsJeuSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(horsJeuMapper::toDto)
        .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<HorsJeuDTO> findByMatch(MatchDTO matchDTO) {
        log.debug("Request to get all CoupFrancs by match");
        return horsJeuRepository.findAllByMatchAndDeletedIsFalse(matchMapper.toEntity(matchDTO))
            .stream()
            .map(horsJeuMapper::toDto).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<HorsJeuDTO> findByMatchAndEquipe(MatchDTO matchDTO, Equipe equipe) {
        log.debug("Request to get all CoupFrancs by match and equipe");
        return horsJeuRepository.findAllByMatchAndEquipeAndDeletedIsFalse(matchMapper.toEntity(matchDTO), equipe)
            .stream()
            .map(horsJeuMapper::toDto).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<HorsJeuDTO> findByHashcode(String hashcode) {
        log.debug("Request to get HorsJeu : {}", hashcode);
        return horsJeuRepository.findByHashcodeAndDeletedIsFalse(hashcode)
            .map(horsJeuMapper::toDto);
    }

    @Override
    public List<HorsJeuDTO> findAllByDeletedIsFalse() {
        return horsJeuRepository.findAllByDeletedIsFalse().stream()
            .map(horsJeuMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public void reIndex() {
        log.info("Request to reindex all HorsJeus");
        horsJeuSearchRepository.deleteAll();
        findAllByDeletedIsFalse().stream().map(horsJeuMapper::toEntity).forEach(horsJeuSearchRepository::save);
    }
}
