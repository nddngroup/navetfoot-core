package org.nddngroup.navetfoot.core.service.impl;

import org.nddngroup.navetfoot.core.domain.Penalty;
import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;
import org.nddngroup.navetfoot.core.repository.PenaltyRepository;
import org.nddngroup.navetfoot.core.repository.search.PenaltySearchRepository;
import org.nddngroup.navetfoot.core.service.PenaltyService;
import org.nddngroup.navetfoot.core.service.dto.MatchDTO;
import org.nddngroup.navetfoot.core.service.dto.PenaltyDTO;
import org.nddngroup.navetfoot.core.service.dto.TirAuButDTO;
import org.nddngroup.navetfoot.core.service.mapper.MatchMapper;
import org.nddngroup.navetfoot.core.service.mapper.PenaltyMapper;
import org.nddngroup.navetfoot.core.service.mapper.TirAuButMapper;
import org.nddngroup.navetfoot.core.service.mapper.custom.PenaltyStatsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link Penalty}.
 */
@Service
@Transactional
public class PenaltyServiceImpl implements PenaltyService {

    private final Logger log = LoggerFactory.getLogger(PenaltyServiceImpl.class);

    private final PenaltyRepository penaltyRepository;

    private final PenaltyMapper penaltyMapper;

    private final PenaltySearchRepository penaltySearchRepository;
    private final PenaltyStatsMapper penaltyStatsMapper;
    @Autowired
    private TirAuButMapper tirAuButMapper;
    @Autowired
    private MatchMapper matchMapper;

    public PenaltyServiceImpl(PenaltyRepository penaltyRepository,
                              PenaltyMapper penaltyMapper,
                              PenaltySearchRepository penaltySearchRepository,
                              PenaltyStatsMapper penaltyStatsMapper
                              ) {
        this.penaltyRepository = penaltyRepository;
        this.penaltyMapper = penaltyMapper;
        this.penaltySearchRepository = penaltySearchRepository;
        this.penaltyStatsMapper = penaltyStatsMapper;
    }

    @Override
    public PenaltyDTO save(PenaltyDTO penaltyDTO) {
        log.debug("Request to save Penalty : {}", penaltyDTO);

        penaltyDTO.setUpdatedAt(ZonedDateTime.now());
        if (penaltyDTO.getCreatedAt() == null) {
            penaltyDTO.setCreatedAt(ZonedDateTime.now());
        }

        Penalty penalty = penaltyMapper.toEntity(penaltyDTO);
        penalty = penaltyRepository.save(penalty);
        PenaltyDTO result = penaltyMapper.toDto(penalty);

        if (penaltyDTO.isDeleted()) {
            penaltySearchRepository.deleteById(result.getId());
        } else {
            penaltySearchRepository.save(penaltyStatsMapper.toStats(result));
        }

        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<PenaltyDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Penalties");
        return penaltyRepository.findAll(pageable)
            .map(penaltyMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<PenaltyDTO> findOne(Long id) {
        log.debug("Request to get Penalty : {}", id);
        return penaltyRepository.findById(id)
            .map(penaltyMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Penalty : {}", id);
        penaltyRepository.deleteById(id);
        penaltySearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<PenaltyDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Penalties for query {}", query);
        return penaltySearchRepository.search(queryStringQuery(query), pageable)
            .map(penaltyMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<PenaltyDTO> findByTirAuBut(TirAuButDTO tirAuButDTO) {
        log.debug("Request to get all Penalties");
        return penaltyRepository.findAllByTirAuButAndDeletedIsFalse(tirAuButMapper.toEntity(tirAuButDTO))
            .stream()
            .map(penaltyMapper::toDto).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<PenaltyDTO> findByMatch(MatchDTO matchDTO) {
        log.debug("Request to get all Penalties by match");
        return penaltyRepository.findAllByMatchAndDeletedIsFalse(matchMapper.toEntity(matchDTO))
            .stream()
            .map(penaltyMapper::toDto).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<PenaltyDTO> findByMatchAndEquipe(MatchDTO matchDTO, Equipe equipe) {
        log.debug("Request to get all Penalties by match and equipe");
        return penaltyRepository.findAllByMatchAndEquipeAndDeletedIsFalse(matchMapper.toEntity(matchDTO), equipe)
            .stream()
            .map(penaltyMapper::toDto).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<PenaltyDTO> findByHashcode(String hashcode) {
        log.debug("Request to get Penalty : {}", hashcode);
        return penaltyRepository.findByHashcodeAndDeletedIsFalse(hashcode)
            .map(penaltyMapper::toDto);
    }

    @Override
    public List<PenaltyDTO> findAll() {
        return penaltyRepository.findAllByDeletedIsFalse().stream()
            .map(penaltyMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public void reIndex() {
        log.info("Request to reindex all Penalty");
        penaltySearchRepository.deleteAll();
        findAll().stream().map(penaltyStatsMapper::toStats).forEach(penaltySearchRepository::save);
    }
}
