package org.nddngroup.navetfoot.core.service.custom;

import org.nddngroup.navetfoot.core.service.DetailFeuilleDeMatchService;
import org.nddngroup.navetfoot.core.service.dto.DetailFeuilleDeMatchDTO;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class SyncCheck <C, S> {
    public C alreadySynchronized(C toSync, S service) {
        //  CHECK DETAIL_FEUILLE_DE_MATCH
        if (toSync instanceof DetailFeuilleDeMatchDTO) {
            // find by hashcode
            Optional<DetailFeuilleDeMatchDTO> byHashcodeOptionnal;
            try {
                byHashcodeOptionnal = ((DetailFeuilleDeMatchService) service).findByHashcode(((DetailFeuilleDeMatchDTO) toSync).getHashcode());
            } catch (IncorrectResultSizeDataAccessException e) {
                // remove all
                ((DetailFeuilleDeMatchService) service).removeAll(((DetailFeuilleDeMatchDTO) toSync).getHashcode());
                byHashcodeOptionnal = Optional.empty();
            }

            if (byHashcodeOptionnal.isPresent()) {
                DetailFeuilleDeMatchDTO byHashcode = byHashcodeOptionnal.get();

                Integer newNumJoueur = ((DetailFeuilleDeMatchDTO) toSync).getNumeroJoueur();
                Integer oldNumJoueur = byHashcode.getNumeroJoueur();


                if (((DetailFeuilleDeMatchDTO) toSync).getId() == null) {
                    ((DetailFeuilleDeMatchDTO) toSync).setId(byHashcode.getId());
                }

                if (newNumJoueur == null && oldNumJoueur != null) {
                    return toSync;
                } else if (newNumJoueur != null && !newNumJoueur.equals(oldNumJoueur)) {
                    return toSync;
                } else if (((DetailFeuilleDeMatchDTO) toSync).isDeleted()) {
                    return toSync;
                } else {
                    return null;
                }
            }
        }

        return toSync;
    }
}
