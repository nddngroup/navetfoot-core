package org.nddngroup.navetfoot.core.service.mapper;


import org.nddngroup.navetfoot.core.domain.*;
import org.nddngroup.navetfoot.core.domain.TirAuBut;
import org.nddngroup.navetfoot.core.service.dto.TirAuButDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link TirAuBut} and its DTO {@link TirAuButDTO}.
 */
@Mapper(componentModel = "spring", uses = {MatchMapper.class})
public interface TirAuButMapper extends EntityMapper<TirAuButDTO, TirAuBut> {

    @Mapping(source = "match.id", target = "matchId")
    TirAuButDTO toDto(TirAuBut tirAuBut);

    @Mapping(source = "matchId", target = "match")
    @Mapping(target = "penalties", ignore = true)
    @Mapping(target = "removePenalties", ignore = true)
    TirAuBut toEntity(TirAuButDTO tirAuButDTO);

    default TirAuBut fromId(Long id) {
        if (id == null) {
            return null;
        }
        TirAuBut tirAuBut = new TirAuBut();
        tirAuBut.setId(id);
        return tirAuBut;
    }
}
