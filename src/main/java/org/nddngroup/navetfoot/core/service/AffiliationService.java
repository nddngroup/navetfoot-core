package org.nddngroup.navetfoot.core.service;

import org.nddngroup.navetfoot.core.domain.Affiliation;
import org.nddngroup.navetfoot.core.service.dto.AffiliationDTO;
import org.nddngroup.navetfoot.core.service.dto.AssociationDTO;
import org.nddngroup.navetfoot.core.service.dto.OrganisationDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Affiliation}.
 */
public interface AffiliationService {

    /**
     * Save a affiliation.
     *
     * @param affiliationDTO the entity to save.
     * @return the persisted entity.
     */
    AffiliationDTO save(AffiliationDTO affiliationDTO);

    /**
     * Get all the affiliations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<AffiliationDTO> findAll(Pageable pageable);
    List<AffiliationDTO> findAll();


    /**
     * Get the "id" affiliation.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<AffiliationDTO> findOne(Long id);

    /**
     * Delete the "id" affiliation.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the affiliation corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<AffiliationDTO> search(String query, Pageable pageable);

    Optional<AffiliationDTO> findByOrganisationAndAssociation(OrganisationDTO organisationDTO, AssociationDTO associationDTO);
    List<AffiliationDTO> findAllByOrganisation(OrganisationDTO organisationDTO);
    Optional<AffiliationDTO> findByHashcode(String hashcode);
    AffiliationDTO save(OrganisationDTO organisationDTO, AssociationDTO associationDTO);
    List<AffiliationDTO> findByAssociation(AssociationDTO associationDTO);
    void reIndex();

}
