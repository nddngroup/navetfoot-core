package org.nddngroup.navetfoot.core.service.impl;

import org.nddngroup.navetfoot.core.domain.TirAuBut;
import org.nddngroup.navetfoot.core.repository.TirAuButRepository;
import org.nddngroup.navetfoot.core.repository.search.TirAuButSearchRepository;
import org.nddngroup.navetfoot.core.service.TirAuButService;
import org.nddngroup.navetfoot.core.service.dto.MatchDTO;
import org.nddngroup.navetfoot.core.service.dto.TirAuButDTO;
import org.nddngroup.navetfoot.core.service.mapper.MatchMapper;
import org.nddngroup.navetfoot.core.service.mapper.TirAuButMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link TirAuBut}.
 */
@Service
@Transactional
public class TirAuButServiceImpl implements TirAuButService {

    private final Logger log = LoggerFactory.getLogger(TirAuButServiceImpl.class);

    private final TirAuButRepository tirAuButRepository;

    private final TirAuButMapper tirAuButMapper;

    private final TirAuButSearchRepository tirAuButSearchRepository;
    @Autowired
    private MatchMapper matchMapper;

    public TirAuButServiceImpl(TirAuButRepository tirAuButRepository, TirAuButMapper tirAuButMapper, TirAuButSearchRepository tirAuButSearchRepository) {
        this.tirAuButRepository = tirAuButRepository;
        this.tirAuButMapper = tirAuButMapper;
        this.tirAuButSearchRepository = tirAuButSearchRepository;
    }

    @Override
    public TirAuButDTO save(TirAuButDTO tirAuButDTO) {
        log.debug("Request to save TirAuBut : {}", tirAuButDTO);

        tirAuButDTO.setUpdatedAt(ZonedDateTime.now());
        if (tirAuButDTO.getCreatedAt() == null) {
            tirAuButDTO.setCreatedAt(ZonedDateTime.now());
        }

        TirAuBut tirAuBut = tirAuButMapper.toEntity(tirAuButDTO);
        tirAuBut = tirAuButRepository.save(tirAuBut);
        TirAuButDTO result = tirAuButMapper.toDto(tirAuBut);

        if (tirAuButDTO.isDeleted()) {
            tirAuButSearchRepository.deleteById(tirAuButDTO.getId());
        } else {
            tirAuButSearchRepository.save(tirAuButMapper.toEntity(result));
        }

        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<TirAuButDTO> findAll(Pageable pageable) {
        log.debug("Request to get all TirAuButs");
        return tirAuButRepository.findAll(pageable)
            .map(tirAuButMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<TirAuButDTO> findOne(Long id) {
        log.debug("Request to get TirAuBut : {}", id);
        return tirAuButRepository.findById(id)
            .map(tirAuButMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete TirAuBut : {}", id);
        tirAuButRepository.deleteById(id);
        tirAuButSearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<TirAuButDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of TirAuButs for query {}", query);
        return tirAuButSearchRepository.search(queryStringQuery(query), pageable)
            .map(tirAuButMapper::toDto);
    }

    @Override
    public Optional<TirAuButDTO> findOneByMatch(MatchDTO matchDTO) {
        log.debug("Request to get TirAuBut : {}", matchDTO.getId());
        return tirAuButRepository.findOneByMatchAndDeletedIsFalse(matchMapper.toEntity(matchDTO))
            .map(tirAuButMapper::toDto);
    }

    @Override
    public Optional<TirAuButDTO> findByHashcode(String hashcode) {
        log.debug("Request to get tirAuBut by hashcode: {}", hashcode);
        return tirAuButRepository.findByHashcodeAndDeletedIsFalse(hashcode)
            .map(tirAuButMapper::toDto);
    }

    @Override
    public List<TirAuButDTO> findAll() {
        return tirAuButRepository.findAllByDeletedIsFalse().stream()
            .map(tirAuButMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public void reIndex() {
        log.info("Request to reindex all TirAuButs");
        tirAuButSearchRepository.deleteAll();
        findAll().stream().map(tirAuButMapper::toEntity).forEach(tirAuButSearchRepository::save);
    }
}
