package org.nddngroup.navetfoot.core.service.client;

import org.nddngroup.navetfoot.core.domain.custom.Abonnement;
import org.nddngroup.navetfoot.core.domain.custom.NotificationObject;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "navetfoot-notif")
public interface NotifApiClient {
    @PostMapping("/api/events")
    ResponseEntity<Void> sendPushNotification(
        @RequestParam(name = "deviceId", required = false) String deviceId,
        @RequestBody NotificationObject notificationObject
    );

    @PostMapping("/api/abonnements")
    ResponseEntity<Abonnement> addAbonnement(@RequestBody Abonnement abonnement);
}
