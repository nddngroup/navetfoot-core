package org.nddngroup.navetfoot.core.service.dto;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;

import org.nddngroup.navetfoot.core.domain.Match;
import org.nddngroup.navetfoot.core.domain.enumeration.EtatMatch;

/**
 * A DTO for the {@link Match} entity.
 */
public class MatchDTO implements Serializable {

    private Long id;

    private String coupDEnvoi;

    private String hashcode;

    private ZonedDateTime createdAt;

    private ZonedDateTime updatedAt;

    private Boolean deleted;

    private LocalDate dateMatch;

    private EtatMatch etatMatch;

    @NotNull
    private String code;

    private Integer journee;

    private String chrono;


    private Long visiteurId;

    private Long localId;

    private Long etapeCompetitionId;

    private String etapeCompetitionNom;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCoupDEnvoi() {
        return coupDEnvoi;
    }

    public void setCoupDEnvoi(String coupDEnvoi) {
        this.coupDEnvoi = coupDEnvoi;
    }

    public String getHashcode() {
        return hashcode;
    }

    public void setHashcode(String hashcode) {
        this.hashcode = hashcode;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public LocalDate getDateMatch() {
        return dateMatch;
    }

    public void setDateMatch(LocalDate dateMatch) {
        this.dateMatch = dateMatch;
    }

    public EtatMatch getEtatMatch() {
        return etatMatch;
    }

    public void setEtatMatch(EtatMatch etatMatch) {
        this.etatMatch = etatMatch;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getJournee() {
        return journee;
    }

    public void setJournee(Integer journee) {
        this.journee = journee;
    }

    public String getChrono() {
        return chrono;
    }

    public void setChrono(String chrono) {
        this.chrono = chrono;
    }

    public Long getVisiteurId() {
        return visiteurId;
    }

    public void setVisiteurId(Long associationId) {
        this.visiteurId = associationId;
    }

    public Long getLocalId() {
        return localId;
    }

    public void setLocalId(Long associationId) {
        this.localId = associationId;
    }

    public Long getEtapeCompetitionId() {
        return etapeCompetitionId;
    }

    public void setEtapeCompetitionId(Long etapeCompetitionId) {
        this.etapeCompetitionId = etapeCompetitionId;
    }

    public String getEtapeCompetitionNom() {
        return etapeCompetitionNom;
    }

    public void setEtapeCompetitionNom(String etapeCompetitionNom) {
        this.etapeCompetitionNom = etapeCompetitionNom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MatchDTO)) {
            return false;
        }

        return id != null && id.equals(((MatchDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MatchDTO{" +
            "id=" + getId() +
            ", coupDEnvoi='" + getCoupDEnvoi() + "'" +
            ", hashcode='" + getHashcode() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", deleted='" + isDeleted() + "'" +
            ", dateMatch='" + getDateMatch() + "'" +
            ", etatMatch='" + getEtatMatch() + "'" +
            ", code='" + getCode() + "'" +
            ", journee=" + getJournee() +
            ", chrono='" + getChrono() + "'" +
            ", visiteurId=" + getVisiteurId() +
            ", localId=" + getLocalId() +
            ", etapeCompetitionId=" + getEtapeCompetitionId() +
            ", etapeCompetitionNom='" + getEtapeCompetitionNom() + "'" +
            "}";
    }
}
