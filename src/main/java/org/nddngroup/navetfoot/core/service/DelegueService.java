package org.nddngroup.navetfoot.core.service;

import org.nddngroup.navetfoot.core.domain.Delegue;
import org.nddngroup.navetfoot.core.service.dto.DelegueDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Delegue}.
 */
public interface DelegueService {

    /**
     * Save a delegue.
     *
     * @param delegueDTO the entity to save.
     * @return the persisted entity.
     */
    DelegueDTO save(DelegueDTO delegueDTO);

    /**
     * Get all the delegues.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<DelegueDTO> findAll(Pageable pageable);


    /**
     * Get the "id" delegue.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<DelegueDTO> findOne(Long id);

    /**
     * Delete the "id" delegue.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the delegue corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<DelegueDTO> search(String query, Pageable pageable);
    List<DelegueDTO> findAll();
    void reIndex();
}
