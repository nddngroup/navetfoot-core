package org.nddngroup.navetfoot.core.service.mapper;


import org.nddngroup.navetfoot.core.domain.*;
import org.nddngroup.navetfoot.core.domain.DelegueMatch;
import org.nddngroup.navetfoot.core.service.dto.DelegueMatchDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link DelegueMatch} and its DTO {@link DelegueMatchDTO}.
 */
@Mapper(componentModel = "spring", uses = {MatchMapper.class, DelegueMapper.class})
public interface DelegueMatchMapper extends EntityMapper<DelegueMatchDTO, DelegueMatch> {

    @Mapping(source = "match.id", target = "matchId")
    @Mapping(source = "match.code", target = "matchCode")
    @Mapping(source = "delegue.id", target = "delegueId")
    @Mapping(source = "delegue.code", target = "delegueCode")
    DelegueMatchDTO toDto(DelegueMatch delegueMatch);

    @Mapping(source = "matchId", target = "match")
    @Mapping(source = "delegueId", target = "delegue")
    DelegueMatch toEntity(DelegueMatchDTO delegueMatchDTO);

    default DelegueMatch fromId(Long id) {
        if (id == null) {
            return null;
        }
        DelegueMatch delegueMatch = new DelegueMatch();
        delegueMatch.setId(id);
        return delegueMatch;
    }
}
