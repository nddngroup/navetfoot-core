package org.nddngroup.navetfoot.core.service.impl;

import org.nddngroup.navetfoot.core.domain.Participation;
import org.nddngroup.navetfoot.core.repository.ParticipationRepository;
import org.nddngroup.navetfoot.core.repository.search.ParticipationSearchRepository;
import org.nddngroup.navetfoot.core.service.EtapeService;
import org.nddngroup.navetfoot.core.service.ParticipationService;
import org.nddngroup.navetfoot.core.service.custom.CommunService;
import org.nddngroup.navetfoot.core.service.custom.DeleteService;
import org.nddngroup.navetfoot.core.service.dto.*;
import org.nddngroup.navetfoot.core.service.dto.AssociationDTO;
import org.nddngroup.navetfoot.core.service.dto.EtapeCompetitionDTO;
import org.nddngroup.navetfoot.core.service.dto.OrganisationDTO;
import org.nddngroup.navetfoot.core.service.dto.ParticipationDTO;
import org.nddngroup.navetfoot.core.service.mapper.AssociationMapper;
import org.nddngroup.navetfoot.core.service.mapper.EtapeCompetitionMapper;
import org.nddngroup.navetfoot.core.service.mapper.ParticipationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link Participation}.
 */
@Service
@Transactional
public class ParticipationServiceImpl implements ParticipationService {

    private final Logger log = LoggerFactory.getLogger(ParticipationServiceImpl.class);

    private final ParticipationRepository participationRepository;

    private final ParticipationMapper participationMapper;

    private final ParticipationSearchRepository participationSearchRepository;
    @Autowired
    private EtapeCompetitionMapper etapeCompetitionMapper;
    @Autowired
    private AssociationMapper associationMapper;
    @Autowired
    private CommunService communService;
    @Autowired
    private EtapeService etapeService;
    @Autowired
    private DeleteService deleteService;

    public ParticipationServiceImpl(ParticipationRepository participationRepository, ParticipationMapper participationMapper, ParticipationSearchRepository participationSearchRepository) {
        this.participationRepository = participationRepository;
        this.participationMapper = participationMapper;
        this.participationSearchRepository = participationSearchRepository;
    }

    @Override
    public ParticipationDTO save(ParticipationDTO participationDTO) {
        log.debug("Request to save Participation : {}", participationDTO);
        Participation participation = participationMapper.toEntity(participationDTO);
        participation = participationRepository.save(participation);
        ParticipationDTO result = participationMapper.toDto(participation);

        if (participationDTO.isDeleted()) {
            participationSearchRepository.deleteById(participationDTO.getId());
        } else {
            participationSearchRepository.save(participationMapper.toEntity(result));
        }

        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ParticipationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Participations");
        return participationRepository.findAll(pageable)
            .map(participationMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<ParticipationDTO> findOne(Long id) {
        log.debug("Request to get Participation : {}", id);
        return participationRepository.findById(id)
            .map(participationMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Participation : {}", id);
        participationRepository.deleteById(id);
        participationSearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ParticipationDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Participations for query {}", query);
        return participationSearchRepository.search(queryStringQuery(query), pageable)
            .map(participationMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ParticipationDTO> findByEtapeCompetition(EtapeCompetitionDTO etapeCompetitionDTO) {
        log.debug("Request to get all Participations");
        return participationRepository.findAllByEtapeCompetitionAndDeletedIsFalse(etapeCompetitionMapper.toEntity(etapeCompetitionDTO))
            .stream()
            .map(participationMapper::toDto).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ParticipationDTO> findOneByEtapeCompetitionAndAssociation(EtapeCompetitionDTO etapeCompetitionDTO, AssociationDTO associationDTO) {
        log.debug("Request to get all Participations");
        return participationRepository.findOneByEtapeCompetitionAndAssociationAndDeletedIsFalse(
                                                        etapeCompetitionMapper.toEntity(etapeCompetitionDTO),
                                                        associationMapper.toEntity(associationDTO))
            .map(participationMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ParticipationDTO> findByHashcode(String hashcode) {
        log.debug("Request to get Participation : {}", hashcode);
        return participationRepository.findByHashcodeAndDeletedIsFalse(hashcode)
            .map(participationMapper::toDto);
    }

    @Override
    public void saveAll(List<ParticipationDTO> participationDTOS, EtapeCompetitionDTO etapeCompetitionDTO, OrganisationDTO organisationDTO) {
        log.debug("Request to save all Participations : {}", participationDTOS);
        List<Participation> participations = participationMapper.toEntity(participationDTOS);
        List<Participation> savedParticipations = participationRepository.saveAll(participations);

        participationSearchRepository.saveAll(savedParticipations);
    }

    @Override
    public void deleteAll(List<ParticipationDTO> participationDTOS, EtapeCompetitionDTO etapeCompetitionDTO) {
        log.debug("Request to delete all Participations : {}", participationDTOS);
        List<Participation> participations = participationMapper.toEntity(participationDTOS);
        List<Participation> savedParticipations = participationRepository.saveAll(participations);

        participationSearchRepository.deleteAll(savedParticipations);

        deleteService.deleteParticipationsClean(participationDTOS, etapeCompetitionDTO);
    }

    @Override
    public List<ParticipationDTO> findAll() {
        return participationRepository.findAllByDeletedIsFalse().stream()
            .map(participationMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public void reIndex() {
        log.info("Request to reindex all Participations");
        participationSearchRepository.deleteAll();
        findAll().stream().map(participationMapper::toEntity).forEach(participationSearchRepository::save);
    }
}
