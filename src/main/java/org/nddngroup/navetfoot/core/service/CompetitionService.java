package org.nddngroup.navetfoot.core.service;

import org.nddngroup.navetfoot.core.domain.Competition;
import org.nddngroup.navetfoot.core.domain.custom.CompetitionWithSaisonsAndMatchs;
import org.nddngroup.navetfoot.core.service.dto.CompetitionDTO;
import org.nddngroup.navetfoot.core.service.dto.OrganisationDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Competition}.
 */
public interface CompetitionService {

    /**
     * Save a competition.
     *
     * @param competitionDTO the entity to save.
     * @return the persisted entity.
     */
    CompetitionDTO save(CompetitionDTO competitionDTO);

    /**
     * Get all the competitions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CompetitionDTO> findAll(Pageable pageable);


    /**
     * Get the "id" competition.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CompetitionDTO> findOne(Long id);

    /**
     * Delete the "id" competition.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the competition corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CompetitionDTO> search(String query, Pageable pageable);
    Page<CompetitionDTO> findAllByOrganisation(OrganisationDTO organisationDTO, Pageable pageable);
    List<CompetitionDTO> findAllByOrganisation(OrganisationDTO organisationDTO);
    Page<CompetitionDTO> findByIdIn(List<Long> competitionIds, Pageable pageable);
    List<CompetitionDTO> findByIdIn(List<Long> competitionIds);
    Optional<CompetitionDTO> findByHashcode(String hashcode);
    List<CompetitionWithSaisonsAndMatchs> findAllCompetitionsWithSaisonsAndMatchs(String keyword, String selectedDate);
    List<CompetitionDTO> save(OrganisationDTO organisationDTO, List<CompetitionDTO> competitionDTOS);
    CompetitionDTO save(OrganisationDTO organisationDTO, CompetitionDTO competitionDTO);
    Optional<CompetitionDTO> findByOrganisationAndNom(OrganisationDTO organisationDTO, String nom);
    Page<CompetitionDTO> search(OrganisationDTO organisationDTO, String query, Pageable pageable);
    List<CompetitionDTO> findAll();
    void reIndex();
}
