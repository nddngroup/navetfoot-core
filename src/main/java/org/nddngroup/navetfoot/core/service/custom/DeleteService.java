package org.nddngroup.navetfoot.core.service.custom;

import org.nddngroup.navetfoot.core.domain.enumeration.EtatMatch;
import org.nddngroup.navetfoot.core.exception.ApiRequestException;
import org.nddngroup.navetfoot.core.exception.ExceptionCode;
import org.nddngroup.navetfoot.core.exception.ExceptionLevel;
import org.nddngroup.navetfoot.core.service.*;
import org.nddngroup.navetfoot.core.service.dto.*;
import org.nddngroup.navetfoot.core.service.*;
import org.nddngroup.navetfoot.core.service.dto.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class DeleteService {
    private final Logger log = LoggerFactory.getLogger(DeleteService.class);

    @Autowired
    private SaisonService saisonService;
    @Autowired
    private CompetitionService competitionService;
    @Autowired
    private EtapeCompetitionService etapeCompetitionService;
    @Autowired
    private CompetitionSaisonService competitionSaisonService;
    @Autowired
    private ContratService contratService;
    @Autowired
    private MatchService matchService;
    @Autowired
    private CommunService communService;
    @Autowired
    private PouleService pouleService;
    @Autowired
    private DetailPouleService detailPouleService;
    @Autowired
    private CompetitionSaisonCategoryService competitionSaisonCategoryService;
    @Autowired
    private FeuilleDeMatchService feuilleDeMatchService;
    @Autowired
    private ButService butService;
    @Autowired
    private CartonService cartonService;
    @Autowired
    private PenaltyService penaltyService;
    @Autowired
    private CornerService cornerService;
    @Autowired
    private CoupFrancService coupFrancService;
    @Autowired
    private RemplacementService remplacementService;
    @Autowired
    private HorsJeuService horsJeuService;
    @Autowired
    private TirService tirService;
    @Autowired
    private DetailFeuilleDeMatchService detailFeuilleDeMatchService;
    @Autowired
    private AffiliationService affiliationService;
    @Autowired
    private AssociationService associationService;
    @Autowired
    private JoueurService joueurService;

    public SaisonDTO deleteSaison(SaisonDTO saison) {
        log.debug("Request to delete Saison : {}", saison);

        saison.setDeleted(true);
        saison.setUpdatedAt(ZonedDateTime.now());

        saisonService.save(saison);

        //clean
        deleteSaisonClean(saison);

        return saison;
    }

    public CompetitionDTO deleteCompetition(CompetitionDTO competition) {
        log.debug("Request to delete Competition : {}", competition);

        competition.setDeleted(true);
        competition.setUpdatedAt(ZonedDateTime.now());

        competitionService.save(competition);

        //clean
        deleteCompetitionClean(competition);

        return competition;
    }

    public void deleteCompetitionSaison(CompetitionSaisonDTO competitionSaisonDTO) {
        log.debug("Request to delete CompetitionSaison : {}", competitionSaisonDTO);

        competitionSaisonDTO.setDeleted(true);
        competitionSaisonDTO.setUpdatedAt(ZonedDateTime.now());

        competitionSaisonDTO = competitionSaisonService.save(competitionSaisonDTO);

        // clean
        deleteCompetitionSaisonClean(competitionSaisonDTO);
    }

    public void deleteContrat(ContratDTO contratDTO) {
        log.debug("Request to delete Contrat : {}", contratDTO);

        contratDTO.setDeleted(true);
        contratDTO.setUpdatedAt(ZonedDateTime.now());

        contratService.save(contratDTO);
    }

    public void deleteEtapeCompetition(EtapeCompetitionDTO etapeCompetitionDTO) {
        log.debug("Request to delete etapeCompetition : {}", etapeCompetitionDTO);

        etapeCompetitionDTO.setDeleted(true);
        etapeCompetitionDTO.setUpdatedAt(ZonedDateTime.now());
        etapeCompetitionDTO = etapeCompetitionService.save(etapeCompetitionDTO);

        // clean
        deleteEtapeCompetitionClean(etapeCompetitionDTO);
    }

    public void deleteEtapeCompetition(List<EtapeCompetitionDTO> etapeCompetitionDTOs) {
        log.debug("Request to delete Matchs by etape : {}", etapeCompetitionDTOs);

        for (EtapeCompetitionDTO etapeCompetitionDTO : etapeCompetitionDTOs) {
            deleteEtapeCompetition(etapeCompetitionDTO);
        }
    }

    public void deleteCompetitionSaisonCategory(CompetitionSaisonCategoryDTO competitionSaisonCategoryDTO) {
        log.debug("Request to delete competitionSaisonCategory : {}", competitionSaisonCategoryDTO);

        competitionSaisonCategoryDTO.setDeleted(true);
        competitionSaisonCategoryDTO.setUpdatedAt(ZonedDateTime.now());
        competitionSaisonCategoryService.save(competitionSaisonCategoryDTO);
    }

    public void deleteMatch(MatchDTO matchDTO) {
        if (!matchDTO.getEtatMatch().equals(EtatMatch.PROGRAMME)) {
            throw new ApiRequestException(
                ExceptionCode.CAN_NOT_DELETE_MATCH.getMessage(),
                ExceptionCode.CAN_NOT_DELETE_MATCH.getValue(),
                ExceptionLevel.WARNING
            );
        }

        matchDTO.setDeleted(true);
        matchDTO.setUpdatedAt(ZonedDateTime.now());

        matchDTO = matchService.save(matchDTO);

        // clean
        deleteMatchClean(matchDTO);
    }

    public void deleteAffiliation(AffiliationDTO affiliationDTO) {
        affiliationDTO.setDeleted(true);
        affiliationDTO.setUpdatedAt(ZonedDateTime.now());
        affiliationService.save(affiliationDTO);
    }

    public void deleteAssociation(AssociationDTO associationDTO) {
        associationDTO.setDeleted(true);
        associationDTO.setUpdatedAt(ZonedDateTime.now());

        associationDTO = associationService.save(associationDTO);

        // clean
        deleteAssociationClean(associationDTO);
    }

    public void deleteAssociation(AssociationDTO associationDTO, OrganisationDTO organisationDTO) {
        if (organisationDTO != null) {
            // find affiliation
            this.affiliationService.findByOrganisationAndAssociation(organisationDTO, associationDTO)
                .ifPresent(this::deleteAffiliation);
        }

        associationDTO.setDeleted(true);
        associationDTO.setUpdatedAt(ZonedDateTime.now());

        associationDTO = associationService.save(associationDTO);

        // clean
        deleteAssociationClean(associationDTO);
    }

    public void deleteJoueur(JoueurDTO joueurDTO) {
        joueurDTO.setDeleted(true);
        joueurDTO.setUpdatedAt(ZonedDateTime.now());

        joueurDTO = joueurService.save(joueurDTO);

        // clean
        deleteJoueurClean(joueurDTO);
    }

    public void deleteJoueur(Long joueurId, Long associationId, OrganisationDTO organisationDTO) {
        var joueurDTO = joueurService.findOne(joueurId)
            .orElseThrow(
                () -> new ApiRequestException(
                    ExceptionCode.JOUEUR_NOT_FOUND.getMessage(),
                    ExceptionCode.JOUEUR_NOT_FOUND.getValue(),
                    ExceptionLevel.WARNING
                )
            );

        List<ContratDTO> contratDTOS = null;

        if (associationId != null) {
            // trouver association
            var associationDTOOptional = associationService.findOne(associationId);
            if (associationDTOOptional.isPresent()) {
                // contrats du joueur avec cette association
                var associationDTO = associationDTOOptional.get();
                contratDTOS = contratService.findByAssociationAndJoueur(associationDTO, joueurDTO);
            }
        } else {
            // tous les contrat du joueur
            contratDTOS = contratService.findByJoueur(joueurDTO);
        }

        if (contratDTOS != null) {
            contratDTOS.forEach(contratDTO ->
                    saisonService.findOne(contratDTO.getSaisonId())
                        .ifPresent(saisonDTO -> {
                            if (saisonDTO.getOrganisationId().equals(organisationDTO.getId())) {
                                deleteContrat(contratDTO);
                            }
                        })
            );
        }

        // supprimer le joueur s'il n'a aucun contrat
        if (contratService.findByJoueur(joueurDTO).isEmpty()) {
            deleteJoueur(joueurDTO);
        }
    }

    public void deleteMatch(List<MatchDTO> matchDTOs) {
        for (MatchDTO matchDTO: matchDTOs) {
            deleteMatch(matchDTO);
        }
    }

    public void deletePoule(PouleDTO pouleDTO) {
        pouleDTO.setDeleted(true);
        pouleDTO.setUpdatedAt(ZonedDateTime.now());

        pouleDTO = pouleService.save(pouleDTO);

        // clean
        deletePouleClean(pouleDTO);
    }

    public void deleteDetailPoule(DetailPouleDTO detailPouleDTO) {
        detailPouleDTO.setDeleted(true);
        detailPouleService.save(detailPouleDTO);
    }

    public void deleteFeuilleDeMatch(FeuilleDeMatchDTO feuilleDeMatchDTO) {
        feuilleDeMatchDTO.setDeleted(true);
        feuilleDeMatchDTO.setUpdatedAt(ZonedDateTime.now());

        feuilleDeMatchDTO = feuilleDeMatchService.save(feuilleDeMatchDTO);

        // clean
        deleteFeuilleDeMatchClean(feuilleDeMatchDTO);
    }

    public void deleteDetailFeuilleDeMatch(DetailFeuilleDeMatchDTO detailFeuilleDeMatchDTO) {
        detailFeuilleDeMatchDTO.setDeleted(true);
        detailFeuilleDeMatchDTO.setUpdatedAt(ZonedDateTime.now());
        detailFeuilleDeMatchService.save(detailFeuilleDeMatchDTO);
    }

    public void deleteBut(ButDTO butDTO) {
        butDTO.setDeleted(true);
        butDTO.setUpdatedAt(ZonedDateTime.now());
        butService.save(butDTO);
    }

    public void deleteCarton(CartonDTO cartonDTO) {
        cartonDTO.setDeleted(true);
        cartonDTO.setUpdatedAt(ZonedDateTime.now());
        cartonService.save(cartonDTO);
    }

    public void deletePenalty(PenaltyDTO penaltyDTO) {
        penaltyDTO.setDeleted(true);
        penaltyDTO.setUpdatedAt(ZonedDateTime.now());
        penaltyService.save(penaltyDTO);
    }

    public void deleteCorner(CornerDTO cornerDTO) {
        cornerDTO.setDeleted(true);
        cornerDTO.setUpdatedAt(ZonedDateTime.now());
        cornerService.save(cornerDTO);
    }

    public void deleteCoupFranc(CoupFrancDTO coupFrancDTO) {
        coupFrancDTO.setDeleted(true);
        coupFrancDTO.setUpdatedAt(ZonedDateTime.now());
        coupFrancService.save(coupFrancDTO);
    }

    public void deleteRemplacement(RemplacementDTO remplacementDTO) {
        remplacementDTO.setDeleted(true);
        remplacementDTO.setUpdatedAt(ZonedDateTime.now());
        remplacementService.save(remplacementDTO);
    }

    public void deleteHorsJeu(HorsJeuDTO horsJeuDTO) {
        horsJeuDTO.setDeleted(true);
        horsJeuDTO.setUpdatedAt(ZonedDateTime.now());
        horsJeuService.save(horsJeuDTO);
    }

    public void deleteTir(TirDTO tirDTO) {
        tirDTO.setDeleted(true);
        tirDTO.setUpdatedAt(ZonedDateTime.now());
        tirService.save(tirDTO);
    }


    //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    //$$$$$$$$$$ REMOVE LIST $$$$$$$$$$$
    //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

    public List<SaisonDTO> removeSaisonList(OrganisationDTO organisationDTO, List<String> saisonIds) {
        log.debug("Request to remove list of saisons : {}", saisonIds);

        List<SaisonDTO> saisonDTOS = new ArrayList<>();

        if (saisonIds != null) {
            for (String saisonId : saisonIds) {

                Long id = Long.parseLong(saisonId);

                var saisonDTO = communService.checkSaisonForOrganisation(id, organisationDTO);

                saisonDTO = deleteSaison(saisonDTO);

                saisonDTOS.add(saisonDTO);
            }
        }

        return saisonDTOS;
    }

    public List<CompetitionDTO> removeCompetitionList(OrganisationDTO organisationDTO, List<String> competitionIds) {
        log.debug("Request to remove list of competitions : {}", competitionIds);

        List<CompetitionDTO> competitionDTOS = new ArrayList<>();

        if (competitionIds != null) {
            for (String item : competitionIds) {

                Long id = Long.parseLong(item);

                var competitionDTO = communService.checkCompetitionForOrganisation(id, organisationDTO);

                competitionDTO = deleteCompetition(competitionDTO);

                competitionDTOS.add(competitionService.save(competitionDTO));
            }
        }

        return competitionDTOS;
    }

    public List<MatchDTO> removeMatchList(OrganisationDTO organisationDTO, List<String> matchIds) {
        log.debug("Request to remove list of matchs : {}", matchIds);

        List<MatchDTO> matchDTOS = new ArrayList<>();

        if (matchIds != null) {
            for (String item : matchIds) {

                Long id = Long.parseLong(item);

                var matchDTO = communService.checkMatch(id, organisationDTO);

                deleteMatch(Collections.singletonList(matchDTO));
            }
        }

        return matchDTOS;
    }

    public List<AssociationDTO> removeAssociationList(OrganisationDTO organisationDTO, List<String> associationIds, Boolean owner) {
        log.debug("Request to remove list of association : {}", associationIds);

        List<AssociationDTO> associationDTOS = new ArrayList<>();

        if (associationIds != null) {
            for (String associationId : associationIds) {

                Long id = Long.parseLong(associationId);

                associationService.findOne(id)
                    .ifPresent(
                        associationDTO -> {
                            associationDTOS.add(associationDTO);

                            // trouver l'affiliation et la supprimer
                            affiliationService.findByOrganisationAndAssociation(organisationDTO, associationDTO)
                                .ifPresent(this::deleteAffiliation);

                            // supprimer le club si l'organisation est propriétaire
                            if (owner == null || owner.equals(true)) {
                                deleteAssociation(associationDTO);
                            }
                        }
                    );
            }
        }

        return associationDTOS;
    }

    public List<JoueurDTO> removeJoueurList(OrganisationDTO organisationDTO, AssociationDTO associationDTO, List<String> joueurIds, Boolean owner) {
        log.debug("Request to remove list joueurs : {}", joueurIds);

        List<JoueurDTO> joueurDTOS = new ArrayList<>();

        if (joueurIds != null) {
            for (String joueurId: joueurIds) {
                Long id = Long.parseLong(joueurId);

                // find joueur by id
                joueurService.findOne(id).ifPresent(joueurDTO -> {
                    joueurDTOS.add(joueurDTO);

                    removeContrat(organisationDTO, joueurDTO, associationDTO, owner);
                });
            }
        }

        return joueurDTOS;
    }

    private void removeContrat(OrganisationDTO organisationDTO, JoueurDTO joueurDTO, AssociationDTO associationDTO, Boolean owner) {
        // find all saisons
        List<SaisonDTO> saisonDTOS = saisonService.findAllByOrganisation(organisationDTO);

        if (saisonDTOS != null) {
            for (SaisonDTO saisonDTO : saisonDTOS) {
                // remove contrat
                if (associationDTO != null) {
                    contratService.findBySaisonAndAssociationAndJoueur(saisonDTO, associationDTO, joueurDTO)
                        .ifPresent(this::deleteContrat);
                } else {
                    contratService.findBySaisonAndJoueur(saisonDTO, joueurDTO)
                        .ifPresent(this::deleteContrat);
                }
            }
        }

        // remove joueur if owner
        if (associationDTO == null && (owner == null || owner.equals(true))) {
            deleteJoueur(joueurDTO);
        }
    }


    //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    //$$$$$$$$$$ CLEAN $$$$$$$$$$$$$$$$$
    //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

    public void deleteSaisonClean(SaisonDTO saisonDTO) {
        if (saisonDTO != null) {
            // remove all competitionSaison
            competitionSaisonService.findBySaison(saisonDTO)
                .forEach(this::deleteCompetitionSaison)
            ;

            // remove all contrat
            contratService.findBySaison(saisonDTO)
                .forEach(this::deleteContrat);
        }
    }

    public void deleteCompetitionClean(CompetitionDTO competitionDTO) {
        if (competitionDTO != null) {
            // remove all competitionSaison
            competitionSaisonService.findByCompetition(competitionDTO)
                .forEach(this::deleteCompetitionSaison);
        }
    }

    public void deleteEtapeCompetitionClean(EtapeCompetitionDTO etapeCompetitionDTO) {
        if (etapeCompetitionDTO != null) {
            // remove all matchs
            matchService.findByEtapeCompetition(etapeCompetitionDTO)
                .forEach(this::deleteMatch);

            // remove all poule
            pouleService.findByEtapeCompetition(etapeCompetitionDTO)
                .forEach(this::deletePoule);
        }
    }

    public void deleteCompetitionSaisonClean(CompetitionSaisonDTO competitionSaisonDTO) {
        if (competitionSaisonDTO != null) {
            // remove all etapeCompetitions
            etapeCompetitionService.findByCompetitionSaison(competitionSaisonDTO)
                .forEach(this::deleteEtapeCompetition);

            // remove all competitionSaisonCategories
            competitionSaisonCategoryService.findByCompetitionSaison(competitionSaisonDTO)
                .forEach(this::deleteCompetitionSaisonCategory);
        }
    }

    public void deletePouleClean(PouleDTO pouleDTO) {
        if (pouleDTO != null) {
            // remove all detailsPoule
            detailPouleService.findByPoule(pouleDTO)
                .forEach(this::deleteDetailPoule);
        }
    }

    public void deleteFeuilleDeMatchClean(FeuilleDeMatchDTO feuilleDeMatchDTO) {
        if (feuilleDeMatchDTO != null) {
            // remove all detailsFeuilleDeMatch
            detailFeuilleDeMatchService.findByFeuilleDeMatch(feuilleDeMatchDTO)
                .forEach(this::deleteDetailFeuilleDeMatch);
        }
    }

    public void deleteMatchClean(MatchDTO matchDTO) {
        if (matchDTO != null) {
            // find all feuilleDeMatch

            feuilleDeMatchService.findByMatch(matchDTO)
                .ifPresent(this::deleteFeuilleDeMatch);

            // find all buts
            butService.findByMatch(matchDTO)
                .forEach(this::deleteBut);

            // find all cartons
            cartonService.findByMatch(matchDTO)
                .forEach(this::deleteCarton);

            // find all penalties
            penaltyService.findByMatch(matchDTO)
                .forEach(this::deletePenalty);

            // find all corners
            cornerService.findByMatch(matchDTO)
                .forEach(this::deleteCorner);

            // find all coupFrancs
            coupFrancService.findByMatch(matchDTO)
                .forEach(this::deleteCoupFranc);

            // find all remplacements
            remplacementService.findByMatch(matchDTO)
                .forEach(this::deleteRemplacement);

            // find all horsjeus
            horsJeuService.findByMatch(matchDTO)
                .forEach(this::deleteHorsJeu);

            // find all tirs
            tirService.findByMatch(matchDTO)
                .forEach(this::deleteTir);
        }
    }

    public void deleteAssociationClean(AssociationDTO associationDTO) {
        if (associationDTO != null) {
            // remove all affiliation
            affiliationService.findByAssociation(associationDTO)
                .forEach(this::deleteAffiliation);

            // remove all contrat
            contratService.findByAssociation(associationDTO)
                .forEach(this::deleteContrat);
        }
    }

    public void deleteJoueurClean(JoueurDTO joueurDTO) {
        if (joueurDTO != null) {
            // remove all contrat
            contratService.findByJoueur(joueurDTO)
                .forEach(this::deleteContrat);
        }
    }

    public void deleteParticipationsClean(List<ParticipationDTO> participationDTOS, EtapeCompetitionDTO etapeCompetitionDTO) {
        if (Boolean.TRUE.equals(etapeCompetitionDTO.isHasPoule())) { // supprimer le club dans les poules
            List<Long> pouleIds = pouleService.findByEtapeCompetition(etapeCompetitionDTO).stream().map(PouleDTO::getId).collect(Collectors.toList());

            if (pouleIds.size() != 0) {
                participationDTOS.forEach(participationDTO -> {
                    // find association by id
                    associationService.findOne(participationDTO.getAssociationId()).ifPresent(
                        associationDTO -> detailPouleService.findByPouleInAndAssociation(pouleIds, associationDTO)
                            .forEach(detailPouleDTO -> {
                                log.debug("_____ DETAIL POULE TO REMOVE : {}", detailPouleDTO);
                                detailPouleDTO.setDeleted(true);
                                detailPouleDTO.setUpdateAt(ZonedDateTime.now());
                                detailPouleService.save(detailPouleDTO);
                            })
                    );
                });
            }

        }

    }
}
