package org.nddngroup.navetfoot.core.service;

import org.nddngroup.navetfoot.core.domain.Organisation;
import org.nddngroup.navetfoot.core.service.dto.OrganisationDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Organisation}.
 */
public interface OrganisationService {

    /**
     * Save a organisation.
     *
     * @param organisationDTO the entity to save.
     * @return the persisted entity.
     */
    OrganisationDTO save(OrganisationDTO organisationDTO);

    /**
     * Get all the organisations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<OrganisationDTO> findAll(Pageable pageable);

    /**
     * Get all the organisations with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    Page<OrganisationDTO> findAllWithEagerRelationships(Pageable pageable);


    /**
     * Get the "id" organisation.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<OrganisationDTO> findOne(Long id);

    /**
     * Delete the "id" organisation.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the organisation corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<OrganisationDTO> search(String query, Pageable pageable);

    List<OrganisationDTO> findAll(Long userId);
    List<OrganisationDTO> findAll();
    List<OrganisationDTO> findAllOrganisations();
    Optional<OrganisationDTO> findByHashcode(String hashcode);
    void reIndex();
}
