package org.nddngroup.navetfoot.core.service.impl;

import org.nddngroup.navetfoot.core.service.DepartementService;
import org.nddngroup.navetfoot.core.domain.Departement;
import org.nddngroup.navetfoot.core.repository.DepartementRepository;
import org.nddngroup.navetfoot.core.repository.search.DepartementSearchRepository;
import org.nddngroup.navetfoot.core.service.dto.DepartementDTO;
import org.nddngroup.navetfoot.core.service.dto.RegionDTO;
import org.nddngroup.navetfoot.core.service.mapper.DepartementMapper;
import org.nddngroup.navetfoot.core.service.mapper.RegionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link Departement}.
 */
@Service
@Transactional
public class DepartementServiceImpl implements DepartementService {

    private final Logger log = LoggerFactory.getLogger(DepartementServiceImpl.class);

    private final DepartementRepository departementRepository;

    private final DepartementMapper departementMapper;

    @Autowired
    private RegionMapper regionMapper;

    private final DepartementSearchRepository departementSearchRepository;

    public DepartementServiceImpl(DepartementRepository departementRepository, DepartementMapper departementMapper, DepartementSearchRepository departementSearchRepository) {
        this.departementRepository = departementRepository;
        this.departementMapper = departementMapper;
        this.departementSearchRepository = departementSearchRepository;
    }

    @Override
    public DepartementDTO save(DepartementDTO departementDTO) {
        log.debug("Request to save Departement : {}", departementDTO);
        Departement departement = departementMapper.toEntity(departementDTO);
        departement = departementRepository.save(departement);
        DepartementDTO result = departementMapper.toDto(departement);
        departementSearchRepository.save(departement);
        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<DepartementDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Departements");
        return departementRepository.findAll(pageable)
            .map(departementMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<DepartementDTO> findOne(Long id) {
        log.debug("Request to get Departement : {}", id);
        return departementRepository.findById(id)
            .map(departementMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Departement : {}", id);
        departementRepository.deleteById(id);
        departementSearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<DepartementDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Departements for query {}", query);
        return departementSearchRepository.search(queryStringQuery(query), pageable)
            .map(departementMapper::toDto);
    }


    @Override
    public List<DepartementDTO> findDepartementsByRegion(RegionDTO region) {
        log.debug("Request to get Departement by region id: {}", region.getId());
            List<Departement> departements = departementRepository.findDepartementByRegion(regionMapper.toEntity(region));
            List<DepartementDTO> departementDTOS = new ArrayList<>();

            for(Departement d: departements){
                    departementDTOS.add(departementMapper.toDto(d));
            }
        return departementDTOS;
    }

    @Override
    public List<DepartementDTO> findAll() {
        return departementRepository.findAll().stream()
            .map(departementMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public void reIndex() {
        log.info("Request to reindex all Departement");
        departementSearchRepository.deleteAll();
        findAll().stream().map(departementMapper::toEntity).forEach(departementSearchRepository::save);
    }
}
