package org.nddngroup.navetfoot.core.service.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "navetfoot-gateway")
public interface WebSocketApiClient {
    @PostMapping("/websocket")
    ResponseEntity<Void> sendToClientViaWebSocket(@RequestParam(name = "topic") String topic, @RequestBody Object data);
}

