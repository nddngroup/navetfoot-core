package org.nddngroup.navetfoot.core.service.dto;

import org.nddngroup.navetfoot.core.domain.CompetitionSaison;

import java.time.ZonedDateTime;
import java.io.Serializable;

/**
 * A DTO for the {@link CompetitionSaison} entity.
 */
public class CompetitionSaisonDTO implements Serializable {

    private Long id;

    private String hashcode;

    private String code;

    private ZonedDateTime createdAt;

    private ZonedDateTime updatedAt;

    private Boolean deleted;


    private Long competitionId;

    private String competitionNom;

    private Long saisonId;

    private String saisonLibelle;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHashcode() {
        return hashcode;
    }

    public void setHashcode(String hashcode) {
        this.hashcode = hashcode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Long getCompetitionId() {
        return competitionId;
    }

    public void setCompetitionId(Long competitionId) {
        this.competitionId = competitionId;
    }

    public String getCompetitionNom() {
        return competitionNom;
    }

    public void setCompetitionNom(String competitionNom) {
        this.competitionNom = competitionNom;
    }

    public Long getSaisonId() {
        return saisonId;
    }

    public void setSaisonId(Long saisonId) {
        this.saisonId = saisonId;
    }

    public String getSaisonLibelle() {
        return saisonLibelle;
    }

    public void setSaisonLibelle(String saisonLibelle) {
        this.saisonLibelle = saisonLibelle;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CompetitionSaisonDTO)) {
            return false;
        }

        return id != null && id.equals(((CompetitionSaisonDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CompetitionSaisonDTO{" +
            "id=" + getId() +
            ", hashcode='" + getHashcode() + "'" +
            ", code='" + getCode() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", deleted='" + isDeleted() + "'" +
            ", competitionId=" + getCompetitionId() +
            ", competitionNom='" + getCompetitionNom() + "'" +
            ", saisonId=" + getSaisonId() +
            ", saisonLibelle='" + getSaisonLibelle() + "'" +
            "}";
    }
}
