package org.nddngroup.navetfoot.core.service.mapper;


import org.nddngroup.navetfoot.core.domain.*;
import org.nddngroup.navetfoot.core.domain.CompetitionSaison;
import org.nddngroup.navetfoot.core.service.dto.CompetitionSaisonDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link CompetitionSaison} and its DTO {@link CompetitionSaisonDTO}.
 */
@Mapper(componentModel = "spring", uses = {CompetitionMapper.class, SaisonMapper.class})
public interface CompetitionSaisonMapper extends EntityMapper<CompetitionSaisonDTO, CompetitionSaison> {

    @Mapping(source = "competition.id", target = "competitionId")
    @Mapping(source = "competition.nom", target = "competitionNom")
    @Mapping(source = "saison.id", target = "saisonId")
    @Mapping(source = "saison.libelle", target = "saisonLibelle")
    CompetitionSaisonDTO toDto(CompetitionSaison competitionSaison);

    @Mapping(target = "etapeCompetitions", ignore = true)
    @Mapping(target = "removeEtapeCompetition", ignore = true)
    @Mapping(source = "competitionId", target = "competition")
    @Mapping(source = "saisonId", target = "saison")
    CompetitionSaison toEntity(CompetitionSaisonDTO competitionSaisonDTO);

    default CompetitionSaison fromId(Long id) {
        if (id == null) {
            return null;
        }
        CompetitionSaison competitionSaison = new CompetitionSaison();
        competitionSaison.setId(id);
        return competitionSaison;
    }
}
