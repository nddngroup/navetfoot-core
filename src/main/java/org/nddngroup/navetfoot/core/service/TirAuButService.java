package org.nddngroup.navetfoot.core.service;

import org.nddngroup.navetfoot.core.domain.TirAuBut;
import org.nddngroup.navetfoot.core.service.dto.MatchDTO;
import org.nddngroup.navetfoot.core.service.dto.TirAuButDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link TirAuBut}.
 */
public interface TirAuButService {

    /**
     * Save a tirAuBut.
     *
     * @param tirAuButDTO the entity to save.
     * @return the persisted entity.
     */
    TirAuButDTO save(TirAuButDTO tirAuButDTO);

    /**
     * Get all the tirAuButs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<TirAuButDTO> findAll(Pageable pageable);


    /**
     * Get the "id" tirAuBut.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<TirAuButDTO> findOne(Long id);

    /**
     * Delete the "id" tirAuBut.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the tirAuBut corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<TirAuButDTO> search(String query, Pageable pageable);

    Optional<TirAuButDTO> findOneByMatch(MatchDTO matchDTO);
    Optional<TirAuButDTO> findByHashcode(String hashcode);
    List<TirAuButDTO> findAll();
    void reIndex();
}
