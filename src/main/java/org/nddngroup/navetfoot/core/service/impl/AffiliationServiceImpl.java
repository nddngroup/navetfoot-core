package org.nddngroup.navetfoot.core.service.impl;

import org.nddngroup.navetfoot.core.domain.Affiliation;
import org.nddngroup.navetfoot.core.repository.AffiliationRepository;
import org.nddngroup.navetfoot.core.repository.search.AffiliationSearchRepository;
import org.nddngroup.navetfoot.core.service.AffiliationService;
import org.nddngroup.navetfoot.core.service.custom.CommunService;
import org.nddngroup.navetfoot.core.service.dto.AffiliationDTO;
import org.nddngroup.navetfoot.core.service.dto.AssociationDTO;
import org.nddngroup.navetfoot.core.service.dto.OrganisationDTO;
import org.nddngroup.navetfoot.core.service.mapper.AffiliationMapper;
import org.nddngroup.navetfoot.core.service.mapper.AssociationMapper;
import org.nddngroup.navetfoot.core.service.mapper.OrganisationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link Affiliation}.
 */
@Service
@Transactional
public class AffiliationServiceImpl implements AffiliationService {

    private final Logger log = LoggerFactory.getLogger(AffiliationServiceImpl.class);

    private final AffiliationRepository affiliationRepository;

    private final AffiliationMapper affiliationMapper;

    private final AffiliationSearchRepository affiliationSearchRepository;

    @Autowired
    private OrganisationMapper organisationMapper;
    @Autowired
    private AssociationMapper associationMapper;
    @Autowired
    private CommunService communService;

    public AffiliationServiceImpl(AffiliationRepository affiliationRepository, AffiliationMapper affiliationMapper, AffiliationSearchRepository affiliationSearchRepository) {
        this.affiliationRepository = affiliationRepository;
        this.affiliationMapper = affiliationMapper;
        this.affiliationSearchRepository = affiliationSearchRepository;
    }

    @Override
    public AffiliationDTO save(AffiliationDTO affiliationDTO) {
        log.debug("Request to save Affiliation : {}", affiliationDTO);
        Affiliation affiliation = affiliationMapper.toEntity(affiliationDTO);
        affiliation = affiliationRepository.save(affiliation);
        AffiliationDTO result = affiliationMapper.toDto(affiliation);

        if (affiliationDTO.isDeleted()) {
            affiliationSearchRepository.deleteById(affiliationDTO.getId());
        } else {
            affiliationSearchRepository.save(affiliationMapper.toEntity(result));
        }

        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<AffiliationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Affiliations");
        return affiliationRepository.findAll(pageable)
            .map(affiliationMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<AffiliationDTO> findAll() {
        log.debug("Request to get all Affiliations");
        return affiliationRepository.findAllByDeletedIsFalse().stream()
            .map(affiliationMapper::toDto).collect(Collectors.toList());
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<AffiliationDTO> findOne(Long id) {
        log.debug("Request to get Affiliation : {}", id);
        return affiliationRepository.findById(id)
            .map(affiliationMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Affiliation : {}", id);
        affiliationRepository.deleteById(id);
        affiliationSearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<AffiliationDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Affiliations for query {}", query);
        return affiliationSearchRepository.search(queryStringQuery(query), pageable)
            .map(affiliationMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<AffiliationDTO> findByOrganisationAndAssociation(OrganisationDTO organisationDTO, AssociationDTO associationDTO) {
        log.debug("Request to get all Affiliations by organisation and association");
        return affiliationRepository.findOneByOrganisationAndAssociationAndDeletedIsFalse(
            organisationMapper.toEntity(organisationDTO), associationMapper.toEntity(associationDTO))
            .map(affiliationMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<AffiliationDTO> findAllByOrganisation(OrganisationDTO organisationDTO) {
        log.debug("Request to get all Affiliations by organisation");
        return affiliationRepository.findAllByOrganisationAndDeletedIsFalse(organisationMapper.toEntity(organisationDTO)).stream()
            .map(affiliationMapper::toDto).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<AffiliationDTO> findByHashcode(String hashcode) {
        log.debug("Request to get Affiliation : {}", hashcode);
        return affiliationRepository.findByHashcodeAndDeletedIsFalse(hashcode)
            .map(affiliationMapper::toDto);
    }

    @Override
    public AffiliationDTO save(OrganisationDTO organisationDTO, AssociationDTO associationDTO) {
        log.debug("Request to save new Affiliation");

        AffiliationDTO affiliationDTO = new AffiliationDTO();
        affiliationDTO.setOrganisationId(organisationDTO.getId());
        affiliationDTO.setAssociationId(associationDTO.getId());
        affiliationDTO.setCode("REF_" + organisationDTO.getId().toString());

        String toHash = organisationDTO.getId().toString() + ZonedDateTime.now().toString();
        affiliationDTO.setHashcode(this.communService.toHash(toHash));

        affiliationDTO.setDeleted(false);
        affiliationDTO.setCreatedAt(ZonedDateTime.now());
        affiliationDTO.setUpdatedAt(ZonedDateTime.now());

      /*  affiliationDTO = save(affiliationDTO);

        affiliationDTO.setCode(affiliationDTO.getCode() + "_" + affiliationDTO.getId().toString());*/

        return save(affiliationDTO);
    }

    @Override
    @Transactional(readOnly = true)
    public List<AffiliationDTO> findByAssociation(AssociationDTO associationDTO) {
        log.debug("Request to get all Affiliations by association : {}", associationDTO);
        return affiliationRepository.findAllByAssociationAndDeletedIsFalse(associationMapper.toEntity(associationDTO)).stream()
            .map(affiliationMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public void reIndex() {
        log.info("Request to reindex Affiliations");
        affiliationSearchRepository.deleteAll();
        findAll().stream().map(affiliationMapper::toEntity).forEach(affiliationSearchRepository::save);
    }
}
