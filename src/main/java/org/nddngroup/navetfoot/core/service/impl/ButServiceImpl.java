package org.nddngroup.navetfoot.core.service.impl;

import org.nddngroup.navetfoot.core.domain.But;
import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;
import org.nddngroup.navetfoot.core.repository.ButRepository;
import org.nddngroup.navetfoot.core.repository.search.ButSearchRepository;
import org.nddngroup.navetfoot.core.service.ButService;
import org.nddngroup.navetfoot.core.service.dto.ButDTO;
import org.nddngroup.navetfoot.core.service.dto.JoueurDTO;
import org.nddngroup.navetfoot.core.service.dto.MatchDTO;
import org.nddngroup.navetfoot.core.service.mapper.ButMapper;
import org.nddngroup.navetfoot.core.service.mapper.JoueurMapper;
import org.nddngroup.navetfoot.core.service.mapper.MatchMapper;
import org.nddngroup.navetfoot.core.service.mapper.custom.ButStatsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link But}.
 */
@Service
@Transactional
public class ButServiceImpl implements ButService {

    private final Logger log = LoggerFactory.getLogger(ButServiceImpl.class);

    private final ButRepository butRepository;

    private final ButMapper butMapper;

    private final ButSearchRepository butSearchRepository;
    @Autowired
    private MatchMapper matchMapper;
    @Autowired
    private JoueurMapper joueurMapper;
    @Autowired
    private ButStatsMapper butStatsMapper;

    public ButServiceImpl(ButRepository butRepository, ButMapper butMapper, ButSearchRepository butSearchRepository) {
        this.butRepository = butRepository;
        this.butMapper = butMapper;
        this.butSearchRepository = butSearchRepository;
    }

    @Override
    public ButDTO save(ButDTO butDTO) {
        log.debug("Request to save But : {}", butDTO);

        butDTO.setUpdatedAt(ZonedDateTime.now());
        if (butDTO.getCreatedAt() == null) {
            butDTO.setCreatedAt(ZonedDateTime.now());
        }

        var but = butMapper.toEntity(butDTO);
        but = butRepository.save(but);
        ButDTO result = butMapper.toDto(but);

        if (Boolean.TRUE.equals(butDTO.isDeleted())) {
            butSearchRepository.deleteById(result.getId());
        } else {
            butSearchRepository.save(butStatsMapper.toStats(result));
        }

        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ButDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Buts");
        return butRepository.findAll(pageable)
            .map(butMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<ButDTO> findOne(Long id) {
        log.debug("Request to get But : {}", id);
        return butRepository.findById(id)
            .map(butMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete But : {}", id);
        butRepository.deleteById(id);
        butSearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ButDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Buts for query {}", query);
        return butSearchRepository.search(queryStringQuery(query), pageable)
            .map(butMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ButDTO> findByMatch(MatchDTO matchDTO) {
        log.debug("Request to get all Buts by match");
        return butRepository.findAllByMatchAndDeletedIsFalse(matchMapper.toEntity(matchDTO))
            .stream()
            .map(butMapper::toDto).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<ButDTO> findByMatchAndEquipe(MatchDTO matchDTO, Equipe equipe) {
        log.debug("Request to get all Buts by match and equipe");
        return butRepository.findAllByMatchAndEquipeAndDeletedIsFalse(matchMapper.toEntity(matchDTO), equipe)
            .stream()
            .map(butMapper::toDto).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<ButDTO> findByMatchAndJoueur(MatchDTO matchDTO, JoueurDTO joueurDTO) {
        log.debug("Request to get all Buts by match and joueur");
        return butRepository.findAllByMatchAndJoueurAndDeletedIsFalse(matchMapper.toEntity(matchDTO), joueurMapper.toEntity(joueurDTO))
            .stream()
            .map(butMapper::toDto).collect(Collectors.toList());
    }



    @Override
    @Transactional(readOnly = true)
    public Optional<ButDTO> findByHashcode(String hashcode) {
        log.debug("Request to get But : {}", hashcode);
        return butRepository.findByHashcodeAndDeletedIsFalse(hashcode)
            .map(butMapper::toDto);
    }

    @Override
    public List<ButDTO> findAll() {
        return butRepository.findAllByDeletedIsFalse().stream()
            .map(butMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public void reIndex() {
        log.info("Request to reindex all Buts");
        butSearchRepository.deleteAll();
        findAll().stream().map(butStatsMapper::toStats).forEach(butSearchRepository::save);
    }
}
