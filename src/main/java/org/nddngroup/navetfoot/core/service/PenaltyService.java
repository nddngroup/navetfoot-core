package org.nddngroup.navetfoot.core.service;

import org.nddngroup.navetfoot.core.domain.Penalty;
import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;
import org.nddngroup.navetfoot.core.service.dto.MatchDTO;
import org.nddngroup.navetfoot.core.service.dto.PenaltyDTO;
import org.nddngroup.navetfoot.core.service.dto.TirAuButDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Penalty}.
 */
public interface PenaltyService {

    /**
     * Save a penalty.
     *
     * @param penaltyDTO the entity to save.
     * @return the persisted entity.
     */
    PenaltyDTO save(PenaltyDTO penaltyDTO);

    /**
     * Get all the penalties.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PenaltyDTO> findAll(Pageable pageable);


    /**
     * Get the "id" penalty.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PenaltyDTO> findOne(Long id);

    /**
     * Delete the "id" penalty.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the penalty corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PenaltyDTO> search(String query, Pageable pageable);
    List<PenaltyDTO> findByTirAuBut(TirAuButDTO tirAuButDTO);
    List<PenaltyDTO> findByMatch(MatchDTO matchDTO);
    List<PenaltyDTO> findByMatchAndEquipe(MatchDTO matchDTO, Equipe equipe);
    Optional<PenaltyDTO> findByHashcode(String hashcode);
    List<PenaltyDTO> findAll();
    void reIndex();
}
