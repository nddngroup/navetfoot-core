package org.nddngroup.navetfoot.core.service.dto;

import org.nddngroup.navetfoot.core.domain.EtapeCompetition;

import java.time.ZonedDateTime;
import java.io.Serializable;

/**
 * A DTO for the {@link EtapeCompetition} entity.
 */
public class EtapeCompetitionDTO implements Serializable {

    private Long id;

    private String nom;

    private String hashcode;

    private String code;

    private ZonedDateTime dateDebut;

    private ZonedDateTime dateFin;

    private ZonedDateTime createdAt;

    private ZonedDateTime updatedAt;

    private Boolean deleted;

    private Boolean hasPoule;

    private Boolean allerRetour;


    private Long competitionSaisonCategoryId;

    private Long competitionSaisonId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getHashcode() {
        return hashcode;
    }

    public void setHashcode(String hashcode) {
        this.hashcode = hashcode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public ZonedDateTime getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(ZonedDateTime dateDebut) {
        this.dateDebut = dateDebut;
    }

    public ZonedDateTime getDateFin() {
        return dateFin;
    }

    public void setDateFin(ZonedDateTime dateFin) {
        this.dateFin = dateFin;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Boolean isHasPoule() {
        return hasPoule;
    }

    public void setHasPoule(Boolean hasPoule) {
        this.hasPoule = hasPoule;
    }

    public Boolean isAllerRetour() {
        return allerRetour;
    }

    public void setAllerRetour(Boolean allerRetour) {
        this.allerRetour = allerRetour;
    }

    public Long getCompetitionSaisonCategoryId() {
        return competitionSaisonCategoryId;
    }

    public void setCompetitionSaisonCategoryId(Long competitionSaisonCategoryId) {
        this.competitionSaisonCategoryId = competitionSaisonCategoryId;
    }

    public Long getCompetitionSaisonId() {
        return competitionSaisonId;
    }

    public void setCompetitionSaisonId(Long competitionSaisonId) {
        this.competitionSaisonId = competitionSaisonId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EtapeCompetitionDTO)) {
            return false;
        }

        return id != null && id.equals(((EtapeCompetitionDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "EtapeCompetitionDTO{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", hashcode='" + getHashcode() + "'" +
            ", code='" + getCode() + "'" +
            ", dateDebut='" + getDateDebut() + "'" +
            ", dateFin='" + getDateFin() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", deleted='" + isDeleted() + "'" +
            ", hasPoule='" + isHasPoule() + "'" +
            ", allerRetour='" + isAllerRetour() + "'" +
            ", competitionSaisonCategoryId=" + getCompetitionSaisonCategoryId() +
            ", competitionSaisonId=" + getCompetitionSaisonId() +
            "}";
    }
}
