package org.nddngroup.navetfoot.core.service.impl;

import org.nddngroup.navetfoot.core.service.PaysService;
import org.nddngroup.navetfoot.core.domain.Pays;
import org.nddngroup.navetfoot.core.repository.PaysRepository;
import org.nddngroup.navetfoot.core.repository.search.PaysSearchRepository;
import org.nddngroup.navetfoot.core.service.dto.PaysDTO;
import org.nddngroup.navetfoot.core.service.mapper.PaysMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link Pays}.
 */
@Service
@Transactional
public class PaysServiceImpl implements PaysService {

    private final Logger log = LoggerFactory.getLogger(PaysServiceImpl.class);

    private final PaysRepository paysRepository;

    private final PaysMapper paysMapper;

    private final PaysSearchRepository paysSearchRepository;

    public PaysServiceImpl(PaysRepository paysRepository, PaysMapper paysMapper, PaysSearchRepository paysSearchRepository) {
        this.paysRepository = paysRepository;
        this.paysMapper = paysMapper;
        this.paysSearchRepository = paysSearchRepository;
    }

    @Override
    public PaysDTO save(PaysDTO paysDTO) {
        log.debug("Request to save Pays : {}", paysDTO);
        Pays pays = paysMapper.toEntity(paysDTO);
        pays = paysRepository.save(pays);
        PaysDTO result = paysMapper.toDto(pays);
        paysSearchRepository.save(pays);
        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<PaysDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Pays");
        return paysRepository.findAll(pageable)
            .map(paysMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Pays> findAll() {
        log.debug("Request to get all Pays");
        return paysRepository.findAll();
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<PaysDTO> findOne(Long id) {
        log.debug("Request to get Pays : {}", id);
        return paysRepository.findById(id)
            .map(paysMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Pays : {}", id);
        paysRepository.deleteById(id);
        paysSearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<PaysDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Pays for query {}", query);
        return paysSearchRepository.search(queryStringQuery(query), pageable)
            .map(paysMapper::toDto);
    }

    @Override
    public List<PaysDTO> findAllPays() {
        return paysRepository.findAll().stream()
            .map(paysMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public void reIndex() {
        log.info("Request to reindex all Pays");
        paysSearchRepository.deleteAll();
        findAllPays().stream().map(paysMapper::toEntity).forEach(paysSearchRepository::save);
    }
}
