package org.nddngroup.navetfoot.core.service.dto;

import org.nddngroup.navetfoot.core.domain.FeuilleDeMatch;

import java.time.ZonedDateTime;
import java.io.Serializable;

/**
 * A DTO for the {@link FeuilleDeMatch} entity.
 */
public class FeuilleDeMatchDTO implements Serializable {

    private Long id;

    private String code;

    private String hashcode;

    private ZonedDateTime updatedAt;

    private ZonedDateTime createdAt;

    private Boolean deleted;


    private Long matchId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getHashcode() {
        return hashcode;
    }

    public void setHashcode(String hashcode) {
        this.hashcode = hashcode;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Long getMatchId() {
        return matchId;
    }

    public void setMatchId(Long matchId) {
        this.matchId = matchId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FeuilleDeMatchDTO)) {
            return false;
        }

        return id != null && id.equals(((FeuilleDeMatchDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FeuilleDeMatchDTO{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", hashcode='" + getHashcode() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", deleted='" + isDeleted() + "'" +
            ", matchId=" + getMatchId() +
            "}";
    }
}
