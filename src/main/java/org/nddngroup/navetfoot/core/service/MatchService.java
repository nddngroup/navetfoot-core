package org.nddngroup.navetfoot.core.service;

import org.nddngroup.navetfoot.core.domain.Match;
import org.nddngroup.navetfoot.core.domain.custom.stats.MatchDetail;
import org.nddngroup.navetfoot.core.service.dto.*;
import org.nddngroup.navetfoot.core.service.dto.AssociationDTO;
import org.nddngroup.navetfoot.core.service.dto.EtapeCompetitionDTO;
import org.nddngroup.navetfoot.core.service.dto.MatchDTO;
import org.nddngroup.navetfoot.core.service.dto.OrganisationDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Match}.
 */
public interface MatchService {

    /**
     * Save a match.
     *
     * @param matchDTO the entity to save.
     * @return the persisted entity.
     */
    MatchDTO save(MatchDTO matchDTO);

    /**
     * Get all the matches.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<MatchDTO> findAll(Pageable pageable);
    /**
     * Get all the MatchDTO where TirAuBut is {@code null}.
     *
     * @return the {@link List} of entities.
     */
    List<MatchDTO> findAllWhereTirAuButIsNull();


    /**
     * Get the "id" match.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<MatchDTO> findOne(Long id);
    Optional<MatchDetail> findById(Long matchId);

    /**
     * Delete the "id" match.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the match corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<MatchDTO> search(String query, Pageable pageable);

    List<MatchDTO> findByEtapeCompetition(EtapeCompetitionDTO etapeCompetitionDTO);
    List<MatchDTO> findByEtapeCompetitions(List<EtapeCompetitionDTO> etapeCompetitionDTOS);
    Page<MatchDetail> findByIdIn(List<Long> matchIds, Pageable pageable);
    Page<MatchDetail> findAllByAssociationIdIn(List<Long> clubIds, LocalDate dateStart, Pageable pageable);
    Page<MatchDetail> findAllByAssociationIdIn(List<Long> clubIds, Pageable pageable);
    Page<MatchDetail> findAllMatchDetails(Pageable pageable);
    List<MatchDTO> findByEtapeCompetitionAndLocal(EtapeCompetitionDTO etapeCompetitionDTO, AssociationDTO associationDTO);
    List<MatchDTO> findByEtapeCompetitionAndVisiteur(EtapeCompetitionDTO etapeCompetitionDTO, AssociationDTO associationDTO);
    List<MatchDTO> findByEtapeCompetition(EtapeCompetitionDTO etapeCompetitionDTO, Integer journee);
    Optional<MatchDTO> findByHashcode(String hashcode);
    MatchDTO save(MatchDTO matchDTO, OrganisationDTO organisationDTO);
    Page<MatchDetail> search(OrganisationDTO organisationDTO, String query, Pageable pageable);
    List<MatchDTO> findAll();
    void reIndex();
}
