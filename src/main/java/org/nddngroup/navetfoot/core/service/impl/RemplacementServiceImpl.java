package org.nddngroup.navetfoot.core.service.impl;

import org.nddngroup.navetfoot.core.domain.Remplacement;
import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;
import org.nddngroup.navetfoot.core.repository.RemplacementRepository;
import org.nddngroup.navetfoot.core.repository.search.RemplacementSearchRepository;
import org.nddngroup.navetfoot.core.service.RemplacementService;
import org.nddngroup.navetfoot.core.service.dto.MatchDTO;
import org.nddngroup.navetfoot.core.service.dto.RemplacementDTO;
import org.nddngroup.navetfoot.core.service.mapper.MatchMapper;
import org.nddngroup.navetfoot.core.service.mapper.RemplacementMapper;
import org.nddngroup.navetfoot.core.service.mapper.custom.RemplacementStatsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link Remplacement}.
 */
@Service
@Transactional
public class RemplacementServiceImpl implements RemplacementService {

    private final Logger log = LoggerFactory.getLogger(RemplacementServiceImpl.class);

    private final RemplacementRepository remplacementRepository;

    private final RemplacementMapper remplacementMapper;

    private final RemplacementSearchRepository remplacementSearchRepository;

    private final RemplacementStatsMapper remplacementStatsMapper;
    @Autowired
    private MatchMapper matchMapper;

    public RemplacementServiceImpl(RemplacementRepository remplacementRepository,
                                   RemplacementMapper remplacementMapper,
                                   RemplacementSearchRepository remplacementSearchRepository,
                                   RemplacementStatsMapper remplacementStatsMapper
                                   ) {
        this.remplacementRepository = remplacementRepository;
        this.remplacementMapper = remplacementMapper;
        this.remplacementSearchRepository = remplacementSearchRepository;
        this.remplacementStatsMapper = remplacementStatsMapper;
    }

    @Override
    public RemplacementDTO save(RemplacementDTO remplacementDTO) {
        log.debug("Request to save Remplacement : {}", remplacementDTO);

        remplacementDTO.setUpdatedAt(ZonedDateTime.now());
        if (remplacementDTO.getCreatedAt() == null) {
            remplacementDTO.setCreatedAt(ZonedDateTime.now());
        }

        Remplacement remplacement = remplacementMapper.toEntity(remplacementDTO);
        remplacement = remplacementRepository.save(remplacement);
        RemplacementDTO result = remplacementMapper.toDto(remplacement);

        if (Boolean.TRUE.equals(remplacementDTO.isDeleted())) {
            remplacementSearchRepository.deleteById(result.getId());
        } else {
            remplacementSearchRepository.save(remplacementStatsMapper.toStats(result));
        }

        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<RemplacementDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Remplacements");
        return remplacementRepository.findAll(pageable)
            .map(remplacementMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<RemplacementDTO> findOne(Long id) {
        log.debug("Request to get Remplacement : {}", id);
        return remplacementRepository.findById(id)
            .map(remplacementMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Remplacement : {}", id);
        remplacementRepository.deleteById(id);
        remplacementSearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<RemplacementDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Remplacements for query {}", query);
        return remplacementSearchRepository.search(queryStringQuery(query), pageable)
            .map(remplacementMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<RemplacementDTO> findByMatch(MatchDTO matchDTO) {
        log.debug("Request to get all remplacements by matchs ");
        return remplacementRepository.findAllByMatchAndDeletedIsFalse(matchMapper.toEntity(matchDTO))
            .stream()
            .map(remplacementMapper::toDto).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<RemplacementDTO> findByMatchAndEquipe(MatchDTO matchDTO, Equipe equipe) {
        log.debug("Request to get all remplacements by matchs and equipe ");
        return remplacementRepository.findAllByMatchAndEquipeAndDeletedIsFalse(matchMapper.toEntity(matchDTO), equipe)
            .stream()
            .map(remplacementMapper::toDto).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<RemplacementDTO> findByHashcode(String hashcode) {
        log.debug("Request to get Remplacement : {}", hashcode);
        return remplacementRepository.findByHashcodeAndDeletedIsFalse(hashcode)
            .map(remplacementMapper::toDto);
    }

    @Override
    public List<RemplacementDTO> findAll() {
        return remplacementRepository.findAllByDeletedIsFalse().stream()
            .map(remplacementMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public void reIndex() {
        log.debug("Request to reindex all Remplacements");
        remplacementSearchRepository.deleteAll();
        findAll().stream().map(remplacementStatsMapper::toStats).forEach(remplacementSearchRepository::save);
    }
}
