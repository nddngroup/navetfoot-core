package org.nddngroup.navetfoot.core.service;

import org.nddngroup.navetfoot.core.domain.Corner;
import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;
import org.nddngroup.navetfoot.core.service.dto.CornerDTO;
import org.nddngroup.navetfoot.core.service.dto.MatchDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Corner}.
 */
public interface CornerService {

    /**
     * Save a corner.
     *
     * @param cornerDTO the entity to save.
     * @return the persisted entity.
     */
    CornerDTO save(CornerDTO cornerDTO);

    /**
     * Get all the corners.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CornerDTO> findAll(Pageable pageable);


    /**
     * Get the "id" corner.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CornerDTO> findOne(Long id);

    /**
     * Delete the "id" corner.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the corner corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CornerDTO> search(String query, Pageable pageable);
    List<CornerDTO> findByMatch(MatchDTO matchDTO);
    List<CornerDTO> findByMatchAndEquipe(MatchDTO matchDTO, Equipe equipe);
    Optional<CornerDTO> findByHashcode(String hashcode);
    List<CornerDTO> findAll();
    void reIndex();
}
