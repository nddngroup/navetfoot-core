package org.nddngroup.navetfoot.core.service.dto;

import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;

import org.nddngroup.navetfoot.core.domain.Tir;
import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;

/**
 * A DTO for the {@link Tir} entity.
 */
public class TirDTO implements Serializable {

    private Long id;

    @NotNull
    private String code;

    @NotNull
    private String hashcode;

    private Integer instant;

    private Boolean deleted;

    private Boolean cadre;

    private ZonedDateTime createdAt;

    private ZonedDateTime updatedAt;

    private Equipe equipe;


    private Long matchId;

    private Long joueurId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getHashcode() {
        return hashcode;
    }

    public void setHashcode(String hashcode) {
        this.hashcode = hashcode;
    }

    public Integer getInstant() {
        return instant;
    }

    public void setInstant(Integer instant) {
        this.instant = instant;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Boolean isCadre() {
        return cadre;
    }

    public void setCadre(Boolean cadre) {
        this.cadre = cadre;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Equipe getEquipe() {
        return equipe;
    }

    public void setEquipe(Equipe equipe) {
        this.equipe = equipe;
    }

    public Long getMatchId() {
        return matchId;
    }

    public void setMatchId(Long matchId) {
        this.matchId = matchId;
    }

    public Long getJoueurId() {
        return joueurId;
    }

    public void setJoueurId(Long joueurId) {
        this.joueurId = joueurId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TirDTO)) {
            return false;
        }

        return id != null && id.equals(((TirDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TirDTO{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", hashcode='" + getHashcode() + "'" +
            ", instant=" + getInstant() +
            ", deleted='" + isDeleted() + "'" +
            ", cadre='" + isCadre() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", equipe='" + getEquipe() + "'" +
            ", matchId=" + getMatchId() +
            ", joueurId=" + getJoueurId() +
            "}";
    }
}
