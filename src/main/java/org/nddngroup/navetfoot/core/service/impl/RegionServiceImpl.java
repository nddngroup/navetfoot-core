package org.nddngroup.navetfoot.core.service.impl;

import org.nddngroup.navetfoot.core.service.RegionService;
import org.nddngroup.navetfoot.core.domain.Region;
import org.nddngroup.navetfoot.core.repository.RegionRepository;
import org.nddngroup.navetfoot.core.repository.search.RegionSearchRepository;
import org.nddngroup.navetfoot.core.service.dto.PaysDTO;
import org.nddngroup.navetfoot.core.service.dto.RegionDTO;
import org.nddngroup.navetfoot.core.service.mapper.PaysMapper;
import org.nddngroup.navetfoot.core.service.mapper.RegionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link Region}.
 */
@Service
@Transactional
public class RegionServiceImpl implements RegionService {

    private final Logger log = LoggerFactory.getLogger(RegionServiceImpl.class);

    private final RegionRepository regionRepository;

    private final RegionMapper regionMapper;

    private final RegionSearchRepository regionSearchRepository;

    @Autowired
    private PaysMapper paysMapper;

    public RegionServiceImpl(RegionRepository regionRepository, RegionMapper regionMapper, RegionSearchRepository regionSearchRepository) {
        this.regionRepository = regionRepository;
        this.regionMapper = regionMapper;
        this.regionSearchRepository = regionSearchRepository;
    }

    @Override
    public RegionDTO save(RegionDTO regionDTO) {
        log.debug("Request to save Region : {}", regionDTO);
        Region region = regionMapper.toEntity(regionDTO);
        region = regionRepository.save(region);
        RegionDTO result = regionMapper.toDto(region);
        regionSearchRepository.save(region);
        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<RegionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Regions");
        return regionRepository.findAll(pageable)
            .map(regionMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Region> findAll() {
        log.debug("Request to get all Regions");
        return regionRepository.findAll();
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<RegionDTO> findOne(Long id) {
        log.debug("Request to get Region : {}", id);
        return regionRepository.findById(id)
            .map(regionMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Region : {}", id);
        regionRepository.deleteById(id);
        regionSearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<RegionDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Regions for query {}", query);
        return regionSearchRepository.search(queryStringQuery(query), pageable)
            .map(regionMapper::toDto);
    }

    @Override
    public List<RegionDTO> findRegionsByPays(PaysDTO pays) {
        log.debug("Request to get regions by pays id: {}", pays.getId());
        List<Region> regions = regionRepository.findRegionsByPays(paysMapper.toEntity(pays));
        List<RegionDTO> regionDTOS = new ArrayList<>();

        for(Region region: regions){
            regionDTOS.add(regionMapper.toDto(region));
        }
        return regionDTOS;
    }

    @Override
    public List<RegionDTO> findAllRegions() {
        return regionRepository.findAll().stream()
            .map(regionMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public void reIndex() {
        log.info("Request to reindex all Regions");
        regionSearchRepository.deleteAll();
        findAllRegions().stream().map(regionMapper::toEntity).forEach(regionSearchRepository::save);
    }
}
