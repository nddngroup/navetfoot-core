package org.nddngroup.navetfoot.core.service.impl;

import org.nddngroup.navetfoot.core.service.CompetitionSaisonCategoryService;
import org.nddngroup.navetfoot.core.domain.CompetitionSaisonCategory;
import org.nddngroup.navetfoot.core.repository.CompetitionSaisonCategoryRepository;
import org.nddngroup.navetfoot.core.repository.search.CompetitionSaisonCategorySearchRepository;
import org.nddngroup.navetfoot.core.service.custom.CommunService;
import org.nddngroup.navetfoot.core.service.dto.*;
import org.nddngroup.navetfoot.core.service.dto.*;
import org.nddngroup.navetfoot.core.service.mapper.CategoryMapper;
import org.nddngroup.navetfoot.core.service.mapper.CompetitionSaisonCategoryMapper;
import org.nddngroup.navetfoot.core.service.mapper.CompetitionSaisonMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link CompetitionSaisonCategory}.
 */
@Service
@Transactional
public class CompetitionSaisonCategoryServiceImpl implements CompetitionSaisonCategoryService {

    private final Logger log = LoggerFactory.getLogger(CompetitionSaisonCategoryServiceImpl.class);

    private final CompetitionSaisonCategoryRepository competitionSaisonCategoryRepository;

    private final CompetitionSaisonCategoryMapper competitionSaisonCategoryMapper;

    private final CompetitionSaisonCategorySearchRepository competitionSaisonCategorySearchRepository;

    @Autowired
    private CompetitionSaisonMapper competitionSaisonMapper;
    @Autowired
    private CategoryMapper categoryMapper;
    @Autowired
    private CommunService communService;

    public CompetitionSaisonCategoryServiceImpl(CompetitionSaisonCategoryRepository competitionSaisonCategoryRepository, CompetitionSaisonCategoryMapper competitionSaisonCategoryMapper, CompetitionSaisonCategorySearchRepository competitionSaisonCategorySearchRepository) {
        this.competitionSaisonCategoryRepository = competitionSaisonCategoryRepository;
        this.competitionSaisonCategoryMapper = competitionSaisonCategoryMapper;
        this.competitionSaisonCategorySearchRepository = competitionSaisonCategorySearchRepository;
    }

    @Override
    public CompetitionSaisonCategoryDTO save(CompetitionSaisonCategoryDTO competitionSaisonCategoryDTO) {
        log.debug("Request to save CompetitionSaisonCategory : {}", competitionSaisonCategoryDTO);
        CompetitionSaisonCategory competitionSaisonCategory = competitionSaisonCategoryMapper.toEntity(competitionSaisonCategoryDTO);
        competitionSaisonCategory = competitionSaisonCategoryRepository.save(competitionSaisonCategory);
        CompetitionSaisonCategoryDTO result = competitionSaisonCategoryMapper.toDto(competitionSaisonCategory);

        if (competitionSaisonCategoryDTO.isDeleted()) {
            competitionSaisonCategorySearchRepository.deleteById(competitionSaisonCategoryDTO.getId());
        } else {
            competitionSaisonCategorySearchRepository.save(competitionSaisonCategoryMapper.toEntity(result));
        }

        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<CompetitionSaisonCategoryDTO> findAll(Pageable pageable) {
        log.debug("Request to get all CompetitionSaisonCategories");
        return competitionSaisonCategoryRepository.findAll(pageable)
            .map(competitionSaisonCategoryMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<CompetitionSaisonCategoryDTO> findOne(Long id) {
        log.debug("Request to get CompetitionSaisonCategory : {}", id);
        return competitionSaisonCategoryRepository.findById(id)
            .map(competitionSaisonCategoryMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete CompetitionSaisonCategory : {}", id);
        competitionSaisonCategoryRepository.deleteById(id);
        competitionSaisonCategorySearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<CompetitionSaisonCategoryDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of CompetitionSaisonCategories for query {}", query);
        return competitionSaisonCategorySearchRepository.search(queryStringQuery(query), pageable)
            .map(competitionSaisonCategoryMapper::toDto);
    }

    @Override
    public List<CompetitionSaisonCategoryDTO> findByCompetitionSaison(CompetitionSaisonDTO competitionSaisonDTO) {
        log.debug("Request to get all CompetitionSaisonCategories by competitionSaison");
        return competitionSaisonCategoryRepository.findAllByCompetitionSaisonAndDeletedIsFalse(competitionSaisonMapper.toEntity(competitionSaisonDTO))
            .stream()
            .map(competitionSaisonCategoryMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public Optional<CompetitionSaisonCategoryDTO> findByCompetitionSaisonAndCategory(CompetitionSaisonDTO competitionSaisonDTO, CategoryDTO categoryDTO) {
        log.debug("Request to get all CompetitionSaisonCategories by competitionSaison and category");
        return competitionSaisonCategoryRepository.findAllByCompetitionSaisonAndCategoryAndDeletedIsFalse(
                                                                competitionSaisonMapper.toEntity(competitionSaisonDTO),
                                                                categoryMapper.toEntity(categoryDTO))
            .map(competitionSaisonCategoryMapper::toDto);
    }

    @Override
    public CompetitionSaisonCategoryDTO save(CategoryDTO categoryDTO, OrganisationDTO organisationDTO, SaisonDTO saisonDTO, CompetitionDTO competitionDTO) {
        log.debug("Request to add category {} to saison {} and competition {}", categoryDTO, saisonDTO, competitionDTO);

        // vérifier si la categorie n'est pas déja ajouté à la compétition pour cette saison
        CompetitionSaisonDTO competitionSaisonDTO = communService.checkCategoryForCompetition(competitionDTO.getId(), saisonDTO.getId(), categoryDTO.getId());

        CompetitionSaisonCategoryDTO competitionSaisonCategoryDTO = new CompetitionSaisonCategoryDTO();

        competitionSaisonCategoryDTO.setCategoryId(categoryDTO.getId());
        competitionSaisonCategoryDTO.setCompetitionSaisonId(competitionSaisonDTO.getId());
        competitionSaisonCategoryDTO.setHashcode(communService.toHash(competitionSaisonDTO.getId() + "." + categoryDTO.getId()));
        competitionSaisonCategoryDTO.setCode("REF_" + organisationDTO.getId());
        competitionSaisonCategoryDTO.setCreatedAt(ZonedDateTime.now());
        competitionSaisonCategoryDTO.setUpdatedAt(ZonedDateTime.now());
        competitionSaisonCategoryDTO.setDeleted(false);

        /*competitionSaisonCategoryDTO = save(competitionSaisonCategoryDTO);

        competitionSaisonCategoryDTO.setCode(competitionSaisonCategoryDTO.getCode() + "_" + competitionSaisonCategoryDTO.getId());
*/
        return save(competitionSaisonCategoryDTO);
    }

    @Override
    public List<CompetitionSaisonCategoryDTO> findAll() {
        return competitionSaisonCategoryRepository.findAllByDeletedIsFalse().stream()
            .map(competitionSaisonCategoryMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public void reIndex() {
        log.info("Request to reindex all CompetitionSaisonCategory");
        competitionSaisonCategorySearchRepository.deleteAll();
        findAll().stream().map(competitionSaisonCategoryMapper::toEntity).forEach(competitionSaisonCategorySearchRepository::save);
    }
}
