package org.nddngroup.navetfoot.core.service.mapper;


import org.nddngroup.navetfoot.core.domain.*;
import org.nddngroup.navetfoot.core.domain.Match;
import org.nddngroup.navetfoot.core.service.dto.MatchDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Match} and its DTO {@link MatchDTO}.
 */
@Mapper(componentModel = "spring", uses = {AssociationMapper.class, EtapeCompetitionMapper.class})
public interface MatchMapper extends EntityMapper<MatchDTO, Match> {

    @Mapping(source = "visiteur.id", target = "visiteurId")
    @Mapping(source = "local.id", target = "localId")
    @Mapping(source = "etapeCompetition.id", target = "etapeCompetitionId")
    @Mapping(source = "etapeCompetition.nom", target = "etapeCompetitionNom")
    MatchDTO toDto(Match match);

    @Mapping(target = "buts", ignore = true)
    @Mapping(target = "removeButs", ignore = true)
    @Mapping(target = "cartons", ignore = true)
    @Mapping(target = "removeCartons", ignore = true)
    @Mapping(target = "coupFrancs", ignore = true)
    @Mapping(target = "removeCoupFrancs", ignore = true)
    @Mapping(target = "penalties", ignore = true)
    @Mapping(target = "removePenalties", ignore = true)
    @Mapping(target = "corners", ignore = true)
    @Mapping(target = "removeCorners", ignore = true)
    @Mapping(target = "delegueMatches", ignore = true)
    @Mapping(target = "removeDelegueMatch", ignore = true)
    @Mapping(source = "visiteurId", target = "visiteur")
    @Mapping(source = "localId", target = "local")
    @Mapping(target = "tirAuBut", ignore = true)
    @Mapping(source = "etapeCompetitionId", target = "etapeCompetition")
    Match toEntity(MatchDTO matchDTO);

    default Match fromId(Long id) {
        if (id == null) {
            return null;
        }
        Match match = new Match();
        match.setId(id);
        return match;
    }
}
