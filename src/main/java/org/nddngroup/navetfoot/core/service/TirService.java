package org.nddngroup.navetfoot.core.service;

import org.nddngroup.navetfoot.core.domain.Tir;
import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;
import org.nddngroup.navetfoot.core.service.dto.MatchDTO;
import org.nddngroup.navetfoot.core.service.dto.TirDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Tir}.
 */
public interface TirService {

    /**
     * Save a tir.
     *
     * @param tirDTO the entity to save.
     * @return the persisted entity.
     */
    TirDTO save(TirDTO tirDTO);

    /**
     * Get all the tirs.
     *
     * @return the list of entities.
     */
    List<TirDTO> findAll();


    /**
     * Get the "id" tir.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<TirDTO> findOne(Long id);

    /**
     * Delete the "id" tir.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the tir corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @return the list of entities.
     */
    List<TirDTO> search(String query);
    List<TirDTO> findByMatch(MatchDTO matchDTO);
    List<TirDTO> findByMatchAndEquipe(MatchDTO matchDTO, Equipe equipe);
    List<TirDTO> findByMatchAndEquipeAndCadre(MatchDTO matchDTO, Equipe equipe, Boolean cadre);
    Optional<TirDTO> findByHashcode(String hashcode);
    List<TirDTO> findAllByDeletedIsFalse();
    void reIndex();
}
