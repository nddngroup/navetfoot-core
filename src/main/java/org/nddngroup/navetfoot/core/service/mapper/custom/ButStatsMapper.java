package org.nddngroup.navetfoot.core.service.mapper.custom;

import org.nddngroup.navetfoot.core.domain.But;
import org.nddngroup.navetfoot.core.service.*;
import org.nddngroup.navetfoot.core.service.*;
import org.nddngroup.navetfoot.core.service.dto.ButDTO;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring")
public abstract class ButStatsMapper {
    @Autowired
    protected EtapeCompetitionService etapeCompetitionService;
    @Autowired
    protected CompetitionSaisonService competitionSaisonService;
    @Autowired
    protected SaisonService saisonService;
    @Autowired
    protected CompetitionService competitionService;
    @Autowired
    protected OrganisationService organisationService;
    @Autowired
    protected AssociationService associationService;
    @Autowired
    protected MatchService matchService;
    @Autowired
    protected JoueurService joueurService;
    @Autowired
    protected MatchStatsMapper matchStatsMapper;

    public But toStats(ButDTO but) {
        var butStats = new But()
            .code(but.getCode())
            .hashcode(but.getHashcode())
            .cause(but.getCause())
            .equipe(but.getEquipe())
            .instant(but.getInstant())
            .deleted(but.isDeleted())
            .annule(but.isAnnule())
            .createdAt(but.getCreatedAt())
            .updatedAt(but.getUpdatedAt());

        butStats.setId(but.getId());
        // find joueur
        joueurService.findOne(but.getJoueurId())
            .ifPresent(joueurDTO -> {
                butStats.setJoueurId(joueurDTO.getId());
                butStats.setJoueurNom(joueurDTO.getNom());
                butStats.setJoueurPrenom(joueurDTO.getPrenom());
                butStats.setJoueurImage(joueurDTO.getImageUrl());
            });

        // find match
        matchService.findOne(but.getMatchId())
            .ifPresent(matchDTO -> {
                butStats.setMatchId(matchDTO.getId());

                var matchStats = matchStatsMapper.toStats(matchDTO);
                butStats.setLocalId(matchStats.getLocalId());
                butStats.setLocalNom(matchStats.getLocalNom());

                butStats.setVisiteurId(matchStats.getVisiteurId());
                butStats.setVisiteurNom(matchStats.getVisiteurNom());

                butStats.setEtapeCompetitionId(matchStats.getEtapeCompetitionId());
                butStats.setEtapeCompetitionNom(matchStats.getEtapeCompetitionNom());

                butStats.setSaisonId(matchStats.getSaisonId());
                butStats.setSaisonLibelle(matchStats.getSaisonLibelle());

                butStats.setCompetitionId(matchStats.getCompetitionId());
                butStats.setCompetitionNom(matchStats.getCompetitionNom());

                butStats.setOrganisationId(matchStats.getOrganisationId());
                butStats.setOrganisationNom(matchStats.getOrganisationNom());
            });

        return butStats;
    }
}
