package org.nddngroup.navetfoot.core.service.impl;

import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.IdsQueryBuilder;
import org.joda.time.LocalDate;
import org.nddngroup.navetfoot.core.domain.Joueur;
import org.nddngroup.navetfoot.core.exception.ApiRequestException;
import org.nddngroup.navetfoot.core.exception.ExceptionCode;
import org.nddngroup.navetfoot.core.exception.ExceptionLevel;
import org.nddngroup.navetfoot.core.repository.JoueurRepository;
import org.nddngroup.navetfoot.core.repository.search.ContratSearchRepository;
import org.nddngroup.navetfoot.core.repository.search.JoueurSearchRepository;
import org.nddngroup.navetfoot.core.service.ContratService;
import org.nddngroup.navetfoot.core.service.JoueurCategoryService;
import org.nddngroup.navetfoot.core.service.JoueurService;
import org.nddngroup.navetfoot.core.service.custom.CommunService;
import org.nddngroup.navetfoot.core.service.dto.*;
import org.nddngroup.navetfoot.core.service.dto.*;
import org.nddngroup.navetfoot.core.service.mapper.JoueurMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.boolQuery;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link Joueur}.
 */
@Service
@Transactional
public class JoueurServiceImpl implements JoueurService {

    private final Logger log = LoggerFactory.getLogger(JoueurServiceImpl.class);
    private static final AtomicLong counter = new AtomicLong();

    private final JoueurRepository joueurRepository;

    private final JoueurMapper joueurMapper;

    private final JoueurSearchRepository joueurSearchRepository;

    @Autowired
    private CommunService communService;
    @Autowired
    private JoueurCategoryService joueurCategoryService;
    @Autowired
    private ContratService contratService;
    @Autowired
    private ContratSearchRepository contratSearchRepository;

    public JoueurServiceImpl(JoueurRepository joueurRepository, JoueurMapper joueurMapper, JoueurSearchRepository joueurSearchRepository) {
        this.joueurRepository = joueurRepository;
        this.joueurMapper = joueurMapper;
        this.joueurSearchRepository = joueurSearchRepository;
    }

    @Override
    public JoueurDTO save(JoueurDTO joueurDTO) {
        log.debug("Request to save Joueur : {}", joueurDTO);
        Joueur joueur = joueurMapper.toEntity(joueurDTO);
        joueur = joueurRepository.save(joueur);
        JoueurDTO result = joueurMapper.toDto(joueur);

        if (joueurDTO.isDeleted()) {
            joueurSearchRepository.deleteById(joueurDTO.getId());
        } else {
            joueurSearchRepository.save(joueurMapper.toEntity(result));
        }

        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<JoueurDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Joueurs");
        return joueurRepository.findAll(pageable)
            .map(joueurMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<JoueurDTO> findOne(Long id) {
        log.debug("Request to get Joueur : {}", id);
        return joueurRepository.findById(id)
            .map(joueurMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Joueur : {}", id);
        joueurRepository.deleteById(id);
        joueurSearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<JoueurDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Joueurs for query {}", query);
        return joueurSearchRepository.search(queryStringQuery(query), pageable)
            .map(joueurMapper::toDto);
    }

    @Override
    public Optional<JoueurDTO> findByIdentifiant(String numero) {
        log.debug("Request to get Joueur by licence : {}", numero);
        return joueurRepository.findByIdentifiantAndDeletedIsFalse(numero)
            .map(joueurMapper::toDto);
    }

    @Override
    public Page<JoueurDTO> findByIdIn(List<Long> joueurIds, Pageable pageable) {
        log.debug("Request to get all Joueurs by ids");
        return joueurRepository.findAllByIdInAndDeletedIsFalse(joueurIds, pageable)
            .map(joueurMapper::toDto);
    }

    @Override
    public Optional<JoueurDTO> findByHashcode(String hashcode) {
        log.debug("Request to get joueur by hashcode");
        return joueurRepository.findByHashcodeAndDeletedIsFalse(hashcode)
            .map(joueurMapper::toDto);
    }

    @Override
    public List<JoueurDTO> save(OrganisationDTO organisationDTO, SaisonDTO saisonDTO, AssociationDTO associationDTO, List<JoueurDTO> joueurDTOS) {
        log.debug("Request to add list of joueurs: {}", joueurDTOS);

        List<JoueurDTO> resultList = new ArrayList<>();

        if (joueurDTOS != null) {
            for (JoueurDTO joueurDTO : joueurDTOS) {
                if (joueurDTO.getIdentifiant() == null || joueurDTO.getIdentifiant().isEmpty()) {
                    joueurDTO.setIdentifiant(generateJoueurIdentifiant(associationDTO.getId()));
                }

                // vérifier l'identifiant du joueur
                if (!this.communService.checkUniqueIdentifiant(joueurDTO)) {
                    log.debug("L'identifiant est déja utilisé");
                    throw new ApiRequestException(ExceptionCode.IDENTIFIANT_EXISTS.getMessage(), ExceptionCode.IDENTIFIANT_EXISTS.getValue(), ExceptionLevel.WARNING);
                }

                if (joueurDTO.getId() == null) {
                    joueurDTO.setDeleted(false);
                    joueurDTO.setCreatedAt(ZonedDateTime.now());
                    joueurDTO.setUpdatedAt(ZonedDateTime.now());

                    joueurDTO.setCode("REF_" + organisationDTO.getId());

                    String hashcode = organisationDTO.getId() + ZonedDateTime.now().toString();
                    joueurDTO.setHashcode(this.communService.toHash(hashcode));

                    joueurDTO = save(joueurDTO);

                    if (joueurDTO != null) {
                        joueurDTO.setCode(joueurDTO.getCode() + "_" + joueurDTO.getId());
                        joueurDTO.setHashcode(this.communService.toHash(joueurDTO.getId().toString()));
                        joueurDTO = save(joueurDTO);
                    }
                } else {
                    // vérifier si le joueur n'a pas de contrat
                    if (this.communService.hasContrat(joueurDTO, associationDTO, saisonDTO)) {
                        log.debug("Le joueur a déja un contrat");
                        throw new ApiRequestException(ExceptionCode.JOUEUR_HAS_CONTRAT.getMessage(), ExceptionCode.JOUEUR_HAS_CONTRAT.getValue(), ExceptionLevel.WARNING);
                    }
                }

                if (joueurDTO != null) {
                    resultList.add(joueurDTO);

                    // save contrat
                    contratService.save(joueurDTO, associationDTO, saisonDTO, organisationDTO);
                }

            }
        }

        return resultList;
    }

    @Override
    public JoueurDTO save(JoueurDTO joueurDTO, OrganisationDTO organisationDTO, SaisonDTO saisonDTO, AssociationDTO associationDTO, CategoryDTO categoryDTO) {
        log.debug("Request to add joueur {} to organisation {} and saison {} and association {} and category {}",
            joueurDTO, organisationDTO, saisonDTO, associationDTO, categoryDTO);

        // check affiliation
        if (!this.communService.checkAffiliation(associationDTO, organisationDTO)) {
            throw new ApiRequestException(ExceptionCode.CLUB_NON_AFFILIE.getMessage(),  ExceptionCode.CLUB_NON_AFFILIE.getValue(), ExceptionLevel.ERROR);
        } else {
            if (joueurDTO.getId() != null) { // le joueur existe déja
                // vérifier si le joueur a déja un contrat
                if (this.communService.hasContrat(joueurDTO, associationDTO, saisonDTO)) {
                    log.debug("Le joueur a déja un contrat");
                    throw new ApiRequestException(ExceptionCode.JOUEUR_HAS_CONTRAT.getMessage(), ExceptionCode.JOUEUR_HAS_CONTRAT.getValue(), ExceptionLevel.WARNING);
                } else {
                    this.contratService.save(joueurDTO, associationDTO, saisonDTO, organisationDTO);
                }
            } else { // le joueur n'existe pas
                if (!this.communService.checkUniqueIdentifiant(joueurDTO)) {
                    log.debug("Le numéro de license est déja utilisé");
                    throw new ApiRequestException(ExceptionCode.IDENTIFIANT_EXISTS.getMessage(), ExceptionCode.IDENTIFIANT_EXISTS.getValue(), ExceptionLevel.WARNING);
                }

                joueurDTO.setDeleted(false);
                joueurDTO.setCreatedAt(ZonedDateTime.now());
                joueurDTO.setUpdatedAt(ZonedDateTime.now());

                joueurDTO.setCode("REF_" + organisationDTO.getId());

                String hashcode = organisationDTO.getId() + ZonedDateTime.now().toString();
                joueurDTO.setHashcode(this.communService.toHash(hashcode));

                // add joueur
                JoueurDTO result = save(joueurDTO);
                if (result != null) {
                    joueurDTO = result;

                    /*result.setCode(result.getCode() + "_" + result.getId());
                    result.setHashcode(this.communService.toHash(result.getId().toString()));
                    joueurDTO = save(result);*/

                    // save contrat
                    this.contratService.save(joueurDTO, associationDTO, saisonDTO, organisationDTO);

                    JoueurCategoryDTO joueurCategoryDTO = new JoueurCategoryDTO();
                    joueurCategoryDTO.setCreatedAt(ZonedDateTime.now());
                    joueurCategoryDTO.setUpdatedAt(ZonedDateTime.now());
                    joueurCategoryDTO.setJoueurId(result.getId());
                    joueurCategoryDTO.setCategoryId(categoryDTO.getId());
                    joueurCategoryDTO.setDeleted(false);

                    joueurCategoryService.save(joueurCategoryDTO);
                }
            }
        }

        return joueurDTO;
    }


    @Override
    public Page<JoueurDTO> search(OrganisationDTO organisationDTO, Long associationId, String query, Pageable pageable) {
        log.debug("Request to search for a page of Joueurs for query {}", query);

        BoolQueryBuilder queryBuilder = communService.organisationQueryBuilder(organisationDTO.getId(), "organisationId");

        if (associationId != null) {
            queryBuilder.must(queryStringQuery(associationId.toString()).analyzeWildcard(true).field("associationId"));
        }

        IdsQueryBuilder idsQueryBuilder  = new IdsQueryBuilder();
        contratSearchRepository.search(queryBuilder, Pageable.unpaged())
            .forEach(contrat -> {
                log.info("$$$$$$ CONTRAT : {}", contrat);

                idsQueryBuilder.addIds(
                    contrat.getJoueurId().toString());
            });

        log.debug("$$$$$$ JOUEUR IDS : {}", idsQueryBuilder.ids());

        queryBuilder = boolQuery()
            .must(queryStringQuery("false").analyzeWildcard(true).field("deleted"))
            .must(queryStringQuery(query))
            .must(idsQueryBuilder);

        return joueurSearchRepository.search(queryBuilder, pageable)
            .map(joueurMapper::toDto);
    }

    @Override
    public List<JoueurDTO> findAll() {
        return joueurRepository.findAllByDeletedIsFalse().stream()
            .map(joueurMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public void reIndex() {
        log.info("Request to reindex all Joueurs");
        joueurSearchRepository.deleteAll();
        findAll().stream().map(joueurMapper::toEntity).forEach(joueurSearchRepository::save);
    }

    private String generateJoueurIdentifiant(Long clubId) {
        var today = LocalDate.now();
        long timestamp = System.currentTimeMillis();
        long uniqueId = counter.incrementAndGet();

        return String.format("%s%S%s.%s.%s", today.getYear(), today.getMonthOfYear(), today.getMonthOfYear(), clubId, timestamp + "-" + uniqueId);
    }
}
