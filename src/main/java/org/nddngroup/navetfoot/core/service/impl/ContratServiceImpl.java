package org.nddngroup.navetfoot.core.service.impl;

import org.nddngroup.navetfoot.core.domain.Contrat;
import org.nddngroup.navetfoot.core.repository.ContratRepository;
import org.nddngroup.navetfoot.core.repository.search.ContratSearchRepository;
import org.nddngroup.navetfoot.core.service.ContratService;
import org.nddngroup.navetfoot.core.service.custom.CommunService;
import org.nddngroup.navetfoot.core.service.dto.*;
import org.nddngroup.navetfoot.core.service.dto.*;
import org.nddngroup.navetfoot.core.service.mapper.AssociationMapper;
import org.nddngroup.navetfoot.core.service.mapper.ContratMapper;
import org.nddngroup.navetfoot.core.service.mapper.JoueurMapper;
import org.nddngroup.navetfoot.core.service.mapper.SaisonMapper;
import org.nddngroup.navetfoot.core.service.mapper.custom.ContratStatsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link Contrat}.
 */
@Service
@Transactional
public class ContratServiceImpl implements ContratService {

    private final Logger log = LoggerFactory.getLogger(ContratServiceImpl.class);

    private final ContratRepository contratRepository;

    private final ContratMapper contratMapper;

    private final ContratSearchRepository contratSearchRepository;
    @Autowired
    private SaisonMapper saisonMapper;
    @Autowired
    private AssociationMapper associationMapper;
    @Autowired
    private JoueurMapper joueurMapper;
    @Autowired
    private ContratStatsMapper contratStatsMapper;
    @Autowired
    private CommunService communService;

    public ContratServiceImpl(ContratRepository contratRepository, ContratMapper contratMapper, ContratSearchRepository contratSearchRepository) {
        this.contratRepository = contratRepository;
        this.contratMapper = contratMapper;
        this.contratSearchRepository = contratSearchRepository;
    }

    @Override
    public ContratDTO save(ContratDTO contratDTO) {
        log.debug("Request to save Contrat : {}", contratDTO);
        Contrat contrat = contratMapper.toEntity(contratDTO);
        contrat = contratRepository.save(contrat);
        ContratDTO result = contratMapper.toDto(contrat);


        if (contratDTO.isDeleted()) {
            contratSearchRepository.deleteById(contratDTO.getId());
        } else {
            contratSearchRepository.save(contratStatsMapper.toStats(result));
        }

        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ContratDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Contrats");
        return contratRepository.findAll(pageable)
            .map(contratMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<ContratDTO> findOne(Long id) {
        log.debug("Request to get Contrat : {}", id);
        return contratRepository.findById(id)
            .map(contratMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Contrat : {}", id);
        contratRepository.deleteById(id);
        contratSearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ContratDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Contrats for query {}", query);
        return contratSearchRepository.search(queryStringQuery(query), pageable)
            .map(contratMapper::toDto);
    }

    @Override
    public Optional<ContratDTO> findBySaisonAndAssociationAndJoueur(SaisonDTO saisonDTO, AssociationDTO associationDTO, JoueurDTO joueurDTO) {
        log.debug("Request to get all Contrats by saison and association and joueur");
        return contratRepository.findAllBySaisonAndAssociationAndJoueurAndDeletedIsFalse(
            saisonMapper.toEntity(saisonDTO), associationMapper.toEntity(associationDTO), joueurMapper.toEntity(joueurDTO))
            .map(contratMapper::toDto);
    }

    @Override
    public List<ContratDTO> findBySaisonAndAssociation(SaisonDTO saisonDTO, AssociationDTO associationDTO) {
        log.debug("Request to get all Contrats by saison and association");
        return contratRepository.findAllBySaisonAndAssociationAndDeletedIsFalse(
            saisonMapper.toEntity(saisonDTO), associationMapper.toEntity(associationDTO))
            .stream()
            .map(contratMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public List<ContratDTO> findByAssociation(AssociationDTO associationDTO) {
        log.debug("Request to get all Contrats by association");
        return contratRepository.findAllByAssociationAndDeletedIsFalse(associationMapper.toEntity(associationDTO))
            .stream()
            .map(contratMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public List<ContratDTO> findByAssociationAndJoueur(AssociationDTO associationDTO, JoueurDTO joueurDTO) {
        log.debug("Request to get all Contrats by association : {} and joueur : {}", associationDTO, joueurDTO);
        return contratRepository.findAllByAssociationAndJoueurAndDeletedIsFalse(associationMapper.toEntity(associationDTO), joueurMapper.toEntity(joueurDTO))
            .stream()
            .map(contratMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public List<ContratDTO> findBySaison(SaisonDTO saisonDTO) {
        log.debug("Request to get all Contrats by saison : {}", saisonDTO);
        return contratRepository.findAllBySaisonAndDeletedIsFalse(saisonMapper.toEntity(saisonDTO))
            .stream()
            .map(contratMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public Optional<ContratDTO> findBySaisonAndJoueur(SaisonDTO saisonDTO, JoueurDTO joueurDTO) {
        log.debug("Request to get all Contrats by saison : {} and joueur : {}", saisonDTO, joueurDTO);
        return contratRepository.findAllBySaisonAndJoueurAndDeletedIsFalse(saisonMapper.toEntity(saisonDTO), joueurMapper.toEntity(joueurDTO))
            .map(contratMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ContratDTO> findByHashcode(String hashcode) {
        log.debug("Request to get Contrat : {}", hashcode);
        return contratRepository.findByHashcodeAndDeletedIsFalse(hashcode)
            .map(contratMapper::toDto);
    }

    @Override
    public List<ContratDTO> findByJoueur(JoueurDTO joueurDTO) {
        log.debug("Request to get all Contrats by joueur : {}", joueurDTO);
        return contratRepository.findAllByJoueurAndDeletedIsFalse(joueurMapper.toEntity(joueurDTO))
            .stream().map(contratMapper::toDto).collect(Collectors.toList());
    }

    /**
     * Add new contrat
     *
     * @param joueurDTO //
     * @param associationDTO //
     * @param saisonDTO //
     * @param organisationDTO //
     * @return //
     */
    @Override
    public ContratDTO save(JoueurDTO joueurDTO, AssociationDTO associationDTO, SaisonDTO saisonDTO, OrganisationDTO organisationDTO) {
        ContratDTO contratDTO = new ContratDTO();

        contratDTO.setJoueurId(joueurDTO.getId());
        contratDTO.setAssociationId(associationDTO.getId());
        contratDTO.setSaisonId(saisonDTO.getId());
        contratDTO.setCode("Ref_" + organisationDTO.getId());

        String hashcode = organisationDTO.getId().toString() + joueurDTO.getId() + ZonedDateTime.now();
        contratDTO.setHashcode(this.communService.toHash(hashcode));

        contratDTO.setCreatedAt(ZonedDateTime.now());
        contratDTO.setUpdatedAt(ZonedDateTime.now());
        contratDTO.setDeleted(false);

        /*if (result != null) {
            result.setCode(result.getCode() + "_" + result.getId());
            result.setHashcode(this.toHash(result.getId().toString()));
        }*/

        return this.save(contratDTO);
    }

    @Override
    public List<ContratDTO> findAll() {
        return contratRepository.findAllByDeletedIsFalse().stream()
            .map(contratMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public void reIndex() {
        log.info("Request to reindex all Contrats");
        contratSearchRepository.deleteAll();
        findAll().stream().map(contratStatsMapper::toStats).forEach(contratSearchRepository::save);
    }
}
