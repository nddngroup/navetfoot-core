package org.nddngroup.navetfoot.core.service.impl;

import org.nddngroup.navetfoot.core.domain.DelegueMatch;
import org.nddngroup.navetfoot.core.repository.DelegueMatchRepository;
import org.nddngroup.navetfoot.core.repository.search.DelegueMatchSearchRepository;
import org.nddngroup.navetfoot.core.service.DelegueMatchService;
import org.nddngroup.navetfoot.core.service.dto.DelegueMatchDTO;
import org.nddngroup.navetfoot.core.service.dto.MatchDTO;
import org.nddngroup.navetfoot.core.service.mapper.DelegueMatchMapper;
import org.nddngroup.navetfoot.core.service.mapper.MatchMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link DelegueMatch}.
 */
@Service
@Transactional
public class DelegueMatchServiceImpl implements DelegueMatchService {

    private final Logger log = LoggerFactory.getLogger(DelegueMatchServiceImpl.class);

    private final DelegueMatchRepository delegueMatchRepository;

    private final DelegueMatchMapper delegueMatchMapper;

    private final DelegueMatchSearchRepository delegueMatchSearchRepository;
    @Autowired
    private MatchMapper matchMapper;

    public DelegueMatchServiceImpl(DelegueMatchRepository delegueMatchRepository, DelegueMatchMapper delegueMatchMapper, DelegueMatchSearchRepository delegueMatchSearchRepository) {
        this.delegueMatchRepository = delegueMatchRepository;
        this.delegueMatchMapper = delegueMatchMapper;
        this.delegueMatchSearchRepository = delegueMatchSearchRepository;
    }

    @Override
    public DelegueMatchDTO save(DelegueMatchDTO delegueMatchDTO) {
        log.debug("Request to save DelegueMatch : {}", delegueMatchDTO);
        DelegueMatch delegueMatch = delegueMatchMapper.toEntity(delegueMatchDTO);
        delegueMatch = delegueMatchRepository.save(delegueMatch);
        DelegueMatchDTO result = delegueMatchMapper.toDto(delegueMatch);

        if (delegueMatchDTO.isDeleted()) {
            delegueMatchSearchRepository.deleteById(delegueMatchDTO.getId());
        } else {
            delegueMatchSearchRepository.save(delegueMatchMapper.toEntity(result));
        }

        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public List<DelegueMatchDTO> findAll() {
        log.debug("Request to get all DelegueMatches");
        return delegueMatchRepository.findAll().stream()
            .map(delegueMatchMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public List<DelegueMatchDTO> findAllByDeletedIsFalse() {
        log.debug("Request to get all DelegueMatches");
        return delegueMatchRepository.findAllByDeletedIsFalse().stream()
            .map(delegueMatchMapper::toDto)
            .collect(Collectors.toList());
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<DelegueMatchDTO> findOne(Long id) {
        log.debug("Request to get DelegueMatch : {}", id);
        return delegueMatchRepository.findById(id)
            .map(delegueMatchMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete DelegueMatch : {}", id);
        delegueMatchRepository.deleteById(id);
        delegueMatchSearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<DelegueMatchDTO> search(String query) {
        log.debug("Request to search DelegueMatches for query {}", query);
        return StreamSupport
            .stream(delegueMatchSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(delegueMatchMapper::toDto)
        .collect(Collectors.toList());
    }

    @Override
    public Optional<DelegueMatchDTO> findByMatch(MatchDTO matchDTO) {
        log.debug("Request to get DelegueMatch by match : {}", matchDTO.getId());
        return delegueMatchRepository.findOneByMatchAndDeletedIsFalse(matchMapper.toEntity(matchDTO))
            .map(delegueMatchMapper::toDto);
    }

    @Override
    public Optional<DelegueMatchDTO> findByConfirmCode(String codeMatch) {
        log.debug("Request to get DelegueMatch by codeMatch : {}", codeMatch);
        return delegueMatchRepository.findOneByConfirmCodeAndDeletedIsFalse(codeMatch)
            .map(delegueMatchMapper::toDto);
    }

    @Override
    public void reIndex() {
        log.info("Request to reindex all DelegueMatch");
        delegueMatchSearchRepository.deleteAll();
        findAllByDeletedIsFalse().stream().map(delegueMatchMapper::toEntity).forEach(delegueMatchSearchRepository::save);
    }
}
