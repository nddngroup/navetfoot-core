package org.nddngroup.navetfoot.core.service.mapper;


import org.nddngroup.navetfoot.core.domain.*;
import org.nddngroup.navetfoot.core.domain.Penalty;
import org.nddngroup.navetfoot.core.service.dto.PenaltyDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Penalty} and its DTO {@link PenaltyDTO}.
 */
@Mapper(componentModel = "spring", uses = {JoueurMapper.class, MatchMapper.class, TirAuButMapper.class})
public interface PenaltyMapper extends EntityMapper<PenaltyDTO, Penalty> {

    @Mapping(source = "joueur.id", target = "joueurId")
    @Mapping(source = "match.id", target = "matchId")
    @Mapping(source = "tirAuBut.id", target = "tirAuButId")
    PenaltyDTO toDto(Penalty penalty);

    @Mapping(source = "joueurId", target = "joueur")
    @Mapping(source = "matchId", target = "match")
    @Mapping(source = "tirAuButId", target = "tirAuBut")
    Penalty toEntity(PenaltyDTO penaltyDTO);

    default Penalty fromId(Long id) {
        if (id == null) {
            return null;
        }
        Penalty penalty = new Penalty();
        penalty.setId(id);
        return penalty;
    }
}
