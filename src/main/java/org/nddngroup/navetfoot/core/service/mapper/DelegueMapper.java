package org.nddngroup.navetfoot.core.service.mapper;


import org.nddngroup.navetfoot.core.domain.*;
import org.nddngroup.navetfoot.core.domain.Delegue;
import org.nddngroup.navetfoot.core.service.dto.DelegueDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Delegue} and its DTO {@link DelegueDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface DelegueMapper extends EntityMapper<DelegueDTO, Delegue> {


    @Mapping(target = "delegueMatches", ignore = true)
    @Mapping(target = "removeDelegueMatch", ignore = true)
    Delegue toEntity(DelegueDTO delegueDTO);

    default Delegue fromId(Long id) {
        if (id == null) {
            return null;
        }
        Delegue delegue = new Delegue();
        delegue.setId(id);
        return delegue;
    }
}
