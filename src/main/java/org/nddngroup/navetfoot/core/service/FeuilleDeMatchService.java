package org.nddngroup.navetfoot.core.service;

import org.nddngroup.navetfoot.core.domain.FeuilleDeMatch;
import org.nddngroup.navetfoot.core.service.dto.FeuilleDeMatchDTO;
import org.nddngroup.navetfoot.core.service.dto.MatchDTO;
import org.nddngroup.navetfoot.core.service.dto.OrganisationDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link FeuilleDeMatch}.
 */
public interface FeuilleDeMatchService {

    /**
     * Save a feuilleDeMatch.
     *
     * @param feuilleDeMatchDTO the entity to save.
     * @return the persisted entity.
     */
    FeuilleDeMatchDTO save(FeuilleDeMatchDTO feuilleDeMatchDTO);

    /**
     * Get all the feuilleDeMatches.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<FeuilleDeMatchDTO> findAll(Pageable pageable);


    /**
     * Get the "id" feuilleDeMatch.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<FeuilleDeMatchDTO> findOne(Long id);

    /**
     * Delete the "id" feuilleDeMatch.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the feuilleDeMatch corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<FeuilleDeMatchDTO> search(String query, Pageable pageable);
    Optional<FeuilleDeMatchDTO> findByMatch(MatchDTO matchDTO);
    Optional<FeuilleDeMatchDTO> findByHashcode(String hashcode);
    FeuilleDeMatchDTO save(Long matchId, OrganisationDTO organisationDTO);
    List<FeuilleDeMatchDTO> findAll();
    void reIndex();
}
