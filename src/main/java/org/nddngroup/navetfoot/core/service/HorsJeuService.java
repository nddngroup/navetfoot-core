package org.nddngroup.navetfoot.core.service;

import org.nddngroup.navetfoot.core.domain.HorsJeu;
import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;
import org.nddngroup.navetfoot.core.service.dto.HorsJeuDTO;
import org.nddngroup.navetfoot.core.service.dto.MatchDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link HorsJeu}.
 */
public interface HorsJeuService {

    /**
     * Save a horsJeu.
     *
     * @param horsJeuDTO the entity to save.
     * @return the persisted entity.
     */
    HorsJeuDTO save(HorsJeuDTO horsJeuDTO);

    /**
     * Get all the horsJeus.
     *
     * @return the list of entities.
     */
    List<HorsJeuDTO> findAll();
    List<HorsJeuDTO> findAllByDeletedIsFalse();


    /**
     * Get the "id" horsJeu.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<HorsJeuDTO> findOne(Long id);

    /**
     * Delete the "id" horsJeu.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the horsJeu corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @return the list of entities.
     */
    List<HorsJeuDTO> search(String query);
    List<HorsJeuDTO> findByMatch(MatchDTO matchDTO);
    List<HorsJeuDTO> findByMatchAndEquipe(MatchDTO matchDTO, Equipe equipe);
    Optional<HorsJeuDTO> findByHashcode(String hashcode);
    void reIndex();
}
