package org.nddngroup.navetfoot.core.service.mapper.custom;

import org.nddngroup.navetfoot.core.domain.Tir;
import org.nddngroup.navetfoot.core.service.*;
import org.nddngroup.navetfoot.core.service.*;
import org.nddngroup.navetfoot.core.service.dto.TirDTO;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring")
public abstract class TirStatsMapper {
    @Autowired
    protected EtapeCompetitionService etapeCompetitionService;
    @Autowired
    protected CompetitionSaisonService competitionSaisonService;
    @Autowired
    protected SaisonService saisonService;
    @Autowired
    protected CompetitionService competitionService;
    @Autowired
    protected OrganisationService organisationService;
    @Autowired
    protected AssociationService associationService;
    @Autowired
    protected MatchService matchService;
    @Autowired
    protected JoueurService joueurService;
    @Autowired
    protected MatchStatsMapper matchStatsMapper;

    public Tir toStats(TirDTO tir) {
        var tirStats = new Tir()
            .code(tir.getCode())
            .hashcode(tir.getHashcode())
            .equipe(tir.getEquipe())
            .instant(tir.getInstant())
            .cadre(tir.isCadre())
            .deleted(tir.isDeleted())
            .createdAt(tir.getCreatedAt())
            .updatedAt(tir.getUpdatedAt());

        tirStats.setId(tir.getId());

        // find joueur
        joueurService.findOne(tir.getJoueurId())
            .ifPresent(joueurDTO -> {
                tirStats.setJoueurId(joueurDTO.getId());
                tirStats.setJoueurNom(joueurDTO.getNom());
                tirStats.setJoueurPrenom(joueurDTO.getPrenom());
                tirStats.setJoueurImage(joueurDTO.getImageUrl());
            });

        // find match
        matchService.findOne(tir.getMatchId())
            .ifPresent(matchDTO -> {
                tirStats.setMatchId(matchDTO.getId());

                var matchStats = matchStatsMapper.toStats(matchDTO);
                tirStats.setLocalId(matchStats.getLocalId());
                tirStats.setLocalNom(matchStats.getLocalNom());

                tirStats.setVisiteurId(matchStats.getVisiteurId());
                tirStats.setVisiteurNom(matchStats.getVisiteurNom());

                tirStats.setEtapeCompetitionId(matchStats.getEtapeCompetitionId());
                tirStats.setEtapeCompetitionNom(matchStats.getEtapeCompetitionNom());

                tirStats.setSaisonId(matchStats.getSaisonId());
                tirStats.setSaisonLibelle(matchStats.getSaisonLibelle());

                tirStats.setCompetitionId(matchStats.getCompetitionId());
                tirStats.setCompetitionNom(matchStats.getCompetitionNom());

                tirStats.setOrganisationId(matchStats.getOrganisationId());
                tirStats.setOrganisationNom(matchStats.getOrganisationNom());
            });

        return tirStats;
    }
}
