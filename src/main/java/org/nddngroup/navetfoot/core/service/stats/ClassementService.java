package org.nddngroup.navetfoot.core.service.stats;

import org.nddngroup.navetfoot.core.exception.ApiRequestException;
import org.nddngroup.navetfoot.core.exception.ExceptionCode;
import org.nddngroup.navetfoot.core.exception.ExceptionLevel;
import org.nddngroup.navetfoot.core.service.*;
import org.nddngroup.navetfoot.core.service.dto.*;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.LongFilter;
import org.nddngroup.navetfoot.core.domain.custom.stats.Stat;
import org.nddngroup.navetfoot.core.domain.enumeration.Cause;
import org.nddngroup.navetfoot.core.domain.enumeration.Couleur;
import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;
import org.nddngroup.navetfoot.core.service.*;
import org.nddngroup.navetfoot.core.service.dto.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by souleymane91 on 02/08/2019.
 */
@Service
public class ClassementService {
    private final Logger log = LoggerFactory.getLogger(ClassementService.class);

    @Autowired
    private AssociationService associationService;
    @Autowired
    private JoueurService joueurService;
    @Autowired
    private MatchService matchService;
    @Autowired
    private EtapeCompetitionService etapeCompetitionService;
    @Autowired
    private ButService butService;
    @Autowired
    private CartonService cartonService;
    @Autowired
    private PenaltyService penaltyService;
    @Autowired
    private CoupFrancService coupFrancService;
    @Autowired
    private CornerService cornerService;
    @Autowired
    private RemplacementService remplacementService;
    @Autowired
    private HorsJeuService horsJeuService;
    @Autowired
    private TirService tirService;

    /**
     * Le total des events d'une association pour une étape donnée
     *
     * @param etapeId L'id de l'étape
     * @param associationId L'id du club
     * @return
     */
    public Stat getAssociationTotalEvents (Long etapeId, Long associationId, String event) {
        Stat stat = new Stat();

        this.log.debug("__________________________________________________________________");
        this.log.debug("_______________ Get total 'events' for association _________________");
        this.log.debug("__________________________________________________________________");


        if (associationId == null) {
            this.log.warn("Association id is null");
            return null;
        }

        // get associationDTO
        AssociationDTO associationDTO = this.associationService.findOne(associationId)
            .orElseThrow(() -> new ApiRequestException(
                ExceptionCode.ASSOCIATIONS_NOT_FOUND.getMessage(),
                ExceptionCode.ASSOCIATIONS_NOT_FOUND.getValue(),
                ExceptionLevel.ERROR
            ));

        BooleanFilter deletedFilter = new BooleanFilter();
        deletedFilter.setEquals(false);

        LongFilter etapeFilter = new LongFilter();
        etapeFilter.setEquals(etapeId);

        List<MatchDTO> matchDTOS_local = new ArrayList<>();  // list matchs where club is 'local'
        List<MatchDTO> matchDTOS_visiteur = new ArrayList<>();  // list matchs where club is 'visiteur'

        if (etapeId != null) {

            // find etapeCompetition by id
            EtapeCompetitionDTO etapeCompetitionDTO = etapeCompetitionService.findOne(etapeId)
                .orElseThrow(() -> new ApiRequestException(
                    ExceptionCode.ETAPE_NOT_FOUND.getMessage(),
                    ExceptionCode.ETAPE_NOT_FOUND.getValue(),
                    ExceptionLevel.ERROR
                ));

            matchDTOS_local = this.matchService.findByEtapeCompetitionAndLocal(etapeCompetitionDTO, associationDTO);

            matchDTOS_visiteur = this.matchService.findByEtapeCompetitionAndVisiteur(etapeCompetitionDTO, associationDTO);

        }
        else {
            this.log.warn("Etape id is null");
        }

        int nbEvents = this.getNbEventsByEquipe(matchDTOS_local, Equipe.LOCAL, event) + this.getNbEventsByEquipe(matchDTOS_visiteur, Equipe.VISITEUR, event);

        stat.setId(associationId);
        stat.setCritere("club");
        stat.setType(event);
        if (associationDTO != null) {
            stat.setLibelle(associationDTO.getNom());
        }
        stat.setTotal(nbEvents);

        return stat;
    }

    /**
     * Le total des events d'un joueur pour une étape donnée
     *
     * @param etapeId
     * @param joueurId
     * @return
     */
    public Stat getJoueurTotalEvents (Long etapeId, Long joueurId, String event) {
        Stat stat = new Stat();

        this.log.debug("__________________________________________________________________");
        this.log.debug("_______________ Get total 'events' for joueur _________________");
        this.log.debug("__________________________________________________________________");

        if (joueurId == null) {
            this.log.warn("Joueur id is null");
            return null;
        }

        // get joueurDTO
        JoueurDTO joueurDTO = this.joueurService.findOne(joueurId)
            .orElseThrow(() -> new ApiRequestException(
                ExceptionCode.JOUEUR_NOT_FOUND.getMessage(),
                ExceptionCode.JOUEUR_NOT_FOUND.getValue(),
                ExceptionLevel.ERROR
            ));

        BooleanFilter deletedFilter = new BooleanFilter();
        deletedFilter.setEquals(false);

        LongFilter etapeFilter = new LongFilter();
        etapeFilter.setEquals(etapeId);

        List<MatchDTO> matchDTOS = new ArrayList<>();  // list matchs

        if (etapeId != null) {
            // find etapeCompetition by id
            EtapeCompetitionDTO etapeCompetitionDTO = etapeCompetitionService.findOne(etapeId)
                .orElseThrow(() -> new ApiRequestException(
                    ExceptionCode.ETAPE_NOT_FOUND.getMessage(),
                    ExceptionCode.ETAPE_NOT_FOUND.getValue(),
                    ExceptionLevel.ERROR
                ));

            // get list matchs
            matchDTOS = this.matchService.findByEtapeCompetition(etapeCompetitionDTO);

        }
        else {
            this.log.warn("Etape id is null");
        }

        int nbEvents = this.getNbEventsByJoueur(matchDTOS, joueurId, event);

        stat.setId(joueurId);
        stat.setCritere("joueur");
        stat.setType(event);
        stat.setLibelle(joueurDTO.getPrenom() + " " + joueurDTO.getNom());
        stat.setTotal(nbEvents);

        return stat;
    }

    /**
     *
     * @param matchDTOS
     * @param equipe       (LOCAL, VISITEUR)
     * @param event
     * @return
     */
    public int getNbEventsByEquipe(List<MatchDTO> matchDTOS, Equipe equipe, String event) {
        int nbEvents = 0;

        if (matchDTOS != null && matchDTOS.size() != 0) {
            for(MatchDTO matchDTO : matchDTOS) {

                LongFilter longFilter = new LongFilter();
                BooleanFilter deletedFilter = new BooleanFilter();

                longFilter.setEquals(matchDTO.getId());
                deletedFilter.setEquals(false);

                switch (event) {
                    case "butContre":

                        List<ButDTO> butDTOS = this.butService.findByMatchAndEquipe(matchDTO, equipe);

                        if (butDTOS != null) {
                            nbEvents += butDTOS.size();
                        }
                        break;
                    case "butPour":
                        List<ButDTO> butsPour = this.butService.findByMatchAndEquipe(matchDTO, equipe);

                        nbEvents += butsPour.size();

                        break;
                    case "carton":
                        List<CartonDTO> cartonDTOS = this.cartonService.findByMatchAndEquipe(matchDTO, equipe);

                        nbEvents += cartonDTOS.size();

                        break;
                    case "cartonJaune":

                        List<CartonDTO> cartonJDTOS = this.cartonService.findByMatchAndEquipeAndCouleur(matchDTO, equipe, Couleur.JAUNE);

                        nbEvents += cartonJDTOS.size();

                        break;
                    case "cartonRouge":

                        List<CartonDTO> cartonRDTOS = this.cartonService.findByMatchAndEquipeAndCouleur(matchDTO, equipe, Couleur.ROUGE);

                        nbEvents += cartonRDTOS.size();

                        break;
                    case "tir":

                        List<TirDTO> tirDTOS = this.tirService.findByMatchAndEquipe(matchDTO, equipe);

                        nbEvents += tirDTOS.size();

                        break;
                    case "tirCadre":

                        List<TirDTO> tirCDTOS = this.tirService.findByMatchAndEquipeAndCadre(matchDTO, equipe, true);

                        nbEvents += tirCDTOS.size();

                        break;
                    case "tirNonCadre":

                        List<TirDTO> tirNCDTOS = this.tirService.findByMatchAndEquipeAndCadre(matchDTO, equipe, false);

                        nbEvents += tirNCDTOS.size();


                        break;
                    case "penalty":

                        List<PenaltyDTO> penaltyDTOS = this.penaltyService.findByMatchAndEquipe(matchDTO, equipe);

                        nbEvents += penaltyDTOS.size();
                        break;
                    case "remplacement":

                        List<RemplacementDTO> remplacementDTOS = this.remplacementService.findByMatchAndEquipe(matchDTO, equipe);

                        nbEvents += remplacementDTOS.size();
                        break;
                    case "coupFranc":

                        List<CoupFrancDTO> coupFrancDTOS = this.coupFrancService.findByMatchAndEquipe(matchDTO, equipe);

                        nbEvents += coupFrancDTOS.size();
                        break;
                    case "corner":

                        List<CornerDTO> cornerDTOS = this.cornerService.findByMatchAndEquipe(matchDTO, equipe);

                        nbEvents += cornerDTOS.size();
                        break;
                    case "horsJeu":

                        List<HorsJeuDTO> horsJeuDTOS = this.horsJeuService.findByMatchAndEquipe(matchDTO, equipe);

                        nbEvents += horsJeuDTOS.size();
                        break;
                    default: break;
                }
            }
        }

        return nbEvents;
    }

    /**
     *
     * @param matchDTOS
     * @param joueurId
     * @param event
     * @return
     */
    public int getNbEventsByJoueur(List<MatchDTO> matchDTOS, Long joueurId, String event) {
        int nbEvents = 0;

        // find joueur
        JoueurDTO joueurDTO = joueurService.findOne(joueurId)
            .orElseThrow(() -> new ApiRequestException(
                ExceptionCode.JOUEUR_NOT_FOUND.getMessage(),
                ExceptionCode.JOUEUR_NOT_FOUND.getValue(),
                ExceptionLevel.ERROR
            ));

        if (matchDTOS != null && matchDTOS.size() != 0) {
            for(MatchDTO matchDTO : matchDTOS) {


                LongFilter longFilter = new LongFilter();
                BooleanFilter deletedFilter = new BooleanFilter();

                longFilter.setEquals(matchDTO.getId());
                deletedFilter.setEquals(false);

                switch (event) {
                    case "but":

                        List<ButDTO> butDTOS = this.butService.findByMatchAndJoueur(matchDTO, joueurDTO);

                        if (butDTOS != null) {
                            for(ButDTO butDTO : butDTOS) {
                                if (butDTO.getCause() != Cause.FORFAIT) {
                                    nbEvents += 1;
                                }
                            }
                        }

                        break;
                    case "cartonJaune":

                        List<CartonDTO> cartonDTOS = this.cartonService.findByMatchAndJoueurAndCouleur(matchDTO, joueurDTO, Couleur.JAUNE);

                        if (cartonDTOS != null) {
                            nbEvents += cartonDTOS.size();
                        }

                        break;
                    case "cartonRouge":

                        List<CartonDTO> cartonRDTOS = this.cartonService.findByMatchAndJoueurAndCouleur(matchDTO, joueurDTO, Couleur.ROUGE);

                        if (cartonRDTOS != null) {
                            nbEvents += cartonRDTOS.size();
                        }

                        break;
                    default: break;
                }
            }
        }

        return nbEvents;
    }

    public List<Stat> getJoueurStats(List<MatchDTO> matchDTOS, String event) {
        if (matchDTOS == null || matchDTOS.size() == 0) {
            return null;
        }

        List<Stat> stats = new ArrayList<>();

        for (MatchDTO matchDTO : matchDTOS) {
            if (!matchDTO.isDeleted()) {
                // get all events
                BooleanFilter deletedFilter = new BooleanFilter();
                deletedFilter.setEquals(false);

                LongFilter matchIdFilter = new LongFilter();
                matchIdFilter.setEquals(matchDTO.getId());

                switch (event) {
                    case "but":
                        List<ButDTO> butDTOS = this.butService.findByMatch(matchDTO);

                        if (butDTOS != null && butDTOS.size() != 0) {
                            for (ButDTO butDTO : butDTOS) {
                                if (butDTO.getCause() != Cause.FORFAIT && butDTO.getCause() != Cause.RESERVE) {
                                    // get joueur by id
                                    JoueurDTO joueurDTO = this.joueurService.findOne(butDTO.getJoueurId())
                                        .orElseThrow(() -> new ApiRequestException(
                                            ExceptionCode.JOUEUR_NOT_FOUND.getMessage(),
                                            ExceptionCode.JOUEUR_NOT_FOUND.getValue(),
                                            ExceptionLevel.ERROR
                                        ));

                                    if (joueurDTO != null) {
                                        Stat stat = new Stat();
                                        stat.setType(event);
                                        stat.setCritere("joueur");
                                        stat.setId(joueurDTO.getId());
                                        stat.setReference(joueurDTO.getCode());
                                        stat.setLibelle(joueurDTO.getPrenom() + " " + joueurDTO.getNom());
                                        stat.setTotal(1);

                                        stats = this.addStat(stat, stats, event);

                                    }
                                }

                            }
                        }
                        break;
                    case "cartonJaune":
                        List<CartonDTO> cartonDTOS = this.cartonService.findByMatchAndCouleur(matchDTO, Couleur.JAUNE);

                        if (cartonDTOS != null && cartonDTOS.size() != 0) {
                            for (CartonDTO cartonDTO : cartonDTOS) {
                                // get joueur by id
                                JoueurDTO joueurDTO = this.joueurService.findOne(cartonDTO.getJoueurId())
                                    .orElseThrow(() -> new ApiRequestException(
                                        ExceptionCode.JOUEUR_NOT_FOUND.getMessage(),
                                        ExceptionCode.JOUEUR_NOT_FOUND.getValue(),
                                        ExceptionLevel.ERROR
                                    ));

                                if (joueurDTO != null) {
                                    Stat stat = new Stat();
                                    stat.setType(event);
                                    stat.setCritere("joueur");
                                    stat.setId(joueurDTO.getId());
                                    stat.setReference(joueurDTO.getCode());
                                    stat.setLibelle(joueurDTO.getPrenom() + " " + joueurDTO.getNom());
                                    stat.setTotal(1);

                                    stats = this.addStat(stat, stats, event);
                                }

                            }
                        }
                        break;
                    case "cartonRouge":

                        List<CartonDTO> cartonRDTOS = this.cartonService.findByMatchAndCouleur(matchDTO, Couleur.ROUGE);

                        if (cartonRDTOS != null && cartonRDTOS.size() != 0) {
                            for (CartonDTO cartonDTO : cartonRDTOS) {
                                // get joueur by id
                                JoueurDTO joueurDTO = this.joueurService.findOne(cartonDTO.getJoueurId())
                                    .orElseThrow(() -> new ApiRequestException(
                                        ExceptionCode.JOUEUR_NOT_FOUND.getMessage(),
                                        ExceptionCode.JOUEUR_NOT_FOUND.getValue(),
                                        ExceptionLevel.ERROR
                                    ));

                                if (joueurDTO != null) {
                                    Stat stat = new Stat();
                                    stat.setType(event);
                                    stat.setCritere("joueur");
                                    stat.setId(joueurDTO.getId());
                                    stat.setReference(joueurDTO.getCode());
                                    stat.setLibelle(joueurDTO.getPrenom() + " " + joueurDTO.getNom());
                                    stat.setTotal(1);

                                    stats = this.addStat(stat, stats, event);
                                }

                            }
                        }
                        break;
                    default: break;
                }
            }
        }


        return stats;
    }

    public List<Stat> addStat(Stat stat, List<Stat> stats, String type) {
        boolean existe = false;

        for (Stat item: stats) {
            if (item.getId().equals(stat.getId())) {
                existe = true;
                item.setTotal(item.getTotal() + stat.getTotal());

                break;
            }
        }

        if (!existe) {
            stats.add(stat);
        }

        // sort list
        if (type.equals("butPour")) {
            Object[] array = stats.toArray();

            Arrays.sort(array);

        }
        else if (type.equals("butContre")) {
            Arrays.sort(stats.toArray());
        }

        return stats;
    }
}
