package org.nddngroup.navetfoot.core.service.dto;

import org.nddngroup.navetfoot.core.domain.DelegueMatch;

import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link DelegueMatch} entity.
 */
public class DelegueMatchDTO implements Serializable {

    private Long id;

    @NotNull
    private String hashcode;

    @NotNull
    private String code;

//    private String matchCode;

    private Boolean deleted;

    private ZonedDateTime createdAt;

    private ZonedDateTime updatedAt;

    private String status;

    private String confirmCode;


    private Long matchId;

    private String matchCode;

    private Long delegueId;

    private String delegueCode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHashcode() {
        return hashcode;
    }

    public void setHashcode(String hashcode) {
        this.hashcode = hashcode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

//    public String getMatchCode() {
//        return matchCode;
//    }
//
//    public void setMatchCode(String matchCode) {
//        this.matchCode = matchCode;
//    }

    public Boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getConfirmCode() {
        return confirmCode;
    }

    public void setConfirmCode(String confirmCode) {
        this.confirmCode = confirmCode;
    }

    public Long getMatchId() {
        return matchId;
    }

    public void setMatchId(Long matchId) {
        this.matchId = matchId;
    }

    public String getMatchCode() {
        return matchCode;
    }

    public void setMatchCode(String matchCode) {
        this.matchCode = matchCode;
    }

    public Long getDelegueId() {
        return delegueId;
    }

    public void setDelegueId(Long delegueId) {
        this.delegueId = delegueId;
    }

    public String getDelegueCode() {
        return delegueCode;
    }

    public void setDelegueCode(String delegueCode) {
        this.delegueCode = delegueCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DelegueMatchDTO)) {
            return false;
        }

        return id != null && id.equals(((DelegueMatchDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DelegueMatchDTO{" +
            "id=" + getId() +
            ", hashcode='" + getHashcode() + "'" +
            ", code='" + getCode() + "'" +
            ", matchCode='" + getMatchCode() + "'" +
            ", deleted='" + isDeleted() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", status='" + getStatus() + "'" +
            ", confirmCode='" + getConfirmCode() + "'" +
            ", matchId=" + getMatchId() +
            ", matchCode='" + getMatchCode() + "'" +
            ", delegueId=" + getDelegueId() +
            ", delegueCode='" + getDelegueCode() + "'" +
            "}";
    }
}
