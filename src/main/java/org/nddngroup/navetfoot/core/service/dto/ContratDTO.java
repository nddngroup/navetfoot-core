package org.nddngroup.navetfoot.core.service.dto;

import org.nddngroup.navetfoot.core.domain.Contrat;

import java.time.ZonedDateTime;
import java.io.Serializable;

/**
 * A DTO for the {@link Contrat} entity.
 */
public class ContratDTO implements Serializable {

    private Long id;

    private String code;

    private String hashcode;

    private ZonedDateTime createdAt;

    private ZonedDateTime updatedAt;

    private Boolean deleted;


    private Long saisonId;

    private String saisonLibelle;

    private Long associationId;

    private String associationNom;

    private Long joueurId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getHashcode() {
        return hashcode;
    }

    public void setHashcode(String hashcode) {
        this.hashcode = hashcode;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Long getSaisonId() {
        return saisonId;
    }

    public void setSaisonId(Long saisonId) {
        this.saisonId = saisonId;
    }

    public String getSaisonLibelle() {
        return saisonLibelle;
    }

    public void setSaisonLibelle(String saisonLibelle) {
        this.saisonLibelle = saisonLibelle;
    }

    public Long getAssociationId() {
        return associationId;
    }

    public void setAssociationId(Long associationId) {
        this.associationId = associationId;
    }

    public String getAssociationNom() {
        return associationNom;
    }

    public void setAssociationNom(String associationNom) {
        this.associationNom = associationNom;
    }

    public Long getJoueurId() {
        return joueurId;
    }

    public void setJoueurId(Long joueurId) {
        this.joueurId = joueurId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ContratDTO)) {
            return false;
        }

        return id != null && id.equals(((ContratDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ContratDTO{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", hashcode='" + getHashcode() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", deleted='" + isDeleted() + "'" +
            ", saisonId=" + getSaisonId() +
            ", saisonLibelle='" + getSaisonLibelle() + "'" +
            ", associationId=" + getAssociationId() +
            ", associationNom='" + getAssociationNom() + "'" +
            ", joueurId=" + getJoueurId() +
            "}";
    }
}
