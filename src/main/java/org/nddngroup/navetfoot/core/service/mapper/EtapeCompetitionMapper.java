package org.nddngroup.navetfoot.core.service.mapper;


import org.nddngroup.navetfoot.core.domain.*;
import org.nddngroup.navetfoot.core.domain.EtapeCompetition;
import org.nddngroup.navetfoot.core.service.dto.EtapeCompetitionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link EtapeCompetition} and its DTO {@link EtapeCompetitionDTO}.
 */
@Mapper(componentModel = "spring", uses = {CompetitionSaisonCategoryMapper.class, CompetitionSaisonMapper.class})
public interface EtapeCompetitionMapper extends EntityMapper<EtapeCompetitionDTO, EtapeCompetition> {

    @Mapping(source = "competitionSaisonCategory.id", target = "competitionSaisonCategoryId")
    @Mapping(source = "competitionSaison.id", target = "competitionSaisonId")
    EtapeCompetitionDTO toDto(EtapeCompetition etapeCompetition);

    @Mapping(target = "matches", ignore = true)
    @Mapping(target = "removeMatch", ignore = true)
    @Mapping(target = "poules", ignore = true)
    @Mapping(target = "removePoule", ignore = true)
    @Mapping(target = "participations", ignore = true)
    @Mapping(target = "removeParticipation", ignore = true)
    @Mapping(source = "competitionSaisonCategoryId", target = "competitionSaisonCategory")
    @Mapping(source = "competitionSaisonId", target = "competitionSaison")
    EtapeCompetition toEntity(EtapeCompetitionDTO etapeCompetitionDTO);

    default EtapeCompetition fromId(Long id) {
        if (id == null) {
            return null;
        }
        EtapeCompetition etapeCompetition = new EtapeCompetition();
        etapeCompetition.setId(id);
        return etapeCompetition;
    }
}
