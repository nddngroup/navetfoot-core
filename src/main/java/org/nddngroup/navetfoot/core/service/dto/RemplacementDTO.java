package org.nddngroup.navetfoot.core.service.dto;

import org.nddngroup.navetfoot.core.domain.Remplacement;
import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;

import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * A DTO for the {@link Remplacement} entity.
 */
public class RemplacementDTO implements Serializable {

    private Long id;

    private String hashcode;

    private String code;

    private Integer instant;

    private ZonedDateTime createdAt;

    private ZonedDateTime updatedAt;

    private Equipe equipe;

    private Boolean deleted;


    private Long matchId;

    private Long sortantId;

    private String sortantIdentifiant;

    private Long entrantId;

    private String entrantIdentifiant;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHashcode() {
        return hashcode;
    }

    public void setHashcode(String hashcode) {
        this.hashcode = hashcode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getInstant() {
        return instant;
    }

    public void setInstant(Integer instant) {
        this.instant = instant;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Equipe getEquipe() {
        return equipe;
    }

    public void setEquipe(Equipe equipe) {
        this.equipe = equipe;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Long getMatchId() {
        return matchId;
    }

    public void setMatchId(Long matchId) {
        this.matchId = matchId;
    }

    public Long getSortantId() {
        return sortantId;
    }

    public void setSortantId(Long joueurId) {
        this.sortantId = joueurId;
    }

    public String getSortantIdentifiant() {
        return sortantIdentifiant;
    }

    public void setSortantIdentifiant(String joueurIdentifiant) {
        this.sortantIdentifiant = joueurIdentifiant;
    }

    public Long getEntrantId() {
        return entrantId;
    }

    public void setEntrantId(Long joueurId) {
        this.entrantId = joueurId;
    }

    public String getEntrantIdentifiant() {
        return entrantIdentifiant;
    }

    public void setEntrantIdentifiant(String joueurIdentifiant) {
        this.entrantIdentifiant = joueurIdentifiant;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RemplacementDTO)) {
            return false;
        }

        return id != null && id.equals(((RemplacementDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RemplacementDTO{" +
            "id=" + getId() +
            ", hashcode='" + getHashcode() + "'" +
            ", code='" + getCode() + "'" +
            ", instant=" + getInstant() +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", equipe='" + getEquipe() + "'" +
            ", deleted='" + isDeleted() + "'" +
            ", matchId=" + getMatchId() +
            ", sortantId=" + getSortantId() +
            ", sortantIdentifiant='" + getSortantIdentifiant() + "'" +
            ", entrantId=" + getEntrantId() +
            ", entrantIdentifiant='" + getEntrantIdentifiant() + "'" +
            "}";
    }
}
