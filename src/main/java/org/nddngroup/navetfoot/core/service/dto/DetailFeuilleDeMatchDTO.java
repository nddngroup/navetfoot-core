package org.nddngroup.navetfoot.core.service.dto;

import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;

import org.nddngroup.navetfoot.core.domain.DetailFeuilleDeMatch;
import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;
import org.nddngroup.navetfoot.core.domain.enumeration.Position;
import org.nddngroup.navetfoot.core.domain.enumeration.Poste;

/**
 * A DTO for the {@link DetailFeuilleDeMatch} entity.
 */
public class DetailFeuilleDeMatchDTO implements Serializable {

    private Long id;

    private Equipe equipe;

    private Integer numeroJoueur;

    private String code;

    private String hashcode;

    private ZonedDateTime updatedAt;

    private ZonedDateTime createdAt;

    private Boolean deleted;

    @NotNull
    private Position position;

    @NotNull
    private Poste poste = Poste.NONE;

    private String grille;


    private Long joueurId;

    private Long feuilleDeMatchId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Equipe getEquipe() {
        return equipe;
    }

    public void setEquipe(Equipe equipe) {
        this.equipe = equipe;
    }

    public Integer getNumeroJoueur() {
        return numeroJoueur;
    }

    public void setNumeroJoueur(Integer numeroJoueur) {
        this.numeroJoueur = numeroJoueur;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getHashcode() {
        return hashcode;
    }

    public void setHashcode(String hashcode) {
        this.hashcode = hashcode;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Poste getPoste() {
        return poste;
    }

    public void setPoste(Poste poste) {
        this.poste = poste;
    }

    public String getGrille() {
        return grille;
    }

    public void setGrille(String grille) {
        this.grille = grille;
    }

    public Long getJoueurId() {
        return joueurId;
    }

    public void setJoueurId(Long joueurId) {
        this.joueurId = joueurId;
    }

    public Long getFeuilleDeMatchId() {
        return feuilleDeMatchId;
    }

    public void setFeuilleDeMatchId(Long feuilleDeMatchId) {
        this.feuilleDeMatchId = feuilleDeMatchId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DetailFeuilleDeMatchDTO)) {
            return false;
        }

        return id != null && id.equals(((DetailFeuilleDeMatchDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DetailFeuilleDeMatchDTO{" +
            "id=" + getId() +
            ", equipe='" + getEquipe() + "'" +
            ", numeroJoueur=" + getNumeroJoueur() +
            ", code='" + getCode() + "'" +
            ", hashcode='" + getHashcode() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", deleted='" + isDeleted() + "'" +
            ", position='" + getPosition() + "'" +
            ", poste='" + getPoste() + "'" +
            ", grille='" + getGrille() + "'" +
            ", joueurId=" + getJoueurId() +
            ", feuilleDeMatchId=" + getFeuilleDeMatchId() +
            "}";
    }
}
