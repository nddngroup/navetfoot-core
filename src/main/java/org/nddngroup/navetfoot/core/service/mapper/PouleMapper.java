package org.nddngroup.navetfoot.core.service.mapper;


import org.nddngroup.navetfoot.core.domain.*;
import org.nddngroup.navetfoot.core.domain.Poule;
import org.nddngroup.navetfoot.core.service.dto.PouleDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Poule} and its DTO {@link PouleDTO}.
 */
@Mapper(componentModel = "spring", uses = {EtapeCompetitionMapper.class})
public interface PouleMapper extends EntityMapper<PouleDTO, Poule> {

    @Mapping(source = "etapeCompetition.id", target = "etapeCompetitionId")
    @Mapping(source = "etapeCompetition.nom", target = "etapeCompetitionNom")
    PouleDTO toDto(Poule poule);

    @Mapping(source = "etapeCompetitionId", target = "etapeCompetition")
    Poule toEntity(PouleDTO pouleDTO);

    default Poule fromId(Long id) {
        if (id == null) {
            return null;
        }
        Poule poule = new Poule();
        poule.setId(id);
        return poule;
    }
}
