package org.nddngroup.navetfoot.core.service.impl;

import org.nddngroup.navetfoot.core.domain.Corner;
import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;
import org.nddngroup.navetfoot.core.repository.CornerRepository;
import org.nddngroup.navetfoot.core.repository.search.CornerSearchRepository;
import org.nddngroup.navetfoot.core.service.CornerService;
import org.nddngroup.navetfoot.core.service.dto.CornerDTO;
import org.nddngroup.navetfoot.core.service.dto.MatchDTO;
import org.nddngroup.navetfoot.core.service.mapper.CornerMapper;
import org.nddngroup.navetfoot.core.service.mapper.MatchMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link Corner}.
 */
@Service
@Transactional
public class CornerServiceImpl implements CornerService {

    private final Logger log = LoggerFactory.getLogger(CornerServiceImpl.class);

    private final CornerRepository cornerRepository;

    private final CornerMapper cornerMapper;

    private final CornerSearchRepository cornerSearchRepository;
    @Autowired
    MatchMapper matchMapper;

    public CornerServiceImpl(CornerRepository cornerRepository, CornerMapper cornerMapper, CornerSearchRepository cornerSearchRepository) {
        this.cornerRepository = cornerRepository;
        this.cornerMapper = cornerMapper;
        this.cornerSearchRepository = cornerSearchRepository;
    }

    @Override
    public CornerDTO save(CornerDTO cornerDTO) {
        log.debug("Request to save Corner : {}", cornerDTO);

        cornerDTO.setUpdatedAt(ZonedDateTime.now());
        if (cornerDTO.getCreatedAt() == null) {
            cornerDTO.setCreatedAt(ZonedDateTime.now());
        }

        Corner corner = cornerMapper.toEntity(cornerDTO);
        corner = cornerRepository.save(corner);
        CornerDTO result = cornerMapper.toDto(corner);

        if (cornerDTO.isDeleted()) {
            cornerSearchRepository.deleteById(cornerDTO.getId());
        } else {
            cornerSearchRepository.save(cornerMapper.toEntity(result));
        }

        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<CornerDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Corners");
        return cornerRepository.findAll(pageable)
            .map(cornerMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<CornerDTO> findOne(Long id) {
        log.debug("Request to get Corner : {}", id);
        return cornerRepository.findById(id)
            .map(cornerMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Corner : {}", id);
        cornerRepository.deleteById(id);
        cornerSearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<CornerDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Corners for query {}", query);
        return cornerSearchRepository.search(queryStringQuery(query), pageable)
            .map(cornerMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<CornerDTO> findByMatch(MatchDTO matchDTO) {
        log.debug("Request to get all Corners by match");
        return cornerRepository.findAllByMatchAndDeletedIsFalse(matchMapper.toEntity(matchDTO))
            .stream()
            .map(cornerMapper::toDto).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<CornerDTO> findByMatchAndEquipe(MatchDTO matchDTO, Equipe equipe) {
        log.debug("Request to get all Corners by match and equipe");
        return cornerRepository.findAllByMatchAndEquipeAndDeletedIsFalse(matchMapper.toEntity(matchDTO), equipe)
            .stream()
            .map(cornerMapper::toDto).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<CornerDTO> findByHashcode(String hashcode) {
        log.debug("Request to get Corner : {}", hashcode);
        return cornerRepository.findByHashcodeAndDeletedIsFalse(hashcode)
            .map(cornerMapper::toDto);
    }

    @Override
    public List<CornerDTO> findAll() {
        return cornerRepository.findAllByDeletedIsFalse().stream()
            .map(cornerMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public void reIndex() {
        log.info("Request to reindex all Corners");
        cornerSearchRepository.deleteAll();
        findAll().stream().map(cornerMapper::toEntity).forEach(cornerSearchRepository::save);
    }
}
