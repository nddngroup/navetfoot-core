package org.nddngroup.navetfoot.core.service.impl;

import org.nddngroup.navetfoot.core.domain.EtapeCompetition;
import org.nddngroup.navetfoot.core.repository.EtapeCompetitionRepository;
import org.nddngroup.navetfoot.core.repository.search.EtapeCompetitionSearchRepository;
import org.nddngroup.navetfoot.core.service.EtapeCompetitionService;
import org.nddngroup.navetfoot.core.service.custom.CommunService;
import org.nddngroup.navetfoot.core.service.dto.*;
import org.nddngroup.navetfoot.core.service.dto.*;
import org.nddngroup.navetfoot.core.service.mapper.CompetitionSaisonCategoryMapper;
import org.nddngroup.navetfoot.core.service.mapper.CompetitionSaisonMapper;
import org.nddngroup.navetfoot.core.service.mapper.EtapeCompetitionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link EtapeCompetition}.
 */
@Service
@Transactional
public class EtapeCompetitionServiceImpl implements EtapeCompetitionService {

    private final Logger log = LoggerFactory.getLogger(EtapeCompetitionServiceImpl.class);

    private final EtapeCompetitionRepository etapeCompetitionRepository;

    private final EtapeCompetitionMapper etapeCompetitionMapper;

    private final EtapeCompetitionSearchRepository etapeCompetitionSearchRepository;

    @Autowired
    private CompetitionSaisonMapper competitionSaisonMapper;
    @Autowired
    private CompetitionSaisonCategoryMapper competitionSaisonCategoryMapper;
    @Autowired
    private CommunService communService;

    public EtapeCompetitionServiceImpl(EtapeCompetitionRepository etapeCompetitionRepository, EtapeCompetitionMapper etapeCompetitionMapper, EtapeCompetitionSearchRepository etapeCompetitionSearchRepository) {
        this.etapeCompetitionRepository = etapeCompetitionRepository;
        this.etapeCompetitionMapper = etapeCompetitionMapper;
        this.etapeCompetitionSearchRepository = etapeCompetitionSearchRepository;
    }

    @Override
    public EtapeCompetitionDTO save(EtapeCompetitionDTO etapeCompetitionDTO) {
        log.debug("Request to save EtapeCompetition : {}", etapeCompetitionDTO);
        EtapeCompetition etapeCompetition = etapeCompetitionMapper.toEntity(etapeCompetitionDTO);
        etapeCompetition = etapeCompetitionRepository.save(etapeCompetition);
        EtapeCompetitionDTO result = etapeCompetitionMapper.toDto(etapeCompetition);

        if (etapeCompetitionDTO.isDeleted()) {
            etapeCompetitionSearchRepository.deleteById(etapeCompetitionDTO.getId());
        } else {
            etapeCompetitionSearchRepository.save(etapeCompetitionMapper.toEntity(result));
        }

        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<EtapeCompetitionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all EtapeCompetitions");
        return etapeCompetitionRepository.findAll(pageable)
            .map(etapeCompetitionMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<EtapeCompetitionDTO> findOne(Long id) {
        log.debug("Request to get EtapeCompetition : {}", id);
        return etapeCompetitionRepository.findById(id)
            .map(etapeCompetitionMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete EtapeCompetition : {}", id);
        etapeCompetitionRepository.deleteById(id);
        etapeCompetitionSearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<EtapeCompetitionDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of EtapeCompetitions for query {}", query);
        return etapeCompetitionSearchRepository.search(queryStringQuery(query), pageable)
            .map(etapeCompetitionMapper::toDto);
    }

    @Override
    public List<EtapeCompetitionDTO> findByCompetitionSaisonAndCompetitionSaisonCategory(CompetitionSaisonDTO competitionSaisonDTO, CompetitionSaisonCategoryDTO competitionSaisonCategoryDTO) {
        log.debug("Request to get all EtapeCompetitions");
        return etapeCompetitionRepository.findAllByCompetitionSaisonAndCompetitionSaisonCategoryAndDeletedIsFalse(
            competitionSaisonMapper.toEntity(competitionSaisonDTO),
            competitionSaisonCategoryMapper.toEntity(competitionSaisonCategoryDTO))
            .stream()
            .map(etapeCompetitionMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public List<EtapeCompetitionDTO> findByCompetitionSaison(CompetitionSaisonDTO competitionSaisonDTO) {
        log.debug("Request to get all EtapeCompetitions by competitionSaison");
        return etapeCompetitionRepository.findAllByCompetitionSaisonAndDeletedIsFalse(
            competitionSaisonMapper.toEntity(competitionSaisonDTO))
            .stream()
            .map(etapeCompetitionMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public Optional<EtapeCompetitionDTO> findByHashcode(String hashcode) {
        log.debug("Request to get EtapeCompetition by hashcode: {}", hashcode);
        return etapeCompetitionRepository.findByHashcodeAndDeletedIsFalse(hashcode)
            .map(etapeCompetitionMapper::toDto);
    }

    @Override
    public List<EtapeCompetitionDTO> findByCompetitionSaisons(List<Long> competitionSaisonIds) {
        log.debug("Request to get all EtapeCompetitions by competitionSaison list");
        return etapeCompetitionRepository.findAllByCompetitionSaisonIdInAndDeletedIsFalse(competitionSaisonIds)
            .stream()
            .map(etapeCompetitionMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public EtapeCompetitionDTO save(EtapeDTO etapeDTO,
                                    OrganisationDTO organisationDTO,
                                    SaisonDTO saisonDTO,
                                    CompetitionDTO competitionDTO,
                                    Long categoryId,
                                    boolean allerRetour,
                                    boolean hasPoule) {
        log.debug("Request to save Etape : {} to organisation {}, saison {}, competition {} and category {}",
            etapeDTO, organisationDTO, saisonDTO, competitionDTO, categoryId);

        // find etapeLibelle
        EtapeDTO etape = this.communService.checkEtapeLibelle(etapeDTO.getId());

        // find competitionSaison
        CompetitionSaisonDTO competitionSaisonDTO = this.communService.checkCompetitionSaison(saisonDTO, competitionDTO, categoryId, etape);

        // find competitionSaisonCategory
        CompetitionSaisonCategoryDTO competitionSaisonCategoryDTO = communService.getCompetitionSaisonCategory(competitionSaisonDTO, categoryId);


        EtapeCompetitionDTO etapeCompetitionDTO = new EtapeCompetitionDTO();

        etapeCompetitionDTO.setNom(etape.getLibelle());
        etapeCompetitionDTO.setCode("REF_" + organisationDTO.getId());

        String hashcode = organisationDTO.getId() + ZonedDateTime.now().toString();
        etapeCompetitionDTO.setHashcode(this.communService.toHash(hashcode));

        etapeCompetitionDTO.setAllerRetour(allerRetour);
        etapeCompetitionDTO.setCompetitionSaisonId(competitionSaisonDTO.getId());
        etapeCompetitionDTO.setCompetitionSaisonCategoryId(competitionSaisonCategoryDTO.getId());
        etapeCompetitionDTO.setHasPoule(hasPoule);
        etapeCompetitionDTO.setDeleted(false);

        etapeCompetitionDTO.setCreatedAt(ZonedDateTime.now());
        etapeCompetitionDTO.setUpdatedAt(ZonedDateTime.now());
/*
        etapeCompetitionDTO = save(etapeCompetitionDTO);

        etapeCompetitionDTO.setCode(etapeCompetitionDTO.getCode() + "_" + etapeCompetitionDTO.getId());
        etapeCompetitionDTO.setHashcode(this.communService.toHash(etapeCompetitionDTO.getId().toString()));*/

        return save(etapeCompetitionDTO);
    }

    @Override
    public List<EtapeCompetitionDTO> findAll() {
        return etapeCompetitionRepository.findAllByDeletedIsFalse().stream()
            .map(etapeCompetitionMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public void reIndex() {
        log.info("Request to reindex all EtapeCompetitions");
        etapeCompetitionSearchRepository.deleteAll();
        findAll().stream().map(etapeCompetitionMapper::toEntity).forEach(etapeCompetitionSearchRepository::save);
    }
}
