package org.nddngroup.navetfoot.core.service.mapper;


import org.nddngroup.navetfoot.core.domain.*;
import org.nddngroup.navetfoot.core.domain.HorsJeu;
import org.nddngroup.navetfoot.core.service.dto.HorsJeuDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link HorsJeu} and its DTO {@link HorsJeuDTO}.
 */
@Mapper(componentModel = "spring", uses = {MatchMapper.class})
public interface HorsJeuMapper extends EntityMapper<HorsJeuDTO, HorsJeu> {

    @Mapping(source = "match.id", target = "matchId")
    HorsJeuDTO toDto(HorsJeu horsJeu);

    @Mapping(source = "matchId", target = "match")
    HorsJeu toEntity(HorsJeuDTO horsJeuDTO);

    default HorsJeu fromId(Long id) {
        if (id == null) {
            return null;
        }
        HorsJeu horsJeu = new HorsJeu();
        horsJeu.setId(id);
        return horsJeu;
    }
}
