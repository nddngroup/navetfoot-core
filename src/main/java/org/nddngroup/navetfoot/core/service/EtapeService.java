package org.nddngroup.navetfoot.core.service;

import org.nddngroup.navetfoot.core.domain.Etape;
import org.nddngroup.navetfoot.core.service.dto.EtapeDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Etape}.
 */
public interface EtapeService {

    /**
     * Save a etape.
     *
     * @param etapeDTO the entity to save.
     * @return the persisted entity.
     */
    EtapeDTO save(EtapeDTO etapeDTO);

    /**
     * Get all the etapes.
     *
     * @return the list of entities.
     */
    Page<EtapeDTO> findAll(Pageable pageable);


    /**
     * Get the "id" etape.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<EtapeDTO> findOne(Long id);

    /**
     * Delete the "id" etape.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the etape corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @return the list of entities.
     */
    List<EtapeDTO> search(String query);
    Optional<EtapeDTO> findByLibelle(String libelle);
    List<EtapeDTO> findAll();
    void reIndex();
}
