package org.nddngroup.navetfoot.core.service;

import org.nddngroup.navetfoot.core.service.dto.*;
import org.nddngroup.navetfoot.core.domain.Joueur;
import org.nddngroup.navetfoot.core.service.dto.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Joueur}.
 */
public interface JoueurService {

    /**
     * Save a joueur.
     *
     * @param joueurDTO the entity to save.
     * @return the persisted entity.
     */
    JoueurDTO save(JoueurDTO joueurDTO);

    /**
     * Get all the joueurs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<JoueurDTO> findAll(Pageable pageable);


    /**
     * Get the "id" joueur.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<JoueurDTO> findOne(Long id);

    /**
     * Delete the "id" joueur.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the joueur corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<JoueurDTO> search(String query, Pageable pageable);
    Optional<JoueurDTO> findByIdentifiant(String numero);
    Page<JoueurDTO> findByIdIn(List<Long> joueurIds, Pageable pageable);
    Optional<JoueurDTO> findByHashcode(String hashcode);
    List<JoueurDTO> save(OrganisationDTO organisationDTO, SaisonDTO saisonDTO, AssociationDTO associationDTO, List<JoueurDTO> joueurDTOs);
    JoueurDTO save(JoueurDTO joueurDTO, OrganisationDTO organisationDTO, SaisonDTO saisonDTO, AssociationDTO associationDTO, CategoryDTO categoryDTO);
    Page<JoueurDTO> search(OrganisationDTO organisationDTO, Long associationId, String query, Pageable pageable);
    List<JoueurDTO> findAll();
    void reIndex();
}
