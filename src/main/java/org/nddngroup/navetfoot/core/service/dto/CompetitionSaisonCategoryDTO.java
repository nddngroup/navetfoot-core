package org.nddngroup.navetfoot.core.service.dto;

import org.nddngroup.navetfoot.core.domain.CompetitionSaisonCategory;

import java.time.ZonedDateTime;
import java.io.Serializable;

/**
 * A DTO for the {@link CompetitionSaisonCategory} entity.
 */
public class CompetitionSaisonCategoryDTO implements Serializable {

    private Long id;

    private String hashcode;

    private String code;

    private ZonedDateTime createdAt;

    private ZonedDateTime updatedAt;

    private Boolean deleted;


    private Long competitionSaisonId;

    private Long categoryId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHashcode() {
        return hashcode;
    }

    public void setHashcode(String hashcode) {
        this.hashcode = hashcode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Long getCompetitionSaisonId() {
        return competitionSaisonId;
    }

    public void setCompetitionSaisonId(Long competitionSaisonId) {
        this.competitionSaisonId = competitionSaisonId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CompetitionSaisonCategoryDTO)) {
            return false;
        }

        return id != null && id.equals(((CompetitionSaisonCategoryDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CompetitionSaisonCategoryDTO{" +
            "id=" + getId() +
            ", hashcode='" + getHashcode() + "'" +
            ", code='" + getCode() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", deleted='" + isDeleted() + "'" +
            ", competitionSaisonId=" + getCompetitionSaisonId() +
            ", categoryId=" + getCategoryId() +
            "}";
    }
}
