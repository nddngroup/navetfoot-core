package org.nddngroup.navetfoot.core.service.mapper;


import org.nddngroup.navetfoot.core.domain.*;
import org.nddngroup.navetfoot.core.domain.Carton;
import org.nddngroup.navetfoot.core.service.dto.CartonDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Carton} and its DTO {@link CartonDTO}.
 */
@Mapper(componentModel = "spring", uses = {JoueurMapper.class, MatchMapper.class})
public interface CartonMapper extends EntityMapper<CartonDTO, Carton> {

    @Mapping(source = "joueur.id", target = "joueurId")
    @Mapping(source = "match.id", target = "matchId")
    CartonDTO toDto(Carton carton);

    @Mapping(source = "joueurId", target = "joueur")
    @Mapping(source = "matchId", target = "match")
    Carton toEntity(CartonDTO cartonDTO);

    default Carton fromId(Long id) {
        if (id == null) {
            return null;
        }
        Carton carton = new Carton();
        carton.setId(id);
        return carton;
    }
}
