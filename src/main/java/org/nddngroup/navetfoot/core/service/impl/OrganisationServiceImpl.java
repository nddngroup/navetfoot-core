package org.nddngroup.navetfoot.core.service.impl;

import org.nddngroup.navetfoot.core.domain.Organisation;
import org.nddngroup.navetfoot.core.repository.OrganisationRepository;
import org.nddngroup.navetfoot.core.repository.search.OrganisationSearchRepository;
import org.nddngroup.navetfoot.core.service.OrganisationService;
import org.nddngroup.navetfoot.core.service.dto.OrganisationDTO;
import org.nddngroup.navetfoot.core.service.mapper.OrganisationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link Organisation}.
 */
@Service
@Transactional
public class OrganisationServiceImpl implements OrganisationService {

    private final Logger log = LoggerFactory.getLogger(OrganisationServiceImpl.class);

    private final OrganisationRepository organisationRepository;

    private final OrganisationMapper organisationMapper;

    private final OrganisationSearchRepository organisationSearchRepository;

    public OrganisationServiceImpl(OrganisationRepository organisationRepository, OrganisationMapper organisationMapper, OrganisationSearchRepository organisationSearchRepository) {
        this.organisationRepository = organisationRepository;
        this.organisationMapper = organisationMapper;
        this.organisationSearchRepository = organisationSearchRepository;
    }

    @Override
    public OrganisationDTO save(OrganisationDTO organisationDTO) {
        log.debug("Request to save Organisation : {}", organisationDTO);
        Organisation organisation = organisationMapper.toEntity(organisationDTO);
        organisation = organisationRepository.save(organisation);
        OrganisationDTO result = organisationMapper.toDto(organisation);

        if (Boolean.TRUE.equals(organisationDTO.isDeleted())) {
            organisationSearchRepository.deleteById(organisationDTO.getId());
        } else {
            organisationSearchRepository.save(organisationMapper.toEntity(result));
        }

        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<OrganisationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Organisations");
        return organisationRepository.findAllByDeletedIsFalse(pageable)
            .map(organisationMapper::toDto);
    }


    public Page<OrganisationDTO> findAllWithEagerRelationships(Pageable pageable) {
        return organisationRepository.findAllWithEagerRelationships(pageable).map(organisationMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<OrganisationDTO> findOne(Long id) {
        log.debug("Request to get Organisation : {}", id);
        return organisationRepository.findOneWithEagerRelationships(id)
            .map(organisationMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Organisation : {}", id);
        organisationRepository.deleteById(id);
        organisationSearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<OrganisationDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Organisations for query {}", query);
        return organisationSearchRepository.search(queryStringQuery(query), pageable)
            .map(organisationMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<OrganisationDTO> findAll(Long userId) {
        log.debug("Request to get all Organisations by userId");
        return organisationRepository.findAllByUserId(userId)
            .stream().map(organisationMapper::toDto).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<OrganisationDTO> findAll() {
        log.debug("Request to get all Organisations");
        return organisationRepository.findAllByDeletedIsFalse()
            .stream().map(organisationMapper::toDto).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<OrganisationDTO> findAllOrganisations() {
        log.debug("Request to get all Organisations by userId");
        return organisationRepository.findAllOrganisationByDeletedIsFalse()
            .stream().map(organisationMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public Optional<OrganisationDTO> findByHashcode(String hashcode) {
        log.debug("Request to get Organisation : {}", hashcode);
        return organisationRepository.findByHashcodeAndDeletedIsFalse(hashcode)
            .map(organisationMapper::toDto);
    }

    @Override
    public void reIndex() {
        log.info("Request to reindex Organisations");
        organisationSearchRepository.deleteAll();
        findAll().stream().map(organisationMapper::toEntity).forEach(organisationSearchRepository::save);
    }
}
