package org.nddngroup.navetfoot.core.service.impl;

import org.nddngroup.navetfoot.core.domain.CompetitionSaison;
import org.nddngroup.navetfoot.core.repository.CompetitionSaisonRepository;
import org.nddngroup.navetfoot.core.repository.search.CompetitionSaisonSearchRepository;
import org.nddngroup.navetfoot.core.service.CompetitionSaisonService;
import org.nddngroup.navetfoot.core.service.custom.DeleteService;
import org.nddngroup.navetfoot.core.service.dto.CompetitionDTO;
import org.nddngroup.navetfoot.core.service.dto.CompetitionSaisonDTO;
import org.nddngroup.navetfoot.core.service.dto.SaisonDTO;
import org.nddngroup.navetfoot.core.service.mapper.CompetitionMapper;
import org.nddngroup.navetfoot.core.service.mapper.CompetitionSaisonMapper;
import org.nddngroup.navetfoot.core.service.mapper.SaisonMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link CompetitionSaison}.
 */
@Service
@Transactional
public class CompetitionSaisonServiceImpl implements CompetitionSaisonService {

    private final Logger log = LoggerFactory.getLogger(CompetitionSaisonServiceImpl.class);

    private final CompetitionSaisonRepository competitionSaisonRepository;

    private final CompetitionSaisonMapper competitionSaisonMapper;

    private final CompetitionSaisonSearchRepository competitionSaisonSearchRepository;
    @Autowired
    private SaisonMapper saisonMapper;
    @Autowired
    private CompetitionMapper competitionMapper;
    @Autowired
    private DeleteService deleteService;

    public CompetitionSaisonServiceImpl(CompetitionSaisonRepository competitionSaisonRepository, CompetitionSaisonMapper competitionSaisonMapper, CompetitionSaisonSearchRepository competitionSaisonSearchRepository) {
        this.competitionSaisonRepository = competitionSaisonRepository;
        this.competitionSaisonMapper = competitionSaisonMapper;
        this.competitionSaisonSearchRepository = competitionSaisonSearchRepository;
    }

    @Override
    public CompetitionSaisonDTO save(CompetitionSaisonDTO competitionSaisonDTO) {
        log.debug("Request to save CompetitionSaison : {}", competitionSaisonDTO);
        CompetitionSaison competitionSaison = competitionSaisonMapper.toEntity(competitionSaisonDTO);
        competitionSaison = competitionSaisonRepository.save(competitionSaison);
        CompetitionSaisonDTO result = competitionSaisonMapper.toDto(competitionSaison);

        if (competitionSaisonDTO.isDeleted()) {
            competitionSaisonSearchRepository.deleteById(competitionSaisonDTO.getId());
        } else {
            competitionSaisonSearchRepository.save(competitionSaisonMapper.toEntity(result));
        }

        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<CompetitionSaisonDTO> findAll(Pageable pageable) {
        log.debug("Request to get all CompetitionSaisons");
        return competitionSaisonRepository.findAll(pageable)
            .map(competitionSaisonMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<CompetitionSaisonDTO> findOne(Long id) {
        log.debug("Request to get CompetitionSaison : {}", id);
        return competitionSaisonRepository.findById(id)
            .map(competitionSaisonMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete CompetitionSaison : {}", id);
        competitionSaisonRepository.deleteById(id);
        competitionSaisonSearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<CompetitionSaisonDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of CompetitionSaisons for query {}", query);
        return competitionSaisonSearchRepository.search(queryStringQuery(query), pageable)
            .map(competitionSaisonMapper::toDto);
    }

    @Override
    public List<CompetitionSaisonDTO> findBySaison(SaisonDTO saisonDTO) {
        log.debug("Request to get all CompetitionSaisons by saison");
        return competitionSaisonRepository.findAllBySaisonAndDeletedIsFalse(saisonMapper.toEntity(saisonDTO))
            .stream()
            .map(competitionSaisonMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public List<CompetitionSaisonDTO> findBySaisonAndCompetition(SaisonDTO saisonDTO, CompetitionDTO competitionDTO) {
        log.debug("Request to get all CompetitionSaisons by saison");
        return competitionSaisonRepository.findAllBySaisonAndCompetitionAndDeletedIsFalse(saisonMapper.toEntity(saisonDTO), competitionMapper.toEntity(competitionDTO))
            .stream()
            .map(competitionSaisonMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public Optional<CompetitionSaisonDTO> findByHashcode(String hashcode) {
        log.debug("Request to get CompetitionSaison by hashcode: {}", hashcode);
        return competitionSaisonRepository.findByHashcodeAndDeletedIsFalse(hashcode)
            .map(competitionSaisonMapper::toDto);
    }

    @Override
    public List<CompetitionSaisonDTO> findByCompetition(CompetitionDTO competitionDTO) {
        log.debug("Request to get all CompetitionSaisons by competition: {}", competitionDTO);
        return competitionSaisonRepository.findAllByCompetitionAndDeletedIsFalse(competitionMapper.toEntity(competitionDTO))
            .stream()
            .map(competitionSaisonMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public List<CompetitionSaisonDTO> findBySaisons(List<Long> saisonIds) {
        log.debug("Request to get all CompetitionSaisons by saison");

        return competitionSaisonRepository.findAllBySaisonIdInAndDeletedIsFalse(saisonIds)
            .stream()
            .map(competitionSaisonMapper::toDto).collect(Collectors.toList());
    }


    @Override
    public List<CompetitionSaisonDTO> findAll() {
        return competitionSaisonRepository.findAllByDeletedIsFalse().stream()
            .map(competitionSaisonMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public void reIndex() {
        log.info("Request to reindex all CompetitionSaisons");
        competitionSaisonSearchRepository.deleteAll();
        findAll().stream().map(competitionSaisonMapper::toEntity).forEach(competitionSaisonSearchRepository::save);
    }
}
