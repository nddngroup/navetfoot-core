package org.nddngroup.navetfoot.core.service.impl;

import org.nddngroup.navetfoot.core.domain.Etape;
import org.nddngroup.navetfoot.core.repository.EtapeRepository;
import org.nddngroup.navetfoot.core.repository.search.EtapeSearchRepository;
import org.nddngroup.navetfoot.core.service.EtapeService;
import org.nddngroup.navetfoot.core.service.dto.EtapeDTO;
import org.nddngroup.navetfoot.core.service.mapper.EtapeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link Etape}.
 */
@Service
@Transactional
public class EtapeServiceImpl implements EtapeService {

    private final Logger log = LoggerFactory.getLogger(EtapeServiceImpl.class);

    private final EtapeRepository etapeRepository;

    private final EtapeMapper etapeMapper;

    private final EtapeSearchRepository etapeSearchRepository;

    public EtapeServiceImpl(EtapeRepository etapeRepository, EtapeMapper etapeMapper, EtapeSearchRepository etapeSearchRepository) {
        this.etapeRepository = etapeRepository;
        this.etapeMapper = etapeMapper;
        this.etapeSearchRepository = etapeSearchRepository;
    }

    @Override
    public EtapeDTO save(EtapeDTO etapeDTO) {
        log.debug("Request to save Etape : {}", etapeDTO);
        Etape etape = etapeMapper.toEntity(etapeDTO);
        etape = etapeRepository.save(etape);
        EtapeDTO result = etapeMapper.toDto(etape);
        etapeSearchRepository.save(etape);
        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<EtapeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Etapes");
        return etapeRepository.findAll(pageable)
            .map(etapeMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<EtapeDTO> findOne(Long id) {
        log.debug("Request to get Etape : {}", id);
        return etapeRepository.findById(id)
            .map(etapeMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Etape : {}", id);
        etapeRepository.deleteById(id);
        etapeSearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<EtapeDTO> search(String query) {
        log.debug("Request to search Etapes for query {}", query);
        return StreamSupport
            .stream(etapeSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(etapeMapper::toDto)
        .collect(Collectors.toList());
    }

    @Override
    public Optional<EtapeDTO> findByLibelle(String libelle) {
        log.debug("Request to get Etape by libelle : {}", libelle);
        return etapeRepository.findByLibelle(libelle)
            .map(etapeMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<EtapeDTO> findAll() {
        log.debug("Request to get all Etapes");
        return etapeRepository.findAll().stream()
            .map(etapeMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public void reIndex() {
        log.info("Request to reindex all Etapes");
        etapeSearchRepository.deleteAll();
        findAll().stream().map(etapeMapper::toEntity).forEach(etapeSearchRepository::save);
    }
}
