package org.nddngroup.navetfoot.core.service;

import org.nddngroup.navetfoot.core.domain.DetailPoule;
import org.nddngroup.navetfoot.core.service.dto.*;
import org.nddngroup.navetfoot.core.service.dto.AssociationDTO;
import org.nddngroup.navetfoot.core.service.dto.DetailPouleDTO;
import org.nddngroup.navetfoot.core.service.dto.OrganisationDTO;
import org.nddngroup.navetfoot.core.service.dto.PouleDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link DetailPoule}.
 */
public interface DetailPouleService {

    /**
     * Save a detailPoule.
     *
     * @param detailPouleDTO the entity to save.
     * @return the persisted entity.
     */
    DetailPouleDTO save(DetailPouleDTO detailPouleDTO);

    /**
     * Get all the detailPoules.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<DetailPouleDTO> findAll(Pageable pageable);


    /**
     * Get the "id" detailPoule.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<DetailPouleDTO> findOne(Long id);

    /**
     * Delete the "id" detailPoule.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the detailPoule corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<DetailPouleDTO> search(String query, Pageable pageable);
    List<DetailPouleDTO> findByPoule(PouleDTO pouleDTO);
    Optional<DetailPouleDTO> findByHashcode(String hashcode);
    void saveAll(List<DetailPouleDTO> detailPouleDTOS);
    DetailPouleDTO getToSave(PouleDTO pouleDTO, AssociationDTO associationDTO, OrganisationDTO organisationDTO, int journee);
    Optional<DetailPouleDTO> findByPouleAndAssociationAndJournee(PouleDTO pouleDTO, AssociationDTO associationDTO, int journee);
    List<DetailPouleDTO> findByPouleInAndAssociation(List<Long> pouleIds, AssociationDTO associationDTO);
    List<DetailPouleDTO> findByPouleAndJourneeGreaterThan(PouleDTO pouleDTO, int totalJournee);
    List<DetailPouleDTO> findAll();
    void reIndex();
}
