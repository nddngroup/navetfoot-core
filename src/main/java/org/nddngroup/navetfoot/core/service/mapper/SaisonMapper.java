package org.nddngroup.navetfoot.core.service.mapper;


import org.nddngroup.navetfoot.core.domain.*;
import org.nddngroup.navetfoot.core.domain.Saison;
import org.nddngroup.navetfoot.core.service.dto.SaisonDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Saison} and its DTO {@link SaisonDTO}.
 */
@Mapper(componentModel = "spring", uses = {OrganisationMapper.class})
public interface SaisonMapper extends EntityMapper<SaisonDTO, Saison> {

    @Mapping(source = "organisation.id", target = "organisationId")
    @Mapping(source = "organisation.nom", target = "organisationNom")
    SaisonDTO toDto(Saison saison);

    @Mapping(target = "competitionSaisons", ignore = true)
    @Mapping(target = "removeCompetitionSaison", ignore = true)
    @Mapping(source = "organisationId", target = "organisation")
    Saison toEntity(SaisonDTO saisonDTO);

    default Saison fromId(Long id) {
        if (id == null) {
            return null;
        }
        Saison saison = new Saison();
        saison.setId(id);
        return saison;
    }
}
