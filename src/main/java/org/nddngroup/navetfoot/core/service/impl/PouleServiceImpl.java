package org.nddngroup.navetfoot.core.service.impl;

import org.nddngroup.navetfoot.core.domain.Poule;
import org.nddngroup.navetfoot.core.exception.ApiRequestException;
import org.nddngroup.navetfoot.core.exception.ExceptionCode;
import org.nddngroup.navetfoot.core.exception.ExceptionLevel;
import org.nddngroup.navetfoot.core.repository.PouleRepository;
import org.nddngroup.navetfoot.core.repository.search.PouleSearchRepository;
import org.nddngroup.navetfoot.core.service.DetailPouleService;
import org.nddngroup.navetfoot.core.service.PouleService;
import org.nddngroup.navetfoot.core.service.custom.CommunService;
import org.nddngroup.navetfoot.core.service.dto.*;
import org.nddngroup.navetfoot.core.service.dto.*;
import org.nddngroup.navetfoot.core.service.mapper.EtapeCompetitionMapper;
import org.nddngroup.navetfoot.core.service.mapper.PouleMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link Poule}.
 */
@Service
@Transactional
public class PouleServiceImpl implements PouleService {

    private final Logger log = LoggerFactory.getLogger(PouleServiceImpl.class);

    private final PouleRepository pouleRepository;

    private final PouleMapper pouleMapper;

    private final PouleSearchRepository pouleSearchRepository;
    @Autowired
    private EtapeCompetitionMapper etapeCompetitionMapper;
    @Autowired
    private CommunService communService;
    @Autowired
    private DetailPouleService detailPouleService;

    public PouleServiceImpl(PouleRepository pouleRepository, PouleMapper pouleMapper, PouleSearchRepository pouleSearchRepository) {
        this.pouleRepository = pouleRepository;
        this.pouleMapper = pouleMapper;
        this.pouleSearchRepository = pouleSearchRepository;
    }

    @Override
    public PouleDTO save(PouleDTO pouleDTO) {
        log.debug("Request to save Poule : {}", pouleDTO);
        Poule poule = pouleMapper.toEntity(pouleDTO);

        poule = pouleRepository.save(poule);
        PouleDTO result = pouleMapper.toDto(poule);

        if (pouleDTO.isDeleted()) {
            pouleSearchRepository.deleteById(pouleDTO.getId());
        } else {
            pouleSearchRepository.save(pouleMapper.toEntity(result));
        }

        return result;

    }

    @Override
    @Transactional(readOnly = true)
    public Page<PouleDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Poules");
        return pouleRepository.findAll(pageable)
            .map(pouleMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<PouleDTO> findOne(Long id) {
        log.debug("Request to get Poule : {}", id);
        return pouleRepository.findById(id)
            .map(pouleMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Poule : {}", id);
        pouleRepository.deleteById(id);
        pouleSearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<PouleDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Poules for query {}", query);
        return pouleSearchRepository.search(queryStringQuery(query), pageable)
            .map(pouleMapper::toDto);
    }

    @Override
    public List<PouleDTO> findByEtapeCompetition(EtapeCompetitionDTO etapeCompetitionDTO) {
        log.debug("Request to get all Poules");
        return pouleRepository.findAllByEtapeCompetitionAndDeletedIsFalse(etapeCompetitionMapper.toEntity(etapeCompetitionDTO))
            .stream()
            .map(pouleMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public Optional<PouleDTO> findByHashcode(String hashcode) {
        log.debug("Request to get poule by hashcode: {}", hashcode);
        return pouleRepository.findByHashcodeAndDeletedIsFalse(hashcode)
            .map(pouleMapper::toDto);
    }

    @Override
    public PouleDTO save(PouleDTO pouleDTO, OrganisationDTO organisationDTO) {
        log.debug("Request to save poules {} to organisation {}", pouleDTO, organisationDTO);

        String toHash = organisationDTO.getId().toString() + ZonedDateTime.now();
        pouleDTO.setHashcode(this.communService.toHash(toHash));

        pouleDTO.setCode("REF_" + organisationDTO.getId().toString());

        pouleDTO.setUpdatedAt(ZonedDateTime.now());
        pouleDTO.setDeleted(false);

        return save(pouleDTO);
    }

    @Override
    public PouleDTO save(PouleDTO pouleDTO, List<AssociationDTO> associationDTOS, OrganisationDTO organisationDTO) {
        log.debug("Request to save poules {} to organisation {}", pouleDTO, organisationDTO);

        this.communService.checkPoule(pouleDTO);

        if (pouleDTO.getId() == null) {
            pouleDTO = save(pouleDTO, organisationDTO);
        } else {
            pouleDTO = save(pouleDTO);
        }
/*
        result.setCode(pouleDTO.getCode() + "_" + result.getId().toString());
        result.setHashcode(this.communService.toHash(result.getId().toString()));
        save(result);*/

        // add details poule
        if (associationDTOS == null) {
            throw new ApiRequestException(
                ExceptionCode.ASSOCIATIONS_NOT_FOUND.getMessage(),
                ExceptionCode.ASSOCIATIONS_NOT_FOUND.getValue(),
                ExceptionLevel.ERROR
            );
        }

        Integer nbJournees = pouleDTO.getNombreJournee();

        for (AssociationDTO associationDTO: associationDTOS) {
            log.debug("_______ club for detailPoule : {}", associationDTO);
            if (associationDTO != null) {
                int i = 1;

                List<DetailPouleDTO> detailPouleDTOS = new ArrayList<>();

                while(i <= nbJournees) {
                    if (detailPouleService.findByPouleAndAssociationAndJournee(pouleDTO, associationDTO, i).isEmpty()) {
                        detailPouleDTOS.add(detailPouleService.getToSave(pouleDTO, associationDTO, organisationDTO, i));
                    }
                    i++;
                }

                detailPouleService.saveAll(detailPouleDTOS);
            }
        }

        // find detailsPoule to delete
        List<DetailPouleDTO> detailPouleDTOS = detailPouleService.findByPouleAndJourneeGreaterThan(pouleDTO, nbJournees)
            .stream().peek(detailPouleDTO -> {
                detailPouleDTO.setDeleted(true);
                detailPouleDTO.setUpdateAt(ZonedDateTime.now());
            }).collect(Collectors.toList());

        detailPouleService.saveAll(detailPouleDTOS);

        return pouleDTO;
    }

    @Override
    public List<PouleDTO> findAll() {
        return pouleRepository.findAllByDeletedIsFalse().stream()
            .map(pouleMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public void reIndex() {
        log.info("Request to reindex all Poules");
        pouleSearchRepository.deleteAll();
        findAll().stream().map(pouleMapper::toEntity).forEach(pouleSearchRepository::save);
    }
}
