package org.nddngroup.navetfoot.core.service;

import org.nddngroup.navetfoot.core.domain.Saison;
import org.nddngroup.navetfoot.core.service.dto.OrganisationDTO;
import org.nddngroup.navetfoot.core.service.dto.SaisonDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Saison}.
 */
public interface SaisonService {

    /**
     * Save a saison.
     *
     * @param saisonDTO the entity to save.
     * @return the persisted entity.
     */
    SaisonDTO save(SaisonDTO saisonDTO);

    /**
     * Get all the saisons.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<SaisonDTO> findAll(Pageable pageable);


    /**
     * Get the "id" saison.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<SaisonDTO> findOne(Long id);

    /**
     * Delete the "id" saison.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the saison corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<SaisonDTO> search(String query, Pageable pageable);
    Page<SaisonDTO> findAllByOrganisation(OrganisationDTO organisationDTO, Pageable pageable);
    List<SaisonDTO> findAllByOrganisation(OrganisationDTO organisationDTO);
    List<SaisonDTO> findPreviousByOrganisation(SaisonDTO saisonDTO, OrganisationDTO organisationDTO);
    Optional<SaisonDTO> findByHashcode(String hashcode);
    List<SaisonDTO> save(OrganisationDTO organisationDTO, List<SaisonDTO> saisonDTOS);
    SaisonDTO save(OrganisationDTO organisationDTO, SaisonDTO saisonDTO);
    Optional<SaisonDTO> findByOrganisationAndLibelle(OrganisationDTO organisationDTO, String libelle);
    Page<SaisonDTO> search(OrganisationDTO organisationDTO, String query, Pageable pageable);
    List<SaisonDTO> findAll();
    Page<SaisonDTO> findByIdIn(List<Long> saisonIds, Pageable pageable);
    void reIndex();
}
