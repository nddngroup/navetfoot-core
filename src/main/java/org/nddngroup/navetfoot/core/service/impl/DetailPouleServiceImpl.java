package org.nddngroup.navetfoot.core.service.impl;

import org.nddngroup.navetfoot.core.domain.DetailPoule;
import org.nddngroup.navetfoot.core.repository.DetailPouleRepository;
import org.nddngroup.navetfoot.core.repository.search.DetailPouleSearchRepository;
import org.nddngroup.navetfoot.core.service.DetailPouleService;
import org.nddngroup.navetfoot.core.service.custom.CommunService;
import org.nddngroup.navetfoot.core.service.dto.*;
import org.nddngroup.navetfoot.core.service.dto.AssociationDTO;
import org.nddngroup.navetfoot.core.service.dto.DetailPouleDTO;
import org.nddngroup.navetfoot.core.service.dto.OrganisationDTO;
import org.nddngroup.navetfoot.core.service.dto.PouleDTO;
import org.nddngroup.navetfoot.core.service.mapper.AssociationMapper;
import org.nddngroup.navetfoot.core.service.mapper.DetailPouleMapper;
import org.nddngroup.navetfoot.core.service.mapper.PouleMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link DetailPoule}.
 */
@Service
@Transactional
public class DetailPouleServiceImpl implements DetailPouleService {

    private final Logger log = LoggerFactory.getLogger(DetailPouleServiceImpl.class);

    private final DetailPouleRepository detailPouleRepository;

    private final DetailPouleMapper detailPouleMapper;

    private final DetailPouleSearchRepository detailPouleSearchRepository;
    @Autowired
    private PouleMapper pouleMapper;
    @Autowired
    private AssociationMapper associationMapper;
    @Autowired
    private CommunService communService;

    public DetailPouleServiceImpl(DetailPouleRepository detailPouleRepository, DetailPouleMapper detailPouleMapper, DetailPouleSearchRepository detailPouleSearchRepository) {
        this.detailPouleRepository = detailPouleRepository;
        this.detailPouleMapper = detailPouleMapper;
        this.detailPouleSearchRepository = detailPouleSearchRepository;
    }

    @Override
    public DetailPouleDTO save(DetailPouleDTO detailPouleDTO) {
        log.debug("Request to save DetailPoule : {}", detailPouleDTO);
        DetailPoule detailPoule = detailPouleMapper.toEntity(detailPouleDTO);
        detailPoule = detailPouleRepository.save(detailPoule);
        DetailPouleDTO result = detailPouleMapper.toDto(detailPoule);

        if (detailPouleDTO.isDeleted()) {
            detailPouleSearchRepository.deleteById(detailPouleDTO.getId());
        } else {
            detailPouleSearchRepository.save(detailPouleMapper.toEntity(result));
        }

        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<DetailPouleDTO> findAll(Pageable pageable) {
        log.debug("Request to get all DetailPoules");
        return detailPouleRepository.findAll(pageable)
            .map(detailPouleMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<DetailPouleDTO> findOne(Long id) {
        log.debug("Request to get DetailPoule : {}", id);
        return detailPouleRepository.findById(id)
            .map(detailPouleMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete DetailPoule : {}", id);
        detailPouleRepository.deleteById(id);
        detailPouleSearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<DetailPouleDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of DetailPoules for query {}", query);
        return detailPouleSearchRepository.search(queryStringQuery(query), pageable)
            .map(detailPouleMapper::toDto);
    }

    @Override
    public List<DetailPouleDTO> findByPoule(PouleDTO pouleDTO) {
        log.debug("Request to get all DetailPoules");
        return detailPouleRepository.findAllByPouleAndDeletedIsFalse(pouleMapper.toEntity(pouleDTO))
            .stream()
            .map(detailPouleMapper::toDto).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<DetailPouleDTO> findByHashcode(String hashcode) {
        log.debug("Request to get DetailPoule : {}", hashcode);
        return detailPouleRepository.findByHashcodeAndDeletedIsFalse(hashcode)
            .map(detailPouleMapper::toDto);
    }

    @Override
    public void saveAll(List<DetailPouleDTO> detailPouleDTOS) {
        log.debug("Request to save all DetailPoules : {}", detailPouleDTOS);
        List<DetailPoule> detailPoules = detailPouleMapper.toEntity(detailPouleDTOS);

        List<DetailPoule> savedDetailPoules = detailPouleRepository.saveAll(detailPoules);

        detailPouleSearchRepository.saveAll(savedDetailPoules);
    }

    @Override
    public DetailPouleDTO getToSave(PouleDTO pouleDTO, AssociationDTO associationDTO, OrganisationDTO organisationDTO, int journee) {
        DetailPouleDTO detailPouleDTO = new DetailPouleDTO();

        detailPouleDTO.setAssociationId(associationDTO.getId());
        detailPouleDTO.setPouleId(pouleDTO.getId());

        String toHash2 = organisationDTO.getId() + pouleDTO.getId() + associationDTO.getId() + ZonedDateTime.now().toString() + journee;

        detailPouleDTO.setHashcode(this.communService.toHash(toHash2));
        detailPouleDTO.setCode("REF_" + organisationDTO.getId() + "_" + associationDTO.getId() + journee);

        detailPouleDTO.setCreatedAt(ZonedDateTime.now());
        detailPouleDTO.setUpdateAt(ZonedDateTime.now());
        detailPouleDTO.setDeleted(false);
        detailPouleDTO.setButsPour(0);
        detailPouleDTO.setButsContre(0);
        detailPouleDTO.setVictoire(0);
        detailPouleDTO.setDefaite(0);
        detailPouleDTO.setMatchNull(0);
        detailPouleDTO.setNombreDePoint(0);
        detailPouleDTO.setGoalDifference(0);
        detailPouleDTO.setJournee(journee);

        return detailPouleDTO;
    }

    @Override
    public Optional<DetailPouleDTO> findByPouleAndAssociationAndJournee(PouleDTO pouleDTO, AssociationDTO associationDTO, int journee) {
        log.debug("Request to get DetailPoule by poule {} and association {} and journee {}", pouleDTO, associationDTO, journee);
        return detailPouleRepository.findAllByPouleAndAssociationAndJourneeAndDeletedIsFalse(
            pouleMapper.toEntity(pouleDTO),
            associationMapper.toEntity(associationDTO),
            journee).map(detailPouleMapper::toDto);
    }

    @Override
    public List<DetailPouleDTO> findByPouleInAndAssociation(List<Long> pouleIds, AssociationDTO associationDTO) {
        log.debug("Request to get DetailPoule by poules {} and association {}", pouleIds, associationDTO);
        return detailPouleRepository.findAllByPouleIdInAndAssociationAndDeletedIsFalse(
            pouleIds,
            associationMapper.toEntity(associationDTO)).stream().map(detailPouleMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public List<DetailPouleDTO> findByPouleAndJourneeGreaterThan(PouleDTO pouleDTO, int totalJournee) {
        log.debug("Request to get DetailPoule by poules {} and journee greater than {}", pouleDTO, totalJournee);
        return detailPouleRepository.findAllByPouleAndJourneeGreaterThanAndDeletedIsFalse(
            pouleMapper.toEntity(pouleDTO),
            totalJournee).stream().map(detailPouleMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public List<DetailPouleDTO> findAll() {
        return detailPouleRepository.findAllByDeletedIsFalse().stream()
            .map(detailPouleMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public void reIndex() {
        log.info("Request to reindex all DetailPoules");
        detailPouleSearchRepository.deleteAll();
        findAll().stream().map(detailPouleMapper::toEntity).forEach(detailPouleSearchRepository::save);
    }
}
