package org.nddngroup.navetfoot.core.service.mapper;


import org.nddngroup.navetfoot.core.domain.*;
import org.nddngroup.navetfoot.core.domain.Corner;
import org.nddngroup.navetfoot.core.service.dto.CornerDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Corner} and its DTO {@link CornerDTO}.
 */
@Mapper(componentModel = "spring", uses = {MatchMapper.class})
public interface CornerMapper extends EntityMapper<CornerDTO, Corner> {

    @Mapping(source = "match.id", target = "matchId")
    CornerDTO toDto(Corner corner);

    @Mapping(source = "matchId", target = "match")
    Corner toEntity(CornerDTO cornerDTO);

    default Corner fromId(Long id) {
        if (id == null) {
            return null;
        }
        Corner corner = new Corner();
        corner.setId(id);
        return corner;
    }
}
