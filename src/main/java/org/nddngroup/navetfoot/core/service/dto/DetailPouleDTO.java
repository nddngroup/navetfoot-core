package org.nddngroup.navetfoot.core.service.dto;

import org.nddngroup.navetfoot.core.domain.DetailPoule;

import java.time.ZonedDateTime;
import java.io.Serializable;

/**
 * A DTO for the {@link DetailPoule} entity.
 */
public class DetailPouleDTO implements Serializable {

    private Long id;

    private Integer nombreDePoint;

    private Integer goalDifference;

    private Integer journee;

    private String code;

    private String hashcode;

    private ZonedDateTime createdAt;

    private ZonedDateTime updateAt;

    private Boolean deleted;

    private Integer victoire;

    private Integer matchNull;

    private Integer defaite;

    private Integer butsPour;

    private Integer butsContre;


    private Long associationId;

    private Long pouleId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNombreDePoint() {
        return nombreDePoint;
    }

    public void setNombreDePoint(Integer nombreDePoint) {
        this.nombreDePoint = nombreDePoint;
    }

    public Integer getGoalDifference() {
        return goalDifference;
    }

    public void setGoalDifference(Integer goalDifference) {
        this.goalDifference = goalDifference;
    }

    public Integer getJournee() {
        return journee;
    }

    public void setJournee(Integer journee) {
        this.journee = journee;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getHashcode() {
        return hashcode;
    }

    public void setHashcode(String hashcode) {
        this.hashcode = hashcode;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(ZonedDateTime updateAt) {
        this.updateAt = updateAt;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Integer getVictoire() {
        return victoire;
    }

    public void setVictoire(Integer victoire) {
        this.victoire = victoire;
    }

    public Integer getMatchNull() {
        return matchNull;
    }

    public void setMatchNull(Integer matchNull) {
        this.matchNull = matchNull;
    }

    public Integer getDefaite() {
        return defaite;
    }

    public void setDefaite(Integer defaite) {
        this.defaite = defaite;
    }

    public Integer getButsPour() {
        return butsPour;
    }

    public void setButsPour(Integer butsPour) {
        this.butsPour = butsPour;
    }

    public Integer getButsContre() {
        return butsContre;
    }

    public void setButsContre(Integer butsContre) {
        this.butsContre = butsContre;
    }

    public Long getAssociationId() {
        return associationId;
    }

    public void setAssociationId(Long associationId) {
        this.associationId = associationId;
    }

    public Long getPouleId() {
        return pouleId;
    }

    public void setPouleId(Long pouleId) {
        this.pouleId = pouleId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DetailPouleDTO)) {
            return false;
        }

        return id != null && id.equals(((DetailPouleDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DetailPouleDTO{" +
            "id=" + getId() +
            ", nombreDePoint=" + getNombreDePoint() +
            ", goalDifference=" + getGoalDifference() +
            ", journee=" + getJournee() +
            ", code='" + getCode() + "'" +
            ", hashcode='" + getHashcode() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updateAt='" + getUpdateAt() + "'" +
            ", deleted='" + isDeleted() + "'" +
            ", victoire=" + getVictoire() +
            ", matchNull=" + getMatchNull() +
            ", defaite=" + getDefaite() +
            ", butsPour=" + getButsPour() +
            ", butsContre=" + getButsContre() +
            ", associationId=" + getAssociationId() +
            ", pouleId=" + getPouleId() +
            "}";
    }
}
