package org.nddngroup.navetfoot.core.service.impl;

import org.elasticsearch.index.query.QueryBuilder;
import org.nddngroup.navetfoot.core.domain.Competition;
import org.nddngroup.navetfoot.core.domain.custom.CompetitionWithSaisonsAndMatchs;
import org.nddngroup.navetfoot.core.domain.custom.stats.MatchDetail;
import org.nddngroup.navetfoot.core.exception.ApiRequestException;
import org.nddngroup.navetfoot.core.exception.ExceptionCode;
import org.nddngroup.navetfoot.core.exception.ExceptionLevel;
import org.nddngroup.navetfoot.core.repository.*;
import org.nddngroup.navetfoot.core.repository.search.CompetitionSearchRepository;
import org.nddngroup.navetfoot.core.service.CompetitionService;
import org.nddngroup.navetfoot.core.service.custom.CommunService;
import org.nddngroup.navetfoot.core.service.dto.*;
import org.nddngroup.navetfoot.core.service.external.ExternalApiService;
import org.nddngroup.navetfoot.core.service.mapper.*;
import org.nddngroup.navetfoot.core.repository.*;
import org.nddngroup.navetfoot.core.service.dto.CompetitionDTO;
import org.nddngroup.navetfoot.core.service.dto.CompetitionSaisonDTO;
import org.nddngroup.navetfoot.core.service.dto.OrganisationDTO;
import org.nddngroup.navetfoot.core.service.dto.SaisonDTO;
import org.nddngroup.navetfoot.core.service.mapper.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import java.time.LocalDate;

import static org.elasticsearch.index.query.QueryBuilders.boolQuery;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link Competition}.
 */
@Service
@Transactional
public class CompetitionServiceImpl implements CompetitionService {

    private final Logger log = LoggerFactory.getLogger(CompetitionServiceImpl.class);

    private final CompetitionRepository competitionRepository;

    private final CompetitionMapper competitionMapper;

    private final CompetitionSearchRepository competitionSearchRepository;
    private final ExternalApiService externalApiService;

    @Autowired
    private CommunService communService;
    @Autowired
    private OrganisationMapper organisationMapper;

    @Autowired
    private CompetitionSaisonRepository competitionSaisonRepository;
    @Autowired
    private SaisonRepository saisonRepository;
    @Autowired
    private OrganisationRepository organisationRepository;
    @Autowired
    private SaisonMapper saisonMapper;
    @Autowired
    private MatchMapper matchMapper;
    @Autowired
    private CompetitionSaisonMapper competitionSaisonMapper;
    @Autowired
    private MatchRepository matchRepository;
    @Autowired
    private EtapeCompetitionRepository etapeCompetitionRepository;

    public CompetitionServiceImpl(CompetitionRepository competitionRepository,
                                  CompetitionMapper competitionMapper,
                                  CompetitionSearchRepository competitionSearchRepository,
                                  ExternalApiService externalApiService
                                  ) {
        this.competitionRepository = competitionRepository;
        this.competitionMapper = competitionMapper;
        this.competitionSearchRepository = competitionSearchRepository;
        this.externalApiService = externalApiService;
    }

    @Override
    public CompetitionDTO save(CompetitionDTO competitionDTO) {
        log.debug("Request to save Competition : {}", competitionDTO);
        Competition competition = competitionMapper.toEntity(competitionDTO);
        competition = competitionRepository.save(competition);
        CompetitionDTO result = competitionMapper.toDto(competition);

        if (competitionDTO.isDeleted()) {
            this.competitionSearchRepository.deleteById(competitionDTO.getId());
        } else {
            competitionSearchRepository.save(competitionMapper.toEntity(result));
        }

        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<CompetitionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Competitions");
        return competitionRepository.findAll(pageable)
            .map(competitionMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<CompetitionDTO> findOne(Long id) {
        log.debug("Request to get Competition : {}", id);
        return competitionRepository.findById(id)
            .map(competitionMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Competition : {}", id);
        competitionRepository.deleteById(id);
        competitionSearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<CompetitionDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Competitions for query {}", query);
        return competitionSearchRepository.search(queryStringQuery(query), pageable)
            .map(competitionMapper::toDto);
    }

    @Override
    public Page<CompetitionDTO> findAllByOrganisation(OrganisationDTO organisationDTO, Pageable pageable) {
        log.debug("Request to get all Saisons by organisation");
        return competitionRepository.findAllByOrganisationAndDeletedIsFalse(pageable, organisationMapper.toEntity(organisationDTO))
            .map(competitionMapper::toDto);
    }

    @Override
    public List<CompetitionDTO> findAllByOrganisation(OrganisationDTO organisationDTO) {
        log.debug("Request to get all Saisons by organisation");
        return competitionRepository.findAllByOrganisationAndDeletedIsFalse(organisationMapper.toEntity(organisationDTO))
            .stream()
            .map(competitionMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public Page<CompetitionDTO> findByIdIn(List<Long> competitionIds, Pageable pageable) {
        log.debug("Request to get all Competitions by ids");
        return competitionRepository.findAllByIdInAndDeletedIsFalse(pageable, competitionIds)
            .map(competitionMapper::toDto);
    }

    @Override
    public List<CompetitionDTO> findByIdIn(List<Long> competitionIds) {
        log.debug("Request to get all Competitions by ids");
        return competitionRepository.findAllByIdInAndDeletedIsFalse(competitionIds)
            .stream()
            .map(competitionMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public Optional<CompetitionDTO> findByHashcode(String hashcode) {
        log.debug("Request to get Competition by hashcode: {}", hashcode);
        return competitionRepository.findByHashcodeAndDeletedIsFalse(hashcode)
            .map(competitionMapper::toDto);
    }

    @Override
    public List<CompetitionWithSaisonsAndMatchs> findAllCompetitionsWithSaisonsAndMatchs(String keyword, String selectedDate) {
        log.debug("Request to get all Competitions with saisons");

        List<Competition> competitions;

        if (keyword != null && !keyword.equals("")) {
            competitions = competitionRepository.findAllByNomContainingIgnoreCaseAndDeletedIsFalse(keyword);
        } else {
            competitions = competitionRepository.findAllByDeletedIsFalse();
        }

        List<CompetitionWithSaisonsAndMatchs> competitionWithSaisonsAndMatchsList = new ArrayList<>();
        List<CompetitionWithSaisonsAndMatchs> emptyCompetitions = new ArrayList<>();

            competitions.forEach(competition -> {
                CompetitionDTO competitionDTO = competitionMapper.toDto(competition);

                CompetitionWithSaisonsAndMatchs competitionWithSaisonsAndMatchs = new CompetitionWithSaisonsAndMatchs();
                competitionWithSaisonsAndMatchs.setCompetition(competitionDTO);

                // find all competitionSaison by competition
                List<CompetitionSaisonDTO> competitionSaisonDTOS = competitionSaisonRepository.findAllByCompetitionAndDeletedIsFalse(competition)
                    .stream().map(competitionSaisonMapper::toDto).collect(Collectors.toList());

                List<SaisonDTO> saisonDTOS = new ArrayList<>();
                List<MatchDetail> matchDetails = new ArrayList<>();

                for (CompetitionSaisonDTO competitionSaisonDTO : competitionSaisonDTOS) {
                    // find Saison by id
                    saisonRepository.findById(competitionSaisonDTO.getSaisonId())
                        .map(saisonMapper::toDto)
                        .ifPresent(saisonDTOS::add);

                    // find all matchs of this competition
                    etapeCompetitionRepository.findAllByCompetitionSaisonAndDeletedIsFalse(competitionSaisonMapper.toEntity(competitionSaisonDTO))
                        .forEach(etapeCompetition -> {
                            if (selectedDate == null) {
                                matchRepository.findAllByEtapeCompetitionAndDeletedIsFalseOrderByDateMatchAsc(etapeCompetition)
                                    .stream().map(matchMapper::toDto)
                                    .map(communService::getMatchDetail)
                                    .forEach(matchDetails::add);
                            } else {
                                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                                LocalDate localDate = LocalDate.parse(selectedDate, formatter);

                                matchRepository.findAllByEtapeCompetitionAndDateMatchEqualsAndDeletedIsFalseOrderByDateMatchAsc(etapeCompetition, localDate)
                                    .stream().map(matchMapper::toDto)
                                    .map(communService::getMatchDetail)
                                    .forEach(matchDetails::add);
                            }
                        });
                }

                matchDetails.sort(Comparator.reverseOrder());

                competitionWithSaisonsAndMatchs.setSaisons(saisonDTOS);
                competitionWithSaisonsAndMatchs.setMatchs(matchDetails);

                // find organisation
                organisationRepository.findById(competitionDTO.getOrganisationId())
                    .map(organisationMapper::toDto)
                    .ifPresent(competitionWithSaisonsAndMatchs::setOrganisation);

                if (matchDetails.isEmpty()) {
                    emptyCompetitions.add(competitionWithSaisonsAndMatchs);
                } else {
                    competitionWithSaisonsAndMatchsList.add(competitionWithSaisonsAndMatchs);
                }
            });

            if (!emptyCompetitions.isEmpty()) {
                competitionWithSaisonsAndMatchsList.addAll(emptyCompetitions);
            }

            var externalCompetitions = externalApiService.getExternalMatchs(selectedDate);

            // ajouter les leagues externe à la première page
            competitionWithSaisonsAndMatchsList.addAll(0, externalCompetitions);
            return competitionWithSaisonsAndMatchsList;
    }

    @Override
    public List<CompetitionDTO> save(OrganisationDTO organisationDTO, List<CompetitionDTO> competitionDTOS) {
        log.debug("Request to save list of competitions : {}", competitionDTOS);

        List<CompetitionDTO> resultList = new ArrayList<>();

        if (competitionDTOS != null) {
            for (CompetitionDTO competitionDTO : competitionDTOS) {
                /*if (competitionDTO.getId() != null) {
                    throw new BadRequestAlertException("A new competition cannot already have an ID", "saison", "idexists");
                }*/

                CompetitionDTO result = save(organisationDTO, competitionDTO);
                resultList.add(result);
            }
        }

        return resultList;
    }

    /**
     * Add new Competition
     * @param organisationDTO Organisation
     * @param competitionDTO Competition
     * @return CompetitionDTO
     */
    public CompetitionDTO save(OrganisationDTO organisationDTO, CompetitionDTO competitionDTO) {
        log.debug("Request to save competition : {} to organisation {}", competitionDTO, organisationDTO);

        // vérifier si la compétition n'est dupliquée
        if (findByOrganisationAndNom(organisationDTO, competitionDTO.getNom()).isPresent()) {
            throw new ApiRequestException(
                ExceptionCode.COMPETITION_DUPLICATED_FOR_ORGANISATION.getMessage(),
                ExceptionCode.COMPETITION_DUPLICATED_FOR_ORGANISATION.getValue(),
                ExceptionLevel.WARNING
            );
        }

        if (organisationDTO != null) {
            String toHash = organisationDTO.getId().toString() + ZonedDateTime.now().toString();
            competitionDTO.setHashcode(communService.toHash(toHash));

            competitionDTO.setOrganisationId(organisationDTO.getId());
            competitionDTO.setCode("REF_" + organisationDTO.getId().toString());

            competitionDTO.setCreatedAt(ZonedDateTime.now());
            competitionDTO.setUpdatedAt(ZonedDateTime.now());
            competitionDTO.setDeleted(false);

            /*CompetitionDTO result = save(competitionDTO);
            result.setCode(competitionDTO.getCode() + "_" + result.getId().toString());
            result.setHashcode(communService.toHash(result.getId().toString()));*/

            return save(competitionDTO);
        } else {
            throw new ApiRequestException(
                ExceptionCode.ORGANISATION_NOT_FOUND.getMessage(),
                ExceptionCode.ORGANISATION_NOT_FOUND.getValue(),
                ExceptionLevel.ERROR
            );
        }
    }

    @Override
    public Optional<CompetitionDTO> findByOrganisationAndNom(OrganisationDTO organisationDTO, String nom) {
        log.debug("Request to get competition by organisation: {} and nom: {}", organisationDTO, nom);

        return competitionRepository.findOneByOrganisationAndNomAndDeletedIsFalse(organisationMapper.toEntity(organisationDTO), nom)
            .map(competitionMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<CompetitionDTO> search(OrganisationDTO organisationDTO, String query, Pageable pageable) {
        log.debug("Request to search for a page of Competitions for query {} and organisation {}", query, organisationDTO);

        QueryBuilder queryBuilder = boolQuery()
            .must(
                    queryStringQuery(organisationDTO.getId().toString()).analyzeWildcard(true).field("organisation.id")
            )
            .must(
                queryStringQuery("false").analyzeWildcard(true).field("deleted")
            )
            .must(queryStringQuery(query));

        return competitionSearchRepository.search(queryBuilder, pageable)
            .map(competitionMapper::toDto);
    }

    @Override
    public List<CompetitionDTO> findAll() {
        return competitionRepository.findAllByDeletedIsFalse().stream()
            .map(competitionMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public void reIndex() {
        log.info("Request to reindex all Competitions");
        competitionSearchRepository.deleteAll();
        findAll().stream().map(competitionMapper::toEntity).forEach(competitionSearchRepository::save);
    }
}
