package org.nddngroup.navetfoot.core.service.mapper.custom;

import org.nddngroup.navetfoot.core.domain.Match;
import org.nddngroup.navetfoot.core.service.*;
import org.nddngroup.navetfoot.core.service.*;
import org.nddngroup.navetfoot.core.service.dto.MatchDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring")
public abstract class MatchStatsMapper {
    @Autowired
    protected EtapeCompetitionService etapeCompetitionService;
    @Autowired
    protected CompetitionSaisonService competitionSaisonService;
    @Autowired
    protected SaisonService saisonService;
    @Autowired
    protected CompetitionService competitionService;
    @Autowired
    protected OrganisationService organisationService;
    @Autowired
    protected AssociationService associationService;

    @Named("toStats")
    public Match toStats(MatchDTO match) {
        var matchStats = new Match()
            .code(match.getCode())
            .hashcode(match.getHashcode())
            .journee(match.getJournee())
            .etatMatch(match.getEtatMatch())
            .dateMatch(match.getDateMatch())
            .coupDEnvoi(match.getCoupDEnvoi())
            .chrono(match.getChrono())
            .createdAt(match.getCreatedAt())
            .updatedAt(match.getUpdatedAt())
            .deleted(match.isDeleted());

        matchStats.setId(match.getId());

        // find local
        associationService.findOne(match.getLocalId())
            .ifPresent(associationDTO -> {
                matchStats.setLocalId(associationDTO.getId());
                matchStats.setLocalNom(associationDTO.getNom());
                matchStats.setLocalImageUrl(associationDTO.getCouleurs());
            });
        // find visiteur
        associationService.findOne(match.getVisiteurId())
            .ifPresent(associationDTO -> {
                matchStats.setVisiteurId(associationDTO.getId());
                matchStats.setVisiteurNom(associationDTO.getNom());
                matchStats.setVisiteurImageUrl(associationDTO.getCouleurs());
            });
        // find etape by id
        etapeCompetitionService.findOne(match.getEtapeCompetitionId())
            .ifPresent(etapeCompetitionDTO -> {
                matchStats.setEtapeCompetitionId(etapeCompetitionDTO.getId());
                matchStats.setEtapeCompetitionNom(etapeCompetitionDTO.getNom());

                // find competition and saison
                competitionSaisonService.findOne(etapeCompetitionDTO.getCompetitionSaisonId())
                    .ifPresent(competitionSaisonDTO -> {
                        // set Saison
                        matchStats.setSaisonId(competitionSaisonDTO.getCompetitionId());
                        matchStats.setSaisonLibelle(competitionSaisonDTO.getSaisonLibelle());
                        // find competition
                        competitionService.findOne(competitionSaisonDTO.getCompetitionId())
                            .ifPresent(competitionDTO -> {
                                matchStats.setCompetitionId(competitionDTO.getId());
                                matchStats.setCompetitionNom(competitionDTO.getNom());

                                matchStats.setOrganisationId(competitionDTO.getOrganisationId());
                                matchStats.setOrganisationNom(competitionDTO.getOrganisationNom());
                            });
                    });
            });

        return matchStats;
    }

    @Named("toDto")
    public MatchDTO toDto(Match match) {
        var matchDto = new MatchDTO();

        matchDto.setId(match.getId());
        matchDto.setCode(match.getCode());
        matchDto.setHashcode(match.getHashcode());
        matchDto.setJournee(match.getJournee());
        matchDto.setEtatMatch(match.getEtatMatch());
        matchDto.setDateMatch(match.getDateMatch());
        matchDto.setCoupDEnvoi(match.getCoupDEnvoi());
        matchDto.setChrono(match.getChrono());
        matchDto.setCreatedAt(match.getCreatedAt());
        matchDto.setUpdatedAt(match.getUpdatedAt());
        matchDto.setDeleted(match.isDeleted());
        matchDto.setEtapeCompetitionId(match.getEtapeCompetitionId());
        matchDto.setEtapeCompetitionNom(match.getEtapeCompetitionNom());
        matchDto.setLocalId(match.getLocalId());
        matchDto.setVisiteurId(match.getVisiteurId());

        return matchDto;
    }
}
