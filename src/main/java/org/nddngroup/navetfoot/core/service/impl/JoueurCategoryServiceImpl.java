package org.nddngroup.navetfoot.core.service.impl;

import org.nddngroup.navetfoot.core.service.JoueurCategoryService;
import org.nddngroup.navetfoot.core.domain.JoueurCategory;
import org.nddngroup.navetfoot.core.repository.JoueurCategoryRepository;
import org.nddngroup.navetfoot.core.repository.search.JoueurCategorySearchRepository;
import org.nddngroup.navetfoot.core.service.dto.JoueurCategoryDTO;
import org.nddngroup.navetfoot.core.service.dto.JoueurDTO;
import org.nddngroup.navetfoot.core.service.mapper.JoueurCategoryMapper;
import org.nddngroup.navetfoot.core.service.mapper.JoueurMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link JoueurCategory}.
 */
@Service
@Transactional
public class JoueurCategoryServiceImpl implements JoueurCategoryService {

    private final Logger log = LoggerFactory.getLogger(JoueurCategoryServiceImpl.class);

    private final JoueurCategoryRepository joueurCategoryRepository;

    private final JoueurCategoryMapper joueurCategoryMapper;

    private final JoueurCategorySearchRepository joueurCategorySearchRepository;
    @Autowired
    private JoueurMapper joueurMapper;

    public JoueurCategoryServiceImpl(JoueurCategoryRepository joueurCategoryRepository, JoueurCategoryMapper joueurCategoryMapper, JoueurCategorySearchRepository joueurCategorySearchRepository) {
        this.joueurCategoryRepository = joueurCategoryRepository;
        this.joueurCategoryMapper = joueurCategoryMapper;
        this.joueurCategorySearchRepository = joueurCategorySearchRepository;
    }

    @Override
    public JoueurCategoryDTO save(JoueurCategoryDTO joueurCategoryDTO) {
        log.debug("Request to save JoueurCategory : {}", joueurCategoryDTO);
        JoueurCategory joueurCategory = joueurCategoryMapper.toEntity(joueurCategoryDTO);
        joueurCategory = joueurCategoryRepository.save(joueurCategory);
        JoueurCategoryDTO result = joueurCategoryMapper.toDto(joueurCategory);

        if (joueurCategoryDTO.isDeleted()) {
            joueurCategorySearchRepository.deleteById(joueurCategoryDTO.getId());
        } else {
            joueurCategorySearchRepository.save(joueurCategoryMapper.toEntity(result));
        }

        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<JoueurCategoryDTO> findAll(Pageable pageable) {
        log.debug("Request to get all JoueurCategories");
        return joueurCategoryRepository.findAll(pageable)
            .map(joueurCategoryMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<JoueurCategoryDTO> findOne(Long id) {
        log.debug("Request to get JoueurCategory : {}", id);
        return joueurCategoryRepository.findById(id)
            .map(joueurCategoryMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete JoueurCategory : {}", id);
        joueurCategoryRepository.deleteById(id);
        joueurCategorySearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<JoueurCategoryDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of JoueurCategories for query {}", query);
        return joueurCategorySearchRepository.search(queryStringQuery(query), pageable)
            .map(joueurCategoryMapper::toDto);
    }

    @Override
    public Optional<JoueurCategoryDTO> findByJoueur(JoueurDTO joueurDTO) {
        log.debug("Request to get all JoueurCategories by joueur");
        return joueurCategoryRepository.findByJoueurAndDeletedIsFalse(joueurMapper.toEntity(joueurDTO))
            .map(joueurCategoryMapper::toDto)       ;
    }

    @Override
    public List<JoueurCategoryDTO> findAll() {
        return joueurCategoryRepository.findAllByDeletedIsFalse().stream()
            .map(joueurCategoryMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public void reIndex() {
        log.info("Request to reindex all JoueurCategories");
        joueurCategorySearchRepository.deleteAll();
        findAll().stream().map(joueurCategoryMapper::toEntity).forEach(joueurCategorySearchRepository::save);
    }
}
