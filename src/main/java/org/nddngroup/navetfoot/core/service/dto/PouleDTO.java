package org.nddngroup.navetfoot.core.service.dto;

import org.nddngroup.navetfoot.core.domain.Poule;

import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link Poule} entity.
 */
public class PouleDTO implements Serializable {

    private Long id;

    @NotNull
    private String libelle;

    @NotNull
    private Integer nombreEquipe;

    private String code;

    private String hashcode;

    private ZonedDateTime createdAt;

    private ZonedDateTime updatedAt;

    private Boolean deleted;

    private Integer nombreJournee;


    private Long etapeCompetitionId;

    private String etapeCompetitionNom;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Integer getNombreEquipe() {
        return nombreEquipe;
    }

    public void setNombreEquipe(Integer nombreEquipe) {
        this.nombreEquipe = nombreEquipe;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getHashcode() {
        return hashcode;
    }

    public void setHashcode(String hashcode) {
        this.hashcode = hashcode;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Integer getNombreJournee() {
        return nombreJournee;
    }

    public void setNombreJournee(Integer nombreJournee) {
        this.nombreJournee = nombreJournee;
    }

    public Long getEtapeCompetitionId() {
        return etapeCompetitionId;
    }

    public void setEtapeCompetitionId(Long etapeCompetitionId) {
        this.etapeCompetitionId = etapeCompetitionId;
    }

    public String getEtapeCompetitionNom() {
        return etapeCompetitionNom;
    }

    public void setEtapeCompetitionNom(String etapeCompetitionNom) {
        this.etapeCompetitionNom = etapeCompetitionNom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PouleDTO)) {
            return false;
        }

        return id != null && id.equals(((PouleDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PouleDTO{" +
            "id=" + getId() +
            ", libelle='" + getLibelle() + "'" +
            ", nombreEquipe=" + getNombreEquipe() +
            ", code='" + getCode() + "'" +
            ", hashcode='" + getHashcode() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", deleted='" + isDeleted() + "'" +
            ", nombreJournee=" + getNombreJournee() +
            ", etapeCompetitionId=" + getEtapeCompetitionId() +
            ", etapeCompetitionNom='" + getEtapeCompetitionNom() + "'" +
            "}";
    }
}
