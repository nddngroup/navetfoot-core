package org.nddngroup.navetfoot.core.service.mapper;


import org.nddngroup.navetfoot.core.domain.*;
import org.nddngroup.navetfoot.core.domain.Participation;
import org.nddngroup.navetfoot.core.service.dto.ParticipationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Participation} and its DTO {@link ParticipationDTO}.
 */
@Mapper(componentModel = "spring", uses = {SaisonMapper.class, AssociationMapper.class, EtapeCompetitionMapper.class})
public interface ParticipationMapper extends EntityMapper<ParticipationDTO, Participation> {

    @Mapping(source = "saison.id", target = "saisonId")
    @Mapping(source = "saison.libelle", target = "saisonLibelle")
    @Mapping(source = "association.id", target = "associationId")
    @Mapping(source = "association.nom", target = "associationNom")
    @Mapping(source = "etapeCompetition.id", target = "etapeCompetitionId")
    @Mapping(source = "etapeCompetition.nom", target = "etapeCompetitionNom")
    ParticipationDTO toDto(Participation participation);

    @Mapping(source = "saisonId", target = "saison")
    @Mapping(source = "associationId", target = "association")
    @Mapping(source = "etapeCompetitionId", target = "etapeCompetition")
    Participation toEntity(ParticipationDTO participationDTO);

    default Participation fromId(Long id) {
        if (id == null) {
            return null;
        }
        Participation participation = new Participation();
        participation.setId(id);
        return participation;
    }
}
