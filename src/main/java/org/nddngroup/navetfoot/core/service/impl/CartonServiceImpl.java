package org.nddngroup.navetfoot.core.service.impl;

import org.nddngroup.navetfoot.core.domain.Carton;
import org.nddngroup.navetfoot.core.domain.enumeration.Couleur;
import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;
import org.nddngroup.navetfoot.core.repository.CartonRepository;
import org.nddngroup.navetfoot.core.repository.search.CartonSearchRepository;
import org.nddngroup.navetfoot.core.service.CartonService;
import org.nddngroup.navetfoot.core.service.dto.CartonDTO;
import org.nddngroup.navetfoot.core.service.dto.JoueurDTO;
import org.nddngroup.navetfoot.core.service.dto.MatchDTO;
import org.nddngroup.navetfoot.core.service.mapper.CartonMapper;
import org.nddngroup.navetfoot.core.service.mapper.JoueurMapper;
import org.nddngroup.navetfoot.core.service.mapper.MatchMapper;
import org.nddngroup.navetfoot.core.service.mapper.custom.CartonStatsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link Carton}.
 */
@Service
@Transactional
public class CartonServiceImpl implements CartonService {

    private final Logger log = LoggerFactory.getLogger(CartonServiceImpl.class);

    private final CartonRepository cartonRepository;

    private final CartonMapper cartonMapper;

    private final CartonSearchRepository cartonSearchRepository;
    @Autowired
    private CartonStatsMapper cartonStatsMapper;
    @Autowired
    private MatchMapper matchMapper;
    @Autowired
    private JoueurMapper joueurMapper;

    public CartonServiceImpl(CartonRepository cartonRepository, CartonMapper cartonMapper, CartonSearchRepository cartonSearchRepository) {
        this.cartonRepository = cartonRepository;
        this.cartonMapper = cartonMapper;
        this.cartonSearchRepository = cartonSearchRepository;
    }

    @Override
    public CartonDTO save(CartonDTO cartonDTO) {
        log.debug("Request to save Carton : {}", cartonDTO);


        cartonDTO.setUpdatedAt(ZonedDateTime.now());
        if (cartonDTO.getCreatedAt() == null) {
            cartonDTO.setCreatedAt(ZonedDateTime.now());
        }


        var carton = cartonMapper.toEntity(cartonDTO);
        carton = cartonRepository.save(carton);
        CartonDTO result = cartonMapper.toDto(carton);

        if (Boolean.TRUE.equals(cartonDTO.isDeleted())) {
            cartonSearchRepository.deleteById(result.getId());
        } else {
            cartonSearchRepository.save(cartonStatsMapper.toStats(result));
        }

        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<CartonDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Cartons");
        return cartonRepository.findAll(pageable)
            .map(cartonMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<CartonDTO> findOne(Long id) {
        log.debug("Request to get Carton : {}", id);
        return cartonRepository.findById(id)
            .map(cartonMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Carton : {}", id);
        cartonRepository.deleteById(id);
        cartonSearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<CartonDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Cartons for query {}", query);
        return cartonSearchRepository.search(queryStringQuery(query), pageable)
            .map(cartonMapper::toDto);
    }

    @Override
    public List<CartonDTO> findByMatch(MatchDTO matchDTO) {
        log.debug("Request to get all Cartons by match");
        return cartonRepository.findAllByMatchAndDeletedIsFalse(matchMapper.toEntity(matchDTO))
            .stream()
            .map(cartonMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public List<CartonDTO> findByMatchAndJoueurAndCouleur(MatchDTO matchDTO, JoueurDTO joueurDTO, Couleur couleur) {
        log.debug("Request to get all Cartons by match and joueur and couleur");
        return cartonRepository.findAllByMatchAndJoueurAndCouleurAndDeletedIsFalse(matchMapper.toEntity(matchDTO), joueurMapper.toEntity(joueurDTO), couleur)
            .stream()
            .map(cartonMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public List<CartonDTO> findByMatchAndCouleur(MatchDTO matchDTO, Couleur couleur) {
        log.debug("Request to get all Cartons by match and couleur");
        return cartonRepository.findAllByMatchAndCouleurAndDeletedIsFalse(matchMapper.toEntity(matchDTO), couleur)
            .stream()
            .map(cartonMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public List<CartonDTO> findByMatchAndEquipe(MatchDTO matchDTO, Equipe equipe) {
        log.debug("Request to get all Cartons by match and equipe");
        return cartonRepository.findAllByMatchAndEquipeAndDeletedIsFalse(matchMapper.toEntity(matchDTO), equipe)
            .stream()
            .map(cartonMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public List<CartonDTO> findByMatchAndEquipeAndCouleur(MatchDTO matchDTO, Equipe equipe, Couleur couleur) {
        log.debug("Request to get all Cartons by match and equipe and couleur");
        return cartonRepository.findAllByMatchAndEquipeAndCouleurAndDeletedIsFalse(matchMapper.toEntity(matchDTO), equipe, couleur)
            .stream()
            .map(cartonMapper::toDto).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<CartonDTO> findByHashcode(String hashcode) {
        log.debug("Request to get Carton : {}", hashcode);
        return cartonRepository.findByHashcodeAndDeletedIsFalse(hashcode)
            .map(cartonMapper::toDto);
    }

    @Override
    public List<CartonDTO> findAll() {
        return cartonRepository.findAllByDeletedIsFalse().stream()
            .map(cartonMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public void reIndex() {
        log.info("Request to reindex all Cartons");
        cartonSearchRepository.deleteAll();
        findAll().stream().map(cartonStatsMapper::toStats).forEach(cartonSearchRepository::save);
    }
}
