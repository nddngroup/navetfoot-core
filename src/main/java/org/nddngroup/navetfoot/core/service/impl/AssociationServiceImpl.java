package org.nddngroup.navetfoot.core.service.impl;

import org.elasticsearch.index.query.IdsQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.nddngroup.navetfoot.core.domain.Association;
import org.nddngroup.navetfoot.core.repository.AssociationRepository;
import org.nddngroup.navetfoot.core.repository.search.AffiliationSearchRepository;
import org.nddngroup.navetfoot.core.repository.search.AssociationSearchRepository;
import org.nddngroup.navetfoot.core.service.AffiliationService;
import org.nddngroup.navetfoot.core.service.AssociationService;
import org.nddngroup.navetfoot.core.service.custom.CommunService;
import org.nddngroup.navetfoot.core.service.dto.AssociationDTO;
import org.nddngroup.navetfoot.core.service.dto.OrganisationDTO;
import org.nddngroup.navetfoot.core.service.mapper.AssociationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link Association}.
 */
@Service
@Transactional
public class AssociationServiceImpl implements AssociationService {

    private final Logger log = LoggerFactory.getLogger(AssociationServiceImpl.class);

    private final AssociationRepository associationRepository;

    private final AssociationMapper associationMapper;

    private final AssociationSearchRepository associationSearchRepository;

    @Autowired
    private CommunService communService;
    @Autowired
    private AffiliationService affiliationService;
    @Autowired
    private AffiliationSearchRepository affiliationSearchRepository;

    public AssociationServiceImpl(AssociationRepository associationRepository,
                                  AssociationMapper associationMapper,
                                  AssociationSearchRepository associationSearchRepository) {
        this.associationRepository = associationRepository;
        this.associationMapper = associationMapper;
        this.associationSearchRepository = associationSearchRepository;
    }

    @Override
    public AssociationDTO save(AssociationDTO associationDTO) {
        log.debug("Request to save Association : {}", associationDTO);
        Association association = associationMapper.toEntity(associationDTO);
        association = associationRepository.save(association);
        AssociationDTO result = associationMapper.toDto(association);

        if (Boolean.TRUE.equals(associationDTO.isDeleted())) {
            associationSearchRepository.deleteById(associationDTO.getId());
        } else {
            associationSearchRepository.save(associationMapper.toEntity(result));
        }

        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<AssociationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Associations");
        return associationRepository.findAll(pageable)
            .map(associationMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<AssociationDTO> findAll() {
        log.debug("Request to get all Associations");
        return associationRepository.findAllByDeletedIsFalse().stream()
            .map(associationMapper::toDto).collect(Collectors.toList());
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<AssociationDTO> findOne(Long id) {
        log.debug("Request to get Association : {}", id);
        return associationRepository.findById(id)
            .map(associationMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Association : {}", id);
        associationRepository.deleteById(id);
        associationSearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<AssociationDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Associations for query {}", query);
        return associationSearchRepository.search(queryStringQuery(query), pageable)
            .map(associationMapper::toDto);
    }

    @Override
    public Page<AssociationDTO> findByIdIn(List<Long> ids, Pageable pageable) {
        log.debug("Request to get all Associations");
        return associationRepository.findAllByIdInAndDeletedIsFalse(pageable, ids)
            .map(associationMapper::toDto);
    }

    @Override
    public List<AssociationDTO> findByIdIn(List<Long> ids) {
        log.debug("Request to get all Associations");
        return associationRepository.findAllByIdInAndDeletedIsFalse(ids).stream()
            .map(associationMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public List<AssociationDTO> findByNom(String nom) {
        log.debug("Request to get all Associations by nom");
        return associationRepository.findAllByNomAndDeletedIsFalse(nom)
            .stream()
            .map(associationMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public Page<AssociationDTO> findByNomContains(String nom, Pageable pageable) {
        log.debug("Request to get all Associations by nom");
        return associationRepository.findAllByNomContainingIgnoreCaseAndDeletedIsFalse(nom, pageable)
            .map(associationMapper::toDto);
    }

    @Override
    public Optional<AssociationDTO> findByHashcode(String hashcode) {
        log.debug("Request to get association by hashcode: {}", hashcode);
        return associationRepository.findByHashcodeAndDeletedIsFalse(hashcode)
            .map(associationMapper::toDto);
    }

    @Override
    public List<AssociationDTO> save(OrganisationDTO organisationDTO, List<AssociationDTO> associationDTOs) {
        log.debug("Request to save Associations : {}", associationDTOs);

        List<AssociationDTO> toReturn = new ArrayList<>();

        if (associationDTOs != null && associationDTOs.size() != 0) {
            for(AssociationDTO associationDTO : associationDTOs) {
                communService.checkAssociation(organisationDTO, associationDTO);

                associationDTO.setCreatedAt(ZonedDateTime.now());
                associationDTO.setUpdatedAt(ZonedDateTime.now());
                associationDTO.setDeleted(false);
                associationDTO.setCode("REF_" + organisationDTO.getId().toString());
                String toHash = organisationDTO.getId().toString() + ZonedDateTime.now().toString();
                associationDTO.setHashcode(this.communService.toHash(toHash));

                AssociationDTO result = save(associationDTO);

                if (result != null) {
                    result.setCode(associationDTO.getCode() + "_" + result.getId().toString());
                    result = save(result);
                }

                // add affiliation
                affiliationService.save(organisationDTO, result);

                toReturn.add(result);
            }
        }

        return toReturn;
    }

    @Override
    public AssociationDTO save(AssociationDTO associationDTO, OrganisationDTO organisationDTO) {
        log.debug("Request to add Associations {} with organisation : {}", associationDTO, organisationDTO);

        associationDTO.setCode("REF_" + organisationDTO.getId().toString());
        String toHash = organisationDTO.getId().toString() + ZonedDateTime.now();
        associationDTO.setHashcode(this.communService.toHash(toHash));
        associationDTO.setCreatedAt(ZonedDateTime.now());
        associationDTO.setUpdatedAt(ZonedDateTime.now());
        associationDTO.setDeleted(false);

        associationDTO = save(associationDTO);

        // add affiliation
        affiliationService.save(organisationDTO, associationDTO);

        return associationDTO;
    }

    @Override
    public Optional<AssociationDTO> findByNomAndAdresse(String nom, String adresse) {
        log.debug("Request to get association by nom: {} and adresse: {}", nom, adresse);
        return associationRepository.findByNomAndAdresseAndDeletedIsFalse(nom, adresse)
            .map(associationMapper::toDto);
    }

    @Override
    public Page<AssociationDTO> search(OrganisationDTO organisationDTO, String query, Pageable pageable) {
        log.debug("Request to search for a page of Associations for query {}", query);

        QueryBuilder queryBuilder = communService.organisationQueryBuilder(organisationDTO.getId(), "organisation.id");

        IdsQueryBuilder idsQueryBuilder = new IdsQueryBuilder();

        affiliationSearchRepository.search(queryBuilder, Pageable.unpaged())
            .forEach(affiliation -> idsQueryBuilder.addIds(affiliation.getAssociation().getId().toString()));

        log.debug("$$$$$$ AFFILIATION IDS : {}", idsQueryBuilder.ids());

        queryBuilder = boolQuery()
            .must(
                queryStringQuery("false").analyzeWildcard(true).field("deleted")
            )
            .must(queryStringQuery(query))
            .must(idsQueryBuilder);

        return associationSearchRepository.search(queryBuilder, pageable)
            .map(associationMapper::toDto);
    }

    @Override
    public void reIndex() {
        log.info("Request to reindex Associations");
        associationSearchRepository.deleteAll();
        findAll().stream().map(associationMapper::toEntity).forEach(associationSearchRepository::save);
    }
}
