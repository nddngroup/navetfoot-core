package org.nddngroup.navetfoot.core.service.mapper;


import org.nddngroup.navetfoot.core.domain.*;
import org.nddngroup.navetfoot.core.domain.Association;
import org.nddngroup.navetfoot.core.service.dto.AssociationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Association} and its DTO {@link AssociationDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AssociationMapper extends EntityMapper<AssociationDTO, Association> {


    @Mapping(target = "affiliations", ignore = true)
    @Mapping(target = "removeAffiliation", ignore = true)
    @Mapping(target = "organisations", ignore = true)
    @Mapping(target = "removeOrganisations", ignore = true)
    Association toEntity(AssociationDTO associationDTO);

    default Association fromId(Long id) {
        if (id == null) {
            return null;
        }
        Association association = new Association();
        association.setId(id);
        return association;
    }
}
