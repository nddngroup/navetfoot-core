package org.nddngroup.navetfoot.core.service.mapper;


import org.nddngroup.navetfoot.core.domain.*;
import org.nddngroup.navetfoot.core.domain.CompetitionSaisonCategory;
import org.nddngroup.navetfoot.core.service.dto.CompetitionSaisonCategoryDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link CompetitionSaisonCategory} and its DTO {@link CompetitionSaisonCategoryDTO}.
 */
@Mapper(componentModel = "spring", uses = {CompetitionSaisonMapper.class, CategoryMapper.class})
public interface CompetitionSaisonCategoryMapper extends EntityMapper<CompetitionSaisonCategoryDTO, CompetitionSaisonCategory> {

    @Mapping(source = "competitionSaison.id", target = "competitionSaisonId")
    @Mapping(source = "category.id", target = "categoryId")
    CompetitionSaisonCategoryDTO toDto(CompetitionSaisonCategory competitionSaisonCategory);

    @Mapping(source = "competitionSaisonId", target = "competitionSaison")
    @Mapping(source = "categoryId", target = "category")
    CompetitionSaisonCategory toEntity(CompetitionSaisonCategoryDTO competitionSaisonCategoryDTO);

    default CompetitionSaisonCategory fromId(Long id) {
        if (id == null) {
            return null;
        }
        CompetitionSaisonCategory competitionSaisonCategory = new CompetitionSaisonCategory();
        competitionSaisonCategory.setId(id);
        return competitionSaisonCategory;
    }
}
