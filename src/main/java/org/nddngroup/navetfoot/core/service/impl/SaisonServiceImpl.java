package org.nddngroup.navetfoot.core.service.impl;

import org.elasticsearch.index.query.QueryBuilder;
import org.nddngroup.navetfoot.core.config.Constants;
import org.nddngroup.navetfoot.core.domain.Saison;
import org.nddngroup.navetfoot.core.exception.ApiRequestException;
import org.nddngroup.navetfoot.core.exception.ExceptionCode;
import org.nddngroup.navetfoot.core.exception.ExceptionLevel;
import org.nddngroup.navetfoot.core.repository.SaisonRepository;
import org.nddngroup.navetfoot.core.repository.search.SaisonSearchRepository;
import org.nddngroup.navetfoot.core.service.SaisonService;
import org.nddngroup.navetfoot.core.service.custom.CommunService;
import org.nddngroup.navetfoot.core.service.dto.OrganisationDTO;
import org.nddngroup.navetfoot.core.service.dto.SaisonDTO;
import org.nddngroup.navetfoot.core.service.mapper.OrganisationMapper;
import org.nddngroup.navetfoot.core.service.mapper.SaisonMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.boolQuery;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link Saison}.
 */
@Service
@Transactional
public class SaisonServiceImpl implements SaisonService {

    private final Logger log = LoggerFactory.getLogger(SaisonServiceImpl.class);

    private final SaisonRepository saisonRepository;

    private final SaisonMapper saisonMapper;

    private final SaisonSearchRepository saisonSearchRepository;
    @Autowired
    private CommunService communService;
    @Autowired
    private OrganisationMapper organisationMapper;

    public SaisonServiceImpl(SaisonRepository saisonRepository,
                             SaisonMapper saisonMapper,
                             SaisonSearchRepository saisonSearchRepository) {
        this.saisonRepository = saisonRepository;
        this.saisonMapper = saisonMapper;
        this.saisonSearchRepository = saisonSearchRepository;
    }

    @Override
    public SaisonDTO save(SaisonDTO saisonDTO) {
        log.debug("Request to save Saison : {}", saisonDTO);
        Saison saison = saisonMapper.toEntity(saisonDTO);
        saison = saisonRepository.save(saison);
        SaisonDTO result = saisonMapper.toDto(saison);

        if (saisonDTO.isDeleted()) {
            saisonSearchRepository.deleteById(saisonDTO.getId());
        } else {
            saisonSearchRepository.save(saisonMapper.toEntity(result));
        }

        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<SaisonDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Saisons");
        return saisonRepository.findAll(pageable)
            .map(saisonMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<SaisonDTO> findOne(Long id) {
        log.debug("Request to get Saison : {}", id);
        return saisonRepository.findById(id)
            .map(saisonMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Saison : {}", id);
        saisonRepository.deleteById(id);
        saisonSearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<SaisonDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Saisons for query {}", query);
        return saisonSearchRepository.search(queryStringQuery(query), pageable)
            .map(saisonMapper::toDto);
    }

    @Override
    public Page<SaisonDTO> findAllByOrganisation(OrganisationDTO organisationDTO, Pageable pageable) {
        log.debug("Request to get all Saisons by organisation");
        return saisonRepository.findAllByOrganisationAndDeletedIsFalse(pageable, organisationMapper.toEntity(organisationDTO))
            .map(saisonMapper::toDto);
    }

    @Override
    public List<SaisonDTO> findAllByOrganisation(OrganisationDTO organisationDTO) {
        log.debug("Request to get all Saisons by organisation");
        return saisonRepository.findAllByOrganisationAndDeletedIsFalse(organisationMapper.toEntity(organisationDTO))
            .stream()
            .map(saisonMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public List<SaisonDTO> findPreviousByOrganisation(SaisonDTO saisonDTO, OrganisationDTO organisationDTO) {
        log.debug("Request to get all previous Saisons by organisation");
        return saisonRepository.findAllByOrganisationAndCreatedAtBeforeAndDeletedIsFalse(organisationMapper.toEntity(organisationDTO), saisonDTO.getCreatedAt())
            .stream()
            .map(saisonMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public Optional<SaisonDTO> findByHashcode(String hashcode) {
        log.debug("Request to get Saison by hashcode: {}", hashcode);
        return saisonRepository.findByHashcodeAndDeletedIsFalse(hashcode)
            .map(saisonMapper::toDto);
    }

    @Override
    public List<SaisonDTO> save(OrganisationDTO organisationDTO, List<SaisonDTO> saisonDTOS) {
        log.debug("Request to save list of saisons : {}", saisonDTOS);

        List<SaisonDTO> resultList = new ArrayList<>();

        if (saisonDTOS != null) {
            for (SaisonDTO saisonDTO : saisonDTOS) {
                // check libelle is valid
               if (!saisonDTO.getLibelle().matches(Constants.SAISON_LIBELLE_REGEX)) {
                    throw new ApiRequestException(
                        ExceptionCode.SAISON_LIBELLE_NOT_VALID.getMessage(),
                        ExceptionCode.SAISON_LIBELLE_NOT_VALID.getValue(),
                        ExceptionLevel.WARNING
                    );
                }

                SaisonDTO result = save(organisationDTO, saisonDTO);
                resultList.add(result);
            }
        }

        return resultList;
    }


    /**
     * Add new Saison
     * @param organisationDTO Organisation
     * @param saisonDTO Saison
     * @return SaisonDTO
     */
    @Override
    public SaisonDTO save(OrganisationDTO organisationDTO, SaisonDTO saisonDTO) {
        log.debug("Request to save saison : {} to organisation {}", saisonDTO, organisationDTO);

        // vérifier si la saison n'est dupliquée
        if (findByOrganisationAndLibelle(organisationDTO, saisonDTO.getLibelle()).isPresent()) {
            throw new ApiRequestException(
                ExceptionCode.SAISON_DUPLICATED_FOR_ORGANISATION.getMessage(),
                ExceptionCode.SAISON_DUPLICATED_FOR_ORGANISATION.getValue(),
                ExceptionLevel.WARNING
            );
        }

        if (organisationDTO != null) {
            if (!saisonDTO.getLibelle().matches(Constants.SAISON_LIBELLE_REGEX)) {
                throw new ApiRequestException(
                    ExceptionCode.SAISON_LIBELLE_NOT_VALID.getMessage(),
                    ExceptionCode.SAISON_LIBELLE_NOT_VALID.getValue(),
                    ExceptionLevel.WARNING
                );
            }

            saisonDTO.setOrganisationId(organisationDTO.getId());

            String toHash = organisationDTO.getId().toString() + ZonedDateTime.now().toString();
            saisonDTO.setHashcode(communService.toHash(toHash));

            saisonDTO.setCode("REF_" + organisationDTO.getId().toString());

            saisonDTO.setCreatedAt(ZonedDateTime.now());
            saisonDTO.setUpdatedAt(ZonedDateTime.now());
            saisonDTO.setDeleted(false);

           /* SaisonDTO result = save(saisonDTO);

            result.setCode(saisonDTO.getCode() + "_" + result.getId().toString());
            result.setHashcode(communService.toHash(result.getId().toString()));*/

            return save(saisonDTO);
        } else {
            throw new ApiRequestException(
                ExceptionCode.ORGANISATION_NOT_FOUND.getMessage(),
                ExceptionCode.ORGANISATION_NOT_FOUND.getValue(),
                ExceptionLevel.ERROR
            );
        }
    }

    @Override
    public Optional<SaisonDTO> findByOrganisationAndLibelle(OrganisationDTO organisationDTO, String libelle) {
        log.debug("Request to get saison by organisation: {} and libelle: {}", organisationDTO, libelle);

        return saisonRepository.findOneByOrganisationAndLibelleAndDeletedIsFalse(organisationMapper.toEntity(organisationDTO), libelle)
            .map(saisonMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<SaisonDTO> search(OrganisationDTO organisationDTO, String query, Pageable pageable) {
        log.debug("Request to search for a page of Saisons for query {}", query);

        QueryBuilder queryBuilder = boolQuery()
            .must(
                queryStringQuery(organisationDTO.getId().toString()).analyzeWildcard(true).field("organisation.id")
            )
            .must(
                queryStringQuery("false").analyzeWildcard(true).field("deleted")
            )
            .must(queryStringQuery(query));

        return saisonSearchRepository.search(queryBuilder, pageable)
            .map(saisonMapper::toDto);
    }

    @Override
    public List<SaisonDTO> findAll() {
        return saisonRepository.findAllByDeletedIsFalse().stream()
            .map(saisonMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public Page<SaisonDTO> findByIdIn(List<Long> saisonIds, Pageable pageable) {
        log.debug("Request to get all saisons by ids");
        return saisonRepository.findAllByIdInAndDeletedIsFalse(pageable, saisonIds)
            .map(saisonMapper::toDto);
    }
    @Override
    public void reIndex() {
        log.info("Request to reindex all Saisons");
        saisonSearchRepository.deleteAll();
        findAll().stream().map(saisonMapper::toEntity).forEach(saisonSearchRepository::save);
    }
}
