package org.nddngroup.navetfoot.core.service.impl;

import org.nddngroup.navetfoot.core.domain.CoupFranc;
import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;
import org.nddngroup.navetfoot.core.repository.CoupFrancRepository;
import org.nddngroup.navetfoot.core.repository.search.CoupFrancSearchRepository;
import org.nddngroup.navetfoot.core.service.CoupFrancService;
import org.nddngroup.navetfoot.core.service.dto.CoupFrancDTO;
import org.nddngroup.navetfoot.core.service.dto.MatchDTO;
import org.nddngroup.navetfoot.core.service.mapper.CoupFrancMapper;
import org.nddngroup.navetfoot.core.service.mapper.MatchMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link CoupFranc}.
 */
@Service
@Transactional
public class CoupFrancServiceImpl implements CoupFrancService {

    private final Logger log = LoggerFactory.getLogger(CoupFrancServiceImpl.class);

    private final CoupFrancRepository coupFrancRepository;

    private final CoupFrancMapper coupFrancMapper;

    private final CoupFrancSearchRepository coupFrancSearchRepository;
    @Autowired
    private MatchMapper matchMapper;

    public CoupFrancServiceImpl(CoupFrancRepository coupFrancRepository, CoupFrancMapper coupFrancMapper, CoupFrancSearchRepository coupFrancSearchRepository) {
        this.coupFrancRepository = coupFrancRepository;
        this.coupFrancMapper = coupFrancMapper;
        this.coupFrancSearchRepository = coupFrancSearchRepository;
    }

    @Override
    public CoupFrancDTO save(CoupFrancDTO coupFrancDTO) {
        log.debug("Request to save CoupFranc : {}", coupFrancDTO);

        coupFrancDTO.setUpdatedAt(ZonedDateTime.now());
        if (coupFrancDTO.getCreatedAt() == null) {
            coupFrancDTO.setCreatedAt(ZonedDateTime.now());
        }

        CoupFranc coupFranc = coupFrancMapper.toEntity(coupFrancDTO);
        coupFranc = coupFrancRepository.save(coupFranc);
        CoupFrancDTO result = coupFrancMapper.toDto(coupFranc);

        if (coupFrancDTO.isDeleted()) {
            coupFrancSearchRepository.deleteById(coupFrancDTO.getId());
        } else {
            coupFrancSearchRepository.save(coupFrancMapper.toEntity(result));
        }

        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<CoupFrancDTO> findAll(Pageable pageable) {
        log.debug("Request to get all CoupFrancs");
        return coupFrancRepository.findAll(pageable)
            .map(coupFrancMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<CoupFrancDTO> findOne(Long id) {
        log.debug("Request to get CoupFranc : {}", id);
        return coupFrancRepository.findById(id)
            .map(coupFrancMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete CoupFranc : {}", id);
        coupFrancRepository.deleteById(id);
        coupFrancSearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<CoupFrancDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of CoupFrancs for query {}", query);
        return coupFrancSearchRepository.search(queryStringQuery(query), pageable)
            .map(coupFrancMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<CoupFrancDTO> findByMatch(MatchDTO matchDTO) {
        log.debug("Request to get all CoupFrancs by match");
        return coupFrancRepository.findAllByMatchAndDeletedIsFalse(matchMapper.toEntity(matchDTO))
            .stream()
            .map(coupFrancMapper::toDto).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<CoupFrancDTO> findByMatchAndEquipe(MatchDTO matchDTO, Equipe equipe) {
        log.debug("Request to get all CoupFrancs by match and equipe");
        return coupFrancRepository.findAllByMatchAndEquipeAndDeletedIsFalse(matchMapper.toEntity(matchDTO), equipe)
            .stream()
            .map(coupFrancMapper::toDto).collect(Collectors.toList());
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<CoupFrancDTO> findByHashcode(String hashcode) {
        log.debug("Request to get CoupFranc : {}", hashcode);
        return coupFrancRepository.findByHashcodeAndDeletedIsFalse(hashcode)
            .map(coupFrancMapper::toDto);
    }

    @Override
    public List<CoupFrancDTO> findAll() {
        return coupFrancRepository.findAllByDeletedIsFalse().stream()
            .map(coupFrancMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public void reIndex() {
        log.info("Request to reindex all CoupFrancs");
        coupFrancSearchRepository.deleteAll();
        findAll().stream().map(coupFrancMapper::toEntity).forEach(coupFrancSearchRepository::save);
    }
}
