package org.nddngroup.navetfoot.core.service.mapper;


import org.nddngroup.navetfoot.core.domain.*;
import org.nddngroup.navetfoot.core.domain.Contrat;
import org.nddngroup.navetfoot.core.service.dto.ContratDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Contrat} and its DTO {@link ContratDTO}.
 */
@Mapper(componentModel = "spring", uses = {SaisonMapper.class, AssociationMapper.class, JoueurMapper.class})
public interface ContratMapper extends EntityMapper<ContratDTO, Contrat> {

    @Mapping(source = "saison.id", target = "saisonId")
    @Mapping(source = "saison.libelle", target = "saisonLibelle")
    @Mapping(source = "association.id", target = "associationId")
    @Mapping(source = "association.nom", target = "associationNom")
    @Mapping(source = "joueur.id", target = "joueurId")
    ContratDTO toDto(Contrat contrat);

    @Mapping(source = "saisonId", target = "saison")
    @Mapping(source = "associationId", target = "association")
    @Mapping(source = "joueurId", target = "joueur")
    Contrat toEntity(ContratDTO contratDTO);

    default Contrat fromId(Long id) {
        if (id == null) {
            return null;
        }
        Contrat contrat = new Contrat();
        contrat.setId(id);
        return contrat;
    }
}
