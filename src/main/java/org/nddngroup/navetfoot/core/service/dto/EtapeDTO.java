package org.nddngroup.navetfoot.core.service.dto;

import org.nddngroup.navetfoot.core.domain.Etape;

import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link Etape} entity.
 */
public class EtapeDTO implements Serializable {

    private Long id;

    @NotNull
    private String libelle;

    @NotNull
    private Integer niveau;

    @NotNull
    private String code;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Integer getNiveau() {
        return niveau;
    }

    public void setNiveau(Integer niveau) {
        this.niveau = niveau;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EtapeDTO)) {
            return false;
        }

        return id != null && id.equals(((EtapeDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "EtapeDTO{" +
            "id=" + getId() +
            ", libelle='" + getLibelle() + "'" +
            ", niveau=" + getNiveau() +
            ", code='" + getCode() + "'" +
            "}";
    }
}
