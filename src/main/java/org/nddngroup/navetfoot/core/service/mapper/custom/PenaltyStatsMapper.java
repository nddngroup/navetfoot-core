package org.nddngroup.navetfoot.core.service.mapper.custom;

import org.nddngroup.navetfoot.core.domain.Penalty;
import org.nddngroup.navetfoot.core.service.*;
import org.nddngroup.navetfoot.core.service.*;
import org.nddngroup.navetfoot.core.service.dto.PenaltyDTO;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring")
public abstract class PenaltyStatsMapper {
    @Autowired
    protected EtapeCompetitionService etapeCompetitionService;
    @Autowired
    protected CompetitionSaisonService competitionSaisonService;
    @Autowired
    protected SaisonService saisonService;
    @Autowired
    protected CompetitionService competitionService;
    @Autowired
    protected OrganisationService organisationService;
    @Autowired
    protected AssociationService associationService;
    @Autowired
    protected MatchService matchService;
    @Autowired
    protected JoueurService joueurService;
    @Autowired
    protected MatchStatsMapper matchStatsMapper;

    public Penalty toStats(PenaltyDTO penalty) {
        var penaltyStats = new Penalty()
            .code(penalty.getCode())
            .hashcode(penalty.getHashcode())
            .equipe(penalty.getEquipe())
            .instant(penalty.getInstant())
            .issu(penalty.getIssu())
            .deleted(penalty.isDeleted())
            .createdAt(penalty.getCreatedAt())
            .updatedAt(penalty.getUpdatedAt());

        penaltyStats.setId(penalty.getId());

        // find joueur
        joueurService.findOne(penalty.getJoueurId())
            .ifPresent(joueurDTO -> {
                penaltyStats.setJoueurId(joueurDTO.getId());
                penaltyStats.setJoueurNom(joueurDTO.getNom());
                penaltyStats.setJoueurPrenom(joueurDTO.getPrenom());
                penaltyStats.setJoueurImage(joueurDTO.getImageUrl());
            });

        // find match
        matchService.findOne(penalty.getMatchId())
            .ifPresent(matchDTO -> {
                penaltyStats.setMatchId(matchDTO.getId());

                var matchStats = matchStatsMapper.toStats(matchDTO);
                penaltyStats.setLocalId(matchStats.getLocalId());
                penaltyStats.setLocalNom(matchStats.getLocalNom());

                penaltyStats.setVisiteurId(matchStats.getVisiteurId());
                penaltyStats.setVisiteurNom(matchStats.getVisiteurNom());

                penaltyStats.setEtapeCompetitionId(matchStats.getEtapeCompetitionId());
                penaltyStats.setEtapeCompetitionNom(matchStats.getEtapeCompetitionNom());

                penaltyStats.setSaisonId(matchStats.getSaisonId());
                penaltyStats.setSaisonLibelle(matchStats.getSaisonLibelle());

                penaltyStats.setCompetitionId(matchStats.getCompetitionId());
                penaltyStats.setCompetitionNom(matchStats.getCompetitionNom());

                penaltyStats.setOrganisationId(matchStats.getOrganisationId());
                penaltyStats.setOrganisationNom(matchStats.getOrganisationNom());
            });

        return penaltyStats;
    }
}
