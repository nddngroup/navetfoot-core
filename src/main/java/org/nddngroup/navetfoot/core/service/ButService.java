package org.nddngroup.navetfoot.core.service;

import org.nddngroup.navetfoot.core.domain.But;
import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;
import org.nddngroup.navetfoot.core.service.dto.ButDTO;
import org.nddngroup.navetfoot.core.service.dto.JoueurDTO;
import org.nddngroup.navetfoot.core.service.dto.MatchDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link But}.
 */
public interface ButService {

    /**
     * Save a but.
     *
     * @param butDTO the entity to save.
     * @return the persisted entity.
     */
    ButDTO save(ButDTO butDTO);

    /**
     * Get all the buts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ButDTO> findAll(Pageable pageable);


    /**
     * Get the "id" but.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ButDTO> findOne(Long id);

    /**
     * Delete the "id" but.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the but corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ButDTO> search(String query, Pageable pageable);

    List<ButDTO> findByMatch(MatchDTO matchDTO);
    List<ButDTO> findByMatchAndEquipe(MatchDTO matchDTO, Equipe equipe);
    List<ButDTO> findByMatchAndJoueur(MatchDTO matchDTO, JoueurDTO joueurDTO);
    Optional<ButDTO> findByHashcode(String hashcode);
    List<ButDTO> findAll();
    void reIndex();
}
