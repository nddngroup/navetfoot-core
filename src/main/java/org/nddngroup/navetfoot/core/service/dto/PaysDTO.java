package org.nddngroup.navetfoot.core.service.dto;

import org.nddngroup.navetfoot.core.domain.Pays;

import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link Pays} entity.
 */
public class PaysDTO implements Serializable {

    private Long id;

    @NotNull
    private String nom;

    @NotNull
    private String indicatif;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getIndicatif() {
        return indicatif;
    }

    public void setIndicatif(String indicatif) {
        this.indicatif = indicatif;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PaysDTO)) {
            return false;
        }

        return id != null && id.equals(((PaysDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PaysDTO{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", indicatif='" + getIndicatif() + "'" +
            "}";
    }
}
