package org.nddngroup.navetfoot.core.service.mapper;


import org.nddngroup.navetfoot.core.domain.*;
import org.nddngroup.navetfoot.core.domain.But;
import org.nddngroup.navetfoot.core.service.dto.ButDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link But} and its DTO {@link ButDTO}.
 */
@Mapper(componentModel = "spring", uses = {JoueurMapper.class, MatchMapper.class})
public interface ButMapper extends EntityMapper<ButDTO, But> {

    @Mapping(source = "joueur.id", target = "joueurId")
    @Mapping(source = "match.id", target = "matchId")
    ButDTO toDto(But but);

    @Mapping(source = "joueurId", target = "joueur")
    @Mapping(source = "matchId", target = "match")
    But toEntity(ButDTO butDTO);

    default But fromId(Long id) {
        if (id == null) {
            return null;
        }
        But but = new But();
        but.setId(id);
        return but;
    }
}
