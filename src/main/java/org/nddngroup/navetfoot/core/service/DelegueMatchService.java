package org.nddngroup.navetfoot.core.service;

import org.nddngroup.navetfoot.core.domain.DelegueMatch;
import org.nddngroup.navetfoot.core.service.dto.DelegueMatchDTO;
import org.nddngroup.navetfoot.core.service.dto.MatchDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link DelegueMatch}.
 */
public interface DelegueMatchService {

    /**
     * Save a delegueMatch.
     *
     * @param delegueMatchDTO the entity to save.
     * @return the persisted entity.
     */
    DelegueMatchDTO save(DelegueMatchDTO delegueMatchDTO);

    /**
     * Get all the delegueMatches.
     *
     * @return the list of entities.
     */
    List<DelegueMatchDTO> findAll();


    /**
     * Get the "id" delegueMatch.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<DelegueMatchDTO> findOne(Long id);

    /**
     * Delete the "id" delegueMatch.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the delegueMatch corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @return the list of entities.
     */
    List<DelegueMatchDTO> search(String query);
    Optional<DelegueMatchDTO> findByMatch(MatchDTO matchDTO);
    Optional<DelegueMatchDTO> findByConfirmCode(String codeMatch);
    List<DelegueMatchDTO> findAllByDeletedIsFalse();
    void reIndex();
}
