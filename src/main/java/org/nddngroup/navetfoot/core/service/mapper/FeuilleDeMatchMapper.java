package org.nddngroup.navetfoot.core.service.mapper;


import org.nddngroup.navetfoot.core.domain.*;
import org.nddngroup.navetfoot.core.domain.FeuilleDeMatch;
import org.nddngroup.navetfoot.core.service.dto.FeuilleDeMatchDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link FeuilleDeMatch} and its DTO {@link FeuilleDeMatchDTO}.
 */
@Mapper(componentModel = "spring", uses = {MatchMapper.class})
public interface FeuilleDeMatchMapper extends EntityMapper<FeuilleDeMatchDTO, FeuilleDeMatch> {

    @Mapping(source = "match.id", target = "matchId")
    FeuilleDeMatchDTO toDto(FeuilleDeMatch feuilleDeMatch);

    @Mapping(source = "matchId", target = "match")
    @Mapping(target = "detailFeuilleDeMatchs", ignore = true)
    @Mapping(target = "removeDetailFeuilleDeMatchs", ignore = true)
    FeuilleDeMatch toEntity(FeuilleDeMatchDTO feuilleDeMatchDTO);

    default FeuilleDeMatch fromId(Long id) {
        if (id == null) {
            return null;
        }
        FeuilleDeMatch feuilleDeMatch = new FeuilleDeMatch();
        feuilleDeMatch.setId(id);
        return feuilleDeMatch;
    }
}
