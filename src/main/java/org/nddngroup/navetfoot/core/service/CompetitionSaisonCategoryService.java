package org.nddngroup.navetfoot.core.service;

import org.nddngroup.navetfoot.core.service.dto.*;

import org.nddngroup.navetfoot.core.domain.CompetitionSaisonCategory;
import org.nddngroup.navetfoot.core.service.dto.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link CompetitionSaisonCategory}.
 */
public interface CompetitionSaisonCategoryService {

    /**
     * Save a competitionSaisonCategory.
     *
     * @param competitionSaisonCategoryDTO the entity to save.
     * @return the persisted entity.
     */
    CompetitionSaisonCategoryDTO save(CompetitionSaisonCategoryDTO competitionSaisonCategoryDTO);

    /**
     * Get all the competitionSaisonCategories.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CompetitionSaisonCategoryDTO> findAll(Pageable pageable);


    /**
     * Get the "id" competitionSaisonCategory.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CompetitionSaisonCategoryDTO> findOne(Long id);

    /**
     * Delete the "id" competitionSaisonCategory.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the competitionSaisonCategory corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CompetitionSaisonCategoryDTO> search(String query, Pageable pageable);
    List<CompetitionSaisonCategoryDTO> findByCompetitionSaison(CompetitionSaisonDTO competitionSaisonDTO);
    Optional<CompetitionSaisonCategoryDTO> findByCompetitionSaisonAndCategory(CompetitionSaisonDTO competitionSaisonDTO, CategoryDTO categoryDTO);
    CompetitionSaisonCategoryDTO save(CategoryDTO categoryDTO, OrganisationDTO organisationDTO, SaisonDTO saisonDTO, CompetitionDTO competitionDTO);
    List<CompetitionSaisonCategoryDTO> findAll();
    void reIndex();
}
