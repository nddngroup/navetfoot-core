package org.nddngroup.navetfoot.core.service;

import org.nddngroup.navetfoot.core.domain.Departement;
import org.nddngroup.navetfoot.core.service.dto.DepartementDTO;

import org.nddngroup.navetfoot.core.service.dto.RegionDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Departement}.
 */
public interface DepartementService {

    /**
     * Save a departement.
     *
     * @param departementDTO the entity to save.
     * @return the persisted entity.
     */
    DepartementDTO save(DepartementDTO departementDTO);

    /**
     * Get all the departements.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<DepartementDTO> findAll(Pageable pageable);


    /**
     * Get the "id" departement.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<DepartementDTO> findOne(Long id);

    /**
     * Delete the "id" departement.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the departement corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<DepartementDTO> search(String query, Pageable pageable);

    List<DepartementDTO> findDepartementsByRegion(RegionDTO region);
    List<DepartementDTO> findAll();
    void reIndex();
}
