package org.nddngroup.navetfoot.core.service.mapper;


import org.nddngroup.navetfoot.core.domain.*;
import org.nddngroup.navetfoot.core.domain.Competition;
import org.nddngroup.navetfoot.core.service.dto.CompetitionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Competition} and its DTO {@link CompetitionDTO}.
 */
@Mapper(componentModel = "spring", uses = {OrganisationMapper.class})
public interface CompetitionMapper extends EntityMapper<CompetitionDTO, Competition> {

    @Mapping(source = "organisation.id", target = "organisationId")
    @Mapping(source = "organisation.nom", target = "organisationNom")
    CompetitionDTO toDto(Competition competition);

    @Mapping(target = "competitionSaisons", ignore = true)
    @Mapping(target = "removeCompetitionSaison", ignore = true)
    @Mapping(source = "organisationId", target = "organisation")
    Competition toEntity(CompetitionDTO competitionDTO);

    default Competition fromId(Long id) {
        if (id == null) {
            return null;
        }
        Competition competition = new Competition();
        competition.setId(id);
        return competition;
    }
}
