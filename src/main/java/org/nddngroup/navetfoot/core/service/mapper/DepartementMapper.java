package org.nddngroup.navetfoot.core.service.mapper;


import org.nddngroup.navetfoot.core.domain.*;
import org.nddngroup.navetfoot.core.domain.Departement;
import org.nddngroup.navetfoot.core.service.dto.DepartementDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Departement} and its DTO {@link DepartementDTO}.
 */
@Mapper(componentModel = "spring", uses = {RegionMapper.class})
public interface DepartementMapper extends EntityMapper<DepartementDTO, Departement> {

    @Mapping(source = "region.id", target = "regionId")
    DepartementDTO toDto(Departement departement);

    @Mapping(source = "regionId", target = "region")
    Departement toEntity(DepartementDTO departementDTO);

    default Departement fromId(Long id) {
        if (id == null) {
            return null;
        }
        Departement departement = new Departement();
        departement.setId(id);
        return departement;
    }
}
