package org.nddngroup.navetfoot.core.service.custom;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import org.nddngroup.navetfoot.core.config.rabbitMQ.RabbitMQConfig;
import org.nddngroup.navetfoot.core.domain.custom.Abonnement;
import org.nddngroup.navetfoot.core.domain.custom.NotificationObject;
import org.nddngroup.navetfoot.core.domain.custom.stats.*;
import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;
import org.nddngroup.navetfoot.core.domain.enumeration.TypeNotif;
import org.nddngroup.navetfoot.core.domain.enumeration.websocket.RabbitMQMessageType;
import org.nddngroup.navetfoot.core.domain.websocket.RabbitMQMessage;
import org.nddngroup.navetfoot.core.domain.websocket.WebSocketTopic;
import org.nddngroup.navetfoot.core.exception.ApiRequestException;
import org.nddngroup.navetfoot.core.exception.ExceptionCode;
import org.nddngroup.navetfoot.core.exception.ExceptionLevel;
import org.nddngroup.navetfoot.core.service.AssociationService;
import org.nddngroup.navetfoot.core.service.ButService;
import org.nddngroup.navetfoot.core.service.JoueurService;
import org.nddngroup.navetfoot.core.service.MatchService;
import org.nddngroup.navetfoot.core.service.client.NotifApiClient;
import org.nddngroup.navetfoot.core.service.dto.*;
import org.nddngroup.navetfoot.core.service.websocket.WebSocketApiClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

/**
 * Created by souleymane91 on 06/01/2019.
 */
@Service
public class NotificationService {
    // Find your Account Sid and Auth Token at twilio.com/console
    @Value("${application.twilio.account-sid}")
    private String twilioAccountSid;
    @Value("${application.twilio.auth-token}")
    private String twilioAuthToken;
    @Value("${application.twilio.service-sid}")
    private String twilioServiceSid;

    private final Logger log = LoggerFactory.getLogger(NotificationService.class);
    @Autowired
    private JoueurService joueurService;
    @Autowired
    private MatchService matchService;
    @Autowired
    private AssociationService associationService;
    @Autowired
    private ButService butService;
    @Autowired
    private NotifApiClient notifApiClient;
    @Autowired
    private WebSocketApiClientService webSocketApiClientService;

    /**
     * @param matchDTO //
     */
    public void sendEtatMatchNotif(MatchDTO matchDTO, String etatMatch) {
        if (matchDTO.getChrono() == null) {
            matchDTO.setChrono("0");
        }

        Long matchId = matchDTO.getId();
        Integer instant = Integer.parseInt(matchDTO.getChrono());

        sendEventNotif(etatMatch, matchId, instant, null, null, null, null, null);
    }

    /**
     * @param matchDTO //
     */
    public void sendRappel(MatchDTO matchDTO) {
        Long matchId = matchDTO.getId();
        Integer instant = Integer.parseInt(matchDTO.getChrono());

        sendEventNotif(TypeNotif.RAPPEL.name(), matchId, instant, null, null, null, null, null);
    }

    /**
     *
     * @param butDTO //
     */
    public void sendButNotification(ButDTO butDTO) {
        Long matchId = butDTO.getMatchId();
        Long joueurId = butDTO.getJoueurId();
        Integer instant = butDTO.getInstant();
        var equipe = butDTO.getEquipe();

        sendEventNotif(TypeNotif.BUT.name(), matchId, instant, equipe, joueurId, null, null, null);
    }

    @Async
    public void sendEventNotif(String type, Long matchId, Integer instant, Equipe equipe, Long joueurId, Long entrantId,
            String deviceId, String extra) {

        var notificationObject = new NotificationObject();

        notificationObject.setType(type);
        notificationObject.setMatchId(matchId);
        notificationObject.setInstant(instant);

        if (equipe == Equipe.LOCAL) {
            notificationObject.setEquipe("local");
        } else if (equipe == Equipe.VISITEUR) {
            notificationObject.setEquipe("visiteur");
        } else {
            notificationObject.setEquipe("all");
        }

        if (type.equals(TypeNotif.PENALTY_SERIE.name())) {
            notificationObject.setIssu(extra);
        }

        notificationObject = getDetailsNotification(notificationObject, matchId, joueurId, entrantId);

        // envoi notif
        toNotif(notificationObject, deviceId);
    }

    /**
     * Envoi de la requete vers le serveur de notification
     *
     * @param notificationObject NotificationObject
     */
    public void toNotif(NotificationObject notificationObject, String deviceId) {
        try {
            notifApiClient.sendPushNotification(deviceId, notificationObject);
        } catch (Exception e) {
            log.error("Error sending to notif server : {}", e.getMessage());
        }
    }

    @Async
    public void sendCodeMatch(String codeMatch, DelegueDTO delegueDTO, Logger logger) {
        logger.info("_______________ SENDING CODE MATCH ________________");

        String telephone = "+221" + delegueDTO.getTelephone();
        String email = delegueDTO.getEmail();

        Twilio.init(twilioAccountSid, twilioAuthToken);

        try {
            Message message = Message
                    .creator(
                            new PhoneNumber(telephone), // to
                            new PhoneNumber(twilioServiceSid), // from
                            "Bonjour, votre code d'autorisation pour la gestion du match est: code = " + codeMatch)
                    .create();

            logger.info("______________ AFTER SEND SMS ___________");
            logger.info(message.toString());
        } catch (Exception e) {
            logger.info("______________ ERROR SEND SMS ___________");
            logger.info(e.getMessage());

            throw new ApiRequestException(
                    ExceptionCode.SMS_NOT_SENT.getMessage(),
                    ExceptionCode.SMS_NOT_SENT.getValue(),
                    ExceptionLevel.WARNING);
        }
    }

    public NotificationObject getDetailsNotification(NotificationObject notificationObject, Long matchId, Long joueurId,
            Long entrantId) {
        // get joueur by id
        if (joueurId != null) {
            JoueurDTO joueurDTO = joueurService.findOne(joueurId)
                    .orElseThrow(
                            () -> new ApiRequestException(
                                    ExceptionCode.JOUEUR_NOT_FOUND.getMessage(),
                                    ExceptionCode.JOUEUR_NOT_FOUND.getValue(),
                                    ExceptionLevel.ERROR));

            notificationObject.setJoueurPrenom(joueurDTO.getPrenom());
            notificationObject.setJoueurNom(joueurDTO.getNom());
            notificationObject.setJoueurImage(joueurDTO.getImageUrl());
            notificationObject.setJoueurHashcode(joueurDTO.getHashcode());
        }

        // get entrant if type = 'remplacement'
        if (entrantId != null) {
            JoueurDTO entrantDTO = joueurService.findOne(entrantId)
                    .orElseThrow(
                            () -> new ApiRequestException(
                                    ExceptionCode.JOUEUR_NOT_FOUND.getMessage(),
                                    ExceptionCode.JOUEUR_NOT_FOUND.getValue(),
                                    ExceptionLevel.ERROR));

            notificationObject.setEntrantPrenom(entrantDTO.getPrenom());
            notificationObject.setEntrantNom(entrantDTO.getNom());
            notificationObject.setEntrantImage(entrantDTO.getImageUrl());
            notificationObject.setEntrantHashcode(entrantDTO.getHashcode());
        }

        // get match by id
        MatchDTO matchDTO = matchService.findOne(matchId)
                .orElseThrow(
                        () -> new ApiRequestException(
                                ExceptionCode.MATCH_NOT_FOUND.getMessage(),
                                ExceptionCode.MATCH_NOT_FOUND.getValue(),
                                ExceptionLevel.ERROR));

        Long localId = matchDTO.getLocalId();
        Long visiteurId = matchDTO.getVisiteurId();

        // get local and visiteur by ids
        AssociationDTO local = associationService.findOne(localId)
                .orElseThrow(
                        () -> new ApiRequestException(
                                ExceptionCode.CLUB_LOCAL_AFFILIATION_NOT_FOUND.getMessage(),
                                ExceptionCode.CLUB_LOCAL_AFFILIATION_NOT_FOUND.getValue(),
                                ExceptionLevel.ERROR));
        AssociationDTO visiteur = associationService.findOne(visiteurId)
                .orElseThrow(
                        () -> new ApiRequestException(
                                ExceptionCode.CLUB_VISITEUR_NOT_FOUND_FOR_MATCH.getMessage(),
                                ExceptionCode.CLUB_VISITEUR_NOT_FOUND_FOR_MATCH.getValue(),
                                ExceptionLevel.ERROR));

        notificationObject.setLocal(local.getNom());
        notificationObject.setVisiteur(visiteur.getNom());

        notificationObject.setLocalImage(local.getCouleurs());
        notificationObject.setVisiteurImage(visiteur.getCouleurs());

        // get all 'buts' by matchId
        List<ButDTO> buts = butService.findByMatch(matchDTO);

        Integer nbButsLocal = 0;
        Integer nbButsVisiteur = 0;

        for (ButDTO but : buts) {
            if (but.getEquipe() == Equipe.VISITEUR) {
                nbButsVisiteur++;
            } else if (but.getEquipe() == Equipe.LOCAL) {
                nbButsLocal++;
            }
        }

        notificationObject.setNbButsLocal(nbButsLocal);
        notificationObject.setNbButsVisiteur(nbButsVisiteur);

        return notificationObject;
    }

    public Abonnement addAbonnement(Abonnement abonnement) {
        ResponseEntity<Abonnement> response = notifApiClient.addAbonnement(abonnement);

        if (response.getStatusCode().equals(HttpStatus.CREATED)) {
            Abonnement result = response.getBody();

            // find match
            if (result != null && result.getMatchId() != null) {
                Long matchId = result.getMatchId();
                Integer instant = 0;

                sendEventNotif(TypeNotif.CONFIRM.name(), matchId, instant, null, null, null, abonnement.getDeviceId(),
                        null);
            }

            return result;

        } else {
            log.info("Error abonnement : deviceId = {}", abonnement.getDeviceId());
            throw new ApiRequestException(
                    ExceptionCode.ERROR_ABONNEMENT.getMessage(),
                    ExceptionCode.ERROR_ABONNEMENT.getValue(),
                    ExceptionLevel.WARNING);
        }
    }

    public void sendButToClient(ButDTO event) {
        NotificationObject notificationObject = new NotificationObject();
        notificationObject = getDetailsNotification(notificationObject, event.getMatchId(), event.getJoueurId(), null);

        ButDetail detail = new ButDetail();
        detail.setPrenomJoueur(notificationObject.getJoueurPrenom());
        detail.setNomJoueur(notificationObject.getJoueurNom());
        detail.setImageUrlJoueur(notificationObject.getJoueurImage());
        detail.setJoueurHashcode(notificationObject.getJoueurHashcode());
        detail.setButDTO(event);

        webSocketApiClientService.sendToClientViaWebSocket(WebSocketTopic.MATCH_EVENT.getTopic(event.getMatchId()), detail);
        webSocketApiClientService.sendToRabbitMQ(RabbitMQConfig.MATCH_LIVE_ROUTING_KEY, List.of(event.getMatchId().toString()), RabbitMQMessage.build(detail, RabbitMQMessageType.MATCH_EVENT));
    }
    public void sendCartonToClient(CartonDTO event) {
        NotificationObject notificationObject = new NotificationObject();
        notificationObject = getDetailsNotification(notificationObject, event.getMatchId(), event.getJoueurId(), null);

        CartonDetail detail = new CartonDetail();
        detail.setPrenomJoueur(notificationObject.getJoueurPrenom());
        detail.setNomJoueur(notificationObject.getJoueurNom());
        detail.setImageUrlJoueur(notificationObject.getJoueurImage());
        detail.setJoueurHashcode(notificationObject.getJoueurHashcode());
        detail.setCartonDTO(event);

        webSocketApiClientService.sendToClientViaWebSocket(WebSocketTopic.MATCH_EVENT.getTopic(event.getMatchId()), detail);
        webSocketApiClientService.sendToRabbitMQ(RabbitMQConfig.MATCH_LIVE_ROUTING_KEY, List.of(event.getMatchId().toString()), RabbitMQMessage.build(detail, RabbitMQMessageType.MATCH_EVENT));
    }
    public void sendPenaltyToClient(PenaltyDTO event) {
        NotificationObject notificationObject = new NotificationObject();
        notificationObject = getDetailsNotification(notificationObject, event.getMatchId(), event.getJoueurId(), null);

        PenaltyDetail detail = new PenaltyDetail();
        detail.setPrenomJoueur(notificationObject.getJoueurPrenom());
        detail.setNomJoueur(notificationObject.getJoueurNom());
        detail.setImageUrlJoueur(notificationObject.getJoueurImage());
        detail.setJoueurHashcode(notificationObject.getJoueurHashcode());
        detail.setPenaltyDTO(event);

        webSocketApiClientService.sendToClientViaWebSocket(WebSocketTopic.MATCH_EVENT.getTopic(event.getMatchId()), detail);
        webSocketApiClientService.sendToRabbitMQ(RabbitMQConfig.MATCH_LIVE_ROUTING_KEY, List.of(event.getMatchId().toString()), RabbitMQMessage.build(detail, RabbitMQMessageType.MATCH_EVENT));
    }
    public void sendCoupFrancToClient(CoupFrancDTO event) {
        CoupFrancDetail detail = new CoupFrancDetail();
        detail.setCoupFrancDTO(event);

        webSocketApiClientService.sendToClientViaWebSocket(WebSocketTopic.MATCH_EVENT.getTopic(event.getMatchId()), detail);
        webSocketApiClientService.sendToRabbitMQ(RabbitMQConfig.MATCH_LIVE_ROUTING_KEY, List.of(event.getMatchId().toString()), RabbitMQMessage.build(detail, RabbitMQMessageType.MATCH_EVENT));
    }
    public void sendCornerToClient(CornerDTO event) {
        CornerDetail detail = new CornerDetail();
        detail.setCornerDTO(event);

        webSocketApiClientService.sendToClientViaWebSocket(WebSocketTopic.MATCH_EVENT.getTopic(event.getMatchId()), detail);
        webSocketApiClientService.sendToRabbitMQ(RabbitMQConfig.MATCH_LIVE_ROUTING_KEY, List.of(event.getMatchId().toString()), RabbitMQMessage.build(detail, RabbitMQMessageType.MATCH_EVENT));
    }
    public void sendRemplacementToClient(RemplacementDTO event) {
        NotificationObject notificationObject = new NotificationObject();
        notificationObject = getDetailsNotification(notificationObject, event.getMatchId(), event.getSortantId(), event.getEntrantId());

        RemplacementDetail detail = new RemplacementDetail();
        detail.setPrenomSortant(notificationObject.getJoueurPrenom());
        detail.setNomSortant(notificationObject.getJoueurNom());
        detail.setSortantHashcode(notificationObject.getJoueurHashcode());
        detail.setImageUrlSortant(notificationObject.getJoueurImage());
        detail.setPrenomEntrant(notificationObject.getEntrantPrenom());
        detail.setNomEntrant(notificationObject.getEntrantNom());
        detail.setEntrantHashcode(notificationObject.getEntrantHashcode());
        detail.setImageUrlEntrant(notificationObject.getEntrantImage());
        detail.setRemplacementDTO(event);

        webSocketApiClientService.sendToClientViaWebSocket(WebSocketTopic.MATCH_EVENT.getTopic(event.getMatchId()), detail);
        webSocketApiClientService.sendToRabbitMQ(RabbitMQConfig.MATCH_LIVE_ROUTING_KEY, List.of(event.getMatchId().toString()), RabbitMQMessage.build(detail, RabbitMQMessageType.MATCH_EVENT));
    }
    public void sendHorsJeuToClient(HorsJeuDTO event) {
        HorsJeuDetail detail = new HorsJeuDetail();
        detail.setHorsJeuDTO(event);

        webSocketApiClientService.sendToClientViaWebSocket(WebSocketTopic.MATCH_EVENT.getTopic(event.getMatchId()), detail);
        webSocketApiClientService.sendToRabbitMQ(RabbitMQConfig.MATCH_LIVE_ROUTING_KEY, List.of(event.getMatchId().toString()), RabbitMQMessage.build(detail, RabbitMQMessageType.MATCH_EVENT));
    }
    public void sendTirToClient(TirDTO event) {
        NotificationObject notificationObject = new NotificationObject();
        notificationObject = getDetailsNotification(notificationObject, event.getMatchId(), event.getJoueurId(), null);

        TirDetail detail = new TirDetail();
        detail.setPrenomJoueur(notificationObject.getJoueurPrenom());
        detail.setNomJoueur(notificationObject.getJoueurNom());
        detail.setImageUrlJoueur(notificationObject.getJoueurImage());
        detail.setJoueurHashcode(notificationObject.getJoueurHashcode());
        detail.setTirDTO(event);

        webSocketApiClientService.sendToClientViaWebSocket(WebSocketTopic.MATCH_EVENT.getTopic(event.getMatchId()), detail);
        webSocketApiClientService.sendToRabbitMQ(RabbitMQConfig.MATCH_LIVE_ROUTING_KEY, List.of(event.getMatchId().toString()), new RabbitMQMessage().body(detail));
    }
}
