package org.nddngroup.navetfoot.core.service.mapper;


import org.nddngroup.navetfoot.core.domain.*;
import org.nddngroup.navetfoot.core.domain.Parametrage;
import org.nddngroup.navetfoot.core.service.dto.ParametrageDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Parametrage} and its DTO {@link ParametrageDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ParametrageMapper extends EntityMapper<ParametrageDTO, Parametrage> {



    default Parametrage fromId(Long id) {
        if (id == null) {
            return null;
        }
        Parametrage parametrage = new Parametrage();
        parametrage.setId(id);
        return parametrage;
    }
}
