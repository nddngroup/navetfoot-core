package org.nddngroup.navetfoot.core.service.mapper;


import org.nddngroup.navetfoot.core.domain.*;
import org.nddngroup.navetfoot.core.domain.Joueur;
import org.nddngroup.navetfoot.core.service.dto.JoueurDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Joueur} and its DTO {@link JoueurDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface JoueurMapper extends EntityMapper<JoueurDTO, Joueur> {



    default Joueur fromId(Long id) {
        if (id == null) {
            return null;
        }
        Joueur joueur = new Joueur();
        joueur.setId(id);
        return joueur;
    }
}
