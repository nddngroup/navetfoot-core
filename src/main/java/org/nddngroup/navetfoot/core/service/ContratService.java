package org.nddngroup.navetfoot.core.service;

import org.nddngroup.navetfoot.core.service.dto.*;
import org.nddngroup.navetfoot.core.domain.Contrat;
import org.nddngroup.navetfoot.core.service.dto.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Contrat}.
 */
public interface ContratService {

    /**
     * Save a contrat.
     *
     * @param contratDTO the entity to save.
     * @return the persisted entity.
     */
    ContratDTO save(ContratDTO contratDTO);

    /**
     * Get all the contrats.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ContratDTO> findAll(Pageable pageable);


    /**
     * Get the "id" contrat.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ContratDTO> findOne(Long id);

    /**
     * Delete the "id" contrat.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the contrat corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ContratDTO> search(String query, Pageable pageable);
    Optional<ContratDTO> findBySaisonAndAssociationAndJoueur(SaisonDTO saisonDTO, AssociationDTO associationDTO, JoueurDTO joueurDTO);
    List<ContratDTO> findBySaisonAndAssociation(SaisonDTO saisonDTO, AssociationDTO associationDTO);
    List<ContratDTO> findByAssociation(AssociationDTO associationDTO);
    List<ContratDTO> findByAssociationAndJoueur(AssociationDTO associationDTO, JoueurDTO joueurDTO);
    List<ContratDTO> findBySaison(SaisonDTO saisonDTO);
    Optional<ContratDTO> findBySaisonAndJoueur(SaisonDTO saisonDTO, JoueurDTO joueurDTO);
    Optional<ContratDTO> findByHashcode(String hashcode);
    List<ContratDTO> findByJoueur(JoueurDTO joueurDTO);
    ContratDTO save(JoueurDTO joueurDTO, AssociationDTO associationDTO, SaisonDTO saisonDTO, OrganisationDTO organisationDTO);
    List<ContratDTO> findAll();
    void reIndex();
}
