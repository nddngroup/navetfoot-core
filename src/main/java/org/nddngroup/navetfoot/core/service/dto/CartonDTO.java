package org.nddngroup.navetfoot.core.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;

import org.nddngroup.navetfoot.core.domain.Carton;
import org.nddngroup.navetfoot.core.domain.enumeration.Couleur;
import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;

/**
 * A DTO for the {@link Carton} entity.
 */
public class CartonDTO implements Serializable {

    private Long id;

    private Couleur couleur;

    private Equipe equipe;

    private String code;

    private String hashcode;

    private ZonedDateTime createdAt;

    private ZonedDateTime updatedAt;

    private Boolean deleted;

    private Integer instant;


    private Long joueurId;

    private Long matchId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Couleur getCouleur() {
        return couleur;
    }

    public void setCouleur(Couleur couleur) {
        this.couleur = couleur;
    }

    public Equipe getEquipe() {
        return equipe;
    }

    public void setEquipe(Equipe equipe) {
        this.equipe = equipe;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getHashcode() {
        return hashcode;
    }

    public void setHashcode(String hashcode) {
        this.hashcode = hashcode;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Integer getInstant() {
        return instant;
    }

    public void setInstant(Integer instant) {
        this.instant = instant;
    }

    public Long getJoueurId() {
        return joueurId;
    }

    public void setJoueurId(Long joueurId) {
        this.joueurId = joueurId;
    }

    public Long getMatchId() {
        return matchId;
    }

    public void setMatchId(Long matchId) {
        this.matchId = matchId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CartonDTO)) {
            return false;
        }

        return id != null && id.equals(((CartonDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CartonDTO{" +
            "id=" + getId() +
            ", couleur='" + getCouleur() + "'" +
            ", equipe='" + getEquipe() + "'" +
            ", code='" + getCode() + "'" +
            ", hashcode='" + getHashcode() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", deleted='" + isDeleted() + "'" +
            ", instant=" + getInstant() +
            ", joueurId=" + getJoueurId() +
            ", matchId=" + getMatchId() +
            "}";
    }
}
