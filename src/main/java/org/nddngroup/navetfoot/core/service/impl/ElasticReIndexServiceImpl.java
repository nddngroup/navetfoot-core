package org.nddngroup.navetfoot.core.service.impl;

import org.nddngroup.navetfoot.core.service.*;
import org.nddngroup.navetfoot.core.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class ElasticReIndexServiceImpl implements ElasticReIndexService {
    @Autowired
    private OrganisationService organisationService;
    @Autowired
    private AffiliationService affiliationService;
    @Autowired
    private AssociationService associationService;
    @Autowired
    private ButService butService;
    @Autowired
    private CartonService cartonService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private CompetitionSaisonCategoryService competitionSaisonCategoryService;
    @Autowired
    private CompetitionSaisonService competitionSaisonService;
    @Autowired
    private CompetitionService competitionService;
    @Autowired
    private ContratService contratService;
    @Autowired
    private CornerService cornerService;
    @Autowired
    private CoupFrancService coupFrancService;
    @Autowired
    private DelegueMatchService delegueMatchService;
    @Autowired
    private DelegueService delegueService;
    @Autowired
    private DepartementService departementService;
    @Autowired
    private DetailFeuilleDeMatchService detailFeuilleDeMatchService;
    @Autowired
    private DetailPouleService detailPouleService;
    @Autowired
    private EtapeCompetitionService etapeCompetitionService;
    @Autowired
    private EtapeService etapeService;
    @Autowired
    private FeuilleDeMatchService feuilleDeMatchService;
    @Autowired
    private HorsJeuService horsJeuService;
    @Autowired
    private JoueurCategoryService joueurCategoryService;
    @Autowired
    private JoueurService joueurService;
    @Autowired
    private MatchService matchService;
    @Autowired
    private ParametrageService parametrageService;
    @Autowired
    private ParticipationService participationService;
    @Autowired
    private PaysService paysService;
    @Autowired
    private PenaltyService penaltyService;
    @Autowired
    private PouleService pouleService;
    @Autowired
    private RegionService regionService;
    @Autowired
    private RemplacementService remplacementService;
    @Autowired
    private SaisonService saisonService;
    @Autowired
    private TirAuButService tirAuButService;
    @Autowired
    private TirService tirService;

    @Override
    @Async
    public void reIndexAll() {
        organisationService.reIndex();
        affiliationService.reIndex();
        associationService.reIndex();
        butService.reIndex();
        cartonService.reIndex();
        categoryService.reIndex();
        competitionSaisonCategoryService.reIndex();
        competitionSaisonService.reIndex();
        competitionService.reIndex();
        contratService.reIndex();
        cornerService.reIndex();
        coupFrancService.reIndex();
        delegueMatchService.reIndex();
        delegueService.reIndex();
        detailFeuilleDeMatchService.reIndex();
        detailPouleService.reIndex();
        etapeCompetitionService.reIndex();
        etapeService.reIndex();
        feuilleDeMatchService.reIndex();
        horsJeuService.reIndex();
        joueurCategoryService.reIndex();
        joueurService.reIndex();
        matchService.reIndex();
        parametrageService.reIndex();
        participationService.reIndex();
        penaltyService.reIndex();
        pouleService.reIndex();
        remplacementService.reIndex();
        saisonService.reIndex();
        tirAuButService.reIndex();
        tirService.reIndex();

        paysService.reIndex();
        regionService.reIndex();
        departementService.reIndex();

    }
}
