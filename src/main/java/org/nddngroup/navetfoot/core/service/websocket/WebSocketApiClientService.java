package org.nddngroup.navetfoot.core.service.websocket;

import org.nddngroup.navetfoot.core.config.rabbitMQ.RabbitMQConfig;
import org.nddngroup.navetfoot.core.domain.websocket.RabbitMQMessage;
import org.nddngroup.navetfoot.core.service.client.WebSocketApiClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WebSocketApiClientService {
    private final Logger log = LoggerFactory.getLogger(WebSocketApiClientService.class);
    private final WebSocketApiClient webSocketApiClient;
    private final RabbitTemplate rabbitTemplate;

    WebSocketApiClientService(WebSocketApiClient webSocketApiClient, RabbitTemplate rabbitTemplate) {
        this.webSocketApiClient = webSocketApiClient;
        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendToClientViaWebSocket(String topic, Object data) {
        try {
            webSocketApiClient.sendToClientViaWebSocket(topic, data);
        } catch (Exception e) {
            log.error("Error sending to websocket (gateway) - topic = {} : {}", topic, e.getMessage());
        }
    }

    public void sendToRabbitMQ(String routingKey, List<String> args, RabbitMQMessage message) {
        String formattedRoutingKey = RabbitMQConfig.formatRoutingKey(routingKey, args);
        sendToRabbitMQ(formattedRoutingKey, message);
    }

    public void sendToRabbitMQ(String routingKey, RabbitMQMessage message) {
        rabbitTemplate.convertAndSend(RabbitMQConfig.EXCHANGE, routingKey, message);
    }
}
