package org.nddngroup.navetfoot.core.service.impl;

import org.nddngroup.navetfoot.core.service.ParametrageService;
import org.nddngroup.navetfoot.core.domain.Parametrage;
import org.nddngroup.navetfoot.core.repository.ParametrageRepository;
import org.nddngroup.navetfoot.core.repository.search.ParametrageSearchRepository;
import org.nddngroup.navetfoot.core.service.dto.ParametrageDTO;
import org.nddngroup.navetfoot.core.service.mapper.ParametrageMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link Parametrage}.
 */
@Service
@Transactional
public class ParametrageServiceImpl implements ParametrageService {

    private final Logger log = LoggerFactory.getLogger(ParametrageServiceImpl.class);

    private final ParametrageRepository parametrageRepository;

    private final ParametrageMapper parametrageMapper;

    private final ParametrageSearchRepository parametrageSearchRepository;

    public ParametrageServiceImpl(ParametrageRepository parametrageRepository, ParametrageMapper parametrageMapper, ParametrageSearchRepository parametrageSearchRepository) {
        this.parametrageRepository = parametrageRepository;
        this.parametrageMapper = parametrageMapper;
        this.parametrageSearchRepository = parametrageSearchRepository;
    }

    @Override
    public ParametrageDTO save(ParametrageDTO parametrageDTO) {
        log.debug("Request to save Parametrage : {}", parametrageDTO);
        Parametrage parametrage = parametrageMapper.toEntity(parametrageDTO);
        parametrage = parametrageRepository.save(parametrage);
        ParametrageDTO result = parametrageMapper.toDto(parametrage);
        parametrageSearchRepository.save(parametrage);
        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ParametrageDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Parametrages");
        return parametrageRepository.findAll(pageable)
            .map(parametrageMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<ParametrageDTO> findOne(Long id) {
        log.debug("Request to get Parametrage : {}", id);
        return parametrageRepository.findById(id)
            .map(parametrageMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Parametrage : {}", id);
        parametrageRepository.deleteById(id);
        parametrageSearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ParametrageDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Parametrages for query {}", query);
        return parametrageSearchRepository.search(queryStringQuery(query), pageable)
            .map(parametrageMapper::toDto);
    }

    @Override
    public List<ParametrageDTO> findAll() {
        return parametrageRepository.findAll().stream()
            .map(parametrageMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public void reIndex() {
        log.info("Request to reindex all Parametrages");
        parametrageSearchRepository.deleteAll();
        findAll().stream().map(parametrageMapper::toEntity).forEach(parametrageSearchRepository::save);
    }
}
