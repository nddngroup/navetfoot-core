package org.nddngroup.navetfoot.core.service.mapper;


import org.nddngroup.navetfoot.core.domain.*;
import org.nddngroup.navetfoot.core.domain.Tir;
import org.nddngroup.navetfoot.core.service.dto.TirDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Tir} and its DTO {@link TirDTO}.
 */
@Mapper(componentModel = "spring", uses = {MatchMapper.class, JoueurMapper.class})
public interface TirMapper extends EntityMapper<TirDTO, Tir> {

    @Mapping(source = "match.id", target = "matchId")
    @Mapping(source = "joueur.id", target = "joueurId")
    TirDTO toDto(Tir tir);

    @Mapping(source = "matchId", target = "match")
    @Mapping(source = "joueurId", target = "joueur")
    Tir toEntity(TirDTO tirDTO);

    default Tir fromId(Long id) {
        if (id == null) {
            return null;
        }
        Tir tir = new Tir();
        tir.setId(id);
        return tir;
    }
}
