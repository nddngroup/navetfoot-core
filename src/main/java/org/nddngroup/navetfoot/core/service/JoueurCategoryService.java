package org.nddngroup.navetfoot.core.service;

import org.nddngroup.navetfoot.core.domain.JoueurCategory;
import org.nddngroup.navetfoot.core.service.dto.JoueurCategoryDTO;

import org.nddngroup.navetfoot.core.service.dto.JoueurDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link JoueurCategory}.
 */
public interface JoueurCategoryService {

    /**
     * Save a joueurCategory.
     *
     * @param joueurCategoryDTO the entity to save.
     * @return the persisted entity.
     */
    JoueurCategoryDTO save(JoueurCategoryDTO joueurCategoryDTO);

    /**
     * Get all the joueurCategories.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<JoueurCategoryDTO> findAll(Pageable pageable);


    /**
     * Get the "id" joueurCategory.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<JoueurCategoryDTO> findOne(Long id);

    /**
     * Delete the "id" joueurCategory.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the joueurCategory corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<JoueurCategoryDTO> search(String query, Pageable pageable);
    Optional<JoueurCategoryDTO> findByJoueur(JoueurDTO joueurDTO);
    List<JoueurCategoryDTO> findAll();
    void reIndex();
}
