package org.nddngroup.navetfoot.core.service.impl;

import org.nddngroup.navetfoot.core.domain.DetailFeuilleDeMatch;
import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;
import org.nddngroup.navetfoot.core.repository.DetailFeuilleDeMatchRepository;
import org.nddngroup.navetfoot.core.repository.search.DetailFeuilleDeMatchSearchRepository;
import org.nddngroup.navetfoot.core.service.DetailFeuilleDeMatchService;
import org.nddngroup.navetfoot.core.service.dto.DetailFeuilleDeMatchDTO;
import org.nddngroup.navetfoot.core.service.dto.FeuilleDeMatchDTO;
import org.nddngroup.navetfoot.core.service.dto.JoueurDTO;
import org.nddngroup.navetfoot.core.service.mapper.DetailFeuilleDeMatchMapper;
import org.nddngroup.navetfoot.core.service.mapper.FeuilleDeMatchMapper;
import org.nddngroup.navetfoot.core.service.mapper.JoueurMapper;
import org.nddngroup.navetfoot.core.service.mapper.custom.DetailFeuilleDeMatchStatsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link DetailFeuilleDeMatch}.
 */
@Service
@Transactional
public class DetailFeuilleDeMatchServiceImpl implements DetailFeuilleDeMatchService {

    private final Logger log = LoggerFactory.getLogger(DetailFeuilleDeMatchServiceImpl.class);

    private final DetailFeuilleDeMatchRepository detailFeuilleDeMatchRepository;

    private final DetailFeuilleDeMatchMapper detailFeuilleDeMatchMapper;

    @Autowired
    private DetailFeuilleDeMatchStatsMapper detailFeuilleDeMatchStatsMapper;

    private final DetailFeuilleDeMatchSearchRepository detailFeuilleDeMatchSearchRepository;
    @Autowired
    private FeuilleDeMatchMapper feuilleDeMatchMapper;
    @Autowired
    private JoueurMapper joueurMapper;

    public DetailFeuilleDeMatchServiceImpl(DetailFeuilleDeMatchRepository detailFeuilleDeMatchRepository, DetailFeuilleDeMatchMapper detailFeuilleDeMatchMapper, DetailFeuilleDeMatchSearchRepository detailFeuilleDeMatchSearchRepository) {
        this.detailFeuilleDeMatchRepository = detailFeuilleDeMatchRepository;
        this.detailFeuilleDeMatchMapper = detailFeuilleDeMatchMapper;
        this.detailFeuilleDeMatchSearchRepository = detailFeuilleDeMatchSearchRepository;
    }

    @Override
    public DetailFeuilleDeMatchDTO save(DetailFeuilleDeMatchDTO detailFeuilleDeMatchDTO) {
        log.debug("Request to save DetailFeuilleDeMatch : {}", detailFeuilleDeMatchDTO);
        DetailFeuilleDeMatch detailFeuilleDeMatch = detailFeuilleDeMatchMapper.toEntity(detailFeuilleDeMatchDTO);
        detailFeuilleDeMatch = detailFeuilleDeMatchRepository.save(detailFeuilleDeMatch);
        DetailFeuilleDeMatchDTO result = detailFeuilleDeMatchMapper.toDto(detailFeuilleDeMatch);

        if (detailFeuilleDeMatchDTO.isDeleted() && detailFeuilleDeMatchDTO.getId() != null) {
            detailFeuilleDeMatchSearchRepository.deleteById(detailFeuilleDeMatchDTO.getId());
        } else {
            detailFeuilleDeMatchSearchRepository.save(detailFeuilleDeMatchStatsMapper.toStats(result));
        }

        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<DetailFeuilleDeMatchDTO> findAll(Pageable pageable) {
        log.debug("Request to get all DetailFeuilleDeMatches");
        return detailFeuilleDeMatchRepository.findAll(pageable)
            .map(detailFeuilleDeMatchMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<DetailFeuilleDeMatchDTO> findOne(Long id) {
        log.debug("Request to get DetailFeuilleDeMatch : {}", id);
        return detailFeuilleDeMatchRepository.findById(id)
            .map(detailFeuilleDeMatchMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete DetailFeuilleDeMatch : {}", id);
        detailFeuilleDeMatchRepository.deleteById(id);
        detailFeuilleDeMatchSearchRepository.deleteById(id);
    }

    @Override
    public void removeAll(String hashcode) {
        detailFeuilleDeMatchRepository.findAllByHashcode(hashcode)
            .forEach(detailFeuilleDeMatch -> {
                detailFeuilleDeMatchRepository.delete(detailFeuilleDeMatch);
                detailFeuilleDeMatchSearchRepository.delete(detailFeuilleDeMatch);
            });
    }

    @Override
    @Transactional(readOnly = true)
    public Page<DetailFeuilleDeMatchDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of DetailFeuilleDeMatches for query {}", query);
        return detailFeuilleDeMatchSearchRepository.search(queryStringQuery(query), pageable)
            .map(detailFeuilleDeMatchMapper::toDto);
    }

    @Override
    public List<DetailFeuilleDeMatchDTO> findByFeuilleDeMatch(FeuilleDeMatchDTO feuilleDeMatchDTO) {
        log.debug("Request to get all DetailFeuilleDeMatches by feuille de match {}", feuilleDeMatchDTO);
        return detailFeuilleDeMatchRepository.findAllByFeuilleDeMatchAndDeletedIsFalse(feuilleDeMatchMapper.toEntity(feuilleDeMatchDTO))
            .stream()
            .map(detailFeuilleDeMatchMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public Optional<DetailFeuilleDeMatchDTO> findByFeuilleDeMatchAndJoueur(FeuilleDeMatchDTO feuilleDeMatchDTO, JoueurDTO joueurDTO) {
        log.debug("Request to get all DetailFeuilleDeMatches by feuille de match {} and joueur {}", feuilleDeMatchDTO, joueurDTO);
        return detailFeuilleDeMatchRepository.findAllByFeuilleDeMatchAndJoueurAndDeletedIsFalse(
            feuilleDeMatchMapper.toEntity(feuilleDeMatchDTO),
            joueurMapper.toEntity(joueurDTO))
            .map(detailFeuilleDeMatchMapper::toDto);
    }

    @Override
    public List<DetailFeuilleDeMatchDTO> findByFeuilleDeMatchAndEquipe(FeuilleDeMatchDTO feuilleDeMatchDTO, Equipe equipe) {
        log.debug("Request to get all DetailFeuilleDeMatches by feuille de match and equipe");
        return detailFeuilleDeMatchRepository.findAllByFeuilleDeMatchAndEquipeAndDeletedIsFalse(feuilleDeMatchMapper.toEntity(feuilleDeMatchDTO), equipe)
            .stream()
            .map(detailFeuilleDeMatchMapper::toDto).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<DetailFeuilleDeMatchDTO> findByHashcode(String hashcode) {
        log.debug("Request to get DetailFeuilleDeMatch : {}", hashcode);
        return detailFeuilleDeMatchRepository.findByHashcodeAndDeletedIsFalse(hashcode)
            .map(detailFeuilleDeMatchMapper::toDto);
    }

    @Override
    public List<DetailFeuilleDeMatchDTO> findAll() {
        return detailFeuilleDeMatchRepository.findAllByDeletedIsFalse().stream()
            .map(detailFeuilleDeMatchMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public void reIndex() {
        log.info("Request to reindex all DetailFeuilleDeMatchs");
        detailFeuilleDeMatchSearchRepository.deleteAll();
        findAll().stream().map(detailFeuilleDeMatchStatsMapper::toStats).forEach(detailFeuilleDeMatchSearchRepository::save);
    }
}
