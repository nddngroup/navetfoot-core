package org.nddngroup.navetfoot.core.service.external;


import org.nddngroup.navetfoot.core.domain.custom.CompetitionWithSaisonsAndMatchs;
import org.nddngroup.navetfoot.core.domain.custom.stats.EventsDetail;
import org.nddngroup.navetfoot.core.domain.custom.stats.FeuilleDeMatchDetail;
import org.nddngroup.navetfoot.externalapi.model.TeamStatistic;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ExternalApiService {
    List<CompetitionWithSaisonsAndMatchs> getExternalMatchs(String selectedDate);
    EventsDetail getEventsDetails(Long matchId);
    FeuilleDeMatchDetail getExternalClassement(Long matchId);
    List<TeamStatistic> getExternalStats(Long matchId);
    void majFixtures();
}
