package org.nddngroup.navetfoot.core.service;

import org.nddngroup.navetfoot.core.service.dto.*;
import org.nddngroup.navetfoot.core.domain.EtapeCompetition;
import org.nddngroup.navetfoot.core.service.dto.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link EtapeCompetition}.
 */
public interface EtapeCompetitionService {

    /**
     * Save a etapeCompetition.
     *
     * @param etapeCompetitionDTO the entity to save.
     * @return the persisted entity.
     */
    EtapeCompetitionDTO save(EtapeCompetitionDTO etapeCompetitionDTO);

    /**
     * Get all the etapeCompetitions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<EtapeCompetitionDTO> findAll(Pageable pageable);


    /**
     * Get the "id" etapeCompetition.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<EtapeCompetitionDTO> findOne(Long id);

    /**
     * Delete the "id" etapeCompetition.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the etapeCompetition corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<EtapeCompetitionDTO> search(String query, Pageable pageable);
    List<EtapeCompetitionDTO> findByCompetitionSaison(CompetitionSaisonDTO competitionSaisonDTO);
    List<EtapeCompetitionDTO> findByCompetitionSaisonAndCompetitionSaisonCategory(CompetitionSaisonDTO competitionSaisonDTO, CompetitionSaisonCategoryDTO competitionSaisonCategoryDTO);
    Optional<EtapeCompetitionDTO> findByHashcode(String hashcode);
    List<EtapeCompetitionDTO> findByCompetitionSaisons(List<Long> comeptitionSaisonIds);
    EtapeCompetitionDTO save(EtapeDTO etapeDTO, OrganisationDTO organisationDTO, SaisonDTO saisonDTO, CompetitionDTO competitionDTO, Long categoryId, boolean allerRetour, boolean hasPoule);
    List<EtapeCompetitionDTO> findAll();
    void reIndex();
}
