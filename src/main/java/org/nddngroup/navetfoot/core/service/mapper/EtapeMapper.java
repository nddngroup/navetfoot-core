package org.nddngroup.navetfoot.core.service.mapper;


import org.nddngroup.navetfoot.core.domain.*;
import org.nddngroup.navetfoot.core.domain.Etape;
import org.nddngroup.navetfoot.core.service.dto.EtapeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Etape} and its DTO {@link EtapeDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface EtapeMapper extends EntityMapper<EtapeDTO, Etape> {



    default Etape fromId(Long id) {
        if (id == null) {
            return null;
        }
        Etape etape = new Etape();
        etape.setId(id);
        return etape;
    }
}
