package org.nddngroup.navetfoot.core.service.external.impl;

import org.nddngroup.navetfoot.core.domain.custom.CompetitionWithSaisonsAndMatchs;
import org.nddngroup.navetfoot.core.domain.custom.stats.*;
import org.nddngroup.navetfoot.core.domain.enumeration.*;
import org.nddngroup.navetfoot.core.service.dto.*;
import org.nddngroup.navetfoot.core.service.external.ExternalApiService;
import org.nddngroup.navetfoot.externalapi.commun.FixtureEventType;
import org.nddngroup.navetfoot.externalapi.commun.FixtureShortStatus;
import org.nddngroup.navetfoot.externalapi.model.*;
import org.nddngroup.navetfoot.externalapi.model.elastic.FixtureDetail;
import org.nddngroup.navetfoot.externalapi.service.FixtureService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ExternalApiFootballServiceImpl implements ExternalApiService {
    private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private final DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm");
    private final Logger log = LoggerFactory.getLogger(ExternalApiFootballServiceImpl.class);

    private FixtureService fixtureService;

    ExternalApiFootballServiceImpl(FixtureService fixtureService) {
        this.fixtureService = fixtureService;
    }

    @Override
    public List<CompetitionWithSaisonsAndMatchs> getExternalMatchs(String selectedDate) {
        List<CompetitionWithSaisonsAndMatchs> competitionWithSaisonsAndMatchs = new ArrayList<>();
        List<FixtureDetail> fixtureDetails = fixtureService.getFixtures("*", selectedDate);
        for(var fixtureDetail: fixtureDetails)  {
            addToListMatch(fixtureDetail, competitionWithSaisonsAndMatchs);
        }

        competitionWithSaisonsAndMatchs.forEach(competition -> log.debug("..... {} : {}", competition.getCompetition().getNom(), competition));
        return competitionWithSaisonsAndMatchs;
    }

    @Override
    public EventsDetail getEventsDetails(Long matchId) {
        EventsDetail eventsDetail = new EventsDetail();
        eventsDetail.setCartonsDetail(new ArrayList<>());
        eventsDetail.setRemplacementsDetail(new ArrayList<>());
        eventsDetail.setButsDetail(new ArrayList<>());
        eventsDetail.setPenaltiesDetail(new ArrayList<>());
        eventsDetail.setCornersDetail(new ArrayList<>());
        eventsDetail.setCoupFrancsDetail(new ArrayList<>());
        eventsDetail.setHorsJeusDetail(new ArrayList<>());
        eventsDetail.setTirsDetail(new ArrayList<>());

        fixtureService.getFixtureEventsById(matchId)
            .ifPresent(result -> {
                List<Event> events = (List<Event>) result.get("events");
                Teams teams = (Teams) result.get("teams");

                events.forEach(event -> {
                    switch (event.getType()) {
                        case FixtureEventType.CARD:
                            CartonDTO cartonDTO = new CartonDTO();
                            cartonDTO.setId(1L);
                            cartonDTO.setCode("dqfsqfsqfsqf");
                            cartonDTO.setHashcode("dqfsqfsqfsqf");
                            cartonDTO.setDeleted(false);
                            cartonDTO.setUpdatedAt(ZonedDateTime.now());
                            cartonDTO.setCreatedAt(ZonedDateTime.now());
                            cartonDTO.setEquipe(getEquipe(event.getTeam().getName(), teams));
                            cartonDTO.setJoueurId(event.getPlayer().getId());
                            cartonDTO.setMatchId(matchId);
                            cartonDTO.setCouleur(Couleur.JAUNE);
                            cartonDTO.setInstant(event.getTime().getElapsed() + event.getTime().getExtra());

                            CartonDetail cartonDetail = new CartonDetail();
                            cartonDetail.setCartonDTO(cartonDTO);
                            cartonDetail.setJoueurHashcode(String.valueOf(event.getPlayer().getNumber()));
                            cartonDetail.setPrenomJoueur(event.getPlayer().getName());
                            cartonDetail.setNomJoueur("");

                            eventsDetail.getCartonsDetail().add(cartonDetail);
                            break;
                        case FixtureEventType.GOAL:
                            ButDTO butDTO = new ButDTO();
                            butDTO.setId(1L);
                            butDTO.setCode("dqfsqfsqfsqf");
                            butDTO.setHashcode("dqfsqfsqfsqf");
                            butDTO.setDeleted(false);
                            butDTO.setUpdatedAt(ZonedDateTime.now());
                            butDTO.setCreatedAt(ZonedDateTime.now());
                            butDTO.setEquipe(getEquipe(event.getTeam().getName(), teams));
                            butDTO.setJoueurId(event.getPlayer().getId());
                            butDTO.setMatchId(matchId);
                            butDTO.setCause(Cause.COURS_JEU);
                            butDTO.setInstant(event.getTime().getElapsed() + event.getTime().getExtra());

                            ButDetail butDetail = new ButDetail();
                            butDetail.setButDTO(butDTO);
                            butDetail.setJoueurHashcode(String.valueOf(event.getPlayer().getNumber()));
                            butDetail.setPrenomJoueur(event.getPlayer().getName());
                            butDetail.setNomJoueur("");

                            eventsDetail.getButsDetail().add(butDetail);
                            break;
                        case FixtureEventType.SUBSTITUTION:
                            RemplacementDTO remplacementDTO = new RemplacementDTO();
                            remplacementDTO.setId(1L);
                            remplacementDTO.setCode("dqfsqfsqfsqf");
                            remplacementDTO.setHashcode("dqfsqfsqfsqf");
                            remplacementDTO.setDeleted(false);
                            remplacementDTO.setUpdatedAt(ZonedDateTime.now());
                            remplacementDTO.setCreatedAt(ZonedDateTime.now());
                            remplacementDTO.setEquipe(getEquipe(event.getTeam().getName(), teams));
                            remplacementDTO.setSortantId(event.getAssist().getId());
                            remplacementDTO.setEntrantId(event.getPlayer().getId());
                            remplacementDTO.setMatchId(matchId);
                            remplacementDTO.setInstant(event.getTime().getElapsed() + event.getTime().getExtra());

                            RemplacementDetail remplacementDetail = new RemplacementDetail();
                            remplacementDetail.setRemplacementDTO(remplacementDTO);
                            remplacementDetail.setEntrantHashcode(String.valueOf(event.getPlayer().getNumber()));
                            remplacementDetail.setPrenomEntrant(event.getPlayer().getName());
                            remplacementDetail.setNomEntrant("");

                            remplacementDetail.setSortantHashcode(String.valueOf(event.getAssist().getNumber()));
                            remplacementDetail.setPrenomSortant(event.getAssist().getName());
                            remplacementDetail.setNomSortant("");

                            eventsDetail.getRemplacementsDetail().add(remplacementDetail);
                            break;
                    }
                });
            });

        return eventsDetail;
    }

    @Override
    public FeuilleDeMatchDetail getExternalClassement(Long matchId) {
        FeuilleDeMatchDetail feuilleDeMatchDetail = new FeuilleDeMatchDetail();
        feuilleDeMatchDetail.setMatchHashcode("sqfmfsqfsfqf");
        feuilleDeMatchDetail.setDetailsFeuilleDeMatchDetail(new ArrayList<>());

        FeuilleDeMatchDTO feuilleDeMatchDTO = new FeuilleDeMatchDTO();
        feuilleDeMatchDTO.setId(1L);
        feuilleDeMatchDTO.setCode("lsdfsffsdfqf");
        feuilleDeMatchDTO.setHashcode("lsdfsffsdfqf");
        feuilleDeMatchDTO.setMatchId(matchId);
        feuilleDeMatchDTO.setDeleted(false);

        feuilleDeMatchDetail.setFeuilleDeMatchDTO(feuilleDeMatchDTO);

        fixtureService.getFixtureLineUpsById(matchId)
            .ifPresent(lineUps -> {
                for(var i = 0; i < lineUps.size(); i++) {
                    var lineUp = lineUps.get(i);
                    int finalI = i;
                    lineUp.getStartXI().forEach(item -> {
                        feuilleDeMatchDetail.getDetailsFeuilleDeMatchDetail().add(createDetailFeuilleDeMatch(item, Position.TITULAIRE, finalI));
                    });
                    lineUp.getSubstitutes().forEach(item -> {
                        feuilleDeMatchDetail.getDetailsFeuilleDeMatchDetail().add(createDetailFeuilleDeMatch(item, Position.SUPPLEANT, finalI));
                    });
                };
            });

        return feuilleDeMatchDetail;
    }

    @Override
    public List<TeamStatistic> getExternalStats(Long matchId) {
        Optional<List<TeamStatistic>> teamStatistics = fixtureService.getfixtureStatsById(matchId);
        if (teamStatistics.isPresent()) {
            return teamStatistics.get();
        }

        return new ArrayList<>();
    }

    @Override
    public void majFixtures() {
        fixtureService.majFixturesFromFixturesWithLineUpEventsAndStats();
    }

    private void addToListMatch(FixtureDetail fixtureDetail, List<CompetitionWithSaisonsAndMatchs> competitions) {
        CompetitionWithSaisonsAndMatchs competition = competitions.stream()
            .filter(item -> item.getCompetition().getId().equals(fixtureDetail.getLeague().getId()))
            .findFirst().orElseGet(() -> {
                CompetitionWithSaisonsAndMatchs newCcompetition = new CompetitionWithSaisonsAndMatchs();
                CompetitionDTO competitionDTO = new CompetitionDTO();
                competitionDTO.setId(fixtureDetail.getLeague().getId());
                competitionDTO.setNom(fixtureDetail.getLeague().getName());
                competitionDTO.setCode(fixtureDetail.getLeague().getLogo());
                competitionDTO.setDeleted(false);
                competitionDTO.setHashcode("Unknown");
                competitionDTO.setOrganisationId(1L);
                competitionDTO.setOrganisationNom(fixtureDetail.getLeague().getCountry());
                competitionDTO.setCreatedAt(ZonedDateTime.now());
                competitionDTO.setUpdatedAt(ZonedDateTime.now());
                newCcompetition.setCompetition(competitionDTO);

                // create organisation
                OrganisationDTO organisationDTO = new OrganisationDTO();
                organisationDTO.setId(competitionDTO.getOrganisationId());
                organisationDTO.setNom(competitionDTO.getOrganisationNom());
                organisationDTO.setCode("Unknown");
                organisationDTO.setHashcode("Unknown");
                organisationDTO.setDeleted(false);
                organisationDTO.setAdresse("Unknown");
                organisationDTO.setRegionId(1L);
                organisationDTO.setRegionNom("Unknown");
                organisationDTO.setDepartementId(1L);
                organisationDTO.setDepartementNom("Unknown");
                organisationDTO.setPaysId(1L);
                organisationDTO.setPaysNom("Unknown");
                organisationDTO.setUserId(1L);
                organisationDTO.setAdresse("Unknown");
                newCcompetition.setOrganisation(organisationDTO);

                competitions.add(newCcompetition);

                return newCcompetition;
            });

        addSaisonInList(competition, fixtureDetail);

        if (competition.getMatchs() == null) {
            competition.setMatchs(new ArrayList<>());
        }

        MatchDetail matchDetail = new MatchDetail();

        MatchDTO matchDTO = new MatchDTO();
        matchDTO.setId(fixtureDetail.getId());
        ZonedDateTime dateMatch = ZonedDateTime.parse(fixtureDetail.getFixture().getDate());
        matchDTO.setDateMatch(dateMatch.toLocalDate());
        matchDTO.setCoupDEnvoi(dateMatch.format(timeFormatter));
        matchDTO.setEtatMatch(getMatchStatus(fixtureDetail.getFixture().getStatus().getShortt()));
        matchDTO.setDeleted(false);
        matchDTO.setCode("Unknown");
        matchDTO.setHashcode("Unknown");
        matchDTO.setEtapeCompetitionId(1L);

        var journee = getJournee(fixtureDetail.getLeague().getRound());
        if (journee != null) {
            matchDTO.setEtapeCompetitionNom("Championnat");
            matchDTO.setJournee((int) journee);
        } else {
            matchDTO.setEtapeCompetitionNom(fixtureDetail.getLeague().getRound());
        }
        matchDTO.setLocalId(1L);
        matchDTO.setVisiteurId(1L);
        matchDTO.setChrono(String.valueOf(fixtureDetail.getFixture().getStatus().getElapsed()));

        matchDetail.setMatchDTO(matchDTO);

        matchDetail.setLocalNom(fixtureDetail.getTeams().getHome().getName());
        matchDetail.setLocalImage(fixtureDetail.getTeams().getHome().getLogo());
        matchDetail.setVisiteurNom(fixtureDetail.getTeams().getAway().getName());
        matchDetail.setVisiteurImage(fixtureDetail.getTeams().getAway().getLogo());
        matchDetail.setNbButsLocal(fixtureDetail.getGoals().getHome());
        matchDetail.setNbButsVisiteur(fixtureDetail.getGoals().getAway());
        matchDetail.setCompetitionNom(fixtureDetail.getLeague().getName());
        matchDetail.setCompetitionId(fixtureDetail.getLeague().getId());
        matchDetail.setSaisonId(1L);
        matchDetail.setSaisonNom(String.valueOf(fixtureDetail.getLeague().getSeason()));
        matchDetail.setCategoryId(1L);
        matchDetail.setCategoryNom("A");
        matchDetail.setHasTirAuBut(false);
        matchDetail.setExternal(true);

        competition.getMatchs().add(matchDetail);
    }

    private void addSaisonInList(CompetitionWithSaisonsAndMatchs competition, FixtureDetail fixtureDetail) {
        if (competition.getSaisons() == null) {
            competition.setSaisons(new ArrayList<>());
        }

        Optional<SaisonDTO> saisonDTOOptional = competition.getSaisons().stream()
            .filter(saisonDTO -> saisonDTO.getLibelle().equals(String.valueOf(fixtureDetail.getLeague().getSeason())))
            .findFirst();

        if (saisonDTOOptional  == null || saisonDTOOptional.isEmpty()) {
            SaisonDTO saisonDTO = new SaisonDTO();
            saisonDTO.setLibelle(String.valueOf(fixtureDetail.getLeague().getSeason()));
            saisonDTO.setId(1L);
            saisonDTO.setCode("Unknown");
            saisonDTO.setHashcode("Unknown");
            saisonDTO.setOrganisationNom("Unknown");
            saisonDTO.setOrganisationId(1L);
            saisonDTO.setDeleted(false);
            competition.getSaisons().add(saisonDTO);
        }
    }

    private EtatMatch getMatchStatus(String status) {
        switch (status) {
            case FixtureShortStatus.NOT_STARTED:
            case FixtureShortStatus.TIME_TO_BE_DEFINED:
                return EtatMatch.PROGRAMME;
            case FixtureShortStatus.FIST_HALF:
                return EtatMatch.EN_COURS_1;
            case FixtureShortStatus.SECOND_HALF:
                return EtatMatch.EN_COURS_2;
            case FixtureShortStatus.HALF_TIME:
                return EtatMatch.MI_TEMPS_1;
            case FixtureShortStatus.MATCH_FINISHED:
            case FixtureShortStatus.MATCH_FINISHED_AFTER_PENALTY:
            case FixtureShortStatus.MATCH_FINISHED_AFTER_EXTRA_TIME:
                return EtatMatch.TERMINE;
            case FixtureShortStatus.BREAK_TIME:
                return EtatMatch.MI_TEMPS_2;

            default:
                return EtatMatch.PROGRAMME;
        }
    }

    private Object getJournee(String round) {
        var arr = round.split("-");
        if (arr.length == 2) {
            try {
                return Integer.parseInt(arr[1].trim());
            } catch (Exception e) {
                log.error("Error get journee : {}", e.getMessage());
                return null;
            }
        }

        return null;
    }

    private Equipe getEquipe(String nom, Teams teams) {
        if (teams.getHome().getName().equals(nom)) {
            return Equipe.LOCAL;
        }

        if (teams.getAway().getName().equals(nom)) {
            return Equipe.VISITEUR;
        }

        return null;
    }

    private DetailFeuilleDeMatchDetail createDetailFeuilleDeMatch(LineUpPlayer item, Position position, int index) {
        DetailFeuilleDeMatchDetail detailFeuilleDeMatchDetail = new DetailFeuilleDeMatchDetail();
        DetailFeuilleDeMatchDTO detailFeuilleDeMatchDTO = new DetailFeuilleDeMatchDTO();
        detailFeuilleDeMatchDTO.setId(1L);
        detailFeuilleDeMatchDTO.setFeuilleDeMatchId(1L);
        detailFeuilleDeMatchDTO.setCode("sdfsqfqsfdsqf");
        detailFeuilleDeMatchDTO.setHashcode("sdfsqfqsfdsqf");
        detailFeuilleDeMatchDTO.setDeleted(false);
        detailFeuilleDeMatchDTO.setCreatedAt(ZonedDateTime.now());
        detailFeuilleDeMatchDTO.setUpdatedAt(ZonedDateTime.now());
        detailFeuilleDeMatchDTO.setJoueurId(item.getPlayer().getId());
        detailFeuilleDeMatchDTO.setNumeroJoueur(item.getPlayer().getNumber());
        detailFeuilleDeMatchDTO.setPosition(position);
        detailFeuilleDeMatchDTO.setPoste(getPoste(item.getPlayer().getPos()));
        detailFeuilleDeMatchDTO.setGrille(item.getPlayer().getGrid());
        detailFeuilleDeMatchDTO.setEquipe(index == 0 ? Equipe.LOCAL : Equipe.VISITEUR);

        detailFeuilleDeMatchDetail.setDetailFeuilleDeMatchDTO(detailFeuilleDeMatchDTO);
        detailFeuilleDeMatchDetail.setIdentifiant("sdqfsqfsqf");
        detailFeuilleDeMatchDetail.setJoueurHashcode("sdqfsqfsqf");
        detailFeuilleDeMatchDetail.setPrenomJoueur(item.getPlayer().getName());
        detailFeuilleDeMatchDetail.setNomJoueur("");

        return detailFeuilleDeMatchDetail;
    }

    private Poste getPoste(String poste) {
        switch (poste) {
            case "G":
                return Poste.GARDIEN;
            case "D":
                return Poste.DENFENSEUR;
            case "M":
                return Poste.MILIEU;
            case "F":
                return Poste.ATTAQUANT;
            default:
                return Poste.NONE;
        }
    }
}

