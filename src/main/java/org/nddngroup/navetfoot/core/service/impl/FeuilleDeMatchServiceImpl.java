package org.nddngroup.navetfoot.core.service.impl;

import org.nddngroup.navetfoot.core.domain.FeuilleDeMatch;
import org.nddngroup.navetfoot.core.exception.ApiRequestException;
import org.nddngroup.navetfoot.core.exception.ExceptionCode;
import org.nddngroup.navetfoot.core.exception.ExceptionLevel;
import org.nddngroup.navetfoot.core.repository.FeuilleDeMatchRepository;
import org.nddngroup.navetfoot.core.repository.search.FeuilleDeMatchSearchRepository;
import org.nddngroup.navetfoot.core.service.FeuilleDeMatchService;
import org.nddngroup.navetfoot.core.service.custom.CommunService;
import org.nddngroup.navetfoot.core.service.dto.FeuilleDeMatchDTO;
import org.nddngroup.navetfoot.core.service.dto.MatchDTO;
import org.nddngroup.navetfoot.core.service.dto.OrganisationDTO;
import org.nddngroup.navetfoot.core.service.mapper.FeuilleDeMatchMapper;
import org.nddngroup.navetfoot.core.service.mapper.MatchMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link FeuilleDeMatch}.
 */
@Service
@Transactional
public class FeuilleDeMatchServiceImpl implements FeuilleDeMatchService {

    private final Logger log = LoggerFactory.getLogger(FeuilleDeMatchServiceImpl.class);

    private final FeuilleDeMatchRepository feuilleDeMatchRepository;

    private final FeuilleDeMatchMapper feuilleDeMatchMapper;

    private final FeuilleDeMatchSearchRepository feuilleDeMatchSearchRepository;
    @Autowired
    private MatchMapper matchMapper;
    @Autowired
    private CommunService communService;

    public FeuilleDeMatchServiceImpl(FeuilleDeMatchRepository feuilleDeMatchRepository, FeuilleDeMatchMapper feuilleDeMatchMapper, FeuilleDeMatchSearchRepository feuilleDeMatchSearchRepository) {
        this.feuilleDeMatchRepository = feuilleDeMatchRepository;
        this.feuilleDeMatchMapper = feuilleDeMatchMapper;
        this.feuilleDeMatchSearchRepository = feuilleDeMatchSearchRepository;
    }

    @Override
    public FeuilleDeMatchDTO save(FeuilleDeMatchDTO feuilleDeMatchDTO) {
        log.debug("Request to save FeuilleDeMatch : {}", feuilleDeMatchDTO);
        FeuilleDeMatch feuilleDeMatch = feuilleDeMatchMapper.toEntity(feuilleDeMatchDTO);
        feuilleDeMatch = feuilleDeMatchRepository.save(feuilleDeMatch);
        FeuilleDeMatchDTO result = feuilleDeMatchMapper.toDto(feuilleDeMatch);

        if (feuilleDeMatchDTO.isDeleted()) {
            feuilleDeMatchSearchRepository.deleteById(feuilleDeMatchDTO.getId());
        } else {
            feuilleDeMatchSearchRepository.save(feuilleDeMatchMapper.toEntity(result));
        }

        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<FeuilleDeMatchDTO> findAll(Pageable pageable) {
        log.debug("Request to get all FeuilleDeMatches");
        return feuilleDeMatchRepository.findAll(pageable)
            .map(feuilleDeMatchMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<FeuilleDeMatchDTO> findOne(Long id) {
        log.debug("Request to get FeuilleDeMatch : {}", id);
        return feuilleDeMatchRepository.findById(id)
            .map(feuilleDeMatchMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete FeuilleDeMatch : {}", id);
        feuilleDeMatchRepository.deleteById(id);
        feuilleDeMatchSearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<FeuilleDeMatchDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of FeuilleDeMatches for query {}", query);
        return feuilleDeMatchSearchRepository.search(queryStringQuery(query), pageable)
            .map(feuilleDeMatchMapper::toDto);
    }

    @Override
    public Optional<FeuilleDeMatchDTO> findByMatch(MatchDTO matchDTO) {
        log.debug("Request to get FeuilleDeMatch by match : {}", matchDTO.getId());
        return feuilleDeMatchRepository.findOneByMatchAndDeletedIsFalse(matchMapper.toEntity(matchDTO))
            .map(feuilleDeMatchMapper::toDto);
    }

    @Override
    public Optional<FeuilleDeMatchDTO> findByHashcode(String hashcode) {
        log.debug("Request to get feuilleDeMatch by hashcode");
        return feuilleDeMatchRepository.findByHashcodeAndDeletedIsFalse(hashcode)
            .map(feuilleDeMatchMapper::toDto);
    }

    @Override
    public FeuilleDeMatchDTO save(Long matchId, OrganisationDTO organisationDTO) {
        log.debug("Request to save feuilleDeMatch to match {} for organisation : {}", matchId, organisationDTO);
        MatchDTO matchDTO = communService.checkMatch(matchId, organisationDTO);

        if (findByMatch(matchDTO).isPresent()) {
            throw new ApiRequestException(
                ExceptionCode.FEUILLE_DE_MATCH_ALREADY_EXISTS.getMessage(),
                ExceptionCode.FEUILLE_DE_MATCH_ALREADY_EXISTS.getValue(),
                ExceptionLevel.WARNING
            );
        }

        // check feuilleDeMatch
        findByMatch(matchDTO).ifPresent(feuilleDeMatchDTO -> {
            throw new ApiRequestException(
                ExceptionCode.FEUILLE_DE_MATCH_ALREADY_EXISTS.getMessage(),
                ExceptionCode.FEUILLE_DE_MATCH_ALREADY_EXISTS.getValue(),
                ExceptionLevel.WARNING
            );
        });

        FeuilleDeMatchDTO feuilleDeMatchDTO = new FeuilleDeMatchDTO();
        feuilleDeMatchDTO.setMatchId(matchDTO.getId());
        feuilleDeMatchDTO.setCode("REF_" + organisationDTO.getId());

        String toHash = organisationDTO.getId().toString() + matchDTO.getId() + ZonedDateTime.now();
        feuilleDeMatchDTO.setHashcode(communService.toHash(toHash));

        feuilleDeMatchDTO.setDeleted(false);
        feuilleDeMatchDTO.setCreatedAt(ZonedDateTime.now());
        feuilleDeMatchDTO.setUpdatedAt(ZonedDateTime.now());

        /*feuilleDeMatchDTO = save(feuilleDeMatchDTO);

        feuilleDeMatchDTO.setCode(feuilleDeMatchDTO.getCode() + "_" + feuilleDeMatchDTO.getId());
        feuilleDeMatchDTO.setHashcode(this.communService.toHash(feuilleDeMatchDTO.getId().toString()));*/

        return save(feuilleDeMatchDTO);
    }

    @Override
    public List<FeuilleDeMatchDTO> findAll() {
        return feuilleDeMatchRepository.findAllByDeletedIsFalse().stream()
            .map(feuilleDeMatchMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public void reIndex() {
        log.info("Request to reindex all FeuilleDeMatchs");
        feuilleDeMatchSearchRepository.deleteAll();
        findAll().stream().map(feuilleDeMatchMapper::toEntity).forEach(feuilleDeMatchSearchRepository::save);
    }
}
