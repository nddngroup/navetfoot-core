package org.nddngroup.navetfoot.core.service.impl;

import org.nddngroup.navetfoot.core.service.DelegueService;
import org.nddngroup.navetfoot.core.domain.Delegue;
import org.nddngroup.navetfoot.core.repository.DelegueRepository;
import org.nddngroup.navetfoot.core.repository.search.DelegueSearchRepository;
import org.nddngroup.navetfoot.core.service.dto.DelegueDTO;
import org.nddngroup.navetfoot.core.service.mapper.DelegueMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link Delegue}.
 */
@Service
@Transactional
public class DelegueServiceImpl implements DelegueService {

    private final Logger log = LoggerFactory.getLogger(DelegueServiceImpl.class);

    private final DelegueRepository delegueRepository;

    private final DelegueMapper delegueMapper;

    private final DelegueSearchRepository delegueSearchRepository;

    public DelegueServiceImpl(DelegueRepository delegueRepository, DelegueMapper delegueMapper, DelegueSearchRepository delegueSearchRepository) {
        this.delegueRepository = delegueRepository;
        this.delegueMapper = delegueMapper;
        this.delegueSearchRepository = delegueSearchRepository;
    }

    @Override
    public DelegueDTO save(DelegueDTO delegueDTO) {
        log.debug("Request to save Delegue : {}", delegueDTO);
        Delegue delegue = delegueMapper.toEntity(delegueDTO);
        delegue = delegueRepository.save(delegue);
        DelegueDTO result = delegueMapper.toDto(delegue);

        if (delegueDTO.isDeleted()) {
            delegueSearchRepository.deleteById(delegueDTO.getId());
        } else {
            delegueSearchRepository.save(delegueMapper.toEntity(result));
        }

        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<DelegueDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Delegues");
        return delegueRepository.findAll(pageable)
            .map(delegueMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<DelegueDTO> findOne(Long id) {
        log.debug("Request to get Delegue : {}", id);
        return delegueRepository.findById(id)
            .map(delegueMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Delegue : {}", id);
        delegueRepository.deleteById(id);
        delegueSearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<DelegueDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Delegues for query {}", query);
        return delegueSearchRepository.search(queryStringQuery(query), pageable)
            .map(delegueMapper::toDto);
    }

    @Override
    public List<DelegueDTO> findAll() {
        return delegueRepository.findAllByDeletedIsFalse().stream()
            .map(delegueMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public void reIndex() {
        log.info("Request to reindex all Delegue");
        delegueSearchRepository.deleteAll();
        findAll().stream().map(delegueMapper::toEntity).forEach(delegueSearchRepository::save);
    }
}
