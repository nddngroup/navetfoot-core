package org.nddngroup.navetfoot.core.service.mapper.custom;

import org.nddngroup.navetfoot.core.domain.Carton;
import org.nddngroup.navetfoot.core.service.*;
import org.nddngroup.navetfoot.core.service.*;
import org.nddngroup.navetfoot.core.service.dto.CartonDTO;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring")
public abstract class CartonStatsMapper {
    @Autowired
    protected EtapeCompetitionService etapeCompetitionService;
    @Autowired
    protected CompetitionSaisonService competitionSaisonService;
    @Autowired
    protected SaisonService saisonService;
    @Autowired
    protected CompetitionService competitionService;
    @Autowired
    protected OrganisationService organisationService;
    @Autowired
    protected AssociationService associationService;
    @Autowired
    protected MatchService matchService;
    @Autowired
    protected JoueurService joueurService;
    @Autowired
    protected MatchStatsMapper matchStatsMapper;

    public Carton toStats(CartonDTO cartonDTO) {
        var cartonStats = new Carton()
            .code(cartonDTO.getCode())
            .hashcode(cartonDTO.getHashcode())
            .couleur(cartonDTO.getCouleur())
            .equipe(cartonDTO.getEquipe())
            .instant(cartonDTO.getInstant())
            .deleted(cartonDTO.isDeleted())
            .createdAt(cartonDTO.getCreatedAt())
            .updatedAt(cartonDTO.getUpdatedAt());

        cartonStats.setId(cartonDTO.getId());

        // find joueur
        joueurService.findOne(cartonDTO.getJoueurId())
            .ifPresent(joueurDTO -> {
                cartonStats.setJoueurId(joueurDTO.getId());
                cartonStats.setJoueurNom(joueurDTO.getNom());
                cartonStats.setJoueurPrenom(joueurDTO.getPrenom());
                cartonStats.setJoueurImage(joueurDTO.getImageUrl());
            });

        // find match
        matchService.findOne(cartonDTO.getMatchId())
            .ifPresent(matchDTO -> {
                cartonStats.setMatchId(matchDTO.getId());

                var matchStats = matchStatsMapper.toStats(matchDTO);
                cartonStats.setLocalId(matchStats.getLocalId());
                cartonStats.setLocalNom(matchStats.getLocalNom());

                cartonStats.setVisiteurId(matchStats.getVisiteurId());
                cartonStats.setVisiteurNom(matchStats.getVisiteurNom());

                cartonStats.setEtapeCompetitionId(matchStats.getEtapeCompetitionId());
                cartonStats.setEtapeCompetitionNom(matchStats.getEtapeCompetitionNom());

                cartonStats.setSaisonId(matchStats.getSaisonId());
                cartonStats.setSaisonLibelle(matchStats.getSaisonLibelle());

                cartonStats.setCompetitionId(matchStats.getCompetitionId());
                cartonStats.setCompetitionNom(matchStats.getCompetitionNom());

                cartonStats.setOrganisationId(matchStats.getOrganisationId());
                cartonStats.setOrganisationNom(matchStats.getOrganisationNom());
            });

        return cartonStats;
    }
}
