package org.nddngroup.navetfoot.core.service.mapper;


import org.nddngroup.navetfoot.core.domain.*;
import org.nddngroup.navetfoot.core.domain.Affiliation;
import org.nddngroup.navetfoot.core.service.dto.AffiliationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Affiliation} and its DTO {@link AffiliationDTO}.
 */
@Mapper(componentModel = "spring", uses = {AssociationMapper.class, OrganisationMapper.class})
public interface AffiliationMapper extends EntityMapper<AffiliationDTO, Affiliation> {

    @Mapping(source = "association.id", target = "associationId")
    @Mapping(source = "association.nom", target = "associationNom")
    @Mapping(source = "organisation.id", target = "organisationId")
    @Mapping(source = "organisation.nom", target = "organisationNom")
    AffiliationDTO toDto(Affiliation affiliation);

    @Mapping(source = "associationId", target = "association")
    @Mapping(source = "organisationId", target = "organisation")
    Affiliation toEntity(AffiliationDTO affiliationDTO);

    default Affiliation fromId(Long id) {
        if (id == null) {
            return null;
        }
        Affiliation affiliation = new Affiliation();
        affiliation.setId(id);
        return affiliation;
    }
}
