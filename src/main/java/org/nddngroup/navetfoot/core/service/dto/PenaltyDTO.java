package org.nddngroup.navetfoot.core.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;

import org.nddngroup.navetfoot.core.domain.Penalty;
import org.nddngroup.navetfoot.core.domain.enumeration.Issu;
import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;

/**
 * A DTO for the {@link Penalty} entity.
 */
public class PenaltyDTO implements Serializable {

    private Long id;

    private Issu issu;

    private Equipe equipe;

    private String code;

    private String hashcode;

    private ZonedDateTime createdAt;

    private ZonedDateTime updatedAt;

    private Boolean deleted;

    private Integer instant;


    private Long joueurId;

    private Long matchId;

    private Long tirAuButId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Issu getIssu() {
        return issu;
    }

    public void setIssu(Issu issu) {
        this.issu = issu;
    }

    public Equipe getEquipe() {
        return equipe;
    }

    public void setEquipe(Equipe equipe) {
        this.equipe = equipe;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getHashcode() {
        return hashcode;
    }

    public void setHashcode(String hashcode) {
        this.hashcode = hashcode;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Integer getInstant() {
        return instant;
    }

    public void setInstant(Integer instant) {
        this.instant = instant;
    }

    public Long getJoueurId() {
        return joueurId;
    }

    public void setJoueurId(Long joueurId) {
        this.joueurId = joueurId;
    }

    public Long getMatchId() {
        return matchId;
    }

    public void setMatchId(Long matchId) {
        this.matchId = matchId;
    }

    public Long getTirAuButId() {
        return tirAuButId;
    }

    public void setTirAuButId(Long tirAuButId) {
        this.tirAuButId = tirAuButId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PenaltyDTO)) {
            return false;
        }

        return id != null && id.equals(((PenaltyDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PenaltyDTO{" +
            "id=" + getId() +
            ", issu='" + getIssu() + "'" +
            ", equipe='" + getEquipe() + "'" +
            ", code='" + getCode() + "'" +
            ", hashcode='" + getHashcode() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", deleted='" + isDeleted() + "'" +
            ", instant=" + getInstant() +
            ", joueurId=" + getJoueurId() +
            ", matchId=" + getMatchId() +
            ", tirAuButId=" + getTirAuButId() +
            "}";
    }
}
