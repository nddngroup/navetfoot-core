package org.nddngroup.navetfoot.core.service.custom;

import org.apache.commons.codec.digest.DigestUtils;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.nddngroup.navetfoot.core.domain.custom.stats.*;
import org.nddngroup.navetfoot.core.domain.custom.stats.*;
import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;
import org.nddngroup.navetfoot.core.domain.enumeration.Issu;
import org.nddngroup.navetfoot.core.domain.enumeration.Position;
import org.nddngroup.navetfoot.core.exception.ApiRequestException;
import org.nddngroup.navetfoot.core.exception.ExceptionCode;
import org.nddngroup.navetfoot.core.exception.ExceptionLevel;
import org.nddngroup.navetfoot.core.security.jwt.TokenProvider;
import org.nddngroup.navetfoot.core.service.*;
import org.nddngroup.navetfoot.core.service.dto.*;
import org.nddngroup.navetfoot.core.service.*;
import org.nddngroup.navetfoot.core.service.dto.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.boolQuery;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

@Service
@Transactional
public class CommunService {
    private final Logger log = LoggerFactory.getLogger(CommunService.class);

    @Autowired
    private OrganisationService organisationService;
    @Autowired
    private AffiliationService affiliationService;
    @Autowired
    private AssociationService associationService;
    @Autowired
    private JoueurService joueurService;
    @Autowired
    private ContratService contratService;
    @Autowired
    private SaisonService saisonService;
    @Autowired
    private JoueurCategoryService joueurCategoryService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private CompetitionService competitionService;
    @Autowired
    private CompetitionSaisonService competitionSaisonService;
    @Autowired
    private CompetitionSaisonCategoryService competitionSaisonCategoryService;
    @Autowired
    private EtapeService etapeService;
    @Autowired
    private EtapeCompetitionService etapeCompetitionService;
    @Autowired
    private ParticipationService participationService;
    @Autowired
    private MatchService matchService;
    @Autowired
    private ButService butService;
    @Autowired
    private PenaltyService penaltyService;
    @Autowired
    private TirAuButService tirAuButService;
    @Autowired
    private PouleService pouleService;
    @Autowired
    private DetailPouleService detailPouleService;
    @Autowired
    private FeuilleDeMatchService feuilleDeMatchService;
    @Autowired
    private DetailFeuilleDeMatchService detailFeuilleDeMatchService;
    @Autowired
    private CartonService cartonService;
    @Autowired
    private CoupFrancService coupFrancService;
    @Autowired
    private CornerService cornerService;
    @Autowired
    private RemplacementService remplacementService;
    @Autowired
    private HorsJeuService horsJeuService;
    @Autowired
    private TirService tirService;
    @Autowired
    private TokenProvider tokenProvider;


    @Value("${application.image.base-url}")
    private String imagesBaseUrl;
    @Value("${application.image.base-folder}")
    private String imagesBaseFolder;

    public OrganisationDTO checkOrganisation(Long id) {
        return organisationService.findOne(id)
            .orElseThrow(() -> new ApiRequestException(
                ExceptionCode.ORGANISATION_NOT_FOUND.getMessage(),
                ExceptionCode.ORGANISATION_NOT_FOUND.getValue(),
                ExceptionLevel.ERROR
            ));
    }

    public Page<AssociationDTO> findPageAssociationsByOrganisation(OrganisationDTO organisationDTO, Pageable pageable) {
        List<Long> associationIds = new ArrayList<>();

        List<AffiliationDTO> affiliationDTOS = affiliationService.findAllByOrganisation(organisationDTO);

        for (AffiliationDTO affiliationDTO : affiliationDTOS) {
            var associationDTO = associationService.findOne(affiliationDTO.getAssociationId())
                .orElseThrow(
                    () -> new ApiRequestException(
                        ExceptionCode.ASSOCIATIONS_NOT_FOUND.getMessage(),
                        ExceptionCode.ASSOCIATIONS_NOT_FOUND.getValue(),
                        ExceptionLevel.ERROR
                    )
                );

            associationIds.add(associationDTO.getId());
        }

        return associationService.findByIdIn(associationIds, pageable);
    }

    public List<AssociationDTO> findAssociationsByOrganisation(OrganisationDTO organisationDTO) {
        List<AssociationDTO> associationDTOS = new ArrayList<>();

        List<AffiliationDTO> affiliationDTOS = affiliationService.findAllByOrganisation(organisationDTO);

        for (AffiliationDTO affiliationDTO : affiliationDTOS) {
            var associationDTO = associationService.findOne(affiliationDTO.getAssociationId())
                .orElseThrow(
                    () -> new ApiRequestException(
                        ExceptionCode.ASSOCIATIONS_NOT_FOUND.getMessage(),
                        ExceptionCode.ASSOCIATIONS_NOT_FOUND.getValue(),
                        ExceptionLevel.ERROR
                    )
                );

            associationDTOS.add(associationDTO);
        }

        return associationDTOS;
    }

    /**
     * Vérifier si un club est affilié à une organisation donnée
     *
     * @param associationDTO //
     * @param organisationDTO //
     * @return //
     */
    public boolean checkAffiliation(AssociationDTO associationDTO, OrganisationDTO organisationDTO) {
        this.logFunction("checkAffiliation");

        if (associationDTO == null || organisationDTO == null) {
            return false;
        } else {
            // check affiliation
            return this.affiliationService.findByOrganisationAndAssociation(organisationDTO, associationDTO).isPresent();
        }
    }


    /**
     * Vérifier si un joueur a déja un contrat avec un club pour une saison donnée
     *
     * @param joueurDTO //
     * @param associationDTO //
     * @param saisonDTO //
     * @return //
     */
    public boolean hasContrat(JoueurDTO joueurDTO, AssociationDTO associationDTO, SaisonDTO saisonDTO) {
        this.logFunction("hasContrat");

        // check contrat
        return this.contratService.findBySaisonAndAssociationAndJoueur(saisonDTO, associationDTO, joueurDTO).isPresent();
    }


    /**
     * Vérifier si la saison appartient à l'organisation
     *
     * @param saisonId //
     * @param organisationDTO //
     */
    public SaisonDTO checkSaisonForOrganisation(Long saisonId, OrganisationDTO organisationDTO) {
        this.logFunction("checkSaisonForOrganisation");

        // find saison by id
        var saisonDTO = this.saisonService.findOne(saisonId)
            .orElseThrow(
                () -> new ApiRequestException(
                    ExceptionCode.SAISON_NOT_FOUND.getMessage(),
                    ExceptionCode.SAISON_NOT_FOUND.getValue(),
                    ExceptionLevel.ERROR
                )
            );

        // check saison with organisation
        if (!saisonDTO.getOrganisationId().equals(organisationDTO.getId())) {
            log.info("La saison n'appartient pas à cette organisation: organisationId = {}", organisationDTO.getId());

            throw new ApiRequestException(
                ExceptionCode.SAISON_NOT_FOR_ORGANISATION.getMessage(),
                ExceptionCode.SAISON_NOT_FOR_ORGANISATION.getValue(),
                ExceptionLevel.ERROR
            );
        }

        return saisonDTO;
    }


    /**
     * Vérifier si le numéro de license est unique
     *
     * @param joueurDTO //
     * @return //
     */
    public boolean checkUniqueIdentifiant(JoueurDTO joueurDTO) {
        logFunction("checkUniqueIdentifiant");

        JoueurDTO joueurFinded = this.joueurService.findByIdentifiant(joueurDTO.getIdentifiant())
            .orElseGet(() -> null);

        if (joueurDTO.getId() != null) {
            return joueurFinded == null || joueurDTO.getId().equals(joueurFinded.getId());
        } else {
            return joueurFinded == null;
        }
    }

    public CategoryDTO getCategoryByJoueurId(Long joueurId) {
        logFunction("getCategoryByJoueurId");

        // find joueur
        var joueurDTO = joueurService.findOne(joueurId)
            .orElseThrow(
                () -> new ApiRequestException(
                    ExceptionCode.JOUEUR_NOT_FOUND.getMessage(),
                    ExceptionCode.JOUEUR_NOT_FOUND.getValue(),
                    ExceptionLevel.ERROR
                )
            );

        var joueurCategoryDTO = joueurCategoryService.findByJoueur(joueurDTO)
            .orElseThrow(() -> new ApiRequestException(
                ExceptionCode.JOUEUR_CATEGORY_DUPLICATED.getMessage(),
                ExceptionCode.JOUEUR_CATEGORY_DUPLICATED.getValue(),
                ExceptionLevel.ERROR
            ));

        return categoryService.findOne(joueurCategoryDTO.getCategoryId()).orElseGet(() -> null);
    }

    /**
     * Find competitionIds by saison and organisation
     *
     * @param organisationDTO L'organisation
     * @param saisonDTO La saison
     * @return List des ids
     */
    public List<Long> getCompetitionIds(OrganisationDTO organisationDTO, SaisonDTO saisonDTO) {
        List<Long> competitionIds = new ArrayList<>();

        // get competitionSaison
        List<CompetitionSaisonDTO> competitionSaisonDTOS = this.competitionSaisonService.findBySaison(saisonDTO);

        if (competitionSaisonDTOS != null && !competitionSaisonDTOS.isEmpty()) {

            for (CompetitionSaisonDTO competitionSaison: competitionSaisonDTOS) {
                // find competition by id
                var competitionDTO = competitionService.findOne(competitionSaison.getCompetitionId())
                    .orElseThrow(
                        () -> new ApiRequestException(
                            ExceptionCode.COMPETITION_NOT_FOUND.getMessage(),
                            ExceptionCode.COMPETITION_NOT_FOUND.getValue(),
                            ExceptionLevel.ERROR
                        )
                    );

                if (competitionDTO.getOrganisationId().equals(organisationDTO.getId())) {
                    competitionIds.add(competitionSaison.getCompetitionId());
                }
            }
        }

        return competitionIds;
    }

    public List<Long> getSaisonIds(OrganisationDTO organisationDTO, CompetitionDTO competitionDTO) {
        List<Long> saisonIds = new ArrayList<>();

        // get competitionSaison
        List<CompetitionSaisonDTO> competitionSaisonDTOS = this.competitionSaisonService.findByCompetition(competitionDTO);

        if (competitionSaisonDTOS != null && !competitionSaisonDTOS.isEmpty()) {

            for (CompetitionSaisonDTO competitionSaison: competitionSaisonDTOS) {
                // find competition by id
                var saisonDTO = saisonService.findOne(competitionSaison.getSaisonId())
                    .orElseThrow(
                        () -> new ApiRequestException(
                            ExceptionCode.SAISON_NOT_FOUND.getMessage(),
                            ExceptionCode.SAISON_NOT_FOUND.getValue(),
                            ExceptionLevel.ERROR
                        )
                    );

                if (saisonDTO.getOrganisationId().equals(organisationDTO.getId())) {
                    saisonIds.add(competitionSaison.getSaisonId());
                }
            }
        }

        return saisonIds;
    }

    /**
     * Vérifier si la compétition appartient à l'organisation
     *
     * @param competitionId //
     * @param organisationDTO //
     */
    public CompetitionDTO checkCompetitionForOrganisation(Long competitionId, OrganisationDTO organisationDTO) {
        this.logFunction("checkCompetitionForOrganisation");

        // find competition by id
        var competitionDTO = this.competitionService.findOne(competitionId).orElseThrow(
            () -> new ApiRequestException(
                ExceptionCode.COMPETITION_NOT_FOUND.getMessage(),
                ExceptionCode.COMPETITION_NOT_FOUND.getValue(),
                ExceptionLevel.ERROR
            )
        );

        // check competition with organisation
        if (!competitionDTO.getOrganisationId().equals(organisationDTO.getId())) {
            log.info("La compétititon n'appartient pas à cette organisation: organisationId = {}", organisationDTO.getId());

            throw new ApiRequestException(
                ExceptionCode.COMPETITION_NOT_FOR_ORGANISATION.getMessage(),
                ExceptionCode.COMPETITION_NOT_FOR_ORGANISATION.getValue(),
                ExceptionLevel.ERROR
            );
        }

        return competitionDTO;
    }


    /**
     * Recupérer les categories correspondant à une competition pour une saison
     * @param competitionDTO la competition
     * @param saisonDTO la saison
     * @return list of CategoryDTO
     */
    public List<CategoryDTO> getCompetitionSaisonCategories(CompetitionDTO competitionDTO, SaisonDTO saisonDTO) {
        logFunction("getCompetitionSaisonCategories");

        List<CategoryDTO> categoryDTOS = new ArrayList<>();

        // find competition Saison
        List<CompetitionSaisonDTO> competitionSaisonDTOS = competitionSaisonService.findBySaisonAndCompetition(saisonDTO, competitionDTO);
        if (competitionSaisonDTOS == null || competitionSaisonDTOS.isEmpty()) {
            throw new ApiRequestException(
                ExceptionCode.COMPETITION_SAISON_NOT_FOUND.getMessage(),
                ExceptionCode.COMPETITION_SAISON_NOT_FOUND.getValue(),
                ExceptionLevel.ERROR
            );
        }

        if (competitionSaisonDTOS.size() > 1) {
            throw new ApiRequestException(
                ExceptionCode.COMPETITION_SAISON_DUPLICATED.getMessage(),
                ExceptionCode.COMPETITION_SAISON_DUPLICATED.getValue(),
                ExceptionLevel.ERROR
            );
        }

        var competitionSaisonDTO = competitionSaisonDTOS.get(0);

        // find all competitionSaisonCategory
        List<CompetitionSaisonCategoryDTO> competitionSaisonCategoryDTOS = competitionSaisonCategoryService.findByCompetitionSaison(competitionSaisonDTO);

        if (competitionSaisonCategoryDTOS != null && !competitionSaisonCategoryDTOS.isEmpty()) {
            for (CompetitionSaisonCategoryDTO competitionSaisonCategoryDTO : competitionSaisonCategoryDTOS) {
                Long categoryId = competitionSaisonCategoryDTO.getCategoryId();
                // find category by id
                categoryService.findOne(categoryId).ifPresent(categoryDTOS::add);
            }
        }

        return categoryDTOS;
    }

    public CompetitionSaisonDTO checkCategoryForCompetition(Long competitionId, Long saisonId, Long categoryId) {
        logFunction("checkCategoryForCompetition");

        // find competition
        var competitionDTO = competitionService.findOne(competitionId)
            .orElseThrow(
                () -> new ApiRequestException(
                    ExceptionCode.COMPETITION_NOT_FOUND.getMessage(),
                    ExceptionCode.COMPETITION_NOT_FOUND.getValue(),
                    ExceptionLevel.ERROR
                )
            );

        // find competition
        var saisonDTO = saisonService.findOne(saisonId)
            .orElseThrow(
                () -> new ApiRequestException(
                    ExceptionCode.SAISON_NOT_FOUND.getMessage(),
                    ExceptionCode.SAISON_NOT_FOUND.getValue(),
                    ExceptionLevel.ERROR
                )
            );

        // find competitionSaison
        var competitionSaisonDTO = getCompetitionSaison(competitionDTO, saisonDTO);

        List<CategoryDTO> categoryDTOS = this.getCompetitionSaisonCategories(competitionDTO, saisonDTO);

        if (categoryDTOS != null) {
            for (CategoryDTO categoryDTO : categoryDTOS) {
                if (categoryDTO.getId().equals(categoryId)) {
                    throw new ApiRequestException(
                        ExceptionCode.CATEGORY_ALREADY_ADDED_TO_COMPETITION.getMessage(),
                        ExceptionCode.CATEGORY_ALREADY_ADDED_TO_COMPETITION.getValue(),
                        ExceptionLevel.WARNING
                    );
                }
            }
        }

        return competitionSaisonDTO;
    }


    /**
     * Check if etape exists
     *
     * @param etapeId // L'étape
     * @return //
     */
    public EtapeDTO checkEtapeLibelle(Long etapeId) {
        this.logFunction("checkEtapeLibelle");

        // find etape by id
        return this.etapeService.findOne(etapeId)
            .orElseThrow(
                () -> new ApiRequestException(
                    ExceptionCode.ETAPE_NOT_FOUND.getMessage(),
                    ExceptionCode.ETAPE_NOT_FOUND.getValue(),
                    ExceptionLevel.ERROR
                )
            );
    }


    /**
     * Vérifier competitionSaison
     *
     * @param saisonDTO // La saison
     * @param competitionDTO // La compétition
     * @param etapeDTO // L'étape
     * @return //
     */
    public CompetitionSaisonDTO checkCompetitionSaison(SaisonDTO saisonDTO, CompetitionDTO competitionDTO, Long categoryId, EtapeDTO etapeDTO) {
        this.logFunction("checkCompetitionSaison");

        var competitionSaisonDTO = this.getCompetitionSaison(competitionDTO, saisonDTO);

        if (competitionSaisonDTO == null) {
            log.info("La compétition {} n'est pas liée à la saison {}", competitionDTO.getId(), saisonDTO.getId());

            throw new ApiRequestException(
                ExceptionCode.COMPETITION_SAISON_NOT_FOUND.getMessage(),
                ExceptionCode.COMPETITION_SAISON_NOT_FOUND.getValue(),
                ExceptionLevel.ERROR
            );
        }

        if (etapeDTO != null) {
            // get all etapeCompetitions for competitionSaison
            List<EtapeCompetitionDTO> etapeCompetitionDTOS = this.findEtapeCompetitonsBySaisonAndCompetitionAndCategory(saisonDTO, competitionDTO, categoryId);

            if (etapeCompetitionDTOS != null && !etapeCompetitionDTOS.isEmpty()) {
                for (EtapeCompetitionDTO etape: etapeCompetitionDTOS) {
                    if (etape.getNom().equals(etapeDTO.getLibelle())) {
                        throw new ApiRequestException(
                            ExceptionCode.ETAPE_EXISTS_IN_COMPETITION_SAISON.getMessage(),
                            ExceptionCode.ETAPE_EXISTS_IN_COMPETITION_SAISON.getValue(),
                            ExceptionLevel.WARNING
                        );
                    }
                }
            }
        }

        return competitionSaisonDTO;
    }


    /**
     * Get all etapeCompetitions by saison and competition
     *
     * @param saisonDTO //
     * @param competitionDTO //
     * @param categoryId //
     * @return //
     */
    public List<EtapeCompetitionDTO> findEtapeCompetitonsBySaisonAndCompetitionAndCategory(SaisonDTO saisonDTO, CompetitionDTO competitionDTO, Long categoryId) {
        this.logFunction("findEtapeCompetitonsBySaisonAndCompetition");

        // find competitionSaison
        var competitionSaisonDTO = this.checkCompetitionSaison(saisonDTO, competitionDTO, categoryId,null);

        // find competitionSaisonCategory
        var competitionSaisonCategoryDTO = this.getCompetitionSaisonCategory(competitionSaisonDTO, categoryId);

        return this.findByCompetitionSaisonAndCategory(competitionSaisonDTO, competitionSaisonCategoryDTO);
    }

    public CompetitionSaisonCategoryDTO getCompetitionSaisonCategory(CompetitionSaisonDTO competitionSaisonDTO, Long categoryId) {
        logFunction("getCompetitionSaisonCategory");

        // find category by id
        var categoryDTO = categoryService.findOne(categoryId)
            .orElseThrow(
                () -> new ApiRequestException(
                    ExceptionCode.CATEGORY_NOT_FOUND.getMessage(),
                    ExceptionCode.CATEGORY_NOT_FOUND.getValue(),
                    ExceptionLevel.ERROR
                )
            );

        if (competitionSaisonDTO == null || categoryDTO == null) {
            throw new ApiRequestException(
                ExceptionCode.COMPETITION_SAISON_CATEGORY_NOT_FOUND.getMessage(),
                ExceptionCode.COMPETITION_SAISON_CATEGORY_NOT_FOUND.getValue(),
                ExceptionLevel.ERROR
            );
        }

        CompetitionSaisonCategoryDTO competitionSaisonCategoryDTO = competitionSaisonCategoryService
            .findByCompetitionSaisonAndCategory(competitionSaisonDTO, categoryDTO)
            .orElseThrow(() -> new ApiRequestException(
                ExceptionCode.COMPETITION_SAISON_CATEGORY_NOT_FOUND.getMessage(),
                ExceptionCode.COMPETITION_SAISON_CATEGORY_NOT_FOUND.getValue(),
                ExceptionLevel.ERROR
            ));


        return competitionSaisonCategoryDTO;
    }

    public List<EtapeCompetitionDTO> findByCompetitionSaisonAndCategory(CompetitionSaisonDTO competitionSaisonDTO,
                                                                        CompetitionSaisonCategoryDTO competitionSaisonCategoryDTO) {
        logFunction("findByCompetitionSaisonAndCategory");
        return etapeCompetitionService.findByCompetitionSaisonAndCompetitionSaisonCategory(competitionSaisonDTO, competitionSaisonCategoryDTO);
    }

    /**
     * Trouver competitionSaison par saison et competition
     *
     * @param competitionDTO CompetitionDTO
     * @param saisonDTO SaisonDTO
     * @return CompetitionSaisonDTO
     */
    public CompetitionSaisonDTO getCompetitionSaison(CompetitionDTO competitionDTO, SaisonDTO saisonDTO) {
        logFunction("getCompetitionSaison");

        List<CompetitionSaisonDTO> competitionSaisonDTOS = competitionSaisonService.findBySaisonAndCompetition(saisonDTO, competitionDTO);

        if (competitionSaisonDTOS == null || competitionSaisonDTOS.isEmpty()) {
            throw new ApiRequestException(
                ExceptionCode.COMPETITION_SAISON_NOT_FOUND.getMessage(),
                ExceptionCode.COMPETITION_SAISON_NOT_FOUND.getValue(),
                ExceptionLevel.ERROR
            );
        }

        if (competitionSaisonDTOS.size() > 1) {
            throw new ApiRequestException(
                ExceptionCode.COMPETITION_SAISON_DUPLICATED.getMessage(),
                ExceptionCode.COMPETITION_SAISON_DUPLICATED.getValue(),
                ExceptionLevel.ERROR
            );
        }

        return competitionSaisonDTOS.get(0);
    }


    /**
     * Check if etapeLibelle is in list of etapeCompetitions
     *
     * @param etapeDTO // L'étape
     * @param etapeCompetitionDTOS // Liste des étapes compétition
     * @return boolean
     */
    public boolean etapeInEtapeCompetition(EtapeDTO etapeDTO, List<EtapeCompetitionDTO> etapeCompetitionDTOS) {
        this.logFunction("etapeInEtapeCompetition");

        if (etapeCompetitionDTOS == null || etapeDTO == null) {
            log.info("Etape non trouvé dans la liste");
            return false;
        }

        for (EtapeCompetitionDTO etapeCompetition: etapeCompetitionDTOS) {
            if (etapeCompetition != null && etapeCompetition.getNom().equalsIgnoreCase(etapeDTO.getLibelle())) {
                log.info("Etape '{}' est dans la liste", etapeDTO.getLibelle());
                return true;
            } else if (etapeCompetition != null) { // on vérifie si l'étape n'est pas dépassée
                Optional<EtapeDTO> etapeDTOOptional = this.etapeService.findByLibelle(etapeCompetition.getNom());

                if (etapeDTOOptional.isPresent() && etapeDTOOptional.get().getNiveau() < etapeDTO.getNiveau()) { // l'étape est dépassée
                    return true;
                }
            }
        }

        log.info("Etape '{}' non trouvé dans la liste", etapeDTO.getLibelle());
        return false;
    }


    /**
     * Vérifier la validité d'une étape competition
     *
     * @param etapeCompetitionId // L'étape competition
     * @param organisationDTO // L'organisation
     * @return //
     */
    public EtapeCompetitionDTO checkEtapeCompetition(Long etapeCompetitionId,
                                                     OrganisationDTO organisationDTO) {
        this.logFunction("checkEtapeCompetition");

        var etapeCompetitionDTO = this.etapeCompetitionService.findOne(etapeCompetitionId)
            .orElseThrow(
                () -> new ApiRequestException(
                    ExceptionCode.ETAPE_NOT_FOUND.getMessage(),
                    ExceptionCode.ETAPE_NOT_FOUND.getValue(),
                    ExceptionLevel.ERROR
                )
            );

        var competitionSaisonDTO = this.competitionSaisonService.findOne(etapeCompetitionDTO.getCompetitionSaisonId())
            .orElseThrow(
                () -> new ApiRequestException(
                    ExceptionCode.COMPETITION_SAISON_NOT_FOUND.getMessage(),
                    ExceptionCode.COMPETITION_SAISON_NOT_FOUND.getValue(),
                    ExceptionLevel.ERROR
                )
            );

        this.checkSaisonForOrganisation(competitionSaisonDTO.getSaisonId(), organisationDTO);

        this.checkCompetitionForOrganisation(competitionSaisonDTO.getCompetitionId(), organisationDTO);

        return etapeCompetitionDTO;
    }


    /**
     * Recupérer les clubs d'une étape donnée
     *
     * @param etapeCompetitionId // L'étape
     * @return //
     */
    public List<AssociationDTO> getAssociationsByEtapeCompetition(Long etapeCompetitionId) {
        this.logFunction("getAssociationsByEtapeCompetition");

        List<AssociationDTO> associationDTOS = new ArrayList<>();

        if (etapeCompetitionId != null) {
            var etapeCompetitionDTO = this.etapeCompetitionService.findOne(etapeCompetitionId)
                .orElseGet(() -> null);

            if (etapeCompetitionDTO != null) {
                List<ParticipationDTO> participationDTOS = this.participationService.findByEtapeCompetition(etapeCompetitionDTO);

                if (participationDTOS != null && !participationDTOS.isEmpty()) {
                    for (ParticipationDTO participation : participationDTOS) {
                        if (participation != null) {
                            var associationOptional = associationService.findOne(participation.getAssociationId());
                            if (associationOptional.isPresent()) {
                                var associationDTO = associationOptional.get();
                                associationDTOS.add(associationDTO);
                            }
                        }
                    }
                }
            }
        }

        return associationDTOS;
    }

    /**
     * Get match details
     *
     * @param matchDTO
     * @return
     */
    public MatchDetail getMatchDetail(MatchDTO matchDTO) {
        var matchDetail = new MatchDetail();

        log.info("_____ MATCH DTO to detail : {}", matchDTO);

        // find etape by id
        var etapeCompetitionOptional = etapeCompetitionService.findOne(matchDTO.getEtapeCompetitionId());
        if (etapeCompetitionOptional.isPresent()) {
            var etapeCompetitionDTO = etapeCompetitionOptional.get();

            // find CompetitionSaison
            var competitionSaisonDTO = competitionSaisonService.findOne(etapeCompetitionDTO.getCompetitionSaisonId())
                .orElseThrow(
                    () -> new ApiRequestException(
                        ExceptionCode.COMPETITION_SAISON_CATEGORY_NOT_FOUND.getMessage(),
                        ExceptionCode.COMPETITION_SAISON_CATEGORY_NOT_FOUND.getValue(),
                        ExceptionLevel.ERROR
                    )
                );

            if (competitionSaisonDTO != null) {

                // find saison
                var saisonOptional = saisonService.findOne(competitionSaisonDTO.getSaisonId());
                if (saisonOptional.isPresent()) {
                    var saisonDTO = saisonOptional.get();

                    matchDetail.setSaisonId(saisonDTO.getId());
                    matchDetail.setSaisonNom(saisonDTO.getLibelle());
                }

                // find competition
                var competitionOptional = competitionService.findOne(competitionSaisonDTO.getCompetitionId());
                if (competitionOptional.isPresent()) {
                    var competitionDTO = competitionOptional.get();

                    matchDetail.setCompetitionId(competitionDTO.getId());
                    matchDetail.setCompetitionNom(competitionDTO.getNom());
                }
            }

            // find competionSaisonCategory
            var competitionSaisonCategoryDTO = competitionSaisonCategoryService.findOne(etapeCompetitionDTO.getCompetitionSaisonCategoryId())
                .orElseThrow(
                    () -> new ApiRequestException(
                        ExceptionCode.COMPETITION_SAISON_CATEGORY_NOT_FOUND.getMessage(),
                        ExceptionCode.COMPETITION_SAISON_CATEGORY_NOT_FOUND.getValue(),
                        ExceptionLevel.ERROR
                    )
                );

            if (competitionSaisonCategoryDTO != null) {
                // find category
                var categoryOptional = categoryService.findOne(competitionSaisonCategoryDTO.getCategoryId());

                if (categoryOptional.isPresent()) {
                    var categoryDTO = categoryOptional.get();

                    matchDetail.setCategoryId(categoryDTO.getId());
                    matchDetail.setCategoryNom(categoryDTO.getLibelle());
                }
            }
        }

        //set local name
        matchDetail.setMatchDTO(matchDTO);

        if (matchDTO.getLocalId() != null) {
            var associationDTO = this.associationService.findOne(matchDTO.getLocalId())
                .orElseThrow(
                    () -> new ApiRequestException(
                        ExceptionCode.ASSOCIATIONS_NOT_FOUND.getMessage(),
                        ExceptionCode.ASSOCIATIONS_NOT_FOUND.getValue(),
                        ExceptionLevel.ERROR
                    )
                );
            matchDetail.setLocalNom(associationDTO.getNom());
            matchDetail.setLocalImage(associationDTO.getCouleurs());
        }

        //set visiteur name
        if (matchDTO.getVisiteurId() != null) {
            var associationDTO = this.associationService.findOne(matchDTO.getVisiteurId())
                .orElseThrow(
                    () -> new ApiRequestException(
                        ExceptionCode.ASSOCIATIONS_NOT_FOUND.getMessage(),
                        ExceptionCode.ASSOCIATIONS_NOT_FOUND.getValue(),
                        ExceptionLevel.ERROR
                    )
                );
            matchDetail.setVisiteurNom(associationDTO.getNom());
            matchDetail.setVisiteurImage(associationDTO.getCouleurs());
        }

        var butsLocal    = 0;
        var butsVisiteur = 0;
        var tirAuButsLocalMarque    = 0;
        var tirAuButsVisiteurMarque = 0;
        var tirAuButsLocalRate    = 0;
        var tirAuButsVisiteurRate = 0;

        List<ButDTO> buts = this.butService.findByMatch(matchDTO);

        if (buts != null) {
            for(ButDTO but: buts) {
                if (Boolean.FALSE.equals(but.isDeleted())) {
                    if(but.getEquipe() == Equipe.LOCAL) {
                        butsLocal++;
                    }
                    else if(but.getEquipe() == Equipe.VISITEUR) {
                        butsVisiteur++;
                    }
                }
            }

            matchDetail.setNbButsLocal(butsLocal);
            matchDetail.setNbButsVisiteur(butsVisiteur);
        }


        // check tirAuBut
        var tirAuButOptional = this.tirAuButService.findOneByMatch(matchDTO);
        if (tirAuButOptional.isPresent()) {
            var tirAuButDTO = tirAuButOptional.get();

            matchDetail.setHasTirAuBut(true);

            // get score for tirAuBut
            List<PenaltyDTO> penaltyDTOS = this.penaltyService.findByTirAuBut(tirAuButDTO);

            if (penaltyDTOS != null && !penaltyDTOS.isEmpty()) {
                for(PenaltyDTO penaltyDTO : penaltyDTOS) {
                    if (penaltyDTO.getIssu().equals(Issu.MARQUE)) {
                        if (penaltyDTO.getEquipe().equals(Equipe.LOCAL)) {
                            tirAuButsLocalMarque++;
                        }
                        else if (penaltyDTO.getEquipe().equals(Equipe.VISITEUR)) {
                            tirAuButsVisiteurMarque++;
                        }
                    } else if (penaltyDTO.getIssu().equals(Issu.RATE)) {
                        if (penaltyDTO.getEquipe().equals(Equipe.LOCAL)) {
                            tirAuButsLocalRate++;
                        }
                        else if (penaltyDTO.getEquipe().equals(Equipe.VISITEUR)) {
                            tirAuButsVisiteurRate++;
                        }
                    }
                }
            }

            matchDetail.setNbTirAuButsLocalMarque(tirAuButsLocalMarque);
            matchDetail.setNbTirAuButsVisiteurMarque(tirAuButsVisiteurMarque);
            matchDetail.setNbTirAuButsLocalRate(tirAuButsLocalRate);
            matchDetail.setNbTirAuButsVisiteurRate(tirAuButsVisiteurRate);
        }
        else {
            matchDetail.setHasTirAuBut(false);
        }

        return matchDetail;
    }

    public List<MatchDetail> getMatchDetailList(EtapeCompetitionDTO etapeCompetitionDTO, int journee) {
        List<MatchDTO> matchDTOS = (journee != 0)
            ? this.matchService.findByEtapeCompetition(etapeCompetitionDTO, journee)
            : this.matchService.findByEtapeCompetition(etapeCompetitionDTO);

        List<MatchDetail> matchDetails = new ArrayList<>();

        if (matchDTOS != null) {
            matchDetails = matchDTOS.stream().map(this::getMatchDetail).collect(Collectors.toList());
        }


        return matchDetails;
    }

    public List<Long> findJoueurIds(OrganisationDTO organisationDTO, Long saisonId, Long associationId) {
        List<Long> joueurIds = new ArrayList<>();

        // get contrats by this 'saison'
        List<ContratDTO> contratDTOS;
        SaisonDTO saisonDTO = null;
        AssociationDTO associationDTO = null;

        // find association
        if (associationId != null) {
            associationDTO = associationService.findOne(associationId)
                .orElseThrow(
                    () -> new ApiRequestException(
                        ExceptionCode.ASSOCIATIONS_NOT_FOUND.getMessage(),
                        ExceptionCode.ASSOCIATIONS_NOT_FOUND.getValue(),
                        ExceptionLevel.ERROR
                    )
                );
        }

        // find saison
        if (saisonId != null) {
            saisonDTO = saisonService.findOne(saisonId)
                .orElseThrow(
                    () -> new ApiRequestException(
                        ExceptionCode.SAISON_NOT_FOUND.getMessage(),
                        ExceptionCode.SAISON_NOT_FOUND.getValue(),
                        ExceptionLevel.ERROR
                    )
                );
        }

        if (saisonId != null || associationId != null) {
            if (saisonId == null) { // on recupère les joueurs de l'association
                contratDTOS = this.contratService.findByAssociation(associationDTO);
            } else if (associationId == null) { // on recupère les joueurs de la saison
                contratDTOS = this.contratService.findBySaison(saisonDTO);
            } else { // on recupère les joueurs de l'association pour cette saison
                contratDTOS = this.contratService.findBySaisonAndAssociation(saisonDTO, associationDTO);
            }
        } else { // on recupére tous les joueurs de l'organisation
            contratDTOS = new ArrayList<>();

            // find all saisons
            List<SaisonDTO> saisonDTOS = this.saisonService.findAllByOrganisation(organisationDTO);

            if (saisonDTOS != null && !saisonDTOS.isEmpty()) {
                for (SaisonDTO saison: saisonDTOS) {
                    if (saison != null) {
                        contratDTOS.addAll(contratService.findBySaison(saison));
                    }
                }
            }
        }

        if (contratDTOS != null && !contratDTOS.isEmpty()) {
            for (ContratDTO contrat: contratDTOS) {
                if (contrat != null) {
                    joueurIds.add(contrat.getJoueurId());
                }
            }
        }

        return joueurIds;
    }

    /**
     * Recupérer les clubs non ajoutés à une étape
     *
     * @param etapeCompetitionDTO // L'étape competition
     * @param organisationDTO // L'organisation
     * @return //
     */
    public List<AssociationDTO> getNotAddedAssociations(EtapeCompetitionDTO etapeCompetitionDTO, OrganisationDTO organisationDTO) {
        this.logFunction("getNotAddedAssociations");

        List<AssociationDTO> toReturns = new ArrayList<>();

        if (etapeCompetitionDTO != null) {
            List<AssociationDTO> associationDTOS   = this.getAssociationsToAdd(etapeCompetitionDTO, organisationDTO);

            log.info("_____ ALL ASSOS {}", associationDTOS);

            // get added association ids
            List<Long> addAssociationIds = new ArrayList<>();
            List<AssociationDTO> addedAssociations = this.getAssociationsByEtapeCompetition(etapeCompetitionDTO.getId());
            for (AssociationDTO item : addedAssociations) {
                if (item != null) {
                    addAssociationIds.add(item.getId());
                }
            }

            // get not added associations
            for (AssociationDTO association : associationDTOS) {
                if (association != null && !addAssociationIds.contains(association.getId())) {
                    toReturns.add(association);
                }
            }
        }

        return toReturns;
    }

    /**
     * Trouver les détails d'une feuille de match
     *
     * @param matchDTO Le match
     * @return FeuilleDeMatchDetail
     */
    public FeuilleDeMatchDetail getFeuilleDeMatch(MatchDTO matchDTO) {
        FeuilleDeMatchDetail feuilleDeMatchDetail = null;

        //get feuille de match
        var feuilleDeMatchDTO = this.feuilleDeMatchService.findByMatch(matchDTO)
            .orElseGet(() -> null);

        if(feuilleDeMatchDTO != null) {

            feuilleDeMatchDetail = new FeuilleDeMatchDetail();

            feuilleDeMatchDetail.setFeuilleDeMatchDTO(feuilleDeMatchDTO);

            //get list detailFeuilleDeMatch
            List<DetailFeuilleDeMatchDTO> detailFeuilleDeMatchDTOS = this.detailFeuilleDeMatchService.findByFeuilleDeMatch(feuilleDeMatchDTO);

            if (matchDTO != null) {
                feuilleDeMatchDetail.setMatchHashcode(matchDTO.getHashcode());
            }

            if(detailFeuilleDeMatchDTOS != null) {
                for(DetailFeuilleDeMatchDTO detailFeuilleDeMatchDTO: detailFeuilleDeMatchDTOS) {
                    var detailFeuilleDeMatchDetail = new DetailFeuilleDeMatchDetail();

                    //get joueur
                    var joueurDTO = this.joueurService.findOne(detailFeuilleDeMatchDTO.getJoueurId())
                        .orElseThrow(
                            () -> new ApiRequestException(
                                ExceptionCode.JOUEUR_NOT_FOUND.getMessage(),
                                ExceptionCode.JOUEUR_NOT_FOUND.getValue(),
                                ExceptionLevel.ERROR
                            )
                        );

                    detailFeuilleDeMatchDetail.setDetailFeuilleDeMatchDTO(detailFeuilleDeMatchDTO);
                    detailFeuilleDeMatchDetail.setPrenomJoueur(joueurDTO.getPrenom());
                    detailFeuilleDeMatchDetail.setNomJoueur(joueurDTO.getNom());
                    detailFeuilleDeMatchDetail.setJoueurHashcode(joueurDTO.getHashcode());
                    detailFeuilleDeMatchDetail.setIdentifiant(joueurDTO.getIdentifiant());
                    detailFeuilleDeMatchDetail.setImageUrlJoueur(joueurDTO.getImageUrl());

                    feuilleDeMatchDetail.getDetailsFeuilleDeMatchDetail().add(detailFeuilleDeMatchDetail);
                }
            }
        }

        return feuilleDeMatchDetail;
    }

    /**
     * Trouver les événements d'un match
     *
     * @param matchId L'id du match
     * @return EventsDetail
     */
    public EventsDetail getEventsDetails(Long matchId) {

        var eventsDetails = new EventsDetail();

        // find match by id
        var matchDTO = matchService.findOne(matchId)
            .orElseThrow(
                () -> new ApiRequestException(
                    ExceptionCode.MATCH_NOT_FOUND.getMessage(),
                    ExceptionCode.MATCH_NOT_FOUND.getValue(),
                    ExceptionLevel.ERROR
                )
            );

        //get all buts
        List<ButDTO> butDTOS = this.butService.findByMatch(matchDTO);
        List<ButDetail> butDetails = new ArrayList<>();
        if (butDTOS != null) {
            for (ButDTO butDTO : butDTOS) {
                var butDetail = new ButDetail();
                //get joueur
                var joueurDTO = this.joueurService.findOne(butDTO.getJoueurId())
                    .orElseThrow(
                        () -> new ApiRequestException(
                            ExceptionCode.JOUEUR_NOT_FOUND.getMessage(),
                            ExceptionCode.JOUEUR_NOT_FOUND.getValue(),
                            ExceptionLevel.ERROR
                        )
                    );

                //set butDetail
                butDetail.setButDTO(butDTO);
                butDetail.setPrenomJoueur(joueurDTO.getPrenom());
                butDetail.setNomJoueur(joueurDTO.getNom());
                butDetail.setJoueurHashcode(joueurDTO.getHashcode());
                butDetail.setImageUrlJoueur(joueurDTO.getImageUrl());

                //add butDetail to list
                butDetails.add(butDetail);
            }
        }

        //get all cartons
        List<CartonDTO> cartonDTOS = this.cartonService.findByMatch(matchDTO);
        List<CartonDetail> cartonDetails = new ArrayList<>();
        if (cartonDTOS != null) {
            for (CartonDTO cartonDTO : cartonDTOS) {
                var cartonDetail = new CartonDetail();
                //get joueur
                var joueurDTO = this.joueurService.findOne(cartonDTO.getJoueurId())
                    .orElseThrow(
                        () -> new ApiRequestException(
                            ExceptionCode.JOUEUR_NOT_FOUND.getMessage(),
                            ExceptionCode.JOUEUR_NOT_FOUND.getValue(),
                            ExceptionLevel.ERROR
                        )
                    );

                //set cartonDetail
                cartonDetail.setCartonDTO(cartonDTO);
                cartonDetail.setPrenomJoueur(joueurDTO.getPrenom());
                cartonDetail.setNomJoueur(joueurDTO.getNom());
                cartonDetail.setJoueurHashcode(joueurDTO.getHashcode());
                cartonDetail.setImageUrlJoueur(joueurDTO.getImageUrl());

                //add cartonDetail to list
                cartonDetails.add(cartonDetail);
            }
        }

        //get all tirs
        List<TirDTO> tirDTOS = this.tirService.findByMatch(matchDTO);
        List<TirDetail> tirDetails = new ArrayList<>();
        if (tirDTOS != null) {
            for (TirDTO tirDTO : tirDTOS) {
                var tirDetail = new TirDetail();
                //get joueur
                var joueurDTO = this.joueurService.findOne(tirDTO.getJoueurId())
                    .orElseThrow(
                        () -> new ApiRequestException(
                            ExceptionCode.JOUEUR_NOT_FOUND.getMessage(),
                            ExceptionCode.JOUEUR_NOT_FOUND.getValue(),
                            ExceptionLevel.ERROR
                        )
                    );

                //set tirDetail
                tirDetail.setTirDTO(tirDTO);
                tirDetail.setPrenomJoueur(joueurDTO.getPrenom());
                tirDetail.setNomJoueur(joueurDTO.getNom());
                tirDetail.setJoueurHashcode(joueurDTO.getHashcode());
                tirDetail.setImageUrlJoueur(joueurDTO.getImageUrl());

                //add tirDetail to list
                tirDetails.add(tirDetail);
            }
        }

        //get all coupFrancs
        List<CoupFrancDTO> coupFrancDTOS = this.coupFrancService.findByMatch(matchDTO);
        List<CoupFrancDetail> coupFrancDetails = new ArrayList<>();
        if (coupFrancDTOS != null) {
            for (CoupFrancDTO coupFrancDTO : coupFrancDTOS) {
                var coupFrancDetail = new CoupFrancDetail();

                coupFrancDetail.setCoupFrancDTO(coupFrancDTO);

                coupFrancDetails.add(coupFrancDetail);
            }

        }

        //get all corners
        List<CornerDTO> cornerDTOS = this.cornerService.findByMatch(matchDTO);
        List<CornerDetail> cornerDetails = new ArrayList<>();
        if (cornerDTOS != null) {
            for (CornerDTO cornerDTO : cornerDTOS) {
                var cornerDetail = new CornerDetail();

                cornerDetail.setCornerDTO(cornerDTO);

                cornerDetails.add(cornerDetail);
            }

        }

        //get all 'horsJeus'
        List<HorsJeuDTO> horsJeuDTOS = this.horsJeuService.findByMatch(matchDTO);
        List<HorsJeuDetail> horsJeuDetails = new ArrayList<>();
        if (horsJeuDTOS != null) {
            for (HorsJeuDTO horsJeuDTO : horsJeuDTOS) {
                var horsJeuDetail = new HorsJeuDetail();

                horsJeuDetail.setHorsJeuDTO(horsJeuDTO);

                horsJeuDetails.add(horsJeuDetail);
            }

        }

        //get all penalties
        List<PenaltyDTO> penaltyDTOS = this.penaltyService.findByMatch(matchDTO);
        List<PenaltyDetail> penaltyDetails = new ArrayList<>();
        if (penaltyDTOS != null) {
            for (PenaltyDTO penaltyDTO : penaltyDTOS) {
                var penaltyDetail = new PenaltyDetail();
                //get joueur
                var joueurDTO = this.joueurService.findOne(penaltyDTO.getJoueurId())
                    .orElseThrow(
                        () -> new ApiRequestException(
                            ExceptionCode.JOUEUR_NOT_FOUND.getMessage(),
                            ExceptionCode.JOUEUR_NOT_FOUND.getValue(),
                            ExceptionLevel.ERROR
                        )
                    );

                //set penaltyDetail
                penaltyDetail.setPenaltyDTO(penaltyDTO);
                penaltyDetail.setPrenomJoueur(joueurDTO.getPrenom());
                penaltyDetail.setNomJoueur(joueurDTO.getNom());
                penaltyDetail.setJoueurHashcode(joueurDTO.getHashcode());
                penaltyDetail.setImageUrlJoueur(joueurDTO.getImageUrl());

                //add penaltyDetail to list
                penaltyDetails.add(penaltyDetail);
            }
        }

        //get all remplacements
        List<RemplacementDTO> remplacementDTOS = this.remplacementService.findByMatch(matchDTO);
        List<RemplacementDetail> remplacementDetails = new ArrayList<>();
        if (remplacementDTOS != null) {
            for (RemplacementDTO remplacementDTO : remplacementDTOS) {
                var remplacementDetail = new RemplacementDetail();
                //get entrant
                JoueurDTO entrant = this.joueurService.findOne(remplacementDTO.getEntrantId())
                    .orElseThrow(
                        () -> new ApiRequestException(
                            ExceptionCode.JOUEUR_NOT_FOUND.getMessage(),
                            ExceptionCode.JOUEUR_NOT_FOUND.getValue(),
                            ExceptionLevel.ERROR
                        )
                    );

                //get sortant
                JoueurDTO sortant = this.joueurService.findOne(remplacementDTO.getSortantId())
                    .orElseThrow(
                        () -> new ApiRequestException(
                            ExceptionCode.JOUEUR_NOT_FOUND.getMessage(),
                            ExceptionCode.JOUEUR_NOT_FOUND.getValue(),
                            ExceptionLevel.ERROR
                        )
                    );

                //set remplacementDetail
                remplacementDetail.setRemplacementDTO(remplacementDTO);
                remplacementDetail.setPrenomEntrant(entrant.getPrenom());
                remplacementDetail.setNomEntrant(entrant.getNom());
                remplacementDetail.setImageUrlEntrant(entrant.getImageUrl());
                remplacementDetail.setEntrantHashcode(entrant.getHashcode());
                remplacementDetail.setPrenomSortant(sortant.getPrenom());
                remplacementDetail.setNomSortant(sortant.getNom());
                remplacementDetail.setSortantHashcode(sortant.getHashcode());
                remplacementDetail.setImageUrlSortant(sortant.getImageUrl());

                //add remplacementDetail to list
                remplacementDetails.add(remplacementDetail);
            }
        }

        //set all events to list to return
        eventsDetails.setButsDetail(butDetails);
        eventsDetails.setCartonsDetail(cartonDetails);
        eventsDetails.setCornersDetail(cornerDetails);
        eventsDetails.setCoupFrancsDetail(coupFrancDetails);
        eventsDetails.setPenaltiesDetail(penaltyDetails);
        eventsDetails.setRemplacementsDetail(remplacementDetails);
        eventsDetails.setHorsJeusDetail(horsJeuDetails);
        eventsDetails.setTirsDetail(tirDetails);

        return eventsDetails;
    }

    /**
     * Recupérer les clubs qu'on peut ajouter à une étape donnée
     *
     * @param etapeCompetitionDTO L'étape
     * @param organisationDTO L'organisation
     * @return //
     */
    public List<AssociationDTO> getAssociationsToAdd(EtapeCompetitionDTO etapeCompetitionDTO, OrganisationDTO organisationDTO) {
        this.logFunction("getAssociationsToAdd");

        List<AssociationDTO> associationDTOS = new ArrayList<>();

        // find previous etape
        if (etapeCompetitionDTO != null) {
            var competitionSaisonDTO = this.competitionSaisonService.findOne(etapeCompetitionDTO.getCompetitionSaisonId())
                .orElseThrow(() -> new ApiRequestException(
                    ExceptionCode.COMPETITION_SAISON_NOT_FOUND.getMessage(),
                    ExceptionCode.COMPETITION_SAISON_NOT_FOUND.getValue(),
                    ExceptionLevel.ERROR
                ));

            var competitionSaisonCategoryDTO = this.competitionSaisonCategoryService.findOne(etapeCompetitionDTO.getCompetitionSaisonCategoryId())
                .orElseThrow(() -> new ApiRequestException(
                    ExceptionCode.COMPETITION_SAISON_CATEGORY_NOT_FOUND.getMessage(),
                    ExceptionCode.COMPETITION_SAISON_CATEGORY_NOT_FOUND.getValue(),
                    ExceptionLevel.ERROR
                ));

            if (competitionSaisonDTO != null) {
                var saisonDTO = saisonService.findOne(competitionSaisonDTO.getSaisonId())
                    .orElseThrow(() -> new ApiRequestException(
                        ExceptionCode.SAISON_NOT_FOUND.getMessage(),
                        ExceptionCode.SAISON_NOT_FOUND.getValue(),
                        ExceptionLevel.ERROR
                    ));
                var competitionDTO = competitionService.findOne(competitionSaisonDTO.getCompetitionId())
                    .orElseThrow(() -> new ApiRequestException(
                        ExceptionCode.COMPETITION_NOT_FOUND.getMessage(),
                        ExceptionCode.COMPETITION_NOT_FOUND.getValue(),
                        ExceptionLevel.ERROR
                    ));
                // get all etapes
                List<EtapeCompetitionDTO> allEtapes = this.findEtapeCompetitonsBySaisonAndCompetitionAndCategory(saisonDTO, competitionDTO, competitionSaisonCategoryDTO.getCategoryId());

                if (allEtapes == null || allEtapes.size() == 1 || allEtapes.get(0).getId().equals(etapeCompetitionDTO.getId())) {
                    return this.getAllAssociations(organisationDTO);
                } else {
                    Long previousId = allEtapes.get(0).getId();

                    for (EtapeCompetitionDTO etape : allEtapes) {
                        if (etape.getId() > previousId && etape.getId() < etapeCompetitionDTO.getId()) {
                            previousId = etape.getId();
                        }
                    }

                    return this.getAssociationsByEtapeCompetition(previousId);
                }
            }
        }

        return associationDTOS;
    }

    /**
     * Vérifier si on peut supprimer un club d'une étape donnée
     *
     * @param associationDTO //
     * @param organisationDTO //
     * @return //
     */
    public boolean canRemoveAssociationFromEtape(AssociationDTO associationDTO, EtapeCompetitionDTO etapeCompetitionDTO, OrganisationDTO organisationDTO) {
        this.logFunction("canRemoveAssociationFromEtape");

        // find all match of etape
        List<MatchDTO> matchDTOS = this.matchService.findByEtapeCompetition(etapeCompetitionDTO);

        if (matchDTOS != null && !matchDTOS.isEmpty()) {
            for (MatchDTO matchDTO : matchDTOS) {
                if (matchDTO.getLocalId().equals(associationDTO.getId()) || matchDTO.getVisiteurId().equals(associationDTO.getId())) {
                    throw new ApiRequestException(
                        ExceptionCode.CAN_NOT_DELETE_ALREADY_HAS_MATCH.getMessage(),
                        ExceptionCode.CAN_NOT_DELETE_ALREADY_HAS_MATCH.getValue(),
                        ExceptionLevel.WARNING
                    );
                }
            }
        }

        return true;
    }


    /**
     * Vérifier si on peut ajouter un club à une étape donnée
     *
     * @param associationDTO //
     * @param organisationDTO //
     * @return //
     */
    public boolean canAddAssociationToEtape(AssociationDTO associationDTO, EtapeCompetitionDTO etapeCompetitionDTO, OrganisationDTO organisationDTO) {
        this.logFunction("canAddAssociationToEtape");

        // Vérifier si le club est affilié à l'organisation
        if (this.affiliationService.findByOrganisationAndAssociation(organisationDTO, associationDTO).isEmpty()) {
            throw new ApiRequestException(
                ExceptionCode.CLUB_NOT_IN_ORGANISATION.getMessage(),
                ExceptionCode.CLUB_NOT_IN_ORGANISATION.getValue(),
                ExceptionLevel.WARNING
            );
        }

        // find participation by etape and associations
        if (this.participationService.findOneByEtapeCompetitionAndAssociation(etapeCompetitionDTO, associationDTO).isPresent()) { // le club est déja ajouté
            throw new ApiRequestException(
                ExceptionCode.PARTICIPATION_ALREADY_EXISTS.getMessage(),
                ExceptionCode.PARTICIPATION_ALREADY_EXISTS.getValue(),
                ExceptionLevel.WARNING
            );
        }

        return true;
    }

    public void checkPoule(PouleDTO pouleDTO) {
        logFunction("checkPoule");

        if (pouleDTO == null) {
            throw new ApiRequestException(
                ExceptionCode.POULE_NOT_FOUND.getMessage(),
                ExceptionCode.POULE_NOT_FOUND.getValue(),
                ExceptionLevel.ERROR
            );
        }

        // find etape
        var etapeCompetitionDTO = this.etapeCompetitionService.findOne(pouleDTO.getEtapeCompetitionId())
            .orElseThrow(
                () -> new ApiRequestException(
                    ExceptionCode.ETAPE_COMPETITION_NOT_FOUND_FOR_POULE.getMessage(),
                    ExceptionCode.ETAPE_COMPETITION_NOT_FOUND_FOR_POULE.getValue(),
                    ExceptionLevel.ERROR
                )
            );

        // find all poules
        List<PouleDTO> pouleDTOS = this.pouleService.findByEtapeCompetition(etapeCompetitionDTO);

        if (pouleDTOS != null && !pouleDTOS.isEmpty()) {
            for (PouleDTO item : pouleDTOS) {
                if (item.getLibelle().equals(pouleDTO.getLibelle()) && !item.getId().equals(pouleDTO.getId())) {
                    throw new ApiRequestException(
                        ExceptionCode.POULE_ALREADY_EXISTS.getMessage(),
                        ExceptionCode.POULE_ALREADY_EXISTS.getValue(),
                        ExceptionLevel.WARNING
                    );
                }
            }
        }
    }


    /**
     * Vérifier si une poule contient des matchs
     *
     * @param pouleDTO //
     * @param etapeCompetitionDTO //
     * @param organisationDTO //
     * @return //
     */
    public boolean checkPouleHasMatch(PouleDTO pouleDTO, EtapeCompetitionDTO etapeCompetitionDTO, OrganisationDTO organisationDTO) {
        this.logFunction("checkPouleHasMatch");

        this.checkEtapeCompetition(etapeCompetitionDTO.getId(), organisationDTO);

        if (pouleDTO != null) {
            // get all details poule
            List<DetailPouleDTO> detailPouleDTOS = this.detailPouleService.findByPoule(pouleDTO);

            if (detailPouleDTOS != null && !detailPouleDTOS.isEmpty()) {
                for(DetailPouleDTO detailPouleDTO: detailPouleDTOS) {
                    // find association
                    var associationDTO = this.associationService.findOne(detailPouleDTO.getAssociationId())
                        .orElseThrow(
                            () -> new ApiRequestException(
                                ExceptionCode.ASSOCIATIONS_NOT_FOUND.getMessage(),
                                ExceptionCode.ASSOCIATIONS_NOT_FOUND.getValue(),
                                ExceptionLevel.ERROR
                            )
                        );

                    if (this.checkAssociationInMatch(associationDTO, etapeCompetitionDTO)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }


    /**
     * Vérifier si un club est déja dans un match
     *
     * @param associationDTO //
     * @param etapeCompetitionDTO //
     * @return //
     */
    public boolean checkAssociationInMatch(AssociationDTO associationDTO, EtapeCompetitionDTO etapeCompetitionDTO) {
        this.logFunction("checkAssociationInMatch");

        if (etapeCompetitionDTO != null) {
            // find all matchs
            List<MatchDTO> matchDTOS = this.matchService.findByEtapeCompetition(etapeCompetitionDTO);

            if (matchDTOS != null && !matchDTOS.isEmpty()) {
                for(MatchDTO matchDTO : matchDTOS) {
                    if (matchDTO != null) {
                        if (matchDTO.getLocalId().equals(associationDTO.getId()) || matchDTO.getVisiteurId().equals(associationDTO.getId())) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }


    /**
     * Check match is valid
     *
     * @param matchId //
     * @param organisationDTO //
     * @return //
     */
    public MatchDTO checkMatch(Long matchId, OrganisationDTO organisationDTO) {
        this.logFunction("checkMatch");

        // find match by id
        var matchDTO = this.matchService.findOne(matchId)
            .orElseThrow(
                () -> new ApiRequestException(
                    ExceptionCode.MATCH_NOT_FOUND.getMessage(),
                    ExceptionCode.MATCH_NOT_FOUND.getValue(),
                    ExceptionLevel.ERROR
                )
            );

        AssociationDTO local    = this.associationService.findOne(matchDTO.getLocalId())
            .orElseThrow(
                () -> new ApiRequestException(
                    ExceptionCode.CLUB_LOCAL_NOT_FOUND_FOR_MATCH.getMessage(),
                    ExceptionCode.CLUB_LOCAL_NOT_FOUND_FOR_MATCH.getValue(),
                    ExceptionLevel.ERROR
                )
            );
        AssociationDTO visiteur = this.associationService.findOne(matchDTO.getVisiteurId())
            .orElseThrow(
                () -> new ApiRequestException(
                    ExceptionCode.CLUB_VISITEUR_NOT_FOUND_FOR_MATCH.getMessage(),
                    ExceptionCode.CLUB_VISITEUR_NOT_FOUND_FOR_MATCH.getValue(),
                    ExceptionLevel.ERROR
                )
            );

        // Vérifier que le club local est affilié à cette organisation
        if (!checkAffiliation(local, organisationDTO)) {
            throw new ApiRequestException(
                ExceptionCode.CLUB_LOCAL_AFFILIATION_NOT_FOUND.getMessage(),
                ExceptionCode.CLUB_LOCAL_AFFILIATION_NOT_FOUND.getValue(),
                ExceptionLevel.ERROR
            );
        }

        // Vérifier que le club visiteur est affilié à cette organisation
        if (!checkAffiliation(visiteur, organisationDTO)) {
            throw new ApiRequestException(
                ExceptionCode.CLUB_VISITEUR_AFFILIATION_NOT_FOUND.getMessage(),
                ExceptionCode.CLUB_VISITEUR_AFFILIATION_NOT_FOUND.getValue(),
                ExceptionLevel.ERROR
            );
        }

        this.checkEtapeCompetition(matchDTO.getEtapeCompetitionId(), organisationDTO);

        return matchDTO;
    }

    /**
     * Recupérer les clubs d'une organisation
     *
     * @param organisationDTO // L'organisation
     * @return //
     */
    public List<AssociationDTO> getAllAssociations(OrganisationDTO organisationDTO) {
        this.logFunction("getAllAssociations");

        List<AssociationDTO> associationDTOS = new ArrayList<>();

        if (organisationDTO != null) {
            List<AffiliationDTO> affiliationDTOS = this.affiliationService.findAllByOrganisation(organisationDTO);

            if (affiliationDTOS != null && !affiliationDTOS.isEmpty()) {
                for (AffiliationDTO affiliationDTO: affiliationDTOS) {
                    if (affiliationDTO != null) {
                        var associationOptional = this.associationService.findOne(affiliationDTO.getAssociationId());
                        if (associationOptional.isPresent()) {
                            associationDTOS.add(associationOptional.get());
                        }
                    }
                }
            }
        }

        return associationDTOS;
    }


    /**
     *
     * @param associationDTO  //
     * @param feuilleDeMatchDTO  //
     * @param matchDTO  //
     * @param organisationDTO  //
     * @return //
     */
    public List<JoueurDetail> getJoueurForClubInFeuilleDeMatch(AssociationDTO associationDTO,
                                                               FeuilleDeMatchDTO feuilleDeMatchDTO,
                                                               MatchDTO matchDTO,
                                                               OrganisationDTO organisationDTO,
                                                               Position position,
                                                               Equipe equipe) {
        this.logFunction("getJoueurForClubInFeuilleDeMatch");

        List<JoueurDetail> joueurDetails = new ArrayList<>();

        // find etape
        var etapeCompetitionDTO = this.etapeCompetitionService.findOne(matchDTO.getEtapeCompetitionId())
            .orElseThrow(
                () -> new ApiRequestException(
                    ExceptionCode.ETAPE_COMPETITION_NOT_FOUND_FOR_MATCH.getMessage(),
                    ExceptionCode.ETAPE_COMPETITION_NOT_FOUND_FOR_MATCH.getValue(),
                    ExceptionLevel.ERROR
                )
            );

        // find competitionSaison
        var competitionSaisonDTO = this.competitionSaisonService.findOne(etapeCompetitionDTO.getCompetitionSaisonId())
            .orElseThrow(
                () -> new ApiRequestException(
                    ExceptionCode.COMPETITION_SAISON_NOT_FOUND_FOR_ETAPE.getMessage(),
                    ExceptionCode.COMPETITION_SAISON_NOT_FOUND_FOR_ETAPE.getValue(),
                    ExceptionLevel.ERROR
                )
            );

        // check saison
        var saisonDTO = this.checkSaisonForOrganisation(competitionSaisonDTO.getSaisonId(), organisationDTO);

        // find all contrats
        List<ContratDTO> contratDTOS = this.contratService.findBySaisonAndAssociation(saisonDTO, associationDTO);

        if (contratDTOS != null && !contratDTOS.isEmpty()) {
            for(ContratDTO contratDTO: contratDTOS) {
                if (contratDTO != null) {
                    var joueurOptional = this.joueurService.findOne(contratDTO.getJoueurId());
                    if (joueurOptional.isPresent()) {
                        var joueurDTO = joueurOptional.get();

                        var joueurDetail = new JoueurDetail();
                        joueurDetail
                            .setId(joueurDTO.getId())
                            .setPrenom(joueurDTO.getPrenom())
                            .setNom(joueurDTO.getNom())
                            .setIdentifiant(joueurDTO.getIdentifiant())
                            .setDateDeNaissance(joueurDTO.getDateDeNaissance())
                            .setLieuDeNaissance(joueurDTO.getLieuDeNaissance())
                        ;

                        // check joueur in feuille de match
                        var detailFeuilleDeMatchDTO = this.getDetailInFeuilleDeMatch(joueurDTO, feuilleDeMatchDTO, equipe);

                        if (detailFeuilleDeMatchDTO != null) {

                            joueurDetail.setNumero(detailFeuilleDeMatchDTO.getNumeroJoueur());
                            joueurDetail.setPosition(detailFeuilleDeMatchDTO.getPosition());

                            if (detailFeuilleDeMatchDTO.getPosition().equals(position)) {
                                joueurDetails.add(joueurDetail);
                            }
                            else {
                                if (position == null || (!position.equals(Position.SUPPLEANT) && !position.equals(Position.TITULAIRE))) {
                                    joueurDetails.add(joueurDetail);
                                }
                            }
                        } else {
                            joueurDetails.add(joueurDetail);
                        }
                    }

                }
            }
        }


        return joueurDetails;
    }

    public FeuilleDeMatchDetail getFeuilleDeMatchDetails(Long matchId) {
        // find match by id
        var matchDTO = matchService.findOne(matchId)
            .orElseThrow(
                () -> new ApiRequestException(
                    ExceptionCode.MATCH_NOT_FOUND.getMessage(),
                    ExceptionCode.MATCH_NOT_FOUND.getValue(),
                    ExceptionLevel.ERROR
                )
            );

        //get feuille de match
        var feuilleDeMatchDTO = this.feuilleDeMatchService.findByMatch(matchDTO)
            .orElseGet(
                () -> null
            );

        FeuilleDeMatchDetail feuilleDeMatchDetail = new FeuilleDeMatchDetail();

        feuilleDeMatchDetail.setFeuilleDeMatchDTO(feuilleDeMatchDTO);

        if(feuilleDeMatchDTO != null) {
            //get list detailFeuilleDeMatch
            List<DetailFeuilleDeMatchDTO> detailFeuilleDeMatchDTOS = this.detailFeuilleDeMatchService.findByFeuilleDeMatch(feuilleDeMatchDTO);

            // get matchHashcode
            if (matchDTO != null) {
                feuilleDeMatchDetail.setMatchHashcode(matchDTO.getHashcode());
            }

            if(detailFeuilleDeMatchDTOS != null) {
                for(DetailFeuilleDeMatchDTO detailFeuilleDeMatchDTO: detailFeuilleDeMatchDTOS) {
                    var detailFeuilleDeMatchDetail = new DetailFeuilleDeMatchDetail();

                    //get joueur
                    var joueurDTO = this.joueurService.findOne(detailFeuilleDeMatchDTO.getJoueurId())
                        .orElseThrow(
                            () -> new ApiRequestException(
                                ExceptionCode.JOUEUR_NOT_FOUND.getMessage(),
                                ExceptionCode.JOUEUR_NOT_FOUND.getValue(),
                                ExceptionLevel.ERROR
                            )
                        );

                    detailFeuilleDeMatchDetail.setDetailFeuilleDeMatchDTO(detailFeuilleDeMatchDTO);
                    detailFeuilleDeMatchDetail.setPrenomJoueur(joueurDTO.getPrenom());
                    detailFeuilleDeMatchDetail.setNomJoueur(joueurDTO.getNom());
                    detailFeuilleDeMatchDetail.setJoueurHashcode(joueurDTO.getHashcode());
                    detailFeuilleDeMatchDetail.setImageUrlJoueur(joueurDTO.getImageUrl());
                    detailFeuilleDeMatchDetail.setIdentifiant(joueurDTO.getIdentifiant());

                    feuilleDeMatchDetail.getDetailsFeuilleDeMatchDetail().add(detailFeuilleDeMatchDetail);
                }
            }
        }

        return feuilleDeMatchDetail;
    }

    /**
     *
     * @param joueurDTO // Le joueur à vérifier
     * @param feuilleDeMatchDTO // La feuille de match de la quelle on vérifie
     * @return // retourne DetailFfeuilleDeMatch
     */
    public DetailFeuilleDeMatchDTO getDetailInFeuilleDeMatch(JoueurDTO joueurDTO, FeuilleDeMatchDTO feuilleDeMatchDTO, Equipe equipe) {
        this.logFunction("getDetailInFeuilleDeMatch");

        // find detailFeuilleDeMatchs
        List<DetailFeuilleDeMatchDTO> detailFeuilleDeMatches = this.detailFeuilleDeMatchService.findByFeuilleDeMatchAndEquipe(feuilleDeMatchDTO, equipe);

        if (detailFeuilleDeMatches != null && !detailFeuilleDeMatches.isEmpty()) {
            for(DetailFeuilleDeMatchDTO detailFeuilleDeMatchDTO: detailFeuilleDeMatches) {
                if (detailFeuilleDeMatchDTO.getJoueurId().equals(joueurDTO.getId())) {
                    return detailFeuilleDeMatchDTO;
                }
            }
        }

        return null;
    }

    /**
     * Vérifier si une categorie est dans la liste de catégories (competition)
     *
     * @param categoryDTO //
     * @param categoryDTOS //
     * @return //
     */
    public boolean checkCategoryExist(CategoryDTO categoryDTO, List<CategoryDTO> categoryDTOS) {
        if (categoryDTOS != null && !categoryDTOS.isEmpty()) {
            for (CategoryDTO item : categoryDTOS) {
                if (categoryDTO.getId().equals(item.getId())) {
                    return true; // la catégorie est dans la liste
                }
            }
        }

        return false;
    }

    /**
     * Vérifier si un club est valide
     *
     * @param organisationDTO L'organisation
     * @param associationDTO Le club
     */
    public void checkAssociation(OrganisationDTO organisationDTO, AssociationDTO associationDTO) {
        // find association by name
        Optional<AssociationDTO> associationDTOOptional = associationService.findByNomAndAdresse(associationDTO.getNom(), associationDTO.getAdresse());

        if (associationDTOOptional.isPresent()) {
            var result = associationDTOOptional.get();

            // find affiliation by organisationId
            if (checkAffiliation(result, organisationDTO)) {
                throw new ApiRequestException(
                    "Un club du même nom et adresse existe déja: nom = " + associationDTO.getNom() + "; adresse = " + associationDTO.getAdresse(),
                    100,
                    ExceptionLevel.ERROR
                );
            }
        }
    }

    /**
     * Get all matchs by organisation
     *
     * @param organisationDTO L'organisation
     * @return List<MatchDTO>
     */
    public List<MatchDTO> getMatchsByOrganisation(OrganisationDTO organisationDTO) {
        // get all saisons
        List<SaisonDTO> saisonDTOS = this.saisonService.findAllByOrganisation(organisationDTO);

        // get all competitions
        List<CompetitionSaisonDTO> competitionSaisonDTOs = this.competitionSaisonService
            .findBySaisons(saisonDTOS.stream().map(SaisonDTO::getId).collect(Collectors.toList()));

        // find all etapes
        List<EtapeCompetitionDTO> etapeCompetitionDTOS = this.etapeCompetitionService
            .findByCompetitionSaisons(competitionSaisonDTOs.stream().map(CompetitionSaisonDTO::getId).collect(Collectors.toList()));

        // find all match
        return this.matchService.findByEtapeCompetitions(etapeCompetitionDTOS);
    }

    /**
     * Ajouter l'unique poule d'un championnant
     *
     * @param etapeCompetitionDTO EtapeCompetitionDTO
     * @return PouleDTO
     */
    public void addPouleForChampionnat(EtapeCompetitionDTO etapeCompetitionDTO, OrganisationDTO organisationDTO) {
        logFunction("addPouleForChampionnat");

        PouleDTO pouleDTO;
        List<Long> associationIds;
        List<PouleDTO> pouleDTOS = pouleService.findByEtapeCompetition(etapeCompetitionDTO);

        // trouver les participation
        associationIds = participationService.findByEtapeCompetition(etapeCompetitionDTO)
            .stream().map(ParticipationDTO::getAssociationId).collect(Collectors.toList());

        if (pouleDTOS != null && pouleDTOS.size() == 1) {
            pouleDTO = pouleDTOS.get(0);
            pouleDTO.setUpdatedAt(ZonedDateTime.now());

        } else if (pouleDTOS != null && pouleDTOS.size() > 1) {
            throw new ApiRequestException(
                ExceptionCode.POULE_NOT_UNIQUE.getMessage(),
                ExceptionCode.POULE_NOT_UNIQUE.getValue(),
                ExceptionLevel.ERROR
            );
        } else {
            pouleDTO = new PouleDTO();

            pouleDTO.setEtapeCompetitionId(etapeCompetitionDTO.getId());
            pouleDTO.setLibelle("championnat");
            pouleDTO.setCreatedAt(ZonedDateTime.now());
            pouleDTO.setUpdatedAt(ZonedDateTime.now());
        }

        int nombreEquipes = associationIds.size();

        // trouver le nombre de journees
        int journees = estPair(nombreEquipes) ? nombreEquipes - 1 : nombreEquipes;
        if (etapeCompetitionDTO.isAllerRetour()) {
            journees *= 2;
        }

        pouleDTO.setNombreEquipe(nombreEquipes);
        pouleDTO.setNombreJournee(journees);

        // find associations
        List<AssociationDTO> associationDTOS = associationService.findByIdIn(associationIds);

        pouleService.save(pouleDTO, associationDTOS, organisationDTO);
    }

    public void updateParticipations(List<Long> toAdd, List<Long> toRemove,
                                     Long etapeId,
                                     EtapeCompetitionDTO etapeCompetitionDTO,
                                     SaisonDTO saisonDTO,
                                     OrganisationDTO organisationDTO) {
        // remove competitions from 'saison'
        if (toRemove != null && toRemove.size() != 0) {
            List<ParticipationDTO> participationDTOSToRemove = new ArrayList<>();

            for (Long id: toRemove) {
                log.debug("______  TO REMOVE = {}", id);
                // find association by id
                AssociationDTO associationDTO = this.associationService.findOne(id)
                    .orElseThrow(
                        () -> new ApiRequestException(
                            ExceptionCode.ASSOCIATIONS_NOT_FOUND.getMessage(),
                            ExceptionCode.ASSOCIATIONS_NOT_FOUND.getValue(),
                            ExceptionLevel.ERROR
                        )
                    );

                if (this.canRemoveAssociationFromEtape(associationDTO, etapeCompetitionDTO, organisationDTO)) {
                    // find participation
                    if (this.participationService.findOneByEtapeCompetitionAndAssociation(etapeCompetitionDTO, associationDTO).isPresent()) {
                        ParticipationDTO participationDTO = this.participationService.findOneByEtapeCompetitionAndAssociation(etapeCompetitionDTO, associationDTO).get();

                        // remove
                        participationDTO.setDeleted(true);
                        participationDTO.setUpdatedAt(ZonedDateTime.now());

                        participationDTOSToRemove.add(participationDTO);
                    }
                }
            }

            this.participationService.deleteAll(participationDTOSToRemove, etapeCompetitionDTO);

        } else {
            log.debug("______  TO REMOVE is empty");
        }

        List<ParticipationDTO> participationDTOSToAdd = null;

        // add association to 'etape'
        if (toAdd != null && toAdd.size() != 0) {
            participationDTOSToAdd = new ArrayList<>();

            for (Long id: toAdd) {
                // find association by id
                AssociationDTO associationDTO = this.associationService.findOne(id)
                    .orElseThrow(
                        () -> new ApiRequestException(
                            ExceptionCode.ASSOCIATIONS_NOT_FOUND.getMessage(),
                            ExceptionCode.ASSOCIATIONS_NOT_FOUND.getValue(),
                            ExceptionLevel.ERROR
                        )
                    );

                if (this.canAddAssociationToEtape(associationDTO, etapeCompetitionDTO, organisationDTO)) {
                    ParticipationDTO participationDTO = new ParticipationDTO();
                    participationDTO.setCreatedAt(ZonedDateTime.now());
                    participationDTO.setUpdatedAt(ZonedDateTime.now());
                    participationDTO.setCode("REF_" + organisationDTO.getId());
                    participationDTO.setHashcode(organisationDTO.getId().toString() + etapeCompetitionDTO.getId() + ZonedDateTime.now());
                    participationDTO.setDeleted(false);
                    participationDTO.setEtapeCompetitionId(etapeId);
                    participationDTO.setAssociationId(associationDTO.getId());
                    participationDTO.setSaisonId(saisonDTO.getId());

                    participationDTOSToAdd.add(participationDTO);
                }
            }

            this.participationService.saveAll(participationDTOSToAdd, etapeCompetitionDTO, organisationDTO);

        } else {
            log.debug("______  TO ADD is empty");
        }

        // vérifier si c'est un championnat
        // find etape by id
        Optional<EtapeDTO> etapeDTOOptional = etapeService.findByLibelle(etapeCompetitionDTO.getNom());

        if (etapeDTOOptional.isPresent()) {
            EtapeDTO etapeDTO = etapeDTOOptional.get();
            if (etapeDTO.getCode().equals("C200")) {
                addPouleForChampionnat(etapeCompetitionDTO, organisationDTO);
            }
        }
    }

    public MatchJournee createMatchJournee(int journee, EtapeCompetitionDTO etapeCompetitionDTO) {
        MatchJournee matchJournee = new MatchJournee();
        matchJournee.setJournee(journee);
        matchJournee.setMatchs(getMatchDetailList(etapeCompetitionDTO, journee));

        return matchJournee;
    }

    /**
     * Ajouter l'image d'un joueur
     * @param file l'image
     * @param joueurDTO Le joueur
     * @return JoueurDTO
     */
    public JoueurDTO addJoueurImage(MultipartFile file, JoueurDTO joueurDTO) {
        if(!file.isEmpty()) {
            String uploadJoueurDirectory = this.imagesBaseFolder + "joueur";

            var fileName = uploadFile(file, uploadJoueurDirectory, joueurDTO.getId().toString());

            // edit joueur
            joueurDTO.setImageUrl(this.imagesBaseUrl + "/joueur/" + fileName);
            joueurDTO.setUpdatedAt(ZonedDateTime.now());

            joueurDTO = joueurService.save(joueurDTO);
        }

        return joueurDTO;
    }

    /**
     * Ajouter l'image d'un club
     * @param file L'image
     * @param clubId L'id du club
     * @return AssociationDTO
     */
    public AssociationDTO addClubImage(MultipartFile file, Long clubId) {
        // find club by id
        var associationDTO = associationService.findOne(clubId)
            .orElseThrow(
                () -> new ApiRequestException(
                    ExceptionCode.ASSOCIATIONS_NOT_FOUND.getMessage(),
                    ExceptionCode.ASSOCIATIONS_NOT_FOUND.getValue(),
                    ExceptionLevel.ERROR
                )
            );

        if(!file.isEmpty()) {
            String uploadClubDirectory = this.imagesBaseFolder + "club";

            String fileName = uploadFile(file, uploadClubDirectory, clubId.toString());

            // edit joueur
            associationDTO.setCouleurs(this.imagesBaseUrl + "/club/" + fileName);
            associationDTO.setUpdatedAt(ZonedDateTime.now());

            associationDTO = associationService.save(associationDTO);
        }

        return associationDTO;
    }

    public String uploadFile(MultipartFile file, String uploadUserDirectory, String id) {
        var fileName = new StringBuilder();

        var formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");

        String newFileName = id + "_" + formatter.format(LocalDateTime.now()) + ".png";

        var fileNameAndPath = Paths.get(uploadUserDirectory, newFileName);
        fileName.append(newFileName);

        try {
            Files.write(fileNameAndPath, file.getBytes());
        }
        catch (IOException e) {
            System.out.println(Arrays.toString(e.getStackTrace()));
        }

        return fileName.toString();
    }

    public String randomNumeric(int size) {
        var code = new StringBuilder();
        for(var i = 0; i < size; i++) {
            int random = (int)(Math.random() * 9 + 1);

            code.append(random);
        }

        return code.toString();
    }

    /**
     * Hash string
     *
     * @param val //
     * @return //
     */
    public String toHash(String val) {
        return DigestUtils.sha1Hex(val);
    }

    /**
     * Vérifier si un nombre est pair
     *
     * @param val Nombre à vérifier
     * @return boolean
     */
    public boolean estPair(int val) {
        return val % 2 == 0;
    }

    /**
     * Log function
     *
     * @param name // Le nom de la fonction
     */
    public void logFunction(String name) {
        this.log.info("*********************** IN FUNCTION : '{}'", name);
    }

    public BoolQueryBuilder organisationQueryBuilder(Long organisationId, String field) {
        return boolQuery()
            .must(
                queryStringQuery(organisationId.toString()).analyzeWildcard(true).field(field)
            )
            .must(
                queryStringQuery("false").analyzeWildcard(true).field("deleted")
            );
    }
}
