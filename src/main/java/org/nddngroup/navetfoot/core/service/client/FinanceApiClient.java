package org.nddngroup.navetfoot.core.service.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "navetfoot-finance")
public interface FinanceApiClient {
    @PostMapping("/api/debit")
    ResponseEntity<Object> debit(
        @RequestParam(name = "organisationId") Long organisationId,
        @RequestBody Long matchId
    );
}
