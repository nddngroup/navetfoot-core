package org.nddngroup.navetfoot.core.service;

import org.nddngroup.navetfoot.core.domain.Remplacement;
import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;
import org.nddngroup.navetfoot.core.service.dto.MatchDTO;
import org.nddngroup.navetfoot.core.service.dto.RemplacementDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Remplacement}.
 */
public interface RemplacementService {

    /**
     * Save a remplacement.
     *
     * @param remplacementDTO the entity to save.
     * @return the persisted entity.
     */
    RemplacementDTO save(RemplacementDTO remplacementDTO);

    /**
     * Get all the remplacements.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<RemplacementDTO> findAll(Pageable pageable);


    /**
     * Get the "id" remplacement.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<RemplacementDTO> findOne(Long id);

    /**
     * Delete the "id" remplacement.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the remplacement corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<RemplacementDTO> search(String query, Pageable pageable);
    List<RemplacementDTO> findByMatch(MatchDTO matchDTO);
    List<RemplacementDTO> findByMatchAndEquipe(MatchDTO matchDTO, Equipe equipe);
    Optional<RemplacementDTO> findByHashcode(String hashcode);
    List<RemplacementDTO> findAll();
    void reIndex();
}
