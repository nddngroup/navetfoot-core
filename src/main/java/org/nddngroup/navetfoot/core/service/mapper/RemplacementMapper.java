package org.nddngroup.navetfoot.core.service.mapper;


import org.nddngroup.navetfoot.core.domain.Remplacement;
import org.nddngroup.navetfoot.core.service.dto.RemplacementDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity {@link Remplacement} and its DTO {@link RemplacementDTO}.
 */
@Mapper(componentModel = "spring", uses = {MatchMapper.class, JoueurMapper.class})
public interface RemplacementMapper extends EntityMapper<RemplacementDTO, Remplacement> {

    @Mapping(source = "match.id", target = "matchId")
    @Mapping(source = "sortant.id", target = "sortantId")
    @Mapping(source = "sortant.identifiant", target = "sortantIdentifiant")
    @Mapping(source = "entrant.id", target = "entrantId")
    @Mapping(source = "entrant.identifiant", target = "entrantIdentifiant")
    RemplacementDTO toDto(Remplacement remplacement);

    @Mapping(source = "matchId", target = "match")
    @Mapping(source = "sortantId", target = "sortant")
    @Mapping(source = "entrantId", target = "entrant")
    Remplacement toEntity(RemplacementDTO remplacementDTO);

    default Remplacement fromId(Long id) {
        if (id == null) {
            return null;
        }
        Remplacement remplacement = new Remplacement();
        remplacement.setId(id);
        return remplacement;
    }
}
