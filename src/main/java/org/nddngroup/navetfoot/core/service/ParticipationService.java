package org.nddngroup.navetfoot.core.service;

import org.nddngroup.navetfoot.core.service.dto.*;
import org.nddngroup.navetfoot.core.domain.Participation;
import org.nddngroup.navetfoot.core.service.dto.AssociationDTO;
import org.nddngroup.navetfoot.core.service.dto.EtapeCompetitionDTO;
import org.nddngroup.navetfoot.core.service.dto.OrganisationDTO;
import org.nddngroup.navetfoot.core.service.dto.ParticipationDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Participation}.
 */
public interface ParticipationService {

    /**
     * Save a participation.
     *
     * @param participationDTO the entity to save.
     * @return the persisted entity.
     */
    ParticipationDTO save(ParticipationDTO participationDTO);

    /**
     * Get all the participations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ParticipationDTO> findAll(Pageable pageable);


    /**
     * Get the "id" participation.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ParticipationDTO> findOne(Long id);

    /**
     * Delete the "id" participation.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the participation corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ParticipationDTO> search(String query, Pageable pageable);
    List<ParticipationDTO> findByEtapeCompetition(EtapeCompetitionDTO etapeCompetitionDTO);
    Optional<ParticipationDTO> findOneByEtapeCompetitionAndAssociation(EtapeCompetitionDTO etapeCompetitionDTO, AssociationDTO associationDTO);
    Optional<ParticipationDTO> findByHashcode(String hashcode);
    void saveAll(List<ParticipationDTO> participationDTOS, EtapeCompetitionDTO etapeCompetitionDTO, OrganisationDTO organisationDTO);
    void deleteAll(List<ParticipationDTO> participationDTOS, EtapeCompetitionDTO etapeCompetitionDTO);
    List<ParticipationDTO> findAll();
    void reIndex();
}
