package org.nddngroup.navetfoot.core.service.mapper.custom;

import org.nddngroup.navetfoot.core.domain.Remplacement;
import org.nddngroup.navetfoot.core.service.*;
import org.nddngroup.navetfoot.core.service.*;
import org.nddngroup.navetfoot.core.service.dto.RemplacementDTO;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring")
public abstract class RemplacementStatsMapper {
    @Autowired
    protected EtapeCompetitionService etapeCompetitionService;
    @Autowired
    protected CompetitionSaisonService competitionSaisonService;
    @Autowired
    protected SaisonService saisonService;
    @Autowired
    protected CompetitionService competitionService;
    @Autowired
    protected OrganisationService organisationService;
    @Autowired
    protected AssociationService associationService;
    @Autowired
    protected MatchService matchService;
    @Autowired
    protected JoueurService joueurService;
    @Autowired
    protected MatchStatsMapper matchStatsMapper;

    public Remplacement toStats(RemplacementDTO remplacement) {
        var remplacementStats = new Remplacement()
            .code(remplacement.getCode())
            .hashcode(remplacement.getHashcode())
            .equipe(remplacement.getEquipe())
            .instant(remplacement.getInstant())
            .deleted(remplacement.isDeleted())
            .createdAt(remplacement.getCreatedAt())
            .updatedAt(remplacement.getUpdatedAt());

        remplacementStats.setId(remplacement.getId());
        // find entrant
        joueurService.findOne(remplacement.getEntrantId())
            .ifPresent(joueurDTO -> {
                remplacementStats.setEntrantId(joueurDTO.getId());
                remplacementStats.setEntrantNom(joueurDTO.getNom());
                remplacementStats.setEntrantPrenom(joueurDTO.getPrenom());
                remplacementStats.setEntrantImage(joueurDTO.getImageUrl());
            });
        // find sortant
        joueurService.findOne(remplacement.getSortantId())
            .ifPresent(joueurDTO -> {
                remplacementStats.setSortantId(joueurDTO.getId());
                remplacementStats.setSortantNom(joueurDTO.getNom());
                remplacementStats.setSortantPrenom(joueurDTO.getPrenom());
                remplacementStats.setSortantImage(joueurDTO.getImageUrl());
            });

        // find match
        matchService.findOne(remplacement.getMatchId())
            .ifPresent(matchDTO -> {
                remplacementStats.setMatchId(matchDTO.getId());

                var matchStats = matchStatsMapper.toStats(matchDTO);
                remplacementStats.setLocalId(matchStats.getLocalId());
                remplacementStats.setLocalNom(matchStats.getLocalNom());

                remplacementStats.setVisiteurId(matchStats.getVisiteurId());
                remplacementStats.setVisiteurNom(matchStats.getVisiteurNom());

                remplacementStats.setEtapeCompetitionId(matchStats.getEtapeCompetitionId());
                remplacementStats.setEtapeCompetitionNom(matchStats.getEtapeCompetitionNom());

                remplacementStats.setSaisonId(matchStats.getSaisonId());
                remplacementStats.setSaisonLibelle(matchStats.getSaisonLibelle());

                remplacementStats.setCompetitionId(matchStats.getCompetitionId());
                remplacementStats.setCompetitionNom(matchStats.getCompetitionNom());

                remplacementStats.setOrganisationId(matchStats.getOrganisationId());
                remplacementStats.setOrganisationNom(matchStats.getOrganisationNom());
            });

        return remplacementStats;
    }
}
