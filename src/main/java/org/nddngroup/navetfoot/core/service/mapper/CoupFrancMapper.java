package org.nddngroup.navetfoot.core.service.mapper;


import org.nddngroup.navetfoot.core.domain.*;
import org.nddngroup.navetfoot.core.domain.CoupFranc;
import org.nddngroup.navetfoot.core.service.dto.CoupFrancDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link CoupFranc} and its DTO {@link CoupFrancDTO}.
 */
@Mapper(componentModel = "spring", uses = {MatchMapper.class})
public interface CoupFrancMapper extends EntityMapper<CoupFrancDTO, CoupFranc> {

    @Mapping(source = "match.id", target = "matchId")
    CoupFrancDTO toDto(CoupFranc coupFranc);

    @Mapping(source = "matchId", target = "match")
    CoupFranc toEntity(CoupFrancDTO coupFrancDTO);

    default CoupFranc fromId(Long id) {
        if (id == null) {
            return null;
        }
        CoupFranc coupFranc = new CoupFranc();
        coupFranc.setId(id);
        return coupFranc;
    }
}
