package org.nddngroup.navetfoot.core.service.mapper;


import org.nddngroup.navetfoot.core.domain.*;
import org.nddngroup.navetfoot.core.domain.DetailFeuilleDeMatch;
import org.nddngroup.navetfoot.core.service.dto.DetailFeuilleDeMatchDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link DetailFeuilleDeMatch} and its DTO {@link DetailFeuilleDeMatchDTO}.
 */
@Mapper(componentModel = "spring", uses = {JoueurMapper.class, FeuilleDeMatchMapper.class})
public interface DetailFeuilleDeMatchMapper extends EntityMapper<DetailFeuilleDeMatchDTO, DetailFeuilleDeMatch> {

    @Mapping(source = "joueur.id", target = "joueurId")
    @Mapping(source = "feuilleDeMatch.id", target = "feuilleDeMatchId")
    DetailFeuilleDeMatchDTO toDto(DetailFeuilleDeMatch detailFeuilleDeMatch);

    @Mapping(source = "joueurId", target = "joueur")
    @Mapping(source = "feuilleDeMatchId", target = "feuilleDeMatch")
    DetailFeuilleDeMatch toEntity(DetailFeuilleDeMatchDTO detailFeuilleDeMatchDTO);

    default DetailFeuilleDeMatch fromId(Long id) {
        if (id == null) {
            return null;
        }
        DetailFeuilleDeMatch detailFeuilleDeMatch = new DetailFeuilleDeMatch();
        detailFeuilleDeMatch.setId(id);
        return detailFeuilleDeMatch;
    }
}
