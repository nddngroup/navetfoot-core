package org.nddngroup.navetfoot.core.service.dto;

import org.nddngroup.navetfoot.core.domain.Region;

import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link Region} entity.
 */
public class RegionDTO implements Serializable {

    private Long id;

    @NotNull
    private String nom;


    private Long paysId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Long getPaysId() {
        return paysId;
    }

    public void setPaysId(Long paysId) {
        this.paysId = paysId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RegionDTO)) {
            return false;
        }

        return id != null && id.equals(((RegionDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RegionDTO{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", paysId=" + getPaysId() +
            "}";
    }
}
