package org.nddngroup.navetfoot.core.service.impl;

import org.elasticsearch.index.query.BoolQueryBuilder;
import org.nddngroup.navetfoot.core.domain.Match;
import org.nddngroup.navetfoot.core.domain.custom.stats.MatchDetail;
import org.nddngroup.navetfoot.core.domain.enumeration.EtatMatch;
import org.nddngroup.navetfoot.core.exception.ApiRequestException;
import org.nddngroup.navetfoot.core.exception.ExceptionCode;
import org.nddngroup.navetfoot.core.exception.ExceptionLevel;
import org.nddngroup.navetfoot.core.repository.MatchRepository;
import org.nddngroup.navetfoot.core.repository.search.MatchSearchRepository;
import org.nddngroup.navetfoot.core.service.AssociationService;
import org.nddngroup.navetfoot.core.service.EtapeCompetitionService;
import org.nddngroup.navetfoot.core.service.MatchService;
import org.nddngroup.navetfoot.core.service.custom.CommunService;
import org.nddngroup.navetfoot.core.service.dto.*;
import org.nddngroup.navetfoot.core.service.dto.AssociationDTO;
import org.nddngroup.navetfoot.core.service.dto.EtapeCompetitionDTO;
import org.nddngroup.navetfoot.core.service.dto.MatchDTO;
import org.nddngroup.navetfoot.core.service.dto.OrganisationDTO;
import org.nddngroup.navetfoot.core.service.mapper.AssociationMapper;
import org.nddngroup.navetfoot.core.service.mapper.EtapeCompetitionMapper;
import org.nddngroup.navetfoot.core.service.mapper.MatchMapper;
import org.nddngroup.navetfoot.core.service.mapper.custom.MatchStatsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link Match}.
 */
@Service
@Transactional
public class MatchServiceImpl implements MatchService {

    private final Logger log = LoggerFactory.getLogger(MatchServiceImpl.class);

    private final MatchRepository matchRepository;

    private final MatchMapper matchMapper;

    private final MatchSearchRepository matchSearchRepository;
    @Autowired
    private AssociationMapper associationMapper;
    @Autowired
    private EtapeCompetitionMapper etapeCompetitionMapper;
    @Autowired
    private CommunService communService;
    @Autowired
    private AssociationService associationService;
    @Autowired
    private EtapeCompetitionService etapeCompetitionService;
    @Autowired
    private MatchStatsMapper matchStatsMapper;

    public MatchServiceImpl(MatchRepository matchRepository, MatchMapper matchMapper, MatchSearchRepository matchSearchRepository) {
        this.matchRepository = matchRepository;
        this.matchMapper = matchMapper;
        this.matchSearchRepository = matchSearchRepository;
    }

    @Override
    public MatchDTO save(MatchDTO matchDTO) {
        log.debug("Request to save Match : {}", matchDTO);
        var match = matchMapper.toEntity(matchDTO);
        match = matchRepository.save(match);
        MatchDTO result = matchMapper.toDto(match);

        if (Boolean.TRUE.equals(matchDTO.isDeleted())) {
            matchSearchRepository.deleteById(result.getId());
        } else {
            matchSearchRepository.save(matchStatsMapper.toStats(result));
        }

        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<MatchDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Matches");
        return matchRepository.findAll(pageable)
            .map(matchMapper::toDto);
    }

    /**
     *  Get all the matches where TirAuBut is {@code null}.
     *  @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<MatchDTO> findAllWhereTirAuButIsNull() {
        log.debug("Request to get all matches where TirAuBut is null");
        return StreamSupport
            .stream(matchRepository.findAll().spliterator(), false)
            .filter(match -> match.getTirAuBut() == null)
            .map(matchMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<MatchDTO> findOne(Long id) {
        log.debug("Request to get Match : {}", id);
        return matchRepository.findById(id)
            .map(matchMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Match : {}", id);
        matchRepository.deleteById(id);
        matchSearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<MatchDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Matches for query {}", query);
        return matchSearchRepository.search(queryStringQuery(query), pageable)
            .map(matchMapper::toDto);
    }

    @Override
    public List<MatchDTO> findByEtapeCompetition(EtapeCompetitionDTO etapeCompetitionDTO) {
        log.debug("Request to get all Matches");
        return matchRepository.findAllByEtapeCompetitionAndDeletedIsFalseOrderByDateMatchAsc(etapeCompetitionMapper.toEntity(etapeCompetitionDTO))
            .stream()
            .map(matchMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public List<MatchDTO> findByEtapeCompetitions(List<EtapeCompetitionDTO> etapeCompetitionDTOS) {
        log.debug("Request to get all Matches by etapeCompetition");
        return matchRepository.findAllByEtapeCompetitionInAndDeletedIsFalse(etapeCompetitionMapper.toEntity(etapeCompetitionDTOS))
            .stream()
            .map(matchMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public Optional<MatchDetail> findById(Long matchId) {
        log.debug("Request to get Match by id : {}", matchId);
        return matchRepository.findById(matchId)
            .map(matchMapper::toDto).map(communService::getMatchDetail);
    }

    @Override
    public Page<MatchDetail> findByIdIn(List<Long> matchIds, Pageable pageable) {
        log.debug("Request to get all Matches by ids");
        Page<Match> result = matchRepository.findAllByIdInAndDeletedIsFalse(pageable, matchIds);
        return result.map(matchMapper::toDto).map(communService::getMatchDetail);
    }

    @Override
    public Page<MatchDetail> findAllByAssociationIdIn(List<Long> clubIds, LocalDate startDate, Pageable pageable) {
        log.debug("Request to get all Matches by ids");
        Page<Match> result = matchRepository.findAllByLocalIdInOrVisiteurIdInAndDeletedIsFalseAndDateMatchAfterOrderByDateMatchAsc(pageable, clubIds, clubIds, startDate);
        return result.map(matchMapper::toDto).map(communService::getMatchDetail);
    }

    @Override
    public Page<MatchDetail> findAllByAssociationIdIn(List<Long> clubIds, Pageable pageable) {
        log.debug("Request to get all Matches by ids");
        Page<Match> result = matchRepository.findAllByLocalIdInOrVisiteurIdInAndDeletedIsFalseOrderByDateMatchAsc(pageable, clubIds, clubIds);
        return result.map(matchMapper::toDto).map(communService::getMatchDetail);
    }

    @Override
    public Page<MatchDetail> findAllMatchDetails(Pageable pageable) {
        log.debug("Request to get all Matches");
        Page<Match> result = matchRepository.findAllByDeletedIsFalseOrderByDateMatchAsc(pageable);
        return result.map(matchMapper::toDto).map(communService::getMatchDetail);
    }

    @Override
    public List<MatchDTO> findByEtapeCompetitionAndLocal(EtapeCompetitionDTO etapeCompetitionDTO, AssociationDTO associationDTO) {
        log.debug("Request to get all Matches");
        return matchRepository.findAllByEtapeCompetitionAndLocalAndDeletedIsFalseOrderByDateMatchAsc(etapeCompetitionMapper.toEntity(etapeCompetitionDTO), associationMapper.toEntity(associationDTO))
            .stream()
            .map(matchMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public List<MatchDTO> findByEtapeCompetitionAndVisiteur(EtapeCompetitionDTO etapeCompetitionDTO, AssociationDTO associationDTO) {
        log.debug("Request to get all Matches");
        return matchRepository.findAllByEtapeCompetitionAndVisiteurAndDeletedIsFalseOrderByDateMatchAsc(etapeCompetitionMapper.toEntity(etapeCompetitionDTO), associationMapper.toEntity(associationDTO))
            .stream()
            .map(matchMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public List<MatchDTO> findByEtapeCompetition(EtapeCompetitionDTO etapeCompetitionDTO, Integer journee) {
        log.debug("Request to get all Matches");

        List<Match> matches;

        if (journee != null) {
            matches = matchRepository.findAllByEtapeCompetitionAndJourneeAndDeletedIsFalseOrderByDateMatchAsc(etapeCompetitionMapper.toEntity(etapeCompetitionDTO), journee);
        } else {
            matches = matchRepository.findAllByEtapeCompetitionAndDeletedIsFalseOrderByDateMatchAsc(etapeCompetitionMapper.toEntity(etapeCompetitionDTO));
        }

        return matches.stream().map(matchMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public Optional<MatchDTO> findByHashcode(String hashcode) {
        log.debug("Request to get match by hashcode");
        return matchRepository.findByHashcodeAndDeletedIsFalseOrderByDateMatchAsc(hashcode)
            .map(matchMapper::toDto);
    }

    @Override
    public MatchDTO save(MatchDTO matchDTO, OrganisationDTO organisationDTO) {
        log.debug("Request to save match {} to organisation {}", matchDTO, organisationDTO);

        // find etapeCompetition
        var etapeCompetitionDTO = etapeCompetitionService.findOne(matchDTO.getEtapeCompetitionId())
            .orElseThrow(() -> new ApiRequestException(
                ExceptionCode.ETAPE_COMPETITION_NOT_FOUND_FOR_MATCH.getMessage(),
                ExceptionCode.ETAPE_COMPETITION_NOT_FOUND_FOR_MATCH.getValue(),
                ExceptionLevel.ERROR
            ));

        // check etapeCompetition
        this.communService.checkEtapeCompetition(etapeCompetitionDTO.getId(), organisationDTO);

        // check local club
        associationService.findOne(matchDTO.getLocalId())
            .orElseThrow(() -> new ApiRequestException(
                ExceptionCode.CLUB_LOCAL_NOT_FOUND_FOR_MATCH.getMessage(),
                ExceptionCode.CLUB_LOCAL_NOT_FOUND_FOR_MATCH.getValue(),
                ExceptionLevel.ERROR
            ));

        // check visiteur club
        associationService.findOne(matchDTO.getVisiteurId())
            .orElseThrow(() -> new ApiRequestException(
                ExceptionCode.CLUB_VISITEUR_NOT_FOUND_FOR_MATCH.getMessage(),
                ExceptionCode.CLUB_VISITEUR_NOT_FOUND_FOR_MATCH.getValue(),
                ExceptionLevel.ERROR
            ));

        String toHash = organisationDTO.getId() + ZonedDateTime.now().toString();
        matchDTO.setHashcode(this.communService.toHash(toHash));
        matchDTO.setCode("REF_" + organisationDTO.getId().toString());

        matchDTO.setCreatedAt(ZonedDateTime.now());
        matchDTO.setUpdatedAt(ZonedDateTime.now());
        matchDTO.setDeleted(false);
        matchDTO.setEtatMatch(EtatMatch.PROGRAMME);

        matchDTO = save(matchDTO);

        // change code adding match id
        matchDTO.setCode(matchDTO.getCode() + "_" + matchDTO.getId());

        return save(matchDTO);
    }

    @Override
    public Page<MatchDetail> search(OrganisationDTO organisationDTO, String query, Pageable pageable) {
        log.debug("Request to search for a page of Match for query {}", query);

        BoolQueryBuilder queryBuilder = communService.organisationQueryBuilder(organisationDTO.getId(), "organisationId");

        queryBuilder.must(queryStringQuery(query));

        return matchSearchRepository.search(queryBuilder, pageable)
            .map(match -> communService.getMatchDetail(matchStatsMapper.toDto(match)));
    }

    @Override
    public List<MatchDTO> findAll() {
        return matchRepository.findAllByDeletedIsFalse().stream()
            .map(matchMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public void reIndex() {
        log.info("Request to reindex all Matchs");
        matchSearchRepository.deleteAll();
        findAll().stream().map(matchStatsMapper::toStats).forEach(matchSearchRepository::save);
    }
}
