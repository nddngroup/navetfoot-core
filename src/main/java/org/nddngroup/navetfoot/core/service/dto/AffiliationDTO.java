package org.nddngroup.navetfoot.core.service.dto;

import org.nddngroup.navetfoot.core.domain.Affiliation;

import java.time.ZonedDateTime;
import java.io.Serializable;

/**
 * A DTO for the {@link Affiliation} entity.
 */
public class AffiliationDTO implements Serializable {

    private Long id;

    private String code;

    private String hashcode;

    private String state;

    private Boolean deleted;

    private ZonedDateTime createdAt;

    private ZonedDateTime updatedAt;


    private Long associationId;

    private String associationNom;

    private Long organisationId;

    private String organisationNom;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getHashcode() {
        return hashcode;
    }

    public void setHashcode(String hashcode) {
        this.hashcode = hashcode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Long getAssociationId() {
        return associationId;
    }

    public void setAssociationId(Long associationId) {
        this.associationId = associationId;
    }

    public String getAssociationNom() {
        return associationNom;
    }

    public void setAssociationNom(String associationNom) {
        this.associationNom = associationNom;
    }

    public Long getOrganisationId() {
        return organisationId;
    }

    public void setOrganisationId(Long organisationId) {
        this.organisationId = organisationId;
    }

    public String getOrganisationNom() {
        return organisationNom;
    }

    public void setOrganisationNom(String organisationNom) {
        this.organisationNom = organisationNom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AffiliationDTO)) {
            return false;
        }

        return id != null && id.equals(((AffiliationDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AffiliationDTO{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", hashcode='" + getHashcode() + "'" +
            ", state='" + getState() + "'" +
            ", deleted='" + isDeleted() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", associationId=" + getAssociationId() +
            ", associationNom='" + getAssociationNom() + "'" +
            ", organisationId=" + getOrganisationId() +
            ", organisationNom='" + getOrganisationNom() + "'" +
            "}";
    }
}
