package org.nddngroup.navetfoot.core.service;

import org.nddngroup.navetfoot.core.domain.Parametrage;
import org.nddngroup.navetfoot.core.service.dto.ParametrageDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Parametrage}.
 */
public interface ParametrageService {

    /**
     * Save a parametrage.
     *
     * @param parametrageDTO the entity to save.
     * @return the persisted entity.
     */
    ParametrageDTO save(ParametrageDTO parametrageDTO);

    /**
     * Get all the parametrages.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ParametrageDTO> findAll(Pageable pageable);


    /**
     * Get the "id" parametrage.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ParametrageDTO> findOne(Long id);

    /**
     * Delete the "id" parametrage.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the parametrage corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ParametrageDTO> search(String query, Pageable pageable);
    List<ParametrageDTO> findAll();
    void reIndex();
}
