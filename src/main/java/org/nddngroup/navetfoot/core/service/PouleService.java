package org.nddngroup.navetfoot.core.service;

import org.nddngroup.navetfoot.core.domain.Poule;
import org.nddngroup.navetfoot.core.service.dto.*;
import org.nddngroup.navetfoot.core.service.dto.AssociationDTO;
import org.nddngroup.navetfoot.core.service.dto.EtapeCompetitionDTO;
import org.nddngroup.navetfoot.core.service.dto.OrganisationDTO;
import org.nddngroup.navetfoot.core.service.dto.PouleDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Poule}.
 */
public interface PouleService {

    /**
     * Save a poule.
     *
     * @param pouleDTO the entity to save.
     * @return the persisted entity.
     */
    PouleDTO save(PouleDTO pouleDTO);

    /**
     * Get all the poules.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PouleDTO> findAll(Pageable pageable);


    /**
     * Get the "id" poule.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PouleDTO> findOne(Long id);

    /**
     * Delete the "id" poule.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the poule corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PouleDTO> search(String query, Pageable pageable);
    List<PouleDTO> findByEtapeCompetition(EtapeCompetitionDTO etapeCompetitionDTO);
    Optional<PouleDTO> findByHashcode(String hashcode);
    PouleDTO save(PouleDTO pouleDTO, OrganisationDTO organisationDTO);
    PouleDTO save(PouleDTO pouleDTO, List<AssociationDTO> associationDTOS, OrganisationDTO organisationDTO);
    List<PouleDTO> findAll();
    void reIndex();
}
