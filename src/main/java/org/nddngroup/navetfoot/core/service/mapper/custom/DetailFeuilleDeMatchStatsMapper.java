package org.nddngroup.navetfoot.core.service.mapper.custom;

import org.nddngroup.navetfoot.core.domain.DetailFeuilleDeMatch;
import org.nddngroup.navetfoot.core.service.*;
import org.nddngroup.navetfoot.core.service.*;
import org.nddngroup.navetfoot.core.service.dto.DetailFeuilleDeMatchDTO;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring")
public abstract class DetailFeuilleDeMatchStatsMapper {
    @Autowired
    protected EtapeCompetitionService etapeCompetitionService;
    @Autowired
    protected CompetitionSaisonService competitionSaisonService;
    @Autowired
    protected SaisonService saisonService;
    @Autowired
    protected CompetitionService competitionService;
    @Autowired
    protected OrganisationService organisationService;
    @Autowired
    protected AssociationService associationService;
    @Autowired
    protected MatchService matchService;
    @Autowired
    protected JoueurService joueurService;
    @Autowired
    protected FeuilleDeMatchService feuilleDeMatchService;
    @Autowired
    protected MatchStatsMapper matchStatsMapper;

    public DetailFeuilleDeMatch toStats(DetailFeuilleDeMatchDTO detailFeuilleDeMatchDTO) {
        var stats = new DetailFeuilleDeMatch()
            .code(detailFeuilleDeMatchDTO.getCode())
            .hashcode(detailFeuilleDeMatchDTO.getHashcode())
            .equipe(detailFeuilleDeMatchDTO.getEquipe())
            .position(detailFeuilleDeMatchDTO.getPosition())
            .poste(detailFeuilleDeMatchDTO.getPoste())
            .grille(detailFeuilleDeMatchDTO.getGrille())
            .numeroJoueur(detailFeuilleDeMatchDTO.getNumeroJoueur())
            .deleted(detailFeuilleDeMatchDTO.isDeleted())
            .createdAt(detailFeuilleDeMatchDTO.getCreatedAt())
            .updatedAt(detailFeuilleDeMatchDTO.getUpdatedAt());

        stats.setId(detailFeuilleDeMatchDTO.getId());
        // find joueur
        joueurService.findOne(detailFeuilleDeMatchDTO.getJoueurId())
            .ifPresent(joueurDTO -> {
                stats.setJoueurId(joueurDTO.getId());
                stats.setJoueurNom(joueurDTO.getNom());
                stats.setJoueurPrenom(joueurDTO.getPrenom());
                stats.setJoueurImage(joueurDTO.getImageUrl());
            });

        // find feuilleDeMatch
        feuilleDeMatchService.findOne(detailFeuilleDeMatchDTO.getFeuilleDeMatchId())
            .ifPresent(feuilleDeMatchDTO -> {
                stats.feuilleDeMatchId(feuilleDeMatchDTO.getId());
                // find match
                matchService.findOne(feuilleDeMatchDTO.getMatchId())
                    .ifPresent(matchDTO -> {
                        stats.setMatchId(matchDTO.getId());

                        var matchStats = matchStatsMapper.toStats(matchDTO);
                        stats.setLocalId(matchStats.getLocalId());
                        stats.setLocalNom(matchStats.getLocalNom());

                        stats.setVisiteurId(matchStats.getVisiteurId());
                        stats.setVisiteurNom(matchStats.getVisiteurNom());

                        stats.setEtapeCompetitionId(matchStats.getEtapeCompetitionId());
                        stats.setEtapeCompetitionNom(matchStats.getEtapeCompetitionNom());

                        stats.setSaisonId(matchStats.getSaisonId());
                        stats.setSaisonLibelle(matchStats.getSaisonLibelle());

                        stats.setCompetitionId(matchStats.getCompetitionId());
                        stats.setCompetitionNom(matchStats.getCompetitionNom());

                        stats.setOrganisationId(matchStats.getOrganisationId());
                        stats.setOrganisationNom(matchStats.getOrganisationNom());
                    });
            });

        return stats;
    }
}
