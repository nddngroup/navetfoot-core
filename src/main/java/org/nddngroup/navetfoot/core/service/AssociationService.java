package org.nddngroup.navetfoot.core.service;

import org.nddngroup.navetfoot.core.domain.Association;
import org.nddngroup.navetfoot.core.service.dto.AssociationDTO;
import org.nddngroup.navetfoot.core.service.dto.OrganisationDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Association}.
 */
public interface AssociationService {

    /**
     * Save a association.
     *
     * @param associationDTO the entity to save.
     * @return the persisted entity.
     */
    AssociationDTO save(AssociationDTO associationDTO);

    /**
     * Get all the associations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<AssociationDTO> findAll(Pageable pageable);
    List<AssociationDTO> findAll();


    /**
     * Get the "id" association.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<AssociationDTO> findOne(Long id);

    /**
     * Delete the "id" association.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the association corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<AssociationDTO> search(String query, Pageable pageable);
    Page<AssociationDTO> findByIdIn(List<Long> ids, Pageable pageable);
    List<AssociationDTO> findByIdIn(List<Long> ids);
    List<AssociationDTO> findByNom(String nom);
    Page<AssociationDTO> findByNomContains(String nom, Pageable pageable);
    Optional<AssociationDTO> findByHashcode(String hashcode);
    List<AssociationDTO> save(OrganisationDTO organisationDTO, List<AssociationDTO> associationDTOS);
    AssociationDTO save(AssociationDTO associationDTO, OrganisationDTO organisationDTO);
    Optional<AssociationDTO> findByNomAndAdresse(String nom, String adresse);
    Page<AssociationDTO> search(OrganisationDTO organisationDTO, String query, Pageable pageable);
    void reIndex();
}
