package org.nddngroup.navetfoot.core.service.mapper;


import org.nddngroup.navetfoot.core.domain.*;
import org.nddngroup.navetfoot.core.domain.Organisation;
import org.nddngroup.navetfoot.core.service.dto.OrganisationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Organisation} and its DTO {@link OrganisationDTO}.
 */
@Mapper(componentModel = "spring", uses = {PaysMapper.class, RegionMapper.class, DepartementMapper.class, AssociationMapper.class})
public interface OrganisationMapper extends EntityMapper<OrganisationDTO, Organisation> {

    @Mapping(source = "pays.id", target = "paysId")
    @Mapping(source = "pays.nom", target = "paysNom")
    @Mapping(source = "region.id", target = "regionId")
    @Mapping(source = "region.nom", target = "regionNom")
    @Mapping(source = "departement.id", target = "departementId")
    @Mapping(source = "departement.nom", target = "departementNom")
    OrganisationDTO toDto(Organisation organisation);

    @Mapping(target = "saisons", ignore = true)
    @Mapping(target = "removeSaisons", ignore = true)
    @Mapping(target = "affiliations", ignore = true)
    @Mapping(target = "removeAffiliation", ignore = true)
    @Mapping(source = "paysId", target = "pays")
    @Mapping(source = "regionId", target = "region")
    @Mapping(source = "departementId", target = "departement")
    @Mapping(target = "removeAssociations", ignore = true)
    Organisation toEntity(OrganisationDTO organisationDTO);

    default Organisation fromId(Long id) {
        if (id == null) {
            return null;
        }
        Organisation organisation = new Organisation();
        organisation.setId(id);
        return organisation;
    }
}
