package org.nddngroup.navetfoot.core.service;

import org.nddngroup.navetfoot.core.domain.CompetitionSaison;
import org.nddngroup.navetfoot.core.service.dto.CompetitionDTO;
import org.nddngroup.navetfoot.core.service.dto.CompetitionSaisonDTO;
import org.nddngroup.navetfoot.core.service.dto.SaisonDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link CompetitionSaison}.
 */
public interface CompetitionSaisonService {

    /**
     * Save a competitionSaison.
     *
     * @param competitionSaisonDTO the entity to save.
     * @return the persisted entity.
     */
    CompetitionSaisonDTO save(CompetitionSaisonDTO competitionSaisonDTO);

    /**
     * Get all the competitionSaisons.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CompetitionSaisonDTO> findAll(Pageable pageable);


    /**
     * Get the "id" competitionSaison.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CompetitionSaisonDTO> findOne(Long id);

    /**
     * Delete the "id" competitionSaison.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the competitionSaison corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CompetitionSaisonDTO> search(String query, Pageable pageable);
    List<CompetitionSaisonDTO> findBySaison(SaisonDTO saisonDTO);
    List<CompetitionSaisonDTO> findBySaisons(List<Long> saisonIds);
    List<CompetitionSaisonDTO> findBySaisonAndCompetition(SaisonDTO saisonDTO, CompetitionDTO competitionDTO);
    Optional<CompetitionSaisonDTO> findByHashcode(String hashcode);
    List<CompetitionSaisonDTO> findByCompetition(CompetitionDTO competitionDTO);
    List<CompetitionSaisonDTO> findAll();
    void reIndex();
}
