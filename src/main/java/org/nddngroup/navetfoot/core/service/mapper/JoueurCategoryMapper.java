package org.nddngroup.navetfoot.core.service.mapper;


import org.nddngroup.navetfoot.core.domain.*;
import org.nddngroup.navetfoot.core.domain.JoueurCategory;
import org.nddngroup.navetfoot.core.service.dto.JoueurCategoryDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link JoueurCategory} and its DTO {@link JoueurCategoryDTO}.
 */
@Mapper(componentModel = "spring", uses = {JoueurMapper.class, CategoryMapper.class})
public interface JoueurCategoryMapper extends EntityMapper<JoueurCategoryDTO, JoueurCategory> {

    @Mapping(source = "joueur.id", target = "joueurId")
    @Mapping(source = "category.id", target = "categoryId")
    JoueurCategoryDTO toDto(JoueurCategory joueurCategory);

    @Mapping(source = "joueurId", target = "joueur")
    @Mapping(source = "categoryId", target = "category")
    JoueurCategory toEntity(JoueurCategoryDTO joueurCategoryDTO);

    default JoueurCategory fromId(Long id) {
        if (id == null) {
            return null;
        }
        JoueurCategory joueurCategory = new JoueurCategory();
        joueurCategory.setId(id);
        return joueurCategory;
    }
}
