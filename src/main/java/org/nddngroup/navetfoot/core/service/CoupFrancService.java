package org.nddngroup.navetfoot.core.service;

import org.nddngroup.navetfoot.core.domain.CoupFranc;
import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;
import org.nddngroup.navetfoot.core.service.dto.CoupFrancDTO;
import org.nddngroup.navetfoot.core.service.dto.MatchDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link CoupFranc}.
 */
public interface CoupFrancService {

    /**
     * Save a coupFranc.
     *
     * @param coupFrancDTO the entity to save.
     * @return the persisted entity.
     */
    CoupFrancDTO save(CoupFrancDTO coupFrancDTO);

    /**
     * Get all the coupFrancs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CoupFrancDTO> findAll(Pageable pageable);


    /**
     * Get the "id" coupFranc.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CoupFrancDTO> findOne(Long id);

    /**
     * Delete the "id" coupFranc.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the coupFranc corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CoupFrancDTO> search(String query, Pageable pageable);

    List<CoupFrancDTO> findByMatch(MatchDTO matchDTO);
    List<CoupFrancDTO> findByMatchAndEquipe(MatchDTO matchDTO, Equipe equipe);
    Optional<CoupFrancDTO> findByHashcode(String hashcode);
    List<CoupFrancDTO> findAll();
    void reIndex();
}
