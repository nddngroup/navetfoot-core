package org.nddngroup.navetfoot.core.service.impl;

import org.nddngroup.navetfoot.core.domain.Tir;
import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;
import org.nddngroup.navetfoot.core.repository.TirRepository;
import org.nddngroup.navetfoot.core.repository.search.TirSearchRepository;
import org.nddngroup.navetfoot.core.service.TirService;
import org.nddngroup.navetfoot.core.service.dto.MatchDTO;
import org.nddngroup.navetfoot.core.service.dto.TirDTO;
import org.nddngroup.navetfoot.core.service.mapper.MatchMapper;
import org.nddngroup.navetfoot.core.service.mapper.TirMapper;
import org.nddngroup.navetfoot.core.service.mapper.custom.TirStatsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link Tir}.
 */
@Service
@Transactional
public class TirServiceImpl implements TirService {

    private final Logger log = LoggerFactory.getLogger(TirServiceImpl.class);

    private final TirRepository tirRepository;

    private final TirMapper tirMapper;

    private final TirSearchRepository tirSearchRepository;
    private final TirStatsMapper tirStatsMapper;

    @Autowired
    private MatchMapper matchMapper;

    public TirServiceImpl(TirRepository tirRepository,
                          TirMapper tirMapper,
                          TirSearchRepository tirSearchRepository,
                          TirStatsMapper tirStatsMapper
                          ) {
        this.tirRepository = tirRepository;
        this.tirMapper = tirMapper;
        this.tirSearchRepository = tirSearchRepository;
        this.tirStatsMapper = tirStatsMapper;
    }

    @Override
    public TirDTO save(TirDTO tirDTO) {
        log.debug("Request to save Tir : {}", tirDTO);

        tirDTO.setUpdatedAt(ZonedDateTime.now());
        if (tirDTO.getCreatedAt() == null) {
            tirDTO.setCreatedAt(ZonedDateTime.now());
        }

        Tir tir = tirMapper.toEntity(tirDTO);
        tir = tirRepository.save(tir);
        TirDTO result = tirMapper.toDto(tir);

        if (Boolean.TRUE.equals(tirDTO.isDeleted())) {
            tirSearchRepository.deleteById(result.getId());
        } else {
            tirSearchRepository.save(tirStatsMapper.toStats(result));
        }

        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public List<TirDTO> findAll() {
        log.debug("Request to get all Tirs");
        return tirRepository.findAll().stream()
            .map(tirMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<TirDTO> findOne(Long id) {
        log.debug("Request to get Tir : {}", id);
        return tirRepository.findById(id)
            .map(tirMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Tir : {}", id);
        tirRepository.deleteById(id);
        tirSearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<TirDTO> search(String query) {
        log.debug("Request to search Tirs for query {}", query);
        return StreamSupport
            .stream(tirSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(tirMapper::toDto)
        .collect(Collectors.toList());
    }

    @Override
    public List<TirDTO> findByMatch(MatchDTO matchDTO) {
        log.debug("Request to get all tirs by match");
        return tirRepository.findAllByMatchAndDeletedIsFalse(matchMapper.toEntity(matchDTO))
            .stream()
            .map(tirMapper::toDto).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<TirDTO> findByMatchAndEquipe(MatchDTO matchDTO, Equipe equipe) {
        log.debug("Request to get all tirs by match and Equipe");
        return tirRepository.findAllByMatchAndEquipeAndDeletedIsFalse(matchMapper.toEntity(matchDTO), equipe)
            .stream()
            .map(tirMapper::toDto).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<TirDTO> findByMatchAndEquipeAndCadre(MatchDTO matchDTO, Equipe equipe, Boolean cadre) {
        log.debug("Request to get all tirs by match and equipe and cadre");
        return tirRepository.findAllByMatchAndEquipeAndCadreAndDeletedIsFalse(matchMapper.toEntity(matchDTO), equipe, cadre)
            .stream()
            .map(tirMapper::toDto).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<TirDTO> findByHashcode(String hashcode) {
        log.debug("Request to get Tir : {}", hashcode);
        return tirRepository.findByHashcodeAndDeletedIsFalse(hashcode)
            .map(tirMapper::toDto);
    }

    @Override
    public List<TirDTO> findAllByDeletedIsFalse() {
        return tirRepository.findAllByDeletedIsFalse().stream()
            .map(tirMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public void reIndex() {
        log.info("Request to reindex all Competitions");
        tirSearchRepository.deleteAll();
        findAll().stream().map(tirStatsMapper::toStats).forEach(tirSearchRepository::save);
    }
}
