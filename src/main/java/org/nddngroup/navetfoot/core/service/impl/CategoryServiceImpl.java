package org.nddngroup.navetfoot.core.service.impl;

import org.nddngroup.navetfoot.core.service.CategoryService;
import org.nddngroup.navetfoot.core.domain.Category;
import org.nddngroup.navetfoot.core.repository.CategoryRepository;
import org.nddngroup.navetfoot.core.repository.search.CategorySearchRepository;
import org.nddngroup.navetfoot.core.service.dto.CategoryDTO;
import org.nddngroup.navetfoot.core.service.mapper.CategoryMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link Category}.
 */
@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {

    private final Logger log = LoggerFactory.getLogger(CategoryServiceImpl.class);

    private final CategoryRepository categoryRepository;

    private final CategoryMapper categoryMapper;

    private final CategorySearchRepository categorySearchRepository;

    public CategoryServiceImpl(CategoryRepository categoryRepository, CategoryMapper categoryMapper, CategorySearchRepository categorySearchRepository) {
        this.categoryRepository = categoryRepository;
        this.categoryMapper = categoryMapper;
        this.categorySearchRepository = categorySearchRepository;
    }

    @Override
    public CategoryDTO save(CategoryDTO categoryDTO) {
        log.debug("Request to save Category : {}", categoryDTO);
        Category category = categoryMapper.toEntity(categoryDTO);
        category = categoryRepository.save(category);
        CategoryDTO result = categoryMapper.toDto(category);

        if (categoryDTO.isDeleted()) {
            categorySearchRepository.deleteById(categoryDTO.getId());
        } else {
            categorySearchRepository.save(categoryMapper.toEntity(result));
        }

        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<CategoryDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Categories");
        return categoryRepository.findAllByDeletedIsFalse(pageable)
            .map(categoryMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<CategoryDTO> findOne(Long id) {
        log.debug("Request to get Category : {}", id);
        return categoryRepository.findById(id)
            .map(categoryMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Category : {}", id);
        categoryRepository.deleteById(id);
        categorySearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<CategoryDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Categories for query {}", query);
        return categorySearchRepository.search(queryStringQuery(query), pageable)
            .map(categoryMapper::toDto);
    }

    @Override
    public List<CategoryDTO> findAll() {
        log.debug("Request to get all Categories");
        return categoryRepository.findAllByDeletedIsFalse()
            .stream()
            .map(categoryMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public void deleteCategory(Long id) {
        log.debug("Request to remove Category : {}", id);

        CategoryDTO result;

        // find category by id
        findOne(id)
            .ifPresent(
                categoryDTO -> {
                    categoryDTO.setDeleted(true);
                    categoryDTO.setUpdatedAt(ZonedDateTime.now());

                    save(categoryDTO);
                }
            );
    }


    @Override
    public void reIndex() {
        log.info("Request to reindex all Categories");
        categorySearchRepository.deleteAll();
        findAll().stream().map(categoryMapper::toEntity).forEach(categorySearchRepository::save);
    }
}
