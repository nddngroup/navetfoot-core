package org.nddngroup.navetfoot.core.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;

import org.nddngroup.navetfoot.core.domain.Parametrage;
import org.nddngroup.navetfoot.core.domain.enumeration.TypeParamatrage;

/**
 * A DTO for the {@link Parametrage} entity.
 */
public class ParametrageDTO implements Serializable {

    private Long id;

    private String tag;

    private String libelle;

    private TypeParamatrage type;

    private String valeur;

    private String fonctionDeChoix;

    private ZonedDateTime createdAt;

    private ZonedDateTime updatedAt;

    private String referenceTag;

    private Long referenceId;

    private String description;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public TypeParamatrage getType() {
        return type;
    }

    public void setType(TypeParamatrage type) {
        this.type = type;
    }

    public String getValeur() {
        return valeur;
    }

    public void setValeur(String valeur) {
        this.valeur = valeur;
    }

    public String getFonctionDeChoix() {
        return fonctionDeChoix;
    }

    public void setFonctionDeChoix(String fonctionDeChoix) {
        this.fonctionDeChoix = fonctionDeChoix;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getReferenceTag() {
        return referenceTag;
    }

    public void setReferenceTag(String referenceTag) {
        this.referenceTag = referenceTag;
    }

    public Long getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(Long referenceId) {
        this.referenceId = referenceId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ParametrageDTO)) {
            return false;
        }

        return id != null && id.equals(((ParametrageDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ParametrageDTO{" +
            "id=" + getId() +
            ", tag='" + getTag() + "'" +
            ", libelle='" + getLibelle() + "'" +
            ", type='" + getType() + "'" +
            ", valeur='" + getValeur() + "'" +
            ", fonctionDeChoix='" + getFonctionDeChoix() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", referenceTag='" + getReferenceTag() + "'" +
            ", referenceId=" + getReferenceId() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
