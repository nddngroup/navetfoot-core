package org.nddngroup.navetfoot.core.service;

import org.nddngroup.navetfoot.core.domain.Carton;
import org.nddngroup.navetfoot.core.domain.enumeration.Couleur;
import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;
import org.nddngroup.navetfoot.core.service.dto.CartonDTO;
import org.nddngroup.navetfoot.core.service.dto.JoueurDTO;
import org.nddngroup.navetfoot.core.service.dto.MatchDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Carton}.
 */
public interface CartonService {

    /**
     * Save a carton.
     *
     * @param cartonDTO the entity to save.
     * @return the persisted entity.
     */
    CartonDTO save(CartonDTO cartonDTO);

    /**
     * Get all the cartons.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CartonDTO> findAll(Pageable pageable);


    /**
     * Get the "id" carton.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CartonDTO> findOne(Long id);

    /**
     * Delete the "id" carton.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the carton corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CartonDTO> search(String query, Pageable pageable);

    List<CartonDTO> findByMatch(MatchDTO matchDTO);
    List<CartonDTO> findByMatchAndJoueurAndCouleur(MatchDTO matchDTO, JoueurDTO joueurDTO, Couleur couleur);
    List<CartonDTO> findByMatchAndCouleur(MatchDTO matchDTO, Couleur couleur);
    List<CartonDTO> findByMatchAndEquipe(MatchDTO matchDTO, Equipe equipe);
    List<CartonDTO> findByMatchAndEquipeAndCouleur(MatchDTO matchDTO, Equipe equipe, Couleur couleur);
    Optional<CartonDTO> findByHashcode(String hashcode);
    List<CartonDTO> findAll();
    void reIndex();
}
