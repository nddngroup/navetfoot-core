package org.nddngroup.navetfoot.core.service.external;

import org.nddngroup.navetfoot.core.config.ApplicationProperties;
import org.nddngroup.navetfoot.externalapi.service.ApiFootballService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExternalApiFootballRobot {
    private final ApiFootballService apiFootballService;
    private final ApplicationProperties applicationProperties;

    ExternalApiFootballRobot(ApiFootballService apiFootballService, ApplicationProperties applicationProperties) {
        this.apiFootballService = apiFootballService;
        this.applicationProperties = applicationProperties;
    }

    /************** Get data from external API *******************/
    @Scheduled(cron = "0 0 02 * * 4") // "At 02:00, only on Thursday"
    void getFixturesAndSave() {
        List<String> leagueIds = applicationProperties.getExternalApi().getLeagueIds();
        String season = applicationProperties.getExternalApi().getSeason();
        int interval = applicationProperties.getExternalApi().getInterval();

        leagueIds.forEach(leagueID -> this.apiFootballService.getFixturesAndSave(leagueID, season, interval));
    }

    @Scheduled(cron = "0 0/5 10-22 * * *") // "Every 5 minutes, between 10:00 and 22:59"
    void getFixtureDetailsAndSave() {
        apiFootballService.getFixtureDetailsAndSave();
    }
}
