package org.nddngroup.navetfoot.core.service;

import org.nddngroup.navetfoot.core.domain.DetailFeuilleDeMatch;
import org.nddngroup.navetfoot.core.domain.enumeration.Equipe;
import org.nddngroup.navetfoot.core.service.dto.DetailFeuilleDeMatchDTO;
import org.nddngroup.navetfoot.core.service.dto.FeuilleDeMatchDTO;
import org.nddngroup.navetfoot.core.service.dto.JoueurDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link DetailFeuilleDeMatch}.
 */
public interface DetailFeuilleDeMatchService {

    /**
     * Save a detailFeuilleDeMatch.
     *
     * @param detailFeuilleDeMatchDTO the entity to save.
     * @return the persisted entity.
     */
    DetailFeuilleDeMatchDTO save(DetailFeuilleDeMatchDTO detailFeuilleDeMatchDTO);

    /**
     * Get all the detailFeuilleDeMatches.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<DetailFeuilleDeMatchDTO> findAll(Pageable pageable);


    /**
     * Get the "id" detailFeuilleDeMatch.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<DetailFeuilleDeMatchDTO> findOne(Long id);

    /**
     * Delete the "id" detailFeuilleDeMatch.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
    void removeAll(String hashcode);

    /**
     * Search for the detailFeuilleDeMatch corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<DetailFeuilleDeMatchDTO> search(String query, Pageable pageable);
    List<DetailFeuilleDeMatchDTO> findByFeuilleDeMatch(FeuilleDeMatchDTO feuilleDeMatchDTO);
    Optional<DetailFeuilleDeMatchDTO> findByFeuilleDeMatchAndJoueur(FeuilleDeMatchDTO feuilleDeMatchDTO, JoueurDTO joueurDTO);
    List<DetailFeuilleDeMatchDTO> findByFeuilleDeMatchAndEquipe(FeuilleDeMatchDTO feuilleDeMatchDTO, Equipe equipe);
    Optional<DetailFeuilleDeMatchDTO> findByHashcode(String hashcode);
    List<DetailFeuilleDeMatchDTO> findAll();
    void reIndex();
}
