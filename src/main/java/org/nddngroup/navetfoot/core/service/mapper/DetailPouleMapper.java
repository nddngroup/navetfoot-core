package org.nddngroup.navetfoot.core.service.mapper;


import org.nddngroup.navetfoot.core.domain.*;
import org.nddngroup.navetfoot.core.domain.DetailPoule;
import org.nddngroup.navetfoot.core.service.dto.DetailPouleDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link DetailPoule} and its DTO {@link DetailPouleDTO}.
 */
@Mapper(componentModel = "spring", uses = {AssociationMapper.class, PouleMapper.class})
public interface DetailPouleMapper extends EntityMapper<DetailPouleDTO, DetailPoule> {

    @Mapping(source = "association.id", target = "associationId")
    @Mapping(source = "poule.id", target = "pouleId")
    DetailPouleDTO toDto(DetailPoule detailPoule);

    @Mapping(source = "associationId", target = "association")
    @Mapping(source = "pouleId", target = "poule")
    DetailPoule toEntity(DetailPouleDTO detailPouleDTO);

    default DetailPoule fromId(Long id) {
        if (id == null) {
            return null;
        }
        DetailPoule detailPoule = new DetailPoule();
        detailPoule.setId(id);
        return detailPoule;
    }
}
