package org.nddngroup.navetfoot.core.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.ZoneId;
import java.time.ZonedDateTime;

@ControllerAdvice
public class ApiExceptionHandler {
    @ExceptionHandler(value = {ApiRequestException.class})
    public ResponseEntity<Object> handleApiRequestException(ApiRequestException e) {
        // 1. create payload containing exception details
        HttpStatus badRequest = HttpStatus.BAD_REQUEST;

        ApiException apiException = new ApiException(
            e.getMessage(),
            e.getCode(),
            e.getLevel(),
            badRequest,
            ZonedDateTime.now(ZoneId.of("Z"))
        );

        // 2. return response entity
        return new ResponseEntity<>(apiException, badRequest);
    }
//
//    @ExceptionHandler(value = Exception.class)
//    public ResponseEntity<Object> handleException(Exception e) {
//        // 1. create payload containing exception details
//        HttpStatus badRequest = HttpStatus.BAD_REQUEST;
//
//        ApiException apiException = new ApiException(
//            ExceptionCode.ERREUR_INTERNE.getMessage(),
//            ExceptionCode.ERREUR_INTERNE.getValue(),
//            ExceptionLevel.ERROR,
//            badRequest,
//            ZonedDateTime.now(ZoneId.of("Z"))
//        );
//
//        // 2. return response entity
//        return new ResponseEntity<>(apiException, badRequest);
//    }
}
