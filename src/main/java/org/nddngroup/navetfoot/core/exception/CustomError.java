package org.nddngroup.navetfoot.core.exception;

import org.springframework.http.HttpStatus;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

public class CustomError implements Serializable {
    private int code;
    private HttpStatus status;
    private String message;
    private List<String> errors;

    public CustomError(int code, HttpStatus status, String message, List<String> errors) {
        super();
        this.code = code;
        this.status = status;
        this.message = message;
        this.errors = errors;
    }

    public CustomError(int code, HttpStatus status, String message, String error) {
        super();
        this.code = code;
        this.status = status;
        this.message = message;
        errors = Arrays.asList(error);
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getCode() {
        return this.code;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public HttpStatus getStatus() {
        return this.status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public List<String> getErrors() {
        return this.errors;
    }

}
