package org.nddngroup.navetfoot.core.exception;

public enum ExceptionLevel {
    WARNING,
    ERROR
}
