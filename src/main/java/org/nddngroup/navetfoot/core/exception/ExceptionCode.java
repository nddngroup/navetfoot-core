package org.nddngroup.navetfoot.core.exception;

public enum ExceptionCode {
    USER_NOT_FOUND(400, "Aucun utilisateur trouvé."),
    ORGANISATION_NOT_FOUND(401, "Aucune organisation trouvée"),

    ERREUR_INTERNE(500, "Système en maintenance! réessayez plutard."),

    // ORGANISATION RESOURCE

    // SAISON RESOURCE
    SAISON_LIBELLE_NOT_VALID(2000, "L'année n'est pas valide"),

    // COMPETITION RESOURCE

    // ASSOCIATION RESOURCE
    ASSOCIATIONS_NOT_FOUND(4000, "Aucun club trouvé dans la poule"),

    // JOUEUR RESSOURCE
    JOUEUR_NOT_FOUND(5000, "Aucun joueur trouvé"),
    CLUB_NON_AFFILIE(5003, "Association non affiliée à cette organisation"),
    JOUEUR_HAS_CONTRAT(5004, "Le joueur a déja un contrat"),
    CLUB_NOT_FOUND(5005, "Club non trouvée"),
    IDENTIFIANT_EXISTS(5006, "L'identifiant du joueur existe déja."),

    // MATCH RESOURCE
    ETAPE_COMPETITION_NOT_FOUND_FOR_MATCH(6001, "Etape non trouvé pour ce match."),
    CLUB_LOCAL_NOT_FOUND_FOR_MATCH(6002, "Le club local n'est renseigné."),
    CLUB_VISITEUR_NOT_FOUND_FOR_MATCH(6003, "Le club visiteur n'est renseigné."),
    CAN_NOT_DELETE_MATCH(6004, "Le match ne peut pas être supprimé."),
    MATCH_ALREADY_HAS_DELEGUE(6005, "Le match a déja un délégué."),
    UNSUFFISANT_ACCOUNT(6006, "Votre quota de matchs est insuffisant."),

    // POULE RESOURCE
    POULE_NOT_FOUND(7000, "Poule non trouvée"),
    ETAPE_COMPETITION_NOT_FOUND_FOR_POULE(7001, "L'étape de la poule non trouvée"),
    POULE_HAS_MATCHS(7002, "La poule contient des matchs"),
    POULE_NOT_UNIQUE(7003, "Un championnat ne peut pas avoir plus d'une poule."),

    // COMMON SERVICE
    SAISON_NOT_FOUND(9000, "Saison non trouvée"),
    COMPETITION_NOT_FOUND(9001, "Compétition non trouvée"),
    SAISON_NOT_FOR_ORGANISATION(9002, "Saison n'appartient pas à l'organisation"),
    COMPETITION_NOT_FOR_ORGANISATION(9003, "Compétition n'appartient pas à l'organisation"),
    COMPETITION_SAISON_NOT_FOUND(9004, "CompétitionSaison non trouvé"),
    ETAPE_NOT_FOUND(9005, "Etape non trouvé"),
    ETAPE_EXISTS_IN_COMPETITION_SAISON(9006, "Etape déja ajouté"),
    MATCH_NOT_FOUND(9007, "Match non trouvé"),
    CLUB_LOCAL_AFFILIATION_NOT_FOUND(9008, "Le club local n'est pas affilié à l'organisation"),
    CLUB_VISITEUR_AFFILIATION_NOT_FOUND(9008, "Le club visiteur n'est pas affilié à l'organisation"),
    COMPETITION_SAISON_NOT_FOUND_FOR_ETAPE(9009, "CompetitionSaison non trouvé pour cette étape"),
    COMPETITION_SAISON_NOT_IN_ORGANISATION(9009, "CompetitionSaison non trouvé pour cette étape"),
    PARTICIPATION_ALREADY_EXISTS(9010, "Le club est déja ajouté"),
    CLUB_NOT_IN_ORGANISATION(9011, "Le club n'est pas affilié à l'organisation"),
    CAN_NOT_DELETE_ALREADY_HAS_MATCH(9012, "Impossible de retirer un club déja programmé pour un match"),
    POULE_ALREADY_EXISTS(9013, "Une poule du même nom est déja ajouté"),
    CAN_NOT_REMOVE_CLUB_FROM_ETAPE(9014, "Impossible de retirer un club déja ajouté à une poule"),
    COMPETITION_SAISON_DUPLICATED(9015, "CompetitionSaison dupliqué"),
    COMPETITION_DUPLICATED_FOR_ORGANISATION(9016, "Competition dupliquée pour cette organisation"),
    SAISON_DUPLICATED_FOR_ORGANISATION(9017, "Saison dupliquée pour cette organisation"),


    // NOTIFICATION SERVICE
    SMS_NOT_SENT(10000, "Le sms n'a pas pu être envoyé."),

    // FEUILLE_DE_MATCH RESOURCE
    FEUILLE_DE_MATCH_ALREADY_EXISTS(11000, "La feuille de match existe déja."),
    JOUEUR_NOT_FOUND_IN_FEUILLE_DE_MATCH(11001, "Joueur non trouvé."),
    FEULLE_DE_MATCH_NOT_FOUND(11002, "Feuille de match non trouvée."),
    ASSOCIATIONS_NOT_FOUND_IN_FEUILLE_DE_MATCH(11003, "Le club n'est pas trouvé."),
    CAN_NOT_EDIT_FEUILLE_DE_MATCH(11004, "Modification impossible (match terminé)."),

    // CONTRAT RESOURCE
    CONTRAT_NOT_FOUND(12000, "Contrat non trouvé."),

    // PARTICIPATION RESOURCE
    PARTICIPATION_NOT_FOUND(13000, "Participation non trouvée."),

    // TIR_AU_BUT RESOURCE
    TIR_AU_BUT_NOT_FOUND(14000, "Tir au but non trouvé."),

    // CATEGORY RESOURCE
    CATEGORY_NOT_FOUND(15000, "Catégory non trouvé."),
    CATEGORY_LIST_EMPTY(15001, "La liste des catégories est vide."),
    CATEGORY_ALREADY_ADDED_TO_COMPETITION(15002, "La catégorie est déja ajoutée."),

    // COMPETITION_SAISON_CATEGORY RESOURCE
    COMPETITION_SAISON_CATEGORY_NOT_FOUND(16000, "CompetitionSaisonCategory non trouvé."),
    COMPETITION_SAISON_CATEGORY_DUPLICATED(16001, "CompetitionSaisonCategory dupliqué."),

    // JOUEUR_CATEGORY RESOURCE
    JOUEUR_CATEGORY_NOT_FOUND(17000, "JoueurCategory non trouvé."),
    JOUEUR_CATEGORY_DUPLICATED(17001, "JoueurCategory dupliqué."),

    // JOUEUR_CATEGORY RESOURCE
    DELEGUE_NOT_FOUND(18000, "Délégué de match non trouvé."),
    DELEGUE_CODE_NOT_GENERATED(18001, "Erreur génération du code délégué."),
    DELEGUE_NOT_FOUND_FOR_MATCH(18002, "Il n'existe pas de délégué pour ce match."),

    // APP MOBILE
    ERROR_ABONNEMENT(20000, "L'abonnement au match a échoué."),

    TEST(0, "");


    private int value;
    private String message;

    ExceptionCode(int value, String message) {
        this.value   = value;
        this.message = message;
    }

    public int getValue() {
        return value;
    }

    public String getMessage() {
        return message;
    }
}
