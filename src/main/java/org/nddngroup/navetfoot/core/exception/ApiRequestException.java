package org.nddngroup.navetfoot.core.exception;

public class ApiRequestException extends RuntimeException {
    private final int code;
    private final ExceptionLevel level;

    public ApiRequestException(String message, int code, ExceptionLevel level) {
        super(message);

        this.code = code;
        this.level = level;
    }

    public int getCode() {
        return code;
    }

    public ExceptionLevel getLevel() {
        return level;
    }
}
