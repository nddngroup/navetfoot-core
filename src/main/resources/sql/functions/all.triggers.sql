CREATE OR REPLACE FUNCTION update_detail_poule_on_match_start()
  RETURNS trigger
  LANGUAGE plpgsql
 AS $function$declare
         resp integer := 0;
         encours character varying := 'EN_COURS_1';
 begin
         if new.id is null then
                 raise exception '_______ Match does not exist.';
         end if;

         if new.etat_match = encours then
                 raise log '_______ Match has started _____';

                 select maj_detail_poule(new.id) into resp;
         end if;

         return new;
 end;$function$;




CREATE OR REPLACE FUNCTION update_detail_poule_on_but()
  RETURNS trigger
  LANGUAGE plpgsql
 AS $function$declare
         resp integer;
 begin
         raise log '************ TRIGGER : UPDATE DETAIL POULE ************';

         raise log '_______ check if matchId is null _____';
         if new.match_id is null then
                 raise log '___ match id is null ___';
                 raise exception 'Match id must not be null.';
         end if;

         select maj_detail_poule(new.match_id) into resp;

         return new;
 end;$function$;


--
-- Name: but on_but_event; Type: TRIGGER; Schema: public; Owner: navetFoot
--

CREATE TRIGGER on_but_event AFTER INSERT ON but FOR EACH ROW EXECUTE PROCEDURE update_detail_poule_on_but();
CREATE TRIGGER on_but_update_event AFTER UPDATE ON but FOR EACH ROW EXECUTE PROCEDURE update_detail_poule_on_but();
CREATE TRIGGER on_but_delete_event AFTER DELETE ON but FOR EACH ROW EXECUTE PROCEDURE update_detail_poule_on_but();


--
-- Name: match on_match_updated; Type: TRIGGER; Schema: public; Owner: navetFoot
--

CREATE  TRIGGER on_match_updated AFTER UPDATE ON match FOR EACH ROW EXECUTE PROCEDURE update_detail_poule_on_match_start();

