 CREATE OR REPLACE FUNCTION total_buts_by_club_and_match(idclub bigint, idmatch bigint)
  RETURNS bigint
  LANGUAGE plpgsql
 AS $function$begin
         return (
                 select count(*) as nb_buts from but as bt
                 where match_id = (
                         select id from match as mt
                         where id = idmatch
                         and mt.deleted = false
                         and (
                                 (local_id = idclub and bt.equipe = 'LOCAL') or (visiteur_id = idclub and bt.equipe = 'VISITEUR')
                         )
                 )
                 and bt.deleted = false
         );
 end;$function$;




CREATE OR REPLACE FUNCTION total_buts_by_club_and_etape(idclub bigint, idetape bigint)
  RETURNS bigint
  LANGUAGE plpgsql
 AS $function$declare nb_buts bigint := 0; r bigint;

 begin
         for r in
                 select get_match_ids_by_etape(idetape)
         loop
                 raise info 'in loop .... %', r;

                 declare nb bigint;
                 begin
                         select total_buts_by_club_and_match(idclub, r) into nb;

                         raise info 'Match id %; Club id % : %',r,idclub,nb;

                         nb_buts := nb_buts + nb;
                 end;
         end loop;

         return nb_buts;
 end;$function$;





CREATE OR REPLACE FUNCTION total_buts_by_club_and_competition_and_saison(idclub bigint, idcompetition bigint, idsaison bigint)
  RETURNS bigint
  LANGUAGE plpgsql
 AS $function$declare
         nb_buts bigint := 0;
         cs_id bigint;
         etape_id bigint;

 begin

         for cs_id in
                 select id from competition_saison where competition_id = idcompetition and saison_id = idsaison  and deleted = false
         loop
                 raise info '_______ GET ALL ETAPES ________';

                 for etape_id in
                         select id from etape_competition where competition_saison_id = cs_id and deleted = false
                 loop
                         raise info 'in loop .... %', etape_id;

                         declare nb bigint;
                         begin
                                 select total_buts_by_club_and_etape(idclub, etape_id) into nb;

                                 raise info 'CompetitionId %; SaisonId %; ClubId % : %',idcompetition,idsaison, idclub, nb;

                                 nb_buts := nb_buts + nb;
                         end;
                 end loop;

         end loop;

         return nb_buts;
 end;$function$;




CREATE OR REPLACE FUNCTION get_match_ids_by_etape(idetape bigint)
  RETURNS SETOF bigint
  LANGUAGE plpgsql
 AS $function$begin
         return query(select id from match where etape_competition_id = idetape  and deleted = false);
 end;$function$;




CREATE OR REPLACE FUNCTION maj_detail_poule(idmatch bigint)
  RETURNS integer
  LANGUAGE plpgsql
 AS $function$declare
         idLocal bigint;
         idVisiteur bigint;
         idEtape bigint;
         mJournee integer;
         nb_buts_local bigint;
         nb_buts_visiteur bigint;
         goalDiff_l integer;
         goalDiff_v integer;
         butPour_l integer;
         butPour_v integer;
         butContre_l integer;
         butContre_v integer;
         nbPoints_l integer;
         nbPoints_v integer;
         victoire_l integer;
         victoire_v integer;
         defaite_l integer;
         defaite_v integer;
         matchNull_l integer;
         matchNull_v integer;

 begin
         raise log '____ Match id = %', idmatch;

         raise log '_____ Retrieve localId ____';
         select local_id from match as mt where mt.id = idMatch and deleted = false into idLocal;
         raise log '_____ localId = %____', idLocal;

         raise log '_____ Retrieve visiteurId ____';
         select visiteur_id from match where id = idMatch and deleted = false into idVisiteur;
         raise log '_____ visiteurId = %____', idVisiteur;

         raise log '_____ Retrieve journee ____';
         select journee from match where id = idMatch and deleted = false into mJournee;
         raise log '_____ Journee = %____', mJournee;

         raise log '_____ Retrieve etapeId ____';
         select etape_competition_id from match where id = idMatch and deleted = false into idEtape;
         raise log '_____ EtapeId = %____', idEtape;

         select total_buts_by_club_and_match(idLocal, idMatch) into nb_buts_local;
         select total_buts_by_club_and_match(idVisiteur, idMatch) into nb_buts_visiteur;

         raise log '_____ Nb buts local = %', nb_buts_local;
         raise log '_____ Nb buts visiteur = %', nb_buts_visiteur;

         goalDiff_l  := cast((nb_buts_local - nb_buts_visiteur) as integer);
         goalDiff_v  := cast((nb_buts_visiteur - nb_buts_local) as integer);
         butPour_l   := cast(nb_buts_local as integer);
         butPour_v   := cast(nb_buts_visiteur as integer);
         butContre_l := cast(nb_buts_visiteur as integer);
         butContre_v := cast(nb_buts_local as integer);

         if nb_buts_local > nb_buts_visiteur then
                 nbPoints_l  := 3;                nbPoints_v  := 0;
                 victoire_l  := 1;                victoire_v  := 0;
                 defaite_l   := 0;                defaite_v   := 1;
                 matchNull_l := 0;                matchNull_v := 0;

         elsif nb_buts_local < nb_buts_visiteur then
                 nbPoints_l  := 0;                nbPoints_v  := 3;
                 victoire_l  := 0;                victoire_v  := 1;
                 defaite_l   := 1;                defaite_v   := 0;
                 matchNull_l := 0;                matchNull_v := 0;

         else
                 nbPoints_l  := 1;                nbPoints_v  := 1;
                 victoire_l  := 0;                victoire_v  := 0;
                 defaite_l   := 0;                defaite_v   := 0;
                 matchNull_l := 1;                matchNull_v := 1;
         end if;

         raise log '______ update detail poule local';
         update detail_poule set
                 nombre_de_point = nbPoints_l,
                 goal_difference = goalDiff_l,
                 buts_pour       = butPour_l,
                 buts_contre     = butContre_l,
                 victoire        = victoire_l,
                 defaite         = defaite_l,
                 match_null      = matchNull_l,
                 update_at      = CURRENT_TIMESTAMP
         where association_id = idLocal

         and journee = mJournee
         and poule_id in (select id from poule where etape_competition_id = idEtape);

         raise log '______ update detail poule visiteur';
         update detail_poule set
                 nombre_de_point = nbPoints_v,
                 goal_difference = goalDiff_v,
                 buts_pour       = butPour_v,
                 buts_contre     = butContre_v,
                 victoire        = victoire_v,
                 defaite         = defaite_v,
                 match_null      = matchNull_v,
                 update_at      = CURRENT_TIMESTAMP
         where association_id = idVisiteur
         and journee = mJournee
         and poule_id in (select id from poule where etape_competition_id = idEtape);

         return 1;
 end;$function$;
