CREATE OR REPLACE FUNCTION get_match_ids_by_etape(idetape bigint)
  RETURNS SETOF bigint
  LANGUAGE plpgsql
 AS $function$begin
         return query(select id from match where etape_competition_id = idetape  and deleted = false);
 end;$function$;
