CREATE OR REPLACE FUNCTION update_detail_poule_on_but()
  RETURNS trigger
  LANGUAGE plpgsql
 AS $function$declare
         resp integer;
 begin
         raise log '************ TRIGGER : UPDATE DETAIL POULE ************';

         raise log '_______ check if matchId is null _____';
         if new.match_id is null then
                 raise log '___ match id is null ___';
                 raise exception 'Match id must not be null.';
         end if;

         select maj_detail_poule(new.match_id) into resp;

         return new;
 end;$function$;
